<?php
// Rule: function/variable_website_page

// ========== ONE FOR ALL ==========

// === LANGUAGE ===

define('DEFAULT_LANGUAGE', 'vi');
define('LANGUAGE_EN', 'en');
define('LANGUAGE_VI', 'vi');

// === Enable ===

define('ENABLE', true);

// === Social Media - URL ===
define('FACEBOOK_URL', 'https://www.facebook.com/NVHAI-306458502862792/');
define('YOUTUBE_URL', 'https://www.youtube.com/channel/UCxUL0zS-XiU36bkUsr5dWbg?sub_confirmation=1');
define('TWITTER_URL', 'https://twitter.com/KNvhai');
define('VIMEO_URL', 'https://vimeo.com/user195827968');
define('TWITCH_URL', 'https://www.twitch.tv/kingdomnvhai');
define('F2C_URL', ''); // f2c.com nvhai2306@gmail.com VietHai2306 https://video.laxd.com/a/
define('DALFC_URL', 'http://datealive.kingdomnvhai.info/');
define('KAWAIICODE_URL', 'http://kawaiicode.kingdomnvhai.info/');
define('KDPLAYBACK_COM_URL', 'http://kdplayback.com/');
define('SOSHIKI_URL', 'http://soshiki.kingdomnvhai.info/');

// === Social Media - API ===
define('FACEBOOK_API_URL', 'https://graph.facebook.com/v3.3/306458502862792?fields=id,name,fan_count&access_token=');
define('YOUTUBE_API_URL', '');
define('YOUTUBE_API_KEY', 'AIzaSyBYvogrKc3YK4xsXB0NCh6g7X-fnw2JJ4I');
define('TWITTER_API_URL', 'http://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=');

define('GOOGLE_APP_ID','1031036642052-1pu11eq2m53vqi9qi623ib1pq5a00f2o.apps.googleusercontent.com');
define('GOOGLE_APP_SECRET','lsmKvL-ODXxU3i0e8V-DPOHT');
define('GOOGLE_APP_CALLBACK_URL','http://kingdomnvhai.info/dashboard');

define('TWITTER_KEY', 'nvhai2306');

// === Ad ===
define('AD_728_DEFAULT', 'ad-728-default.png');
define('AD_300_DEFAULT', 'ad-300-default.png');
define('AD_728_1', 'ad-728-1.png');
define('AD_728_2', 'ad-728-2.png');
define('AD_728_3', 'ad-728-3.png');
define('AD_300_1', 'ad-300-1.png');
define('AD_300_2', 'ad-300-2.png');
define('AD_300_3', 'ad-300-3.png');

define('AD_NHATHUOC', 'http://nhathuockhanhan.com/');
define('AD_NHATHUOC_IMG', 'ads-medical-360.jpg');
define('AD_NHATHUOC_ALT', 'Nhà thuốc khánh an online 24g');
define('AD_CHAN_NUOI_92', 'http://channuoi92.vn/');
define('AD_CHAN_NUOI_92_IMG', 'ads-chan-nuoi-92-360.jpg');
define('AD_CHAN_NUOI_92_ALT', 'Nhà thuốc khánh an online 24g');

// === LIMIT ===
define('HOME_POSTS', 5);
define('MOST_VIEW_HOME_POSTS', 4);
define('UPDATED_HOME_POSTS', 6);
define('RECENT_HOME_POSTS', 10);
define('NEWEST_HOME_POSTS', 8);
define('DASHBOARD_HOME_POSTS', 6);

// === Type Excel ===
define('TYPE_EXCEL_ALL', 0);
define('TYPE_EXCEL_VTUBER', 1);

// ========== ERROR ==========

// === Auth ===
define('WRONG_USERNAME', 'Tên đăng nhập sai');
define('WRONG_PASSWORD', 'Mật khẩu sau');
define('USERNAME_IS_NOT_EXIST', 'Tên đăng nhập không tồn tại');
define('EMAIL_IS_EXIST', 'Email đã tồn tại');
define('EMAIL_IS_SENT', 'Email is sent');





// ========== ADMIN ==========

// === User's Role ===
define('ROLE_ADMIN', 1);
define('ROLE_EDITOR', 2);
define('ROLE_WRITER', 3);
define('ROLE_MEMBER', 4);

// === Paginate ===

define('ITEM_PER_PAGE', 20);

// === Category ===

define('NO_CATEGORY', 1);
define('WEBSITE_POST', 2);
define('GAME_POST', 3);
define('ANIME_POST', 4);
define('THU_THUAT_IT_POST', 5);
define('GAME_GIRL_POST', 6);
define('UNIVERSAL_POST', 7);
define('GRAVURE_IDOL_POST', 8);

// === Hidden Post ===

define('HIDDEN_POST', 0);
define('APPEAR_POST', 1);

// === Update Post ===

define('UPDATE_POST', 1);

// === title ===

define('TITLE_ADMIN_DASHBOARD', 'DASHBOARD KINGDOM NVHAI');
define('TITLE_ADMIN_POST'     , 'POST MANAGEMENT KINGDOM NVHAI');
define('TITLE_ADMIN_SITE'     , 'SITE MANAGEMENT KINGDOM NVHAI');

define('TITLE_NEWS_INDEX', ' | NEWS KINGDOM NVHAI');
define('TITLE_KDPLAYBACK_INDEX', ' | KDPLAYBACK');

// === paginate ===

define('PAGINATE_POST_INDEX', '40');
define('PAGINATE_POST_DELETE', '40');
define('PAGINATE_POST_SEARCH', '40');

define('PAGINATE_SITE_INDEX', '15');

define('PAGINATE_CHANNEL', '30');

// ========== NEWS ==========

// === list post ===
define('POST_PER_CATEGORY', 30);

// === involve posts ===
define('INVOLVE_POST', '6');

// ========== VIDEOS ==========

// === all ===
define('HOME_VIDEOS', '8');

// === channel ===
define('VIDEO_PER_CHANNEL', '20');

// === ID CHANNEL ===
define('CHANNEL_KINGDOM_NVHAI', 'UCxUL0zS-XiU36bkUsr5dWbg');
define('CHANNEL_PEWDIEPIE', 'UC-lHJZR3Gqxm24_Vd_AJ5Yw');
define('CHANNEL_KIZUNA_AI', 'UC4YaOt1yT-ZeyB0OmxHgolA');
define('CHANNEL_HOLOLIVE', 'UCJFZiqLMntJufDCHc6bQixg');
