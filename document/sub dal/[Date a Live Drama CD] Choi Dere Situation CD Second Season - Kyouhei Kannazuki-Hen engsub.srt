﻿0
00:00:03,512-->00:00:05,113
Shidou đây.

1
00:00:06,112-->00:00:08,513
Kotori, anh hỏi lại nhé.

2
00:00:09,112-->00:00:14,113
Tinh Linh lần này, thật sự là người đó sao?

3
00:00:16,112-->00:00:17,113
Vậy à.

4
00:00:19,112-->00:00:21,513
Không còn cách nào khác.

5
00:00:22,112-->00:00:24,513
Mình phải hẹn hò với anh ấy,
làm anh ấy yêu mình.

6
00:00:25,112-->00:00:26,113
Và phong ấn anh ấy.

7
00:00:26,512-->00:00:29,113
Xin chào! Xin lỗi đã để em đợi lâu, Shidou-kun.

8
00:00:30,512-->00:00:32,513
Chào anh, Kannazuki-san.

9
00:00:33,112-->00:00:35,113
Được rồi. Mọi thứ sẵn sàng chưa?

10
00:00:35,512-->00:00:36,513
Làm gì cơ?

11
00:00:37,112-->00:00:40,113
Thôi nào Shidou-kun. 
Anh đáng nói về chuyện phong ấn. Phong ấn đấy.

12
00:00:40,512-->00:00:43,113
Đó là lý do em mời anh đi hẹn hò phải không?

13
00:00:43,312-->00:00:45,113
Thôi nào, đừng ngại mà.

14
00:00:45,312-->00:00:47,513
Thật sao?

15
00:00:49,112-->00:00:51,113
Nhưng đầu tiên là phải hẹn hò chút đã...

16
00:00:51,512-->00:00:53,513
Rồi còn thang đo tâm trạng...

17
00:00:54,112-->00:00:55,513
Em định để anh đợi sao?

18
00:00:56,113-->00:00:58,913
Anh đã sẵn sàng bất cứ lúc nào em muốn.

19
00:00:59,112-->00:01:01,113
Nhưng em chưa sẵn sàng.

20
00:01:02,112-->00:01:03,113
Mà thôi không sao.

21
00:01:03,312-->00:01:05,113
Vậy chúng ta đi đâu đây?

22
00:01:08,112-->00:01:09,713
Đi xem phim thì sao?

23
00:01:10,512-->00:01:13,113
Xem phim nghe hay đấy.

24
00:01:14,112-->00:01:17,113
Shidou-kun đang ngồi xem phim,
ngây thơ ăn bỏng ngô.

25
00:01:17,512-->00:01:20,913
Trong ánh đèn mờ mờ của màn hình,
2 người bắt đầu nhìn nhau.

25
00:01:21,112-->00:01:24,513
Em bắt đầu trấn tĩnh lại 
và đưa tay vào tay anh.

25
00:01:25,112-->00:01:28,113
Cảm thấy sự lo lắng trong bóng tối,
chúng ta nghiên đầu nhìn nhau.

25
00:01:28,312-->00:01:31,913
Và trong khoảnh khắc đó,
đôi ta hôn nhau trước màn hình

25
00:01:32,112-->00:01:33,913
Và rồi Shidou-kun sẽ phong ấn anh
với đôi môi nồng cháy...

25
00:01:34,112-->00:01:36,513
Không xem phim nữa! Hủy bỏ!

25
00:01:37,112-->00:01:40,913
Vậy à? Anh nghĩ đó là tình huống
tuyệt vời để phong ấn mà...

25
00:01:41,112-->00:01:43,513
Đi ăn gì đó đi! Anh muốn đi ăn ở đâu?

25
00:01:44,112-->00:01:47,113
Anh sẽ chỉ cho em chỗ ưa thích của anh.

25
00:01:48,112-->00:01:51,513
Đó là một nơi tuyệt đẹp, 
với đồ ăn ngon và không khí tao nhã.

25
00:01:53,112-->00:01:56,113
Anh đúng là người trưởng thành nhỉ, 
Kannazuki-san.

25
00:01:57,112-->00:01:58,512
Em chỉ biết mấy quán ăn gia đình thôi.

25
00:02:00,112-->00:02:02,513
Với tuổi của em thì đúng, Shidou-kun.

25
00:02:03,112-->00:02:07,113
Em cần học dần dần để làm quen đi. 

25
00:02:07,813-->00:02:09,113
Vâng, em sẽ học.

25
00:02:09,512-->00:02:11,113
Vậy chúng ta đi chứ?

25
00:02:12,112-->00:02:13,313
Đó là một nơi rất lộng lẫy đấy.

25
00:02:13,512-->00:02:17,113
Ở phía sau nó có một căn phòng riêng tư,
rất thích hợp cho các cặp đôi.

25
00:02:17,512-->00:02:19,913
Các nhân viên sẽ chú ý 
và họ sẽ không làm phiền chúng ta...

25
00:02:20,112-->00:02:23,113
Em nghe đủ rồi!

25
00:02:23,312-->00:02:26,113
Em không thể ăn được nữa!

25
00:02:27,112-->00:02:27,913
Em không sao chứ?

25
00:02:28,112-->00:02:29,913
Em nghĩ sẽ tốt hơn nếu chúng ta đi dạo!

25
00:02:30,112-->00:02:32,113
Chúng ta đi dạo đi!

25
00:02:32,312-->00:02:35,113
Đi ra chỗ đường lớn đông người ấy!

25
00:02:35,312-->00:02:36,713
Được rồi.

25
00:02:37,112-->00:02:41,113
Phong ấn ngay trước mặt mọi người
thật kích thích phải không?

25
00:02:43,912-->00:02:45,513
Vậy chúng ta đi chứ?

26
00:02:48,112-->00:02:52,513
Nhưng bất ngờ thật đấy.
Anh chưa bao giờ nghĩ mình là Tinh Linh.

26
00:02:53,112-->00:02:54,513
Nó không có biểu hiện gì sao?

26
00:02:55,512-->00:02:58,113
Anh ghét phải thừa nhân, nhưng đúng là thế.

26
00:02:58,712-->00:03:03,113
Giờ anh nghĩ, anh để ý gần đây
tóc và da anh phát triển khá kỳ lạ.

26
00:03:03,312-->00:03:05,313
Anh thật may mắn vì đã đi kiểm tra sức khỏe.

26
00:03:05,513-->00:03:07,513
Họ phát hiện bằng cách kiểm tra sức khỏe sao?

26
00:03:07,812-->00:03:11,113
Đúng vậy. Họ cũng phát hiện
anh có vài vấn đề nhỏ về tiêu hóa.

26
00:03:12,112-->00:03:13,113
Anh phải chăm sóc bản thân đấy.

26
00:03:13,512-->00:03:15,113
Cảm ơn em nhiều.

26
00:03:16,112-->00:03:20,113
Shidou-kun, đi lối này đi.
Chỗ này nhiều xe lắm đấy.

26
00:03:20,512-->00:03:24,113
Không cần đâu. Em đâu phải con gái đâu.

26
00:03:25,112-->00:03:28,313
Nếu có chuyện gì xảy ra,
chỉ huy và các cô gái sẽ buồn lắm đấy.

27
00:03:28,812-->00:03:31,113
Và cả anh cũng vậy.

28
00:03:33,112-->00:03:34,913
Em xin lỗi.

29
00:03:35,112-->00:03:39,113
Anh đã luôn theo dõi em, Shidou-kun.

30
00:03:39,512-->00:03:44,113
Kể cả khi em có sức mạnh đặc biệt,
đối đầu với Tinh Linh mà không có gì trong tay...

31
00:03:44,312-->00:03:46,113
Đó là việc không ai có thể làm được.

32
00:03:46,312-->00:04:47,513
Thật đáng khen ngợi.

33
00:03:47,712-->00:03:49,113
Không, đó không phải là chuyện to tát gì đâu...

34
00:03:49,512-->00:03:54,513
Nhìn thấy em cố gắng trong tuyệt vọng  
để cứu Tinh Linh, không màng đến hậu quả,

35
00:03:55,312-->00:04:58,113
Thật tuyệt vời. Anh rất thích điều đó.

36
00:04:00,112-->00:04:02,113
Cảm... cảm ơn anh nhiều.

37
00:04:02,313-->00:04:05,913
Nhưng em chỉ làm những gì em nghĩ thôi.
Em không cần được khen ngợi đâu...

38
00:04:06,112-->00:04:08,513
Thật tuyệt khi em có thể làm thế.

39
00:04:09,112-->00:04:10,513
Kannazuki-san...

40
00:04:10,913-->00:04:12,113
Kyouhei.

41
00:04:13,113-->00:04:15,513
Cứ gọi anh là Kyouhei.

42
00:04:16,113-->00:04:20,113
Em không gọi Tohka-chan 
là Yatogami-san phải không?

43
00:04:21,112-->00:04:24,113
Không, đó chỉ là...

39
00:04:25,512-->00:04:28,513
Có vẻ yêu cầu đó hơi quá đáng nhỉ.

40
00:04:29,113-->00:04:31,113
Vậy giờ chúng ta làm gì?

41
00:04:31,513-->00:04:34,113
Chúng ta đang ở trung tâm thành phố rồi.

42
00:04:34,513-->00:04:36,113
Anh nói đúng.

43
00:04:36,312-->00:04:38,113
Chúng ta đi về phía nhà ga nhé.

39
00:04:38,312-->00:04:41,113
và đi dạo qua các trung tâm mua sắm 
hoặc đi đến khu trò chơi thì sao?

40
00:04:43,513-->00:04:45,113
Kannazuki-san?

41
00:04:46,113-->00:04:48,113
Toàn thân ra phát ra ánh sáng kìa!

42
00:04:50,113-->00:04:55,913
Đừng lo. Cách đây vài ngày, 
chuyện này thường xảy ra 
do anh là một Tinh Linh.

43
00:04:56,112-->00:04:59,113
Chuyên gia phân tích Murasame nói rằng
hiện giờ nó không nguy hiểm.

44
00:05:00,113-->00:05:02,513
Nhưng trông anh có vẻ đau đớn lắm.

45
00:05:02,712-->00:05:05,113
Có cần về Fraxinus ngay bây giờ không?

46
00:05:08,112-->00:05:13,113
Không sao đâu. Nhưng có một chuyện
anh muốn nhờ em, Shidou-kun.

47
00:05:13,512-->00:05:17,113
Là gì vậy? Nếu em có thể làm được,
em sẽ làm bất kỳ điều gì.

48
00:05:18,112-->00:05:20,113
Làm ơn, la mắng anh đi.

49
00:05:21,112-->00:05:26,113
Mỗi khi chuyện này xảy ra,
chỉ huy Itsuka đều la mắng anh rất gay gắt

50
00:05:26,512-->00:05:30,113
Nếu làm thế, cơ thể anh sẽ dịu bớt.

51
00:05:30,812-->00:05:33,113
Xin em đấy, Shidou-kun. Hãy la mắng anh đi.

52
00:05:33,712-->00:05:35,313
Dù anh có nói thế...

53
00:05:37,112-->00:05:40,113
Làm ơn đi, Shidou... kun...

54
00:05:41,112-->00:05:44,113
Đợi đã. Em sẽ gọi Kotori.

55
00:05:45,112-->00:05:46,913
Sao không gọi được?

56
00:05:47,112-->00:05:48,713
Kotori, Kotori!

57
00:05:49,112-->00:05:52,913
Anh quên chưa nói. Ánh sáng phát ra từ anh

58
00:05:53,112-->00:05:55,313
có thể cản trở mọi phương tiện liên lạc.

59
00:05:55,512-->00:05:57,113
Đó mới là vấn đề đấy!

60
00:05:58,112-->00:06:00,113
Mình không còn lựa chọn nào khác.

41
00:06:00,312-->00:06:04,113
Em không biết em làm được đến đâu,
nhưng em sẽ cố gắng bắt chước Kotori

42
00:06:07,112-->00:06:13,113
Này Kannazuki! Sao anh lại để
toàn thân phát sáng như vậy?

43
00:06:14,112-->00:06:17,113
Đúng rồi! Tuyệt lắm, Shidou-kun!

44
00:06:18,112-->00:06:22,513
Anh phát sáng cứ chập chờn.
Bộ anh tính làm cây thông Noel à?

45
00:06:23,112-->00:06:28,513
Nếu vậy, sao tôi không gắn ngôi sao
lên đầu anh nhỉ? 

46
00:06:29,112-->00:06:32,113
Tiếp, tiếp đi! Tiếp nữa đi!

47
00:06:32,513-->00:06:37,113
Ngoài ra, cá lồng đèn hay sứa 
còn có thể lớn được.

48
00:06:37,512-->00:06:40,913
Anh đang đóng góp vào chuỗi thức ăn à?
Hay anh định phát triển sinh học?

49
00:06:41,112-->00:06:43,113
Anh nói gì chứ? 
Không phải sao? Không phải sao?

50
00:06:43,312-->00:06:46,113
Đúng là tên khổ dâm biến thái!

51
00:06:48,112-->00:06:50,113
Những lời la mắng thật tuyệt vời!

52
00:06:51,112-->00:06:53,113
Không hổ danh là Onii-san của chỉ huy!

53
00:06:54,112-->00:06:59,113
Ka... Kannazuki-san, em đã làm gì sai sao?

54
00:07:00,112-->00:07:04,513
Không. Chúng đã nghe thấy 
những lời la mắng giống chỉ huy

60
00:07:05,112-->00:07:07,113
Anh biết em sẽ không làm anh thất vọng mà.

61
00:07:07,512-->00:07:09,513
Em không biết em có nên mừng không...

62
00:07:12,112-->00:07:14,113
Cơ thể anh tốt hơn nhiều rồi.

63
00:07:15,112-->00:07:17,513
Vậy chúng ta tiếp tục cuộc hẹn thôi.

64
00:07:21,112-->00:07:22,113
Kannazuki-san?

65
00:07:23,112-->00:07:24,113
Báo động Không Gian Chấn!

66
00:07:24,512-->00:07:26,113
Đây là sức mạnh của Tinh Linh?

67
00:07:27,112-->00:07:29,113
Có vẻ đúng như em nghĩ đấy.

68
00:07:29,512-->00:07:32,113
Tại sao chứ? Em vừa la mắng anh mà!

69
00:07:32,512-->00:07:34,913
Cơ thể anh đã tốt hơn.

70
00:07:35,113-->00:07:38,913
Nhưng em làm giống quá nên 
sức mạnh Tinh Linh đã mất kiểm soát rồi.

71
00:07:39,112-->00:07:41,113
Sao lại có chuyện đó được?

72
00:07:41,512-->00:07:44,913
Chạy đi, Shidou-kun! Không Gian Chấn
sẽ xảy ra bất kỳ lúc nào đấy!

73
00:07:45,112-->00:07:47,113
Anh không thể kiềm chế hơn được nữa!

74
00:07:47,313-->00:07:48,313
Kannazuki-san!

75
00:07:48,512-->00:07:56,113
Anh xin em, làm ơn tránh xa anh ra!

77
00:07:56,512-->00:07:58,513
Anh nghĩ em sẽ bỏ anh sao?

78
00:08:00,112-->00:08:01,113
Shidou-kun?

81
00:08:01,512-->00:08:07,513
Cuộc hẹn với Kannazuki-san, 
không, với Kyouhei vẫn chưa kết thúc!

82
00:08:08,112-->00:08:10,513
Em sẽ không để việc này làm hỏng
khoảnh khắc của chúng ta đâu.

83
00:08:12,112-->00:08:15,113
Shidou-kun, em vừa gọi anh bằng tên...

84
00:08:16,112-->00:08:17,113
Nhưng còn Không Gian Chấn...

85
00:08:17,312-->00:08:18,113
Em không quan tâm!

86
00:08:18,812-->00:08:22,113
Sức mạnh Tinh Linh 
đang làm anh đau đớn, Kyouhei...

87
00:08:22,512-->00:08:26,113
Em sẽ... Em sẽ phong ấn nó bằng đôi môi này!

88
00:08:27,112-->00:08:28,113
Shidou...

89
00:08:30,112-->00:08:31,113
Anh yêu em!

90
00:08:31,512-->00:08:34,113
Em cũng vậy, Kyouhei!

91
00:08:49,112-->00:08:52,113
Chỉ là mơ thôi à?

92
00:08:54,112-->00:08:55,513
Thật hú vía!

93
00:09:00,112-->00:09:02,113
Một giấc mơ buổi sáng kinh khủng.

94
00:09:02,512-->00:09:04,513
Mình cảm thấy hơi mệt.

98
00:09:05,712-->00:09:08,113
Cậu dậy rồi à, Shidou-kun?

99
00:09:08,512-->00:09:11,513
Kyouhei... Không phải, Kannazuki-san...

100
00:09:13,112-->00:09:14,113
Có chuyện gì sao?

101
00:09:14,312-->00:09:15,913
Không, không có gì cả?

102
00:09:16,113-->00:09:17,113
À phải rồi, Shidou-kun,

103
00:09:17,312-->00:09:21,113
Hôm nay họ sẽ bán đấu giá
giày của chỉ huy Itsuka đấy.

104
00:09:21,312-->00:09:22,113
Muốn đi với tôi không?

105
00:09:22,312-->00:09:25,113
Đó là thứ độc nhất vô nhị đấy.

106
00:09:25,312-->00:09:27,513
Không, cảm ơn anh.

107
00:09:29,112-->00:09:31,113
Đúng là Shidou-kun, kín đáo thật đấy.

108
00:09:31,512-->00:09:34,113
Nhưng tôi thích điểm đó ở cậu.

109
00:09:34,312-->00:09:35,113
Xin phép nhé.

110
00:09:36,112-->00:09:42,113
Giấc mơ của mình, giấc mơ của mình, 
giấc mớ được chà đạp bởi đôi giày đó!

111
00:09:44,112-->00:09:48,113
Mình nghĩ mình vừa thấy muốn xỉu đấy.

108
00:09:40,512-->00:09:43,113


