﻿0
00:00:00,512-->00:00:02,113
1) Hẹn hò với Tohka.
2) Hẹn hò với Origami.
3) Hẹn hò với chị em Yamai.
4) Hẹn hò với Miku.
5) Tiếp theo.

1
00:00:06,512-->00:00:09,113
Tớ sẽ chọn... Origami. 

2
00:00:10,512-->00:00:15,513
Tớ biết cậu sẽ chọn tớ mà.

3
00:00:17,112-->00:00:23,513
Gì thế hả, Shidou. Cậu đùa tôi à?
Hóa ra cậu thích cô ta sao!

5
00:00:26,112-->00:00:35,113
Ghen tị. Mặc dù sư phụ Origami rất tuyệt vời,
tại sao cậu không chọn Kaguya và Yuzuru?

6
00:00:36,112-->00:00:40,113
Không, không phải tớ thích cậu ấy.
Tớ cần nói chuyện với cậu ta...

7
00:00:41,112-->00:00:53,113
Bất công quá!
Em biết cô ấy rất dễ thương nhưng
em muốn hẹn hò với anh cơ, darling.

8
00:00:54,112-->00:01:02,513
Shidou! Sao anh lại chọn Tobiichi Origami chứ?
Anh phải hẹn hò với em!

8
00:01:03,112-->00:01:06,113
Tình hình vẫn không khá lên chút nào...

9
00:01:07,112-->00:01:10,113
Trong tình huống thế này... Đúng vậy!
Tôi chỉ muốn yêu cầu sự giúp đỡ từ Fraxinus.

10
00:01:11,112-->00:01:15,113
Khi tôi nói chuyện với các Tinh Linh,
tôi luôn được theo dõi và tư vấn. 
Tôi chắc chắn sẽ cầu cứu 
tình hình quan trọng này!

11
00:01:16,512-->00:01:20,113
Ara Shidou. Anh đang gặp vấn đề, phải không?

12
00:01:21,112-->00:01:23,113
Kotori, vấn đề vừa xảy ra đấy.

13
00:01:24,112-->00:01:28,113
Itsuka Kotori. Em gái tôi và cũng là
chỉ huy phi thuyền Fraxinus. 
Vào những lúc như thế này, nó là
người mà tôi có thể tin tưởng... Có lẽ vậy.

14
00:01:29,112-->00:01:31,113
Làm ơn... làm cái gì đó trong
tình hình này đi.

15
00:01:32,112-->00:01:47,113
Xem nào. Các cô gái, ngoại trừ 
Tobiichi Origami, mất kiểm soát 
sức mạnh nếu tâm lý không ổn định. 
Anh hiểu điều này, phải không?

16
00:01:48,112-->00:01:50,113
Anh biết. Và đó là lý do tại sao 
anh đang gặp rắc rối!

17
00:01:51,112-->00:01:56,513
Và trên hết, anh đã chọn 
Origami phải không?

18
00:01:57,512-->00:02:01,112
Đúng vậy. Nếu chọn Origami là con người
thì không sợ cô ấy mất kiểm soát.
Mặc dù vậy, những cô gái còn lại sẽ tức giận.

19
00:02:02,112-->00:02:07,512
Anh cũng nghĩ được đến thế à.
Cứ làm vậy đi. 

20
00:02:08,112-->00:02:14,112
Bọn em sẽ lo phần còn lại.
Trong mọi trường hợp, 
anh phải để ý cô ấy đó đấy.

21
00:02:15,112-->00:02:17,112
Anh hiểu rồi. Anh sẽ chăm sóc cô ấy.

22
00:02:19,112-->00:02:20,512
Được rồi! Origami, chúng ta đi nhanh nào!

23
00:02:21,512-->00:02:23,513
Tớ hiểu rồi.

25
00:02:25,112-->00:02:26,513
Hở?

25
00:02:31,112-->00:02:35,513
Đứng yên nào, Shidou.

25
00:02:36,512-->00:02:38,513
Cậu định đưa tớ đi đâu?

25
00:02:39,112-->00:02:41,113
Im lặng

25
00:02:42,112-->00:02:44,113
Này... chờ đã...!

25
00:02:51,512-->00:02:54,113
Sau khi tỉnh lại, 
Origami đã đưa tôi đến một nơi khác...

25
00:02:55,112-->00:02:57,113
Đây là... lớp học...

25
00:02:58,112-->00:03:02,113
Ở đây sẽ không có ai để ý.

26
00:03:03,112-->00:03:07,113
Đúng vậy. Giờ chẳng còn ai ở trường nữa.
Tớ cũng không nghĩ mình sẽ quay lại đây.

26
00:03:08,112-->00:03:12,113
Ngay cả khi cậu để đồ đạc lộn xộn...

26
00:03:13,112-->00:03:15,113
Tớ không làm những chuyện như thế...

26
00:03:16,112-->00:03:20,113
Vậy thì, cả khi cậu quậy phá...

26
00:03:21,112-->00:03:23,513
Còn tệ hơn nữa đấy!

26
00:03:24,512-->00:03:27,113
Tớ có thể giúp cậu quên hết mệt mỏi...

26
00:03:28,112-->00:03:31,113
Đây là nơi tớ đi học!
Tớ không làm gì ở đây đâu!

26
00:03:32,112-->00:03:35,113
Vậy là cậu muốn làm ở nơi khác à?

26
00:03:36,112-->00:03:38,113
Ý tớ không phải thế!

26
00:03:39,112-->00:03:41,513
Thế thì tiếc thật...

26
00:03:42,512-->00:03:46,113
Vậy trở lại vấn đề.
Cậu đưa tớ đến đây có việc gì không?

26
00:03:47,112-->00:03:51,113
Chúng ta cùng làm bài tập về nhà đi.

26
00:03:52,112-->00:03:55,113
Hở? Bài tập về nhà à?
Không, ý tớ là.. 

26
00:03:56,112-->00:04:00,513
Phải. Đó là mục đích ban đầu 
để cậu nói chuyện với tớ mà.

26
00:04:01,512-->00:04:06,114
Đúng là tớ có nói thế...
Vậu cậu có thể chỉ tớ chứ?

26
00:04:07,114-->00:04:12,113
Được rồi, Shidou.
Tớ sẽ làm bất cứ điều gì để dạy cậu.

26
00:04:13,112-->00:04:16,113
Này này... "Dạy" nghĩa là sao hả?

26
00:04:18,112-->00:04:20,113
Khoan, chờ chút đã Origami.

26
00:04:21,112-->00:04:23,113
Gì vậy?

26
00:04:23,512-->00:04:27,113
Đó là bàn của tớ.
Hơn nữa, sách giáo khoa đó là của tớ...

26
00:04:28,112-->00:04:33,513
Không sao đâu. Cậu có thể dùng
bàn và sách của tớ.

26
00:04:34,513-->00:04:36,513
Thôi không sao...

26
00:04:37,513-->00:04:41,513
Có nhiều cách để giải bài tập này.

26
00:04:42,512-->00:04:45,513
Nhiều cách là cách gì?

26
00:04:49,112-->00:04:52,113
Woa... Cậu đang tập trung giải quyết vấn đề...
Đây có phải là cách tập trung suy nghĩ không?

26
00:04:53,112-->00:05:02,113
Sướng quá. Bàn ghế... của Shidou...

26
00:05:03,512-->00:05:05,113
Này!

26
00:05:06,112-->00:05:19,113
Không sao đâu. Cứ thử với bàn ghế của tớ đi.
Hương vị và tư thế ngồi dựa vào ghế
rất phù hợp với trí tưởng tượng.

26
00:05:20,112-->00:05:21,513
Cậu...

26
00:05:22,512-->00:05:26,113
Chắc chắn tôi không bao giờ 
nghĩ về Origami như thế.

26
00:05:27,112-->00:05:31,113
Sau đó, tôi không biết Origami 
đang tưởng tượng ra gì với tôi.
OK là từ tôi nhận được 
khi ngồi cách xa Origami...

26
00:05:32,112-->00:05:35,113
Chắc cô ấy đang nghĩ về chuyện gì đó
mà những người yêu nhau hay làm.

26
00:05:36,112-->00:05:38,113
Tôi rất muốn giải quyết tất cả những hiểu lầm...

26
00:05:39,112-->00:05:42,113
Này Origami? Cậu biết đấy...
Về chuyện hẹn hò... chúng ta...

26
00:05:43,112-->00:05:47,513
Cậu hay thường nói chuyện lúng túng.

26
00:05:48,112-->00:05:57,513
Đồ lót con trai khi tập thể dục hay có mùi.
cổ áo thể dục và đồng phục cũng vậy.
Tớ vẫn giữ chúng nguyên vẹn đấy.

26
00:05:58,512-->00:06:07,513
Nhưng cứ yên tâm.
Tớ làm điều này là biểu hiện tình cảm 
giữa những người yêu nhau.

26
00:06:08,512-->00:06:14,113
Này... đó là một quan niệm sai lầm đấy.
Tớ không nghĩ đó là biểu hiện tình cảm đâu!

26
00:06:15,112-->00:06:27,113
Vậy nên Shidou, nhờ có số liệu
tớ thường viết trên ghế, tớ có thể biết
đứa con đầu lòng của chúng ta như thế nào.
Tớ vui lắm.

26
00:06:28,113-->00:06:29,513
Thôi được rồi!

26
00:06:30,513-->00:06:32,113
Sao vậy?

26
00:06:33,512-->00:06:35,513
Được rồi, để tớ làm bài tập đi.
Cậu tập trung làm đi.

26
00:06:36,512-->00:06:38,113
Được rồi.

26
00:06:43,112-->00:06:46,113
Và như thế, chúng tôi không nói thêm điều gì
cho đến khi hoàn thành bài tập về nhà.

368
00:16:08,112-->00:16:10,113
Mình vừa ngủ dậy à?

368
00:16:11,112-->00:16:13,113
Mình đang ở nhà sao? Mình đang nằm mơ à?

368
00:16:18,112-->00:16:20,113
Có lẽ... đây là một giấc mơ kỳ lạ?

368
00:16:21,112-->00:16:24,113
Tôi có cảm giác rất kỳ lạ. 
Nhưng đây chắc chắn là nhà tôi.

368
00:16:25,112-->00:16:27,113
Giờ mình nên bắt đầu chuẩn bị bữa tối thôi.

368
00:16:28,512-->00:16:31,113
Anh đang nói những thứ ngớ ngẩn gì vậy?

368
00:16:32,512-->00:16:34,113
Hở? Kotori?

368
00:16:35,113-->00:16:38,113
Tôi nghe thấy giọng nói rõ ràng.
Nhưng tôi không thấy Kotori.

368
00:16:38,512-->00:16:41,113
Chuyện gì đang xảy ra vậy? 
Tiếng nói phát ra từ đâu? Và tại sao 
anh không đeo tai nghe liên lạc?

368
00:16:44,512-->00:16:47,113
Một khoảnh khắc im lặng. 
Bây giờ là tiếng nói của Kotori.

368
00:16:48,112-->00:16:56,113
Em đang nói chuyện trực tiếp với tâm trí anh.
Khi anh đang ở trong trò chơi, 
không cần phải dùng máy liên lạc.

368
00:16:57,112-->00:17:00,113
Trong trò chơi sao? Khoan đã!
Chẳng lẽ đây là... không gian ảo?

368
00:17:01,112-->00:17:04,113
Tôi nhanh chóng chạm vào một chiếc ghế,
cửa sổ và tường. Cảm giác vẫn như mọi khi.

368
00:17:05,112-->00:17:08,113
Tôi chạm vào mặt tôi. Phải, cả hai tay
và da mặt mình vẫn có cảm giác.

368
00:17:09,112-->00:17:12,113
Nếu đây là trò chơi thì chắc anh
sẽ không cảm thấy đau nếu tự véo má...

368
00:17:13,512-->00:17:15,513
Uida! Không! Anh vẫn thấy đau!

368
00:17:16,512-->00:17:23,513
Tất cả các giác quan đã kết nối.
Em đã nói với anh đây là một 
thế giới Siêu Giả Lập. 

368
00:17:24,112-->00:17:28,513
Cả anh và thế giới đều giống hệt với thực tế.

368
00:17:29,512-->00:17:32,513
Thế giới Siêu Giả Lập...

368
00:17:33,512-->00:17:45,113
Để chính xác, không phải tất cả các dữ liệu 
đều được chuyển đổi. Các đối tượng như ghế 
hoặc tường là từ ký ức của riêng anh, Shidou.

368
00:17:46,112-->00:17:48,113
Lấy hình ảnh từ ký ức của anh?

368
00:17:49,112-->00:17:58,113
Anh không cần quá quan tâm  
chi tiết đâu. Ồ, và cách liên lạc 
này hơi kém hơn bình thường đấy.

368
00:17:59,112-->00:18:01,113
Kém hơn à?

368
00:18:02,112-->00:18:11,113
Theo thời gian, sẽ có sự khác biệt 
về thời gian giữa thế giới ảo 
và thế giới thực.

368
00:18:12,112-->00:18:22,113
Thời gian trong thế giới ảo nhanh hơn. 
Một ngày trong đó bằng 
khoảng 20-30 phút ngoài thật.

368
00:18:23,112-->00:18:35,113
Vì vậy, khi một cuộc trò chuyện 
được thiết lập với thế giới thực, 
anh sẽ phải chỉnh lại thời gian đó.

368
00:18:36,112-->00:18:39,513
Nhưng bọn em không thể cho anh 
hướng dẫn chi tiết như chúng ta thường làm. 

368
00:18:40,112-->00:18:47,513
Hãy nghĩ đó là bài tập thể dục
để nâng cao trí nhớ của anh đi Shidou.

368
00:18:48,512-->00:18:51,513
Thời gian khác nhau à...

368
00:18:52,512-->00:18:54,513
Vậy giờ anh làm gì đây?

368
00:18:55,512-->00:19:02,113
Giờ chúng ta hãy xem bên ngoài.
Anh ra ngoài và đi quanh nhà đi.

368
00:19:02,512-->00:19:07,513
Các sự kiện đã được thiết lập.
Giờ là lúc để kiểm tra chúng.

368
00:19:08,512-->00:19:11,113
Anh hiểu rồi. Anh đi ngay đây.

368
00:19:16,512-->00:19:19,113
Mình không thể tin rằng 
đây là trong trò chơi...

368
00:19:20,112-->00:19:24,113
Không, chắc chắn tôi không cảm thấy 
bất cứ điều gì lạ lẫm. Tất cả đều rất
quen thuộc. Tôi luôn nghĩ làm sao 
Ratatoskr có công nghệ thế này?

368
00:19:27,112-->00:19:30,113
Này Kotori, có rất nhiều người đi bộ. 
Đây cũng là những dữ liệu sao?

368
00:19:32,112-->00:19:41,113
Đúng vậy. Họ được tạo ra bởi trò chơi,
gọi là NPC (Non-player Character). 
Anh có thể nói chuyện với họ bình thường.

368
00:19:42,112-->00:19:44,513
Tuyệt thật...

368
00:19:45,112-->00:19:52,113
Vì không có dữ liệu về tất cả mọi người
ở Tengu nên những người đó không giống thật.

368
00:19:52,512-->00:19:57,113
Nhưng em nghĩ rằng ít nhất là hàng xóm 
đang được sao chép từ ký ức của anh.

368
00:19:58,112-->00:20:01,113
Nếu họ là hàng xóm mà anh biết... 
A, chào buổi chiều.

368
00:20:02,112-->00:20:04,513
Đó là người hàng xóm Saito-san
mà tôi vẫn chào hỏi như thường lệ.

368
00:20:05,512-->00:20:11,513
Ara, Shidou-kun nhà Itsuka. Đi mua đồ à? 
Vẫn suốt ngày làm việc nhà à?

368
00:20:12,512-->00:20:14,113
Không, không phải thế.

368
00:20:15,112-->00:20:18,113
Waa... Tuyệt thật. Giống y hệt Saitou-san.

368
00:20:21,512-->00:20:25,113
Tôi chợt nhớ rằng tôi muốn mua đồ cho bữa tối.
Vì vậy tôi đã đến khu mua sắm.

368
00:20:26,112-->00:20:29,113
Tất cả mọi thứ giống hệt như thật.
Như vậy là mình có thể mua đồ cho bữa tối.

368
00:20:31,113-->00:20:37,113
Anh nói mua đồ cho bữa ăn tối?
Anh định làm món gì vậy, Shidou?

368
00:20:38,112-->00:20:41,113
Waa! Tohka! Em đang liên lạc từ bên ngoài à?

368
00:20:42,113-->00:20:52,113
Ừ. Tất cả bọn em đang quan sát anh đấy, Shidou.
Anh ở đây có vẻ như đang ngủ, nhưng trên 
màn hình, anh đang đi bộ. Thật là kỳ lạ!

368
00:20:53,112-->00:20:58,113
Nghĩa là phòng quan sát truyền được
cả hình ảnh từ đây sao? Anh cảm thấy 
không thoải mái khi biết tất cả 
đang nhìn anh đâu.

368
00:20:59,112-->00:21:07,113
Hôm nay em muốn ăn menchi katsu! 
Shidou, mua katsu menchi đi! 
(Thịt viên giòn tan kiểu Nhật)

368
00:21:08,113-->00:21:11,113
Không, mua đồ ở đây làm sao mang về nhà được?

368
00:21:12,113-->00:21:14,113
Vậy còn mùi vị khi ăn ở đây thì sao?

368
00:21:15,112-->00:21:20,113
Nếu mùi vị quen thuộc lặp đi lặp lại,
anh có thể ăn thoải mái. 

368
00:21:20,512-->00:21:25,113
Bộ não cũng sẽ có được cảm giác no 
và xác nhận là "anh đã ăn".

368
00:21:26,112-->00:21:34,513
Tất nhiên anh không thể hấp thụ 
chất dinh dưỡng vì dạ dày anh chẳng có gì.

368
00:21:35,512-->00:21:37,113
Ra là vậy...

368
00:21:37,512-->00:21:40,113
Vừa đi bộ vừa nói chuyện thế này...

368
00:21:41,112-->00:21:43,113
Uida. Tôi xin lỗi...

26
00:21:44,512-->00:21:46,513
Cậu không sao chứ?

26
00:21:48,112-->00:21:55,513
Không sao đâu. Nó cũng giống như 
ngậm bánh mì chạy đến lớp,
tình cờ gặp một người quen thôi.

26
00:21:56,512-->00:22:00,113
Origami? Chờ đã, tớ không hiểu cậu đang nói gì...

26
00:22:01,112-->00:22:14,513
Tớ đang chạy đến lớp, miệng đang ngậm
bánh mì bữa sáng. Và nhờ định mệnh,
chúng ta đã va vào nhau.

26
00:22:15,512-->00:22:20,113
Chuyện như thế chỉ xảy ra trong phim thôi.
Với lại đó thường là 2 người yêu nhau.

26
00:22:21,112-->00:22:31,113
Phải, chúng ta là người yêu.
Người yêu cũng cần sự kích thích

26
00:22:32,112-->00:22:33,513
Hở...?

26
00:22:34,512-->00:22:37,113
Dù sao, mình có nói gì thì
cậu ta cũng không nghe đâu.

26
00:22:38,112-->00:22:48,113
Vì đây là cuộc gặp gỡ định mệnh.
Tớ không phiền nếu chúng ta
làm ngay ở đây và bây giờ đâu. 

26
00:22:49,112-->00:22:52,113
Không, cậu định làm gì?
Đây là chốn đông người giữa ban ngày đấy!

26
00:22:53,112-->00:23:00,513
Vậy à? Vậy tiếp tục kế hoạch ban đầu.
Tớ sẽ xem xét lại khi gặp nhau trong lớp học

26
00:23:01,512-->00:23:04,113
Chuyện gì sẽ xảy ra vậy...

27
00:23:05,112-->00:23:07,113
Tớ sẽ đợi cậu.

368
00:23:11,112-->00:23:14,113
Vừa rồi là chuyện gì vậy?

368
00:23:15,112-->00:23:20,113
Đó là sự kiện đấy. 
Đây là game hẹn hò mà, phải không?

368
00:23:21,112-->00:23:23,113
Sự kiện à?

368
00:23:24,112-->00:23:27,113
Đúng vậy, trong "My Little Shidou",
những sự kiện cũng hay xảy ra...

368
00:23:28,112-->00:23:31,113
Mặc dù có nhiều công nghệ cải tiến,
Nó vẫn theo quy luật thông thường.

368
00:23:32,112-->00:23:41,513
Dù sao đây chỉ là thử nghiệm. Khi anh sống 
trong thế giới trò chơi, sẽ có nhiều
sự kiện xuất hiện hơn.

368
00:23:42,512-->00:23:44,513
Chuyện đó khiến anh có cảm giác bất an.

368
00:23:45,513-->00:23:50,113
Im lặng đi. NPC cũng giống 
người thật phải không?

368
00:23:51,112-->00:23:55,513
Đúng vậy. Mặc dù có chút khác nhưng
họ hầu như giống hệt với người thật
dù sự kiện này đã được cài đặt trước.

368
00:23:56,512-->00:24:06,113
Để tạo ra các sự kiện, nhiều thiết lập 
được cài đặt khắp thế giới. Nhưng quan trọng 
vẫn là tính cách họ vẫn vậy phải không?

368
00:24:07,112-->00:24:10,113
Ừ, anh không cảm thấy gì lạ cả... 
Anh nghĩ vậy.

368
00:24:11,112-->00:24:21,113
Dù sao, chúng ta vẫn không thể nói 
nó hoàn hảo. Tạo ra cảm xúc, 
đặc biệt là tình yêu vô cùng khó khăn.

368
00:24:22,512-->00:24:26,513
Đó là vấn đề mà chúng ta sẽ làm.

368
00:24:27,512-->00:24:30,113
Giờ chúng ta đi học đi.

368
00:24:31,112-->00:24:34,113
Chúng ta vẫn tiếp tục à? Được rồi...

368
00:24:37,112-->00:24:41,513
Trường học cũng được sao chép sao? 
Dù biết đây là bản sao nhưng mình vẫn 
ngạc nhiên đây là trong trò chơi.

368
00:24:42,512-->00:24:50,113
Đây chỉ là một phần của Tengu.
Nó không nằm ngoài khu vực 
cậu nắm rõ đâu, Shin.

368
00:24:51,112-->00:24:53,113
Tất cả học sinh cũng vậy sao?

368
00:24:54,112-->00:25:00,113
Phải. Tất cả học sinh và giáo viên
trường Raizen đã được sao chép. 

368
00:25:00,512-->00:25:07,113
Tuy nhiên, những người mà cậu 
chưa bao giờ nói chuyện chỉ có hình ảnh thôi.

368
00:25:08,113-->00:25:10,113
Thật đáng kinh ngạc...
