﻿368
00:14:22,112 --> 00:14:23,113


368
00:14:32,912 --> 00:14:35,113
Hở? Đây là lớp học à?
Mình trở lại bình thường rồi.

368
00:14:35,912 --> 00:14:38,513
Chuyện quái gì vừa xảy ra vậy?

368
00:14:39,712 --> 00:14:50,113
Tôi đã xem phản ứng của Itsuka Shidou
trong một mối quan hệ 
và tình huống khác bình thường.

368
00:14:50,512 --> 00:14:53,113
Cậu nhìn thấy hết à?
Nhưng tại sao... nó...

368
00:14:54,112 --> 00:14:58,513
Nó giống như một ảo giác. 
Nghĩ lại, tôi thấy thật xấu hổ. 
Hơn nữa, chúng tôi còn bị Arusu nhìn thấy...

368
00:15:01,112 --> 00:15:07,513
Are? Anh ở đây sao? Tại sao chúng ta lại ở đây?

368
00:15:08,512 --> 00:15:11,113
Miku đấy à? Sao cậu lại ở đây?

368
00:15:12,112 --> 00:15:20,113
Em không chắc lắm.
Em cảm thấy vừa có chuyện gì đó rất thú vị.

なんだかよくわかりませんけど
素敵なことがあつたような気がします

368
00:15:20,712 --> 00:15:24,113
Không lẽ là... cậu cũng biết chuyện
cậu đột ngột đến nhà và tự nhận là hôn thê sao?

突然家に来て許嫁だとか言い出す　を
見たらとかじゃないだろうな？

368
00:15:25,512 --> 00:15:35,113
Darling, sao anh biết vậy? 
Không lẽ là do chúng ta bị trói buộc
bởi sợi dây số phận nên cùng nhìn thấy cảnh đó sao?

なんでわかるんですか？
もしかして、私たち運命の糸で結ばれてるからわかつちゃうとか？

368
00:15:35,912 --> 00:15:38,513
Không,　sao có chuyện đó được... haha.

368
00:15:39,112 --> 00:15:47,113
Nhưng nếu em là hôn thê thật 
thì em cũng không phiền đâu.

私的には、本当にだ一りんの許嫁でも
まあつたく問題ないですけどわ

368
00:15:48,112 --> 00:15:49,713
Vậy à...?

368
00:15:50,712 --> 00:15:58,113
Vậy em về trước nhé.
Darling, bye bye.

368
00:15:59,712 --> 00:16:04,313
Miku... cậu ấy không tỏ ra bất ngờ gì cả.
Đó là vì tính cách ấm áp hay vì cậu ấy là idol?

Miku... 意外に動じないんだな。
ほんわかしてる性格のせいなのか、Idolとして舞台に立つ者の　なのか

368
00:16:05,712 --> 00:16:08,513
Vậy chuyện gì đang xảy ra ở đây vậy?

368
00:16:09,512 --> 00:16:14,113
Đó là một phản ứng thú vị đấy, Itsuka Shidou.

104
00:16:14,912 --> 00:16:18,113
Vậy tình huống đó là do cậu làm à Arusu?

あれはおまえが決めシチュエーションなのか

105
00:16:18,912 --> 00:16:25,513
Phải. Mô phỏng việc tạo ra tình yêu 
trong một tình huống đặc biệt.

368
00:16:26,712 --> 00:16:30,113
Tạo ra tình yêu sao? 
Chuyện này sẽ còn xảy ra nữa à?

368
00:16:31,112 --> 00:16:39,113
Tôi muốn quan sát sự hình thành tình yêu với
Itsuka Shidou trong nhiều tình huống.

368
00:16:40,112 --> 00:16:49,913
May mắn là thế giới này có 
dữ liệu khá phong phú 
về các tình huống tạo ra tình yêu

368
00:16:50,713 --> 00:16:54,113
Cậu nói tớ mới nhớ. Trò chơi này 
có nhiều sự kiện để người chơi lựa chọn.

368
00:16:55,112 --> 00:17:03,513
Tôi muốn một tình huống đặc biệt
để có thể nhận được nhiều thông tin hơn.

368
00:17:03,912 --> 00:17:08,113
Vậy à? Cũng đúng. Dù sao hôm nay cũng
không khác các hoạt động thường ngày lắm.

368
00:17:08,912 --> 00:17:11,113
Mình có thể thấy tương lai đầy khó khăn đang chờ mình...

前途多難だ




368
00:17:16,112 --> 00:17:18,513
Thật là một ngày kinh khủng.

368
00:17:18,912 --> 00:17:23,513
Sau khi đã vật lộn với hiện tượng lạ rồi
đi mua đồ ăn tối, trời đã tối mịt.

368
00:17:24,112 --> 00:17:30,313
Arusu đã về thẳng nhà. Có vẻ cô ấy
không giám sát tôi 24/24.

368
00:17:30,712 --> 00:17:35,113
Mình phải về nhà nhanh.
Nếu Tohka chờ đợi với cái bụng đói 
thì đó là vấn đề lớn đấy.

368
00:17:35,912 --> 00:17:41,113
Tất cả chúng tôi sẽ ăn tối cùng nhau, 
cả Tohka và những cô gái khác.

368
00:17:41,512 --> 00:17:46,113
Phải. Khi tất cả đều đang trải qua 
một tình hình khó hiểu, ở cùng nhau là tốt nhất.

368
00:17:51,112 --> 00:14:55,113
Hở? Arusu phải không? 
Tớ tưởng cậu đã về nhà rồi...

368
00:17:55,912 --> 00:17:57,513
Này Arusu...

368
00:18:01,912 --> 00:18:05,113
Hở? Cô ấy đi đâu thế? Có chuyện gì vậy?

368
00:18:05,912 --> 00:18:09,113
Đó là Arusu phải không? Mặc dù cô ấy có vẻ khác...

368
00:18:09,512 --> 00:18:12,513
Mình thấy tò mò quá. Mình sẽ đuổi theo.

368
00:18:19,112 --> 00:18:21,113
Haa... Haa... Haa...

368
00:18:21,912 --> 00:18:25,113
Phù. Mang theo mấy cái túi đồ chạy mệt thật.

368
00:18:25,512 --> 00:18:27,513
Nơi này là... công viên phải không?





368
00:18:47,112 --> 00:18:52,113
Đúng như tôi đoán. Cậu đã theo tôi, Itsuka Shidou.

368
00:18:52,912 --> 00:18:56,513
Gì vậy? Có phải Arusu không?
Trông cậu có vẻ kỳ lạ lắm...

368
00:18:57,112 --> 00:19:06,113
Sẽ tốt hơn nếu không ngăn cản những sự kiện 
phải không? Trong thế giới này, 
chẳng có ai sống có mục đích cả.

368
00:19:06,712 --> 00:19:19,113
Nhưng... cậu đang ở đây.
Tại sao cậu đi theo tôi?
Cậu đang quan tâm đến tôi à?

368
00:19:20,112 --> 00:19:25,113
Cậu có phải là Arusu không? 
Cách nói chuyện của cậu... trông giống như 
một người khác hoàn toàn vậy.

368
00:19:26,112 --> 00:19:33,113
Tôi trông giống như một người khác sao?
Cậu ngạc nhiên à? Tôi giống người cậu thích à?

368
00:19:34,112 --> 00:19:37,113
Cậu thực sự là Arusu à?

368
00:19:37,512 --> 00:19:39,113
Đúng vậy đấy.

368
00:19:40,112 --> 00:19:42,713
Chắc chắn là vậy... cả khuôn mặt 
và giọng nói đều giống, nhưng...

368
00:19:43,112 --> 00:19:48,113
Tôi là Arusu.
Không nghi ngờ gì cả... Tôi là Arusu.

368
00:19:49,112 --> 00:19:51,513
Vậy... mọi chuyện là...

368
00:19:52,112 --> 00:20:02,113
Đúng vậy nhỉ... Tôi là Arusu.
Nhưng cũng có thể... tôi không phải là Arusu.

368
00:20:03,112 --> 00:20:07,113
Cậu là Arusu, nhưng không phải là Arusu?
Cậu là... một nhân cách khác?

368
00:20:07,712 --> 00:20:13,113
Dạng như... Yoshino và Yoshinon?
Nếu vậy, tôi có thể hiểu được sự khác biệt này.

368
00:20:14,112 --> 00:20:17,513
Ai mà biết được?

368
00:20:19,112 --> 00:20:28,513
Itsuka Shidou, 
Cậu sẽ làm gì trong thế giới này?
Làm sao để thực hiện nó?

368
00:20:29,112 --> 00:20:31,513
Tôi thực sự muốn biết đấy.

368
00:20:32,512 --> 00:20:39,513
Vì vậy, có rất nhiều điều phải lựa chọn...

368
00:20:39,912 --> 00:20:45,113
Và hãy cho tôi thấy
những gì cậu gọi là tình yêu.

368
00:20:47,112 --> 00:20:51,113
Các cậu đều giống nhau phải không?
Cậu và Arusu mà tôi biết .

368
00:20:51,512 --> 00:20:57,113
Được rồi, hôm nay chúng ta kết thúc tại đây.
Hẹn gặp lại nhé, Itsuka Shidou.

368
00:21:03,912 --> 00:21:06,113
Waaa! Chói mắt quá!

368
00:21:06,512 --> 00:21:08,113
Ánh sáng đó là gì? Hở? Arusu?

368
00:21:11,512 --> 00:21:14,513
Cô ấy biến mất rồi... 
Vừa xảy ra chuyện gì vậy?

368
00:21:15,512 --> 00:21:19,113
Đó có phải là Arusu không?
Mình nghĩ có thể lắm...

368
00:21:20,112 --> 00:21:22,113
Tốt nhất là mình nên về nhà.



368
00:00:02,912 --> 00:00:06,113
Mình mệt quá rồi. Mình chẳng hiểu 
mọi chuyện đang xảy ra ở đây nữa.

368
00:00:06,512 --> 00:00:08,113
Anh về rồi đây.

368
00:00:10,112 --> 00:00:12,113
Hở? Arusu? Cậu về rồi à?

368
00:00:12,512 --> 00:00:15,513
Vâng. Cậu cần gì à?

368
00:00:16,112 --> 00:00:19,113
Um... Cậu vừa đến công viên phải không?

368
00:00:20,112 --> 00:00:26,113
Không. Tôi đi thẳng từ trường
về nhà Itsuka.

368
00:00:26,912 --> 00:00:29,913
Sau đó cậu không ra ngoài suốt từ lúc đó?

368
00:00:30,112 --> 00:00:35,513
Vâng. Itsuka Shidou, ý cậu muốn hỏi gì vậy?

368
00:00:36,112 --> 00:00:38,113
Không, um...

368
00:00:38,912 --> 00:00:44,113
Tôi chắc chắn đó là Arusu...
Nhưng hoặc là cậu ấy không nhớ hoặc là nói dối.
Tiếp tục hỏi chắc cũng không có kết quả gì.

368
00:00:45,712 --> 00:00:53,513
Shidou! Lâu quá rồi đấy! 
Anh đã đi đâu thế? Em đang chết đói đây!

368
00:00:54,112 --> 00:00:57,913
À, Xin lỗi. Giờ anh làm ngay đây.
Em đợi anh thêm chút nữa nhé.

368
00:00:58,312 --> 00:01:00,513
Ừ! Được rồi!

368
00:01:01,912 --> 00:01:03,113
Itsuka Shidou?

368
00:01:03,912 --> 00:01:09,113
Không, Hãy quên chuyện vừa rồi đi.
Chắc tớ nhìn nhầm thôi.

368
00:01:09,912 --> 00:01:12,113
Được rồi.

368
00:01:15,112 --> 00:01:19,113
Có thể đó là một đầu mối 
để chiến thắng trò chơi chăng?
Nếu đó là NPC hướng dẫn thì có thể là vậy...

368
00:01:19,712 --> 00:01:24,713
Ngoài ra có thể... "Shiroi Arusu" (Arusu Trắng) 
và "Kuroi Arusu" (Arusu Đen).
Vai trò của họ tùy thuộc vào
nhân cách bị phân chia.

368
00:01:26,112 --> 00:01:29,513
Tôi chẳng thể nào hiểu được.
Có quá nhiều thứ kỳ lạ.

368
00:01:30,112 --> 00:01:32,513
Thôi để chuyện đó sang một bên đi.




368
00:01:38,112 --> 00:01:48,113
Em no rồi! Bữa ăn của 
Shidou làm luôn là ngon nhất!

368
00:01:48,712 --> 00:01:50,513
Cảm ơn em.

368
00:01:51,112 --> 00:02:00,113
Shidou, ăn xong tất cả chúng ta sẽ 
vào phòng anh để xem xét lại tình hình.

368
00:02:01,113 --> 00:02:03,513
Được rồi. Ăn xong chúng ta sẽ đi.



368
00:02:11,113 --> 00:02:18,513
Vậy trong ngày hôm nay, các cô đã
làm gì trong thế giới này?

368
00:02:21,112 --> 00:02:23,113
Tohka giơ tay với vẻ mặt đầy sức sống.

368
00:02:24,112 --> 00:02:26,113
Được rồi, Tohka. Nói đi.

368
00:02:27,112 --> 00:02:34,113
Thế giới này giống y như ngoài thật!
Cả bento lẫn bữa tối của Shidou đều rất ngon!

368
00:02:35,112 --> 00:02:37,113
Vậy à...

368
00:02:38,512 --> 00:02:43,113
Vâng, darling đã trở thành hôn phu của tôi!

368
00:02:43,912 --> 00:02:46,513
Khoan đã! Đừng nói những thứ gây hiểu lầm nữa!

解を生む言い方は止めろ

368
00:02:47,912 --> 00:02:51,513
Shidou. Cậu không được bỏ rơi tớ.

それは聞き捨てならない

368
00:02:52,512 --> 00:03:01,113
Shidou, rau muối chua là gì?
Đó có phải là món dưa muối được làm cẩn thận không?

いい菜漬けとは何だ？
厳選された素材でできた漬け物か？

368
00:03:02,112 --> 00:03:04,113
Thôi, đừng nói tào lao nữa. Nghe tớ đã.

違うんだ。話を聞いてくれ

368
00:03:05,112 --> 00:03:11,113
Đó giống như một giấc mơ, nhưng rõ ràng là 
do Arusu gây ra. Cô ấy nói là để xem tình yêu 
trong một tình huống đặc biệt...

368
00:03:12,112 --> 00:03:25,113
Ra là vậy. Nó giống như một sự kiện.
Quan sát tình yêu trong một tình huống đặc biệt?
Đúng, giống như một cuộc hẹn đặc biệt vậy.




368
00:03:26,112 --> 00:03:36,113
Rõ ràng Arusu có khả năng 
kiểm soát thế giới này.

368
00:03:38,112 --> 00:03:46,113
Tôi không chắc trong thế giới này 
tôi có thể dùng Gabriel được không 
nhưng nếu tôi thử thì sao?

368
00:03:47,112 --> 00:03:56,113
Không, tốt hơn là không nên làm điều đó. 
Đối thủ là Tinh Linh Nhân Tạo đấy.

368
00:03:57,112 --> 00:04:09,113
Hơn nữa, chúng ta vẫn đang kẹt trong
thế giới này. Nếu chúng ta làm gì bất cẩn,
không biết điều gì sẽ xảy ra đâu.

368
00:04:09,513 --> 00:04:17,113
Cô nói đúng. Tôi thấy đó là 
một quyết định khôn ngoan đấy, Tinh Linh Lửa.

368
00:04:18,112 --> 00:04:22,113
Tôi không thích bị gọi thế đâu đấy.

368
00:04:23,112 --> 00:04:35,513
Giờ không còn thời gian để nói chuyện nữa.
Từ ngày mai, chúng ta sẽ tiếp tục điều tra 
và quan sát cả thế giới này và Arusu.

368
00:04:36,512 --> 00:04:43,513
Đúng vậy. Hiện giờ chúng ta không có 
sự lựa chọn nào khác.

368
00:04:39,712 --> 00:04:44,113
Được rồi, giải tán.
Mọi người, đừng mất cảnh giác đấy.





368
00:04:49,112 --> 00:04:54,113
Hôm nay tôi đã có một trải nghiệm kỳ lạ. 
Kinh nghiệm thực tế đó cũng khiến tôi kiệt sức.
Vì vậy, tôi cần phải nghỉ ngơi, nhưng...

368
00:04:54,512 --> 00:04:56,113
Haaa...

368
00:04:57,112 --> 00:05:00,113
Anh sao vậy Shidou?

368
00:05:01,112 --> 00:05:03,513
À, anh đang tự hỏi là bây giờ anh phải làm gì...

368
00:05:04,112 --> 00:05:11,513
Vâng. Giờ chúng ta không biết 
nên làm gì trong thế giới này.

368
00:05:12,512 --> 00:05:17,113
Ara, mọi người vẫn tụ tập ở đây à.

368
00:05:18,112 --> 00:05:22,113
Ừ. Nói sao đây nhỉ. 
Đây giống như buổi tụ tập của con gái thì đúng hơn

完全に女子会の様相を呈しているんだが

368
00:05:22,512 --> 00:05:26,113
Và tôi nghĩ tôi sẽ được nghỉ ngơi...
Nhưng Arusu...

368
00:05:31,712 --> 00:05:34,113
Arusu, cậu đứng đó nãy giờ à?

368
00:05:35,112 --> 00:05:40,113
Vâng. Tôi đứng xem từ nãy đến giờ.

368
00:05:41,112 --> 00:05:44,113
Arusu, cậu thực sự làm việc đó sao?

368
00:05:45,112 --> 00:05:50,113
Vâng. Tôi đã thực hiện một mô phỏng 
một tình huống đặc biệt 
mà trong thực tế không hề xảy ra. 

368
00:05:51,112 --> 00:06:00,113
Tôi đã thu thập một số dữ liệu thú vị.
Tuy nhiên... các thông tin liên quan 
đến tình yêu vẫn chưa đủ.

368
00:06:01,112 --> 00:06:04,113
Trông cậu có vẻ hài lòng lắm mà.

368
00:06:05,112 --> 00:06:12,113
Tôi chỉ vui vì được nhìn thấy cách các cậu 
phản ứng trong tình huống đặc biệt thôi.

368
00:06:13,112 --> 00:06:15,113
Tớ đoán nó sẽ còn tiếp diễn phải không?

368
00:06:16,112 --> 00:06:22,113
Vâng. Trong tình hình hiện giờ, đây là lúc
thích hợp để thu thập thông tin.

368
00:06:23,112 --> 00:06:25,113
V... vậy à?

368
00:06:26,112 --> 00:06:29,113
Tôi thắc mắc tình huống như thế nào
thì có thể dễ dàng phát triển tình yêu.

368
00:06:29,712 --> 00:06:31,113
Hiện giờ mình không có ý tưởng gì cả.
Mình phải hỏi ý kiến ai đó.

368
00:06:31,912 --> 00:06:35,513
Nhưng không thể hỏi tất cả mọi người 
trong căn phòng nhỏ này được. 
Mình sẽ đi gặp ai đó để hỏi.




368
00:06:36,512 --> 00:06:38,113
Lựa chọn này sẽ không có nhân vật
vừa xảy ra sự kiện.

368
00:06:38,513 --> 00:06:41,113
1) Nói chuyện với Tohka
2) Nói chuyện với Origami
3) Nói chuyện với Yoshino
4) Tiếp theo

368
00:06:41,513 --> 00:06:45,513
1) Nói chuyện với Kurumi
2) Nói chuyện với Kotori
3) Nói chuyện với chị em Yamai
4) Trở lại

368
00:06:47,912 --> 00:06:50,113
Thật khó để trả lời câu hỏi "Tình yêu là gì?"

愛つてなんなんだろうな

368
00:06:50,712 --> 00:06:56,513
Ara, Shidou-san... trông cậu suy nghĩ
chăm chú quá đấy.

随分と深いことを考えてますわね

368
00:06:57,713 --> 00:07:01,113
À không... tớ không biết...
làm sao để thoát ra khỏi thế giới này.

それがわからないと、　この世界から出られないみたいだしな

368
00:07:01,712 --> 00:07:04,113
Kurumi... Nếu là cậu, cậu cần gì
ở một người bạn trai?

えばだけど。。。　Kurumiは彼氏ができたら彼氏に何を望むんだ？

368
00:07:04,912 --> 00:07:10,113
Ara, ý cậu là tớ muốn gì
ở một món ăn sao?

随分とデリカシ-のないことをお聞きになりますのね？

368
00:07:11,112 --> 00:07:13,113
À không... xin lỗi.

368
00:07:14,112 --> 00:07:24,113
Không sao đâu. Nhưng nếu cậu hỏi thì...
tớ không thích bị ràng buộc quá nhiều.

構いませんわ。　でも、そうですわね。
あまり束縛されすぎるのは好きではありませんわ

368
00:07:24,712 --> 00:07:27,113
Tớ hiểu rồi. Tớ cũng nghĩ như cậu.

なるほどな。　なんかKurumiらしい気がするよ

368
00:07:27,712 --> 00:07:30,113
Cậu... đang nghĩ về tớ sao?

わたくしらしい。。。ですか？

368
00:07:31,112 --> 00:07:33,113
Không, thật sai lầm khi hỏi cậu.

神出鬼没なKurumiらしいなつて

368
00:07:34,112 --> 00:07:39,513
Tớ không nghĩ cậu lại nghĩ tớ như vậy đấy.

Shidou-sanはわたくしをそんな風に思われてますのね

368
00:07:40,112 --> 00:07:42,113
Không, nhưng mà... có đúng vậy không?

いやほら。。。事実だろ？

368
00:07:43,112 --> 00:07:48,113
Vậy cứ thế đi nhé.

そういうことにしておきましょう





368
00:07:53,112 --> 00:07:57,313
Cuối cùng, tất cả họ đều ra khỏi phòng của tôi.
Và tôi... đang dọn dẹp phòng khách.

368
00:07:57,712 --> 00:08:01,113
Darling, để em dọn phụ nhé.

お片づけ手伝いましょうか

368
00:08:02,112 --> 00:08:04,113
À... không cần đâu, tớ dọn cũng sắp xong rồi.

そぐ終わるから大丈夫だぞ

368
00:08:04,912 --> 00:08:11,113
Tiếc thật đấy...
Em muốn có cảm giác làm hôn thê thêm nữa...

また許嫁気分を味わおうと思つたのに

368
00:08:12,112 --> 00:08:14,113
Vậy... vậy à... Thế thì... tiếc thật.

368
00:08:15,112 --> 00:08:20,113
Đúng vậy. Em rất muốn được làm
vợ darling thêm lần nữa đấy.

せつかくですから
まただ　りんの許嫁になりたいです

368
00:08:21,112 --> 00:08:26,113
Chuyện đó thì có khác gì đâu.
Chẳng phải lúc nào cậu cũng muốn vậy sao?

それはどうだろうな。
或守はまたやるつもりみたいだけど。
同じツチュエーツヨンが来るとは限らないんじゃないか？

368
00:08:26,712 --> 00:08:30,113
Ý tôi là, cô ấy lúc nào cũng cuồng nhiệt như vậy.
Không hiểu sao cô ấy không thấy mệt nhỉ.

ていうか、　そうあつてほしい。
何ていうか、あれは気が体まらん気する

368
00:08:30,912 --> 00:08:36,113
Vậy à? Vậy thì lần này em muốn mặc váy cưới.

じゃあ私、　今度はウエデイングドレスが着てみたいです

368
00:08:37,112 --> 00:08:40,113
Đó không phải lý do chính phải không?

いう趣旨じゃないからな、これ？

368
00:08:40,912 --> 00:08:48,513
Em biết rồi mà.
Nhưng dù sao, anh nên vui vẻ lên.

でもどうせなら楽しい方がいいじゃないですか

368
00:08:49,512 --> 00:08:52,113
Điều này thì đúng rồi đấy.
Miku, cậu luôn tích cực nhỉ.

確かにそうかもしれない。
Mikuは、ポジテイブだな

368
00:08:52,912 --> 00:08:58,113
Vâng, bởi vì Idol luôn là người
khiến mọi người vui vẻ và tràn đầy năng lượng mà.

みんなを明るく元気にするのがアイドルですから

368
00:08:58,912 --> 00:09:00,113
Ừ, thế là tốt.



368
00:09:06,512 --> 00:09:07,513
Vậy là cuộc họp đã xong rồi à?

368
00:09:08,112 --> 00:09:12,313
Ừ. Đã trễ rồi. Mọi người đã giải tán.

368
00:09:12,712 --> 00:09:16,113
Giờ này cũng muộn rồi. 
Họ về căn hộ bên cạnh cả rồi phải không?

368
00:09:17,112 --> 00:09:21,113
Em nghĩ tốt nhất là mọi người ở gần nhau
trong khi chúng ta ở trong thế giới này.

368
00:09:21,512 --> 00:09:28,113
Vì vậy em cho họ sang ở căn hộ dành cho
Tinh Linh. Em cũng đã nói điều này rồi mà.

368
00:09:28,512 --> 00:09:30,113
Anh hiểu rồi. Như thế cũng tốt, nhưng...

368
00:09:30,912 --> 00:09:33,513
Vậy cũng hơi bất ngờ. 
Tất cả Tinh Linh cùng ở lại đây à. 

368
00:09:34,312 --> 00:09:37,113
À, em quên nói với anh.

368
00:09:37,512 --> 00:09:39,113
Gì vậy?

368
00:09:39,512 --> 00:09:47,113
Chỉ là lúc nãy họp trong phòng anh,
phòng của anh có hơi lộn xộn một chút.

368
00:09:48,112 --> 00:09:50,113
Em dọn giùm anh được không?

368
00:09:50,512 --> 00:09:55,113
Không. Anh đi đi.
Phòng anh giờ bừa bộn lắm đấy. 

368
00:09:55,512 --> 00:09:57,113
Ờ... ừ...

368
00:09:57,513 --> 00:10:00,113
Tôi lại thêm việc nữa rồi.




368
00:10:05,112 --> 00:10:07,113
Hôm nay thật sự là một ngày dài.
Quá nhiều việc xảy ra.

368
00:10:08,112 --> 00:10:11,113
Phù... làn gió đêm thật tuyệt.

368
00:10:11,712 --> 00:10:15,113
Itsuka Shidou. Cậu đang làm gì vậy?

368
00:10:16,112 --> 00:10:19,113
Arusu? À, tớ chỉ hít thở không khí thôi.
Và cũng... ngắm nhìn các ngôi sao.

368
00:10:20,112 --> 00:10:29,113
Ngôi sao... những ngôi sao và các hành tinh 
trong hệ mặt trời. Cậu đang quan sát 
các thiên thể à?

368
00:10:29,912 --> 00:10:33,513
Phải. Đúng là vậy, nhưng cũng không hẳn.
Chỉ là tớ nghĩ trong thế giới này,
ngay cả những ngôi sao cũng rất đẹp.

368
00:10:34,112 --> 00:10:42,113
Đẹp sao? Tôi không hiểu điều đó lắm.

368
00:10:42,512 --> 00:10:46,113
Vậy à. Cũng tốt. Đừng nghĩ quá nhiều.
Đến một lúc nào đó cậu sẽ hiểu.

368
00:10:47,112 --> 00:10:52,113
Chúng... đang tỏa sáng

368
00:10:53,112 --> 00:10:55,113
Phải.

368
00:10:56,112 --> 00:11:01,113
Có những ngôi sao đang tỏa sáng
và những ngôi sao không sáng nhiều.

368
00:11:02,112 --> 00:11:05,113
Cậu nói đúng.
Những ngôi sao sáng lấp lánh là vì
chúng rực rỡ lớn hơn hoặc ở gần hơn.

368
00:11:06,112 --> 00:11:09,513
Những ngôi sao sáng đó là những hành tinh 
gần với Trái đất, chẳng hạn như sao Hỏa hay sao Mộc.



368
00:11:10,112 --> 00:11:17,513
Vâng. Tôi biết tên những ngôi sao đó.
Tôi biết nhờ các thông tin được lập trình sẵn.

368
00:11:18,512 --> 00:11:22,113
Vậy à. Cậu nói đúng. Thật buồn cười
khi nói điều mà cậu đã biết.

368
00:11:23,112 --> 00:11:24,113
Không.

368
00:11:26,112 --> 00:11:35,113
Tôi không biết liệu chúng có đẹp hay không.

368
00:11:36,112 --> 00:11:38,113
Cậu đang ngắm nhìn chúng à?

368
00:11:38,712 --> 00:11:48,113
Tôi không biết. Đó là một cảm giác... 
không rõ nhưng rất dễ chịu.

368
00:11:49,112 --> 00:11:52,113
Tớ hiểu rồi. Nếu vậy thì tớ rất vui.

368
00:11:53,112 --> 00:11:58,113
Itsuka Shidou, cậu biết nhiều thứ nhỉ.

368
00:11:59,112 --> 00:12:03,113
Vậy à? Nếu là về kiến thức thì 
cậu biết nhiều hơn tớ đấy Arusu.
Vì cậu biết mọi thứ về thế giới này,
mà thế giới này giống hệt thế giới thực.

368
00:12:04,113 --> 00:12:10,113
Nhưng... vẫn còn nhiều thứ tôi không biết.

368
00:12:11,113 --> 00:12:13,113
Tớ cũng vậy. Có nhiều thứ tớ cũng không biết.

368
00:12:14,113 --> 00:12:20,113
Nhưng Itsuka Shidou, tôi chắc cậu biết tình yêu.

368
00:12:20,513 --> 00:12:22,113
Tớ biết về tình yêu à?

368
00:12:22,513 --> 00:12:28,113
Chính vì vậy, tôi muốn cậu chỉ cho tôi tình yêu.

368
00:12:29,113 --> 00:12:33,113
Ừ. Tớ không biết có thể giúp gì được. 
Nhưng tớ sẽ giúp cậu tìm câu trả lời 
cậu đang tìm kiếm, Arusu.

368
00:12:33,513 --> 00:12:35,113
Vâng.

368
00:12:43,112 --> 00:12:46,113
Ngày thứ 2
