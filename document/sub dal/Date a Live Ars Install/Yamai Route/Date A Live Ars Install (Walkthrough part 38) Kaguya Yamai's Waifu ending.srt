﻿0
00:00:00,512-->00:00:03,513
1) Tôi muốn gặp Kaguya.
2) Tôi muốn gặp Yuzuru.

1
00:00:05,112-->00:00:08,113
Tôi quyết định sẽ gặp Kaguya.

2
00:00:11,112-->00:00:14,513
Giờ mình nên làm gì đây?

3
00:00:15,512-->00:00:20,113
Có ai đến nhà à? 
Được rồi, tớ ra đây.

4
00:00:27,112-->00:00:32,113
Tôi đến rồi đây, Shidou.

5
00:00:33,112-->00:00:35,113
Ừ. Chào cậu, Kaguya.

6
00:00:37,512-->00:00:44,113
Tôi có thể vào nhà chứ...?

7
00:00:45,112-->00:00:48,113
Sao vậy? Đâu cần phải khách sáo thế.
Vào nhà đi.

8
00:00:49,112-->00:00:55,113
Phải rồi. Đâu cần phải khách sáo nhỉ...

9
00:00:58,512-->00:01:03,113
Nhưng có cần phải vào nhà tớ không? 
Nếu cậu muốn, chúng ta có thể đi đâu đó.

10
00:01:04,113-->00:01:13,113
Không cần đâu! Ở nhà cậu cũng được mà. 
Không cần đi đâu cả đâu.

14
00:01:14,113-->00:01:17,113
Vậy sao? Nếu vậy thì tốt. Kaguya?

15
00:01:18,112-->00:01:24,113
Có gì sao, Shidou?

16
00:01:25,112-->00:01:29,113
Từ khi cậu vào, trông cậu lo lắng 
và cư xử lạ lắm. Có chuyện gì sao?

17
00:01:30,112-->00:01:42,513
Chỉ là Yuzuru không có ở đây. 
Chỉ có mình tôi với Shidou... 
Đã lâu rồi... chúng ta không thế này...

18
00:01:43,513-->00:01:47,113
À vậy à? Tóm lại là cậu lo vì chúng ta
ở một mình với nhau phải không?

19
00:01:48,112-->00:01:55,113
Đừng để ý những gì tôi nói
Tôi không lo lắng hay gì đâu!

20
00:01:56,112-->00:01:58,513
Có sao đâu. Tớ thấy bình thường mà.

21
00:01:59,512-->00:02:02,113
Vậy... vậy sao?

22
00:02:03,112-->00:02:05,113
Ừ. Đúng vậy đấy.

23
00:02:06,113-->00:02:08,513
Dù nói vậy, tôi vẫn thấy rất bối rối. 
Tôi không thể trấn an bản thân được.

25
00:02:09,512-->00:02:12,113
À, tớ sẽ lấy nước. Cậu uống trà không?

25
00:02:13,112-->00:02:15,513
À, ừ.

25
00:02:16,512-->00:02:18,513
Được rồi.

25
00:02:23,112-->00:02:25,113
Nhưng giờ làm gì nhỉ?

25
00:02:30,512-->00:02:33,113
Cô ấy nhìn tôi chăm chú.
Tôi không thể bình tĩnh được.

25
00:02:34,112-->00:02:36,113
Này... Có chuyện gì không, Kaguya?

25
00:02:37,112-->00:02:46,513
Đừng lo, Shidou! Cậu chỉ cần 
phục vụ trà trong im lặng thôi.

25
00:02:47,512-->00:02:50,113
Vậy... vậy à? Ừ, nếu cậu nói thế.

25
00:02:59,112-->00:03:03,113
Thật lạ lùng... Không, không sao. 
Tôi sẽ cố gắng hành động bình thường.

25
00:03:07,512-->00:03:10,113
Cậu uống đi. Cũng chẳng đặc biệt gì
nhưng cậu sẽ thích nó.

25
00:03:11,112-->00:03:18,113
Cậu pha ngon lắm. Um... rất tinh tế.

25
00:03:19,112-->00:03:21,113
Tớ mừng vì cậu thích nó.

26
00:03:22,112-->00:03:24,113
Tôi cũng nhấp một ngụm trà.
Cuối cùng tôi đã cảm thấy bình tĩnh lại.

26
00:03:25,112-->00:03:28,513
Có lẽ vì chúng tôi đã quen việc có 3 người.
Thời gian như dừng lại 
và chúng tôi chẳng biết nói gì.

26
00:03:29,512-->00:03:32,513
Này Shidou...

26
00:03:33,512-->00:03:35,513
Gì vậy?

26
00:03:36,512-->00:03:40,113
Tôi có thể ngồi gần cậu chứ?

26
00:03:41,112-->00:03:43,113
Không vấn đề gì.

26
00:03:44,112-->00:03:50,113
Tuyệt quá! Giờ tôi ngồi đây nhé?


26
00:03:51,113-->00:03:52,113
Ừ... ừ...

26
00:03:53,113-->00:03:56,113
Cậu muốn ngồi bên cạnh tớ suốt cả buổi sao?

26
00:03:57,112-->00:04:00,113
Này Kaguya, sao hôm nay cậu lại đến nhà tớ vậy?

32
00:04:01,113-->00:04:11,113
Chỉ đơn giản là tôi muốn gặp cậu thôi.
Tôi muốn cậu tràn ngập tâm trí về tôi.

33
00:04:12,112-->00:04:14,113
Ra là vậy.

34
00:04:15,112-->00:04:18,113
Đúng vậy. Lâu lâu thay đổi không khí
yên tĩnh một chút cũng tốt mà.

38
00:04:19,112-->00:04:25,113
Sh... Shidou! Nó! Là nó phải không?

39
00:04:26,112-->00:04:29,113
Cậu nói máy chơi game console à?
Cậu biết nó sao?

40
00:04:30,113-->00:04:41,113
Tôi biết chứ! 
Nhưng tôi chưa bao giờ chạm vào nó cả.

41
00:04:41,513-->00:04:47,113
Đó là thứ mà ngay cả quỷ thần 
cũng phải thèm muốn đấy!

42
00:04:48,113-->00:04:52,113
Cô ấy thích đến vậy sao?
Trông cô ấy như một đứa trẻ vậy.

43
00:04:53,113-->00:04:55,113
Cậu muốn chơi không, Kaguya?

39
00:04:56,112-->00:04:59,113
Muốn chứ! Muốn chứ!

40
00:05:00,113-->00:05:02,513
Vậy để tớ mở máy. 
Chúng ta chơi game đối kháng nhé.

41
00:05:03,513-->00:05:16,113
Hoàn hảo. Đây sẽ là dịp trả thù 
cho lần thua ở Game Center. 
Cậu sẽ biết sức mạnh thực sự của tôi.

48
00:05:20,112-->00:05:24,113
Giờ là chọn nhân vật.
Cậu chọn ai, Kaguya?

49
00:05:25,112-->00:05:34,513
Con này. Trông nó giống như một con gấu.
Nó sẽ bóp nát óc cậu.

50
00:05:35,513-->00:05:38,513
Cậu lại chọn nhân vật trông cứng rắn à?
Tớ nghĩ cậu nên chọn một nhân vật nữ.

51
00:05:39,512-->00:05:47,513
Tôi không thích cô ta.
Còn cậu thì sao, Shidou?

52
00:05:48,512-->00:05:50,513
Um... Tớ sẽ chọn nhân vật này!

53
00:05:51,512-->00:06:01,513
Trông nó xuất hiện như chiến binh thực thụ vậy! 
Nhưng đầu của nó trông lạ nhỉ?
Nó trông giống như...

54
00:06:02,512-->00:06:06,113
Khoan đã! Đừng đánh giá bề ngoài.
Hãy xem sức mạnh của nó đi.

41
00:06:07,112-->00:06:14,113
Vậy giờ hãy xem đây! Lên đi!

42
00:06:17,512-->00:06:22,113
Trận chiến bắt đầu. Nhưng tôi không thấy 
đây là trận đánh nghiêm túc.

43
00:06:23,512-->00:06:30,113
Chết tiệt! Shidou, tôi chưa biết chơi...

44
00:06:31,112-->00:06:36,113
Tớ sẽ chỉ cậu. 
Nghe này, cậu phải đánh như thế, hiểu không?

45
00:06:39,512-->00:06:44,513
Chết này! Đỡ này!

46
00:06:45,512-->00:06:48,113
Ơ... này! Cậu biết chơi mà!

47
00:06:49,113-->00:06:57,113
Lừa đối thủ mất cảnh giác đấy.
Cậu có thể nguyền rủa sự ngây thơ của cậu.

48
00:06:58,112-->00:07:01,113
Hay lắm. Tớ sẽ cho cậu khóc!

49
00:07:03,112-->00:07:12,113
Con gấu của tôi...
Tôi sẽ không để yên đâu! Đá, đá!

58
00:07:14,112-->00:07:16,113
Này... Tớ không thể tiếp cận.

59
00:07:18,112-->00:07:22,113
Tôi làm được rồi! Tôi thắng rồi!

60
00:07:23,112-->00:07:25,113
Tớ thua rồi...

61
00:07:26,112-->00:07:37,512
Mối thù của chiến binh đã được trả.
Đây là hương vị ngọt ngào của chiến thắng.
Tôi không nhớ những thất bại 
mà tôi phải chịu đựng nữa!

62
00:07:38,512-->00:07:42,113
Cô ấy thưởng thức chiến thắng như một cô bé.
Nếu tôi được thấy một Kaguya hạnh phúc
thì trận thua này không phải quá tệ.

63
00:07:43,112-->00:07:50,513
Shidou, thêm trận nữa!
Lần này tôi sẽ nghiền nát cậu hoàn toàn,

64
00:07:51,512-->00:07:53,113
Được! Lần này tớ không thua đâu!

65
00:07:54,112-->00:08:01,113
Tôi đã bắt đầu quen với trò này rồi.
Tôi không thua đâu, Shidou!

65
00:08:01,513-->00:08:07,113
Nếu cậu muốn, cậu có thể đặt cược 
bữa trưa ngày mai.

66
00:08:08,113-->00:08:11,113
Cậu muốn thế à? Xin lỗi nhé, nhưng cậu sẽ phải
đãi tớ một bữa no bụng ở trường đấy!

82
00:08:12,112-->00:08:18,113
Ngây thơ quá! Cậu hãy chuẩn bị
mua tất cả bánh mì đi, Shidou.

82
00:08:18,512-->00:08:22,513
Dù chỗ đó có đông đến mấy cũng phải mua đấy.
(Trong Yamai Lunch Time, chỗ bán bánh mì 
là nơi học sinh đến mua rất đông.)

83
00:08:23,512-->00:08:25,113
Được!

84
00:08:26,112-->00:08:29,113
Và như vậy, hai chúng tôi chơi đến khi trời tối.

85
00:08:34,512-->00:08:37,113
Muộn rồi sao? Chúng ta dừng lại thôi.

86
00:08:38,112-->00:08:46,112
Tiếc quá nhỉ? Tôi muốn được chơi nữa.

87
00:08:47,112-->00:08:50,113
Ngày mai chúng ta sẽ chơi tiếp mà.

88
00:08:51,112-->00:08:56,513
Cảm ơn nhé Shidou.
Hôm nay vui thật, phải không?

89
00:08:57,513-->00:08:59,113
Ừ!

90
00:09:00,113-->00:09:05,113
Tôi rất mong chờ cuộc hẹn
tiếp theo của chúng ta đấy.

91
00:09:06,113-->00:09:08,113
Đúng vậy...

92
00:09:08,513-->00:09:21,113
1) Chúng ta sẽ vui vẻ cùng nhau.
2) Lần kế tiếp hãy tham gia cùng Yuzuru.

98
00:09:22,512-->00:09:24,513
Lần kế chúng ta sẽ vui vẻ cùng nhau.

99
00:09:25,512-->00:09:34,113
Được rồi... Nhưng chúng ta 
không được quên Yuzuru đâu đấy.

100
00:09:35,112-->00:09:39,113
Hở? À, tớ không có ý đó... 
Ý tớ là chúng ta nên có chút 
thời gian riêng như hôm nay.

101
00:09:40,112-->00:09:43,113
Hôm nay đúng là vui thật.
Không cần thiết phải đủ 3 người, phải không?

102
00:09:44,112-->00:09:48,513
Cậu nghĩ thế à?

103
00:09:49,113-->00:09:55,513
Nhưng nếu cậu mời tôi, tôi sẽ đồng ý thôi.

105
00:09:56,512-->00:09:58,513
Hở?

106
00:09:59,512-->00:10:05,113
Không có gì đâu! 
Chào nhé. Cảm ơn vì ngày hôm nay!

107
00:10:06,512-->00:10:08,113
A, Kaguya!

108
00:10:09,113-->00:10:13,113
Lúc đó, tôi đã không nghĩ kỹ hơn.
Tôi chỉ nghĩ nếu như hẹn hò mà không có Yuzuru...
Khoan đã, không được!

113
00:10:19,512-->00:10:22,113
Nói chuyện với Kaguya và Yuzuru sao?
Nói gì bây giờ chứ?

114
00:10:23,113-->00:10:27,113
Tôi đi lên sân thượng vì một tin nhắn của
Kaguya và Yuzuru muốn nói chuyện gì đó quan trọng. 
Tin nhắn chỉ ghi là: "Gặp bọn tớ đi. Bọn tớ 
muốn nói chuyện. Bọn tớ đang đợi trên sân thượng"

1
00:10:28,112-->00:10:31,113
Mình có thể đoán được họ muốn nói chuyện gì...

1
00:10:33,512-->00:10:37,113
Chúng tôi đang đợi cậu đây, Shidou.

1
00:10:38,112-->00:10:43,513
Chờ đợi. Bọn tớ đang đợi cậu đây, Shidou.

1
00:10:44,512-->00:10:46,513
Ừ. Xin lỗi đã để các cậu đợi.
Các cậu muốn nói chuyện gì?

1
00:10:47,513-->00:10:52,113
Sau khi họ nhìn nhau với ánh mắt ngờ vực,
Tôi nghĩ tình hình này sẽ kéo dài. 
Nhưng Kaguya bước đến phía tôi.

1
00:10:53,112-->00:11:04,513
Sau buổi hẹn hôm qua, tôi nhận thấy...
Thật lòng, tôi rất vui khi cả 3 chúng ta 
chơi cùng nhau, Shidou, Yuzuru, nhưng...

1
00:11:05,513-->00:11:10,113
Shidou, tôi nhận ra rằng tôi thực sự muốn 
2 chúng ta luôn ở bên nhau.

1
00:11:10,513-->00:11:22,113
3 người cũng tốt nhưng cuối cùng,
tôi muốn cậu phải lựa chọn.

368
00:11:23,112-->00:11:33,513
Độc quyền. Khi Kaguya nói với tớ, 
tớ đã rất ngạc nhiên.
Nhưng tớ cũng cảm thấy vậy. 

368
00:11:34,112-->00:11:43,513
Tớ cũng muốn Shidou là của riêng tớ 
như một người bạn trai.

68
00:11:44,513-->00:11:48,113
Tôi biết chuyện này sẽ xảy ra.
Nhưng đây không phải là trò chơi, 
mà là kết quả của những buổi hẹn hò nghiêm túc.

68
00:11:49,113-->00:11:53,113
Nhìn thấy vẻ mặt nghiêm trọng của họ,
Tôi hiểu rằng đây là vấn đề nghiêm túc.
Cả Kaguya và Yuzuru đầy nghi ngờ. 
Vì vậy, việc chọn cả 2 là chuyện khó đạt được.

368
00:11:54,112-->00:11:57,513
Tôi nghĩ đã đến lúc tôi phải 
đối mặt một cách nghiêm túc.
Tôi sẽ không trả lời tránh né nữa.

368
00:11:58,512-->00:12:02,513
Xin lỗi. Có lẽ tớ không phải là 
một đứa con trai tốt.
Tớ không thể làm hài lòng các cậu.

368
00:12:03,513-->00:12:11,113
Không phải thế! Chỉ là bọn tôi muốn vậy thôi!

368
00:12:12,113-->00:12:23,113
Yêu cầu. Tuy nhiên, chúng tớ muốn Shidou 
chọn một trong hai chúng tớ...

368
00:12:25,512-->00:12:27,513
Này... 2 cậu...? Chờ chút đã...!

368
00:12:28,512-->00:12:42,113
Nào Shidou, hãy cảm nhận đi. Cả 2 chúng tôi 
đều có trái tim đang đập mạnh phải không? 
Đó là tình yêu sâu sắc nhất dành cho cậu đấy.

368
00:12:43,112-->00:12:55,113
Khẳng định. Đó là tình yêu dành cho cậu.
Nếu là cậu, Shidou, cậu sẽ cảm nhận được.

368
00:12:56,112-->00:12:58,513
Cả 2 ấn tay tôi vào ngực họ. 
Tôi bắt đầu cảm thấy nhịp đập của họ.

368
00:12:59,513-->00:13:02,513
Không, đó có thể là âm thanh của trái tim tôi.
Thế này quá đột ngột, Tôi không biết 
chuyện gì đang xảy ra cả...

368
00:13:03,512-->00:13:14,513
Hội đồng. Shidou, không cần phải lựa chọn 
người có nhịp đập lớn hơn. 
Cậu chọn người có ngực lớn nhất cũng được.

368
00:13:15,512-->00:13:25,113
Thật không công bằng! 
Nếu vậy thì cô thắng rồi, Yuzuru!
Shidou, nhỏ cũng không sao đâu.
Có lẽ nó đang phát triển thôi.

368
00:13:26,112-->00:13:38,113
Bác bỏ. Tôi nghĩ chọn ngực lớn ngay từ đầu
sẽ tốt hơn. Ngoài ra, cú sốc khi cô
thất vọng rất tuyệt vời.

368
00:13:39,112-->00:13:48,513
Không phải ngực nhỏ là hết hy vọng đâu!
Người ta nói rằng kích cỡ 
không quan trọng. Đúng không Shidou?

368
00:13:49,513-->00:13:52,513
Không, Kaguya, đủ rồi...
Cho tớ nói đã! Tớ chẳng suy nghĩ được gì cả!

368
00:13:53,513-->00:14:00,113
Tôi sẽ không bỏ tay ra đâu.
Tôi sẽ đợi cho đến khi cậu chọn, Shidou.

368
00:14:01,113-->00:14:09,113
Yêu cầu. Shidou, hãy nói cảm nhận của cậu đi.

368
00:14:10,212-->00:14:12,113
Được rồi. Tớ sẽ suy nghĩ cẩn thận.

368
00:14:13,113-->00:14:17,113
Tôi phải làm gì đó để bình tĩnh lại
và tập trung vào nhịp tim của họ
để biết nên chọn ai.

368
00:14:22,512-->00:14:26,113
Tớ cảm thấy nhịp tim Kaguya đập nhanh hơn.

368
00:14:27,112-->00:14:30,113
Shidou, như vậy là...

368
00:14:31,113-->00:14:33,113
Kaguya, cậu có muốn hẹn hò với tớ không?

368
00:14:34,112-->00:14:41,113
Vâng! Tôi vui, vui lắm!

368
00:14:43,112-->00:14:55,113
Chúc phúc. Chúc mừng nhé, Kaguya.
Shidou, hãy chăm sóc Kaguya thật tốt.

368
00:14:56,112-->00:14:59,113
Ừ. Tớ hứa sẽ chăm sóc cô ấy thật tốt.
Yuzuru, tớ...

368
00:15:00,112-->00:15:03,513
Dừng lại. Không cần phải xin lỗi đâu.

368
00:15:04,112-->00:15:15,513
Cậu đã cho tôi nhiều kỷ niệm đẹp rồi.
Không cần phải xin lỗi. Và Kaguya...

368
00:15:16,512-->00:15:26,113
Tôi biết rồi. Tôi sẽ chăm sóc 
Shidou như tôi chăm sóc cô.

368
00:15:27,112-->00:15:49,513
Dấu hiệu. Dường như Kaguya cũng yêu Shidou.
Tình cảm của tôi và cô đều giống nhau.
Và... hạnh phúc Kaguya là hạnh phúc của tôi.

368
00:15:50,512-->00:15:53,113
Yuzuru, cảm ơn cậu nhiều lắm.

368
00:15:54,112-->00:16:00,513
Lặp lại. Chúc mừng cô.

368
00:16:01,113-->00:16:10,513
Kaguya, Cô sao thế?
Cô không muốn nói gì với Shidou sao?

368
00:16:18,512-->00:16:22,113
Cứ nói đi. Tớ sẽ nghe mà.
Tớ không cười đâu.

368
00:16:23,112-->00:16:34,513
Này Shidou... Cuộc hẹn của chúng ta
sẽ là khoảng thời gian siêu lãng mạn nhé.

368
00:16:35,513-->00:16:38,113
Lãng mạn? Kaguya...

368
00:16:39,112-->00:16:43,113
Cậu không thích à?

368
00:16:44,512-->00:16:51,513
Mỉm cười. Lãng mạn và Kaguya
không hợp nhau đâu.

368
00:16:52,512-->00:16:55,113
Yuzuru nói đúng đấy. Cậu không hợp đâu.

368
00:16:56,112-->00:17:00,113
Thôi ngay! Cả 2 thôi đi!

368
00:17:01,112-->00:17:04,513
Và như vậy, tôi đã bắt đầu hẹn hò với Kaguya. 
Tôi cảm thấy mình thực sự muốn chăm sóc tốt 
bạn gái duy nhất của tôi.

368
00:17:05,513-->00:17:09,113
Nhưng không phải là tôi bỏ rơi Yuzuru.
Cô ấy vẫn là người rất quan trọng
với Kaguya và tôi.

368
00:17:10,112-->00:17:14,113
Có những điều mà chỉ có cặp đôi mới làm được.
Nhưng tất nhiên đối với chúng tôi, 
cả 3 chúng tôi đều chung niềm vui.

368
00:17:15,512-->00:17:17,113
Mình nghĩ mình có hơi tham lam...

368
00:17:18,113-->00:17:22,113
Tôi nắm lấy tay Kaguya và tôi bắt đầu chạy 
khi chúng tôi nói lời tạm biệt Yuzuru.






368
00:19:41,112-->00:19:44,113
Hôm nay là lần đầu tiên 
tôi hẹn hò với Kaguya từ hôm đó.

368
00:19:44,512-->00:19:47,513
Nó là cuộc hẹn của riêng chúng tôi.
Tôi cảm thấy lo lắng vì chúng tôi 
không hẹn hò 3 người như trước nữa.

368
00:19:48,512-->00:19:52,513
Ổn thôi, đừng bận tâm, nhưng mà...
Cậu đang làm gì vậy, Yuzuru?

368
00:19:53,512-->00:20:06,113
Trả lời. Tớ đi cùng cậu trong khi chờ Kaguya.
Có thể sẽ giúp cậu đỡ buồn chán.

368
00:20:07,112-->00:20:10,513
Cảm ơn cậu. Chắc chắn cậu sẽ giúp tớ đỡ buồn.
Nhưng cậu ở đây rồi. Cậu có muốn 
đi với bọn tớ không, Yuzuru?

368
00:20:11,513-->00:20:25,113
Từ chối. Chuyện đó là không thể. 
Giờ cậu là bạn trai của Kaguya.
2 người hãy dành thời gian cho nhau. 

368
00:20:26,113-->00:20:29,513
Vậy à? Tớ nghĩ Kaguya sẽ ổn thôi.
Chắc chắn cô ấy sẽ hạnh phúc.

368
00:20:30,513-->00:20:47,113
Khẳng định. Kaguya yêu tớ rất nhiều.
Tuy nhiên, giờ cô ấy đã có bạn trai rồi.
Tớ sẽ yên tâm và quan tâm cô ấy nhiều hơn.

368
00:20:48,112-->00:20:54,113
Thật vậy sao?
Ồ, cô ấy tới rồi kìa.

368
00:20:55,112-->00:20:59,113
Xin lỗi vì đã để anh đợi lâu, Shidou.

368
00:20:59,512-->00:21:06,113
Hở? Yuzuru? Cô cũng đi chung với bọn tôi à?

368
00:21:07,112-->00:21:18,113
Từ chối. Tiếc là tôi không thể đi cùng.
Tôi chỉ đơn giản muốn xem cuộc hẹn của cô thôi.

368
00:21:19,112-->00:21:22,113
Cảm ơn cậu vì mọi thứ, Yuzuru.
Vậy bọn tớ đi đây.

368
00:21:23,112-->00:21:38,113
Cảnh báo. Shidou, tớ phải cho cậu vài lời khuyên.
Sở thích của Kaguya khá kỳ quái đấy.
Vậy nên cậu phải cố gắng lên nhé.

368
00:21:39,112-->00:21:41,113
Ừ... ừ...

368
00:21:42,112-->00:21:46,113
Rút lui. Tớ đi đây...

368
00:21:47,112-->00:21:49,513
Anh phải cố gắng chiều sở thích của em à?
Ý cô ấy là sao?

368
00:21:50,512-->00:22:00,513
Shidou, chúng ta hãy bắt đầu lại nào.
Nếu anh còn để em đợi nữa,
con thú của em sẽ bị đánh thức đấy.

368
00:22:01,513-->00:22:03,513
Ừ... Thôi nào. 
Có một nơi mà em rất muốn đến phải không?

368
00:22:04,512-->00:22:09,113
Ừ! Theo em nào!

368
00:22:13,112-->00:22:15,513
Và nơi mà Kaguya muốn tôi đến là...

368
00:22:16,512-->00:22:19,113
Cửa hàng quần áo à?
Em muốn mua vài bộ đồ sao?

368
00:22:20,112-->00:22:27,113
Chính xác. Hôm nay em sẽ chọn 
một bộ quần áo thời trang nhất!

368
00:22:28,112-->00:22:30,513
Anh rất vui, nhưng em định 
chọn quần áo cho anh à, Kaguya?

368
00:22:31,512-->00:22:35,513
Có... có được không?

368
00:22:36,512-->00:22:40,113
À, không. Chỉ là anh không quen việc 
con gái chọn quần áo cho anh.

368
00:22:41,112-->00:22:49,513
Nếu vậy thì anh chỉ cần mặc thôi.
Giờ thì vào thôi.

368
00:22:50,513-->00:22:52,513
Được rồi...

368
00:22:54,512-->00:22:58,113
Kaguya phấn khích đẩy tôi vào.
Tôi không phiền khi bị đem đi thử nghiệm, nhưng...

368
00:22:59,112-->00:23:01,513
Mình bắt đầu cảm thấy hơi lo lắng 
vì lý do nào đó.

368
00:23:02,513-->00:23:08,513
Để mọi thứ cho cô ấy lo có ổn không?
Không, đây là việc mà bạn gái làm cho tôi.
Tôi phải tin tưởng cô ấy.
Nhưng khoan đã, thời trang mà Kaguya chọn là...

368
00:23:09,513-->00:23:18,113
Xin lỗi vì để anh đợi lâu! Em đã mua nó rồi! 
Anh đợi ở đây. Em sẽ mặc thử nó.

368
00:23:19,113-->00:23:21,113
Mặc thử à?

368
00:23:22,112-->00:23:26,113
Cô ấy chọn quần áo giống các cặp đôi
mặc đồ giống nhau sao?
Đừng nghĩ lung tung nữa. Tôi đang nghĩ
tôi sẽ phải mặc bộ đồ gì đây.

368
00:23:27,112-->00:23:29,513
Đây là... Ra là vậy.
Đây đúng là bộ đồ theo cặp.

368
00:23:30,513-->00:23:33,113
Mình cảm thấy nhớ bộ đồ cũ của mình.

368
00:23:34,112-->00:23:37,113
Không, đây là lúc để chuẩn bị tinh thần.
Tôi là bạn trai của Kaguya mà.

368
00:23:38,112-->00:23:42,113
Tôi sẽ dồn hết can đảm để mặc nó.

368
00:23:45,112-->00:23:48,113
Và hiện giờ, tôi đang đi với Kaguya
trong khu mua sắm.

368
00:23:51,112-->00:23:55,113
Quả nhiên anh mặc rất hợp đấy Shidou!

368
00:23:56,112-->00:23:58,113
Vậy... vậy sao?

368
00:23:59,112-->00:24:07,113
Đó là một sự kết hợp tuyệt vời đấy, Shidou!
Bộ đồ rất hợp với tay sai của em.

368
00:24:08,112-->00:24:11,113
Anh vui vì em thích, theo cách nào đó...

368
00:24:13,512-->00:24:17,113
Em trông rất xinh đẹp, Kaguya.

368
00:24:18,512-->00:24:24,113
Anh khen vậy làm em xấu hổ đấy...

368
00:24:25,113-->00:24:28,113
Sao thế? Em chọn quần áo để
chúng ta giống như một cặp phải không?

368
00:24:29,112-->00:24:39,113
Em... em không nghĩ thế! Em chỉ nghĩ 
chúng dễ thương và hợp với anh thôi.

368
00:24:40,112-->00:24:43,113
Anh hiểu rồi. Em nói đúng.
Em chọn tốt lắm, Kaguya.
Em rất dễ thương.

368
00:24:44,112-->00:24:53,113
Cảm ơn nhé, Shidou.
Anh cũng nổi bật lắm.

368
00:24:54,112-->00:24:57,113
Thật sao? Anh thấy khác với anh thường ngày quá.

368
00:24:58,112-->00:25:05,113
Anh rất tuyệt. Cứ yên tâm đi.
Dù sao anh cũng là bạn trai em mà.

368
00:25:06,112-->00:25:08,113
Hahaha... Anh thấy tự tin trở lại rồi.

368
00:25:09,112-->00:25:12,113
Sự thật là dù thế nào,
tôi cũng sẽ giữ bí mật chuyện này.

368
00:25:13,112-->00:25:16,513
Tôi đoán mọi người đang nhìn chúng tôi 
như một cặp vợ chồng. Nhưng tôi không quan tâm,
nhất là khi Kaguya có một nụ cười đẹp như vậy.

368
00:25:17,513-->00:25:22,513
Hôm nay chúng ta sẽ tiếp tục mua quần áo nhé?

368
00:25:23,512-->00:25:25,113
Hở? Lại quần áo nữa à?

368
00:25:26,112-->00:25:39,113
Shidou, Khi hẹn hò bí mật, quần áo rất thích hợp 
cho dịp này đấy. Chúng ta sẽ 
mua thật nhiều bộ quần áo khác nhau.

368
00:25:40,112-->00:25:42,513
Vậy anh không có lựa chọn nào khác.
Anh sẽ đi cùng em, công chúa.

368
00:25:43,512-->00:25:45,513
Ừ!

368
00:25:46,512-->00:25:48,113
Nhưng chỉ hôm nay thôi đấy.

368
00:25:49,112-->00:25:58,113
Vì anh đã chọn em, anh phải chấp nhận nó 
như một chuyện hiển nhiên đi.

368
00:25:59,113-->00:26:01,113
Vậy à? Anh hiểu rồi. 

368
00:26:02,113-->00:26:07,113
Đúng vậy. Tôi đã chọn Kaguya.
Tôi sẽ không trốn tránh bất cứ điều gì.
Do đó, lời cảnh báo của Yuzuru 
không có nhiều ý nghĩa lắm. 

368
00:26:08,512-->00:26:15,513
Và... em cũng nói luôn điều này.
Em sẽ không bỏ cảnh tay này ra đâu!

368
00:26:16,512-->00:26:19,113
Không cần phải nói với anh đâu. 
Em có thể giữ bao lâu tùy ý.

368
00:26:20,512-->00:26:25,113
Chúng ta sẽ bên nhau mãi mãi như thế này nhé?

368
00:26:25,112-->00:26:31,113

