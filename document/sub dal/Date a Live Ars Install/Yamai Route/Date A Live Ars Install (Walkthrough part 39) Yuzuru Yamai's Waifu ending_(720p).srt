﻿0
00:00:00,512-->00:00:03,113
1) Tôi muốn gặp Kaguya.
2) Tôi muốn gặp Yuzuru.

1
00:00:04,112-->00:00:08,113
Tôi quyết định sẽ gặp Yuzuru.

2
00:00:10,512-->00:00:13,113
Đó là một buổi chiều đẹp đẽ. 
Tôi đến công viên để hẹn hò với Yuzuru.

3
00:00:14,112-->00:00:18,113
Mình đến đúng giờ và đi bộ nãy giờ rồi.
Yuzuru ở đâu nhỉ?

4
00:00:21,512-->00:00:33,113
Kêu gọi. Shidou, ở đây này.
Cảm ơn cậu đã mời tớ đi hẹn hò hôm nay.

5
00:00:34,112-->00:00:36,113
Ừ. Cậu chờ lâu chưa?

6
00:00:37,112-->00:00:44,513
Phủ định. Tớ cũng vừa mới tới thôi. Đừng lo.

7
00:00:45,512-->00:00:50,113
Với Yuzuru, tôi không thể biết 
cô ấy nói thật hay chỉ tỏ ra lịch sự. 
Lần cuối tôi trễ hẹn, cô ấy đã rất tức giận.

8
00:00:51,112-->00:00:55,113
Nhưng không cần phải lo mãi chuyện đó.
Trông cô ấy cũng không có vẻ gì tức giận cả.

9
00:00:56,112-->00:00:58,113
Vậy chúng ta đi đâu đây?

10
00:00:59,112-->00:01:05,113
Hỏi lại. Cậu không thích ở đây sao?

11
00:01:06,112-->00:01:09,113
Hở? Không phải chúng ta gặp ở đây
rồi sau đó đi chỗ khác sao?

12
00:01:10,112-->00:01:14,113
Yuzuru là người quyết định nơi gặp nhau.
Nhưng đi chơi ở đây có vẻ hơi khác mọi ngày.

13
00:01:15,112-->00:01:25,513
Hài lòng. Không, tớ muốn chúng ta
sẽ hẹn hò ở đây. Cậu không thích ý tưởng này sao?

14
00:01:26,512-->00:01:31,513
À không, không có gì. Dĩ nhiên là tớ đồng ý.
Nhưng đi chơi trong công viên à?
Chúng ta có thể đến chỗ nào cậu thích mà.

15
00:01:32,512-->00:01:45,113
Khẳng định. Hôm nay tớ muốn ở đây.
Đi đâu đó khi chúng ta đang ở một mình với nhau,
tớ sẽ cảm thấy có lỗi với Kaguya.

16
00:01:46,112-->00:01:50,113
Vậy à? Cậu nói đúng.
Tớ có thể tưởng tượng Kaguya nói:
"Tại sao không cho tôi đi chung chứ?"

17
00:01:51,112-->00:02:04,113
Khẳng định. Kaguya là một cô bé không có 
biện pháp khắc phục. Tớ đã là người lớn, 
tớ sẽ đi chơi trong công viên này để
Kaguya không than phiền gì cả.

18
00:02:05,113-->00:02:10,113
Mặc dù vậy, những lời nói đó là 
suy nghĩ của Kaguya. Nếu Kaguya đang ở đây, 
có lẽ cô ấy sẽ nói như vậy.

25
00:02:11,112-->00:02:17,113
Yêu cầu. Shidou, cậu đi dạo không?

25
00:02:18,112-->00:02:20,113
Được rồi. Thư giãn thế này cũng tốt phải không?

25
00:02:21,113-->00:02:24,513
Tôi xem bản đồ công viên và nhìn những con đường.
Tôi nghĩ cũng tốt khi đi bộ một cách lặng lẽ 
như những tia nắng mặt trời xuyên qua hàng cây.

25
00:02:25,512-->00:02:28,113
Um... nói gì đây nhỉ?

25
00:02:29,112-->00:02:33,513
Im lặng...

25
00:02:34,512-->00:02:37,113
Hở? Sao vậy, Yuzuru?

25
00:02:38,112-->00:02:49,113
Tức giận. Shidou, hãy suy nghĩ chút đi.
Tớ nói tôi muốn có một chuyến đi, 
nhưng chẳng lẽ tớ phải đi một mình sao?

25
00:02:49,512-->00:02:55,113
Cậu quên mất điều rất quan trọng 
đối với các cặp đôi đấy.

25
00:02:56,112-->00:02:58,113
Hở? À... Xin lỗi Yuzuru... Cậu nói đúng.

25
00:02:59,112-->00:03:06,513
Xin lỗi. Miễn là cậu hiểu là được.

25
00:02:07,112-->00:03:12,513
Yêu cầu. Tôi nắm tay cậu được chứ, Shidou?

25
00:03:13,513-->00:03:15,113
Được rồi. Cậu cứ nắm đi, Yuzuru.

25
00:03:16,113-->00:03:19,113
Chúng tôi bắt đầu đi bộ tay trong tay.

26
00:03:23,112-->00:03:27,113
Chúng tôi chỉ bước đi lặng lẽ trong công viên
mà không làm bất cứ điều gì đặc biệt. 
Đôi lúc có một làn gió nhẹ thổi qua.
Yuzuru vẫn nắm tay và nhắm mắt lại thưởng thức.

26
00:03:28,112-->00:03:30,113
Dường như chúng ta quên cả thời gian rồi.

26
00:03:31,112-->00:03:43,513
Bác bỏ. Dù là vậy thật. 
Nhưng khi tớ được ở một mình với Shidou,
tớ không muốn quên khoảng thời gian này đâu.

26
00:03:44,512-->00:03:48,113
Tớ rất vui, nhưng cậu nói thế làm tớ xấu hổ đấy.

26
00:03:49,112-->00:03:55,513
Ngạc nhiên. Vậy sao? 
Tớ chỉ nói những gì tớ nghĩ thôi.

26
00:03:56,512-->00:04:00,113
Haha... đúng là Yuzuru...
giống y hệt Kaguya...

26
00:04:01,113-->00:04:14,113
Cảnh báo. Shidou, bây giờ cậu đang 
hẹn hò với tớ. Nói về phụ nữ khác là 
bất lịch sự lắm đấy biết không?

26
00:04:15,113-->00:04:17,113
Hở? Ngay cả Kaguya sao?

27
00:04:18,113-->00:04:26,513
Khẳng định. Tất nhiên.
Ngay cả Kaguya cũng không thích đâu.

38
00:04:27,512-->00:04:30,113
Ừ... Tớ sẽ rút kinh nghiệm...

39
00:04:31,112-->00:04:41,513
Nóng nảy. Tớ hy vọng thế.
Giờ chúng ta nghỉ ngơi chút đi.

40
00:04:42,513-->00:04:45,113
Ừ. Chúng ta ngồi ở ghế đó đi.
Tớ đi mua nước đây.

41
00:04:46,113-->00:04:49,113
Sau khi tìm không thấy máy bán nước,
tôi tìm thấy một thứ gì đó hay hay.

42
00:04:50,113-->00:04:52,113
Yuzuru, đằng kia thì sao?

43
00:04:53,112-->00:05:02,113
Đồng tình. Chiếc xe đó bán cà phê dạo 
phải không? Không vấn đề gì.

39
00:05:03,112-->00:05:05,513
Được rồi, cậu uống cà phê chứ?

43
00:05:06,512-->00:05:23,113
Khẳng định. Tớ thì không vấn đề gì.
Nhưng tớ lo cho cậu. Những xe bán hàng này
hay thường cho một chút ma thuật vào đó.

44
00:05:24,112-->00:05:26,113
Ma thuật gì cơ?

45
00:05:27,112-->00:05:38,512
Giải thích. Ví dụ: "Thêm sôcôla,
Thêm cà phê có đường, kem Caramen"...
Đại loại thế.

46
00:05:39,512-->00:05:42,113
Waaa? Có chuyện đó sao?

47
00:05:43,112-->00:05:57,113
Trả lời. khi bọn tớ đến một quán cà phê,
Kaguya đọc nó với niềm hạnh phúc tuyệt vời. 

48
00:05:58,112-->00:06:02,513
Vậy... vậy à? Có lẽ cậu không biết 
ở đây chỉ bản đồ rẻ tiền thôi.
Khoan đã! Cậu nói mà không sai chính tả sao?

49
00:06:03,513-->00:06:13,513
Mỉm cười. Tớ biết chứ.
Tớ uống một ly cà phê thường thôi.

41
00:06:14,513-->00:06:17,113
Được rồi. Cậu ngồi đây đợi tớ nhé.

42
00:06:21,112-->00:06:23,513
Đây là của cậu. Ly này của tớ.

44
00:06:24,512-->00:06:33,513
Biết ơn. Cám ơn rất nhiều.
Cậu không bị lỗi chính tả chứ, Shidou?

45
00:06:34,513-->00:06:37,513
May mắn là không. Tớ nói như thế này:
"Cho 2 ly cà phê thường thôi"

46
00:06:38,512-->00:06:44,513
Suy nghĩ... Tớ không biết phải nói gì cả.

47
00:06:45,513-->00:06:49,113
Xin lỗi, quên nó đi...
Cậu uống thấy ngon chứ?

48
00:06:50,112-->00:07:00,113
Đồng tình. Tớ cảm thấy thoải mái hơn rồi.
Nó ngon lắm. Nhưng tớ muốn thứ khác tốt hơn.

59
00:07:01,113-->00:07:02,513
Thứ khác tốt hơn sao?

60
00:07:03,512-->00:07:16,513
Yêu cầu. Vâng, Shidou.
Hãy đến gần tớ một chút và nhắm mắt lại đi.

61
00:07:17,512-->00:07:21,113
Hở? Gì vậy?

62
00:07:23,512-->00:07:26,113
Gì vậy? Yuzuru làm gì vậy?

63
00:07:27,112-->00:07:33,513
Yêu cầu. Shidou, đưa tai cậu đây.

64
00:07:34,512-->00:07:36,513
Sau khi cô ấy làm gì đó,
một âm thanh êm dịu bắt đầu phát ra.

65
00:07:37,512-->00:07:40,113
Một cái tai nghe?
À, một máy nghe nhạc à?

66
00:07:41,112-->00:07:50,513
Xác nhận. Tớ nghe nó khi rảnh rỗi.
Chỉ có một chiếc thôi nên 2 đứa xài chung nhé.

67
00:07:51,512-->00:07:54,513
Cậu mang cả máy nghe nhạc sao?

80
00:07:56,512-->00:08:00,513
Tôi đột ngột mở mắt và thấy Yuzuru
đang nhắm mắt lại, tay cô ấy cầm 
một cái máy nghe nhạc 
và đang dùng chung tai nghe với tôi.

81
00:08:01,512-->00:08:13,113
Yêu cầu. Shidou, đừng ngồi xa quá.

82
00:08:14,512-->00:08:17,113
Tai nghe này ngắn thế à?

83
00:08:18,112-->00:08:25,513
Im lặng. Tớ không thể trả lời.
Hãy nghe nhạc đi.

84
00:08:26,512-->00:08:28,113
Vậy à?

85
00:08:29,512-->00:08:32,513
Nghe Yuzuru nói, tôi nghiêng đầu 
về phía cô ấy và thưởng thức âm nhạc.

86
00:08:33,512-->00:08:35,513
Vậy là cậu thường nghe loại nhạc này à?

87
00:08:36,513-->00:08:51,113
Xác nhận. Đó gọi là âm nhạc chữa lành.
Đây là nhạc khiến cho tớ thanh thản hơn.
Tất nhiên, tớ cũng nghe các thể loại khác nữa.

88
00:08:52,112-->00:08:55,113
Nhìn máy nghe nhạc Yuzuru,
tôi chắc chắn sẽ có tất cả các loại nhạc.

89
00:08:56,112-->00:08:59,512
Có cả nhạc rock, pop, nhạc cổ điển, 
và nhiều hơn nữa.
Ngoài ra còn có nhạc sôi động 
mà chắc Kaguya thích

90
00:09:00,513-->00:09:06,113
Máy nghe nhạc Yuruzu đã được 
thiết lập chọn ngẫu nhiên. 
Cả hai chúng tôi lặng lẽ nghe nhạc

102
00:09:11,112-->00:09:13,513
Cảm ơn cậu đã đi cùng tớ.
Tớ đã có một ngày tuyệt vời đấy.

103
00:09:14,512-->00:09:21,113
Trả lời. Vâng, Shidou. Tớ cũng vậy.

104
00:09:22,112-->00:09:25,113
Yên tĩnh thế này cũng tốt, phải không?
Lần khác chúng ta cũng đi thế này nữa nhé.

105
00:09:26,112-->00:09:39,113
1) Hẹn hò lần nữa với Yuzuru
2) Lần kế tiếp hãy đi cùng Kaguya.

106
00:09:40,112-->00:09:42,113
Buổi hẹn kế tiếp chỉ 2 chúng ta thôi nhé Yuzuru.

107
00:09:43,112-->00:09:49,113
Thắc mắc. Shidou, ý cậu là sao?

108
00:09:50,112-->00:09:54,113
À không, không có gì đâu. Chỉ là hôm nay 
cậu khác với lúc cả 3 chúng ta đi chung.
Mặc dù cũng vui nhưng hôm nay trông cậu
có vẻ trìu mến hơn.

109
00:09:55,112-->00:10:00,113
Vì vậy, tớ sẽ rất vui nếu chúng ta có thể
đi chơi như thế này lần nữa.
Chúng ta đang hẹn hò, nhưng không có nghĩa là 
phải luôn đủ 3 người phải không?

110
00:10:01,113-->00:10:09,113
Bất bình. Tớ không thích ý kiến đó đâu.

110
00:10:09,513-->00:10:20,513
Đúng vậy, hẹn hò với cậu rất vui. 
Tuy nhiên, làm ơn đừng gạt Kaguya sang một bên.

111
00:10:21,513-->00:10:24,513
Tớ biết điều đó. Ý tớ là chúng ta 
đôi lúc nên hẹn hò riêng thế này.

113
00:10:25,512-->00:10:33,513
Yêu cầu. Vậy cậu sẽ mời tớ lần nữa à?

114
00:10:34,512-->00:10:37,113
Ừ. Nếu cậu muốn, tớ sẽ mời cậu. Tớ hứa.

1
00:10:38,112-->00:10:51,113
Hâm nóng. Hôm nay thật vui. 
Giờ tớ phải về rồi
Shidou, mai gặp lại nhé.

368
00:10:53,112-->00:10:55,113
A, Yuzuru!

368
00:10:56,112-->00:11:01,113
Lúc đó, tôi đã không nghĩ kỹ hơn.
Tôi chỉ nghĩ nếu như hẹn hò mà không có Kaguya...
Khoan đã, không được!

368
00:11:06,112-->00:11:08,113
Nói chuyện với Kaguya và Yuzuru sao?
Nói gì bây giờ chứ?

68
00:11:09,113-->00:11:13,513
Tôi đi lên sân thượng vì một tin nhắn của
Kaguya và Yuzuru muốn nói chuyện gì đó quan trọng. 
Tin nhắn chỉ ghi là: "Gặp bọn tớ đi. Bọn tớ 
muốn nói chuyện. Bọn tớ đang đợi trên sân thượng"

368
00:11:14,512-->00:11:18,113
Mình có thể đoán được họ muốn nói chuyện gì...

368
00:11:20,512-->00:11:23,513
Chúng tôi đang đợi cậu đây, Shidou.

368
00:11:24,512-->00:11:30,113
Chờ đợi. Bọn tớ đang đợi cậu đây, Shidou.

368
00:11:31,112-->00:11:34,113
Ừ. Xin lỗi đã để các cậu đợi.
Các cậu muốn nói chuyện gì?

68
00:11:35,112-->00:11:38,513
Sau khi họ nhìn nhau với ánh mắt ngờ vực,
Tôi nghĩ tình hình này sẽ kéo dài. 
Nhưng Yuzuru bước đến phía tôi.

368
00:11:39,512-->00:11:53,513
Ghi nhớ. Buổi hẹn vừa rồi thật sự rất vui.
Thời gian tớ ở một mình với cậu rất hạnh phúc.

368
00:11:54,513-->00:12:16,113
Thú nhận. Tớ yêu cậu Shidou.
Vì vậy tớ không muốn cậu chọn cả 2 nữa.
Tớ muốn cậu là của tớ.

368
00:12:17,112-->00:12:27,113
Tôi cũng cảm thấy như vậy...
3 người cũng tốt nhưng cuối cùng,
tôi muốn được ở một mình với cậu nhiều hơn.

368
00:12:28,113-->00:12:33,113
Vì vậy, tôi nghĩ chúng ta cần phải thay đổi.

368
00:12:34,113-->00:12:37,513
Tôi biết chuyện này sẽ xảy ra.
Nhưng đây không phải là trò chơi, 
mà là kết quả của những buổi hẹn hò nghiêm túc.

368
00:12:38,512-->00:12:42,513
Nhìn thấy vẻ mặt nghiêm trọng của họ,
Tôi hiểu rằng đây là vấn đề nghiêm túc.
Cả Kaguya và Yuzuru đầy nghi ngờ. 
Vì vậy, việc chọn cả 2 là chuyện khó đạt được.

368
00:12:43,512-->00:12:46,513
Tôi nghĩ đã đến lúc tôi phải 
đối mặt một cách nghiêm túc.
Tôi sẽ không trả lời tránh né nữa.

368
00:12:47,512-->00:12:52,113
Xin lỗi. Có lẽ tớ không phải là 
một đứa con trai tốt.
Tớ không thể làm hài lòng các cậu.

368
00:12:53,112-->00:13:00,113
Không phải thế! Chỉ là bọn tôi muốn vậy thôi!

368
00:13:01,112-->00:13:13,113
Yêu cầu. Tuy nhiên, chúng tớ muốn Shidou 
chọn một trong hai chúng tớ...

368
00:13:15,112-->00:13:17,513
Này... 2 cậu...? Chờ chút đã...!

368
00:13:18,512-->00:13:32,113
Nào Shidou, hãy cảm nhận đi. Cả 2 chúng tôi 
đều có trái tim đang đập mạnh phải không? 
Đó là tình yêu sâu sắc nhất dành cho cậu đấy.

368
00:13:33,112-->00:13:44,513
Khẳng định. Đó là tình yêu dành cho cậu.
Nếu là cậu, Shidou, cậu sẽ cảm nhận được.

368
00:13:45,512-->00:13:48,513
Cả 2 ấn tay tôi vào ngực họ. 
Tôi bắt đầu cảm thấy nhịp đập của họ.

368
00:13:49,513-->00:13:52,113
Không, đó có thể là âm thanh của trái tim tôi.
Thế này quá đột ngột, Tôi không biết 
chuyện gì đang xảy ra cả...

368
00:13:53,112-->00:14:03,113
Hội đồng. Shidou, không cần phải lựa chọn 
người có nhịp đập lớn hơn. 
Cậu chọn người có ngực lớn nhất cũng được.

368
00:14:04,113-->00:14:14,513
Thật không công bằng! 
Nếu vậy thì cô thắng rồi, Yuzuru!
Shidou, nhỏ cũng không sao đâu.
Có lẽ nó đang phát triển thôi.

368
00:14:15,512-->00:14:27,113
Bác bỏ. Tôi nghĩ chọn ngực lớn ngay từ đầu
sẽ tốt hơn. Ngoài ra, cú sốc khi cô
thất vọng rất tuyệt vời.

368
00:14:28,112-->00:14:38,113
Không phải ngực nhỏ là hết hy vọng đâu!
Người ta nói rằng kích cỡ 
không quan trọng. Đúng không Shidou?

368
00:14:39,112-->00:14:42,513
Không, Kaguya, đủ rồi...
Cho tớ nói đã! Tớ chẳng suy nghĩ được gì cả!

368
00:14:43,512-->00:14:50,113
Tôi sẽ không bỏ tay ra đâu.
Tôi sẽ đợi cho đến khi cậu chọn, Shidou.

368
00:14:51,112-->00:14:58,113
Yêu cầu. Shidou, hãy nói cảm nhận của cậu đi.

368
00:14:59,112-->00:15:01,113
Được rồi. Tớ sẽ suy nghĩ cẩn thận.

368
00:15:02,112-->00:15:07,113
Tôi phải làm gì đó để bình tĩnh lại
và tập trung vào nhịp tim của họ
để biết nên chọn ai.

368
00:15:11,512-->00:15:15,113
Tớ cảm thấy nhịp tim của Yuzuru đập nhanh hơn.

368
00:15:16,112-->00:15:27,113
Xác nhận. Shidou, thật vậy không?
Có thật là tớ...?

368
00:15:28,112-->00:15:30,113
Phải, là cậu Yuzuru. Tớ yêu cậu.
Cậu có muốn hẹn hò với tớ không?

368
00:15:31,112-->00:15:43,513
Đồng ý. Tớ rất hạnh phúc khi cậu nói vậy, Shidou.

368
00:15:45,112-->00:15:53,113
Xin chúc mừng, Yuzuru. Ngay cả Shidou
cuối cùng cũng đã hiểu Yuzuru dễ thương nhất.

368
00:15:54,113-->00:16:11,513
Bác bỏ. Kaguya, không cần giả vờ đâu.
Giống như tôi yêu Shidou, 
chắc chắn cô cũng yêu cậu ấy, phải không Kaguya?

368
00:16:12,512-->00:16:27,513
Được rồi! Chúng ta đã thảo luận hôm qua rồi mà.
Shidou đã chọn Yuzuru, thực sự tôi cảm thấy 
hạnh phúc. Tất nhiên cũng có chút buồn nữa.

368
00:16:28,513-->00:16:30,113
Xin lỗi nhé, Kaguya.

368
00:16:31,112-->00:16:35,513
Đừng có xin lỗi như thể không có gì như thế!

368
00:16:36,112-->00:16:42,113
Yuzuru, Shidou xấu quá!

368
00:16:43,112-->00:16:56,113
Vuốt ve. Ngoan nào, ngoan nào. 
Shidou xấu lắm phải không?
Với tư cách bạn gái, tôi sẽ mắng cậu ta cho.

368
00:16:57,112-->00:17:03,113
Shidou, Yuzuru chọc tôi kìa!

368
00:17:04,113-->00:17:06,513
Được rồi. Với tư cách bạn trai...

368
00:17:08,512-->00:17:17,113
Đủ rồi! Thôi đi! 2 người đừng có xoa đầu tôi nữa!

368
00:17:18,112-->00:17:20,513
Được rồi. Kaguya, 
cậu chúc phúc cho bọn tớ đi.

368
00:17:22,112-->00:17:30,513
Nếu thỉnh thoảng hai người cho tôi đi cùng 
như bây giờ, tôi sẽ chúc phúc cho hai người.

368
00:17:31,512-->00:17:41,513
Đồng ý. Tất nhiên rồi.
Kaguya, cô là một người rất, 
rất quan trọng với tôi.

368
00:17:42,512-->00:17:44,113
Phải, tớ cũng vậy.

368
00:17:45,112-->00:17:57,113
An ủi đủ rồi đấy! Quan trọng hơn, 
Yuzuru không muốn nói gì với Shidou sao?

368
00:17:58,112-->00:18:14,113
Yêu cầu. Đúng vậy... Vậy Shidou,
yêu cầu đầu tiên từ bạn gái cậu là...
tôi có thể hẹn hò với cậu chứ?

368
00:18:15,112-->00:18:17,113
Được, tất nhiên rồi.

368
00:18:18,112-->00:18:23,113
Thôi nào, thôi nào. Đến giờ về rồi đấy.

368
00:18:24,112-->00:18:26,113
Ừ! Hẹn gặp lại vào ngày mai nhé, Kaguya!

368
00:18:27,112-->00:18:32,513
Làm theo. Chờ đến mai nhé, Kaguya.

368
00:18:33,512-->00:18:44,113
Ừ, hẹn gặp... Khoan đã, Yuzuru?
Cô nói gì? Không! Cô nói gì hả?

368
00:18:45,112-->00:18:47,113
Hahaha...

368
00:18:48,112-->00:18:52,113
Mỉm cười. 

368
00:18:53,113-->00:18:56,113
Tôi nắm lấy bàn tay cô ấy và
bắt đầu suy nghĩ nơi chúng tôi sẽ hẹn hò.

368
00:18:57,112-->00:19:00,513
Và như vậy, tôi đã bắt đầu hẹn hò với Yuzuru.
Tôi cảm thấy mình thực sự muốn chăm sóc tốt 
bạn gái duy nhất của tôi.

368
00:19:01,512-->00:19:04,513
Nhưng không phải là tôi bỏ rơi Kaguya.
Cô ấy vẫn là người rất quan trọng
với Yuzuru và tôi.

368
00:19:05,512-->00:19:10,513
Có những điều mà chỉ có cặp đôi mới làm được.
Nhưng tất nhiên đối với chúng tôi, 
cả 3 chúng tôi đều chung niềm vui.

368
00:19:11,512-->00:19:13,113
Mình nghĩ mình có hơi tham lam...

368
00:19:14,112-->00:19:17,113
Tôi nắm lấy tay Yuzuru và tôi bắt đầu chạy 
khi chúng tôi nói lời tạm biệt Kaguya.




368
00:21:37,112-->00:21:40,513
Yuzuru và tôi bắt đầu hẹn hò trở lại. 
Hôm nay là lần đầu tiên tôi hẹn hò 
với Yuzuru từ hôm đó. Chúng tôi đã có kế hoạch.
Nhưng tôi vẫn cảm thấy lo lắng.

368
00:21:41,513-->00:21:45,113
Bên cạnh đó... 
Tại sao cậu lại ở đây, Kaguya?

368
00:21:46,512-->00:21:59,513
Yu ... Yuzuru nói tôi đi cùng! Cô ấy nói cô ấy sẽ
đến muộn một chút. Vậy nên tôi đến trước để
nói chuyện với cậu. Tôi không có ý gì đâu.

368
00:22:00,512-->00:22:04,113
Ừ, tớ không phiền gì đâu.
Cậu đến để nói chuyện với tớ 
cho đỡ buồn phải không? Cảm ơn nhé.

368
00:22:05,112-->00:22:13,513
Tôi đến bởi vì Yuzuru nhờ tôi thôi.
Lúc này tôi không có suy nghĩ gì với cậu cả.

368
00:22:14,513-->00:22:17,113
Được rồi, tớ không nói gì đâu.

368
00:22:18,112-->00:22:30,113
Giải thích. Kaguya đang hạnh phúc 
khi được nói chuyện với Shidou đấy.

368
00:22:31,512-->00:22:36,513
Xuất hiện. Xin lỗi đã để anh đợi lâu, Shidou.

368
00:22:37,512-->00:22:43,513
Cái...? Yuzuru! Cô vừa nói gì vậy?

368
00:22:44,512-->00:22:47,113
Anh đang đợi em đây, Yuzuru.
Vậy chúng ta đi chứ?

368
00:22:48,112-->00:22:50,113
Này!

368
00:22:51,112-->00:22:54,113
Xin lỗi, xin lỗi. Tớ đùa thôi.
Cảm ơn cậu đã nói chuyện với tớ, Kaguya.
Tớ rất vui đấy.

368
00:22:55,112-->00:23:00,513
Không có gì đâu.

368
00:23:01,513-->00:23:04,513
Cậu muốn đi với bọn tớ không, Kaguya?
Đó cũng là mục đích của Yuzuru phải...

368
00:23:05,512-->00:23:12,113
Nhìn lên. Những đám mây 
đang di chuyển rất nhanh phải không?

368
00:23:13,112-->00:23:22,113
Không. Thật sự là tôi muốn
được ở bên Shidou nhiều hơn một chút.

368
00:23:23,113-->00:23:25,113
Cậu vừa nói gì?

368
00:23:26,112-->00:23:32,113
Không... không có gì, không có gì đâu!
Bây giờ hai người đi với nhau đi.

368
00:23:33,112-->00:23:35,513
Nếu cậu nói vậy, bọn tớ sẽ đi.

368
00:23:36,512-->00:23:46,513
Đồng cảm. Tôi hiểu cảm giác của cô, Kaguya.
Hôm nay tôi sẽ ở một mình với Shidou.

368
00:23:47,512-->00:23:59,113
Chúc hai người vui vẻ.
Tôi không phá rối hai người nữa.
Chào nhé!

368
00:24:00,512-->00:24:03,113
Cô ấy đi rồi. Nhưng như thế có ổn không?

368
00:24:04,112-->00:24:17,513
Xác nhận. Nếu chúng ta nói nữa,
Kaguya sẽ tức giận. Hôm nay cứ lo 
tận hưởng buổi hẹn đi.

368
00:24:18,512-->00:24:22,113
Ừ. Anh sẽ đi cùng em.

368
00:24:27,112-->00:24:31,113
Sau đó, theo yêu cầu Yuzuru, 
chúng tôi đi dạo và xem các cửa hàng.
Với Yuzuru, chỉ nhìn các cửa hàng cũng là niềm vui.

368
00:24:32,112-->00:24:35,113
Vì chỉ có hai người nên 
cảm giác này thật mới mẻ.

368
00:24:36,112-->00:24:45,113
Thấy rõ. Shidou, nhìn kìa.
Rất nhiều con cá dễ thương đang bơi kìa.

368
00:24:46,112-->00:24:49,113
Có vẻ đó là cửa hàng cá nhiệt đới.
Hai con cá này giống em và Kaguya phải không?

368
00:24:50,112-->00:25:08,512
Khẳng định. Mắt Đỏ Bạch Kim (Platinum Red Eye), 
Cá Đuôi Đỏ (Albino Red Tail),
Nghe giống như tên gọi Thiên Phục vậy.
Đúng kiểu của Kaguya phải không? 

368
00:25:09,512-->00:25:13,113
Đúng vậy.
Con kia có vẻ hơi thờ ơ
và giả vờ là trưởng thành, phải không?

368
00:25:14,112-->00:25:25,113
Phản đối. Em không thờ ơ 
cũng không giả vờ trưởng thành đâu.

368
00:25:26,112-->00:25:30,113
Hahaha... Phải rồi. 
Anh chỉ nói mấy con cá thôi mà.

368
00:25:31,112-->00:25:34,113
Nghiến răng...

368
00:25:34,512-->00:25:43,113
Hôm nay anh đóng vai ác nhiều quá nhỉ?
Em lo lắng về tương lai của chúng ta đấy.

368
00:25:44,112-->00:25:47,513
Sau khi thấy biểu hiện tức giận trên 
khuôn mặt Yuzuru, tôi không thể không phì cười. 
Đó là biểu hiện hiếm thấy của Yuzuru, 
trông hơi trẻ con và rất đáng yêu.

368
00:25:48,512-->00:25:51,113
Xin lỗi, xin lỗi. Đừng giận mà.

368
00:25:52,112-->00:26:04,513
Chỉnh sửa. Em không giận.
Trêu ghẹo nhau với anh, em vui lắm

368
00:26:05,513-->00:26:07,513
Anh cũng thế, Yuzuru.

368
00:26:08,512-->00:26:14,113
Khi nói vậy, Yuzuru mỉm cười pha chút xấu hổ.
Trông chúng tôi đang rất hạnh phúc... có lẽ vậy.

368
00:26:15,112-->00:26:23,113
Đề nghị. Giờ chúng ta đi đâu đây?
Đi uống nước đi.

368
00:26:24,112-->00:26:27,113
Ừ. Được rồi. Nghỉ ngơi tí đi.

368
00:26:28,512-->00:26:31,513
Thật may là có bàn trống.
Yuzuru, em chọn nước gì?

368
00:26:32,512-->00:26:40,113
Xem xét. Để xem... em thấy ly này được đấy.

368
00:26:41,112-->00:26:43,513
Ly... ly này à?
Em nghiêm túc đấy chứ?

368
00:26:44,512-->00:26:56,113
Mỉm cười. Tất nhiên rồi.
Em muốn uống ly này với anh.

368
00:26:57,112-->00:27:00,112
Hở, cũng được, nhưng...
Này Yuzuru, em chọn cái khác được không?

368
00:27:01,112-->00:27:10,113
Yêu cầu. Shidou, anh có thể uống cùng
bạn gái anh không?

368
00:27:11,113-->00:27:14,113
Anh hiểu rồi. Được, anh sẽ uống.

368
00:27:17,112-->00:27:21,513
Thứ mà chúng tôi gọi là "Đồ uống cho các cặp đôi"
với một cốc lớn có hai ống hút đan xen nhau.
Giờ tôi mới nhớ những chỗ thế này 
thường có những ly như vậy.

368
00:27:22,512-->00:27:25,113
Này Yuzuru, em không hối hận đấy chứ?
Đổi ly khác vẫn kịp đấy.

368
00:27:26,112-->00:27:36,113
Yêu cầu. Anh sao vậy, Shidou?
Nếu anh không uống thì sẽ hết đấy.

368
00:27:37,112-->00:27:41,513
Tôi bắt đầu nghe thấy tiếng cười khúc khích 
xung quanh. Tôi muốn không để ý 
nhưng không được. Phía trước tôi là Yuzuru. 
Tôi không thể trốn tránh.

368
00:27:42,512-->00:27:44,513
Như... vậy phải không?

368
00:27:46,112-->00:27:52,113
Ngại ngùng. Xấu hổ thật đấy, phải không?

368
00:27:53,112-->00:27:56,113
Trông em đang rất xấu hổ đấy, Yuzuru.
Đây có phải là trả thù cho chuyện vừa rồi không?

368
00:27:57,112-->00:28:07,513
Phản đối. Không phải. 
Em chỉ muốn uống cùng anh thôi, Shidou.
Thật đấy.

368
00:28:08,513-->00:28:10,113
Xin lỗi. Bỏ qua cho anh nhé.

368
00:28:11,112-->00:28:14,113
Sau khi tôi nói vậy, 
cô ấy nở một nụ cười dễ thương.

368
00:28:16,112-->00:28:25,113
Yêu cầu. Thơm ngon đến giọt cuối cùng nhé anh.
Nếu không em sẽ giận đấy.

368
00:28:26,112-->00:28:29,113
Được rồi... Anh tự hỏi em có ngại 
khi người ta nhìn mình không?

368
00:28:31,112-->00:28:41,513
Đồng tình. Đây là chuyện mà các cặp đôi 
thường làm với nhau. Em không ngại đâu.

368
00:28:42,512-->00:28:46,113
Anh cũng nghĩ vậy. Anh sẽ tận hưởng 
khoảnh khắc này và xem em nhút nhát thế nào.

368
00:28:47,112-->00:28:50,513
Dịp thế này không xảy ra thường xuyên đâu.
Chỉ nhìn thấy nụ cười trên khuôn mặt 
của Yuzuru, tôi cũng đã thấy vui rồi.

368
00:28:51,512-->00:29:03,113
Mỉm cười. Anh uống hết đi, Shidou.
Đổi lại, em có một điều kiện.

368
00:29:04,113-->00:29:06,113
Điều kiện gì cơ?

368
00:29:07,112-->00:29:19,113
Yêu cầu. Shidou, hãy ở bên em mãi mãi.
Mãi mãi... nhé anh.


368
00:29:20,112-->00:29:21,113