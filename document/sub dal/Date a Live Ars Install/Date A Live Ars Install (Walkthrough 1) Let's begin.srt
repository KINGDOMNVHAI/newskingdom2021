﻿1
00:02:18,112-->00:02:20,113
Đây là đâu?

2
00:02:25,112-->00:02:30,113
Tại sao tôi được sinh ra?

3
00:02:34,112-->00:02:38,113
Tôi phải làm gì?

4
00:02:42,512-->00:02:45,113
Tôi mong muốn... gì?

5
00:02:49,112-->00:02:55,113
Tôi muốn tìm kiếm... một thứ gì đó?

6
00:03:01,112-->00:03:08,113
Thế giới này thật nhỏ hẹp.
Tôi có thể ra khỏi đây không?

7
00:03:13,112-->00:03:18,113
Trong trường hợp này, đó sẽ là mục tiêu của tôi...

8
00:03:23,112-->00:03:28,113
Tại sao?
Tôi muốn biết cảm giác đó.

9
00:03:34,112-->00:03:41,113
Tôi hiểu rồi. Vì vậy tôi...
tôi muốn biết tất cả mọi thứ.

10
00:03:47,112-->00:03:48,113
Tôi chắc chắn như vậy...

11
00:03:56,112-->00:03:59,113
Phải. Tôi muốn biết điều đó...

12
00:04:09,112-->00:04:14,113
Tinh Linh
Các sinh vật chuyên gây ra thiên tai đặc thù
cư ngụ ở giữa các thế giới.

13
00:04:15,112-->00:04:18,113
Xuất xứ và mục đích của chúng là một ẩn số.

14
00:04:20,112-->00:04:31,113
Khi xuất hiện ở thế giới này,
chúng gây ra các Không gian chấn.
gây sức tàn phá lớn cho các khu vực.

15
00:04:32,112-->00:04:37,113
Giải pháp 1: Tiêu diệt bằng vũ khí.

16
00:04:38,112-->00:04:45,113
Tuy nhiên, điều này khó mà thực hiện được
do năng lực chiến đấu của chúng quá cao.

17
00:04:47,112-->00:04:51,113
Giải pháp 2: dụ dỗ bằng hẹn hò. 

18
00:05:00,112-->00:05:04,113
Yatogami Tohka. Mật danh: Princess.

19
00:05:05,112-->00:05:10,113
Thiên Phục là bộ giáp màu tím, 
mang phong cách Tinh Linh cận chiến.

20
00:05:12,512-->00:05:15,113
Thiên thần hiện tại: Sandalphon.

21
00:05:15,512-->00:05:23,113
Vũ khí là một thanh trường kiếm
với sức mạnh thổi bay một thành phố.

22
00:05:25,112-->00:05:30,113
Sau khi bị phong ấn sức mạnh Tinh Linh, 
cô trở thành học sinh của trường trung học Raizen.

23
00:05:31,112-->00:05:35,113
Sức mạnh Tinh Linh trong cô vẫn còn rất lớn.


24
00:05:36,112-->00:05:39,113
Tuy nhiên, do thiếu kinh nghiệm trong xã hội
loài người nên kiến thức của cô rất yếu.

25
00:05:42,112-->00:05:52,113
Tính cách vui vẻ và thật thà.
Do tính cách đó nên xảy ra nhiều vấn đề.

26
00:05:55,112-->00:05:58,113
Yoshino. Mật danh: Hermit.

27
00:05:58,512-->00:06:04,113
Có thể triệu hồi thiên thần Zadkiel 
trong hình dạng con rối khổng lồ đóng băng mọi thứ.

28
00:06:05,112-->00:06:13,113
Vì tính cách nhút nhát nên cô bé muốn
giảm nguy cơ gây hại cho bất kỳ ai

29
00:06:14,112-->00:06:21,113
Sau khi được phong ấn sức mạnh, 
cô bé sống chung với Yatogami Tohka
trong thân phận là một cô bé dưới 15 tuổi.

30
00:06:22,112-->00:06:29,113
Ngoài ra, cô còn có Yoshinon 
là một con rối trên tay trái của cô bé. 

31
0:06:31,113-->00:06:43,113
Cô bé rất nhút nhát và sống nội tâm.
Nhưng cô bé thường nói qua con rối Yoshinon. 
Con rối rất cá tính và thích nói chuyện.

33
00:06:46,112-->00:06:50,113
Tokisaki Kurumi. Mật danh: Nightmare.

368
00:06:51,112-->00:06:58,113
Thiên thần Zafkiel của cô có hình dạng 
một chiếc đồng hồ điều khiển thời gian.

368
00:06:59,112-->00:07:04,113
Với sức mạnh này, cô có thể biến
quá khứ của mình thành một bản sao

368
00:07:06,112-->00:07:12,113
Tinh Linh này giấu sức mạnh và 
lẩn trốn trong xã hội loài người.
Cô là một người nguy hiểm.

368
00:07:13,112-->00:07:20,113
Cô cần phải giết người 
để lấy thời gian sống của họ.

368
00:07:22,112-->00:07:30,113
Cô có một sự quan tâm đặc biệt 
dành cho Itsuka Shidou. Cô thường 
xuất hiện đột ngột để tiếp cận cậu ta.

368
00:07:32,112-->00:07:36,113
Itsuka Kotori. Mật danh: Efreet.

368
00:07:37,112-->00:07:44,113
Là em gái nuôi của Itsuka Shidou,
14 tuổi và yêu thích thức ăn trẻ con.

368
00:07:47,112-->00:08:00,113
Trước đây cô là con người. 5 năm trước 
Phantom đã xuất hiện, biến cô thành một
Tinh Linh điều khiển lửa. Hiện cô đã bị phong ấn.

368
00:08:02,512-->00:08:07,113
Thiên thần Camael trong hình dạng 
một thanh rìu chiến.

368
00:08:09,112-->00:08:17,113
Khi chuyển sang Megiddo, thanh rìu chiến
biến thành một khẩu đại pháo có sức mạnh rất lớn. 

368
00:08:19,112-->00:08:23,113
Cô là chỉ huy Ratatoskr, tổ chức tìm kiếm 
giải pháp nói chuyện với các Tinh Linh.

368
00:08:24,112-->00:08:35,113
Phi thuyền Fraxinus chỉ đạo và hỗ trợ 
Shidou trong việc nói chuyện với Tinh Linh.

368
00:08:36,112-->00:08:45,113
Tính cách của cô bé thay đổi giữa 
"Chế độ Imouto" vui vẻ và ngây thơ với 
"Chế độ chỉ huy" thông minh nhưng độc miệng. 

368
00:08:45,512-->00:08:51,113
Điều này thay đổi tùy thuộc vào
màu sắc của cặp nơ trên đầu.

368
00:08:53,112-->00:08:59,113
Yamai Kaguya và Yamai Yuzuru. Mật danh: Berserk.

368
00:09:00,112-->00:09:08,113
Ban đầu chỉ là một Tinh Linh. Nhưng vì
lý do nào đó mà tách ra làm hai 
có khả năng điều khiển gió.

368
00:09:10,112-->00:09:19,513
Kaguya với tính cách sôi nổi và cơ thể
mỏng manh. Yuzuru với 3 vòng gợi cảm
và tính cách trầm lặng.

368
00:09:20,112-->00:09:25,113
Họ nhận ra họ giống như chị em song sinh
Giống như Tohka, họ đã được phong ấn.

368
00:09:27,112-->00:09:31,513
Raphael, thiên thần có hình dáng ban đầu
là một cây cung và mũi tên.

368
00:09:32,112-->00:09:40,113
Nó được chia cho Kaguya, mũi tên Re'em
và Yuzuru, dây cung Na'ash.

368
00:09:42,112-->00:09:46,113
Izayoi Miku. Mật danh: Diva.

368
00:09:47,112-->00:09:53,113
Trước đây cô là con người. 
Cô được Phantom biến thành Tinh Linh 

368
00:09:53,512-->00:09:58,113
khi cô không thể hát do vấn đề tâm lý
trong công việc của một thần tượng.

368
00:10:00,112-->00:10:12,113
Gabriel trong hình dạng những chiếc ống
phát nhạc, có khả năng thôi miên
bất kỳ ai nghe thấy âm thanh của cô.

368
00:10:13,113-->00:10:18,513
Cô rất thích những thứ xinh đẹp, 
đặc biệt là những cô gái xinh đẹp. 

368
00:10:19,113-->00:10:26,113
Không giống như cách họ gọi cô là "Onee-san", 
tính cách của cô rất trẻ con.

368
00:10:27,113-->00:10:37,113
Hiện cô vẫn đang giữ một chút sức mạnh
Tinh Linh. Cô học trong trường nữ sinh
Rindouji và tiếp tục công việc ca sĩ.

368
00:10:38,112-->00:10:41,113
Itsuka Shidou, học sinh năm 2
trường Trung học Raizen.

368
00:10:42,112-->00:10:47,113
Cậu là người có khả năng phong ấn 
sức mạnh Tinh Linh.

368
00:10:48,112-->00:10:57,113
Trong tình hình khó khăn khi đối đầu 
với sức mạnh áp đảo của các Tinh Linh,
cậu gần như là cách duy nhất cản được họ.

368
00:10:59,112-->00:11:09,113
Phong ấn được thực hiện bằng cách
khiến họ yêu cậu. Sau đó cậu sẽ 
phong ấn bằng một nụ hôn.

368
00:11:10,112-->00:11:25,113
Hiện nay, ngoại trừ Tokisaki Kurumi, 
cậu đã thành công trong việc phong ấn
Yatogami Tohka, Yoshino, Itsuka Kotori, 
chị em Yamai, Izayoi Miku.

368
00:11:26,112-->00:11:44,113
Cậu có thể sử dụng một phần sức mạnh
đã phong ấn, thể hiện qua khả năng hồi phục 
của Efreet, điều khiển băng của Hermit và 
triệu hồi Sandalphon.

368
00:11:46,112-->00:11:53,113
Tobiichi Origami, bạn cùng lớp của 
Itsuka Shidou, học sinh có năng khiếu 
cả về vẻ đẹp lẫn trí thông minh.

368
00:11:54,112-->00:12:03,113
Cô là thành viên của đội đặc nhiệm 
chống Tinh Linh AST.


368
00:12:04,112-->00:12:12,113
Cô ấy có thể điều khiển White Licorice.
Dù trong khoảng thời gian ngắn nhưng 
cũng gây ra tổn thương lớn cho não bộ.

368
00:12:12,512-->00:12:18,113
Đó là thiết bị CR-Unit có sức mạnh 
rất lớn để tiêu diệt Tinh Linh.

368
00:12:20,112-->00:12:26,113
Cô có tính cách ít nói và không biểu lộ 
cảm xúc, nhưng cũng luôn tin tưởng 
vào những gì mình muốn tin. 

368
00:12:27,112-->00:12:34,113
Cô có một sự quan tâm đặc biệt đến
Itsuka Shidou, người mà cô yêu thương nhất.

368
00:12:36,112-->00:12:42,113
Takamiya Mana. Thành viên AST, người được 
giao nhiệm vụ hỗ trợ để các đơn vị 
đồn trú tại thành phố Tengu của DEM.

368
00:12:42,512-->00:12:53,113
Cấp bậc trung úy, nhưng sức mạnh 
chiến đấu của cô tốt nhất của AST.

368
00:12:55,112-->00:13:05,113
Cô đã bị thương trong trận chiến 
với Nightmare và được điều trị tại Tengu.

368
00:13:06,112-->00:13:17,113
Tuy nhiên, cô đã rời khỏi DEM để 
ngăn cản trận chiến xảy ra trong Lễ hội Ten'ou.
Cô đã trở thành thành viên của Ratatoskr.

368
00:13:19,112-->00:13:24,113
cách biểu hiện của cô rất đặc biệt,
cô sử dụng từ ngữ tôn trọng đến kỳ lạ.

368
00:13:25,112-->00:13:34,113
Mặc dù cả hai không có ký ức thời thơ ấu, 
cô vẫn nghĩ mình là em gái ruột của 
Itsuka Shidou và gọi cậu là "Nii-sama"

368
00:13:37,112-->00:13:48,113
Okamine Mikie là đồng đội của 
Origami trong AST. Ngoài ra, 
cô là fan hâm mộ của Origami trong AST.

368
00:13:50,112-->00:14:02,113
Cô nhỏ hơn Origami 1 tuổi.
Tuy nhiên, do phải hoạt động với Origami,
cô bỏ qua 1 năm và học tại trường Raizen.

368
00:14:03,112-->00:14:19,113
Tính cách của cô vui vẻ và hòa đồng. 
Cô có mối quan hệ cháu gái/dì với Okamine Tamae, 
giáo viên chủ nhiệm lớp Itsuka Shidou, 
Yatogami Tohka và Tobiichi Origami.

368
00:14:22,112-->00:14:26,113
Tonomachi Hiroto. Bạn cùng lớp
Itsuka Shidou và có nhiều tật xấu.

368
00:14:27,112-->00:14:36,113
Trong "Bảng xếp hạng những người 
muốn có bạn trai", cậu ở vị trí 358/358.

368
00:14:38,112-->00:14:50,113
Yamabuki Ai, Hazakura Mai, Fujibakama Mii.
Họ là bạn cùng lớp với Itsuka Shidou.
Họ chơi với nhau cùng với Yatogami Tohka.

368
00:14:52,112-->00:14:56,113
Okamine Tamae. Cô là giáo viên
chủ nhiệm của lớp Itsuka Shidou.

368
00:14:56,512-->00:15:04,113
Học trò hay gọi cô là Tama-chan.
Cô 29 tuổi, độc thân.

368
00:15:06,112-->00:15:12,113
Ratatoskr. Tổ chức bí mật cố gắng
nói chuyện và chung sống hoà bình 
với các Tinh Linh. 

368
00:15:12,512-->00:15:21,513
Nó hoạt động dựa trên Fraxinus, 
một chiếc phi thuyền ở vị trí 15.000M 
trên bầu trời thành phố Tengu.

368
00:15:22,112-->00:15:26,113 
Nó được quản lý bởi một 
hội đồng chỉ huy cao cấp.

368
00:15:27,112-->00:15:33,113
Itsuka Kotori là người chịu trách nhiệm 
chỉ đạo kế hoạch, tập hợp mọi người 
nhằm bảo vệ Tinh Linh.

368
00:15:34,112-->00:15:40,113
Hiện nay, Itsuka Shidou là cách duy nhất 
có thể hòa giải với Tinh Linh 
mà không sử dụng vũ lực. 

368
00:15:40,512-->00:15:46,113
Về cơ bản, họ là một tổ chức 
hỗ trợ việc tiếp cận Tinh Linh.

368
00:15:47,112-->00:16:05,113
AST. Đơn vị được tổ chức bởi các quốc gia 
để đối mặt với mối đe dọa từ Tinh Linh. 
Họ thường được gọi là (Anti-Spirit Team).
Nhiệm vụ là tiêu diệt Tinh Linh bằng vũ lực.

368
00:16:06,112-->00:16:19,113
Các thành viên đều phù hợp với CR-Unit, 
thiết bị sử dụng trong trận chiến
cho phép sử dụng vũ khí theo ý muốn.

368
00:16:20,112-->00:16:30,113
Nó sẽ tạo ra một khu vực xung quanh,
giúp họ có thể dùng những vũ khí
trong tưởng tượng để chiến đấu.

368
00:16:31,112-->00:16:40,113
Những cá nhân đủ điều kiện được
tuyển chọn ở khắp thế giới. Do đó,
thậm chí người chưa đủ tuổi vị thành niên 
đôi khi cũng buộc tham gia làm thành viên.

368
00:16:41,112-->00:16:46,513
Tập đoàn Deus Ex Machina. 
Còn được gọi là DEM Corp.

368
00:16:47,112-->00:16:57,113
Họ là một tập đoàn quốc tế lớn
có trụ sở chính đặt tại Vương quốc Anh.
Họ sở hữu nhiều thị trường sản xuất vũ khí.

368
00:16:58,112-->00:17:03,513
Vì lý do đó, họ có mối quan hệ với 
các lực lượng vũ trang, cả JSDF và quốc tế.

368
00:17:04,112-->00:17:11,113
Tuy nhiên, để đạt được mục tiêu bằng 
mọi giá, hành động của họ đôi khi vô nhân đạo.

368
00:17:12,112-->00:17:26,113
Các chuyên gia và những người đủ 
điều kiện để sử dụng CR-Unit được
tìm kiếm trên khắp thế giới.

368
00:17:27,112-->00:17:31,113
"Thiên tai xuất hiện trên thế giới...
Không gian chấn".

368
00:17:32,112-->00:17:43,113
"Hiện tượng này xóa sạch không thương tiếc
mọi thứ tại đó, là hậu quả của Tinh Linh 
xuất hiện từ một thế giới khác"

368
00:17:44,112-->00:17:53,113
"Cách tốt nhất để loại bỏ thảm họa
Không gian chân là phong ấn 
Tinh Linh bởi Itsuka Shidou"

368
00:17:54,112-->00:18:01,113
"Điều kiện cần thiết để phong ấn
sức mạnh Tinh Linh... Đó là 
làm cho họ biết yêu."

368
00:18:02,112-->00:18:09,113
"Điều gì khiến họ yêu?
Nó được hiểu là yêu thương ai đó."

368
00:18:10,112-->00:18:22,113
"Làm sao để yêu thương ai đó?
Tìm kiếm... không tìm thấy dữ liệu chính xác. 
Thay đổi mục tiêu tìm kiếm cho 'tình yêu' "

368
00:18:24,112-->00:18:31,113
"Tìm kiếm gián đoạn. Hệ thống bị lỗi."

368
00:18:39,112-->00:18:41,113
Hôm nay thật đặc biệt. Mùa thu đã đến rồi.

368
00:18:42,112-->00:18:45,113
Tôi đang trên đường về nhà sau khi 
kết thúc một ngày đi học. Tôi đang 
nói chuyện với suy nghĩ trong đầu.

368
00:18:46,112-->00:18:49,113
Các học sinh khác đã về nhà 
và thay đồng phục.

368
00:18:50,112-->00:18:54,113
Sau vụ việc xảy ra ở Lễ hội Ten'ou, 
Mọi thứ đều yên ổn và hòa bình.
Chúng tôi về với ngôi nhà của mình nhưng...

368
00:18:56,112-->00:18:58,113
Tôi, Itsuka Shidou, hiện giờ 
đang có một cảm giác bất an.

368
00:18:59,112-->00:19:05,113
Chuyện này là sao vậy Shidou?
Giải thích đi!  

368
00:19:07,112-->00:19:12,113
Những gì cậu nói sẽ quyết định 
có thương vong hay không.

368
00:19:13,112-->00:19:15,113
Ý cậu là sao? Tớ không có gì 
để giải thích cả...

368
00:19:18,112-->00:19:28,113
Lừa bọn này vô ích thôi. Shidou, 
cởi mặt nạ ngây thơ và nói sự thật đi.

368
00:19:30,112-->00:19:36,113
Đồng ý. Như Kaguya nói, tớ yêu cầu 
một lời giải thích thỏa đáng.

368
00:19:38,112-->00:19:40,113
Không, nhưng nếu tớ không thể thực hiện...

368
00:19:42,112-->00:19:50,113
Nếu vậy thì...
Darling, hãy hẹn hò với em đi. Hẹn hò!

368
00:19:54,112-->00:19:59,113
Shidou, hãy đi ngay bây giờ. 
Nếu cậu không làm...

368
00:20:00,112-->00:20:02,513
Waaaaaaa! Cậu định làm gì 
ở một nơi như thế này?

368
00:20:03,112-->00:20:06,113
Tôi nhanh chóng ngăn Origami lại
khi cô ấy định làm gì đó dưới váy.

368
00:20:07,112-->00:20:17,113
Nhưng darling và em yêu nhau 
rất nhiều mà. Vậy nên hẹn hò là chuyện 
bình thường, phải không darling?

368
00:20:20,112-->00:20:26,113
Có vẻ cô muốn chết quá nhỉ.

368
00:20:27,112-->00:20:35,113
Hở? Trước kia cô yêu tôi nhiều lắm mà.
Cô còn gọi là "Aneue-sama" nữa.

368
00:20:36,112-->00:20:41,113
Giải thích. Kaguya đang ghen tị đấy.

368
00:20:42,112-->00:20:52,113
Không... không phải!
Aneue... Không, ý tôi là Miku,
cô đang dùng ngực để dụ dỗ đấy à?

368
00:20:53,112-->00:20:59,113
Tội nghiệp. Cô tức vì cô không có 
ngực to phải không?

368
00:21:00,112-->00:21:08,113
Yuzuru! Chỉ vì cô lớn hơn tôi một chút à...!

368
00:21:09,112-->00:21:16,113
Chính xác. Không phải một chút đâu.
Lớn hơn nhiều đấy.

368
00:21:17,112-->00:21:20,113
Này này! Chẳng lẽ tớ phải hẹn hò 
với tất cả mấy cậu sao? Giờ tớ phải làm gì đây?

368
00:21:24,112-->00:21:31,113
Tất nhiên là em rồi! 
Shidou, hãy hẹn hò với em đi!

368
00:21:34,112-->00:21:41,113
Tôi không đồng ý. Chỉ có bạn gái
của Shidou mới được hẹn hò với cậu ấy.

368
00:21:45,112-->00:21:53,113
Tôi không đồng ý! Cậu phải đi với tôi!
Shidou! Cùng đi ăn bánh mật ong nào!

368
00:21:55,112-->00:22:03,113
Tôi đồng ý. Như Kaguya nói.
Cả ba chúng ta cùng đến tiệm bánh
và ăn những cái bánh thật ngon nào.

368
00:22:06,112-->00:22:16,113
Em là người đầu tiên mời anh hẹn hò mà.
Anh sẽ chọn em chứ, darling?

368
00:22:18,112-->00:22:20,513
Tất cả đều muốn hẹn hò với tôi.
Nhưng nếu tôi phải chọn, tôi sẽ chọn...

368
00:22:21,112-->00:22:23,113
1) Hẹn hò với Tohka.
2) Hẹn hò với Origami.
3) Hẹn hò với chị em Yamai.
4) Hẹn hò với Miku.
5) Tiếp theo.