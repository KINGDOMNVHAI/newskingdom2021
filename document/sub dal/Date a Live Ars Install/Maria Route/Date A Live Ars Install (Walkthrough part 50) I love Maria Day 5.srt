﻿0
00:00:01,512-->00:00:09,113
"Bạn đồng hành", "bạn bè".
Mọi người đã gọi mình như thế.

1
00:00:10,112-->00:00:17,113
Shidou và mọi người nghĩ giống nhau.
Và mình cũng vậy.

2
00:00:18,112-->00:00:20,513
Mình cảm thấy có một sự ấm áp 
sâu trong lồng ngực.

2
00:00:21,112-->00:00:29,113
Đây là cảm giác được gọi là "hạnh phúc".
Điều đó mình đã học được.

3
00:00:30,112-->00:00:36,113
Và không chỉ có vậy.
Mình đã học được rất nhiều cảm xúc khác.

4
00:00:37,113-->00:00:44,513
Nhưng mình... nếu mình có thể 
giúp họ làm một điều gì đó.

5
00:00:45,512-->00:00:55,113
Bất cứ điều gì mình có thể giúp họ...
Ví dụ như... giúp họ ra khỏi thế giới này.

6
00:00:56,112-->00:01:07,113
Tuy nhiên, cách này không thể thực hiện.
Đó là lý do tại sao mình có họ 
đang bị nhốt trong thế giới này.

7
00:01:08,112-->00:01:18,113
Câu trả lời đó không có ở trong mình.
Những gì còn lại trong mình là...
hiểu được tình yêu.

8
00:01:19,112-->00:01:23,113
Phải hiểu được tình yêu.

13
00:01:24,113-->00:01:31,113
Các thông tin chính là chìa khóa.
Vì vậy mình phải tiếp tục.

14
00:01:32,112-->00:01:38,113
Để bản thân có thể hiểu được tình yêu,
mình phải đến giai đoạn tiếp theo.

15
00:01:39,112-->00:01:51,113
Giai đoạn tiếp theo... Đó là kết thúc cuộc hẹn.
Chắc chắn là... bằng một nụ hôn.

16
00:01:52,112-->00:02:06,113
Đúng vậy. Tất cả các cô gái sau khi 
làm chuyện ấy, họ đã có tình yêu với Shidou.
Do đó, kết thúc của buổi hẹn hò là... một nụ hôn.

17
00:02:07,112-->00:02:21,113
Nếu mình làm thế... chắc chắn mình sẽ hiểu được.
Nếu mình làm thế, mình có thể 
giúp Shidou ra khỏi thế giới này.

18
00:02:22,112-->00:02:27,113
Nhưng sau khi làm thế, 
mình có gặp chuyện gì không?

25
00:02:28,112-->00:02:39,113
Kotori đã nói mình là một thứ bất thường.
Khi thế giới này trở lại bình thường...
mình có thể sẽ biến mất.

25
00:02:40,112-->00:02:53,113
Tuy nhiên, mình muốn trở nên hữu ích.
Là một "bạn đồng hành" và là "bạn bè",
mọi người đều đối xử với mình
như một người ngang hàng.

25
00:02:54,112-->00:02:58,513
Và cũng là... vì Shidou...

25
00:02:59,512-->00:03:08,113
Vậy nên... chúng ta hãy bắt đầu.
Buổi hẹn hò... cuối cùng của chúng ta.

25
00:03:14,112-->00:03:22,113
Này Shidou, chưa có bữa sáng sao?

26
00:03:23,112-->00:03:25,113
Xin lỗi! Em chờ thêm tí nữa đi!

26
00:03:26,112-->00:03:29,113
Trong phòng khách nhà tôi, 
họ đang chờ đợi bữa ăn sáng. 
Phải chuẩn bị bữa ăn cho nhiều người 
là một công việc rất nặng. Nhưng...

26
00:03:30,112-->00:03:34,113
Shidou. Để tớ giúp cậu.

26
00:03:35,112-->00:03:36,513
Cảm ơn Maria. Tớ sắp xong rồi.

26
00:03:37,112-->00:03:39,113
Nhờ Maria giúp tôi,
dường như tất cả đều suôn sẻ.

26
00:03:40,112-->00:03:42,113
Xong rồi.
Này mọi người, bữa sáng xong rồi đấy!

26
00:03:43,512-->00:03:46,513
Em đợi lâu lắm rồi đấy.

27
00:03:48,112-->00:03:55,113
Tohka-san, cô thật sự rất tham ăn phải không?
Tôi rất ấn tượng đấy.

28
00:03:56,112-->00:04:01,113
Nếu cô khen tôi, tôi xấu hổ lắm đấy.

38
00:04:02,112-->00:04:08,113
Em không nghĩ... đó là một lời khen đâu...

39
00:04:09,112-->00:04:14,113
Tohka-chan đang rất vui.
Vậy nên không sao đâu.

40
00:04:16,113-->00:04:22,113
Cùng với ánh bình minh, 
món này giống như mặt trời vậy.

40
00:04:22,513-->00:04:26,113
Có vẻ như năng lượng mặt trời 
có thể làm tan chảy mọi thứ.

41
00:04:27,113-->00:04:33,113
Đồng ý. Những quả trứng chiên trông rất ngon.

42
00:04:34,113-->00:04:36,513
Vậy à? Trứng chiên của Maria đấy.
Cậu có thấy vui không, Maria?

43
00:04:38,112-->00:04:40,113
Vâng. Tớ rất vui.

39
00:04:41,512-->00:04:51,113
Đồ ăn của Shidou luôn ngon nhất. 
Nếu món này được vào nhà hàng Pháp, 
nó xứng đáng xếp loại 7 sao.
(Xem thêm tại "Michelin Guide")

40
00:04:51,513-->00:04:54,113
Không, không! Tối đa là 3 sao thôi mà.
Nếu là 7 sao thì sẽ trông như
người có vết sẹo trên ngực mình!
(Xem thêm trong phim "Fist of the North Star")

41
00:04:54,513-->00:04:58,113
Điều đó có nghĩa là tuyệt vời.

42
00:04:59,113-->00:05:07,113
Quan trọng là chúng ta không ăn nhanh,
món ăn của Darling và Maria-chan sẽ nguội mất.

43
00:05:08,112-->00:05:14,113
Như thế không ổn phải không?
Đồ ăn nên ăn khi ngon nhất.

44
00:05:15,112-->00:05:19,113
Arusu-chan cũng đã trở thành 
một người tham ăn phải không?

45
00:05:20,112-->00:05:22,113
Maria chỉ nói thế thôi.
Giờ thì ăn thôi. Itadakimasu!

46
00:05:23,112-->00:05:26,513
Ừ! Itadakimasu!

47
00:05:31,112-->00:05:37,513
Bữa sáng ngon quá! Cảm ơn vì bữa ăn!

48
00:05:39,112-->00:05:49,113
Darling, anh nấu ăn ngon lắm.
Hôm nay anh có muốn hẹn hò với em không?

49
00:05:50,112-->00:05:56,113
Cô nói gì hả Miku?
Shidou phải hẹn hò với tôi!

50
00:05:57,512-->00:06:03,513
Em... em cũng muốn...

51
00:06:04,112-->00:06:09,113
Đúng vậy!
Yoshino cũng muốn hẹn hò với Shidou-kun!

41
00:06:10,512-->00:06:22,513
Vậy cậu có muốn hẹn hò với tớ không?
Các cô gái khác không cho cậu
làm-chuyện-người-lớn như tớ đâu.

42
00:06:23,512-->00:06:34,113
Shidou thuộc sở hữu của bọn tôi, Yamai.
Cùng tiến hành những nghi lễ của niềm vui nào.

43
00:06:35,113-->00:06:40,513
Đồng tình. Thật không công bằng khi 
cậu chỉ hẹn hò với Maria.

43
00:06:41,113-->00:06:47,113
Tớ muốn cậu cũng phải chú ý đến tớ.
Đó là những gì Kaguya nói tối qua.

44
00:06:48,112-->00:06:51,113
Tôi không nói thế!

45
00:06:52,112-->00:06:59,513
Nhiệm vụ hẹn hò ngày hôm nay 
phải là của tôi và Shidou.

46
00:07:01,112-->00:07:06,513
Onii-chan, đôi lúc anh cũng phải
hẹn hò với em chứ.

47
00:07:07,113-->00:07:09,112
Cái gì vậy? Hiếm khi nào em nói thế đấy.

48
00:07:09,512-->00:07:18,113
Đùa thôi. Nghe này các cô. 
Các cô không quên việc quan trọng đấy chứ?

49
00:07:19,112-->00:07:21,513
Việc quan trọng sao?

50
00:07:22,512-->00:07:32,513
Có chuyện đó sao? Quay lại vấn đề 
ai sẽ là người hẹn hò với Darling.
Hôm nay cũng sẽ lại hẹn hò nhóm nữa à?

60
00:07:33,112-->00:07:35,113
Tôi đồng ý.

61
00:07:36,112-->00:07:39,113
Nghe này, mọi người...

62
00:07:40,112-->00:07:45,113
Không được.
Shidou đã hứa hôm nay sẽ hẹn hò với tôi.

63
00:07:47,112-->00:07:50,113
Thật không, Shidou?

64
00:07:51,112-->00:07:52,513
Hở? Um...

65
00:07:53,512-->00:08:02,113
Chỉ hôm nay thôi... Tôi mong mọi người
đồng ý cho tôi hẹn hò với Shidou. Làm ơn đấy.

66
00:08:03,113-->00:08:04,513
Ma... Maria?

67
00:08:05,112-->00:08:10,513
Đi nào Shidou. Đây sẽ là...

68
00:08:11,112-->00:08:15,113
... buổi hẹn cuối của chúng ta.

81
00:08:16,112-->00:08:17,513
Hở? Cậu vừa nói gì?

82
00:08:18,112-->00:08:22,113
Không có gì đâu. Đi nào.

83
00:08:23,512-->00:08:25,113
Này này!

84
00:08:25,512-->00:08:28,113
Maria kéo tay tôi rất mạnh.
Tôi cảm thấy giống như có sự quyết tâm.

85
00:08:28,512-->00:08:30,513
Tớ xin lỗi mọi người.
Maria có vẻ rất muốn đi với tớ.
Vậy nên tớ sẽ đi với cô ấy.

86
00:08:31,112-->00:08:37,513
Sau khi nhìn khuôn mặt nghiêm túc của Maria,
Em không thể nói gì cả.

87
00:08:38,512-->00:08:43,513
Tớ đồng ý.
Nhưng ngày mai thì không đâu đấy.

88
00:08:44,112-->00:08:46,113
Cố gắng... lên nhé!

94
00:08:47,112-->00:08:52,513
Hãy cố gắng làm hài lòng Maria-san nhé.

95
00:08:53,512-->00:08:59,113
Tôi đồng ý để cô đi với 
gia đình của tôi đấy, Maria.

96
00:09:00,112-->00:09:08,113
Đồng tình. Có vẻ như Maria đang cố gắng
thực hiện điều gì đó thực sự quan trọng.

97
00:09:09,112-->00:09:21,113
Đúng vậy, cô ấy có vẻ có điều gì
quan trọng lắm. Vì vậy hôm nay 
anh phải giúp Maria-chan đấy.

98
00:09:22,112-->00:09:25,513
Đúng đấy. Cố lên nhé, Shidou.

99
00:09:27,112-->00:09:33,113
Mọi người, cảm ơn rất nhiều.
Cảm ơn đã đồng ý với mong muốn của tôi.

100
00:09:34,112-->00:09:35,513
Tớ cũng rất biết ơn các cậu.

101
00:09:41,112-->00:09:42,513
Nhưng tớ rất ngạc nhiên đấy.

102
00:09:43,112-->00:09:47,513
Xin lỗi. Chỉ ngày hôm nay thôi.

103
00:09:48,513-->00:09:50,113
Không, không sao. Nhưng ...

104
00:09:50,512-->00:09:53,113
Dường như có một bầu không khí 
lo lắng về điều gì đó. Tôi phải làm gì?
Tôi phải nói chuyện gì với cô ấy đây?

105
00:09:54,112-->00:10:05,113
1) Có chuyện gì xảy ra à?
2) Được rồi! Đi nào!

106
00:10:06,112-->00:10:07,513
Maria, có chuyện gì vậy?

107
00:10:08,112-->00:10:12,113
Chuyện gì cơ? Ý cậu là sao?

113
00:10:12,512-->00:10:14,113
Chỉ là... trông vẻ mặt cậu lạ lắm.

114
00:10:15,112-->00:10:22,113
Lạ lắm sao? Tớ làm gì sai à?

1
00:10:23,112-->00:10:25,113
Trông cậu giống như là...
đang có chuyện gì buồn phiền vậy.

1
00:10:26,112-->00:10:33,113
Không. Không có gì đâu.
Tớ chỉ muốn hẹn hò với cậu thôi.

1
00:10:34,112-->00:10:36,113
Ừ, được rồi. 
Xin lỗi, chắc do tớ tưởng tượng.

1
00:10:36,513-->00:10:39,513
Nhưng dù vậy, tôi vẫn cảm thấy có chuyện gì đó. 
Có lẽ cô ấy không muốn nói. 
Bây giờ, tôi chỉ cần đi với cô ấy thôi.

1
00:10:40,112-->00:10:42,513
Vậy giờ bắt đầu buổi hẹn thôi.
Cậu muốn đi đâu đây, Maria?

1
00:10:43,112-->00:10:45,513
Chúng ta đi nhiều nơi lắm.

1
00:10:46,112-->00:10:47,513
Nhiều nơi sao? Rắc rối thế nhỉ.

368
00:10:48,112-->00:11:00,113
Không cần phải đi đâu đó đặc biệt đâu. 
Tớ chỉ muốn... đến những nơi
mà mọi ngày tớ vẫn đến cùng cậu.

368
00:11:01,112-->00:11:03,113
Maria? Được rồi.
Hãy đi càng nhiều nơi càng tốt nhé!

368
00:11:04,112-->00:11:05,113
Vâng!

368
00:11:09,512-->00:11:11,113
Các cửa hàng à?
Dạng như cửa hàng quần áo hoặc tạp hóa à?

368
00:11:12,112-->00:11:19,113
Vâng. Chúng ta sẽ đến những nơi cậu thường đi.

68
00:11:20,112-->00:11:23,113
Cửa hàng tớ thường đi à? 
Um ... Có lẽ là nhà sách... hoặc siêu thị.

368
00:11:24,112-->00:11:27,113
Đây là nơi bán đồ ăn phải không?

368
00:11:28,112-->00:11:30,513
Ừ. Giờ tớ nghĩ, tớ đã từng đến đây rồi nhỉ?

368
00:11:31,112-->00:11:34,113
Chắc chắn tôi có những ký ức về nó.
Nhưng tôi không thể nhớ rõ.
Đó là một cảm giác khá kỳ lạ.

368
00:11:35,112-->00:11:44,113
Xin lỗi. Tớ không để ý.
Trong thế giới này có nhiều 
loại sự kiện khác nhau lắm.

68
00:11:45,112-->00:11:48,113
À, nghĩa là cậu đã khởi động lại trò chơi à? 
Thảo nào cậu đã có kinh nghiệm từ trước. 
Có lẽ một cô gái mà không biết nấu ăn thì...

368
00:11:49,112-->00:11:54,113
Nhìn kìa, Shidou.
Nó là một Yatai, phải không?
(Yatai: quán hàng rong)

368
00:11:55,112-->00:11:56,513
Họ bán Oobanyaki đấy. Cậu muốn thử không?

368
00:11:57,112-->00:12:07,113
Ừ, tớ muốn thử. Nhưng tớ thấy
vẫn còn nhiều điều tớ không biết phải không?
Về những món ăn chẳng hạn.

368
00:12:08,112-->00:12:11,113
Đúng vậy. Giờ tớ mua 2 cái Oobanyaki 
rồi chia nhau nhé.

368
00:12:11,512-->00:12:19,113
Vâng. Nếu chúng ta chia nhau, 
chúng ta có thể ăn nhiều hơn phải không?

368
00:12:19,512-->00:12:22,113
Nó giống với những gì chúng ta 
đã làm trong quán cà phê.

368
00:12:22,513-->00:12:25,113
Haha... Vậy à?
Nếu là Kotori, tớ chỉ ăn phần tớ thích,
còn lại sẽ nhường cho con bé hết.

368
00:12:26,113-->00:12:35,113
Thế à? Đúng là Kotori nhỉ.
Chúng ta đi thôi, Shidou.

368
00:12:37,113-->00:12:39,113
Này này, không cần phải vội vàng thế!

368
00:12:43,512-->00:12:45,113
Chúng ta mới đi mua quần áo mà?

368
00:12:46,512-->00:12:52,113
Vâng. Từ lúc đó, tớ cảm thấy 
tớ thích đến đây để ngắm nhiều bộ quần áo hơn.

368
00:12:53,112-->00:12:54,513
Cậu nói cũng đúng.

368
00:12:55,512-->00:13:01,113
Tại sao tớ cảm thấy muốn có 
những bộ đồ dễ thương này chứ?
Cảm giác đó vẫn tồn tại đến bây giờ.

368
00:13:02,112-->00:13:04,113
Ừ. Nhưng này, cậu có thể đến mua 
vào lần khác mà phải không?
Chúng ta vẫn còn nhiều nơi để đi mà.

368
00:13:05,112-->00:13:08,113
Cậu nói đúng...

368
00:13:09,113-->00:13:10,513
Hở? Sao vậy?

368
00:13:11,112-->00:13:13,513
Không, không có gì đâu.

368
00:13:19,112-->00:13:22,113
Ở đây có nhiều con gái nhỉ.

368
00:13:22,512-->00:13:24,113
Đúng vậy. Con gái thường thích mấy thứ
dễ thương như mấy phụ kiện này mà.

368
00:13:25,112-->00:13:32,113
Phải. Tớ không biết tại sao,
nhưng tớ cảm thấy hạnh phúc khi quan sát họ.

368
00:13:32,512-->00:13:34,113
Vậy cậu có muốn mua một gì không?
Miễn là cậu thích là được.

368
00:13:35,112-->00:13:38,113
Không, thế này là đủ rồi.

368
00:13:38,512-->00:13:40,513
Thật sao? Cậu không muốn mua gì à?

368
00:13:41,512-->00:13:44,513
Hôm nay tớ muốn thứ khác rồi.

368
00:13:45,512-->00:13:47,113
Vậy à? Vậy sao chúng ta 
không mua nó bây giờ đi?

368
00:13:48,112-->00:13:55,113
Đây là một bí mật. Khi nào đến lúc,
tớ sẽ nói cho cậu biết.

368
00:13:56,112-->00:13:58,113
Hở? Tớ không hiểu lắm 
nhưng nếu cậu muốn thế thì được.

368
00:13:59,112-->00:14:04,113
Vâng, chúng ta đi chỗ khác đi.
Chúng ta không còn nhiều thời gian đâu.

368
00:14:06,512-->00:14:08,113
Này... Chờ chút, Maria.
Đừng chạy nhanh như vậy!

368
00:14:12,512-->00:14:15,113
Phù. Chúng ta đã đi lang thang khắp nơi.
Nhưng thực sự cậu không muốn mua gì sao, Maria?

368
00:14:16,112-->00:14:20,513
Đúng vậy. Chỉ cần xem thôi,
tớ cũng đã thấy vui rồi.

368
00:14:21,112-->00:14:23,113
Ừ, tớ hiểu cảm giác đó.

368
00:14:24,112-->00:14:26,113
Cậu cũng vui chứ, Shidou?

368
00:14:26,512-->00:14:29,513
Có chứ. Khi đến những nơi này,
tớ có thể chú ý đến những thứ mà
bình thường tớ không để ý đến.

368
00:14:30,512-->00:14:38,513
Đúng vậy. Ngoài ra còn có rất nhiều điều 
không biết cả về người bán lẫn người mua nữa.

368
00:14:39,512-->00:14:43,113
Những thứ nhỏ nhặt đó 
có nhiều điều thú vị đến thế sao?

368
00:14:44,112-->00:14:53,113
Vâng. Tớ thấy vậy. 
Nhưng Shidou cũng suy nghĩ giống tớ. 
Thật kỳ lạ, phải không?

368
00:14:54,112-->00:14:56,113
Thật sao? Đó không phải điều hiển nhiên
khi hai người đi bộ cùng nhau sao?

368
00:14:57,112-->00:15:05,113
Đối với tớ, tất cả mọi thứ 
là những thông tin mới. 
Nhưng cậu thì đã biết quá rõ rồi.

368
00:15:06,112-->00:15:09,513
Đúng vậy. Nhưng tớ chưa bao giờ
quan sát xung quanh kỹ càng như bây giờ.
Vì vậy, có nhiều thứ mà tớ không để ý.

368
00:15:10,512-->00:15:19,113
Tớ vui vì điều đó.
Tớ vui vì được chia sẻ niềm vui với cậu.

368
00:15:20,112-->00:15:21,513
Điều đó làm cậu vui sao?

368
00:15:22,112-->00:15:27,113
Đúng vậy. Đến bây giờ,
tớ chưa trải qua điều gì tương tự cả.

368
00:15:28,112-->00:15:30,113
Ra vậy. Tớ đã hoàn toàn quên mất.

368
00:15:30,512-->00:15:33,113
Cậu quên gì cơ?

368
00:15:34,112-->00:15:37,113
Đó là Maria rất đặc biệt. 
Đối với tớ, cậu bình thường như mọi cô gái khác.

368
00:15:38,112-->00:15:41,113
Một cô gái... bình thường...

368
00:15:42,112-->00:15:44,113
Đúng vậy. Thật sự, dù cậu rất đặc biệt,
cậu lại rất bình thường như mọi cô gái khác.

368
00:15:44,512-->00:15:50,113
Mình là... một cô gái... bình thường...

368
00:15:51,113-->00:15:52,513
Có gì sao, Maria?

368
00:15:53,112-->00:15:56,113
Không, không có gì đâu!

368
00:15:57,112-->00:15:59,113
Vậy chúng ta làm gì bây giờ?
Đi xem phim nhé?

368
00:16:00,112-->00:16:06,513
Chúng ta hãy đi đến những nơi khác, 
tất cả những nơi mà chúng ta có thể đi.
Được không?

368
00:16:07,112-->00:16:08,513
Cậu thật sự muốn thế à?

368
00:16:09,112-->00:16:15,113
Vâng. Đi dạo vòng quanh thành phố với cậu
là mục tiêu của cuộc hẹn hôm nay.

368
00:16:16,112-->00:16:17,513
Tớ hiểu rồi.

368
00:16:18,112-->00:16:22,113
Vậy chúng ta đi thôi.

368
00:16:29,112-->00:16:31,113
Shidou, một con mèo kìa.

368
00:16:32,112-->00:16:34,113
Phải. Một con mèo đeo vòng cổ.
Nó phải là mèo của ai đó.

368
00:16:37,112-->00:16:39,513
Shidou, nó lại gần kìa.

368
00:16:40,112-->00:16:42,113
Ừ. Có vẻ nó thân thiện với con người.

368
00:16:46,112-->00:16:48,513
Chúng ta có thể làm gì đây?

368
00:16:49,112-->00:16:51,513
Tớ nghĩ chúng ta chỉ cần âu yếm nó thôi.

368
00:16:52,112-->00:17:00,113
Vuốt ve... Được rồi, tớ sẽ thử...
Ngoan nào, ngoan nào...

368
00:17:07,112-->00:17:10,113
Con mèo được Maria vuốt ve.
Sau đó, nó quay đuôi và đi lặng lẽ.

368
00:17:11,112-->00:17:13,513
Tớ làm tốt chứ?

368
00:17:14,112-->00:17:16,113
Tớ nghĩ vậy. Trông nó có vẻ rất vui.

368
00:17:16,512-->00:17:18,113
Thật may quá...

368
00:17:22,512-->00:17:25,113
Tớ gần như chưa bao giờ đến đây.
Chỗ này lạ quá nhỉ.

368
00:17:25,512-->00:17:28,513
Có vẻ chúng ta đã đi hết rồi phải không?

368
00:17:29,512-->00:17:32,113
Đúng vậy. Chúng ta đi nhiều lắm rồi.

368
00:17:32,512-->00:17:40,113
Shidou, tớ sẽ rất mừng 
nếu cậu cảm thấy vui đấy.

368
00:17:40,512-->00:17:43,113
Thật sao?
Tớ thấy ngại khi nói điều đó.

368
00:17:44,112-->00:17:48,113
Tớ còn biết nhiều nơi trong thành phố lắm.

368
00:17:49,112-->00:17:51,513
Ừ. Tớ chưa bao giờ đưa cậu đến 
những nơi đó cả. Sao cậu biết, Maria?

368
00:17:52,112-->00:18:00,113
Vâng. Tớ biết mọi nơi trên bản đồ.
Nhưng tớ chưa bao giờ đến đó cả.

368
00:18:00,513-->00:18:02,513
Được rồi. Vậy chúng ta đi đến đó đi.

368
00:18:03,112-->00:18:08,513
Ừ, đến đó đi. Nơi thú vị đó 
sẽ chỉ có hai chúng ta thôi.

368
00:18:09,512-->00:18:11,513
Và sau đó, Maria và tôi cùng nhau
đi khắp mọi nơi trong thành phố, 
cả những nơi tôi không biết.

368
00:18:12,112-->00:18:15,113
Bằng cách đó, dù chỉ là đi dạo,
nó vẫn có rất nhiều thứ mới lạ, hấp dẫn.

368
00:18:18,512-->00:18:20,113
Hôm nay chúng ta đã đi khắp nơi rồi nhỉ.

368
00:18:21,112-->00:18:25,113
Đúng vậy. Trời đã tối rồi.

368
00:18:26,112-->00:18:28,113
Ừ. Có thể ngắm rõ các ngôi sao,
đây là nơi lý tưởng để kết thúc một cuộc hẹn.

368
00:18:29,112-->00:18:34,113
Phải. Chỉ cần có vậy,
Tớ có thể thư giãn hoàn toàn.

368
00:18:35,112-->00:18:36,513
Giờ tôi mới nhớ, cô ấy rất thích ngắm sao.
Nơi này là nơi thích hợp nhất đấy.

368
00:18:37,112-->00:18:38,513
Chúng thật đẹp phải không?

368
00:18:39,112-->00:18:43,113
Phải. Chúng làm tớ muốn tiếp tục 
nhìn ngắm chúng mãi mãi.

368
00:18:43,512-->00:18:45,513
Lần tới, có lẽ chúng ta sẽ mang
một chiếc kính thiên văn lên đây.

368
00:18:47,512-->00:18:49,513
Đúng vậy.

368
00:18:50,113-->00:18:51,513
Maria? Cậu lạnh à?

368
00:18:52,112-->00:18:59,513
Ừ. Gió mát nhưng hơi lạnh.
Đang là ban đêm rồi mà.

368
00:19:00,113-->00:19:01,513
Đúng vậy. Đây, thấy đỡ hơn chưa?

368
00:19:03,112-->00:19:04,513
Shidou...

368
00:19:05,112-->00:19:06,113
Hở?

368
00:19:07,112-->00:19:10,113
Hôm nay chúng ta đã hẹn hò cả ngày rồi.

368
00:19:10,512-->00:19:11,513
Ừ. Tớ rất thích buổi hẹn hôm nay.

368
00:19:12,112-->00:19:21,113
Này Shidou. Tớ có thể nói... 
điều ước cuối cùng của tớ chứ?

368
00:19:22,112-->00:19:25,113
Tất nhiên rồi. Tohka và mọi người 
cũng đã nói tớ phải lắng nghe cậu.
Nếu không, họ sẽ giận đấy.

368
00:19:25,512-->00:19:40,113
Thật sao? Nhưng Shidou... 
Đây chỉ là một điều ước đơn giản
vì đó chỉ là một việc để kết thúc cuộc hẹn.

368
00:19:41,112-->00:19:42,513
Thực hiện điều ước cuối cùng sao?
Là gì vậy?

368
00:19:43,112-->00:19:47,513
Shidou, nếu cậu không thực hiện.
thì tớ không biết được đâu.

368
00:19:48,112-->00:19:57,113
Nghe này, Shidou.
Sau khi kết thúc buổi hẹn hò... 
cậu có thể hôn tớ không?

368
00:19:57,512-->00:19:58,513
Hở? À...

368
00:19:59,112-->00:20:01,113
Tình yêu và nụ hôn. 
Đúng vậy, đó chính là mục tiêu cuối cùng 
sau khi hẹn hò với Tinh Linh.

368
00:20:02,113-->00:20:05,113
Nếu Maria thực sự là một Tinh Linh,
Tôi sẽ phong ấn sức mạnh của cô ấy
và có lẽ tất cả chúng ta có thể thoát khỏi đây.

368
00:20:06,113-->00:20:08,513
Nhưng tôi tự hỏi, nếu Maria yêu tôi
và biết điều này thì sao?

368
00:20:09,113-->00:20:12,113
Không, cô ấy có thể không biết
và cô ấy nên hiểu cho chúng tôi.

368
00:20:12,513-->00:20:15,513
Thực tế, cô ấy không có cơ thể nên có khả năng 
sau khi phong ấn, cô ấy sẽ biến mất.

368
00:20:16,512-->00:20:18,513
Nếu tôi hôn cô ấy, đây có thể là
lần cuối tôi được nhìn thấy Maria.

368
00:20:19,112-->00:20:21,113
Tôi phải làm gì đây?

368
00:20:22,112-->00:20:27,113
1) Hôn cô ấy
2) Không hôn cô ấy

368
00:20:23,112-->00:20:26,113


368
00:20:26,912-->00:20:28,113



