﻿0
00:00:03,512-->00:00:05,113
Um... Trời sáng rồi.

1
00:00:05,512-->00:00:07,113
Hôm nay cũng là một ngày đẹp trời.

2
00:00:08,112-->00:00:10,113
Dậy đi nào, Onii-chan...

3
00:00:11,112-->00:00:14,113
Anh dậy rồi à!

4
00:00:15,112-->00:00:17,113
Chào buổi sáng, Kotori.

5
00:00:17,512-->00:00:22,113
Chào buổi sáng, Onii-chan.
Em đang định gọi anh dậy, nhưng...

6
00:00:23,112-->00:00:25,113
Không cần đâu Kotori.
Hôm nay anh phải dậy đúng giờ mà.

7
00:00:26,112-->00:00:29,113
Đó là vì hôm nay chúng ta
sẽ đi chơi với mọi người phải không?

8
00:00:30,112-->00:00:32,113
Đúng vậy. Hôm nay tôi sẽ đến công viên giải trí 
với những cô gái đã không đi hôm qua. 
Vì thế nên tôi dậy sớm.

9
00:00:33,112-->00:00:35,113
Trông em đã sẵn sàng rồi nhỉ, Kotori.
Em có thấy Maria đâu không?

10
00:00:36,112-->00:00:38,513
Cô ấy ở dưới nhà rồi.

11
00:00:39,112-->00:00:41,113
Hiểu rồi. Cô ấy cũng đang háo hức đấy.

12
00:00:41,512-->00:00:44,113
Đúng vậy. Chúng tôi chưa bao giờ đến
công viên giải trí cùng nhau cả.
Vậy nên cô ấy rất háo hức.

13
00:00:45,112-->00:00:52,113
Onii-chan, anh cảm thấy buồn vì 
không được ngủ chung với Maria sao? Hentai!

14
00:00:52,512-->00:00:54,113
Không có chuyện đó đâu!

14
00:00:54,512-->00:00:56,513
Sự thật là tôi đã nghĩ:
"Hôm nay cô ấy không ngủ chung nữa à?"

14
00:00:57,112-->00:01:00,113
Thật không đấy?

15
00:01:00,513-->00:01:02,513
Tất nhiên là thật rồi! Giờ xuống ăn sáng đi.

16
00:01:03,112-->00:01:04,513
Vâng!

17
00:01:10,112-->00:01:12,513
Chào buổi sáng, Shidou.

18
00:01:13,512-->00:01:15,513
Chào buổi sáng, Maria.
Hôm nay cậu cũng dậy sớm à?

19
00:01:16,112-->00:01:23,113
Hôm nay đi chơi nên 
tớ đã chuẩn bị bữa sáng. Cậu giúp tớ nhé.

20
00:01:24,112-->00:01:25,513
Ừ, cảm ơn cậu. Để tớ phụ.

21
00:01:26,112-->00:01:33,113
Tớ cũng chuẩn bị bento cho mọi người rồi.

22
00:01:33,512-->00:01:35,513
Ồ, có cả bento nữa à?
Cậu chuẩn bị kỹ quá nhỉ.

23
00:01:36,112-->00:01:39,113
Mặc dù cô ấy cố gắng không thể hiện 
cảm xúc của mình, nhưng tôi có thể thấy
Maria thực sự háo hức hơn tôi nghĩ.

24
00:01:40,112-->00:01:42,113
Này Maria, 
cậu đến công viên giải trí bao giờ chưa?

25
00:01:43,112-->00:01:51,113
Tớ có đủ thông tin về nơi đó.
Nhưng đây là lần đầu tiên tớ được đi đấy.

25
00:01:52,112-->00:01:54,513
Cậu nói đúng. Chúng ta sẽ có 
một ngày đi chơi tuyệt vời.
Giờ những người đi đang có tớ, cậu, Kotori...

25
00:01:55,512-->00:02:03,113
Onii-chan, đừng quên
Origami-san, Yoshino và Yuzuru nữa đấy!

25
00:02:03,512-->00:02:05,113
Đúng thế. Kotori, giờ anh mới để ý,
hôm nay em đeo nơ trắng à?

25
00:02:06,112-->00:02:08,513
Anh nói gì cơ?

25
00:02:09,112-->00:02:10,113
Không, không có gì.

25
00:02:10,512-->00:02:13,113
Điều đó có nghĩa là con bé muốn thật sự
vui vẻ trong công viên giải trí.
Nếu vậy, "chế độ chỉ huy" sẽ không làm việc.

25
00:02:18,112-->00:02:19,513
Giờ này mọi người đều đã đến rồi phải không?

25
00:02:20,112-->00:02:26,513
Vâng. Chúng ta ra ngoài chờ đi.
Như thế dễ nói chuyện hơn.

25
00:02:27,112-->00:02:28,513
Ừ, chúng ta đi thôi.

25
00:02:29,112-->00:02:30,113
Vâng!

25
00:02:36,112-->00:02:38,113
Maria, cậu định chơi gì ở công viên?

25
00:02:39,112-->00:02:52,113
Tớ cũng đã nghĩ đến vài chỗ.
Nhưng tớ không thể lựa chọn. 
Tớ muốn đi tất cả cơ.

25
00:02:53,112-->00:02:56,113
Tất cả à? Khoan đã!
Cậu muốn chơi hết tất cả...
Vậy là cậu muốn chơi cả những trò nhỏ à?

25
00:02:57,112-->00:03:06,113
Tớ biết công viên giải trí rất đông người.
Nếu có thể, cậu có muốn tận hưởng 
những công viên giải trí không?

25
00:03:07,113-->00:03:09,513
Cậu nói đúng. Các cửa hàng 
và trò chơi nhỏ cũng khá thú vị.

25
00:03:10,113-->00:03:15,113
Vậy là không có vấn đề gì phải không?

25
00:03:16,113-->00:03:19,113
Không đâu. Ngoài ra, mọi người có thể
chơi nhiều trò khác nhau trong khi chờ mà.

26
00:03:20,112-->00:03:23,113
Vâng. Tớ rất háo hức đấy.

26
00:03:24,112-->00:03:27,113
Phải. Tôi muốn Maria thưởng thức 
chuyến đi chơi này như một người bình thường.

26
00:03:28,112-->00:03:31,113
Chào buổi sáng...

26
00:03:32,112-->00:03:35,513
Mọi người chờ lâu không?

26
00:03:36,512-->00:03:38,113
Chào buổi sáng, Yoshino. 
Và cả em nữa, Yoshinon.

26
00:03:39,112-->00:03:46,113
Anh biết không? Yoshino hôm qua
vui sướng đến mức gần như không ngủ đấy.

26
00:03:47,112-->00:03:51,113
Vâng, đúng vậy...

26
00:03:52,112-->00:03:54,113
Anh hiểu. Anh cũng rất mong chờ.
Hôm nay sẽ là một ngày rất vui đấy.

26
00:03:55,112-->00:03:59,113
V... vâng. Em sẽ cố gắng.

26
00:04:00,112-->00:04:07,113
Phải. Cố lên nhé, Yoshino.
Cậu cũng có thể có gặp vài trở ngại đấy.

26
00:04:07,512-->00:04:09,513
Có vẻ họ đang có kế hoạch quậy phá nào đó.
Nhưng mọi chuyện sẽ ổn thôi.

26
00:04:10,112-->00:04:11,513
Còn Yuzuru và Origami... 
Họ không đến sao?

27
00:04:12,113-->00:04:17,513
Yuzuru-san, vâng... họ chưa đến...

28
00:04:18,512-->00:04:26,113
Yuzuru-chan không muốn đi khỏi Kaguya-chan,
Trông giống như 2 người sắp chia tay nhau vậy.
Lúc em đến, họ còn sướt mướt lắm.

29
00:04:27,112-->00:04:28,513
À, Như mọi khi, phải không?

38
00:04:29,512-->00:04:33,513
Vậy là chỉ còn thiếu Yuzuru nữa thôi.

39
00:04:34,112-->00:04:35,513
Ồ, chào buổi sáng, Origami.

40
00:04:36,113-->00:04:38,113
Chào buổi sáng, Shidou.

41
00:04:39,213-->00:04:44,113
Chào buổi sáng, Tobiichi Origami.
Tôi hy vọng hôm nay chúng ta sẽ đi chơi vui vẻ.

42
00:04:45,113-->00:04:48,113
Tôi cũng hy vọng như vậy.

43
00:04:49,112-->00:04:51,113
Phải rồi, Origami.
Cậu có thể qua đây được không?

39
00:04:51,512-->00:04:53,513
Gì vậy Shidou?

40
00:04:56,113-->00:04:57,513
À, có một chuyện tớ muốn nhờ cậu.

41
00:04:58,113-->00:04:59,513
Tớ biết rồi.

42
00:05:00,213-->00:05:01,513
Tớ chưa nói mà.

43
00:05:02,112-->00:05:12,113
Cậu muốn Arusu Maria vui chơi.
Vì vậy, cậu muốn tớ giúp đỡ cô ấy, phải không?

39
00:05:13,112-->00:05:15,113
Cậu biết rồi à...

40
00:05:16,112-->00:05:22,113
Vì tớ là bạn gái cậu nên hiểu cậu
là chuyện đương nhiên.

41
00:05:22,512-->00:05:26,113
Cô ấy vừa nhấn mạnh từ "bạn gái" à? 
Um... Tôi cảm thấy hơi có lỗi...
Nhưng tôi cảm thấy mình đang bị áp lực.

44
00:05:26,512-->00:05:28,513
Vậy mong cậu giúp đỡ.

45
00:05:29,112-->00:05:39,513
Tớ chăm sóc cô ấy. Tớ đang thực hiện 
một mô phỏng về công viên giải trí.
Nếu tớ làm việc này, vấn đề sẽ dễ dàng hơn.

46
00:05:40,112-->00:05:41,513
Vậy là... Tớ có thể yên tâm rồi à?

47
00:05:42,112-->00:05:43,513
Không sao đâu.

48
00:05:48,112-->00:05:52,513
Lời xin lỗi. Xin lỗi vì đã đến trễ.

49
00:05:53,112-->00:05:54,513
Ồ, chào buổi sáng, Yuzuru.

50
00:05:55,112-->00:06:05,113
Thú nhận. Tớ cũng muốn đưa Kaguya theo.
Nhưng vì hôm qua tớ đã ở nhà.

50
00:06:05,513-->00:06:11,113
Sẽ không công bằng nếu cô ấy được đi chơi
2 ngày liền. Vì vậy tớ để cô ấy ở nhà.

51
00:06:11,512-->00:06:15,113
Haha... Các cậu không muốn thua nhau 
phải không? Được rồi. Chúng ta đi thôi.

52
00:06:16,312-->00:06:20,113
Đi thôi!

53
00:06:21,112-->00:06:23,113
Vâng. đi thôi.

54
00:06:28,112-->00:06:29,513
Yuzuru, cậu qua đây chút.

55
00:06:30,112-->00:06:34,113
Thắc mắc. Gì vậy Shidou?

42
00:06:34,512-->00:06:38,113
Maria, cô ấy hầu như không có
kinh nghiệm đi chơi nhóm như hôm nay.
Tớ muốn cậu để ý đến cô ấy.

43
00:06:38,512-->00:06:44,113
Hiểu biết. Tuy nhiên, 
có vẻ cậu đang lo lắng quá đấy.

44
00:06:45,112-->00:06:46,513
Lo lắng quá sao?

45
00:06:47,112-->00:06:55,113
Khẳng định. Arusu có thể hòa nhập 
với tất cả chúng ta mà.

46
00:06:56,112-->00:06:57,513
Tớ cũng nghĩ thế, nhưng...

47
00:06:58,113-->00:07:07,113
Mỉm cười. Shidou, cậu vẫn chưa yên tâm à?
Không vấn đề gì. Cứ để tớ lo.

48
00:07:08,112-->00:07:09,513
Cậu nói vậy thì tớ yên tâm rồi.

49
00:07:10,112-->00:07:21,113
Đồng ý. Cậu có thể thưởng thức 
chuyến đi được không? Đó là thứ mà
tớ và Kaguya muốn cạnh tranh đấy.

49
00:07:21,512-->00:07:28,113
Người chiến thắng sẽ là tớ.
Tớ không muốn thua cô ấy.

50
00:07:29,112-->00:07:32,113
Không. Trái lại, tớ muốn tận hưởng nhiều hơn.
Dù sao, những gì họ muốn là 
"thưởng thức chuyến đi" mà.

62
00:07:33,112-->00:07:43,513
Đồng tình. Tớ cũng muốn trở thành
một người bạn tốt với Arusu.
Nhìn xem, Yoshino cũng đang phấn đấu đấy.

63
00:07:46,512-->00:07:54,113
Ma... Maria-san, chúng ta hãy 
đi cùng nhau... và nói chuyện nhé...

64
00:07:55,112-->00:08:00,112
Ừ, Yoshino. Chị cũng muốn nói chuyện lắm.

65
00:08:01,112-->00:08:04,113
Haha... Cứ để họ nói những gì họ muốn.
Giờ chúng ta đi nhanh lên.

84
00:08:11,112-->00:08:14,513
Đây là... công viên giải trí...

85
00:08:15,112-->00:08:16,513
Sao thế? Trông cậu ngạc nhiên vậy?

86
00:08:18,112-->00:08:22,113
Báo cáo. Trông cậu có vẻ kêu ngạo đấy, Shidou.

87
00:08:22,512-->00:08:24,113
Mặc kệ tớ.

88
00:08:24,512-->00:08:34,113
Có rất đông người phải không?
Nhiều người đang rất vui vẻ. 
Tớ cũng thấy vui khi ở đây.

89
00:08:35,112-->00:08:39,113
Như vậy là tốt đấy, Maria.
Nếu cậu nở nụ cười, người khác 
cũng sẽ cảm thấy vui theo đấy.

90
00:08:40,112-->00:08:46,113
Vì nếu tớ cười nhiều thì sẽ rất tốt
cho người khác phải không?

91
00:08:47,112-->00:08:59,113
Em cũng nghĩ vậy...
khi Shidou-san nhìn em và cười...
Em cũng cảm thấy vui...

92
00:09:00,112-->00:09:06,113
Yoshino, chị hiểu điều đó.
Chị cũng cảm thấy vậy.

93
00:09:12,112-->00:09:19,113
Chị cũng giống Yoshino sao, Maria-chan?
Có lẽ chị cũng yêu...

98
00:09:20,112-->00:09:22,113
Yoshinon!

99
00:09:23,112-->00:09:26,513
Sao vậy?

100
00:09:27,512-->00:09:29,113
Không, không có gì đâu. Đừng bận tâm.

101
00:09:29,512-->00:09:31,113
Đó là chuyện bình thường giữa
Yoshino và Yoshinon.

102
00:09:32,112-->00:09:47,113
Đề nghị. Thay vì nói chuyện,
chúng ta hãy đi tham quan trước. 
Sau đó, chúng ta sẽ quyết định chơi trò gì.

103
00:09:48,112-->00:09:53,113
Vâng, vâng! Em cũng đồng ý!
Như vậy vui hơn!

104
00:09:54,112-->00:09:55,513
Vậy chúng ta đi đâu trước?

105
00:09:56,112-->00:10:03,113
Để xem... Ngôi nhà ma thì sao?

106
00:10:06,112-->00:10:14,113
Ng... ngôi nhà ma à?
Em... em chợt nhớ em có việc rồi. Em đi đây!

107
00:10:15,112-->00:10:16,513
Này này...

108
00:10:17,112-->00:10:20,113
Kotori, cô không thích ngôi nhà ma à?

112
00:10:21,112-->00:10:25,113
Kh... không phải, nhưng...

113
00:10:26,112-->00:10:28,113
Maria, cứ để con bé ở ngoài.
Kotori không thích mấy thứ ma quỷ đâu.

114
00:10:29,112-->00:10:33,513
Tớ hiểu rồi. Tốt nhất là nên tránh nó đi phải không?

1
00:10:34,512-->00:10:40,113
Được rồi! Tôi sẽ ở ngoài!
Maria, cô cứ đi trước đi.

1
00:10:41,112-->00:10:45,113
Nhưng... mục tiêu của ta là vui vẻ cùng nhau...

1
00:10:46,512-->00:10:49,513
Maria, nghe Kotori nói đi.
Tí nữa cậu sẽ đi cùng nó mà.

1
00:10:50,512-->00:10:52,113
Nhưng mà...

1
00:10:53,112-->00:11:01,113
Đề nghị. Tớ cũng ở lại.
Chúng ta không thể để Kotori lại một mình.

1
00:11:02,112-->00:11:04,113
Sao vậy Yuzuru? Cậu sợ à?

368
00:11:05,112-->00:11:20,113
Từ chối. Tớ không sợ những thứ như thế. 
Chỉ đơn giản là nếu tớ đi, bầu không khí 
của nơi này có thể sẽ bị phân hủy.

68
00:11:21,112-->00:11:22,513
Nghe nghiêm trọng quá nhỉ...

368
00:11:23,112-->00:11:28,113
Yuzuru... Nếu cô nghĩ như vậy, 
chắc chắn cô sẽ thất bại.

368
00:11:29,112-->00:11:34,513
Thắc mắc. Sư phụ Origami, như vậy là sao?

368
00:11:35,512-->00:11:43,113
Ngôi nhà ma, hay còn gọi là lâu đài ma ám,
thực chất là nơi tối tăm 
dành cho các đôi trai gái.

368
00:11:44,112-->00:11:45,513
Đó không phải là chỗ âu yếm nhau đâu!

68
00:11:46,512-->00:11:52,113
Những nơi tối tăm... em sợ...

368
00:11:53,112-->00:12:03,113
Tớ hiểu những gì Origami-chan nói.
Nếu chúng ta vào nơi đáng sợ và tối tăm,
đó sẽ là cơ hội để chúng ta 
ôm nhau một cách hợp pháp.

368
00:12:04,112-->00:12:13,113
Quên sót. Đệ tử không nghĩ 
nó lại lãng mạn đến thế.
Không hổ danh là sư phụ Origami.

368
00:12:14,112-->00:12:16,113
Mình cảm thấy có cái gì đó sai sai, nhưng...
Vậy cậu có đi không Yuzuru?

368
00:12:17,112-->00:12:29,113
Từ chối. Bây giờ tớ muốn ở với Kotori hơn.
Dù hơi sớm nhưng cô muốn đi ăn kem không?

368
00:12:30,112-->00:12:36,513
Tôi muốn chứ! 
Vậy Onii-chan đi trước đi nhé!

368
00:12:37,113-->00:12:39,113
Cảm ơn 2 người.
Vậy bọn tớ đi đây. 2 người chờ nhé.

368
00:12:40,113-->00:12:44,113
Đồng ý. Tớ sẽ chăm sóc cô ấy.

368
00:12:49,112-->00:12:51,113
Tối hơn tớ nghĩ đấy...
Maria, cậu ở đâu?

368
00:12:52,112-->00:12:56,112
Tớ đây. Đây không có vẻ gì là ngôi nhà ma cả.

368
00:12:56,512-->00:13:01,112
Chỉ là ánh sáng và nhiệt độ thấp 
tạo nên vẻ sợ hãi.

368
00:13:02,112-->00:13:03,513
Maria thay vì sợ hãi,
dường như cô ấy quan tâm đến căn nhà hơn.

368
00:13:04,112-->00:13:05,513
Yoshino, em không sao chứ?

368
00:13:06,112-->00:13:12,113
Vâng! Em... không sao.

368
00:13:13,112-->00:13:20,113
Nhưng Shidou-san... anh có thể nắm tay em không?

368
00:13:21,112-->00:13:22,513
Nắm tay à? Được rồi.
Bóng tối trông nguy hiểm quá phải không?

368
00:13:23,113-->00:13:29,113
Làm tốt lắm, Yoshino. 
Hãy cầm tay một cách tình cờ và...

368
00:13:30,112-->00:13:38,113
Không phải đâu Yoshinon...
Chỉ là tớ không nhìn rõ đường đi thôi.

368
00:13:41,512-->00:13:47,113
Shidou, cậu có thể nắm tay tớ không?

368
00:13:48,112-->00:13:49,513
Được rồi. 

368
00:13:50,112-->00:13:53,513
Vâng. Cảm ơn cậu nhiều lắm.

368
00:13:54,112-->00:13:56,113
Có hơi khó đi đấy. 
Nhưng không sao. Cứ đi chậm và...

368
00:13:57,112-->00:13:58,513
Chờ đã.

368
00:13:59,112-->00:14:00,513
Gì vậy, Origami?

368
00:14:01,112-->00:14:05,113
Tớ cũng sợ quá.
Tớ muốn cậu nắm tay tớ.

368
00:14:05,512-->00:14:08,113
Khoan đã, Origami. Tớ hết tay rồi.
Với lại, trông cậu chẳng có vẻ sợ gì cả...

368
00:14:09,112-->00:14:13,113
Không đúng. Tớ sợ muốn chết đây.

368
00:14:14,112-->00:14:16,113
Thật sao? Nhưng tớ không thể nắm tay cậu được.

368
00:14:17,112-->00:14:21,113
Nếu không nắm tay được 
thì tớ sẽ nắm vào chỗ khác vậy.

368
00:14:21,512-->00:14:24,513
Waaa. Khoan đã! Đừng có ôm cổ tớ!
Bỏ tay ra khỏi áo tớ ngay!
Cậu nghe thấy không? Đừng có sờ chỗ đó!

368
00:14:25,112-->00:14:36,113
"Chỗ đó" là chỗ nào? 
Tớ không hiểu lắm. Hãy nói rõ hơn đi.

368
00:14:36,512-->00:14:39,113
Như vậy là đủ rồi! Chúng ta đi tiếp thôi!

368
00:14:41,512-->00:14:44,113
Dù vậy, chỗ này vẫn tối quá.

368
00:14:52,112-->00:14:54,113
Âm thanh đó giật mình thật!

368
00:14:55,112-->00:14:56,113
Waaa!

368
00:14:57,112-->00:14:59,113
Tôi bị mất tập trung vì những màn hù dọa. 
Và bất ngờ tôi bỏ tay ra.

368
00:15:00,112-->00:15:01,513
Shi... Shidou?

368
00:15:02,112-->00:15:04,113
Shi... Shidou-san?

368
00:15:05,112-->00:15:07,113
Tôi nên đưa tay về phía nào?

368
00:15:08,112-->00:15:29,113
1) Đưa về phía Maria.
2) Đưa về phía Yoshino.
3) Đưa vào giữa họ.

368
00:15:30,112-->00:15:31,513
Tôi đưa tay về phía Maria...

368
00:15:32,112-->00:15:33,113
Maria, có sao không?

368
00:15:33,512-->00:15:39,113
Vâng, tớ không sao. Tớ chỉ giật mình
vì tiếng động lớn thôi.

368
00:15:40,113-->00:15:41,513
Hiểu rồi. Không sao đâu. Đừng sợ.

368
00:15:42,112-->00:15:43,513
Vâng...

368
00:15:44,112-->00:15:46,113
Vậy còn... Này Yoshino?

368
00:15:46,512-->00:15:48,513
V... Vâng!

368
00:15:51,112-->00:15:53,113
Anh đây... Yoshino, em đâu rồi?

368
00:15:54,112-->00:15:55,513
Em ở đây...

368
00:15:56,112-->00:15:57,513
May quá. Em không sao chứ?

368
00:15:58,112-->00:16:07,513
Em... xin lỗi.
Yosinon bị mất... Và em đã hoảng sợ ...

368
00:16:08,112-->00:16:10,113
Sao em kiếm được nó?

368
00:16:10,512-->00:16:17,513
Vâng ... Origamin-san đã tìm giúp em.

368
00:16:18,512-->00:16:20,113
Anh hiểu rồi. Cậu đã cứu tớ đấy, Origami

368
00:16:20,512-->00:16:24,513
Hỗ trợ Shidou là điều hiển nhiên. 

368
00:16:25,112-->00:16:29,113
Những tình huống thế này, 
tớ đã chuẩn bị thiết bị nhìn ban đêm.

368
00:16:30,112-->00:16:31,513
Cậu luôn mang theo những thứ đó sao?

368
00:16:32,113-->00:16:34,113
Tôi nghĩ tốt hơn là không nên hỏi 
những thứ cô ấy thường dùng.

368
00:16:35,112-->00:16:44,113
Tôi cũng phải cảm ơn cô.
Cảm ơn cô, Origami-chan.
Tôi là một phần của Yoshino mà.

368
00:16:45,112-->00:16:47,513
Đó là khi Yoshino mất Yoshinon,
sức mạnh Tinh Linh sẽ ngoài tầm kiểm soát.
Suýt chút nữa...

368
00:16:48,112-->00:16:49,513
Thực sự tớ đánh giá cao việc vừa rồi đấy, Origami.

368
00:16:50,312-->00:16:53,113
Không cần phải bận tâm đâu.

368
00:16:58,112-->00:17:01,113
Xác nhận. Có vẻ họ xong rồi.

368
00:17:02,112-->00:17:04,113
Ừ. Xin lỗi để 2 người đợi lâu.

368
00:17:05,113-->00:17:18,113
Từ chối. Không vấn đề gì. 
Tớ đã nói chuyện rất vui với Kotori.
Kem cũng rất ngon.

368
00:17:19,112-->00:17:23,113
Đúng đấy! Kem siêu ngon!

368
00:17:24,112-->00:17:26,513
Anh mừng vì em nói thế.
Giờ 2 cậu muốn đi đâu đây?

368
00:17:27,512-->00:17:31,113
Đề nghị. Vậy cái đó thì sao?

368
00:17:32,113-->00:17:34,113
Tách xoay vòng à? Được rồi, qua đó đi.

368
00:17:35,112-->00:17:39,113
Đó đúng là những tách cà phê khổng lồ nhỉ?

368
00:17:40,112-->00:17:42,513
Phải, đó là trò chơi mà cậu sẽ
ngồi trong tách và xoay vòng vòng.

368
00:17:43,112-->00:17:52,113
Tớ biết chuyện đó. Tớ chỉ thắc mắc
tại sao họ lại chọn 
hình dạng một tách cà phê thôi.

368
00:17:52,512-->00:17:55,513
Ở nước ngoài, họ gọi đó là tách trà.
Tớ không biết nữa. Có lẽ họ thích tách nhỏ chăng?

368
00:17:56,112-->00:18:00,113
Ra là vậy à?

368
00:18:02,112-->00:18:05,113
Nè nè, Shidou-kun, Shidou-kun.

368
00:18:05,512-->00:18:07,513
Gì vậy, Yoshinon?

368
00:18:08,112-->00:18:14,113
Shidou-san... em...

368
00:18:15,112-->00:18:24,113
Cứ nói rõ đi, Yoshino. 
Trông Yoshino có vẻ chóng mặt,
vậy nên bọn em sẽ ở đây.

368
00:18:25,112-->00:18:28,513
Em không thích những trò xoay vòng sao? 
Yoshino, em nghỉ ngơi đi. 
Em sẽ ở đây một mình à?

368
00:18:29,112-->00:18:35,113
Trường hợp này, tớ có thể ở lại.
4 người đi đi.

368
00:18:36,112-->00:18:37,513
Cậu không đi à, Origami?

368
00:18:38,112-->00:18:43,113
Không vấn đề gì. Thỉnh thoảng
để mục tiêu tự do cũng rất quan trọng.

368
00:18:43,512-->00:18:46,513
Đừng có nghĩ về mấy thứ đó nữa.
Vậy bọn tớ đi nhé.

368
00:18:50,512-->00:18:53,513
Maria, Kotori, Yuzuru và tôi đã ngồi
vào trong tách. Âm nhạc nổi lên 
và cái tách bắt đầu di chuyển.

368
00:18:54,113-->00:18:56,113
Yuzuru, cậu có chơi trò này bao giờ chưa?

368
00:18:57,112-->00:19:07,113
Ghi nhớ. Tớ nhớ có một trận chiến với Kaguya.
Bọn tớ thi xem nó có thể quay 
bao nhiêu vòng trước khi hết nhạc.

368
00:19:08,112-->00:19:09,513
À, vậy à...

368
00:19:10,112-->00:19:12,513
Khi tôi còn nhỏ, tôi chẳng đếm được 
nó quay bao nhiêu vòng cả.

368
00:19:13,112-->00:19:25,113
Báo cáo. Sau khi chơi xong,
bọn tớ quay theo nó quá nhanh 
nên không đếm được. Vì vậy, chẳng ai thắng cả.

368
00:19:25,512-->00:19:27,113
Có vẻ ngay cả lúc lớn cũng chẳng đếm được nhỉ.

368
00:19:28,112-->00:19:35,113
Shidou, cái tách được quay theo điệu nhạc.
Có vẻ nó sắp nhún nhảy.

368
00:19:36,112-->00:19:40,113
Nó trông thú vị hơn phải không?
Nghe này, Maria. Đây mới chỉ bắt đầu thôi.
Một lúc nữa, những chiếc tách sẽ quay nhanh hơn.

368
00:19:48,112-->00:19:52,113
Tớ hiểu rồi. 
Những chiếc tách quay thế này à?

368
00:19:52,512-->00:19:54,113
Không có gì ghê gớm,
nhưng rất thú vị, phải không?

368
00:19:55,112-->00:20:01,113
Đúng vậy. Nó là một cơ chế rất đơn giản.

368
00:20:01,512-->00:20:04,113
Cậu hơi bị ấn tượng quá nhỉ...

368
00:20:05,112-->00:20:10,113
Đúng vậy. Nó rất vui.

368
00:20:10,512-->00:20:12,113
Ừ. Tớ vui khi nghe vậy.

368
00:20:13,112-->00:20:15,113
Nè nè Onii-chan.

368
00:20:20,112-->00:20:24,113
Mắt em càng lúc càng lé rồi.

368
00:20:25,512-->00:20:27,113
Cố chịu đến khi nó dừng lại đi.

368
00:20:31,513-->00:20:35,513
Làm tốt lắm. 
Trông cậu vui thật đấy, Shidou.

368
00:20:36,513-->00:20:39,113
Ừ, cảm ơn cậu. Cậu đã giúp tớ nhiều đấy.
Vậy cậu chọn địa điểm tiếp theo đi.

368
00:20:40,113-->00:20:47,113
Được rồi. Có một nơi không thể bỏ qua
khi đến công viên giải trí.

368
00:20:48,112-->00:20:58,113
Hiểu biết. Đệ tử biết rồi, sư phụ Origami.
Nãy giờ chỉ là khởi động thôi phải không?

368
00:20:59,112-->00:21:01,113
Khoan đã. Tớ vẫn chưa hiểu các cậu muốn đi đâu.

368
00:21:02,112-->00:21:11,113
Anh không hiểu sao, Shidou-kun?
Chính là nó đấy! Tàu lượn siêu tốc!

368
00:21:12,112-->00:21:17,513
Tàu lượn siêu tốc, trò chơi rất tuyệt đấy.

368
00:21:18,112-->00:21:19,513
Ra là vậy.

368
00:21:20,112-->00:21:23,513
Tớ cũng rất muốn chơi đấy.

368
00:21:24,112-->00:21:27,113
Được. Quyết định vậy đi. 
Chúng ta sẽ chơi tàu lượn.

368
00:21:32,112-->00:21:34,113
Chúng ta đến tàu lượn rồi, nhưng...

368
00:21:35,112-->00:21:42,113
Nguyên một hàng chờ dài.
Chứng tỏ trò này rất nhiều người thích phải không?

368
00:21:42,512-->00:21:46,113
Đúng vậy. Tàu lượn siêu tốc rất nổi tiếng.
Không có gì lạ khi đông người chơi đến thế.

368
00:21:47,112-->00:21:50,113
Em muốn đi ngay bây giờ!

368
00:21:51,512-->00:21:54,513
Em đang... rất phấn khích đấy...

368
00:21:56,112-->00:22:02,113
Cậu chắc không đấy Yoshino?
Trò này ở trên cao lắm đấy!

368
00:22:02,512-->00:22:04,513
Em không sợ sao Yoshino?

368
00:21:05,112-->00:21:08,113
Vâng... em sẽ ổn thôi.

368
00:22:09,112-->00:22:13,113
Nhưng hàng chờ dài quá. 
Chẳng thấy nó kết thúc gì cả!

368
00:22:14,112-->00:22:20,113
Shidou, ở đây này.
Điểm kết thúc của dòng người là ở đây.

368
00:22:20,512-->00:22:28,113
Xác nhận. Từ đây đến khi hết dòng người
chắc khoảng 60 phút.

368
00:22:29,112-->00:22:32,113
Ừ. Cảm ơn các cậu.
Vậy là chúng ta sẽ phải đợi 60 phút sao?
1 tiếng đồng hồ cơ à?

368
00:22:33,112-->00:22:39,513
Đề nghị. Cậu có thể xem một bộ phim nếu muốn.

368
00:22:40,512-->00:22:45,513
Chúng ta làm gì đây, Onii-chan?
Chúng ta có việc gì để giết thời gian không?

368
00:22:46,112-->00:22:49,113
Không, chúng ta chỉ có thể đứng đây và đợi thôi!

368
00:22:52,512-->00:22:55,113
Giờ chúng ta xếp hàng đi. 
Ít nhất đó là việc chúng ta có thể làm bây giờ!

368
00:22:58,512-->00:23:00,513
Nhưng cứ nhìn dòng người thế này
thì chán thật đấy nhỉ.

368
00:23:01,112-->00:23:11,113
Mọi người xung quanh tôi đang chơi với 
Smartphone hoặc thiết bị cầm tay của họ.
1 giờ đúng là quá dài.

368
00:23:12,112-->00:23:22,513
Tôi nghe nói nên tránh những khu vui chơi
nếu đi chơi với bạn bè. Đúng là vậy. 
Đứng đợi 1 giờ thật khó khăn.

368
00:23:23,512-->00:23:26,113
Này Maria, nãy giờ cậu nhìn gì vậy?
Có gì hay à?

368
00:23:27,112-->00:23:31,113
Tớ chỉ đang xem mọi người thôi.

368
00:23:32,113-->00:23:33,513
Xem người sao?

368
00:23:34,112-->00:23:45,113
Đúng vậy. Tuổi tác, giới tính, 
số người trong nhóm...
Quan sát hành vi của mọi người rất thú vị.

368
00:23:45,512-->00:23:47,113
À vậy à? Cậu nói đúng.

368
00:23:48,112-->00:23:54,513
Những người đến công viên giải trí này
đều có những lý do khác nhau.

368
00:23:55,513-->00:23:58,113 
Đúng vậy. Các bậc cha mẹ đưa con cái đi chơi.
Các cặp vợ chồng, tình nhân hẹn hò nhau.
Nếu nhìn ở một khía cạnh nào đó, họ rất thú vị.

368
00:23:59,112-->00:24:01,513
Không thể tin được trừ chúng tôi ra,
tất cả họ đều là NPC.

368
00:24:02,512-->00:24:06,113
Đến bây giờ, dòng người vẫn quá đông.

368
00:24:07,112-->00:24:11,113
Họ đang quyết định chỗ ngồi trên tàu lượn. 
Nếu cậu được ngồi kế bên người cậu muốn
thì sẽ thú vị hơn.

368
00:24:12,112-->00:24:17,513
Đó là một gia đình. 
Họ đang cầm trên tay những thứ gì đó.

368
00:24:18,512-->00:24:21,113
Có lẽ đó là máy ảnh kỹ thuật số.
Cậu xem mọi người xung quanh đi.
Họ đang thưởng thức thời gian chờ đợi đấy.

368
00:24:22,112-->00:24:26,113
Vâng. Thật thú vị.

368
00:24:26,512-->00:24:28,513
Tớ nghĩ tớ nên cố gắng
gần gũi hơn với môi trường xung quanh.

368
00:24:29,113-->00:24:31,513
Mới vừa rồi, tôi nghĩ thế này thật nhàm chán. 
Nhưng nói chuyện thế này cũng tốt.

368
00:24:34,112-->00:24:40,113
Trở lại. Xin lỗi, Shidou.
Bọn tớ về rồi

368
00:24:41,112-->00:24:42,513
Hở? 2 cậu đi từ lúc nào mà tớ không biết.

368
00:24:43,113-->00:24:47,113
Bọn tớ vừa đi về phía khu mua sắm.

368
00:24:48,112-->00:24:55,113
Cung cấp. 1 giờ lâu quá phải không?
Bọn tớ nghĩ nên mua vài đồ ăn nhẹ.

368
00:24:56,112-->00:24:57,513
Ra vậy. Cảm ơn nhiều.

368
00:24:59,112-->00:25:01,113
Sắp đến mình rồi kìa, Onii-chan!

368
00:25:02,112-->00:25:04,113
Ừ! Anh thấy rồi. Em đang làm gì thế?

368
00:25:05,112-->00:25:15,113
Em nghe những gì mọi người nói.
Yoshino và em có thể quyết định 
ai sẽ ngồi chung với ai.

368
00:25:16,112-->00:25:19,113
Như vậy thì... sẽ tốt hơn

368
00:25:20,112-->00:25:22,113
Ừ. Cứ làm vậy đi.

368
00:25:23,512-->00:25:32,113
Không vấn đề gì. Ngay cả nếu tôi phó mặc
cho số phận thì người ngồi kế bên Shidou
sẽ là tôi. Không phải bàn cãi.

368
00:25:33,112-->00:25:37,113
Thách thức. 
Điều đó sẽ không xảy ra đâu, sư phụ Origami.

368
00:25:38,112-->00:25:43,513
Ai sẽ giành chiến thắng 
trong cuộc thi xổ số của số phận? 
Người đó sẽ là đệ tử.

368
00:25:45,112-->00:25:48,513
Em... Em sẽ không thua đâu!

368
00:25:50,112-->00:25:53,113
Cố lên Yoshino!

368
00:25:55,112-->00:25:58,113
Chúc tất cả may mắn nhé!

368
00:25:59,112-->00:26:00,513
Khoan đã Kotori. Em đang giấu gì đó
phải không? Lấy nó ra đây.

368
00:26:01,112-->00:26:06,113
Không có gì cả đâu.

368
00:26:07,112-->00:26:10,113
Rõ ràng em đang cầm gì đó trong tay trái.
Này, đó là số vé phải không? Mẹo vặt.

368
00:26:10,512-->00:26:17,113
Cái này... anh biết đấy.
Chỉ là đếm số vé đến lượt mình thôi mà.

368
00:26:18,112-->00:26:25,113
Kotori, cô chơi thế là ăn gian đấy.
Như thế không được phải không?

368
00:26:26,112-->00:26:30,513
Phải rồi. Xin lỗi nhé.

368
00:26:31,512-->00:26:33,113
Vậy giờ mọi người sẵn sàng chưa?

368
00:26:33,512-->00:26:45,113
Xác nhận. Mọi người sẵn sàng chưa?
Tôi sẽ ra hiệu bằng cách nói "Nhào vô!" nhé.
Nhào vô!

368
00:26:49,512-->00:26:51,513
Cho bọn em 6 vé.

368
00:26:52,112-->00:26:56,113
Vâng. Của các em đây. Chúc vui vẻ!

368
00:26:56,512-->00:26:59,113
Thứ tự thế nào?

368
00:27:00,112-->00:27:02,113
Nếu đây là một kết quả xổ số
thì chẳng có cách nào cả.

368
00:27:02,513-->00:27:14,113
Thở dài. Sư phụ Origami, như vậy cũng tốt.
Chúng ta sẽ vui vẻ cùng nhau.

368
00:27:15,112-->00:27:16,513
Được rồi.

368
00:27:18,112-->00:27:22,113
Không còn cách nào khác.

368
00:27:23,112-->00:27:28,513
Kotori-san... cố lên nhé...

368
00:27:29,112-->00:27:33,113
Kotori chuyển sang nơ đen rồi.
Con bé sợ tàu lượn à? Kotori, một đứa 
luôn tỏ ra mạnh mẽ mà lại thế sao?

368
00:27:34,512-->00:27:38,113
Shidou, cậu không ngồi với tớ à?

368
00:27:38,512-->00:27:40,113
Đúng vậy.

368
00:27:40,512-->00:27:44,113
Không hiểu sao tớ cảm thấy hơi lo lắng.

368
00:27:45,112-->00:27:47,113
Không sao đâu.
Chúng ta sẽ ngồi với nhau vào dịp khác.

368
00:27:48,112-->00:27:56,513
Vâng. Mặc dù hơi tiếc vì không được 
ngồi cạnh cậu nhưng như thế 
cũng vui theo cách khác.

368
00:27:57,512-->00:27:58,513
Cảm ơn cậu.

368
00:27:59,113-->00:28:03,113
Không ai ngồi ở ghế đầu tiên của tàu lượn.
Và tôi đã ngồi vào chỗ đó một mình.

368
00:28:06,512-->00:28:09,113
Và sau đó, tàu lượn bắt đầu 
di chuyển chậm và leo lên cao.

368
00:28:10,112-->00:28:16,113
Tàu lượn này không nhanh như lúc
chúng ta xem nhỉ?

368
00:28:16,512-->00:28:21,113
Tàu lượn không lao nhanh ngay từ đầu đâu.
Tốt nhất là hãy để cậu tự trải nghiệm.

368
00:28:22,112-->00:28:27,113
Tớ hiểu rồi. Có nghĩa là
nó sẽ tăng tốc dần dần à?

368
00:28:27,512-->00:28:30,113
Không, không phải thế...
Maria, cậu sẵn sàng chưa?

368
00:28:31,112-->00:28:34,113
Sẵn sàng gì cơ?

368
00:28:35,112-->00:28:37,113
Nào. Chúng ta sắp lao xuống đấy!

368
00:28:38,112-->00:28:42,113
"Lao xuống"? Ý cậu là...

368
00:28:44,512-->00:28:48,113
Tàu lượn bắt đầu lao xuống dốc,
làm gián đoạn câu nói của Maria.
Trong phút chốc, tầm mắt của tôi được mở rộng,
cảm giác như mình đang bay vút xuống.

368
00:28:49,112-->00:28:51,113
Con đường này... quá dài!

368
00:28:52,112-->00:28:53,513
Ngạc nhiên thật!

368
00:28:57,112-->00:28:59,112
Ko... Kotori-san...

368
00:29:00,112-->00:29:02,113
Tốt hơn là đừng nên mở miệng...

368
00:29:03,112-->00:29:09,113
Đồng tình. Nếu mở miệng 
có thể sẽ bị cắn vào lưỡi đấy.

368
00:29:10,112-->00:29:12,113
Dường như tất cả họ đang chịu đựng.

368
00:29:12,512-->00:29:16,113
Lúc này sao không ai hét lên nhỉ?

368
00:29:17,112-->00:29:18,113
Phải...

368
00:29:19,112-->00:29:21,113
Vậy là... 

368
00:29:29,512-->00:29:30,513
Ma... Maria?

368
00:29:31,112-->00:29:33,113
Tôi nghe cô ấy hét mà cứ như đang khóc vậy.

368
00:29:33,512-->00:29:34,513
Waaaaaaaaaaaa!

368
00:29:37,112-->00:29:40,113
Thật... tuyệt vời!

368
00:29:41,512-->00:29:42,513
Đúng vậy!

368
00:30:06,112-->00:30:10,113
Khi tôi nhận ra chúng tôi đang la hét.
Tôi không biết đó là sợ hãi hay vui vẻ,
nhưng tất cả chúng tôi đang la hét.

368
00:30:10,512-->00:30:14,113
Mặc dù trò chơi chỉ có vài giây,
nhưng tàu lượn siêu tốc lại rất hấp dẫn.

368
00:30:17,512-->00:30:21,113
Đôi chân tôi đang run rẩy. 
Tôi chẳng cảm giác gì ở phần thắt lưng. 
Từng bước chân, tôi cảm thấy chóng mặt.

368
00:30:22,112-->00:30:26,113
Vui thật đấy, phải không?

368
00:30:27,112-->00:30:29,113
Ừ, vui thật. Thật đáng để chúng ta chờ đợi.

368
00:30:29,513-->00:30:33,113
Tại sao vậy? Sau khi leo lên "máy hét",
tôi không thể ngừng cười. 

368
00:30:34,112-->00:30:43,113
Đây là một trải nghiệm rất tốt.
Nơi này cũng giống như karaoke, 
là một cái gì đó rất mới mẻ với tớ.

368
00:30:43,512-->00:30:45,113
Phải, cậu nói đúng.

368
00:30:46,112-->00:30:49,513
Nhưng niềm vui của 
tàu lượn siêu tốc chưa hết đâu.

368
00:30:50,512-->00:30:53,513
Sau khi đi tàu lượn,
Kotori trở lại "Imouto Mode" với băng trắng.
Con bé không cho mình thời gian để nghỉ ngơi.

368
00:30:54,112-->00:30:58,113
Vậy là sao, Kotori?

368
00:30:59,112-->00:31:01,113
Ở đây! Ở đây này!

368
00:31:02,112-->00:31:03,513
À, là hình chụp lúc đi tàu lượn à?

368
00:31:04,112-->00:31:08,113
Họ chụp bí mật trong khi chúng ta chơi à?

368
00:31:08,512-->00:31:10,513
Ừ, có lẽ họ chụp ở đâu đó.

368
00:31:11,512-->00:31:14,513
Ra là vậy. Có vẻ thú vị đấy.

368
00:31:15,112-->00:31:16,513
Chúng ta ra xem đi.

368
00:31:17,112-->00:31:18,513
Vâng!

368
00:31:22,512-->00:31:25,113
Haha... mặt tớ cứng đờ lại.

368
00:31:26,112-->00:31:30,113
Vâng. Nhưng tớ thấy hình đẹp đấy.

368
00:31:31,112-->00:31:33,113
Phải. Maria, cậu muốn mua nó làm kỷ niệm không?

368
00:31:34,112-->00:31:35,513
Được sao?

368
00:31:36,112-->00:31:37,513
Tất nhiên rồi.

368
00:31:39,112-->00:31:41,513
Vậy cậu mua cho tớ nữa.

368
00:31:42,512-->00:31:53,113
Yêu cầu. Shidou, tớ cũng vậy.
Tớ muốn có gì đó làm kỷ niệm.

368
00:31:54,112-->00:31:58,113
Shi... Shidou-san, em cũng thế.

368
00:31:59,112-->00:32:04,113
Anh không quên Yoshino và em chứ, Shidou-kun?

368
00:32:05,112-->00:32:07,513
Được rồi.
Kotori, em cũng vậy phải không?
Vậy anh sẽ mua 5 cái.

368
00:32:08,512-->00:32:10,113
Maria, đừng làm mất nó đấy.

368
00:32:10,512-->00:32:13,113
Vâng. Tớ sẽ giữ cẩn thận.

368
00:32:14,112-->00:32:15,113
Ừ.

368
00:32:15,512-->00:32:18,513
Dù thời gian đợi lâu nhưng tôi vui 
khi thấy Maria rất thích nó. 
Tôi chắc rằng tấm ảnh sẽ trở thành một kỷ niệm đẹp.

368
00:32:19,112-->00:32:21,113
Làm sao mình có thể mang tấm ảnh này
về thế giới thực được nhỉ?

368
00:32:21,512-->00:32:24,113
Cậu vừa nói gì vậy Shidou?

368
00:32:25,112-->00:32:28,113
Không, không có gì. Giờ chúng ta đi ăn gì đó
hay cậu muốn chơi trò khác?

368
00:32:29,112-->00:32:34,513
Tớ chưa thấy đói.
Giờ chúng ta chơi trò khác chứ?

368
00:32:35,512-->00:32:37,113
Ừ. Được rồi.

368
00:32:37,512-->00:32:40,113
Và tất cả chúng tôi đã dành cả ngày 
thưởng thức nhiều trò hơn trong 
công viên giải trí cho đến khi mặt trời lặn.

368
00:32:46,112-->00:32:49,513
Hôm nay vui thật.

368
00:32:50,512-->00:32:52,513
Đúng vậy. Lần tới, chúng ta sẽ đi
với những người khác.

368
00:32:53,512-->00:32:54,513
Vâng.

368
00:32:55,112-->00:32:57,113
Kotori và các cô gái khác nói 
họ mệt và đã về trước rồi.

368
00:32:58,112-->00:33:00,113
Giờ chúng ta làm gì đây Maria?
Cậu muốn đi đâu trước khi về nhà?

368
00:33:01,112-->00:33:06,113
Shidou... cậu có thể cho tớ
một chút thời gian ở một mình chứ?

368
00:33:07,112-->00:33:09,113
Cậu đang suy nghĩ gì à?

368
00:33:10,112-->00:33:15,113
Vâng. Có vài việc tớ muốn làm. Được chứ?

368
00:33:16,112-->00:33:18,113
Không sao. Nhưng đừng về trễ quá đấy.

368
00:33:19,112-->00:33:27,113
Cảm ơn cậu rất nhiều vì lo lắng cho tớ. 
Tuy nhiên, tớ sẽ ổn thôi. 
Không có gì đáng lo đâu.

368
00:33:27,512-->00:33:29,113
Được rồi. Nếu có chuyện gì, nhớ gọi tớ ngay nhé.

368
00:33:30,112-->00:33:34,113
Vâng. Vậy hẹn gặp lại nhé, Shidou.

368
00:33:37,512-->00:33:39,513
Tôi nhìn Maria cho đến khi
cô ấy khuất tầm mắt.

368
00:33:40,112-->00:33:42,113
Giờ thì tôi có cơ hội 
đi dạo nhàn nhã rồi về nhà.



368
00:33:47,512-->00:33:50,113
Hở? Đó là... Arusu sao?
Cô ấy đang đứng ở bên kia cầu vượt.
Cô ấy đang làm gì thế?

368
00:33:51,112-->00:33:54,113
Arusu đang đứng một mình.
Tôi thắc mắc hay là cô ấy đang đợi ai đó.

368
00:33:55,112-->00:33:57,113
Có vẻ cô ấy muốn sang đây.

368
00:33:59,512-->00:34:02,113
Trong khi tôi nhìn Arusu,
cô đột nhiên lên cầu vượt và bước sang đây.

368
00:34:02,513-->00:34:04,113
Cô ấy định làm gì vậy?

368
00:34:05,112-->00:34:08,113
Từ phía đối diện Arusu, một bà già 
đi trên cầu vượt. Nhưng bà ấy dừng lại 
nửa chừng do mang gói đồ khá nặng.

368
00:34:09,112-->00:34:14,513
Tại sao cứ phải làm những việc thế này
trước mặt mình chứ?

368
00:34:15,512-->00:34:19,113
Arusu đến gần bà, bắt đầu đi qua 
thật nhanh lối đi dành cho người đi bộ.
Sau đó cô nhặt giúp bà gói đồ và bỏ đi.

368
00:34:19,512-->00:34:23,513
Khoan đã! Đừng bỏ bà ấy lại!

368
00:34:24,512-->00:34:26,513
Đây là tất cả đồ của bà phải không?
Nếu bà muốn, cháu có thể giúp bà sang bên kia.

368
00:34:27,112-->00:34:29,113
Bà ấy gật đầu và nắm lấy tay tôi.

368
00:34:29,512-->00:34:33,113
Bà đừng bận tâm. Đi nào.

368
00:34:39,112-->00:34:42,113
Cậu muốn giúp đỡ bà ấy phải không? 
Tớ ngạc nhiên vì cậu tốt bụng thế đấy.

368
00:34:43,112-->00:34:49,113
Đó chỉ là bà ta làm vướng đường tôi.
Dù bà ta chỉ là NPC, tôi vẫn thấy khó chịu.

368
00:34:50,112-->00:34:53,113
Sau đó, bà ấy cảm ơn chúng tôi với sự vui vẻ. 
Tuy nhiên, Arusu có vẻ hơi khó chịu.

368
00:34:53,512-->00:34:55,513
Cậu đã làm việc tốt.
Đừng làm bộ mặt đó nữa.

368
00:34:56,512-->00:35:03,113
Tôi không muốn giúp bà ấy.
Tôi chỉ nhớ trước đây Itsuka Shidou 
đã tức giận với tôi.

368
00:35:04,112-->00:35:05,513
Hở? Tớ tức giận với cậu khi nào?

368
00:35:06,112-->00:35:07,513
Có gì khiến cho tôi tức giận sao?

368
00:35:08,112-->00:35:13,113
Itsuka Shidou không cần biết chuyện đó.
Buổi nói chuyện đã kết thúc rồi. Hiểu chứ?

368
00:35:14,113-->00:35:15,513
Tất nhiên là chưa.

368
00:35:20,112-->00:35:22,113
L... làm cái gì vậy?

368
00:35:23,112-->00:35:25,113
Vì cậu vừa làm việc tốt, 
tớ nghĩ tớ nên chúc mừng cậu.

368
00:35:25,512-->00:35:28,113
Tôi đã nói tôi không muốn làm thế, vì vậy...

368
00:35:28,512-->00:35:29,512
Nhưng tớ vẫn mừng cho cậu.

368
00:35:34,112-->00:35:41,113
Dừng lại đi. Tôi đã nói với cậu 
tôi không giúp vì lý do đó.
Cậu hiểu lầm rồi đấy.

368
00:35:42,112-->00:35:43,513
Phải, tớ biết.

368
00:35:45,112-->00:35:46,113
Này!

368
00:35:47,112-->00:35:48,513
Dù sao, tớ vẫn muốn cảm ơn cậu.

368
00:35:49,112-->00:35:56,113
Tôi hiểu rồi. Cứ làm những gì cậu muốn đi. 
Nhưng tại sao cậu muốn cảm ơn tôi?
Tôi không thể hiểu được.

368
00:35:57,112-->00:35:59,113
Làm việc tốt và được khen ngợi.
Đó là điều hiển nhiên mà.

368
00:36:00,112-->00:36:02,113
Vậy sao?

368
00:36:05,112-->00:36:10,513
Cậu trông vui quá nhỉ?
Tôi không muốn dính vào Itsuka Shidou đâu.

368
00:36:11,112-->00:36:12,513
Nghe buồn thật đấy...

368
00:36:15,112-->00:36:19,513
À, còn một chuyện tôi muốn hỏi nữa.

368
00:36:20,112-->00:36:21,513
Gì vậy?

368
00:36:22,113-->00:36:26,113
Ai là người lần đầu tiên chúc mừng 
các chàng trai trong cuộc sống của họ?

368
00:36:26,512-->00:36:28,113
Có lẽ là bố mẹ chăng?

368
00:36:28,512-->00:36:33,513
Đúng vậy đấy. 
Đó là lý do tại sao tôi làm điều này.

368
00:36:34,112-->00:36:36,113
Cậu đang nói gì vậy?

368
00:36:36,512-->00:36:40,113
Không có gì. 
Tạm biệt nhé, Itsuka Shidou.

368
00:36:44,112-->00:36:45,513
Một lần nữa, cô ấy lại gây bất ngờ...

368
00:36:46,112-->00:36:48,513
Tôi nghĩ tôi cảm thấy Arusu
có một chút cô đơn ở đâu đó.
Mặc dù có lẽ đó chỉ là trí tưởng tượng của tôi.

368
00:36:49,113-->00:36:51,113
Tôi tốt hơn về nhà.
Nếu Arusu biến mất, 
nghĩa là Maria cũng đã mặt ở nhà.

368
00:36:57,112-->00:37:03,113
Ngày thứ 5

368
00:37:40,112-->00:37:41,113


368
00:37:40,112-->00:37:41,113


368
00:37:40,112-->00:37:41,113


368
00:37:40,112-->00:37:41,113


368
00:37:40,112-->00:37:41,113