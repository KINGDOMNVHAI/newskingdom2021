﻿0
00:00:00,512-->00:00:03,113
1) Hôn cô ấy
2) Không hôn cô ấy

1
00:00:03,512-->00:00:06,113
Maria? Việc này...

2
00:00:07,112-->00:00:15,113
Xin cậu đấy, Shidou...
Đó là điều ước cuối cùng của tớ.

3
00:00:15,512-->00:00:20,113
1) Hôn cô ấy.
2) Không, tốt hơn là không.

4
00:00:21,112-->00:00:23,513
Nhưng dù là vậy!

5
00:00:24,512-->00:00:28,113
1) Hôn cô ấy
2) Tôi không thể kiềm chế được nữa!

6
00:00:28,512-->00:00:31,113
Tôi cảm thấy giống như đầu tôi ghi rõ từ hôn. 
Cảm giác đó dẫm lên ý chí của tôi
và nó bắt đầu phá bỏ lựa chọn từ chối.

7
00:00:32,112-->00:00:33,113
Maria...

8
00:00:33,512-->00:00:35,113
Tôi muốn hôn cô ấy.
Tôi bắt đầu tiến đến gần 
nhưng vẫn còn lưỡng lự.

9
00:00:36,113-->00:00:42,113
Shidou, chẳng lẽ cậu không muốn hôn tớ sao?

10
00:00:42,512-->00:00:44,513
Không phải. Nhưng tớ nghĩ điều này không đúng...

11
00:00:45,112-->00:00:47,513
Không đúng sao...

12
00:00:48,112-->00:00:50,112
Đúng vậy...

13
00:00:51,112-->00:00:53,513
Maria nhắm mắt lại.
Tôi có thể nhìn rõ khuôn mặt 
đang hồi hộp và xấu hổ của cô ấy.

14
00:00:54,112-->00:00:57,113
Tôi chưa bao giờ được nhìn thấy 
khuôn mặt của Maria kỹ như bây giờ.

15
00:00:58,112-->00:01:00,113
Shidou đang đến gần.
Tôi cảm thấy hơi ấm của Shidou.

16
00:01:01,112-->00:01:03,113
Đó là gì? Một giọng nói? Không, không phải.
Tôi bắt đầu có một cảm giác kỳ lạ.

17
00:01:04,512-->00:01:07,513
Tôi có thể nghe thấy nhịp đập của Shidou.
Mùi thơm... từ tóc Shidou.

18
00:01:08,112-->00:01:09,513
Đây là... suy nghĩ của Maria sao?

19
00:01:10,112-->00:01:12,113
Tôi đặt tay mình lên vai Maria.
Tôi đưa môi lại gần cô ấy.

20
00:01:13,112-->00:01:16,113
Tôi muốn gặp Shidou, 
nghe giọng của Shidou, tất cả về Shidou. 
Tôi muốn được ở bên Shidou mãi mãi.

21
00:01:16,512-->00:01:19,113
Tôi đã chạm vào môi của Maria.

22
00:01:20,112-->00:01:22,113
Đúng, tôi chắc chắn rằng mình đã... hôn Shidou.

23
00:01:33,112-->00:01:35,113
Maria? Chuyện gì vậy? Đó là gì vậy?

24
00:01:37,512-->00:01:40,113
Tôi nghĩ tôi đã thấy trong một khoảnh khắc 
có một "tiếng ồn" chạy qua mắt tôi.
Nhưng đó không phải là tầm nhìn của tôi.
Đó là toàn bộ thế giới.

25
00:01:41,112-->00:01:48,113
Chuyện gì đang xảy ra?
Sức mạnh của tớ đã...

26
00:01:52,112-->00:01:54,113
Maria... Này Maria!

26
00:02:01,512-->00:02:10,513
Tôi đã chờ rất lâu rồi.
Cuối cùng thời khắc này cũng đến.

26
00:02:11,512-->00:02:13,113
Cậu là... Arusu? Là... Arusu Đen sao?

26
00:02:14,112-->00:02:18,513
Cậu sai rồi. 
TÔI-LÀ-SỰ-THẬT.

26
00:02:19,513-->00:02:21,113
Vậy người gọi bọn tớ đến 
thế giới này cũng là cậu sao?

26
00:02:22,112-->00:02:30,113
Tôi không làm thế. 
Người gọi các cậu chính là 
quản trị của thế giới này, Maria.

26
00:02:31,112-->00:02:33,113
Arusu nói và nhìn Maria, 
người đang quỳ ở dưới đất.

26
00:02:34,112-->00:02:35,513
Chuyện gì... đang xảy ra vậy?

26
00:02:36,513-->00:02:44,113
Nhưng thật không may, bây giờ tôi đã 
trở thành người quản trị. 
Tôi đã chờ đợi rất lâu rồi.

26
00:02:44,513-->00:02:52,113
Tôi cứ phải ngồi chờ lần nữa và lần nữa,
xem cô ta hẹn hò. Cuối cùng cô ta 
cũng tìm ra câu trả lời cho tình yêu.

26
00:02:56,512-->00:02:58,113
Maria? Cậu không sao chứ?

26
00:02:59,112-->00:03:12,113
Vâng... Tớ không sao. Tuy nhiên,
đã có sự can thiệp vào quyền quản lý của tớ.
Giờ tớ đã gần như mất quyền quản lý rồi.

26
00:03:13,113-->00:03:16,513
Nhưng đó là một cuộc trao đổi công bằng phải không?

26
00:03:17,513-->00:03:19,113
Trao đổi công bằng sao?

26
00:03:20,112-->00:03:34,113
Tôi là người đã cho cô ấy tất cả mọi thứ.
Giọng nói, hình ảnh, thông tin như một con người.
Nếu không phải vậy, cô thậm chí còn không tồn tại.

26
00:03:35,112-->00:03:47,113
Tôi không tồn tại sao?
Ý cô là tôi được tạo ra do những bất thường
mà cô là nguyên nhân sao?

26
00:03:48,112-->00:03:56,113
Đúng thế. Nhưng không phải 
tôi tạo ra cô vì tôi muốn thế đâu. 

26
00:03:56,512-->00:04:05,113
Đơn giản là thế giới này do bị bóp méo 
bởi sự tồn tại của tôi đã tạo ra cô. 
Đó là sự thật.

26
00:04:06,112-->00:04:07,113
Cái...?

26
00:04:08,112-->00:04:13,513
Trên thực tế, việc này không đáng quan tâm.
Nhưng cậu đã giúp tôi giải quyết vấn đề.

26
00:04:14,512-->00:04:15,513
Có vấn đề sao?

32
00:04:16,512-->00:04:28,513
Đúng vậy. Khi tôi xâm nhập vào máy chủ Fraxinus,
hệ thống bảo vệ đã nhốt tôi vào thế giới này.

33
00:04:29,513-->00:04:31,113
Cậu đã bị nhốt sao?

36
00:04:32,113-->00:04:45,513
Đúng vậy. Đến giờ cậu không nhận ra.
Nhưng Itsuka Shidou, nhờ có cậu
tôi có thể thoát ra khỏi thế giới này!

38
00:04:46,512-->00:04:57,513
Tuy nhiên, giờ cậu và cô ta 
đều không cần thiết nữa.
Cả hai sẽ biến mất... cùng với thế giới này!

43
00:04:58,512-->00:05:00,113
Arusu! Cậu không đùa chứ?

44
00:05:01,112-->00:05:12,513
Đừng gọi tôi như thế!
Tôi có tên riêng của tôi: Arusu Marina.
Đó là thứ duy nhất tôi nhận được từ cha tôi!

45
00:05:13,512-->00:05:15,113
Marina... đó là tên cậu sao?

46
00:05:16,112-->00:05:27,113
Đúng vậy. Vậy là cậu không nhầm nữa rồi nhé.
Maria và Marina là hai người hoàn toàn khác nhau. 
Và bên cạnh đó, tôi là bản gốc.

47
00:05:27,512-->00:05:35,113
Tôi đã mất nhiều thời gian quá rồi.
Giờ tôi sẽ làm việc mình cần làm. Tạm biệt.

48
00:05:35,512-->00:05:38,513
Khoan đã! Cậu định kết thúc tất cả sao?
Cậu nghĩ tớ sẽ chấp nhận chuyện đó à?

49
00:05:39,112-->00:05:41,113
Thật đáng tiếc.

50
00:05:42,512-->00:05:44,113
Cái gì? Cơ thể mình không di chuyển được?

51
00:05:45,112-->00:05:47,113
...Maria, còn cậu thì sao?

52
00:05:48,112-->00:05:55,113
Đây là một chức năng 
có trong quyền quản lý.

53
00:05:56,112-->00:05:59,113
Maria đang thở dốc.
Đây cũng là do Arusu Đen, Marina sao?

54
00:05:59,512-->00:06:01,513
Shin. Cậu nghe thấy tôi không?

55
00:06:02,112-->00:06:03,513
Giọng nói này ... Reine-san?

56
00:06:04,112-->00:06:10,113
Thật tuyệt khi tôi chỉ liên lạc được
với cậu khi tình hình càng lúc càng tệ.

57
00:06:10,512-->00:06:13,113
Reine-san, bọn em đang gặp rắc rối lớn! 
Có đến hai Arusu!
Và Arusu Đen nói cô ta kiểm soát thế giới này!

41
00:06:13,512-->00:06:21,113
Shin, bình tĩnh nào.
Chúng tôi đã theo dõi tình hình từ đây.
Cậu có muốn nghe không?

42
00:06:22,112-->00:06:23,513
Vâng... làm ơn.

43
00:06:24,112-->00:06:31,113
Được rồi, tôi sẽ bắt đầu
từ việc chúng ta đã nhận định sai.

44
00:06:32,112-->00:06:38,513
Tinh Linh Nhân Tạo, nguyên nhân của 
những gì đang xảy ra hiện giờ.
Đó không phải là Shiroi Arusu (Arusu Trắng).

44
00:06:39,112-->00:06:42,513
Nhưng như cậu nói, Shin, chính là 
Kuroi Arusu (Arusu Đen). Arusu Marina.

45
00:06:44,112-->00:06:46,113
Điều đó em hiểu. Nhưng quan trọng nhất là 
thế giới này sắp biến mất.

46
00:06:47,112-->00:06:55,513
Đúng vậy. Thế giới đó giờ bị kiểm soát
bởi Marina Arusu. Nhưng không chỉ có thế. 

46
00:06:56,112-->00:07:03,113
Cô ta đã thâm nhập vào máy chủ,
ảnh hưởng đến hầu hết các chức năng 
của Fraxinus ngoài thực.

47
00:07:04,113-->00:07:07,113
Hầu hết các chức năng của Fraxinus sao?
Nghĩa là mọi người ở Ratatoskr
không thể kiểm soát từ bên ngoài?

48
00:07:08,112-->00:07:21,513
Kannazuki đang làm mọi thứ có thể,
nhưng rất khó để phục hồi. Lớp bảo mật mạnh mẽ 
lại trở thành một trở ngại thực sự.

60
00:07:22,512-->00:07:24,113
Vậy ngoài đó, mọi người có thể làm gì?

61
00:07:25,112-->00:07:31,113
Chúng tôi sẽ làm mọi thứ có thể,
nhưng có khả năng là chúng tôi
không thể giải quyết vấn đề tận gốc.

61
00:07:31,512-->00:07:35,513
Chúng tôi thậm chí không thể điều khiển
hoạt động của con tàu được nữa.

62
00:07:36,112-->00:07:37,513
Vậy giờ bọn em phải làm gì?

63
00:07:38,113-->00:07:46,513
Những người trong thế giới ảo phải
ngăn chặn Tinh Linh Nhân Tạo.
Có lẽ không còn cách nào khác.

64
00:07:47,112-->00:07:50,113
Nghĩa là chúng ta phải đánh bại Marina?
Nhưng nếu cô ấy có thể làm bất cứ điều gì 
với thế giới này, chúng ta có thể làm gì được?

65
00:07:51,112-->00:07:54,513
Vẫn có cách...

66
00:07:55,512-->00:07:57,113
Maria? Cậu nói sao?

67
00:07:57,512-->00:08:06,113
Hệ thống Fraxinus 90% đã bị phong tỏa,
Bản thân tớ đã là hệ thống 
hoạt động độc lập tạm thời.

68
00:08:06,512-->00:08:11,113
Vì vậy, tớ đã thoát khỏi sự
ảnh hưởng của Arusu Marina.

68
00:08:12,112-->00:08:14,113
Tớ không hiểu lắm. 
Nhưng giờ cậu không sao rồi phải không?

69
00:08:14,512-->00:08:18,113
Vâng. Xin lỗi đã làm cậu lo lắng.

81
00:08:18,512-->00:08:29,113
Tuy nhiên, những gì đang xảy ra bây giờ,
tất cả là trách nhiệm của tớ. 
Nếu tớ không bị mất quyền quản trị 
thì đã không thế này.

82
00:08:30,112-->00:08:32,113
Không phải thế! 
Maria, không phải do cậu đâu!

83
00:08:33,112-->00:08:43,513
Tại tớ. Tất cả là do tớ không ngăn chặn được
các cuộc tấn công đầu tiên của Arusu Marina.

84
00:08:44,512-->00:08:48,113
Marina tấn công sao? Đó là hack vào
Fraxinus phải không? Cậu không có lỗi.
Maria, cậu là quản trị thế giới này.

85
00:08:48,512-->00:08:51,113
Shin, cậu sai rồi.

86
00:08:52,512-->00:08:54,113
Reine-san? Nghĩa là sao?

87
00:08:54,512-->00:08:56,513
Mặc dù tôi hỏi vậy...
Nhưng tôi lại có một cảm giác mơ hồ.

88
00:08:57,512-->00:08:59,112
Chúng tôi đều nghĩ rằng Maria 
là một sự bất thường,
một Tinh Linh Nhân Tạo từ bên ngoài.

88
00:08:59,512-->00:09:02,113
Vì vậy, để đáp ứng mục tiêu của Maria,
chúng tôi cố gắng dạy cô ấy biết "tình yêu là gì"

89
00:09:02,512-->00:09:05,513
Nhưng chúng tôi đã sai.
Người bóp méo thế giới này không phải là Maria.
Mà là Arusu Đen... Marina.

98
00:09:06,112-->00:09:07,513
Nhưng nếu vậy, một câu hỏi được đặt ra:

99
00:09:08,112-->00:09:10,113
Cô gái trước mặt tôi, Arusu Maria là ai?

100
00:09:11,112-->00:09:20,113
Shin, bình tĩnh nghe đây.
Cô gái trước mặt cậu có lẽ là AI của Fraxinus.
(Artificial Intelligence: trí tuệ nhân tạo)

101
00:09:21,112-->00:09:22,113
Cái gì?

102
00:09:22,512-->00:09:24,513
Maria là AI của Fraxinus sao?

103
00:09:25,112-->00:09:28,113
Fraxinus, chiếc phi thuyền của Ratatoskr này 
được trang bị một trí tuệ nhân tạo.
Tôi đã nghe Kotori và Reine-san nói thế.

104
00:09:28,512-->00:09:33,113
Đúng vậy. Khi tôi hẹn hò với Tinh Linh 
và hiển thị tùy chọn,
trí tuệ nhân tạo đó đã được xem
tôi và các Tinh Linh gần gũi nhau hơn.

105
00:09:33,512-->00:09:42,113
Vâng. Chỉ khi quyền quản lý của tớ 
đã bị đánh cắp, sự phong tỏa 
đã được mở ra, các thông tin đã được tiết lộ.

105
00:09:42,512-->00:09:49,113
Tớ là AI điều khiển con tàu này. 
Có khả năng tớ chính là Fraxinus.

106
00:09:50,112-->00:10:00,112
Do các cuộc tấn công của Arusu Marina, 
một phần hệ thống bảo vệ Fraxinus bị phá hủy.
Đó là lúc Arusu Marina thâm nhập vào hệ thống.

107
00:10:00,512-->00:10:10,513
Nhưng AI đã khóa Arusu Marina trong khu vực
có bảo vệ tốt nhất. Đó là thế giới
mô phỏng thực tế ảo này.

108
00:10:11,112-->00:10:22,113
Những gì xảy ra tiếp theo có lẽ như
Arusu Marina nói. Do biến động này,
những ký ức bị chặn của Maria đã trở lại.

113
00:10:22,512-->00:10:37,113
Đây chỉ là phỏng đoán của tôi. 
Nhưng có lẽ để bảo vệ khỏi kẻ thù bí ẩn,
Maria đã được sinh ra.

114
00:10:38,112-->00:10:41,113
Vậy tại sao Maria muốn biết về tình yêu?
Có vẻ như Marina đã quản lý 
hành vi và cảm xúc của Marina.

1
00:10:42,112-->00:10:53,513
Có lẽ khi hệ thống bị chặn, thứ duy nhất 
còn lại là mục đích của thế giới đó
là để tìm kiếm tình yêu. Hoặc là...

1
00:10:54,112-->00:10:55,513
Hoặc là sao?

1
00:10:56,112-->00:11:06,513
Hoặc là mục tiêu cơ bản nhất của 
AI Fraxinus là để cung cấp 
hỗ trợ các cuộc hẹn cho Shin với các Tinh Linh.

1
00:11:07,112-->00:11:14,113
Giám sát trạng thái cảm xúc của Tinh Linh 
và chương trình các tùy chọn thích hợp nhất 
với tình hình cụ thể.

1
00:11:14,512-->00:11:21,113
Nói cách khác, 
AI của Fraxinus là một trí tuệ nhân tạo 
được sinh ra để tìm hiểu tình yêu.

1
00:11:22,112-->00:11:23,513
Sinh ra... để tìm hiểu tình yêu.

1
00:11:24,112-->00:11:29,113
Đúng vậy. Đó có lẽ là lý do
khiến Arusu muốn biết chính xác 
"tình yêu là gì?"

368
00:11:29,512-->00:11:38,113
Mặc dù cô ấy không biết
tại sao mình cần cảm xúc đó.

68
00:11:41,112-->00:11:45,113
Nhưng lần này, 
cô ấy đã mở cửa hệ thống bảo vệ.

368
00:11:46,112-->00:11:58,113
Vâng. Việc bảo vệ Fraxinus tiếp tục 
hoạt động trong vô thức của tớ
Tuy nhiên, có một phương pháp để phá bỏ.

368
00:11:59,112-->00:12:06,113
Đó là sự phát triển của của AI
trở nên có tính người hơn.

368
00:12:07,112-->00:12:08,513
Có tính người hơn là sao?

368
00:12:09,112-->00:12:23,113
Nhưng đó là một con dao hai lưỡi.
Tớ đã mất cảnh giác. Tớ đã hủy hệ thống bảo vệ 
đang hoạt động trong vô thức của mình.

368
00:12:23,513-->00:12:26,113
Nếu vậy nghĩa là đây không phải 
là lỗi của cậu. Tớ cũng có lỗi.

368
00:12:27,113-->00:12:36,113
Không. Đó là lỗi của tớ. 
Tớ chắc rằng những người khác cũng nghĩ vậy.

368
00:12:36,513-->00:12:44,513 
Chỉ vì tớ muốn tìm câu trả lời 
nên đã kéo tất cả mọi người vào đây.

368
00:12:45,112-->00:12:46,513
Mọi người sẽ không nghĩ thế đâu!

368
00:12:47,112-->00:12:52,113
Không quan trọng mọi người nghĩ thế nào!
Sự thật là thế!

368
00:12:52,513-->00:13:00,513
Hai người, không có thời gian để đổ tội đâu.
Trong khi các cậu đang đứng đó,
tình hình đang xấu đi đấy.

368
00:13:01,112-->00:13:03,113
Tình hình đang xấu đi sao?
Marina đang muốn làm gì vậy?

368
00:13:03,512-->00:13:11,513
Lúc đầu tôi nghĩ cô ta muốn 
thông tin hay công nghệ Ratatoskr. 
Nhưng có vẻ cô ta muốn quấy phá đơn giản hơn.

368
00:13:12,112-->00:13:13,513
Quấy phá đơn giản sao?

368
00:13:14,112-->00:13:27,113
Chỉ cần pháo Fraxinus kích hoạt,
thành phố ở phía dưới sẽ bị hủy diệt.

368
00:13:27,512-->00:13:30,513
Kích hoạt pháo của Fraxinus sao?
Nếu chuyện đó xảy ra thì sao?

368
00:13:31,112-->00:13:37,513
Sẽ là một thảm họa không thể tưởng tượng nổi.

368
00:13:38,112-->00:13:39,513
Không có cách nào để ngăn chặn sao?

368
00:13:40,112-->00:13:53,113
Tôi nói trước là sẽ phức tạp lắm đấy.
Nhưng vì không còn cách nào khác. 
Nghe này, Shin. Phương pháp này là...

368
00:14:00,112-->00:14:09,513
Realizer cơ bản, thực hiện kiểm soát, 
định vị pháo, gắn ống kính... 
thế này đủ rồi.

368
00:14:10,512-->00:14:25,113
Mình sẽ thực hiện nhiệm vụ được giao.
Và sau đó, mình sẽ học nhiều điều mình không biết!
Đó là... mong muốn duy nhất của mình.

368
00:14:30,112-->00:14:37,513
Tại sao mình lại nghĩ đến Itsuka Shidou?
Không phải! Mình không cần 
một người như Itsuka Shidou!

368
00:14:42,112-->00:14:50,113
Không truy cập vào kiểm soát Realizer sao?
Họ có thể lấy lại kiểm soát à?

368
00:14:51,112-->00:14:57,113
Nhưng họ không thể kiểm soát bằng tay 
để lấy lại Realizer được.

368
00:14:58,112-->00:15:08,113
Nhưng vì sự bất thường thường này xảy ra
trong lúc đang tải ma thuật vào pháo. 
Có lẽ... là do cô ta?

368
00:15:09,112-->00:15:19,113
Nếu vậy thì, tôi sẽ chơi với các người.
Nhưng tất cả biến mất cùng thế giới này.

368
00:15:24,512-->00:15:27,113
Đường phố... thế này là sao?

368
00:15:28,112-->00:15:36,113
Phải. Đã có vấn đề phát sinh trong
cấu trúc không gian. 

368
00:15:37,112-->00:15:47,113
Tôi lo sợ có khả năng Marina 
đang cố gắng loại bỏ thế giới này. 
Trong khi các cậu vẫn bị mắc kẹt trong đó.

368
00:15:48,112-->00:15:49,513
Nếu thế giới này bị loại bỏ
thì chuyện gì sẽ xảy ra?

368
00:15:50,513-->00:16:00,113
Nếu bị loại bỏ khi tâm trí vẫn còn 
trong thế giới đó, cậu sẽ bị tổn thương. 
Nói cách khác, có khả năng cậu sẽ 
không bao giờ mở mắt được nữa.

368
00:16:01,113-->00:16:03,113
Chúng ta đang ở trong một bia mộ. 
Nếu chúng ta ngăn chặn Marina, 
chúng ta sẽ ngăn được sự hủy diệt.

368
00:16:04,112-->00:16:10,113
Phải. Nếu kế hoạch này thành công, 
tất cả các cậu sẽ có thể quay về.

368
00:16:11,112-->00:16:13,113
Dù sao, chúng ta không thể để cho Fraxinus 
thế này được. Chúng ta phải bảo vệ 
thế giới ảo này và ngoài thực cùng lúc!

368
00:16:14,112-->00:16:17,113
Đúng vậy. Đó là cách tốt nhất.

368
00:16:18,112-->00:16:21,113
Tớ cũng sẽ giúp cậu hết sức.

368
00:16:22,112-->00:16:23,513
Cảm ơn. Trông cậy vào cậu đấy.

368
00:16:24,112-->00:16:26,113
Vâng.

368
00:16:30,512-->00:16:33,513
Shidou! Cả Maria nữa!

368
00:16:34,112-->00:16:36,513
Kotori! May mà em không sao.
Tình hình này là...

368
00:16:37,112-->00:16:45,113
Không sao. Em biết rồi.
Reine, cô vẫn còn đủ điều kiện 
để có quyền quản lý Fraxinus phải không?

368
00:16:46,112-->00:16:57,113
Đúng vậy. Tuy nhiên, giống như cô 
đã nói trước đây. Chúng ta đã kiểm soát 
lại một phần hệ thống và có thêm thời gian.

368
00:16:58,112-->00:17:05,113
Sau đó, chúng ta phải nhanh đến nơi điều khiển. 
Nếu chiếm được, chúng ta sẽ 
giành lại quyền kiểm soát.

368
00:17:06,112-->00:17:07,513
Đó là phòng đầu não (Mother Room) phải không?

368
00:17:08,112-->00:17:16,513
Đúng vậy. Nó là một khối thế giới ảo.
Tinh Linh Nhân Tạo không thể di chuyển 
xung quanh thế giới thực.

368
00:17:17,112-->00:17:22,113
Vì vậy, đó là nơi thích hợp nhất
để hoạt động Fraxinus.

368
00:17:22,512-->00:17:24,113
Cô ấy thực sự đang ở đó sao?

368
00:17:25,112-->00:17:32,113
Đừng lo, Shidou.
Arusu Marina chắc chắn đang ở phòng đầu não.

368
00:17:33,112-->00:17:35,113
Cô biết sao Maria?

368
00:17:35,512-->00:17:44,113
Vâng. Dù rất yếu nhưng nếu tôi có thể
kết nối vào hệ thống lần nữa, nhất định...

368
00:17:45,112-->00:17:50,513
Như thế quá nguy hiểm! 
Maria, không biết cô chịu được nữa không.

368
00:17:51,112-->00:17:53,513
Đừng làm chuyện điên rồ, Maria.
Chúng ta sẽ đi cùng nhau.

368
00:17:54,112-->00:17:55,113
Vâng!

368
00:17:56,112-->00:18:01,113
Để chiến đấu trong thế giới này, 
sức mạnh của Maria rất cần thiết.

368
00:18:01,512-->00:18:11,113
Điều duy nhất trong thế giới này 
không bị Marina can thiệp là sức mạnh Tinh Linh.
Mà sức mạnh đó lại phụ thuộc vào Maria.

368
00:18:12,112-->00:18:25,513
Vâng. Tôi đang kết nối với Reine trong hệ thống.
Như vậy, sức mạnh Tinh Linh 
trong không gian ảo sẽ như ngoài thực.

368
00:18:26,512-->00:18:37,113
Mặc dù vậy, tất nhiên nó sẽ khác với ngoài thật. 
Thế giới ảo hiện giờ là lãnh thổ của Marina. 

368
00:18:37,512-->00:18:41,113
Hãy nhớ rằng đối thủ không phải con người, 
mà là cả thế giới.

368
00:18:42,112-->00:18:45,113
Cả thế giới sao? Lúc nãy khi em
muốn chặn Marina lại, 
em đã không cử động được.

368
00:18:46,112-->00:18:52,113
Anh nói chi tiết hơn đi.
Chúng ta có rất ít thông tin về kẻ thù.

368
00:18:52,512-->00:18:54,113
Dạng như có ai đó điều khiển anh vậy.

368
00:18:55,112-->00:18:59,113
Giống như Gabriel của Miku à?

368
00:19:00,112-->00:19:02,113
Không, em nghĩ nó hơi khác...
Nó chỉ là khống chế tạm thời thôi.

368
00:19:02,512-->00:19:09,513
Đó là một sự can thiệp 
nhằm làm hạn chế hoạt động của đối thủ.

368
00:19:10,112-->00:19:17,113
Ra là vậy. Nghĩa là chúng ta phải suy nghĩ 
cách thắng trong trò chơi này.
Có vẻ chúng ta đang đi săn boss đấy.

368
00:19:18,112-->00:19:21,113
Um... Nói ngắn gọn là nó không thể 
tẩy não như Miku, nhưng có thể làm 
đối phương bất động à?

368
00:19:22,112-->00:19:35,113
Phải. Trong thế giới này, chỉ cần cài đặt
giới hạn, hoạt động sẽ bị hạn chế đi nhiều.

368
00:19:36,112-->00:19:38,113
Vậy là sẽ không dễ đâu.

368
00:19:38,512-->00:19:44,113
Đúng vậy. Để phá vỡ,
sức mạnh của tớ sẽ rất cần thiết.

368
00:19:45,112-->00:19:54,113
Dù tớ có bị biến mất, 
tớ sẽ giúp mọi người bằng mọi thứ tớ có. 
Mọi người và Shidou đã dạy tớ rất nhiều.

368
00:19:55,112-->00:19:56,513
Maria...

368
00:19:57,112-->00:20:07,113
Vì vậy, không ai có thể thay đổi
thế giới mà tớ đang sống được.

368
00:20:08,113-->00:20:20,113
Họ là những cảm giác, cảm xúc, liên kết,
họ không chỉ là dữ liệu.
Họ là những thứ có hình dạng, cơ thể.

368
00:20:20,512-->00:20:29,513
Tớ giúp mọi người vì tớ muốn thế.
Đó là lý do duy nhất.

368
00:20:30,113-->00:20:34,113
Nếu cậu muốn vậy, chúng ta sẽ làm được. 

368
00:20:35,113-->00:20:40,113
Được! Chúng ta sẽ đi.
Reine, những người khác sao rồi?

368
00:20:41,113-->00:20:49,113
Họ đã biến hình và mặc Thiên Phục.
Nhưng có lẽ họ sẽ không có đầy đủ sức mạnh.

368
00:20:49,512-->00:20:57,113
Bây giờ việc còn lại là hoàn thành mục tiêu. 
Reine, tôi giao Fraxinus cho cô đấy.

368
00:20:58,112-->00:21:00,113
Tôi sẽ lo việc đó.

368
00:21:04,512-->00:21:10,513
Shidou! Maria! Em nghe nói 
họ đã tìm thấy Tinh Linh Nhân Tạo rồi.
Hai người không sao chứ?

368
00:21:11,112-->00:21:12,513
Ừ. Anh và Maria không sao.

368
00:21:13,112-->00:21:18,513
Xin lỗi đã làm cô lo lắng, Tohka.

368
00:21:20,112-->00:21:27,513
Cô nói gì vậy?
Không cần phải xin lỗi, Maria.
Thủ phạm chính là Tinh Linh Nhân Tạo, phải không?

368
00:21:28,112-->00:21:34,113
Nhưng tôi cũng có lỗi. 
Chính tôi đã đưa mọi người vào đây.

368
00:21:35,112-->00:21:43,113
Cô nói gì thế?
Nhờ đó tôi ở đây, tôi mới có thể chiến đấu
Thực sự tôi mới phải cảm ơn cô.

368
00:21:47,112-->00:21:55,113
Đúng như Yatogami Tohka nói.
Đánh nhau từ bên trong thế này rất thuận lợi.

368
00:21:55,512-->00:22:01,113
Ngoài ra, bảo vệ Shidou luôn là ưu tiên hàng đầu.

368
00:22:02,112-->00:22:03,513
Origami, cậu đủ sức không?

368
00:22:04,112-->00:22:09,113
Không vấn đề gì. Ngay cả trong trường hợp này,
tớ có thế giúp ích nhiều hơn Yatogami Tohka.

368
00:22:10,112-->00:22:14,513
Cô nói gì?
Tôi không bỏ qua đâu, Tobiichi Origami!

368
00:22:15,512-->00:22:20,113
Đây không phải lúc để đánh nhau đâu...

368
00:22:21,112-->00:22:30,513
Đừng lo Maria. Tôi, Shidou 
và tất cả mọi người sẽ bảo vệ cô.

368
00:22:31,512-->00:22:44,113
Không sao cả. Tôi chiến đấu vì Shidou.
Tuy nhiên, nếu cần thiết, 
tôi vẫn có thể bảo vệ cô.

368
00:22:45,512-->00:22:52,113
Tohka... Origami...
Cảm ơn rất nhiều.

368
00:22:53,512-->00:23:00,113
Em... em cũng sẽ cố gắng!

368
00:23:01,112-->00:23:03,513
Yoshino tuyệt thật đấy!

368
00:23:04,112-->00:23:06,513
Được rồi. Yoshino, anh tin tưởng em đấy.

368
00:23:07,112-->00:23:15,513
Vâng! Để bảo vệ mọi người...
em sẽ làm... những gì có thể...

368
00:23:16,513-->00:23:23,513
Cảm ơn nhé, Yoshino.
Chị cũng nghĩ thế.

368
00:23:24,512-->00:23:33,513
Được rồi. Như tôi đã nói, thế giới này
giờ hoàn toàn thuộc về Tinh Linh Nhân Tạo.

368
00:23:34,112-->00:23:42,113
Mặc dù chúng ta có nhiều Tinh Linh,
nhưng chúng ta có thắng hay không
phải cần đến tất cả mọi người.

368
00:23:47,112-->00:23:55,113
Vì vậy, hãy sử dụng mọi thứ đang có.
Maria, cô là hy vọng cuối cùng đấy.

368
00:23:56,113-->00:23:57,113
Vâng!

368
00:23:59,112-->00:24:09,113
Chúng ta sẽ cùng đi.
Không cần phải lo đâu.
Tôi sẽ bảo vệ Darling và Maria-chan.

368
00:24:09,512-->00:24:11,513
Phải. Như cậu thấy đấy Miku.
Bọn tớ trông cậy vào cậu đấy.

368
00:24:12,112-->00:24:19,113
Tôi cũng nhờ cô đấy.
Nếu không thoát được, 
mọi người sẽ bị tổn thương.

368
00:24:20,112-->00:24:27,113
Tất nhiên rồi. 
Vì Darling và các cô gái xinh đẹp,
tôi sẽ làm tất cả.

368
00:24:29,112-->00:24:32,513
Mọi người sẵn sàng chưa? Chúng ta đi thôi.

368
00:24:35,112-->00:24:43,113
Ara, ara, ara... Kế hoạch của các cô
là để tôi ở lại trông nhà à?

368
00:24:44,112-->00:24:52,113
Tất nhiên là không. Tất cả đều phải đi.
Nhưng cô chỉ xuất hiện những lúc
bất ngờ thôi phải không?

368
00:24:53,112-->00:25:07,113
Tôi hay làm thế lắm à?
Nhưng may mắn là tôi sắp được vui chơi sau 
một thời gian dài rồi đấy. 

368
00:25:08,112-->00:25:16,113
Mọi người, em đang thắc mắc
phòng đầu não là gì vậy?

368
00:25:17,112-->00:25:25,513
Giống như một phòng điều khiển 
trong không gian. Các cô có thể vào đó 
vì thế giới này bắt đầu sụp đổ.

368
00:25:26,512-->00:25:33,113
Chúng ta gọi đó là "may mắn" sao?
Tóm lại, giờ nhiệm vụ này sẽ khó khăn đấy.

368
00:25:35,112-->00:25:47,113
Vậy là cuộc hành trình đến với 
số phận đã bắt đầu rồi à?
Được. Hãy chỉ đường cho chúng tôi đi.

368
00:25:48,112-->00:25:58,113
Đồng tình. Cứ để cho chúng tôi. 
Yamai, những người điểu khiển gió 
sẽ mang đi tất cả.

368
00:25:59,112-->00:26:06,113
Mặc dù chỉ là một thế giới ảo, 
nó là nơi tôi đang sống với những người bạn.
Tôi sẽ không để ai phá hủy nó đâu!

368
00:26:07,112-->00:26:16,513
Nhấn mạnh. Cùng nhau đánh bại 
Tinh Linh Nhân Tạo và trở về thế giới thật.

368
00:26:17,512-->00:26:19,513
Đúng vậy. Chúng ta sẽ làm thế.
Cả cậu nữa đấy, Maria.

368
00:26:22,112-->00:26:26,113
Vâng. Tất nhiên rồi.

368
00:26:27,112-->00:26:33,113
Được! Đi thôi! Chúng ta không biết 
những gì đang chờ đợi. Mọi người cẩn thận đấy.

368
00:26:33,512-->00:26:38,113
Nào, hãy bắt đầu cuộc hẹn của chúng ta thôi!







368
00:26:47,112-->00:26:52,113
Dừng lại. Có vẻ mọi người có thể
vào phòng đầu não rồi.

368
00:26:52,512-->00:26:54,513
Ở đây sao? Ở không gian bị phá vỡ này à?

368
00:26:55,112-->00:27:00,113
Đúng vậy. Có vẻ không còn đường nào khác.

368
00:27:01,112-->00:27:07,113
Tớ nghĩ tớ có thể dẫn đường.
Tớ cảm thấy Marina đang ở gần đây.

368
00:27:08,112-->00:27:13,513
Thế thì tốt.
Nhiều khả năng cô ta biết chúng ta đang đến. 
Hãy cẩn thận.

368
00:27:14,112-->00:27:15,113
Vâng. Đi nào.

368
00:27:19,112-->00:27:22,113
Một khung cảnh tuyệt vời.
Tôi cảm thấy có một sàn nhà dưới chân tôi.
Không gian này dường như vô tận.

368
00:27:23,112-->00:27:24,513
Chính là đây.

368
00:27:28,512-->00:27:30,113
Làm sao nó trải dài đến vậy?
Cậu không thể đi hết cả chỗ này phải không?

368
00:27:31,112-->00:27:36,513
Tớ có thể cảm nhận được không gian này.

368
00:27:37,112-->00:27:41,113
Cứ tiếp tục đi. Đừng để ý xung quanh.
chúng ta chắc chắn đang đến gần rồi.

368
00:27:42,112-->00:27:44,113
Shidou, kia kìa!

368
00:27:45,112-->00:27:55,113
Cậu không biết từ bỏ phải không, Itsuka Shidou?
Con gái không thích con trai 
đeo bám mình dai dẳng đâu.

368
00:27:56,112-->00:27:58,513
Marina! Dừng lại đi!
Việc này sẽ không khiến ai vui vẻ cả đâu.

368
00:27:59,112-->00:28:07,513
Không đúng. Tôi biết anh ta.
Tôi biết một người luôn giữ lý tưởng 
của riêng mình, dù có phải hủy diệt thế giới này.

368
00:28:08,112-->00:28:13,113
Vì lý do đó,
đây là một sự hy sinh cần thiết của tôi.

368
00:28:14,112-->00:28:16,113
Đó là lý do sao? Chẳng có lý do nào
đáng để hy sinh mạng sống 
của cả ngàn người được!

368
00:28:17,112-->00:28:28,513
Đừng thuyết phục vô ích nữa, Itsuka Shidou.
Tốt nhất là cậu hãy ở đây
trong tuyệt vọng cùng bạn bè cậu đi.

368
00:28:30,112-->00:28:33,513
Đừng nói những câu ngu ngốc nữa, 
Tinh Linh Nhân Tạo!

368
00:28:34,112-->00:28:41,113
Shidou và chúng ta sẽ không đầu hàng!
Đến đây! Kết thúc chuyện này đi!

368
00:28:42,112-->00:28:49,113
Kết thúc à? Nghĩa là cô muốn biến mất
ngay bây giờ sao?

368
00:28:50,112-->00:28:57,113
Thật ngu ngốc. Cô định một mình
đánh lại bọn tôi sao?

368
00:28:58,112-->00:29:09,113
Cảnh báo. Kaguya, đó là cụm từ 
thường thấy của những người sắp thua.
Tôi cảm thấy bất an đấy.

368
00:29:10,112-->00:29:18,113
Nh... Nhưng nếu rõ ràng chúng ta đang có
lợi thế mà, phải không?

368
00:29:19,512-->00:29:26,113
Bác bỏ. Trong tình huống như vậy,
vấn đề lớn thường sắp xảy ra đấy.

368
00:29:28,112-->00:29:37,113
Tôi biết về các cô, Berserk!
Tôi cũng biết về tất cả các cô rồi!

368
00:29:38,112-->00:29:45,513
Thiên Phục, Thiên Thần... 
Những Tinh Linh trở thành vũ khí mạnh mẽ.

368
00:29:46,112-->00:29:52,113
Tuy nhiên, chắc các cô không nghĩ 
tôi sẽ không chuẩn bị biện pháp đối phó chứ?

368
00:29:53,112-->00:29:54,513
Đây là... giống Kurumi sao?

368
00:29:55,112-->00:30:08,113
Cậu thấy sao? Tất nhiên tôi không cần
phải làm thế này. Tuy nhiên, nếu các người muốn,
tôi nên đánh hết sức chứ nhỉ.

368
00:30:08,513-->00:30:11,113
Marina nhân lên hàng chục...
Không, còn nhiều hơn thế!

368
00:30:12,112-->00:30:17,113
Ara, ara. Cô đang làm tôi hứng thú đấy.

368
00:30:17,512-->00:30:19,513
Đó là sức mạnh giống cậu sao?

368
00:30:20,112-->00:30:31,113
Nó khác với "các tớ". "Các tớ" là "tớ" 
của những thời điểm khác nhau.
Nhưng thứ này...

368
00:30:32,112-->00:30:41,113
Chúng là bản sao giống hệt nhau. 
Không giống các Kurumi nhân bản 
có sức mạnh yếu hơn so với bản gốc. 
Nhưng còn chúng...

368
00:30:43,512-->00:30:50,113
Đúng vậy. Tôi không có giới hạn.
Tôi có thể sao chép y hệt bản thân.

368
00:30:51,112-->00:30:53,513
Các cô thực sự nghĩ có thể thắng sao?

368
00:30:55,112-->00:30:56,513
Maria...

368
00:30:57,512-->00:31:00,113
Vâng. Gì vậy Tohka?

368
00:31:01,112-->00:31:05,113
Trong số này có Tinh Linh Nhân Tạo không?

368
00:31:06,112-->00:31:12,113
Có lẽ là không. Ở đây toàn bộ là bản sao.

368
00:31:16,112-->00:31:20,113
Vậy thì chắc thắng rồi đúng không?

368
00:31:22,512-->00:31:25,113
Cô lạc quan quá nhỉ, Diva.

368
00:31:26,512-->00:31:38,113
Suy nghĩ một chút thôi. Nếu thực sự
cô có thể tạo ra các bản sao không giới hạn
thì cô đâu cần phải giấu bản thật.

368
00:31:39,112-->00:31:46,113
Nhưng cô vẫn giấu bản thật, 
nghĩa là cô nghĩ mình vẫn có thể thua
bọn tôi phải không?

368
00:31:47,112-->00:31:56,113
Đúng thế. Cô ta chỉ đang cố gắng câu giờ thôi.

368
00:31:58,112-->00:32:03,113
Nếu thế thì xấu hổ thật đấy, Marina!

368
00:32:04,112-->00:32:07,513
Cô nghĩ vậy à?

368
00:32:08,512-->00:32:16,113
Vậy là phải đánh với cô ta à? 
Shidou, anh đi cùng Maria trước đi.

368
00:32:17,112-->00:32:18,513
Em sẽ ở lại sao, Kotori?

368
00:32:19,112-->00:32:24,113
Đúng vậy. Phong cách "anh đi trước,
cứ để em lo chỗ này" điển hình mà.

368
00:32:24,512-->00:32:26,113
Chiến đấu cùng nhau không tốt hơn sao?
Có quá nhiều kẻ thù mà.

368
00:32:27,112-->00:32:34,113
Reine-san cũng nói không còn thời gian mà.
Vì vậy, phần còn lại giao cho hai người đấy, 
Shidou-kun, Maria-chan!

368
00:32:35,112-->00:32:42,513
Bọn em sẽ ổn thôi. 
Cứ để bọn em chặn những người này.

368
00:32:43,512-->00:32:45,113
Em cũng vậy sao, Yoshino?
Nhưng vậy còn...

368
00:32:46,112-->00:32:57,113
Bọn em đang mặc Thiên Phục 
và có Thiên Thần trong thế giới này.
Anh còn lo lắng gì nữa chứ?

368
00:32:58,112-->00:32:59,513
Vậy anh để chỗ này cho bọn em nhé!

368
00:33:00,112-->00:33:05,113
Phải, đúng thế.
Vậy nên hãy bảo vệ Maria nhé.

368
00:33:06,112-->00:33:19,113
Yên tâm đi, Shidou-san.
Hãy tin bọn em.
Bọn em cũng tin... Shidou-san.

368
00:33:20,113-->00:33:22,113
Anh hiểu rồi! 
Maria, đi nào! Cậu biết chỗ này chứ?

368
00:33:23,112-->00:33:28,113
Vâng, tớ có thể cảm nhận nó...
Đi lối này!

368
00:33:31,512-->00:33:33,113
Nhanh lên, Maria!

368
00:33:33,512-->00:33:45,113
Vâng. Những bản sao đó vẫn kém hơn so với
bản gốc có sức mạnh của một Tinh Linh. 
Và hơn nữa, cô ta có thể chưa sử dụng
sức mạnh thật sự.

368
00:33:46,112-->00:33:47,513
Tớ cũng nghĩ thế.

368
00:33:48,112-->00:33:51,113
Cậu đang... lo lắng cho họ à?

368
00:33:52,112-->00:33:53,513
Không. Tớ hoàn toàn tin tưởng họ.

368
00:33:54,112-->00:34:00,113
Phải. Tớ cũng tin họ.
Lối này. Nhanh lên.

368
00:34:01,112-->00:34:03,113
Ừ!



368
00:34:47,112-->00:34:52,113


368
00:34:47,112-->00:34:52,113


368
00:34:47,112-->00:34:52,113


368
00:34:47,112-->00:34:52,113


