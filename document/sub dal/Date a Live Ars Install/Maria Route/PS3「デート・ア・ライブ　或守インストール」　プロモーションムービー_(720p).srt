﻿1
00:00:14,112-->00:00:17,113
Phiên bản game thứ 2 của Date a Live.

2
00:00:18,112-->00:00:22,113
Dựa theo tác phẩm của Tachibana Koushi,
minh họa bởi Tsunako.

3
00:00:22,312-->00:00:24,513
Xoay quanh một nhân vật trò chơi độc đáo.

4
00:00:25,112-->00:00:27,513
một câu chuyện về "sai lầm" và "tình yêu đầu tiên"

5
00:00:30,112-->00:00:35,113
Đây là game "My Little Shidou 2" 
phiên bản Super Simulator (Siêu giả lập)

6
00:00:36,112-->00:00:40,113
Trong game này, toàn bộ thành phố Tenguu
cùng các nhân vật đều được giả lập

7
00:00:40,512-->00:00:42,113
trong một không gian ảo, nơi cậu có thể 
trải nghiệm những cảm giác rất thật.

8
00:00:43,112-->00:00:47,113
Nói cách khác, nó là một trò chơi mà 
ý thức của cậu được gửi đến thế giới khác.

9
00:00:49,112-->00:00:51,113
Itsuka... Shidou.

10
00:00:51,512-->00:00:52,813
Cô ấy là ai?

11
00:00:53,113-->00:00:55,513
Tôi muốn hỏi cậu...

12
00:00:56,113-->00:00:59,113
Tình yêu... là gì vậy?

13
00:01:01,113-->00:01:06,113
Xuất hiện thứ được cho là không tồn tại, 
Tinh Linh Nhân Tạo (Spirit Artificial).















Arusu Maria. Từ bây giờ, 
hãy gọi tôi như thế, Itsuka Shidou.

14
00:01:07,213-->00:01:10,513
Do sự việc xảy ra bất ngờ,
Shidou đã bị mắc kẹt trong thế giới ảo.












Nhưng mà, Shidou sẽ mãi như vậy sao?

15
00:01:11,112-->00:01:14,113
Sẽ mãi như vậy? Anh ấy sẽ 
không bao giờ thức dậy sao?



16
00:01:15,112-->00:01:20,113
Những gì Tohka và các cô gái hy vọng,
cách để đưa Shidou trở về là...

Không phải quá tốt sao? Nếu anh ấy 
đang ở trong một game hẹn hò, 
những gì chúng ta làm bây giờ là...

17
00:01:21,112-->00:01:25,513
những buổi hẹn hò sao?














Nào, hãy bắt đầu cuộc hẹn của chúng ta thôi!

18
00:01:27,112-->00:01:32,113
Shi... Shidou-kun, anh đang nhìn em phải không?

19
00:01:33,812-->00:01:39,113
Tớ chắc chắn một ngày nào đó, tớ sẽ tỏa sáng.

20
00:01:41,112-->00:01:46,113
O... Onii-chan, cố lên Shidou! Em yêu anh!

21
00:01:48,112-->00:01:52,513
Nyaa~! Hôm nay cậu đến xem Kurumi à?

22
00:01:54,512-->00:01:57,513
Cô gái phép thuật, Miracle Kotori xuất hiện!

23
00:01:59,112-->00:02:02,513
Cảm nhận. Dường như cậu rất thích thú.

24
00:02:03,112-->00:02:09,513
Trong giấc mơ của chúng ta, cậu sẽ đồng ý 
cho bọn tôi làm chuyện đó với cậu.

25
00:02:11,112-->00:02:15,113
Xin anh cho em ngủ với anh...

26
00:02:18,112-->00:01:24,513
Cậu sẽ làm gì trong thế giới này?
Làm sao để thực hiện nó?

27
00:02:25,112-->00:02:27,113
Tôi thực sự muốn biết đấy.

28
00:02:27,412-->00:02:30,113
Thoát khỏi thế giới ảo

29
00:02:30,112-->00:02:32,513
và cuộc sống của họ phụ thuộc vào cậu ta!

30
00:02:33,112-->00:02:35,113
Cách duy nhất là...

31
00:02:36,112-->00:02:38,113
Hẹn hò và chiến đấu.

32
00:03:09,112-->00:03:12,513
Futashika na mondai

Một câu hỏi không xác định

33
00:03:13,112-->00:03:17,513
Niwakani shoujita ERAA

và xảy ra quá bất ngờ.

34
00:03:18,112-->00:03:21,513
Furerarenai Sore wo

Em muốn biết tại sao

35
00:03:22,112-->00:03:23,813
Shiritai no desu

em không thể cảm nhận được.

36
00:03:24,112-->00:03:25,513
Aitowa?

Tình yêu là gì thế?

37
00:03:27,112-->00:03:30,513
Yorokobi/Kitai/Omoiyari

Niềm vui / Hy vọng / Nhân ái

38
00:03:31,112-->00:03:35,113
Toikakeru ai no imi

Em sẽ hỏi ý nghĩa của tình yêu.

39
00:03:35,712-->00:03:40,513
Deai kara atsumeta kanjou

Những cảm xúc đó kết tinh từ khi chúng ta gặp nhau.

40
00:03:41,112-->00:03:43,113
Insutooru

đã được cài đặt rồi.

41
00:03:43,512-->00:03:48,113
Tojikometa itsuwari no sekai de

Trong một thế giới giả lập và bị giới hạn.

42
00:03:48,512-->00:03:52,813
Sukoshizutsu kizamareteku denshi

Những dòng điện từ từ đốt trái tim em.

43
00:03:53,112-->00:03:57,113
Anata no manazashi no saki de

Phản chiếu trong đôi mắt của anh.

44
00:03:57,512-->00:04:01,113
Waratteru ai wa honmono?

Tình yêu đang mỉm cười, phải không anh?

45
00:04:02,112-->00:04:04,613
Shin, tôi có tin xấu.

46
00:04:05,112-->00:04:07,113
Hiện đang có vấn đề trong 
hệ thống điều khiển của Fraxinus.

47
00:04:08,112-->00:04:10,113
Realizer Nền tới giới hạn rồi!

48
00:04:11,112-->00:04:12,113
Khởi động lại đi!

49
00:04:13,112-->00:04:15,513
Hả? Hệ thống đang bị chặn lại?

50
00:04:16,112-->00:04:20,113
Nếu Fraxinus rơi xuống, Tenguu sẽ bị phá hủy.

51
00:04:24,512-->00:04:30,113
Đĩa PlayStation 3 Date a Live Ars Install.

52
00:04:31,112-->00:04:35,113
được phát hành vào ngày 26 tháng 6 năm 2014.

53
00:04:37,112-->00:04:39,113
Sản xuất độc quyền.

54
00:04:41,112-->00:04:46,113
Đĩa phiên bản giới hạn (Limited Edition) 
"Choi Dere Situation, Arusu-hen" và "Shido ga Ippai?"

55
00:04:47,112-->00:04:51,113
bao gồm cả hai Drama CD Original.

56
00:04:52,312-->00:04:57,113
Cùng với các phiên bản giới hạn.












Bản đặc biệt của tác giả 
Tachibana Koushi, Arusu Quest.

57
00:07:02,512-->00:05:01,113
minh họa bởi Tsunako, 
bao gồm trong một cuốn sách đặc biệt.

58
00:05:05,112-->00:05:08,113
Có thể đặt hàng trước.













Đối với những người đặt hàng 
sẽ được tặng một câu chuyện 
về chuyến đi nhỏ của Shidou và Tohka

59
00:05:09,112-->00:05:12,113
"Choi Delle Situation Again, Tohka-hen"

60
00:05:14,112-->00:05:15,113
Tôi chỉ mong muốn

61
00:05:14,112-->00:05:19,113
một tình yêu sâu đậm.

62
00:05:19,512-->00:05:21,113
và tương lai của Itsuka Shidou

63
00:05:22,112-->00:05:24,113
và tất cả mọi người

64
00:05:24,512-->00:05:26,113
sẽ đạt được ước nguyện

65
00:05:27,112-->00:05:28,113
và biến chúng thành hiện thực.

66
00:07:26,812-->00:07:30,113
Lời dịch: NVHAI.
Cảm ơn mọi người đã đón xem.



