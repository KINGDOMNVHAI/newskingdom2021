﻿0
00:00:12,512-->00:00:14,113
Hở? Mình đang... ở đâu?

1
00:00:15,112-->00:00:20,113
Shidou! Anh tỉnh lại rồi sao?
Thật may quá!

2
00:00:21,113-->00:00:23,513
Tohka? Đây là... chúng ta trở về rồi sao?

3
00:00:25,112-->00:00:29,113
Đúng vậy. Mừng cậu trở về, Shin.

4
00:00:29,512-->00:00:32,113
Reine-san... Em cảm thấy như 
đã trôi qua nhiều thế kỷ rồi.

5
00:00:33,112-->00:00:36,113
Mặc dù ngoài thực chỉ là vài giờ thôi.

6
00:00:37,112-->00:00:39,113
Vậy sao? 
Vậy còn Fraxinus và thế giới ảo thì sao?

7
00:00:40,112-->00:00:43,113
Fraxinus đã ổn. 
Quyền điều khiển đã được khôi phục.

7
00:00:43,512-->00:00:48,113
Hiện phi thuyền đang lơ lửng trên không phận 
thành phố Tengu như bình thường.

8
00:00:48,512-->00:00:57,113
Thế giới ảo đã bị phá hủy.
Những người đã được kết nối vào 
trò chơi ở Fraxinus đã trở về an toàn.

9
00:00:58,112-->00:00:59,513
Maria và Marina...
Còn họ thì sao?

10
00:01:01,112-->00:01:05,113
Đáng tiếc là...
Họ đều đã bị phá hủy.

11
00:01:06,112-->00:01:07,513
Vậy sao...

13
00:01:08,112-->00:01:09,513
Shidou...

14
00:01:10,113-->00:01:13,113
Tôi biết điều đó.
Nhưng... đột nhiên, tôi bị suy sụp.
Cuối cùng, tôi vẫn không thể cứu Maria.

15
00:01:13,512-->00:01:22,513
Anh phải tự hào, Shidou.
Anh đã cứu sống nhiều người.
Đó là một thực tế không thể phủ nhận.

16
00:01:23,112-->00:01:24,513
Phải. Anh biết.

17
00:01:25,113-->00:01:31,513
Vì vậy, lau nước mắt và
đứng lên với niềm tự hào đi.

19
00:01:32,112-->00:01:33,513
Ừ... Cảm ơn nhé, Kotori.

20
00:01:34,112-->00:01:37,513
Không có gì. Không có gì đâu.

21
00:01:38,112-->00:01:41,113
Đúng vậy. Cậu thật tuyệt vời.

22
00:01:42,112-->00:01:43,113
Nhưng em...

23
00:01:44,112-->00:01:54,113
Shidou... 
Em... em cũng...
em cũng thấy anh thật tuyệt.

24
00:01:55,112-->00:01:57,113
Cảm ơn em, Tohka.

25
00:01:58,112-->00:02:00,112
Giờ không phải lúc lãng phí thời gian đâu.

25
00:02:00,512-->00:02:09,113
Fraxinus giờ bừa bộn lắm.
Giúp bọn em nào.

25
00:02:10,113-->00:02:11,113
Anh à? Anh làm được gì?

25
00:02:12,112-->00:02:23,513
Anh không phải làm trên máy tính đâu.
Bọn em sẽ cài đặt lại hệ thống AI.
Tất cả mọi người đều bận rộn ở đó.

25
00:02:24,512-->00:02:34,113
Có một núi công việc đơn giản khác 
như sửa chữa, mang vác đồ.

25
00:02:35,112-->00:02:44,513
Tóm lại là việc tay chân đấy.
Anh chỉ cần dùng cơ bắp, 
không phải dùng não đâu.

25
00:02:45,512-->00:02:46,513
Hay quá nhỉ!

25
00:02:47,113-->00:02:49,513
Tôi hiểu. Kotori đang cho tôi
vận động nhiều hơn thay vì suy nghĩ.

25
00:02:50,112-->00:02:56,113
Em cũng giỏi làm việc nặng lắm!
Shidou, cùng nhau làm nhé!

25
00:02:57,112-->00:02:58,513
Ừ. Chúng ta sẽ cùng làm.

25
00:02:59,112-->00:03:06,113
Cô cũng giúp à, Tohka?
Vậy cô lo cho Shidou nhé?

25
00:03:07,112-->00:03:08,513
Ừ!

25
00:03:09,112-->00:03:10,513
Vậy còn những người khác thì sao?

26
00:03:11,112-->00:03:18,113
Origami Tobiichi trở về thành phố. 
Dù sao chúng ta không thể để 
cô ấy ở lâu trên Fraxinus.

26
00:03:19,112-->00:03:26,113
Dường như cô ấy muốn ở cùng Shin.
Nhưng sau khi kiểm tra cậu không có 
bất kỳ vết thương nào, cô ấy đã đi.

26
00:03:26,512-->00:03:28,513
Đúng là vậy...
Vậy là Origami đã kiểm tra em à?

26
00:03:29,112-->00:03:38,113
Tuy nhiên, cô ấy rất muốn có một số
bộ đồ mà cậu mặc để về cầu nguyện cho cậu.
Vì vậy, chúng tôi một vài bộ làm quà lưu niệm.

26
00:03:39,112-->00:03:41,513
Sao lại có kiểu cầu nguyện
bằng quần áo đã qua sử dụng chứ?

26
00:03:42,112-->00:03:47,113
Tôi không biết nữa. 
Có lẽ mỗi người có một cách riêng chăng?

26
00:03:47,512-->00:03:50,513
Hơn nữa, Origami định làm gì
với bộ quần áo của em?
Em cảm thấy ớn lạnh đấy...

26
00:03:51,112-->00:04:04,113
Các Tinh Linh khác cũng trở về nhà.
Tuy nhiên, Kurumi đã biến mất sau khi
cô ta rời Fraxinus.

26
00:04:04,513-->00:04:06,113
Kiểu của Kurumi mà.

38
00:04:06,513-->00:04:13,113
Kotori ở lại để sửa chữa.
Và Tohka nói cô ấy muốn giúp 
các công việc nặng nhọc.

39
00:04:14,112-->00:04:15,513
Em hiểu rồi.

40
00:04:16,113-->00:04:23,513
Shidou! Làm việc nhanh nào!
Reine-san nói sau khi xong việc sẽ cho
chúng ta đồ ăn đấy. Em đói lắm rồi!

41
00:04:24,513-->00:04:26,113
Em thật sự muốn "giúp đỡ" sao?

42
00:04:27,213-->00:04:29,113
Nhanh lên nào, Shidou!

43
00:04:32,112-->00:04:33,513
Được rồi, được rồi.
Đừng kéo anh.

39
00:04:38,112-->00:04:41,113
Em sẽ làm ở đó, phải không?

40
00:04:42,113-->00:04:45,113
Phải. Cảm ơn cô đã giúp đỡ.

41
00:04:46,113-->00:04:48,513
Không có gì. Cứ để em.

42
00:04:49,513-->00:04:51,113
Dường như Tohka rất nhiệt tình.

43
00:04:51,512-->00:04:58,113
Shidou-kun, lần này cậu đã làm rất tốt.
Thay mặt cho cả phi hành đoàn Fraxinus,
xin cảm ơn cậu.

39
00:04:59,112-->00:05:01,113
Em... em không làm gì cả.
Maria... mới là người chiến đấu tất cả.

40
00:05:02,113-->00:05:03,513
Shidou-kun...

41
00:05:04,113-->00:05:06,113
Chỗ này cũng cần dọn dẹp, phải không?

42
00:05:06,513-->00:05:08,513
Phải. Cảm ơn cậu.


47
00:05:14,112-->00:05:16,113
Được rồi. Chỗ này xong rồi.

48
00:05:16,512-->00:05:17,513
Hở? Một tin nhắn à?

49
00:05:18,112-->00:05:19,513
Tôi nhận được tin nhắn trong điện thoại.
Không biết là ai nhỉ.

50
00:05:20,112-->00:05:22,113
Người gửi là...

51
00:05:23,112-->00:05:25,113
Tên người gửi là... Arusu Maria.

52
00:05:26,112-->00:05:30,513
Gần đây, có những lúc tôi cảm thấy hơi lạ. 

52
00:05:31,112-->00:05:39,113
Khi tôi đi với Shidou, 
nhịp đập của tim tôi tăng lên,
và có những lúc tôi không nói nên lời.

53
00:05:40,112-->00:05:49,513
Đây không phải những gì tôi định viết.
Tôi sẽ viết lại.

54
00:05:50,512-->00:06:00,113
Tôi không biết ai sẽ đọc tin nhắn này.
Tuy nhiên, tôi muốn làm một cái gì đó.
Suy nghĩ như một sự khởi đầu cho việc này.

55
00:06:01,112-->00:06:14,113
Khi tôi nhận ra, tôi đã tồn tại trong thế giới này,
tôi chỉ có mục đích "muốn biết tình yêu".
Thậm chí tôi không biết bản thân mình là ai.

56
00:06:15,112-->00:06:30,513
Tôi thực hiện các tình huống khác nhau để
biết tình yêu, và theo dõi phản ứng 
của Shidou và các cô gái.
Tôi khởi động lại và tiếp tục như thế.

57
00:06:31,512-->00:06:36,113
Mục đích để quan sát sự hình thành tình yêu 
giữa Shidou và các cô gái. 

57
00:06:36,512-->00:06:43,113
Tuy nhiên, tôi không thể hiểu
tình yêu là gì.

41
00:06:44,112-->00:06:48,113
Tuy nhiên, có một cảm giác khác lạ trong tôi.

41
00:06:48,512-->00:06:56,113
Điều đó khiến tôi muốn biết 
tại sao Itsuka Shidou có thể 
mang đến nhiều tình yêu như vậy.

42
00:06:57,112-->00:07:04,513
Và cuối cùng, tôi đã quyết định
tự mình đi hẹn hò với Shidou.

43
00:07:05,512-->00:07:18,113
Đó là một trải nghiệm kỳ lạ.
Hẹn hò và dành cả ngày với Shidou...
Khi đó, hành vi bất thường của tôi gia tăng.

44
00:07:19,112-->00:07:33,513
Khi tôi đi cùng Shidou, tim tôi đập mạnh.
Mặt tôi đỏ lên khi tôi nhận ra 
Shidou đang nhìn mình. 

61
00:07:34,512-->00:07:43,113
Tại thời điểm đó, ngay cả những lúc 
chúng tôi không ở bên nhau, 
chỉ cần nghĩ về Shidou, tôi lại như vậy.

62
00:07:44,112-->00:07:52,113
Nhưng điều đó không làm trái ý tôi.
Ngược lại, nó làm tôi hạnh phúc.

62
00:07:52,512-->00:07:57,113
Mặc dù ngực tôi đau nhói, 
đó là một cảm giác dễ chịu.

63
00:07:58,112-->00:08:02,513
Trong thời gian đó, tôi học được
nhiều cảm xúc khác nhau. 

63
00:08:03,112-->00:08:10,113
Niềm vui, hạnh phúc, nỗi buồn... 
Shidou và mọi người đã dạy tôi.

64
00:08:11,113-->00:08:20,113
Tất cả mọi người trở thành bạn bè...
Tôi đã nghĩ như vậy.

82
00:08:21,112-->00:08:39,113
Tôi có thể làm gì cho Shidou và mọi người?
Giúp họ trở về thế giới thực của mình. 
Nhưng tôi không biết phải làm thế nào.

83
00:08:40,112-->00:08:51,113
Nhưng câu hỏi "Tình yêu là gì?". 
Nếu tôi có thể trả lời, tôi chắc chắn...

84
00:08:52,112-->00:08:57,113
Dù nó có khiến tôi kết thúc cuộc sống này,
tôi vẫn muốn giúp đỡ tất cả mọi người.

84
00:08:57,512-->00:09:07,113
Tôi muốn nhìn thấy khuôn mặt 
hạnh phúc của tất cả...
Tôi nghĩ như vậy và quyết định.

85
00:09:08,112-->00:09:23,113
Vì vậy, ngày mai là ngày quyết định.
Tôi đã hẹn trước với Shidou.
Và cuối cùng... tất nhiên là làm chuyện ấy.

103
00:09:24,112-->00:09:27,513
Tôi tin rằng nếu tôi làm chuyện ấy, 
câu trả lời sẽ xuất hiện.

103
00:09:28,112-->00:09:40,113
Nhưng có lẽ, câu trả lời đã xuất hiện từ lâu. 
Cảm giác tôi dành cho Shidou. Đây là...

104
00:09:41,112-->00:09:45,513
Bây giờ, tôi sẽ ghi lại.

105
00:09:46,512-->00:09:49,113
Tâm sự của Maria sao?

106
00:09:49,512-->00:09:52,112
Một giọt nước mắt chảy dài trên má tôi. 
Nó là giọt nước mắt của lòng biết ơn với Maria, 
người đã làm việc rất chăm chỉ, 
hay do bất lực vì không cứu cô ấy? 

107
00:09:53,112-->00:09:55,113
Ghi lại.

108
00:09:56,112-->00:10:03,113
Tôi nghĩ tôi sẽ sớm biến mất.
Vì vậy, tôi sẽ ghi lại những điều này.

109
00:10:04,112-->00:10:16,113
Yatogami Tohka.
Tính cách vui vẻ của Tohka 
khiến tôi cũng vui theo. Cô ấy rất tham ăn,
nhưng tôi nghĩ điều đó khiến cô ấy dễ thương.

110
00:10:17,112-->00:10:28,513
Tobiichi Origami.
Bình thường rất lạnh lùng, 
nhưng khi nói đến tình yêu thì rất hứng thú. 
Tôi rất ngưỡng mộ và ủng hộ cô ấy.

111
00:10:29,512-->00:10:41,513
Yoshino.
Yoshino và Yoshinon, cả hai đã dạy tôi nhiều điều. 
Thật tuyệt khi họ luôn vui vẻ cùng nhau.

108
00:10:42,512-->00:10:52,113
Tokisaki Kurumi.
Cô ấy là một cô gái bí ấn, rất khó nắm bắt.

1
00:10:53,112-->00:11:07,113
Itsuka Kotori.
Một người lớn? hay một cô bé? 
Đó là một cô bé có chút khó hiểu.
Nhưng đó là người đã dạy tôi mọi thứ.

1
00:11:08,112-->00:11:21,113
Yamai Kaguya. 
Cô ấy có cách nói chuyện đặc biệt.
Cô ấy rất đáng yêu giống như vẻ ngoài.

368
00:11:22,112-->00:11:34,113
Yamai Yuzuru.
Cô ấy cũng có một cách nói đặc biệt.
Mặc dù lạnh lùng, cô là người bình tĩnh 
và thận trọng mà ta luôn có thể dựa vào.

368
00:11:35,112-->00:11:42,513
Izayoi Miku.
Đúng như dự đoán từ một thần tượng, 
cô ấy là một người hấp dẫn. 

368
00:11:43,112-->00:11:47,513
Luôn giữ tốc độ của mình với vẻ uy nghi, 
đến khi có một kết thúc ấn tượng.

368
00:11:48,512-->00:11:53,112
Chung sống với tất cả bọn họ, 
tôi đã học được nhiều điều.

368
00:11:53,512-->00:12:01,112
Tình yêu như thế nào, 
nhân cách của tôi... Tôi yêu tất cả.

68
00:12:02,112-->00:12:12,513
Thế giới này đã kết thúc.
Tôi đã luôn luôn muốn tha thứ cho bản thân 
nhưng tôi không thể làm được.

368
00:12:13,112-->00:12:22,113
Xin lỗi. Khoảng thời gian chúng ta 
bên nhau thật sự rất vui.

368
00:12:23,112-->00:12:27,513
Vì vậy... Cảm ơn cậu.

368
00:12:28,512-->00:12:40,513
Cuối cùng, Shidou. Những gì tớ chia sẻ với cậu,
dù cậu không nhớ, nhưng tất cả đều là kho báu của tớ.

368
00:12:41,512-->00:12:47,113
Nhờ cậu, tớ có thể tìm ra câu trả lời.

368
00:12:48,112-->00:12:55,113
Thật không may, có lẽ cậu không thể trả lời 
tin nhắn này. Thật vô vọng, phải không?

368
00:12:55,512-->00:13:06,513
Nhưng dù khi cậu không trả lời, 
đây là cảm xúc thật của tớ.

368
00:13:07,512-->00:13:14,513
Lời cuối cùng.
Cảm ơn cậu, Shidou.

368
00:13:15,112-->00:13:18,513
Arusu Maria.

368
00:13:23,112-->00:13:25,113
Những tâm sự... 
cuối cùng... của cô ấy...

368
00:13:25,512-->00:13:27,113
Thật... ngốc...

368
00:13:28,512-->00:13:38,113
Shin. Sao vậy? 
Cậu đọc xong thư chia tay rồi à?
Xin lỗi vì đã gián đoạn cảm xúc của cậu
khi cậu đang khóc thế này.

368
00:13:38,513-->00:13:40,513
Hở? Chị biết em đọc tin nhắn sao?

368
00:13:41,112-->00:13:45,113
Không, có vẻ như đã có một đường truyền
từ một nơi không bình thường.

368
00:13:46,112-->00:13:48,513
Không bình thường? Trong tin nhắn có viết là
"Cậu không thể trả lời"... nghĩa là sao?

368
00:13:49,112-->00:13:56,513
Phải, đó là của Marina gửi 
bí mật trong hệ thống của Fraxinus.

368
00:13:57,112-->00:13:59,113
Marina gửi sao?
Cô ấy... đã gửi tin nhắn đó...

368
00:14:00,112-->00:14:10,113
Thôi nào, lau nước mắt đi, Shin.
Marina còn cài một ứng dụng trên điện thoại cậu.
Nếu cậu cứ khóc, cậu sẽ làm hỏng mọi thứ đấy.

368
00:14:11,112-->00:14:12,513
Hở? Vậy nghĩa là sao?

368
00:14:13,112-->00:14:17,113
Vẫn còn quá sớm để nói lời chia tay.

368
00:14:17,512-->00:14:19,113
Một ứng dụng đã tự động cài đặt 
trên điện thoại của em sao?

368
00:14:19,512-->00:14:22,113
Phải. Cậu xem nó đi.


368
00:14:28,512-->00:14:29,513
Đây là gì? Hở?

368
00:14:30,512-->00:14:34,113
Shidou, chào buổi sáng.

368
00:14:35,112-->00:14:36,513
Ma... Maria?
Có thật là Maria không?

368
00:14:37,113-->00:14:40,113
Vâng. Maria của cậu đây.

368
00:14:40,512-->00:14:42,513
Tớ chưa bao giờ nói cậu là của tớ...
Nhưng cậu đã biến mất rồi mà?

368
00:14:43,112-->00:14:53,113
Có lẽ trước khi biến mất, 
Marina đã chuyển tính cách và ký ức của tớ 
vào tin nhắn tớ đã để lại.

368
00:14:53,512-->00:14:56,113
Ra vậy. 
Không ngờ tin nhắn đó lại giúp tớ gặp lại cậu.
Tớ không biết phải cảm ơn thế nào cho đủ.

368
00:14:57,112-->00:15:05,113
Shidou, cậu sao vậy?
Có phải là lỗi của tớ không?

368
00:15:06,112-->00:15:08,113
Không, không phải. Không sao hết.
Tớ rất mừng khi thấy cậu một lần nữa, Maria.

368
00:15:08,513-->00:15:15,513
Vâng. 
Tớ cũng rất mừng 
khi nhìn thấy cậu lần nữa, Shidou.

368
00:15:16,512-->00:15:18,513
Nhưng thế này nghĩa là cậu đã được 
cài đặt trên điện thoại của tớ sao?

368
00:15:19,112-->00:15:30,113
Tớ vẫn chưa thể về Fraxinus. 
Ít nhất là cho đến khi 
các chức năng phục hồi hoàn toàn.

368
00:15:30,512-->00:15:31,513
Ra là vậy.

368
00:15:32,112-->00:15:42,112
Cuối cùng tớ cũng sẽ trở về Fraxinus, 
nhưng cho đến lúc đó, 
tớ sẽ chiếm một phần dung lượng
trong điện thoại của cậu.

368
00:15:42,512-->00:15:47,113
Để bù đắp, tớ sẽ hỗ trợ cậu
trong cuộc sống hàng ngày.

368
00:15:48,112-->00:15:50,112
Cậu sẽ giúp tớ trong 
cuộc sống hàng ngày sao?

368
00:15:50,512-->00:15:58,113
Vâng. Ví dụ như lọc ra các 
thông tin quan trọng, chỉ đường bằng GPS.

368
00:15:58,512-->00:16:00,113
Ồ, có vẻ có ích đấy.

368
00:16:00,512-->00:16:12,113
Nếu có nhu cầu, tớ cũng có thể gửi những bài thơ
đáng xấu hổ của Kotori, soạn tin nhắn 
vào lúc nửa đêm và tự chụp hình nữa.

368
00:16:13,112-->00:16:15,113
Không cần đâu! Làm ơn đừng!

368
00:16:15,512-->00:16:20,113
Đùa thôi... Mặc dù cũng đúng một nửa đấy.

368
00:16:20,512-->00:16:23,513
Tớ không cần biết cái một nửa đó đâu!
Đó là chuyện con gái mà.

368
00:16:24,112-->00:16:26,513
Nói vậy cũng đúng.

368
00:16:27,512-->00:16:29,112
Cậu công nhận à?

368
00:16:29,512-->00:16:32,513
Vâng. Tớ là con gái mà.

368
00:16:33,113-->00:16:34,513
Không còn cách nào khác nhỉ...

368
00:16:35,112-->00:16:46,113
Nhưng mà Shidou, xin hãy nhớ điều này.
Từ bây giờ, tớ sẽ tiếp tục 
giúp đỡ Fraxinus.

368
00:16:46,512-->00:16:55,113
Giống như khi cậu giúp tớ,
Tớ nghĩ giờ đã đến lúc tớ giúp cậu
ở thế giới này rồi.

368
00:16:55,512-->00:17:10,113
Tớ hứa tớ sẽ giúp cậu với tất cả mọi thứ tớ có. 
Do đó, đừng bao giờ bỏ cuộc, Shidou.
Nhất định cậu phải giúp mọi người.

368
00:17:11,112-->00:17:13,513
Được rồi. Tớ sẽ phải nỗ lực.
Tớ sẽ giúp đỡ họ, sẽ không có thất bại.

368
00:17:14,112-->00:17:19,113
Vậy mới là Shidou mà tớ yêu.

368
00:17:19,512-->00:17:21,113
À... vậy sao?

368
00:17:21,112-->00:17:26,113
Còn một chuyện quan trọng nữa.

368
00:17:27,112-->00:17:28,513
Hở? Gì vậy?

368
00:17:31,112-->00:17:36,113
Shidou... Từ giờ mong cậu giúp đỡ nhé.

368
00:17:44,512-->00:17:47,113


368
00:17:47,512-->00:17:50,113


