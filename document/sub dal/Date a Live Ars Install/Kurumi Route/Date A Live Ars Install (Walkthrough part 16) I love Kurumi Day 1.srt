﻿0
00:00:02,512-->00:00:04,113
Đi đến Công viên.
1) Đồng ý.
2) Không đồng ý.

1
00:00:09,112-->00:00:11,113
Tôi không biết tại sao tôi cảm thấy thế này.
Nhưng tôi không muốn về nhà.

2
00:00:12,113-->00:00:16,113
Tôi không biết nữa. 
Giống như trí nhớ của tôi rải rác đâu đây.

3
00:00:17,112-->00:00:20,113
Chắc tôi nên đi dạo vòng quanh thành phố.

4
00:00:25,112-->00:00:30,113
Dù đi dạo, tôi vẫn cảm thấy thiếu thiếu.
Tôi chẳng biết gì cả. Tôi cứ đi thôi.

5
00:00:34,112-->00:00:36,113
Sau khi đi nhiều giờ, tôi vẫn không muốn về nhà.

5
00:00:36,512-->00:00:39,113
Tôi đi nhanh hơn như đang 
tìm kiếm điều gì đó.

6
00:00:43,112-->00:00:46,113
Xem nào... Maid Cafe à? Vào thử xem.

7
00:00:51,112-->00:00:55,113
Mừng ngài đã về, chủ nhân. (Goshujin-sama)!

8
00:00:56,112-->00:00:57,513
Chà! Đông vui quá nhỉ!

9
00:00:58,512-->00:01:02,113
Một nơi đầy những cô gái moe. 
Nơi mà tôi muốn trở lại.
Nhưng tôi không biết tại sao nữa.

10
00:01:03,112-->00:01:07,113
A, Shidou-san!

11
00:01:08,112-->00:01:10,113
Hở? Giọng nói này...

13
00:01:14,112-->00:01:20,113
Nyaa~! Hôm nay cậu đến xem Kurumin à?

14
00:01:21,112-->00:01:23,113
Um... Cũng không hẳn thế...

15
00:01:24,112-->00:01:33,113
Tớ vui lắm. 
Kurumin ở đây nhớ Shidou-san nhiều lắm.

16
00:01:34,112-->00:01:37,113
Tớ... tớ cũng nhớ cậu lắm, Kurumin.

17
00:01:38,112-->00:01:42,113
Kurumin... Giờ tôi nhớ rồi.
Cô ấy là nhân viên nổi tiếng nhất của
Maid Cafe Nightmare.
Đây là thiên thần của lòng tôi.

18
00:01:43,112-->00:01:49,113
Vậy thì Shidou-san,
tớ sẽ đưa cậu đến chỗ ngồi quen thuộc nhé.

19
00:01:50,112-->00:01:52,513
Cảm ơn. Và vẫn những món như thường lệ nhé.

20
00:01:53,512-->00:01:58,113
Vâng. Cảm ơn cậu rất nhiều.

21
00:02:01,112-->00:02:06,113
Kurumin thật dễ thương. Cô ấy vẫn nhớ tên tôi,
chỗ tôi thường ngồi mỗi cuối tuần.
Tuy nhiên, hôm nay đúng là một trận chiến.

22
00:02:07,112-->00:02:12,113
Tôi yêu Kurumin hơn bất kỳ ai trong quán.
Tôi có thể chứng minh điều đó.
Đó là nhiệm vụ của tôi. 

25
00:02:15,112-->00:02:23,113
Shidou-san, nước và món cậu thích đây.

25
00:02:24,112-->00:02:28,113
Parfait Tình Yêu (Love Love Dokin Parfait)

25
00:02:29,112-->00:02:35,113
Đây rồi. Parfait Tình Yêu 1500 yen (316.000 VNĐ).
Menu này sẽ khiến maid phải đút cho khách ăn.
Đây là một trong những bí mật đặc biệt của tôi.

25
00:02:36,112-->00:02:42,113
Nào, há miệng ra nào Shidou-san. Aaa...

25
00:02:43,112-->00:02:45,113
Aaa...

25
00:02:46,112-->00:02:51,113
Cậu thấy sao, Shidou-san?

25
00:02:52,112-->00:02:55,113
Ngon lắm, Kurumin.
Trái tim tớ đang cảm nhận được tình yêu.

25
00:02:56,112-->00:03:04,513
Kurumin vui lắm.
Kurumin đã cố gắng làm thật tốt parfait này
trong lúc nghĩ về cậu đấy, Shidou-san.

25
00:03:05,513-->00:03:09,113
Kem trắng được nhẹ nhàng lấy ra từ tay cậu.
Nó thật tuyệt vời. Không từ nào có thể diễn tả được.

25
00:03:10,112-->00:03:18,113
Kurumin muốn Shidou-san ăn nhiều nữa.
Aaa... nào. Ăn nhanh nào, ăn nhanh nào.

26
00:03:19,112-->00:03:20,513
Aaaa....

26
00:03:21,512-->00:03:26,113
Kurumin-chan, có người gọi kìa.

26
00:03:27,112-->00:03:29,113
Hở?

26
00:03:30,112-->00:03:39,513
Shidou-san, Kurumin phải đi phục vụ
chủ nhân khác rồi. Đợi tớ một chút nhé.

26
00:03:40,512-->00:03:42,113
Cái gì?

26
00:03:45,112-->00:03:47,513
Hừm... Dù sao cô ấy cũng là
nhân viên nổi tiếng nhất ở đây mà.

26
00:03:48,512-->00:03:51,513
Dù vậy, ai dám cướp mất 
khoảnh khắc ngọt ngào của mình với Kurumin chứ?

26
00:03:52,512-->00:03:59,113
Kanazuki-san, anh lại đến tìm em à?

26
00:04:00,112-->00:04:08,113
Anh đã chờ em lâu lắm rồi, Kurumin-chan!
Giờ hãy dùng đôi chân trắng trẻo đó
lấy Parfait đổ lên giày cho anh liếm đi!

26
00:04:09,112-->00:04:15,513
Vâng. Một Immoral Slave Parfait nhé.

26
00:04:16,512-->00:04:19,113
Cái gì? Có cả món đẳng cấp cao đến thế ư?

26
00:04:20,112-->00:04:23,113
Phải xem lại xem có món nào 
giống Immoral Slave Parfait không.

27
00:04:24,113-->00:04:28,113
Chết tiệt. Giờ mình phải làm gì đây?
Nghĩ đi, Itsuka Shidou!

28
00:04:29,112-->00:04:36,113
1) Yêu cầu Parfait Nhảy Moe Moe.
2) Ngừng nghĩ về tên khách hàng đó.
3) Sử dụng kỹ thuật tối thượng của tôi.

29
00:04:36,513-->00:04:39,513
Đây rồi! Yêu cầu mới!
Tôi phải yêu cầu món này để gọi Kurumin về đây.

30
00:04:40,513-->00:04:43,513
Xin... xin lỗi. Tôi muốn gọi món
Parfait Nhảy Moe Moe.
(Moe Moe Dance Parfait)

31
00:04:44,112-->00:04:48,113
Vâng. Anh vẫn yêu cầu Kurumin-chan phải không?

32
00:04:49,512-->00:04:51,513
Chính xác.

33
00:04:52,513-->00:04:55,113
Xin đợi một chút ạ.

34
00:04:56,112-->00:04:59,113
Ahihihi. Mình làm được. Mình làm được rồi.

35
00:05:00,112-->00:05:04,113
Parfait Nhảy Moe Moe là yêu cầu
maid phải nhảy múa cho khách xem.

36
00:05:05,113-->00:05:07,513
A bit tough on the wallet... but that's why
it's reserved only for the top level customers.

37
00:05:08,513-->00:05:12,113
Vâng, Shidou-san!

38
00:05:15,112-->00:05:21,113
Xin lỗi vì đã để cậu đợi.
Parfait Nhảy Moe Moe của cậu đây.

39
00:05:22,112-->00:05:24,113
Ồ, đây rồi.

40
00:05:25,113-->00:05:32,513
Thật xấu hổ nhưng 
Kurumin sẽ nhảy cho cậu xem nhé.

41
00:05:33,513-->00:05:35,513
Ừ, tớ đang chờ đây.

42
00:05:36,113-->00:05:39,513
Âm nhạc, bắt đầu!

43
00:05:40,513-->00:05:51,513
Đêm nay, một cơn ác mộng đáng sợ sẽ đến.
Ác mộng sẽ mang cái chết đến với chủ nhân.

39
00:05:52,512-->00:05:56,113
Cô ấy đang nhảy...
Kurumin đang nhảy... Cô ấy thật...

40
00:05:57,113-->00:05:59,113
Moe...

41
00:06:00,113-->00:06:10,513
Và hôm sau, ác mộng đã đến.
Chủ nhân đã chết trong cơn mơ.

42
00:06:11,513-->00:06:14,113
Đây rồi. Đến đoạn cao trào,
maid sẽ nhảy lên và...

43
00:06:15,112-->00:06:22,513
Nhảy lên nào. 
Dokyun Hakyun Kuruminpa. 
Dokyun Hakyun...

39
00:06:23,512-->00:06:25,113
Kurumin...

40
00:06:28,513-->00:06:31,113
Mình thấy nó...

41
00:06:32,113-->00:06:35,113
Nữa nào, Kuruminpa!

42
00:06:36,113-->00:06:38,113
Kuruminpa!

43
00:06:38,513-->00:06:41,113
Kuruminpa!

44
00:06:42,513-->00:06:45,113
Kuruminpa! Kuruminpa!

45
00:06:47,112-->00:06:54,513
Cảm ơn vì đã đón xem.
Parfait Nhảy Moe Moe của cậu đến đây là hết.

46
00:06:53,512-->00:06:56,513
Ồồồồồồồồồ! Vỗ tay!

47
00:06:57,112-->00:06:59,113
Thần linh ơi! Cảm ơn người!

48
00:07:00,112-->00:07:05,513
Nyaa, Shidou-san. 
Kurumin xấu hổ lắm đấy. Cậu biết không?

49
00:07:06,512-->00:07:09,513
Thật... thật tuyệt vời, Kurumin...

50
00:07:10,112-->00:07:13,513
Kurumin... cô ấy cố gắng đến thế vì tôi...
Thật là một cô gái dễ thương...

51
00:07:14,112-->00:07:18,113
Nhờ vậy, tôi đã được nhìn thấy "vùng cấm".
Tôi không còn gì hối tiếc nữa...

52
00:07:24,112-->00:07:29,113
Tổng cộng là 7000 yên (1.544.000 VNĐ)

53
00:07:30,113-->00:07:33,113
Hẹn gặp lại, thưa chủ nhân.

54
00:07:44,112-->00:07:47,113
Chuyện gì vừa xảy ra vậy?

55
00:07:47,512-->00:07:49,113
Mình cảm giác như vừa 
lạc mất bản thân ở lại đó.

56
00:07:50,112-->00:07:52,113
Và cũng...

57
00:07:53,112-->00:07:56,113
Mình đã làm gì vậy?
Thật đấy. Mình đã làm gì?

58
00:07:57,112-->00:07:59,113
Dù vậy... Kurumin rất đáng yêu.

59
00:08:00,112-->00:08:02,112
Bằng cách nào đó... 
Mình cảm thấy cần sự giúp đỡ...





82
00:08:11,112-->00:08:14,113
Hở? Đây là lớp học à?
Mình trở lại bình thường rồi.

83
00:08:15,112-->00:08:17,113
Chuyện quái gì vừa xảy ra vậy?

84
00:08:18,112-->00:08:28,113
Tôi đã xem phản ứng của Itsuka Shidou
trong một mối quan hệ 
và tình huống khác bình thường.

109
00:08:29,113-->00:08:32,113
Cậu nhìn thấy hết à?
Nhưng tại sao... nó...

113
00:08:33,112-->00:08:38,113
Nó giống như một ảo giác. 
Nghĩ lại, tôi thấy thật xấu hổ. 
Hơn nữa, chúng tôi còn bị Arusu nhìn thấy...

85
00:08:39,512-->00:08:44,113
Ara? Tớ vừa làm gì...?

86
00:08:45,112-->00:08:47,113
Ku... Kurumi!
Không thể nào.　Kurumi!

87
00:08:48,112-->00:08:54,113
Shi... Shidou-san! Vừa rồi là...

88
00:08:55,112-->00:08:58,113
Tại sao cậu lại ở đây?
Không, đừng nói cậu cũng...

89
00:08:59,112-->00:09:06,113
Đừng xấu hổ như thế.
không có gì không thể trải qua kinh nghiệm được.

98
00:09:07,112-->00:09:10,113
Dù cậu nói như vậy,
trông cậu không có gì xấu hổ cả.

99
00:09:11,312-->00:09:19,113
Thực tế là tớ đã thử vài lần rồi.
Cậu thấy bộ đồ maid của tớ thế nào?

100
00:09:20,112-->00:09:24,513
Tất nhiên là... dễ thương... Không, không phải!
Giờ tớ không muốn nhớ lại đâu.

101
00:09:25,112-->00:09:33,113
Ara, tiếc thật.
Cậu đã cố gắng nhìn váy tớ mà.

101
00:09:34,112-->00:09:36,513
Thôi quên chuyện đó đi!

102
00:09:40,512-->00:09:43,113
Vậy chuyện gì đang xảy ra ở đây vậy?

103
00:09:44,112-->00:09:48,513
Đó là một phản ứng thú vị đấy, Itsuka Shidou.

104
00:09:49,512-->00:09:52,113
Vậy tình huống đó là do cậu làm à Arusu?

105
00:09:53,112-->00:10:00,513
Phải. Mô phỏng việc tạo ra tình yêu 
trong một tình huống đặc biệt.

368
00:10:01,512-->00:10:05,113
Tạo ra tình yêu sao? 
Chuyện này sẽ còn xảy ra nữa à?

368
00:10:06,112-->00:10:14,113
Tôi muốn quan sát sự hình thành tình yêu với
Itsuka Shidou trong nhiều tình huống.

368
00:10:15,112-->00:10:24,513
May mắn là thế giới này có 
dữ liệu khá phong phú 
về các tình huống tạo ra tình yêu

368
00:10:25,513-->00:10:29,113
Cậu nói tớ mới nhớ. Trò chơi này 
có nhiều sự kiện để người chơi lựa chọn.

368
00:10:30,112-->00:10:38,113
Tôi muốn một tình huống đặc biệt
để có thể nhận được nhiều thông tin hơn.

368
00:10:39,112-->00:10:42,113
Vậy à? Cũng đúng. Dù sao hôm nay cũng
không khác các hoạt động thường ngày lắm.

368
00:10:43,112-->00:10:45,513
Mình có thể thấy tương lai đầy khó khăn đang chờ mình...

114
00:10:04,112-->00:10:06,113


1
00:10:14,112-->00:10:15,113


1
00:10:16,912-->00:10:18,113


1
00:10:19,112-->00:10:20,113


1
00:10:21,112-->00:10:23,113


1
00:10:26,112-->00:10:28,113


1
00:10:28,612-->00:10:30,113


1
00:10:31,112-->00:10:32,113


368
00:10:33,112-->00:10:34,513


368
00:10:35,112-->00:10:36,113


368
00:10:37,112-->00:10:38,313


368
00:10:38,512-->00:10:42,113


368
00:10:43,112-->00:10:45,113


368
00:10:46,512-->00:10:49,113


368
00:10:49,312-->00:10:50,513


368
00:10:51,212-->00:10:52,113

368
00:10:52,312-->00:10:54,113

368
00:10:56,112-->00:10:57,113


368
00:11:01,112-->00:11:09,113


68
00:11:10,112-->00:11:12,113


368
00:11:13,112-->00:11:14,113


368
00:11:14,512-->00:11:19,513


368
00:11:20,112-->00:11:29,113


368
00:11:01,112-->00:11:09,113


68
00:11:10,112-->00:11:12,113


368
00:11:13,112-->00:11:14,113


368
00:11:14,512-->00:11:19,513


368
00:11:20,112-->00:11:29,113



368
00:11:48,112-->00:11:50,513


368
00:11:52,112-->00:11:53,113


368
00:11:53,812-->00:11:56,113


368
00:11:57,112-->00:11:58,113


368
00:11:48,112-->00:11:50,513


368
00:11:52,112-->00:11:53,113


368
00:11:53,812-->00:11:56,113


368
00:11:57,112-->00:11:58,113


368
00:11:58,912-->00:12:01,113


368
00:12:02,113-->00:12:03,113


368
00:12:04,113-->00:12:05,513


368
00:12:10,112-->00:12:11,113


368
00:12:18,112-->00:12:19,513


368
00:12:22,112-->00:12:22,913


368
00:12:23,112-->00:12:25,113


368
00:12:23,112-->00:12:25,113


368
00:12:25,312-->00:12:26,313


368
00:12:27,312-->00:12:32,113


368
00:12:33,112-->00:12:35,113


368
00:12:48,112-->00:12:49,513


368
00:12:23,112-->00:12:25,113


368
00:12:25,312-->00:12:26,313


368
00:12:27,312-->00:12:32,113


368
00:12:33,112-->00:12:35,113


368
00:12:48,112-->00:12:49,513


368
00:12:25,312-->00:12:26,313


368
00:12:27,312-->00:12:32,113


368
00:12:33,112-->00:12:35,113


368
00:13:48,112-->00:13:49,513


368
00:13:48,112-->00:13:49,513


368
00:13:15,512-->00:13:19,113


368
00:13:29,112-->00:13:30,113


368
00:13:48,112-->00:13:49,513


368
00:13:15,512-->00:13:19,113


368
00:13:29,112-->00:13:30,113


368
00:13:48,112-->00:13:49,513


368
00:13:15,512-->00:13:19,113


368
00:13:29,112-->00:13:30,113


368
00:13:48,112-->00:13:49,513


368
00:13:15,512-->00:13:19,113


368
00:13:29,112-->00:13:30,113


368
00:13:15,512-->00:13:19,113


368
00:13:29,112-->00:13:30,113




368
00:14:19,212-->00:14:21,113


368
00:14:22,112-->00:14:23,113


368
00:14:38,112-->00:14:41,113


368
00:14:56,512-->00:14:58,113


368
00:15:46,112-->00:15:50,113


368
00:15:43,112-->00:15:45,113


368
00:15:46,512-->00:15:49,113


368
00:15:50,112-->00:15:51,513


368
00:16:28,112-->00:16:30,113


368
00:16:30,812-->00:16:32,113


368
00:16:32,312-->00:16:34,113


368
00:16:34,112-->00:16:36,113


368
00:16:36,312-->00:16:37,113


368
00:16:39,112-->00:16:40,113


368
00:16:41,112-->00:16:42,113


368
00:16:43,112-->00:16:43,913


368
00:16:44,112-->00:16:45,113


368
00:16:46,112-->00:16:50,113


368
00:16:51,312-->00:16:56,113


368
00:16:57,312-->00:16:58,113


368
00:17:08,112-->00:17:09,113


368
00:17:10,112-->00:17:11,613




368
00:17:13,512-->00:17:22,113
Vậy trong ngày hôm nay, các cô đã
làm gì trong thế giới này?

368
00:17:25,512-->00:17:28,113
Tohka giơ tay với vẻ mặt đầy sức sống.

368
00:17:29,112-->00:17:32,113
Được rồi, Tohka. Nói đi.

368
00:17:33,112-->00:17:41,113
Thế giới này giống y như ngoài thật!
Cả bento lẫn bữa tối của Shidou đều rất ngon!

368
00:17:42,112-->00:17:45,113
Vậy à...

368
00:17:46,512-->00:17:49,113
Giờ mới nhớ, anh và Kurumi cũng...

368
00:17:50,112-->00:17:56,113
Shidou-san, cậu định kể chuyện hồi chiều sao?

368
00:17:57,112-->00:17:59,113
Không, đó là...

368
00:18:00,112-->00:18:08,313
Dù có chuyện gì cũng phải nói ra.
Đó có thể là manh mối để ra khỏi
thế giới này đấy.

368
00:18:09,112-->00:18:11,513
Đó là...
Anh đã thấy anh làm gì đó với Kurumi.

368
00:18:12,512-->00:18:17,113
Làm gì cơ? Kết quả là sao?

368
00:18:18,112-->00:18:20,513
Anh đã...

368
00:18:21,512-->00:18:26,513
Nó giống như một giấc mơ, nhưng chắc chắn
do Arusu gây ra. Cô ấy nói là để xem tình yêu 
trong một tình huống đặc biệt.

368
00:18:27,512-->00:18:41,113
Ra là vậy. Nó giống như một sự kiện.
Quan sát tình yêu trong một tình huống đặc biệt?
Đúng, giống như một cuộc hẹn đặc biệt vậy.

368
00:18:42,112-->00:18:53,113
Rõ ràng Arusu có khả năng 
kiểm soát thế giới này.

368
00:18:55,512-->00:19:04,113
Tôi không chắc trong thế giới này 
tôi có thể dùng Gabriel được không 
nhưng nếu tôi thử thì sao?

368
00:19:05,112-->00:19:15,113
Không, tốt hơn là không nên làm điều đó. 
Đối thủ là Tinh Linh Nhân Tạo đấy.

368
00:19:16,112-->00:19:28,113
Hơn nữa, chúng ta vẫn đang kẹt trong
thế giới này. Nếu chúng ta làm gì bất cẩn,
không biết điều gì sẽ xảy ra đâu.

368
00:19:29,112-->00:19:37,513
Cô nói đúng. Tôi thấy đó là 
một quyết định khôn ngoan đấy, Tinh Linh Lửa.

368
00:19:38,513-->00:19:43,513
Tôi không thích bị gọi thế đâu đấy.

368
00:19:44,512-->00:19:57,513
Giờ không còn thời gian để nói chuyện nữa.
Từ ngày mai, chúng ta sẽ tiếp tục điều tra 
và quan sát cả thế giới này và Arusu.

368
00:19:58,512-->00:20:01,113
Đúng vậy. Hiện giờ chúng ta không có 
sự lựa chọn nào khác.

368
00:20:02,113-->00:20:07,513
Được rồi, chúng ta kết thúc tại đây.
Mọi người, đừng mất cảnh giác đấy.



368
00:20:13,513-->00:20:18,113
Hôm nay tôi đã có một trải nghiệm kỳ lạ. 
Kinh nghiệm thực tế đó cũng khiến tôi kiệt sức.
Vì vậy, tôi cần phải nghỉ ngơi, nhưng...

368
00:20:18,512-->00:20:19,513
Haaa...

368
00:20:21,113-->00:20:24,113
Anh sao vậy Shidou?

368
00:20:25,112-->00:20:27,113
À, anh đang tự hỏi là bây giờ anh phải làm gì...

368
00:20:28,112-->00:20:36,113
Vâng. Giờ chúng ta không biết 
nên làm gì trong thế giới này.

368
00:20:37,512-->00:20:43,113
Ara, mọi người vẫn tụ tập ở đây à.

368
00:20:44,113-->00:20:47,113
Ừ. Nói sao đây nhỉ. 
Tớ tưởng cuộc họp đã kết thúc rồi...

368
00:20:48,112-->00:20:51,113
Và tôi nghĩ tôi sẽ được nghỉ ngơi...
Nhưng Arusu...

368
00:20:57,112-->00:20:59,113
Arusu, cậu đứng đó nãy giờ à?

368
00:21:00,112-->00:21:06,513
Vâng. Tôi đứng xem từ nãy đến giờ.

368
00:21:07,512-->00:21:11,113
Arusu, cậu thực sự làm việc đó sao?

368
00:21:12,112-->00:21:18,513
Vâng. Tôi đã thực hiện một mô phỏng 
một tình huống đặc biệt 
mà trong thực tế không hề xảy ra. 

368
00:21:19,112-->00:21:27,513
Tôi đã thu thập một số dữ liệu thú vị.
Tuy nhiên... các thông tin liên quan 
đến tình yêu vẫn chưa đủ.

368
00:21:28,512-->00:21:31,113
Trông cậu có vẻ hài lòng lắm mà.

368
00:21:32,112-->00:21:40,113
Tôi chỉ vui vì được nhìn thấy cách các cậu 
phản ứng trong tình huống đặc biệt thôi.

368
00:21:41,112-->00:21:43,513
Tớ đoán nó sẽ còn tiếp diễn phải không?

368
00:21:44,512-->00:21:50,513
Vâng. Trong tình hình hiện giờ, 
đây là lúc thích hợp để thu thập thông tin.

368
00:21:51,512-->00:21:53,513
Thật vậy à?

368
00:21:54,512-->00:21:57,513
Tôi thắc mắc tình huống như thế nào
thì có thể dễ dàng phát triển tình yêu.

368
00:21:58,512-->00:22:01,113
Hiện giờ mình không có ý tưởng gì cả.
Mình phải hỏi ý kiến ai đó.

368
00:22:01,512-->00:22:05,113
Nhưng không thể hỏi tất cả mọi người 
trong căn phòng nhỏ này được. 
Mình sẽ đi gặp ai đó để hỏi.

368
00:22:06,112-->00:22:26,113
1) Nói chuyện với Tohka
2) Nói chuyện với Origami
3) Nói chuyện với Yoshino
4) Tiếp theo

368
00:22:27,512-->00:22:31,113
Này Yoshino, em nghĩ tình yêu là gì?

368
00:22:32,112-->00:22:42,513
Là những thứ dễ... dễ thương, 
những thứ mà em... muốn bảo vệ...

368
00:22:43,512-->00:22:47,113
Hmm... Nghe em nói giống như
tình cảm dành cho vật nuôi phải không?
Anh có nghe Arusu nói chuyện này rồi.

368
00:22:48,112-->00:22:53,513
Em... anh nói vậy... là sao? 

368
00:22:54,512-->00:22:57,113
Sao em lại bối rối như vậy?

368
00:22:58,112-->00:23:08,513
Chán anh quá, Shidou-kun.
Vì anh hỏi nên Yoshino chỉ đang
cố gắng trả lời bằng tình cảm của mình thôi.

368
00:23:09,112-->00:23:15,513
Tóm lại, Yoshino đang muốn thể hiện tình cảm
với anh bằng cách trả lời câu hỏi này,

368
00:23:16,112-->00:23:23,113
Cậu ấy hy vọng anh sẽ hiểu 
cậu ấy muốn được bảo vệ và che chở thôi.

368
00:23:24,112-->00:23:26,513
Vậy... có đúng vậy không?

368
00:23:27,512-->00:23:32,513
Em... em... những chuyện thế này...

368
00:23:34,512-->00:23:36,113
Này Yoshino...

368
00:23:37,112-->00:23:40,113
Hừ, khó quá nhỉ...


368
00:23:43,512-->00:23:47,113
Sau đó, tất cả mọi người ngồi trong phòng khách.
Còn tôi đang rửa bát trong bếp.

68
00:23:48,112-->00:23:54,113
Ara, cậu đang rửa bát à?
Tớ có thể giúp không?

368
00:23:55,112-->00:23:57,113
Cảm ơn Kurumi...
Tớ cũng sắp xong rồi...

368
00:23:58,112-->00:24:06,113
Ara, xin lỗi nhé.
Vậy tớ phụ cậu giặt đồ được không?

368
00:24:07,112-->00:24:10,113
Tớ không biết cậu cũng muốn giúp việc nhà đấy.
Vậy khi nào có việc gì, tớ sẽ nhờ cậu.

368
00:24:11,112-->00:24:16,113
Vậy thì tớ hy vọng cậu sẽ cho tớ cơ hội

368
00:24:17,112-->00:24:20,113
Ừ, tớ mong đến lúc đó đấy.

368
00:24:21,512-->00:24:26,113
Đến lúc đó đó,
tớ sẽ thưởng thức hương vị của nó đấy.

368
00:24:27,112-->00:24:29,513
Câu đó... có vẻ hơi sai sai...

368
00:24:29,112-->00:24:29,913


368
00:24:30,112-->00:24:32,513


368
00:24:33,112-->00:24:36,113


368
00:24:37,112-->00:24:38,513


368
00:24:39,112-->00:24:41,113


368
00:24:41,312-->00:24:44,113


368
00:24:45,112-->00:24:46,513


368
00:24:47,112-->00:24:48,113


368
00:24:48,312-->00:24:49,313


368
00:24:50,512-->00:24:51,513


368
00:24:52,112-->00:24:53,113


368
00:24:53,512-->00:24:55,113


368
00:24:55,912-->00:24:57,113


368
00:24:58,112-->00:24:59,113


368
00:24:59,512-->00:25:01,113


368
00:25:02,112-->00:25:03,113


368
00:25:04,112-->00:25:05,113


368
00:25:06,112-->00:25:07,113


368
00:25:08,112-->00:25:11,113


368
00:25:11,912-->00:25:13,113


368
00:25:13,312-->00:25:15,113


368
00:25:21,112-->00:25:23,113


368
00:25:25,112-->00:25:26,113


368
00:25:27,112-->00:25:31,113


368
00:25:32,112-->00:25:34,113


368
00:25:37,112-->00:25:40,113


368
00:25:40,512-->00:25:41,413



