﻿0
00:00:00,512-->00:00:03,113
1) Đừng trêu tớ!
2) Cảm ơn cậu

1
00:00:03,512-->00:00:05,113
Cảm... cảm ơn cậu. Tớ không sao.

2
00:00:06,112-->00:00:09,113
Trong bầu không khí này, 
sự cám dỗ của Kurumi khiến tôi
trượt chân ngã xuống sàn.

3
00:00:10,112-->00:00:14,113
Ara ara...

4
00:00:15,512-->00:00:17,513
Đừng có chọc tớ nữa!

5
00:00:18,512-->00:00:21,113
Tớ lỡ chân thôi!

6
00:00:21,112-->00:00:25,513
Có thật là cậu lỡ chân không đấy?

7
00:00:26,512-->00:00:28,513
Hở?

8
00:00:29,112-->00:00:36,513
Tớ muốn nghe một câu trả lời thành thật từ cậu.

9
00:00:37,512-->00:00:39,513
Thành thật... Kurumi...

10
00:00:44,512-->00:00:48,113
Kuthumi nhìn tôi, dần dần kéo váy lên.

11
00:00:53,512-->00:00:57,513
Kurumi kéo váy lên.
Đôi vớ đen quanh chân giống như
đang tạo ra một lỗ sâu đầy mê hoặc.
Sự xuất hiện của nó khiến tôi bất động.

12
00:00:58,512-->00:01:04,513
Shidou-san, đây là thứ cậu muốn nhìn đây.

13
00:01:05,512-->00:01:14,113
Cậu giả vờ ngã để thấy cảnh này mà phải không?
Hãy nhìn kỹ hơn đi.

14
00:01:15,112-->00:01:18,113
Nghe Kurumi nói, tôi đã nhìn lướt qua chỗ đó.

15
00:01:19,112-->00:01:27,113
Thôi nào Shidou-san... hãy nhìn kỹ hơn đi...

16
00:01:28,112-->00:01:30,113
Ku... Kurumi...

17
00:01:30,512-->00:01:34,113
Tôi không muốn nhìn.
Nhưng bây giờ.. Kurumi quá hấp dẫn. 
Tôi như bị mê hoặc.

18
00:01:35,112-->00:01:38,113
Nè Shidou-san...

19
00:01:39,112-->00:01:45,113
Cậu nhìn gần lại nữa, được không?

20
00:01:46,112-->00:01:48,113
Không... tớ không thể!

21
00:01:49,112-->00:01:54,113
Ara, kể cả trường hợp này sao...

22
00:01:55,112-->00:01:57,113
Không, không được!

23
00:01:58,112-->00:02:07,113
Thôi nào, không được sao?
Nhìn gần hơn, gần hơn nữa đi.

24
00:02:08,112-->00:02:10,113
Gần nữa...

25
00:02:11,112-->00:02:16,113
Đó là lời nói cuối cùng trước khi 
tâm trí tôi bị đóng băng.
Chuyện gì sẽ xảy ra từ bây giờ?
Tôi sẽ đi về đâu?

25
00:02:18,112-->00:02:20,113
Hở? Điện thoại à?

25
00:02:24,512-->00:02:30,113
Ara ara, tiếc thật.
Ai đó đã phá mất khoảnh khắc đẹp đẽ này rồi.

25
00:02:31,112-->00:02:32,513
Đúng là tiếc thật.

25
00:02:33,512-->00:02:37,113
Ý tôi là tôi tiếc vì 
người gọi điện không gọi sớm hơn. 

25
00:02:38,112-->00:02:45,513
Nhưng mà... Shidou-san cũng đã
nhìn tớ rất chăm chú rồi.

25
00:02:46,512-->00:02:50,513
Đó là một tín hiệu tốt đấy.

25
00:02:51,512-->00:02:53,113
Haha...

25
00:02:59,112-->00:03:04,113
Sau đó, tôi và Kurumi đã ăn bánh 
và nói chuyện với nhau cùng với ly trà nguội.

25
00:03:05,113-->00:03:07,113
Nhân tiện, Kurumi.
Sau giờ học ngày mai, cậu có rảnh không?

25
00:03:08,112-->00:02:15,113
Tớ cũng ít việc thôi. Chắc sẽ xong sớm.

25
00:03:16,112-->00:03:19,113
Tốt lắm. Tớ cũng sẽ cố làm bài xong.

25
00:03:20,112-->00:03:24,113
Ngày mai cậu định làm gì à?

25
00:03:25,112-->00:03:29,113
Không, chỉ là tớ muốn rủ cậu xem phim thôi.

25
00:03:30,112-->00:03:34,113
Cậu muốn rủ tớ đi à?

25
00:03:35,112-->00:03:37,113
Ừ. Có được không?

25
00:03:38,112-->00:03:43,113
Tớ vui lắm. Và...

25
00:03:44,112-->00:03:46,113
Và gì?

25
00:03:47,112-->00:03:51,113
Giờ tớ đang rất hạnh phúc.

25
00:03:52,112-->00:03:54,513
Ừ, tớ cũng vậy.

26
00:03:55,512-->00:03:59,113
Sau đó, tôi quàng vai Kurumi.
Tôi cảm thấy cơ thể, hơi ấm của nhau.

26
00:04:00,113-->00:04:04,113
Vậy thì Shidou-san...

26
00:04:05,113-->00:04:09,513
Ngay cả khi hết chủ đề, Kurumi vẫn nói thế.
Tôi bối rối, không biết làm gì 
ngoài ước khoảng thời gian này kéo dài mãi.

26
00:04:16,112-->00:04:20,112
Hôm nay vui thật đấy.

26
00:04:21,112-->00:04:23,112
Ừ, tớ cũng vui lắm.

26
00:04:26,512-->00:04:28,113
Gì vậy?

26
00:04:29,112-->00:04:37,113
Hôm nay, cậu có vẻ rất hài lòng về chuyện ấy đấy.

26
00:04:38,112-->00:04:40,113
Tớ đã nói tớ không thích mà!

26
00:04:41,112-->00:04:45,113
Sau đó, tôi đã cố gắng để bào chữa. 
Kurumi đột nhiên ôm lấy tôi và thì thầm.

26
00:04:46,112-->00:04:52,113
Tớ muốn cậu biết cậu đã quyết định mọi thứ rồi.

26
00:04:53,512-->00:04:56,113
Cái...?

45
00:04:57,112-->00:05:01,113
Vậy chào nhé, Shidou-san.

46
00:05:03,112-->00:05:05,113
Kurumi, để lại lời nói cuối cùng,
vẫy tay với tôi và đi ra ngoài.

47
00:05:12,112-->00:05:14,113
Sau giờ học, tôi ngồi trong lớp đợi Kurumi cùng về.

48
00:05:15,512-->00:05:17,113
Cô ấy trễ quá.

49
00:05:18,112-->00:05:20,113
Rạp chiếu phim... cũng sắp đến giờ rồi.

50
00:05:21,112-->00:05:24,113
Tama-chan sensei nhờ cô ấy giúp đỡ.
Nhưng có lẽ tôi nên đi cùng chăng?

51
00:05:26,112-->00:05:28,113
Khi nào cô ấy về, nhất định chúng tôi sẽ đi xem phim.

52
00:05:29,112-->00:05:32,113
Nhưng cô ấy lại muốn giúp Okamine Tamae (Tama-chan) sensei
mang đồ đến phòng hội học sinh.

53
00:05:34,112-->00:05:35,113
Hmm... mình biết rồi.

54
00:05:36,712-->00:05:39,113
Thay vì ngồi đây, mình sẽ đi giúp Kurumi và Tama-chan.

55
00:05:43,112-->00:05:47,113
Giờ không còn ai trong lớp cả. Hay là họ xong rồi?
Chúng ta đã giải quyết xong chưa?

56
00:05:49,112-->00:05:51,113
Mình nghĩ không đúng lúc chút nào. Tôi nên quay lại.

57
00:05:52,612-->00:05:55,113
Nhưng thay vì quay lại lớp, tôi có thể 
chờ ở cầu thang. Nhưng...

58
00:05:56,112-->00:05:57,113
Hở? Kurumi?

59
00:05:58,612-->00:06:02,113
Mình vừa thấy ai đó giống Kurumi đi lên sân thượng.
Có thể đó là Kurumi. Đi theo thử xem.

60
00:06:09,112-->00:06:12,113
Trên sân thượng, tôi thấy Kurumi.
Cô ấy đứng gần hàng rào, nhìn xuống học sinh phía dưới.

41
00:06:14,112-->00:06:15,113
Kurumi?

42
00:06:17,112-->00:06:19,113
Ara... Shidou-san.

43
00:06:25,112-->00:06:28,113
Làm ơn đừng hoảng hốt như thế.

44
00:06:30,512-->00:06:36,313
Tớ đã lo cậu đi về mà không có tớ.

45
00:06:39,112-->00:06:41,113
Xin lỗi, tớ nghĩ tớ sẽ giúp cậu 
nhưng tớ đã bỏ rơi cậu.

46
00:06:43,112-->00:06:45,513
Cậu đến để... giúp?

47
00:06:47,113-->00:06:49,113
Phải. Tớ nghĩ chúng ta nên đi sớm.

48
00:06:51,112-->00:06:53,113
Nhưng chúng ta phải giải quyết xong đã.

49
00:06:54,112-->00:06:56,113
Nhân tiện, cậu làm gì trên này vậy?

50
00:06:57,112-->00:07:03,113
À, thật ra thì Okamine-sensei nói tớ tìm cậu.

51
00:07:05,113-->00:07:07,113
Tama-chan? Cô ấy muốn gì?

52
00:07:08,113-->00:07:10,113
Dù tôi nghĩ thế nào, tôi cũng không đoán được gì cả.

53
00:07:11,113-->00:07:13,113
Cậu có biết gì không, Kurumi?

54
00:07:14,113-->00:07:22,113
Không, cô ấy chỉ nói tớ tìm cậu thôi.
Thật ra, Shidou-san, cậu đã nộp bài tập của cậu chưa?

55
00:07:24,113-->00:07:26,113
Bài tập? Môn Khoa học xã hội... Aaa!

56
00:07:27,113-->00:07:30,113
Đó là lúc tôi nhớ ra.
Có bài tập phải nộp ngày hôm nay.

57
00:07:31,113-->00:07:33,113
Thôi chết. Tớ quên mất.

58
00:07:34,113-->00:07:37,113
Cậu vẫn chưa làm gì cả à?

59
00:07:39,113-->00:07:42,113
Tớ làm rồi. Tớ chỉ quên nộp thôi. Tin tớ đi.

60
00:07:43,113-->00:07:53,113
Cho dù tớ có tin cậu hay không thì...
Thật sự chẳng quan trọng.

61
00:07:55,112-->00:07:56,513
Phải. Tệ thật.

62
00:07:58,112-->00:08:01,113
Ara, chính xác là tệ gì vậy?

63
00:08:02,512-->00:08:05,513
Giờ vẫn còn sớm nên tớ nghĩ tớ có lẽ 
sẽ về nhà lấy nó hay đại loại thế.

64
00:08:07,112-->00:08:15,112
Đây không phải là lỗi của cậu vì đã để quên sao Shidou-san?
Nếu cậu về lấy nó thì chắc vẫn kịp đấy.

65
00:08:16,112-->00:08:18,112
Đúng vậy nhưng... còn lời hứa 
của chúng ta thì sao, Kurumi?

66
00:08:19,112-->00:08:21,113
Kurumi nói đây là bộ phim cô ấy muốn xem
nhưng chúng tôi chưa có dịp để đi.

67
00:08:21,312-->00:08:23,113
Hôm nay chúng tôi phải đi xem. Ai biết được 
sau này chúng tôi có còn cơ hội hay không?

68
00:08:25,112-->00:08:32,112
Cậu nói cậu quên việc này vì tớ sao?

69
00:08:34,112-->00:08:36,113
Ơ... um... phải...

70
00:08:37,113-->00:08:39,113
Thật xấu hổ khi cô ấy hỏi vậy. 
Tôi cảm thấy mặt mình đang đỏ lên.

71
00:08:41,113-->00:08:44,113
Chúng tôi sẽ không còn cơ hội để xem bộ phim đó.
Chúng tôi cũng không biết họ có bán trễ không.

72
00:08:45,113-->00:08:49,113
Vậy cậu định làm gì, Shidou-san?

73
00:08:51,113-->00:08:53,113
Xin lỗi Tama-chan. Em sẽ nôp bài tập đó vào ngày mai.

74
00:08:54,113-->00:08:57,113
Không phải là tôi chưa làm bài tập.
Tôi thật sự sẽ nộp vào ngày mai.

75
00:08:58,112-->00:09:01,513
Kurumi, cậu có thể nói với Okamine-sensei 
tớ đã rời khỏi trường rồi không?

76
00:09:03,112-->00:09:05,113
Khi tôi nói thế, Kurumi suy nghĩ.

77
00:09:07,113-->00:09:13,113
Không vấn đề gì. Cậu cứ để đấy cho tớ.

78
00:09:15,113-->00:09:17,113
Và cô ấy nở một nụ cười rất dễ thương.

79
00:09:18,113-->00:09:20,113
Cảm ơn cậu nhiều lắm! Chúng ta đi thôi!

80
00:09:21,513-->00:09:27,113
Đầu tiên, tôi sẽ lấy cặp của tôi trong phòng.
Tôi đã nghĩ sẽ bỏ lại cặp nhưng vé xem phim ở trong đó.

81
00:09:28,113-->00:09:33,113
Được rồi. Từ bây giờ  tớ sẽ hỗ trợ cậu.

82
00:09:35,113-->00:09:37,113
Được rồi. Đi thôi nào.

83
00:09:41,113-->00:09:44,113
Nhưng mà, sao cậu lại muốn giúp tớ vậy Kurumi?

84
00:09:45,113-->00:09:54,113
Vì tớ cần sắp xếp bản in cho bài học kế tiếp. 
Bản in còn nhiều nên giúp một chút cũng được. 

85
00:09:55,513-->00:09:57,513
À phải. Cậu là lớp trưởng mà.

86
00:09:58,513-->00:10:01,113
Khi tôi nói vậy, Kurumi đột nhiên dừng lại.

87
00:10:02,113-->00:10:04,113
Kurumi, chuyện gì vậy?

88
00:10:05,113-->00:10:10,113
Im lặng, Shidou-san. Cậu có nghe gì không?

88
00:10:11,513-->00:10:12,813
Hể?

89
00:10:14,112-->00:10:17,113
Tôi nghe Kurumi nói và lắng nghe.
Một âm thanh quen thuộc phát ra từ hành lang.

113
00:10:18,112-->00:10:23,113
Ara, em có thấy Tokisaki-san và Itsuka-kun không?

114
00:10:26,112-->00:10:30,113
Vậy à. Cảm ơn nhé.

1
00:10:32,112-->00:10:33,513
Giọng... nói... này...

1
00:10:34,712-->00:10:38,113
Âm thanh phát ra từ hành lang 
chính là Okamine-sensei, hay còn gọi là Tama-chan.

1
00:10:39,112-->00:10:42,113
Shidou-san, nhanh lên.

1
00:10:43,512-->00:10:45,113
Được rồi.

1
00:10:46,112-->00:10:48,113
Chúng tôi đi về phía cầu thang.

1
00:10:52,112-->00:10:54,113
Giờ thì sao?

1
00:10:56,112-->00:10:58,513
Đầu tiên, đi xuống tầng dưới, vòng qua 
cầu thang bên kia rồi vào lớp.

368
00:10:59,113-->00:11:00,113
Hiểu rồi.

368
00:11:06,112-->00:11:09,113
Chúng tôi thận trọng đi xuống và vòng qua 
cầu thang bên kia rồi trở về lớp.

368
00:11:10,113-->00:11:12,113
Tốt, có vẻ như cô ấy chưa vào đây.

368
00:11:14,113-->00:11:23,113
Tớ có thể đi theo nhưng... tớ không thể 
kéo dài thời gian được.

368
00:11:25,112-->00:11:28,113
Chuyện gì đã lỡ thì lỡ rồi. Cho tớ một phút.

368
00:11:29,112-->00:11:32,113
Tôi đi về phía chỗ ngồi của tôi.
Nhưng... cái cặp không có ở đây.

368
00:11:34,112-->00:11:35,113
Cái...

368
00:11:36,512-->00:11:38,113
Có chuyện gì vậy?

368
00:11:40,112-->00:11:42,113
Cặp của tớ mất rồi.

368
00:11:47,112-->00:11:49,113
Tôi tìm xung quanh nhưng không hề thấy cặp của tôi.

368
00:11:50,113-->00:11:51,513
Giờ thì sao đây...

68
00:11:54,113-->00:11:58,113
Shidou-san, qua đây đi.

368
00:12:00,112-->00:12:03,113
Kurumi gọi tôi từ phía cái bảng. 
Ở bản giáo viên có cái cặp với một dòng ghi chú.

368
00:12:06,112-->00:12:09,113
Gì đây? "Shidou để quên cặp. Vì vậy 
hãy để đây hoặc trả cho cậu ấy" - Tonomachi.

368
00:12:10,112-->00:12:12,113
Chết tiệt thật, Tonomachi...

368
00:12:14,112-->00:12:16,113
Có lẽ cậu ta nghĩ mình sẽ đồng tình với 
chuyện này. Thật chẳng cần thiết chút nào, Tonomachi.

368
00:12:17,512-->00:12:19,513
Được rồi. Lấy cặp rồi đi thôi.

368
00:12:21,112-->00:12:23,113
Nhưng Kurumi nắm tay tôi lại.

368
00:12:25,312-->00:12:26,513
Gì vậy Kurumi?

368
00:12:29,112-->00:12:33,113
Shidou-san, trễ rồi! Cô ấy đến rồi!

368
00:12:35,112-->00:12:37,113
Hả? Thật sao?

368
00:12:39,112-->00:12:41,113
Lắng nghe thật kỹ, đó là âm thanh đã
phát ra ở hành lang lúc nãy.

368
00:12:44,112-->00:12:47,513
Tokisaki-san? Itsuka-kun?

368
00:12:49,512-->00:12:55,113
Các em có ở đây không? Cô muốn nói chuyện về...

368
00:12:58,112-->00:12:59,113
Tệ thật!

368
00:13:00,512-->00:13:04,113
Okamine-sensei đi về phía lớp học.
Tôi chỉ vừa lấy được cái túi, giờ thì 
chúng tôi đi lối nào đây?

368
00:13:06,112-->00:13:11,113
Shidou-san, cậu định làm gì? Cô ấy đang tìm cậu đó.

368
00:13:13,112-->00:13:15,113
Tớ không thể ra khỏi phòng được.

368
00:13:16,112-->00:13:18,113
Tớ sẽ...

368
00:13:19,112-->00:13:28,113
1) Đã quá muộn rồi. Tớ đầu hàng.
2) Kurumi, giúp tớ thoát ra khỏi đây!

368
00:13:29,112-->00:13:31,113
Hiện giờ, tôi không còn cơ hội để trốn 
hay chạy thoát. Tình hình thật khó khăn.

368
00:13:35,112-->00:13:37,113
Shidou-san?

368
00:13:39,112-->00:13:42,513
Nhưng nếu tôi từ bỏ, chúng tôi sẽ không thể
đi xem phim. Kurumi rất mong chờ bộ phim đó.

368
00:13:44,112-->00:13:46,113
Phải có cách nào đó. Mình không thể từ bỏ được!

368
00:13:48,112-->00:13:50,113
Bình tĩnh nào. Phải có phương án nào đó chứ.

368
00:13:51,112-->00:13:55,513
Phương án... Cậu nghĩ ra gì à?

368
00:13:57,512-->00:14:00,113
Nghe Kurumi nói, tôi nhìn xung quanh lần nữa.
Không có chỗ nào để tôi trốn cả.

368
00:14:02,112-->00:14:04,113
Sẽ thoát được nếu tớ có thể trốn đâu đó.

368
00:14:05,112-->00:14:07,113
Một chỗ trốn...

368
00:14:08,112-->00:14:11,113
Trong lớp vẫn còn chỗ để trốn 
là tủ đồ vệ sinh và gầm bàn giáo viên.

368
00:14:12,112-->00:14:14,113
Chính nó! Bàn giáo viên!

368
00:14:15,112-->00:14:17,113
Xin lỗi Kurumi. Hãy giúp tớ ra khỏi đây.

368
00:14:18,112-->00:14:22,113
Tớ phải làm gì để giúp cậu?

368
00:14:24,112-->00:14:28,113
Tớ sẽ trốn dưới gầm bàn giáo viên. Cậu chỉ cần 
ngồi phía trước là cô ấy không thấy tớ.

368
00:14:33,112-->00:14:35,113
Một nụ cười đột nhiên xuất hiện
trên gương mặt Kurumi khi nghe tôi nói.

368
00:14:36,312-->00:14:37,113
Gì vậy?

368
00:14:39,113-->00:14:42,113
Ara ara, tớ nghĩ tớ không còn lựa chọn rồi.

368
00:14:47,113-->00:14:52,113
Thật ra là tớ cũng nghĩ như vậy đấy.

368
00:14:54,113-->00:14:56,113
Ờ, dễ thôi mà! Xin lỗi nhưng tớ phải nhờ cậu.

368
00:14:58,112-->00:15:01,113
Tớ hiểu rồi.

368
00:15:03,113-->00:15:06,113
Tôi gật đầu, lấy cặp rồi chui vào gầm bàn.

368
00:15:07,113-->00:15:08,113
Được rồi. Cậu ngồi vào đi, Kurumi!

368
00:15:10,113-->00:15:12,113
Giờ cô ấy chỉ cần ngồi trước cái bàn và che tôi.

368
00:15:14,113-->00:15:18,113
Vậy thì Shidou-san, cho tớ xin lỗi nhé.

368
00:15:20,112-->00:15:21,113
Hở... cái gì?

368
00:15:25,113-->00:15:28,113
Và mọi thứ xung quanh tôi tối om.
Có thứ gì đó mềm mềm trước mặt tôi.

368
00:15:30,112-->00:15:31,113
Ku... Kurumi? Cậu đang làm...

368
00:15:33,112-->00:15:35,113
Shidou-san, đừng cử động.
Cậu làm tớ nhột đấy.

368
00:15:38,112-->00:15:39,513
Cái gì...

368
00:15:41,112-->00:15:45,113
Im lặng nào, Okamine-sensei sắp đến rồi...

368
00:15:47,113-->00:15:50,113
Lúc đó, cánh cửa mở ra. Tôi nín thở lại.

368
00:15:51,113-->00:15:54,113
Tokisaki-san, em ở đây phải không?

368
00:15:57,113-->00:15:59,113
Ara, Okamine-sensei.

368
00:16:01,112-->00:16:04,512
Em đang làm gì ở đây vậy?

368
00:16:06,112-->00:16:11,112
Em đang tìm vài thứ ạ.

368
00:16:15,112-->00:16:19,113
Một cảm giác mềm mại. Một mùi hương ngọt ngào.
Và tôi có thể cảm thấy hơi ấm của Kurumi.

368
00:16:22,112-->00:16:25,112
Không biết cô ấy có biết tôi đang nghĩ gì không,
Kurumi tiếp tục nói chuyện với Okamine-sensei.

368
00:16:26,112-->00:16:31,112
Vậy em tìm thấy Itsuka-kun chưa?

368
00:16:33,112-->00:16:41,112
Em xin lỗi. Có lẽ cậu ấy rời khỏi trường
trước khi em đi tìm rồi ạ.

368
00:16:43,112-->00:16:45,112
Có một chút vấn đề khi tôi cố gắng thở.
Tôi cử động đầu của tôi khi họ đang nói chuyện.

368
00:16:47,112-->00:16:49,112
Nếu em tìm... Aaa...

368
00:16:54,112-->00:16:56,112
Khi tôi cử động, Kurumi co giật 
một chút và làm gián đoạn cuộc trò chuyện.

368
00:16:57,112-->00:17:01,113
Tokisaki-san, em làm sao vậy?

368
00:17:05,113-->00:17:10,113
Không... không có gì đâu ạ.
Em nghĩ vừa có bụi bay vào mặt em.

368
00:17:13,113-->00:17:16,113
Kurumi tỏ ra chút bối rối.

368
00:17:17,113-->00:17:20,113
Nếu vậy thì...

368
00:17:22,113-->00:17:28,113
À phải rồi, Tokisaki-san. Về chuyện Itsuka-kun...

368
00:17:31,113-->00:17:33,113
Tôi phản ứng theo bản năng 
khi nghe thấy tên của tôi

368
00:17:36,513-->00:17:39,113
Aaa... Vâ... vâng?

368
00:17:42,113-->00:17:47,113
Tokisaki-san, có thật là không có chuyện gì không?

368
00:17:51,113-->00:17:56,113
Vâng, không có gì thật mà, thưa cô.

368
00:17:58,113-->00:17:59,513
Thật không?

368
00:18:01,112-->00:18:04,112
Có vẻ như tôi đã đụng vào đâu đó trên người
Kurumi khi cử động. Xin lỗi Kurumi.

368
00:18:06,112-->00:18:09,112
Tokisaki-san, có khi nào...

368
00:18:12,112-->00:18:16,112
Chết tiệt. Cô ấy biết rồi sao? Tôi cố giữ 
im lặng nhưng tôi không thể thở im lặng được.

368
00:18:22,112-->00:18:27,112
Hiểu rồi. Em cảm thấy không khỏe phải không?

368
00:18:31,112-->00:18:40,113
Vâng... đúng ạ. Chỉ là em... 
đang "đau bụng" ạ...

368
00:18:42,112-->00:18:46,112
Từ giữa hai đùi của cô ấy, tôi cố gắng hít ít 
không khí nhưng cô ấy lại co giật 
và tôi cảm thấy mùi hương ngọt ngào đó.

368
00:18:47,112-->00:18:52,112
Vậy thì em phải đi về đi.

368
00:18:56,112-->00:19:03,113
Em vẫn còn chút việc phải làm mà...

368
00:19:05,512-->00:19:09,113
Nếu có liên quan đến trường thì cứ để cô cho.

368
00:19:11,112-->00:19:18,113
Không. Không sao đâu. Em là lớp trưởng mà.
Đó là nhiệm vụ của em.

368
00:19:21,112-->00:19:25,113
Tôi cuối cùng cũng chú ý đến việc hít thở.
Kurumi chắc chắn là có hành vi khác thường.

368
00:19:26,112-->00:19:36,113
Vậy sao? Vậy em đừng cố gắng quá và
hãy về nhà sớm nhé, được chứ?

368
00:19:38,112-->00:19:48,113
Tiện thể, nếu em đến nhà Itsuka-kun,
nói với em ấy nộp bài tập ngày mai nhé.

368
00:19:50,113-->00:19:55,113
Em hiểu rồi. Chào cô Okamine-sensei...

368
00:19:57,513-->00:20:01,113
Và như thế, chúng tôi đã thoát. Tuy nhiên,
Kurumi sau đó đã rất tức giận với tôi.

368
00:20:07,513-->00:20:09,113
Tối nay vui quá!

368
00:20:10,112-->00:20:13,113
Phải. Bộ phim rất hay.

368
00:20:15,112-->00:20:18,113
Sau khi ra khỏi trường, chúng tôi đi
xem phim. Tất cả đều đúng kế hoạch.

368
00:20:19,112-->00:20:20,513
Um... Kurumi.

368
00:20:22,112-->00:20:24,113
Gì vậy?

368
00:20:25,112-->00:20:27,513
Cảm ơn chuyện hồi chiều.

368
00:20:29,112-->00:20:35,113
Không có gì. Tớ không phiền khi giúp
đỡ cậu đâu, Shidou-san.

368
00:20:37,112-->00:20:39,113
Cậu lúc nào cũng thế.

368
00:20:40,112-->00:20:44,113
Nhưng tớ đoán... như vậy có hơi nhiều đấy.

368
00:20:49,112-->00:20:56,113
Hãy nói cậu nợ tớ một lần đi.
Sau này cậu sẽ trả ơn tớ chứ?

368
00:20:58,112-->00:21:00,113
Tất nhiên rồi. Chắc chắn tớ rất biết ơn cậu.

368
00:21:01,112-->00:21:04,113
Bằng cách nào đó, đúng như mong đợi, 
Kurumi có vẻ đã ở thế trên.
Nghĩ lại thì tất cả đều do lỗi của tôi.

368
00:21:05,112-->00:21:07,113
Cậu dễ thương lắm, Kurumi.

368
00:21:08,112-->00:21:13,113
Shidou-san, cậu nói lại được không?

368
00:21:15,112-->00:21:17,513
Không có gì. Cảm ơn cậu vì hôm nay, Kurumi.

368
00:21:19,112-->00:21:23,113
Shidou-san, cậu lúc nào cũng vậy.

368
00:21:25,112-->00:21:28,113
Và rồi, chúng tôi tiếp tục đi về nhà.

368
00:23:50,512-->00:23:53,113
Mấy ngày sau, Kurumi đã hẹn gặp tôi trở lại.

368
00:23:54,112-->00:23:58,113
Những buổi gặp cũng trở nên thường xuyên, 
nhưng những cuộc hẹn thì 
đã một thời gian dài chưa đi.

368
00:23:59,112-->00:24:03,513
Như thường lệ, chúng tôi đến công viên
và nói những chuyện ngớ ngẩn với Kurumi.

368
00:24:04,512-->00:24:10,113
Shidou-san, có chuyện này.

368
00:24:11,112-->00:24:15,113
Tại thời điểm đó, chúng tôi đang ngồi
ở công viên, Kurumi đột nhiên cất tiếng.

368
00:24:16,112-->00:24:18,113
Gì vậy?

368
00:24:19,112-->00:24:25,513
Thực ra, tớ thấy Shidou-san khác trước nhiều lắm.

368
00:24:26,512-->00:24:28,513
Khác lắm sao?

368
00:24:29,112-->00:24:32,113
Đúng vậy.

368
00:24:33,112-->00:24:36,113
Tôi nghĩ cô ấy đang đùa.
Tuy nhiên, Kurumi rất nghiêm túc 
và khác bình thường.

368
00:24:37,112-->00:24:39,113
Um... tớ khác trước... nghĩa là sao?

368
00:24:40,112-->00:24:45,113
Phải. Cậu đã thay đổi nhiều rồi.

368
00:24:46,112-->00:24:48,113
Thay đổi nhiều sao?

368
00:24:49,112-->00:24:52,513
Vâng... đã được một thời gian...

368
00:24:53,512-->00:24:56,113
Kurumi nói và ngồi lên một con ngựa gỗ.

368
00:24:57,312-->00:25:05,113
Đã được một thời gian từ khi 
chúng ta vui vẻ với nhau rồi nhỉ.

368
00:25:06,112-->00:25:10,113
Cuối cùng, cô ấy cũng đã nở một nụ cười dịu dàng.
Vẻ mặt nghiêm trọng nãy giờ
khiến tim tôi đập thình thịch.

368
00:25:11,112-->00:25:14,113
Đúng vậy. Cũng đã một thời gian rồi.
Cậu thấy trước đây tớ thế nào?

368
00:25:15,512-->00:25:18,113
Tôi đang phải đối mặt với một Kurumi 
nói chuyện rất thẳng thắn.
Và Kurumi, đang có biểu hiện 
nghiêm túc, tiếp tục nói.

368
00:25:19,112-->00:25:29,113
Trước đây, cậu luôn sợ hãi tớ.
Cậu cố gắng thực hiện những gì tớ muốn.

368
00:25:30,112-->00:25:31,513
Kurumi...

368
00:25:32,112-->00:25:39,113
Nhưng giờ cậu khác trước nhiều rồi.
Cậu đã thoải mái, chủ động hơn 
và không đề phòng gì cả.

368
00:25:40,112-->00:25:42,113
Tớ...

368
00:25:43,112-->00:25:46,113
Kurumi bằng cách nào đó dường như bất động.
Tôi suy nghĩ và có một ý tưởng.

368
00:25:47,112-->00:25:49,113
Được rồi... vậy tớ có một đề nghị.

368
00:25:50,112-->00:25:53,113
Vâng. Bất cứ điều gì.

368
00:25:54,112-->00:25:57,113
Vào sáng chủ nhật tuần sau lúc 9h00, 
tớ sẽ đợi ở đối diện nhà ga Tengu

368
00:25:58,112-->00:26:03,113
Tại sao vậy?

368
00:26:04,113-->00:26:07,113
Đừng hỏi. Tớ sẽ đợi cậu.

368
00:26:08,112-->00:26:15,513
Tớ không hiểu lắm.
Nhưng nếu cậu muốn, tớ sẵn lòng.

368
00:26:16,512-->00:26:19,113
Được rồi, nhớ nhé.
Không thay đổi vào phút chót nhé.

368
00:26:20,112-->00:26:29,113
Tớ đã bao giờ đến trễ chưa?
Chỉ có cậu là hay dậy trễ thôi.

368
00:26:30,112-->00:26:32,413
Được rồi. Tớ sẽ cố gắng dậy sớm.

368
00:26:33,112-->00:26:36,113
Cậu tự dậy được à?

368
00:26:37,112-->00:26:39,113
Haha... đúng vậy.

368
00:26:40,112-->00:26:43,113
Dễ dãi với bản thân là không tốt đâu.

368
00:26:44,112-->00:26:46,113
Được rồi, haha...

368
00:26:49,512-->00:26:52,113
Và chúng tôi đã lên lịch cho chủ nhật tới.

368
00:26:57,512-->00:27:00,113
Ngày chủ nhật đến trong nháy mắt.

368
00:27:01,112-->00:27:03,113
Được rồi, đây là lúc thích hợp.

368
00:27:04,112-->00:27:06,113
Kia rồi. Này Kurumi!

368
00:27:07,112-->00:27:11,113
Kurumi nhận ra tôi.
Với nụ cười dễ thương, cô vẫy tay.

368
00:27:12,512-->00:27:17,113
Shidou-san, chào buổi sáng.

368
00:27:18,512-->00:27:20,113
Ừ, chào cậu. Cậu đến lâu chưa?

368
00:27:21,112-->00:27:25,513
Chưa. Tớ cũng mới đến thôi.

368
00:27:26,512-->00:27:32,113
Nhân tiện Shidou-san, 
có chuyện này tớ muốn hỏi.

368
00:27:33,112-->00:27:35,113
Chuyện gì vậy?

368
00:27:36,112-->00:27:40,513
Buổi hẹn hò này... có gì khác mọi khi không?

368
00:27:41,512-->00:27:43,513
Không, sao vậy?

368
00:27:44,512-->00:27:53,113
Yiraeseon, giống như bình thường. Sau đó yêu cầu khác ...

이래선, 평소와 똑같잖아요 그렇다면 다른 부탁이...

368
00:27:54,112-->00:27:57,113
Hôm nay, tớ sẽ nói những gì tớ nghĩ, Kurumi.

368
00:28:01,112-->00:28:05,113
Tôi che giấu sự bối rối của mình,
nhìn thẳng vào khuôn mặt của Kurumi 
và tiếp tục câu chuyện.

368
00:28:06,112-->00:28:08,513
Tớ muốn có một buổi hẹn hò bình thường với cậu.
Tớ muốn dành một thời gian dễ chịu cùng cậu

368
00:28:09,512-->00:28:13,113
Shidou-san...

368
00:28:14,112-->00:28:18,113
Tớ muốn tiếp tục một cách tự nhiên như vậy.
Cả bây giờ và tương lai, Kurumi.

368
00:28:19,512-->00:28:22,113
Nhưng nếu vậy thì...

368
00:28:24,112-->00:28:28,113
Tớ biết trước đây tớ luôn sợ cậu.
Tớ luôn làm mọi thứ cho cậu vì sợ cậu.
Nhưng giờ, những gì tớ làm hoàn toàn tự nguyện.

368
00:28:30,112-->00:28:32,413
Nhưng mà, tớ...

368
00:28:34,112-->00:28:37,113
Tớ muốn hạnh phúc cùng Kurumi.
Còn cậu thì sao?

368
00:28:41,113-->00:28:44,113
Tớ rất hạnh phúc. Và tớ cũng muốn cậu như vậy.
Cậu không cần phải xấu hổ. Hãy tin tớ.

368
00:28:46,112-->00:28:49,113
Tôi nói đến đây, khuôn mặt Kurumi rạng ngời. 
Tôi xấu hổ, tránh nhìn vào cô ấy.

368
00:28:50,112-->00:28:52,113
Sau đó, 2 đứa đều im lặng.

368
00:28:53,112-->00:28:57,113
Tôi nhìn những biểu hiện của sự im lặng 
để tránh ánh mắt Kurumi.
Lần đầu tiên Kurumi có biểu hiện này.

368
00:28:58,112-->00:29:13,513
Tớ cũng muốn như vậy.
Nghe cậu nói, tớ cảm thấy mình 
được chấp nhận và được sống.

368
00:29:14,512-->00:29:20,113
Phải. Như tớ đã nói,
tớ muốn chúng ta hạnh phúc cùng nhau.

368
00:29:21,112-->00:29:26,113
Shidou-san, cậu nói nghe lãng mạn thật.

368
00:29:28,112-->00:29:32,113
Kurumi cúi đầu với khuôn mặt xấu hổ.
Có lẽ chúng tôi đã yêu nhau.
Tôi nhìn rồi nắm lấy tay cô ấy.

368
00:29:36,112-->00:29:39,113
Từ từ nào, Shidou-san.

368
00:29:41,112-->00:29:43,113
Đi nào, Kurumi.
Hôm nay chúng ta sẽ hẹn hò!

368
00:29:46,112-->00:29:50,113
Từ từ, đợi tớ một chút nào!

368
00:29:51,112-->00:29:54,113
Hôm nay, tôi muốn tìm và đến 
những nơi mà Kurumi thích đến.

368
00:29:57,112-->00:30:03,113
Shidou-san! Tớ muốn nói chuyện này trước đã.

368
00:30:05,112-->00:30:07,113
Là gì vậy?

368
00:30:09,112-->00:30:19,113
Cảm ơn cậu nhiều lắm. Shidou-san.
Tớ cũng sẽ cố gắng làm cậu hạnh phúc.

368
00:30:20,112-->00:30:23,113
Cảm ơn cậu. Trông cậu thật dễ thương đấy.
Giờ chúng ta đi thôi, Kurumi.

368
00:30:23,812-->00:30:26,113
Vâng!

368
00:30:29,112-->00:30:33,113
Những gì chúng tôi đã trải qua,
những gì chúng tôi sẽ gặp phải.

368
00:30:33,512-->00:30:37,113
Có lẽ chúng tôi phải mất nhiều thời gian nữa
để có thể ở bên nhau hạnh phúc.

368
00:30:37,512-->00:30:40,113
Nhưng trên tất cả, trái tim chúng tôi thuộc về nhau.
