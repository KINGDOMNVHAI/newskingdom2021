﻿0
00:00:00,512-->00:00:02,113
1) Hẹn hò với Tohka.
2) Hẹn hò với Origami.
3) Hẹn hò với chị em Yamai.
4) Hẹn hò với Miku.
5) Tiếp theo.

1
00:00:06,512-->00:00:09,113
Mấy cậu... tớ chẳng biết chọn ai cả...

2
00:00:10,512-->00:00:15,113
Nào Darling, chúng ta đi thôi.

3
00:00:16,112-->00:00:18,513
Khoan, chờ đã!

5
00:00:19,512-->00:00:25,113
Shidou, cậu không bị cám dỗ 
bởi Miku, phải không?

6
00:00:26,112-->00:00:28,113
Không, không phải...

7
00:00:30,112-->00:00:40,113
Hối thúc. Hãy chọn đi, Shidou.
Cậu có thể bắt cá 2 tay cả Kaguya và Yuzuru.

8
00:00:41,112-->00:00:43,113
Dù 2 cậu nói vậy...

8
00:00:44,112-->00:00:50,113
Shidou! Đi ăn bánh mì đậu nành đi.
Anh muốn ăn thử mà đúng không?

9
00:00:51,112-->00:00:53,113
Không, anh không thể...

10
00:00:55,112-->00:01:07,113
Khoan đã Shidou. Cậu quên rồi à? 
Cậu cần trả tớ cuốn sách giáo khoa

11
00:01:08,112-->00:01:10,113
Không... đó là cái bẫy ...

12
00:01:11,112-->00:01:13,113
Aaaaa! Chết tiệt! Chẳng có cách nào cả!

13
00:01:14,112-->00:01:17,113
Trong tình huống thế này... Đúng vậy!
Tôi chỉ muốn yêu cầu sự giúp đỡ từ Fraxinus.

14
00:01:18,112-->00:01:21,113
Khi tôi nói chuyện với các Tinh Linh,
tôi luôn được theo dõi và tư vấn. 
Bây giờ cũng vậy...

15
00:01:22,112-->00:01:25,113
Kotori! Cứu anh với!

16
00:01:26,512-->00:01:30,113
Ara Shidou. Anh đang gặp vấn đề, phải không?

17
00:01:31,112-->00:01:33,113
Kotori!

18
00:01:34,112-->00:01:37,112
Itsuka Kotori. Em gái tôi và cũng là
chỉ huy phi thuyền Fraxinus. 
Vào những lúc như thế này, nó là
người mà tôi có thể tin tưởng... Có lẽ vậy.

19
00:01:38,112-->00:01:40,112
Làm ơn... làm cái gì đó trong
tình hình này đi.

20
00:01:41,112-->00:01:49,512
Chán thật, không còn cách nào khác.
Fraxinus sẽ đưa anh lên đây.
Kiếm chỗ nào kín đáo đi.

21
00:01:50,512-->00:01:52,112
Anh hiểu rồi.

22
00:01:54,112-->00:01:59,112
Anh đi đâu vậy, Darling?

23
00:02:02,512-->00:02:06,113
Đứng lại, Shidou!

25
00:02:08,112-->00:02:11,513
Và tôi đã được đưa lên Fraxinus.





25
00:02:13,512-->00:02:15,113
Cảm ơn em.

25
00:02:16,112-->00:02:23,113
Anh làm gì thế hả, Shidou?
Anh có biết vị trí của anh không?

25
00:02:24,112-->00:02:32,113
Sức mạnh Tinh Linh gắn liền với cảm xúc của họ.
Anh không có lựa chọn nào khác 
ngoài việc cứu các Tinh Linh.

25
00:02:33,112-->00:02:35,113
Ừ, anh biết chứ.

25
00:02:36,112-->00:02:49,513
Em không biết khi nào sức mạnh sẽ bùng nổ 
ngay cả khi đã phong ấn.
Chăm sóc họ sau đó cũng rất quan trọng. 
Anh biết anh đang làm gì không?

25
00:02:50,112-->00:02:52,113
Có, anh biết.

25
00:02:53,112-->00:03:03,113
Vậy à? Vậy sao anh còn bỏ chạy?
Anh biết sức mạnh mất kiểm soát
khi họ xuống tinh thần mà. 

26
00:03:04,112-->00:03:08,113
Không... dù em nói vậy.
Trong tình huống đó, anh không biết phải chọn ai...

26
00:03:09,112-->00:03:13,113
Im đi, tên hề kia.

26
00:03:14,112-->00:03:16,113
Tên hề à?

26
00:03:17,112-->00:03:30,513
Tinh Linh mới sẽ xuất hiện trong tương lai.
Nói cách khác, số lượng Tinh Linh
sẽ còn tiếp tục gia tăng trong tương lai.
Nếu anh gặp lại trường hợp này thì sao?

26
00:03:31,512-->00:03:34,113
Tức là... anh phải chọn à?

26
00:03:35,112-->00:03:44,513
Phải. Ít nhất anh phải chọn ai đó
để có được sự ủng hộ của Tinh Linh.

26
00:03:45,512-->00:03:48,113
Vậy sao? Anh nên chọn ai...

26
00:03:49,112-->00:03:59,113
Anh có thể nghĩ sau.
Nếu anh có thể chia lịch ra
chắc chắn như một người đàn ông,
Các cô gái khác có thể đợi.

26
00:04:00,112-->00:04:09,513
Ý em là, tất cả mọi người,
đó là sự lựa chọn tồi tệ nhất.
Phải chọn một ai đó.

26
00:04:10,512-->00:04:14,113
Một con côn trùng muốn trở thành vi sinh vật à?
Bên cạnh đó, họ sẽ giận đấy.

26
00:04:15,112-->00:04:21,113
Im đi. Em không quan tâm đến điều đó.
Nhanh lựa chọn đi!

26
00:04:22,112-->00:04:25,113
Anh... anh không biết có nên chọn 
Tinh Linh cấp S...

26
00:04:26,112-->00:04:30,113
Đó thường là một em gái đáng yêu...
Trong "chế độ chỉ huy", thái độ đã thay đổi.
Không, có vẻ như rất nhiều lần 
con bé nói những thứ không liên quan đến Tinh Linh.

26
00:04:31,112-->00:04:38,513
Nghe đây, Shidou. Lần kế tiếp anh chạy đi,
em sẽ nhét đậu phộng vào mũi anh đấy.

26
00:04:39,512-->00:04:42,114
Nghe sợ thật đấy! Không phải thế.

26
00:04:43,114-->00:04:45,113
Gì cơ?

26
00:04:46,112-->00:04:48,113
Về lý thuyết, anh không chạy trốn.

26
00:04:49,112-->00:04:52,513
Bây giờ anh định bào chữa à?

26
00:04:53,512-->00:04:56,113
Không, không phải vậy. 
Anh không chạy trốn.

26
00:04:57,112-->00:05:03,113
Vậy nó là gì?
Em sẽ đá anh ra ngoài đấy.

26
00:05:04,112-->00:05:07,113
Nhìn em đi, Kotori. 
Em không phải Tinh Linh sao?

26
00:05:11,113-->00:05:14,113
Anh... anh nói cái quái gì vậy?

26
00:05:15,113-->00:05:18,113
Ý anh là, anh chọn em.
Anh không chạy trốn.

26
00:05:22,512-->00:05:26,113
Kotori, anh đã chọn em.
Anh quyết định hẹn hò với em.

26
00:05:27,112-->00:05:30,113
Kotori là một trong các Tinh Linh
tôi đã phong ấn gần đây.
Vì vậy, lý do này cũng hợp lý.

26
00:05:38,112-->00:05:43,113
Anh thôi đi! Anh nói gì vậy?

26
00:05:45,112-->00:05:47,113
Uida! Đau đấy!

26
00:05:48,112-->00:05:53,113
Đồ ngốc! Đó không phải lý do hay đâu!

26
00:05:54,112-->00:05:56,113
Tại sao...

26
00:05:57,112-->00:06:02,113
Em thì... không cần hẹn hò đâu.

26
00:06:03,112-->00:06:08,113
Không... nhưng nếu Tinh Linh khác 
đang trong trạng thái nguy hiểm
thì Kotori cũng có thể như thế, phải không?

26
00:06:09,112-->00:06:17,113
Được rồi! Em ổn mà! Em không quan tâm!
Em luôn cố gắng...

26
00:06:18,112-->00:06:20,113
Hở? Gì cơ?

26
00:06:21,112-->00:06:27,513
Không có gì đâu!
Bây giờ về đi! Em còn phải làm việc...

26
00:06:28,512-->00:06:31,113
Em không muốn hẹn hò...

26
00:06:32,112-->00:06:38,113
Được rồi! Em sẽ thả anh gần nhà,
Về chuẩn bị bữa tối đi.

26
00:06:39,112-->00:06:42,113
Được rồi. Cảm ơn em, Kotori.

26
00:06:43,112-->00:06:46,113
Đi về đi.

26
00:06:49,112-->00:06:52,113
Tôi nghe một vài bài giảng...
Dù sao, tôi đã về nhà mà không phải đi xe.

368
00:16:14,512-->00:16:17,113
Mình vừa ngủ dậy à?

368
00:16:18,112-->00:16:21,113
Mình đang ở nhà sao? Mình đang nằm mơ à?

368
00:16:25,112-->00:16:27,113
Có lẽ... đây là một giấc mơ kỳ lạ?

368
00:16:28,112-->00:16:31,113
Tôi có cảm giác rất kỳ lạ. 
Nhưng đây chắc chắn là nhà tôi.

368
00:16:32,112-->00:16:34,113
Giờ mình nên bắt đầu chuẩn bị bữa tối thôi.

368
00:16:36,112-->00:16:38,113
Anh đang nói những thứ ngớ ngẩn gì vậy?

368
00:16:39,512-->00:16:41,113
Hở? Kotori?

368
00:16:42,113-->00:16:44,113
Tôi nghe thấy giọng nói rõ ràng.
Nhưng tôi không thấy Kotori.

368
00:16:45,512-->00:16:48,113
Chuyện gì đang xảy ra vậy? 
Tiếng nói phát ra từ đâu? Và tại sao 
anh không đeo tai nghe liên lạc?

368
00:16:52,512-->00:16:54,113
Một khoảnh khắc im lặng. 
Bây giờ là tiếng nói của Kotori.

368
00:16:55,112-->00:17:03,113
Em đang nói chuyện trực tiếp với tâm trí anh.
Khi anh đang ở trong trò chơi, 
không cần phải dùng máy liên lạc.

368
00:17:04,112-->00:17:07,113
Trong trò chơi sao? Khoan đã!
Chẳng lẽ đây là... không gian ảo?

368
00:17:08,112-->00:17:11,113
Tôi nhanh chóng chạm vào một chiếc ghế,
cửa sổ và tường. Cảm giác vẫn như mọi khi.

368
00:17:12,112-->00:17:15,113
Tôi chạm vào mặt tôi. Phải, cả hai tay
và da mặt mình vẫn có cảm giác.

368
00:17:16,112-->00:17:19,113
Nếu đây là trò chơi thì chắc anh
sẽ không cảm thấy đau nếu tự véo má...

368
00:17:20,512-->00:17:22,513
Uida! Không! Anh vẫn thấy đau!

368
00:17:23,512-->00:17:30,513
Tất cả các giác quan đã kết nối.
Em đã nói với anh đây là một 
thế giới Siêu Giả Lập. 

368
00:17:31,112-->00:17:35,513
Cả anh và thế giới đều giống hệt với thực tế.

368
00:17:36,512-->00:17:39,513
Thế giới Siêu Giả Lập...

368
00:17:40,512-->00:17:52,113
Để chính xác, không phải tất cả các dữ liệu 
đều được chuyển đổi. Các đối tượng như ghế 
hoặc tường là từ ký ức của riêng anh, Shidou.

368
00:17:53,112-->00:17:55,113
Lấy hình ảnh từ ký ức của anh?

368
00:17:56,112-->00:18:05,113
Anh không cần quá quan tâm  
chi tiết đâu. Ồ, và cách liên lạc 
này hơi kém hơn bình thường đấy.

368
00:18:06,112-->00:18:08,113
Kém hơn à?

368
00:18:09,112-->00:18:17,113
Theo thời gian, sẽ có sự khác biệt 
về thời gian giữa thế giới ảo 
và thế giới thực.

368
00:18:18,112-->00:18:28,513
Thời gian trong thế giới ảo nhanh hơn. 
Một ngày trong đó bằng 
khoảng 20-30 phút ngoài thật.

368
00:18:29,112-->00:18:42,113
Vì vậy, khi một cuộc trò chuyện 
được thiết lập với thế giới thực, 
anh sẽ phải chỉnh lại thời gian đó.

368
00:18:43,112-->00:18:46,513
Nhưng bọn em không thể cho anh 
hướng dẫn chi tiết như chúng ta thường làm. 

368
00:18:47,112-->00:18:54,513
Hãy nghĩ đó là bài tập thể dục
để nâng cao trí nhớ của anh đi Shidou.

368
00:18:55,512-->00:18:57,513
Thời gian khác nhau à...

368
00:18:58,512-->00:19:00,513
Vậy giờ anh làm gì đây?

368
00:19:01,512-->00:19:08,113
Giờ chúng ta hãy xem bên ngoài.
Anh ra ngoài và đi quanh nhà đi.

368
00:19:08,512-->00:19:14,513
Các sự kiện đã được thiết lập.
Giờ là lúc để kiểm tra chúng.

368
00:19:15,512-->00:19:18,113
Anh hiểu rồi. Anh đi ngay đây.

368
00:19:23,112-->00:19:25,113
Mình không thể tin rằng 
đây là trong trò chơi...

368
00:19:26,112-->00:19:31,113
Không, chắc chắn tôi không cảm thấy 
bất cứ điều gì lạ lẫm. Tất cả đều rất
quen thuộc. Tôi luôn nghĩ làm sao 
Ratatoskr có công nghệ thế này?

368
00:19:34,112-->00:19:36,113
Này Kotori, có rất nhiều người đi bộ. 
Đây cũng là những dữ liệu sao?

368
00:19:38,112-->00:19:48,113
Đúng vậy. Họ được tạo ra bởi trò chơi,
gọi là NPC (Non-player Character). 
Anh có thể nói chuyện với họ bình thường.

368
00:19:49,112-->00:19:51,113
Tuyệt thật...

368
00:19:52,112-->00:19:58,113
Vì không có dữ liệu về tất cả mọi người
ở Tengu nên những người đó không giống thật.

368
00:19:58,512-->00:20:04,113
Nhưng em nghĩ rằng ít nhất là hàng xóm 
đang được sao chép từ ký ức của anh.

368
00:20:05,112-->00:20:07,113
Nếu họ là hàng xóm mà anh biết... 
A, chào buổi chiều.

368
00:20:08,112-->00:20:11,113
Đó là người hàng xóm Saito-san
mà tôi vẫn chào hỏi như thường lệ.

368
00:20:12,112-->00:20:18,513
Ara, Shidou-kun nhà Itsuka. Đi mua đồ à? 
Vẫn suốt ngày làm việc nhà à?

368
00:20:19,512-->00:20:21,113
Không, không phải thế.

368
00:20:22,112-->00:20:24,113
Waa... Tuyệt thật. Giống y hệt Saitou-san.

368
00:20:28,112-->00:20:32,113
Tôi chợt nhớ rằng tôi muốn mua đồ cho bữa tối.
Vì vậy tôi đã đến khu mua sắm.

368
00:20:33,112-->00:20:36,113
Tất cả mọi thứ giống hệt như thật.
Như vậy là mình có thể mua đồ cho bữa tối.

368
00:20:37,113-->00:20:44,113
Anh nói mua đồ cho bữa ăn tối?
Anh định làm món gì vậy, Shidou?

368
00:20:45,112-->00:20:47,113
Waa! Tohka! Em đang liên lạc từ bên ngoài à?

368
00:20:48,113-->00:20:59,113
Ừ. Tất cả bọn em đang quan sát anh đấy, Shidou.
Anh ở đây có vẻ như đang ngủ, nhưng trên 
màn hình, anh đang đi bộ. Thật là kỳ lạ!

368
00:21:00,112-->00:21:05,113
Nghĩa là phòng quan sát truyền được
cả hình ảnh từ đây sao? Anh cảm thấy 
không thoải mái khi biết tất cả 
đang nhìn anh đâu.

368
00:21:06,112-->00:21:14,113
Hôm nay em muốn ăn menchi katsu! 
Shidou, mua menchi katsu đi! 
(Thịt viên giòn tan kiểu Nhật)

368
00:21:15,113-->00:21:17,513
Không, mua đồ ở đây làm sao mang về nhà được?

368
00:21:18,513-->00:21:21,113
Ủa? Vậy còn mùi vị khi ăn ở đây thì sao?

368
00:21:22,112-->00:21:26,113
Nếu mùi vị quen thuộc lặp đi lặp lại,
anh có thể ăn thoải mái. 

368
00:21:26,512-->00:21:32,113
Bộ não cũng sẽ có được cảm giác no 
và xác nhận là "anh đã ăn".

368
00:21:33,112-->00:21:40,513
Tất nhiên anh không thể hấp thụ 
chất dinh dưỡng vì dạ dày anh chẳng có gì.

368
00:21:41,512-->00:21:43,113
Ra là vậy...

368
00:21:44,112-->00:21:46,113
Vừa đi bộ vừa nói chuyện thế này...

368
00:21:47,112-->00:21:49,113
Uida. Tôi xin lỗi...

26
00:21:52,512-->00:21:59,113
Uida... A, Onii-chan!

26
00:22:00,112-->00:22:02,113
Ủa? Kotori? Em đang làm gì ở đây?

26
00:22:03,113-->00:22:11,113
Em đang vừa chạy vừa ngậm bánh mì.
Và sau đó, thật tình cờ, đụng trúng Onii-chan!

26
00:22:12,112-->00:22:15,113
Em đang nói về cái gì vậy?
Bộ đồ này... em đang đi học à?

26
00:22:16,512-->00:22:20,113
Đó chỉ là nhân vật trong game thôi.

26
00:22:21,112-->00:22:26,113
Vào buổi sáng... Em tự làm đồ ăn sáng à?
Đừng vừa chạy vừa ăn bánh mì.
Sẽ đau bao tử đấy.

26
00:22:27,112-->00:22:37,513
Xin lỗi nhé, Onii-chan.
Tuy nhiên, được gặp nhau thế này... Em vui lắm

26
00:22:38,512-->00:22:40,113
Ừ, Kotori, em không sao chứ?

26
00:22:41,112-->00:22:47,113
Vâng! Em không sao đâu!
Vậy chào anh, hẹn gặp lại nhé!

26
00:22:48,112-->00:22:50,113
Ừ... Chào em.

26
00:22:55,112-->00:22:58,113
Khoan đã Reine, vừa rồi là sao?

26
00:23:00,512-->00:23:04,113
Chẳng phải NPC Kotori sao?

27
00:23:05,112-->00:23:11,513
Tại sao nó lại như vậy?
Tôi có bao giờ nói chuyện với Shidou kiểu đó đâu!

27
00:23:13,112-->00:23:22,113
Đó là sự sao chép chính xác mà.
Trong buổi hẹn hò, Kotori cũng...

27
00:23:24,112-->00:23:31,113
Trong một số trường hợp, 
khả năng tái thiết lập
dường như là một lệnh còn sót lại...

368
00:23:33,512-->00:23:36,113
Vậy vừa rồi là chuyện gì vậy?

368
00:23:37,112-->00:23:43,113
Đó là sự kiện đấy. 
Đây là game hẹn hò mà, phải không?

368
00:23:44,112-->00:23:46,113
Sự kiện à?

368
00:23:47,112-->00:23:50,113
Đúng vậy, trong "My Little Shidou",
những sự kiện cũng hay xảy ra...

368
00:23:51,112-->00:23:53,513
Mặc dù có nhiều công nghệ cải tiến,
Nó vẫn theo quy luật thông thường.

368
00:23:54,112-->00:24:04,513
Dù sao đây chỉ là thử nghiệm. Khi anh sống 
trong thế giới trò chơi, sẽ có nhiều
sự kiện xuất hiện hơn.

368
00:24:05,512-->00:24:07,513
Chuyện đó khiến anh có cảm giác bất an.

368
00:24:08,513-->00:24:13,113
Im lặng đi. NPC cũng giống 
người thật phải không?

368
00:24:14,112-->00:24:18,113
Đúng vậy. Mặc dù có chút khác nhưng
họ hầu như giống hệt với người thật
dù sự kiện này đã được cài đặt trước.

368
00:24:19,112-->00:24:28,113
Để tạo ra các sự kiện, nhiều thiết lập 
được cài đặt khắp thế giới. Nhưng quan trọng 
vẫn là tính cách họ vẫn vậy phải không?

368
00:24:29,112-->00:24:32,113
Ừ, anh không cảm thấy gì lạ cả... 
Anh nghĩ vậy.

368
00:24:33,512-->00:24:44,113
Dù sao, chúng ta vẫn không thể nói 
nó hoàn hảo. Tạo ra cảm xúc, 
đặc biệt là tình yêu vô cùng khó khăn.

368
00:24:45,112-->00:24:49,513
Đó là vấn đề mà chúng ta sẽ làm.

368
00:24:50,512-->00:24:53,113
Giờ chúng ta đi học đi.

368
00:24:54,112-->00:24:56,113
Chúng ta vẫn tiếp tục à? Được rồi...

368
00:24:59,112-->00:25:03,513
Trường học cũng được sao chép sao? 
Dù biết đây là bản sao nhưng mình vẫn 
ngạc nhiên đây là trong trò chơi.

368
00:25:05,512-->00:25:13,113
Đây chỉ là một phần của Tengu.
Nó không nằm ngoài khu vực 
cậu nắm rõ đâu, Shin.

368
00:25:14,112-->00:25:16,113
Tất cả học sinh cũng vậy sao?

368
00:25:16,512-->00:25:22,113
Phải. Tất cả học sinh và giáo viên
trường Raizen đã được sao chép. 

368
00:25:22,512-->00:25:29,513
Tuy nhiên, những người mà cậu 
chưa bao giờ nói chuyện chỉ có hình ảnh thôi.

368
00:25:30,513-->00:25:32,513
Thật đáng kinh ngạc...
