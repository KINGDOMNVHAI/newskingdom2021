﻿0
00:00:00,512-->00:00:02,113
1) Hẹn hò với Tohka.
2) Hẹn hò với Origami.
3) Hẹn hò với chị em Yamai.
4) Hẹn hò với Miku.
5) Tiếp theo.

1
00:00:05,112-->00:00:07,513
Tớ sẽ chọn... Tohka

2
00:00:09,512-->00:00:16,113
Tại sao chứ, Shidou?
Sao cô ấy tốt hơn bọn tôi?

3
00:00:18,112-->00:00:25,113
Thắc mắc. Tại sao cậu lại chọn Tohka?
Bọn tớ cần lời giải thích.

5
00:00:26,112-->00:00:29,113
Vì tớ đã hứa sẽ dẫn cậu ấy 
đi mua bánh mì. Đúng không Tohka?

6
00:00:30,112-->00:00:38,113
Vậy là anh vẫn nhớ, Shidou!
Đúng là anh ấy đã hứa mua bánh mì cho tôi!

7
00:00:40,112-->00:00:53,113
Tohka-san tuyệt thật đấy.
Darling, anh cũng hứa sẽ đi tiệc trà với em đấy.
Vậy nên hãy hẹn hò với em đi.

8
00:00:54,512-->00:01:08,113
Tớ không ép cậu phải chọn tớ ngay bây giờ.
Không quan trọng 10 hay 20 năm nữa,
cậu vẫn phải chọn tớ.

8
00:01:09,112-->00:01:11,113
Tình hình vẫn không khá lên chút nào...

9
00:01:12,112-->00:01:15,113
Trong tình huống thế này... Đúng vậy!
Tôi chỉ muốn yêu cầu sự giúp đỡ từ Fraxinus.

10
00:01:16,112-->00:01:20,113
Khi tôi nói chuyện với các Tinh Linh,
tôi luôn được theo dõi và tư vấn. 
Tôi chắc chắn sẽ cầu cứu 
tình hình quan trọng này!

11
00:01:21,512-->00:01:25,513
Ara Shidou. Anh đang gặp vấn đề, phải không?

12
00:01:26,512-->00:01:29,113
Kotori, vấn đề vừa xảy ra đấy.

13
00:01:30,112-->00:01:34,113
Itsuka Kotori. Em gái tôi và cũng là
chỉ huy phi thuyền Fraxinus. 
Vào những lúc như thế này, nó là
người mà tôi có thể tin tưởng... Có lẽ vậy.

14
00:01:34,512-->00:01:36,113
Làm ơn... làm cái gì đó trong
tình hình này đi.

15
00:01:37,112-->00:01:52,113
Xem nào. Các cô gái, ngoại trừ 
Tobiichi Origami, mất kiểm soát 
sức mạnh nếu tâm lý không ổn định. 
Anh hiểu điều này, phải không?

16
00:01:53,112-->00:01:55,113
Anh biết. Và đó là lý do tại sao 
anh đang gặp rắc rối!

17
00:01:56,112-->00:02:01,113
Và trên hết, anh đã chọn 
Tohka phải không?

18
00:02:02,112-->00:02:07,112
Đúng vậy. Nếu chọn Tohka thì dễ xử lý hơn. 
Suy nghĩ có hơi tàn nhẫn nhưng...
Anh nghĩ đó là lựa chọn không tồi
trong tình thế hỗn loạn này. 

19
00:02:08,112-->00:02:13,512
Anh cũng nghĩ được đến thế à.
Cứ làm vậy đi. 

20
00:02:14,112-->00:02:20,512
Bọn em sẽ lo phần còn lại.
Trong mọi trường hợp, 
anh phải để ý cô ấy đó đấy.

21
00:02:21,512-->00:02:24,112
Anh hiểu rồi. Anh sẽ chăm sóc cô ấy.

22
00:02:25,112-->00:02:27,512
Nhanh lên, Tohka!

23
00:02:30,512-->00:02:33,113
Nhanh lên kẻo hết bánh đấy!

25
00:02:34,112-->00:02:36,513
Gì cơ?

25
00:02:40,512-->00:02:44,113
Và như vậy, tôi đã trốn thoát cùng Tohka. 
Chúng tôi hướng về khu mua sắm.

25
00:02:48,112-->00:02:53,113
Có nhiều bánh đậu nành quá!

25
00:02:54,112-->00:02:56,113
Phải. Em có muốn ăn hết chúng không?

25
00:02:57,113-->00:03:02,113
Gì cơ? Em sẽ được ăn hết sao?

26
00:03:03,112-->00:03:05,513
Phải. Nhưng nhớ phải về trước khi
trời tối để tránh đám đông đấy.

26
00:03:06,512-->00:03:16,113
Anh không muốn ăn sao?
Đó là lý do chúng ta đến trung tâm mua sắm mà.

26
00:03:17,112-->00:03:21,113
Không, chỉ là...
Người ta chỉ bán trong 2 giờ thôi.

26
00:03:22,112-->00:03:29,513
Thật vậy sao?
Vậy thì... chẳng lẽ?

26
00:03:30,512-->00:03:32,113
Hở?

26
00:03:32,512-->00:03:42,113
Đó là lý do anh chọn em? 
Em không biết anh hứa 
chúng ta đi trong ngày hôm nay đấy...

26
00:03:43,112-->00:03:46,113
Phải, chỉ là anh nghĩ
chúng ta nên đi ngày hôm nay.

26
00:03:47,112-->00:03:50,113
Là vì lời hứa sao?

26
00:03:51,112-->00:03:54,113
Còn phải hỏi sao?
Anh chọn em là vì... 
anh muốn hẹn hò với em mà.

26
00:03:55,112-->00:04:00,113
Ra là vậy à? 
Vâng, chỉ là em không biết thôi...

26
00:04:01,112-->00:04:04,514
Không... không phải vì lời hứa
hay vì rảnh rỗi đâu. Chỉ là đã lâu rồi
chúng ta mới được hẹn hò thế này.

26
00:04:07,514-->00:04:12,113
Từ trước mùa xuân, Tinh Linh là một khái niệm
hoàn toàn không tồn tại trong cuộc sống của anh.

26
00:04:13,112-->00:04:16,113
Tuy nhiên, nó đã thay đổi. 
Đây là nơi đầu tiên anh gặp và nói chuyện
với Tinh Linh. Và đó chính là em.

26
00:04:17,112-->00:04:23,113
Vâng! Lúc đó chị em Kaguya, Yoshino 
và Miku chưa xuất hiện nhỉ!

26
00:04:24,112-->00:04:28,113
Đúng vậy. Lần đầu tiên gặp nhau,
mọi thứ đều đổ nát. Bây giờ hoàn toàn
thay đổi 180 độ và không nghe ai nói gì cả.

26
00:04:29,112-->00:04:33,113
Đó là do lỗi của em...

26
00:04:34,112-->00:04:38,113
Sau đó AST đến. Và anh đã làm điều này. 
Anh nghĩ thế này rất khó khăn 
khi đứng trước kẻ thù.

26
00:04:39,113-->00:04:46,513
Nhưng anh thì không như thế.
Anh luôn xem em như một người bình thường.

26
00:04:47,513-->00:04:49,113
Tohka...

26
00:04:50,512-->00:04:54,113
Bánh đậu này chính là món ăn
đầu tiên anh mua cho em đấy!

26
00:04:55,112-->00:04:57,513
Ừ, phải rồi. Nó ngon không? Bánh đậu ấy.

26
00:04:58,512-->00:05:01,513
Vâng! Nó là món ngon nhất đấy!

26
00:05:02,512-->00:05:10,113
Anh là người đầu tiên cố gắng
gặp Tinh Linh mà không hề có ý thù địch đấy.

26
00:05:11,112-->00:05:15,113
Không, anh chỉ nghĩ em vẫn giống con người.
Sau những gì đã trải qua, 
em thật sự giống cô gái bình thường đấy.

26
00:05:16,112-->00:05:22,113
Em chỉ ... không có!

26
00:05:23,112-->00:05:26,113
Sao vậy? Bánh mì có gì à?

26
00:05:27,112-->00:05:33,113
Không, không, không có gì đâu.
Vâng... cái bánh này ngon lắm!

26
00:05:34,112-->00:05:36,113
Haha, hài thật đấy.

26
00:05:37,112-->00:05:43,113
Đừng trêu em, Shidou!
Anh cũng ăn một cái đi.

26
00:05:44,112-->00:05:46,113
Không, nhìn em ăn ngon lành thế kia...

26
00:05:49,512-->00:05:52,113
Không, không có gì đâu.

26
00:05:52,512-->00:05:59,113
Nhưng mà, Cảm ơn anh, Shidou.

26
00:06:00,112-->00:06:02,113
Haha, em không cần cảm ơn
vì anh mua bánh mì đâu.

26
00:06:03,112-->00:06:13,113
Không phải vậy! 
Em cảm ơn... vì chuyện khác cơ!

26
00:06:14,113-->00:06:16,113
Hở? Gì cơ?

26
00:06:17,113-->00:06:22,113
Không có gì đâu!
Chúng ta về nhà ăn tối thôi, Shidou!

26
00:06:23,112-->00:06:25,213
Được rồi. Đi về thôi.

26
00:06:26,112-->00:06:28,113
Vâng!

368
00:15:52,112-->00:15:55,113
Mình vừa ngủ dậy à?

368
00:15:56,112-->00:15:58,513
Mình đang ở nhà sao? Mình đang nằm mơ à?

368
00:16:02,512-->00:16:05,113
Có lẽ... đây là một giấc mơ kỳ lạ?

368
00:16:05,512-->00:16:08,513
Tôi có cảm giác rất kỳ lạ. 
Nhưng đây chắc chắn là nhà tôi.

368
00:16:09,112-->00:16:11,113
Giờ mình nên bắt đầu chuẩn bị bữa tối thôi.

368
00:16:13,112-->00:16:16,113
Anh đang nói những thứ ngớ ngẩn gì vậy?

368
00:16:17,112-->00:16:19,113
Hở? Kotori?

368
00:16:20,113-->00:16:22,113
Tôi nghe thấy giọng nói rõ ràng.
Nhưng tôi không thấy Kotori.

368
00:16:23,112-->00:16:26,113
Chuyện gì đang xảy ra vậy? 
Tiếng nói phát ra từ đâu? Và tại sao 
anh không đeo tai nghe liên lạc?

368
00:16:29,112-->00:16:31,513
Một khoảnh khắc im lặng. 
Bây giờ là tiếng nói của Kotori.

368
00:16:32,512-->00:16:41,113
Em đang nói chuyện trực tiếp với tâm trí anh.
Khi anh đang ở trong trò chơi, 
không cần phải dùng máy liên lạc.

368
00:16:42,112-->00:16:44,513
Trong trò chơi sao? Khoan đã!
Chẳng lẽ đây là... không gian ảo?

368
00:16:45,512-->00:16:48,513
Tôi nhanh chóng chạm vào một chiếc ghế,
cửa sổ và tường. Cảm giác vẫn như mọi khi.

368
00:16:49,513-->00:16:52,513
Tôi chạm vào mặt tôi. Phải, cả hai tay
và da mặt mình vẫn có cảm giác.

368
00:16:53,112-->00:16:56,113
Nếu đây là trò chơi thì chắc anh
sẽ không cảm thấy đau nếu tự véo má...

368
00:16:57,512-->00:17:00,113
Uida! Không! Anh vẫn thấy đau!

368
00:17:01,112-->00:17:08,113
Tất cả các giác quan đã kết nối.
Em đã nói với anh đây là một 
thế giới Siêu Giả Lập. 

368
00:17:08,512-->00:17:13,513
Cả anh và thế giới đều giống hệt với thực tế.

368
00:17:14,512-->00:17:17,113
Thế giới Siêu Giả Lập...

368
00:17:18,112-->00:17:29,113
Để chính xác, không phải tất cả các dữ liệu 
đều được chuyển đổi. Các đối tượng như ghế 
hoặc tường là từ ký ức của riêng anh, Shidou.

368
00:17:30,112-->00:17:32,113
Lấy hình ảnh từ ký ức của anh?

368
00:17:33,112-->00:17:42,113
Anh không cần quá quan tâm  
chi tiết đâu. Ồ, và cách liên lạc 
này hơi kém hơn bình thường đấy.

368
00:17:43,112-->00:17:45,113
Kém hơn à?

368
00:17:46,112-->00:17:54,513
Theo thời gian, sẽ có sự khác biệt 
về thời gian giữa thế giới ảo 
và thế giới thực.

368
00:17:55,512-->00:18:05,513
Thời gian trong thế giới ảo nhanh hơn. 
Một ngày trong đó bằng 
khoảng 20-30 phút ngoài thật.

368
00:18:06,512-->00:18:18,513
Vì vậy, khi một cuộc trò chuyện 
được thiết lập với thế giới thực, 
anh sẽ phải chỉnh lại thời gian đó.

368
00:18:19,512-->00:18:23,513
Nhưng bọn em không thể cho anh 
hướng dẫn chi tiết như chúng ta thường làm. 

368
00:18:24,112-->00:18:31,513
Hãy nghĩ đó là bài tập thể dục
để nâng cao trí nhớ của anh đi Shidou.

368
00:18:32,512-->00:18:35,113
Thời gian khác nhau à...

368
00:18:36,112-->00:18:38,113
Vậy giờ anh làm gì đây?

368
00:18:39,112-->00:18:45,513
Giờ chúng ta hãy xem bên ngoài.
Anh ra ngoài và đi quanh nhà đi.

368
00:18:46,112-->00:18:52,113
Các sự kiện đã được thiết lập.
Giờ là lúc để kiểm tra chúng.

368
00:18:53,112-->00:18:55,113
Anh hiểu rồi. Anh đi ngay đây.

368
00:19:00,512-->00:19:03,113
Mình không thể tin rằng 
đây là trong trò chơi...

368
00:19:04,112-->00:19:08,113
Không, chắc chắn tôi không cảm thấy 
bất cứ điều gì lạ lẫm. Tất cả đều rất
quen thuộc. Tôi luôn nghĩ làm sao 
Ratatoskr có công nghệ thế này?

368
00:19:11,112-->00:19:14,113
Này Kotori, có rất nhiều người đi bộ. 
Đây cũng là những dữ liệu sao?

368
00:19:15,512-->00:19:25,113
Đúng vậy. Họ được tạo ra bởi trò chơi,
gọi là NPC (Non-player Character). 
Anh có thể nói chuyện với họ bình thường.

368
00:19:26,112-->00:19:28,113
Tuyệt thật...

368
00:19:29,112-->00:19:35,513
Vì không có dữ liệu về tất cả mọi người
ở Tengu nên những người đó không giống thật.

368
00:19:36,112-->00:19:41,113
Nhưng em nghĩ rằng ít nhất là hàng xóm 
đang được sao chép từ ký ức của anh.

368
00:19:42,112-->00:19:45,113
Nếu họ là hàng xóm mà anh biết... 
A, chào buổi chiều.

368
00:19:46,112-->00:19:48,513
Đó là người hàng xóm Saito-san
mà tôi vẫn chào hỏi như thường lệ.

368
00:19:49,512-->00:19:55,513
Ara, Shidou-kun nhà Itsuka. Đi mua đồ à? 
Vẫn suốt ngày làm việc nhà à?

368
00:19:56,513-->00:19:58,513
Không, không phải thế.

368
00:19:59,113-->00:20:01,113
Waa... Tuyệt thật. Giống y hệt Saitou-san.

368
00:20:05,512-->00:20:08,513
Tôi chợt nhớ rằng tôi muốn mua đồ cho bữa tối.
Vì vậy tôi đã đến khu mua sắm.

368
00:20:09,512-->00:20:13,113
Tất cả mọi thứ giống hệt như thật.
Như vậy là mình có thể mua đồ cho bữa tối.

368
00:20:14,513-->00:20:21,513
Anh nói mua đồ cho bữa ăn tối?
Anh định làm món gì vậy, Shidou?

368
00:20:22,512-->00:20:25,113
Waa! Tohka! Em đang liên lạc từ bên ngoài à?

368
00:20:26,113-->00:20:36,113
Ừ. Tất cả bọn em đang quan sát anh đấy, Shidou.
Anh ở đây có vẻ như đang ngủ, nhưng trên 
màn hình, anh đang đi bộ. Thật là kỳ lạ!

368
00:20:37,112-->00:20:42,113
Nghĩa là phòng quan sát truyền được
cả hình ảnh từ đây sao? Anh cảm thấy 
không thoải mái khi biết tất cả 
đang nhìn anh đâu.

368
00:20:43,112-->00:20:51,113
Hôm nay em muốn ăn menchi katsu! 
Shidou, mua menchi katsu đi! 
(Thịt viên giòn tan kiểu Nhật)

368
00:20:52,113-->00:20:54,513
Không, mua đồ ở đây làm sao mang về nhà được?

368
00:20:55,513-->00:20:58,113
Ủa? Vậy còn mùi vị khi ăn ở đây thì sao?

368
00:21:00,112-->00:21:03,113
Nếu mùi vị quen thuộc lặp đi lặp lại,
anh có thể ăn thoải mái. 

368
00:21:03,512-->00:21:08,513
Bộ não cũng sẽ có được cảm giác no 
và xác nhận là "anh đã ăn".

368
00:21:09,512-->00:21:17,513
Tất nhiên anh không thể hấp thụ 
chất dinh dưỡng vì dạ dày anh chẳng có gì.

368
00:21:18,512-->00:21:20,113
Ra là vậy...

368
00:21:21,112-->00:21:23,513
Vừa đi bộ vừa nói chuyện thế này...

368
00:21:24,512-->00:21:27,113
Uida. Tôi xin lỗi...

26
00:21:29,112-->00:21:40,113
Xin lỗi, em không chú ý.

26
00:21:41,112-->00:21:44,113
Toh...ka? Xin lỗi... em không sao chứ?

26
00:21:45,112-->00:21:52,113
Ừ! Em đang sợ bị trễ nên 
vừa chạy vừa cắn bánh mì.
Xin lỗi vì đã không chú ý!

26
00:21:53,112-->00:21:56,113
Ừ. Anh cũng xin lỗi vì không để ý.
Em có đâu... ở đâu không?

26
00:21:57,112-->00:21:59,113
Không có!

26
00:22:00,112-->00:22:02,113
Gì cơ?

26
00:22:03,112-->00:22:13,113
Dù sao thì vừa chạy vừa ngậm bánh mì 
mà lại đụng trúng anh nữa, tình cờ quá nhỉ.
Đây giống như là định mệnh vậy!

26
00:22:14,112-->00:22:16,513
Anh không nghĩ em có thể nói thế đấy...

26
00:22:17,512-->00:22:23,113
Lần sau, hy vọng chúng ta sẽ có 
cuộc gặp định mệnh trong lớp học!

26
00:22:24,112-->00:22:27,113
Vậy gặp anh sau nhé, Shidou!

26
00:22:31,112-->00:22:33,113
Vừa rồi... là gì vậy?

27
00:22:34,112-->00:22:42,113
Tuyệt thật đấy, Kotori!
Tôi ở trong đó nói chuyện với Shidou kìa!

27
00:22:43,112-->00:22:52,113
Phải, thú vị đúng không? 
Mặc dù lập trình đúng theo dữ liệu,
nhưng tôi vẫn thấy có chút khác ngoài thật.

27
00:22:54,112-->00:23:04,113
Có chút khác biệt...
tôi cũng cảm thấy kỳ lạ.
Có vẻ cô ấy nói chuyện hơi khác tôi.

27
00:23:05,112-->00:23:07,113
Không, đó là... Phải, anh cũng thấy thế.

368
00:23:09,112-->00:23:11,113
Vậy vừa rồi là chuyện gì vậy?

368
00:23:13,112-->00:23:18,513
Đó là sự kiện đấy. 
Đây là game hẹn hò mà, phải không?

368
00:23:19,512-->00:23:21,113
Sự kiện à?

368
00:23:22,112-->00:23:25,113
Đúng vậy, trong "My Little Shidou",
những sự kiện cũng hay xảy ra...

368
00:23:26,112-->00:23:29,113
Mặc dù có nhiều công nghệ cải tiến,
Nó vẫn theo quy luật thông thường.

368
00:23:30,112-->00:23:39,113
Dù sao đây chỉ là thử nghiệm. Khi anh sống 
trong thế giới trò chơi, sẽ có nhiều
sự kiện xuất hiện hơn.

368
00:23:40,112-->00:23:42,113
Chuyện đó khiến anh có cảm giác bất an.

368
00:23:43,113-->00:23:48,113
Im lặng đi. NPC cũng giống 
người thật phải không?

368
00:23:49,112-->00:23:53,113
Đúng vậy. Mặc dù có chút khác nhưng
họ hầu như giống hệt với người thật
dù sự kiện này đã được cài đặt trước.

368
00:23:54,112-->00:24:03,513
Để tạo ra các sự kiện, nhiều thiết lập 
được cài đặt khắp thế giới. Nhưng quan trọng 
vẫn là tính cách họ vẫn vậy phải không?

368
00:24:04,512-->00:24:08,113
Ừ, anh không cảm thấy gì lạ cả... 
Anh nghĩ vậy.

368
00:24:09,112-->00:24:19,113
Dù sao, chúng ta vẫn không thể nói 
nó hoàn hảo. Tạo ra cảm xúc, 
đặc biệt là tình yêu vô cùng khó khăn.

368
00:24:20,112-->00:24:23,513
Đó là vấn đề mà chúng ta sẽ làm.

368
00:24:24,512-->00:24:28,113
Giờ chúng ta đi học đi.

368
00:24:29,112-->00:24:32,113
Chúng ta vẫn tiếp tục à? Được rồi...

368
00:24:35,112-->00:24:39,513
Trường học cũng được sao chép sao? 
Dù biết đây là bản sao nhưng mình vẫn 
ngạc nhiên đây là trong trò chơi.

368
00:24:40,512-->00:24:48,113
Đây chỉ là một phần của Tengu.
Nó không nằm ngoài khu vực 
cậu nắm rõ đâu, Shin.

368
00:24:49,112-->00:24:51,113
Tất cả học sinh cũng vậy sao?

368
00:24:52,112-->00:24:58,113
Phải. Tất cả học sinh và giáo viên
trường Raizen đã được sao chép. 

368
00:24:58,512-->00:25:04,513
Tuy nhiên, những người mà cậu 
chưa bao giờ nói chuyện chỉ có hình ảnh thôi.

368
00:25:05,513-->00:25:08,113
Thật đáng kinh ngạc...
