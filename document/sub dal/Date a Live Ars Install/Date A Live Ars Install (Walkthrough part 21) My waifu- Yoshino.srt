﻿0
00:00:00,512-->00:00:02,113
1) Kiếm chỗ vắng vẻ và gọi Kotori.
2) Bỏ chạy thật xa.
3) Chạy vào nhà.
4) Trở lại.
5) Tiếp theo.

1
00:00:06,112-->00:00:09,113
Nào, darling. Hẹn hò với em đi

2
00:00:12,512-->00:00:20,113
Không, Shidou, cậu phải đi cùng bọn này.
Cậu thuộc về Yamai bọn tôi.

3
00:00:23,112-->00:00:28,513
Đề nghị. Shidou, cậu phải đi với tớ.

5
00:00:30,112-->00:00:37,113
Shidou! Em chờ lâu lắm rồi!
Nhanh đi hẹn hò với em đi!

6
00:00:38,512-->00:00:48,513
Shidou. Tớ cũng chờ lâu lắm rồi.
Cậu phải từ chối các cô gái khác và đi với tớ.

7
00:00:49,512-->00:00:52,113
Tớ không biết phải chọn ai cả.
Tại sao lúc nào cũng phải thế này?

8
00:00:53,113-->00:00:55,113
Giờ chỉ còn một cách duy nhất!

8
00:00:56,112-->00:00:58,113
Aaaaaaa! Tha cho tớ đi mà!

9
00:01:00,112-->00:01:05,113
Anh định đi đâu thế, darling?

10
00:01:06,112-->00:01:08,113
Xin lỗi Miku. Và cả mọi người nữa. 
Giờ tớ phải về nhà rồi!

11
00:01:10,512-->00:01:15,113
Việc quan trọng hơn tớ sao? Là gì vậy?

12
00:01:16,112-->00:01:19,113
Origami, xin lỗi! 
Dù sao, xin lỗi các cậu!
Tớ phải đi bây giờ!

13
00:01:21,112-->00:01:26,113
Shidou! Cậu phải đi với bọn tôi!

14
00:01:28,512-->00:01:34,113
Nhận định. Shidou chạy mất rồi.

15
00:01:35,112-->00:01:38,113
Khoan đã, Shidou!

16
00:01:40,112-->00:01:43,113
Phù... phù...
Cuối cùng cũng về nhà an toàn.

17
00:01:47,112-->00:01:49,513
Ngày nào cũng kiệt sức... về nhà thế này...

18
00:01:51,112-->00:01:55,112
Shidou-san...

19
00:01:56,112-->00:01:58,112
Hở, Yoshino? Em làm gì trong nhà anh vậy?

20
00:01:59,112-->00:02:02,112
Yoshino, một cô bé với con rối bên tay trái.
Tôi đã gặp cô bé sau khi gặp Tohka.

21
00:02:03,112-->00:02:07,512
Cô bé sống trong khu nhà 
của các Tinh Linh do Ratatoskr xây dựng.
Vì vậy, đôi khi cô bé qua nhà tôi.

22
00:02:08,512-->00:02:11,112
Yoshino, em làm gì ở đây?

23
00:02:19,112-->00:02:21,113
Này, tại sao em khóc?
Chuyện gì xảy ra vậy?

25
00:02:22,112-->00:02:30,113
Shidou... Shidou... Shidou-san

25
00:02:31,112-->00:02:33,113
Tôi không nghe rõ em ấy nói gì...
Những lúc thế này...

25
00:02:34,112-->00:02:36,113
Yoshinon, chuyện gì xảy ra vậy?

25
00:02:37,112-->00:02:41,513
Có vấn đề lớn đấy, Shidou-kun.

25
00:02:42,512-->00:02:45,113
Đúng như dự đoán, con rối trên tay trái
của cô bé lập tức lên tiếng.

25
00:02:46,112-->00:02:50,513
Yoshinon là một "nhân cách khác của Yoshino", 
thường nói thay cô bé khi cô bé 
khó nói chuyện với một ai đó.
Trái với Yoshino, Yoshinon có tính cách sôi nổi.

25
00:02:51,512-->00:02:54,113
Vậy chuyện gì đã xảy ra?
Em lại bị đánh rơi à?

25
00:02:55,112-->00:03:04,113
Nghiêm trọng lắm, Shidou-kun!
Nakamura... ông Nakamura đã chết trong một tai nạn!

26
00:03:05,112-->00:03:07,113
Hở? Ai cơ?

26
00:03:08,112-->00:03:15,113
Trên... trên TV ạ.

26
00:03:16,112-->00:03:18,113
TV à?

26
00:03:19,112-->00:03:24,513
Đúng vậy. Anh hùng Yiragu 
của bộ phim truyền hình "Band of Rama"

26
00:03:25,112-->00:03:28,113
Hóa ra là... phim tình cảm à?

26
00:03:35,512-->00:03:37,513
Thôi nào Yoshino. 
Không việc gì phải khóc đâu

26
00:03:38,112-->00:03:40,113
Anh nói gì vậy Shidou-kun?

26
00:03:40,512-->00:03:50,113
Nakamura không phải là 
bạn thân của ông Hanako sao?
Mất một người bạn thân... thật đánh buồn!
(Như Yoshino và Yoshinon)

26
00:03:51,112-->00:03:54,113
Không, ý anh là...
Lúc đầu anh không biết là trong phim.

26
00:03:55,112-->00:04:00,113
Ông Naka... mura... san

26
00:04:01,112-->00:04:04,113
Này, thôi nào. Đừng khóc nữa, Yoshino.
Nếu cứ khóc thế này...

26
00:04:10,512-->00:04:12,113
Rắc rối thật đấy.

26
00:04:13,112-->00:04:17,513
Em biết rồi, Shidou-kun.

26
00:04:18,512-->00:04:21,113
Ý em là gì, Yoshino?

26
00:04:22,112-->00:04:32,113
Yoshino giờ đang rất buồn phải không?
Em nên cố gắng an ủi Yoshino tốt 
như khi chúng ta ở cùng nhau phải không?

26
00:04:33,112-->00:04:35,114
Như khi chúng ta ở cùng nhau sao?

26
00:04:36,114-->00:04:38,513
Im lặng! (Silence!)

26
00:04:39,512-->00:04:41,113
Gì cơ?

26
00:04:42,112-->00:04:52,113
Hoàn toàn sai, Shidou-kun!
Em không có hy vọng an ủi Yoshino.
Không như anh, Shidou-kun!

26
00:04:59,112-->00:05:04,113
Ồ, phải rồi, Yoshino. 
Giờ chúng ta vào phòng, uống trà 
và anh sẽ nghe em cho đến khi 
em bình tĩnh lại nhé.

26
00:05:05,112-->00:05:13,113
Shidou-san... cảm ơn anh.

26
00:05:15,112-->00:05:17,113
Nghe này...

26
00:05:19,113-->00:05:22,113
Chạy thoát khỏi những người khác,
cuối cùng tôi cũng dính vào Yoshino...

26
00:05:23,113-->00:05:26,113
Yoshino lấy lại ổn định 
một cách nhanh chóng sau đó.

368
00:14:49,512-->00:14:51,113
Mình vừa ngủ dậy à?

368
00:14:52,112-->00:14:55,113
Mình đang ở nhà sao? Mình đang nằm mơ à?

368
00:14:59,112-->00:15:01,513
Có lẽ... đây là một giấc mơ kỳ lạ?

368
00:15:02,512-->00:15:05,113
Tôi có cảm giác rất kỳ lạ. 
Nhưng đây chắc chắn là nhà tôi.

368
00:15:06,112-->00:15:08,113
Giờ mình nên bắt đầu chuẩn bị bữa tối thôi.

368
00:15:10,112-->00:15:13,513
Anh đang nói những thứ ngớ ngẩn gì vậy?

368
00:15:14,112-->00:15:16,113
Hở? Kotori?

368
00:15:16,512-->00:15:19,113
Tôi nghe thấy giọng nói rõ ràng.
Nhưng tôi không thấy Kotori.

368
00:15:20,112-->00:15:22,113
Chuyện gì đang xảy ra vậy? 
Tiếng nói phát ra từ đâu? Và tại sao 
anh không đeo tai nghe liên lạc?

368
00:15:26,112-->00:15:28,113
Một khoảnh khắc im lặng. 
Bây giờ là tiếng nói của Kotori.

368
00:15:29,112-->00:15:37,113
Em đang nói chuyện trực tiếp với tâm trí anh.
Khi anh đang ở trong trò chơi, 
không cần phải dùng máy liên lạc.

368
00:15:38,112-->00:15:41,113
Trong trò chơi sao? Khoan đã!
Chẳng lẽ đây là... không gian ảo?

368
00:15:42,112-->00:15:45,113
Tôi nhanh chóng chạm vào một chiếc ghế,
cửa sổ và tường. Cảm giác vẫn như mọi khi.

368
00:15:46,112-->00:15:49,113
Tôi chạm vào mặt tôi. Phải, cả hai tay
và da mặt mình vẫn có cảm giác.

368
00:15:50,112-->00:15:53,113
Nếu đây là trò chơi thì chắc anh
sẽ không cảm thấy đau nếu tự véo má...

368
00:15:54,512-->00:15:56,513
Uida! Không! Anh vẫn thấy đau!

368
00:15:57,112-->00:16:04,513
Tất cả các giác quan đã kết nối.
Em đã nói với anh đây là một 
thế giới Siêu Giả Lập. 

368
00:16:05,112-->00:16:10,113
Cả anh và thế giới đều giống hệt với thực tế.

368
00:16:11,112-->00:16:13,513
Thế giới Siêu Giả Lập...

368
00:16:14,512-->00:16:26,113
Để chính xác, không phải tất cả các dữ liệu 
đều được chuyển đổi. Các đối tượng như ghế 
hoặc tường là từ ký ức của riêng anh, Shidou.

368
00:16:27,112-->00:16:29,113
Lấy hình ảnh từ ký ức của anh?

368
00:16:30,112-->00:16:38,113
Anh không cần quá quan tâm  
chi tiết đâu. Ồ, và cách liên lạc 
này hơi kém hơn bình thường đấy.

368
00:16:39,112-->00:16:41,113
Kém hơn à?

368
00:16:42,112-->00:16:51,113
Theo thời gian, sẽ có sự khác biệt 
về thời gian giữa thế giới ảo 
và thế giới thực.

368
00:16:52,112-->00:17:02,113
Thời gian trong thế giới ảo nhanh hơn. 
Một ngày trong đó bằng 
khoảng 20-30 phút ngoài thật.

368
00:17:03,112-->00:17:15,113
Vì vậy, khi một cuộc trò chuyện 
được thiết lập với thế giới thực, 
anh sẽ phải chỉnh lại thời gian đó.

368
00:17:16,112-->00:17:19,513
Nhưng bọn em không thể cho anh 
hướng dẫn chi tiết như chúng ta thường làm. 

368
00:17:20,112-->00:17:27,513
Hãy nghĩ đó là bài tập thể dục
để nâng cao trí nhớ của anh đi Shidou.

368
00:17:28,512-->00:17:31,513
Thời gian khác nhau à...

368
00:17:32,512-->00:17:34,513
Vậy giờ anh làm gì đây?

368
00:17:35,512-->00:17:42,113
Giờ chúng ta hãy xem bên ngoài.
Anh ra ngoài và đi quanh nhà đi.

368
00:17:42,512-->00:17:47,513
Các sự kiện đã được thiết lập.
Giờ là lúc để kiểm tra chúng.

368
00:17:48,512-->00:17:51,113
Anh hiểu rồi. Anh đi ngay đây.

368
00:17:56,512-->00:17:59,113
Mình không thể tin rằng 
đây là trong trò chơi...

368
00:18:00,112-->00:18:04,113
Không, chắc chắn tôi không cảm thấy 
bất cứ điều gì lạ lẫm. Tất cả đều rất
quen thuộc. Tôi luôn nghĩ làm sao 
Ratatoskr có công nghệ thế này?

368
00:18:07,112-->00:18:10,113
Này Kotori, có rất nhiều người đi bộ. 
Đây cũng là những dữ liệu sao?

368
00:18:12,112-->00:18:21,113
Đúng vậy. Họ được tạo ra bởi trò chơi,
gọi là NPC (Non-player Character). 
Anh có thể nói chuyện với họ bình thường.

368
00:18:22,512-->00:18:24,513
Tuyệt thật...

368
00:18:25,112-->00:18:32,113
Vì không có dữ liệu về tất cả mọi người
ở Tengu nên những người đó không giống thật.

368
00:18:32,512-->00:18:37,113
Nhưng em nghĩ rằng ít nhất là hàng xóm 
đang được sao chép từ ký ức của anh.

368
00:18:38,112-->00:18:41,113
Nếu họ là hàng xóm mà anh biết... 
A, chào buổi chiều.

368
00:18:42,112-->00:18:44,513
Đó là người hàng xóm Saito-san
mà tôi vẫn chào hỏi như thường lệ.

368
00:18:45,512-->00:18:51,513
Ara, Shidou-kun nhà Itsuka. Đi mua đồ à? 
Vẫn suốt ngày làm việc nhà à?

368
00:18:52,512-->00:18:54,113
Không, không phải thế.

368
00:18:55,112-->00:18:58,113
Waa... Tuyệt thật. Giống y hệt Saitou-san.

368
00:19:01,512-->00:19:05,113
Tôi chợt nhớ rằng tôi muốn mua đồ cho bữa tối.
Vì vậy tôi đã đến khu mua sắm.

368
00:19:06,112-->00:19:09,113
Tất cả mọi thứ giống hệt như thật.
Như vậy là mình có thể mua đồ cho bữa tối.

368
00:19:11,113-->00:19:17,113
Anh nói mua đồ cho bữa ăn tối?
Anh định làm món gì vậy, Shidou?

368
00:19:18,112-->00:19:21,113
Waa! Tohka! Em đang liên lạc từ bên ngoài à?

368
00:19:22,113-->00:19:32,113
Ừ. Tất cả bọn em đang quan sát anh đấy, Shidou.
Anh ở đây có vẻ như đang ngủ, nhưng trên 
màn hình, anh đang đi bộ. Thật là kỳ lạ!

368
00:19:33,112-->00:19:38,113
Nghĩa là phòng quan sát truyền được
cả hình ảnh từ đây sao? Anh cảm thấy 
không thoải mái khi biết tất cả 
đang nhìn anh đâu.

368
00:19:39,112-->00:19:47,113
Hôm nay em muốn ăn menchi katsu! 
Shidou, mua menchi katsu đi! 
(Thịt viên giòn tan kiểu Nhật)

368
00:19:48,113-->00:19:51,113
Không, mua đồ ở đây làm sao mang về nhà được?

368
00:19:52,113-->00:19:54,113
Ủa? Vậy còn mùi vị khi ăn ở đây thì sao?

368
00:19:55,112-->00:20:00,113
Nếu mùi vị quen thuộc lặp đi lặp lại,
anh có thể ăn thoải mái. 

368
00:20:00,512-->00:20:05,113
Bộ não cũng sẽ có được cảm giác no 
và xác nhận là "anh đã ăn".

368
00:20:06,112-->00:20:14,513
Tất nhiên anh không thể hấp thụ 
chất dinh dưỡng vì dạ dày anh chẳng có gì.

368
00:20:15,512-->00:20:17,113
Ra là vậy...

368
00:20:17,512-->00:20:20,113
Vừa đi bộ vừa nói chuyện thế này...

368
00:20:21,112-->00:20:23,113
Uida. Tôi xin lỗi...

26
00:20:24,512-->00:20:31,513
Em xin lỗi... Em chạy vội quá...

26
00:20:32,512-->00:20:35,113
Yoshino? Có chuyện gì thế?
Em làm gì ở đây?

26
00:20:36,112-->00:20:47,113
Em vừa chạy... vừa ngậm... bánh mì...

26
00:20:48,112-->00:20:51,513
Bánh mì à? Phải rồi.
Nhưng em đi đâu mà vội thế...

26
00:20:52,512-->00:21:04,513
Shidou-kun! Đó chẳng phải điều tốt sao?
Anh không cảm thấy gì à?
Này, có phải anh đang đi bộ không?

26
00:21:05,513-->00:21:08,113
Hở? Ý em là sao?

26
00:21:09,112-->00:21:13,513
Ngốc quá! Ngốc quá, Shidou-kun!

26
00:21:14,112-->00:21:20,113
Một cô gái vừa chạy vừa ngậm bánh mì!
Giữa dòng người tấp nập, 
2 người vô tình va vào nhau!

26
00:21:21,112-->00:21:30,113
Em... em xin lỗi.
Em... vội thế... bởi vì ...

26
00:21:31,112-->00:21:42,113
Không phải đâu! Yoshino còn không biết nữa mà!
Vậy là xong nhé, Yoshino, cô gái trong manga!

26
00:21:43,112-->00:21:50,113
Khoan đã... Yoshinon? Tớ là gì cơ?

26
00:21:51,112-->00:22:04,513
Đây là cảnh bắt đầu của tình yêu!
Boom! Tình cảm phát sinh và theo dõi đối thủ!
Tình yêu của Yoshino là bất khả chiến bại!
Shidou-kun cũng vậy đấy!

26
00:22:05,512-->00:22:13,113
Yoshinon... đừng mà... tớ sợ lắm...

27
00:22:14,112-->00:22:20,113
Định mệnh đấy! 
Đây chính là cuộc gặp gỡ định mệnh!
Shidou-kun không nghĩ thế sao?

27
00:22:21,112-->00:22:23,113
Không, đây đâu phải lần đầu
chúng ta gặp nhau đâu...

27
00:22:24,112-->00:22:36,513
Thôi được rồi! Một người quen
thông qua một cuộc gặp gỡ định mệnh.
Chắc chắn người phụ nữ đó
phải có ý nghĩa nào đó rồi.

27
00:22:37,512-->00:22:46,113
Phụ nữ... ý nghĩa. Không có đâu...

27
00:22:47,112-->00:22:56,113
Có đấy! Thử nhìn xem, Shidou-kun có thể 
va vào cả ngàn người khác. Vậy mà lại là cậu.
Đây thực sự là khởi đầu tốt đấy!

27
00:22:57,112-->00:23:00,113
Khởi đầu gì chứ...
Yoshino đừng chạy như thế, nguy hiểm đấy.

27
00:23:01,112-->00:23:08,113
Vâng... cảm ơn anh nhiều.

27
00:23:09,112-->00:23:13,113
Đừng vừa ăn bánh mì vừa chạy.
Cứ ăn xong hết đi rồi tính.

27
00:23:14,112-->00:23:20,113
Vâng... Xin lỗi...

27
00:23:21,112-->00:23:33,113
Gì thế, gì thế? Mặt Yoshino đang đỏ lên kìa!
Cậu ấy đang muốn có một cuộc gặp
định mệnh khác nữa đấy!

27
00:23:34,112-->00:23:45,113
Ừ...! Yoshinon, đi thôi. 
Shidou-san, em xin lỗi.

27
00:23:46,112-->00:23:48,113
Không, không sao. Anh không để ý đâu.

27
368
00:23:51,512-->00:23:54,113
Vừa rồi là chuyện gì vậy?

368
00:23:55,512-->00:24:01,113
Đó là sự kiện đấy. 
Đây là game hẹn hò mà, phải không?

368
00:24:02,112-->00:24:04,113
Sự kiện à?

368
00:24:05,112-->00:24:08,113
Đúng vậy, trong "My Little Shidou",
những sự kiện cũng hay xảy ra...

368
00:24:09,112-->00:24:11,113
Mặc dù có nhiều công nghệ cải tiến,
Nó vẫn theo quy luật thông thường.

368
00:24:12,112-->00:24:21,513
Dù sao đây chỉ là thử nghiệm. 
Khi anh sống trong thế giới trò chơi, 
sẽ có nhiều sự kiện xuất hiện hơn.

368
00:24:22,512-->00:24:24,513
Chuyện đó khiến anh có cảm giác bất an.

368
00:24:25,513-->00:24:31,113
Im lặng đi. NPC cũng giống 
người thật phải không?

368
00:24:32,112-->00:24:36,513
Đúng vậy. Mặc dù có chút khác nhưng
họ hầu như giống hệt với người thật
dù sự kiện này đã được cài đặt trước.

368
00:24:37,512-->00:24:47,113
Để tạo ra các sự kiện, nhiều thiết lập 
được cài đặt khắp thế giới. Nhưng quan trọng 
vẫn là tính cách họ vẫn vậy phải không?

368
00:24:48,112-->00:24:50,113
Ừ, anh không cảm thấy gì lạ cả... 
Anh nghĩ vậy.

368
00:24:52,112-->00:25:02,113
Dù sao, chúng ta vẫn không thể nói 
nó hoàn hảo. Tạo ra cảm xúc, 
đặc biệt là tình yêu vô cùng khó khăn.

368
00:25:04,112-->00:25:07,113
Đó là vấn đề mà chúng ta sẽ làm.

368
00:25:08,112-->00:25:11,113
Giờ chúng ta đi học đi.

368
00:25:12,112-->00:25:14,113
Chúng ta vẫn tiếp tục à? Được rồi...

368
00:25:17,112-->00:25:22,113
Trường học cũng được sao chép sao? 
Dù biết đây là bản sao nhưng mình vẫn 
ngạc nhiên đây là trong trò chơi.

368
00:25:23,512-->00:25:31,113
Đây chỉ là một phần của Tengu.
Nó không nằm ngoài khu vực 
cậu nắm rõ đâu, Shin.

368
00:25:32,112-->00:25:34,113
Tất cả học sinh cũng vậy sao?

368
00:25:35,112-->00:25:41,113
Phải. Tất cả học sinh và giáo viên
trường Raizen đã được sao chép. 

368
00:25:42,112-->00:25:47,113
Tuy nhiên, những người mà cậu 
chưa bao giờ nói chuyện chỉ có hình ảnh thôi.

368
00:25:48,513-->00:25:50,113
Thật đáng kinh ngạc...
