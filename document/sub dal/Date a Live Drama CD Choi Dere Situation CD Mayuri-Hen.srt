﻿0
00:00:05,112-->00:00:06,113
Này Mayuri, 

0
00:00:07,112-->00:00:09,113
em không thấy đây là ý tồi sao?

1
00:00:09,312-->00:00:11,113
Chúng ta nên rời khỏi đây 
trước khi họ phát hiện...

2
00:00:11,312-->00:00:15,113
Anh đã hứa hôm nay sẽ đi cùng em
đến mọi nơi em muốn phải không?

3
00:00:15,312-->00:00:17,513
Anh nói dối sao, Shidou?

4
00:00:18,112-->00:00:19,513
Anh không nói dối, nhưng... 

4
00:00:20,112-->00:00:23,513
Anh không nghĩ em muốn đến trường vào ban đêm.

5
00:00:24,112-->00:00:25,513
Không sao đâu.

6
00:00:26,112-->00:00:29,513
Em rất muốn biết về nơi 
anh thường xuyên đến trông như thế nào.

7
00:00:30,112-->00:00:31,513
Nhưng mà...

8
00:00:31,912-->00:00:32,513
Không sao đâu. 

8
00:00:33,112-->00:00:36,113
Nếu họ phát hiện thì xin lỗi thôi.
Nhưng anh sẽ phải xin lỗi một mình.

9
00:00:36,312-->00:00:41,113
Tại sao chứ? Em phải xin lỗi cùng anh chứ!

10
00:00:42,112-->00:00:43,113
Có ánh sáng kìa.

11
00:00:44,112-->00:00:45,113
Chờ đã Mayuri!

12
00:00:46,112-->00:00:48,113
Shidou, đi nhanh lên.

12
00:00:50,112-->00:00:51,113
Đúng thật là...

13
00:00:53,512-->00:00:57,513
Bánh mì Yakisoba, bánh mì dứa, bánh mì kem.

13
00:00:59,112-->00:01:01,513
Anh luôn mua bữa trưa ở đây à Shidou? 

14
00:01:02,112-->00:01:04,113
Không. Anh thường mang bento đến.

15
00:01:04,512-->00:01:08,113
À phải rồi. Anh nấu ăn giỏi mà.

16
00:01:09,112-->00:01:12,113
Bánh mì sầu riêng là gì vậy?

17
00:01:12,512-->00:01:14,513
À, đó là cái mà anh đã giới thiệu đấy.

18
00:01:15,113-->00:01:16,913
Anh nói dối.

19
00:01:17,112-->00:01:19,913
Sao thế? Nó rất ngon mà.

20
00:01:20,112-->00:01:21,513
Thật à? 

20
00:01:22,112-->00:01:24,113
Vậy mai mốt mua cho em một cái đi.

21
00:01:24,312-->00:01:29,113
Nếu nó dở tệ, anh phải quỳ úp mặt
xuống đất để xin lỗi đấy.

22
00:01:29,312-->00:01:31,113
Em định bắt ép anh đấy à?

23
00:01:32,512-->00:01:35,513
Em đùa đấy. Nhưng để xem...

24
00:01:36,112-->00:01:41,113
Để xin lỗi, anh phải mời em
đến nhà anh ăn món anh làm.

25
00:01:42,112-->00:01:44,513
Nếu vậy thì anh có thể nấu bất cứ khi nào em muốn.

25
00:01:45,112-->00:01:46,313
Thật vậy sao?

25
00:01:46,512-->00:01:49,113
Thật vậy đấy. Em muốn ăn gì?

25
00:01:52,112-->00:01:53,513
Chắc là bento.

25
00:01:54,112-->00:01:55,913
Bento à?

25
00:01:56,112-->00:02:03,113
Phải. Em muốn ăn bento anh thường ăn ở trường.

25
00:02:05,512-->00:02:09,513
Hiểu rồi. Vậy anh sẽ làm thật nhiều cho em.

25
00:02:10,112-->00:02:12,113
Đừng quên hamburgers đấy nhé.

25
00:02:12,312-->00:02:13,513
Tất nhiên rồi.



25
00:02:17,912-->00:02:19,513
Đây là phòng học nhạc.

25
00:02:21,112-->00:02:23,513
Có nhiều nhạc cụ quá nhỉ.

25
00:02:25,112-->00:02:27,113
Anh có biết chơi nhạc cụ nào không?

25
00:02:28,112-->00:02:29,513
Anh biết chơi guitar một chút.

25
00:02:29,812-->00:02:31,513
Có lần anh đã luyện tập 
để tham gia lễ hội truyền thống.

25
00:02:34,112-->00:02:36,113
Anh chơi thử đi.

25
00:02:36,512-->00:02:40,113
Chơi ở đây sao? Bảo vệ sẽ đến đây đấy.

25
00:02:42,112-->00:02:44,113
Anh chơi cho mọi người xem ở lễ hội.

25
00:02:44,312-->00:02:45,913
Vậy mà anh không chơi cho em xem sao?

25
00:02:46,112-->00:02:48,113
Không phải thế. 

25
00:02:49,112-->00:02:50,913
Anh sẽ chơi vào dịp khác cho em xem.

26
00:02:51,112-->00:02:52,113
Thật vậy sao? 

26
00:02:52,512-->00:02:57,113
Vậy anh có thể mặc đồng phục,  
chơi trước mặt mọi người được không?

26
00:02:59,112-->00:03:01,113
Cái đó hơi khó đấy.

26
00:03:01,512-->00:03:04,113
Sao thế? Có vấn đề gì à?

26
00:03:05,112-->00:03:08,913
Đừng nói là anh sẽ không cho em xem đấy nhé.

26
00:03:09,113-->00:03:10,913
Không phải. Có nhiều chuyện lắm.

26
00:03:11,112-->00:03:17,113
Và ở lễ hội, anh đã phải trở thành Shiori. 

26
00:03:18,112-->00:03:21,113
Và nhiều chuyện nữa.

26
00:03:21,312-->00:03:24,113
Sao nữa? Kể tiếp đi.

26
00:03:26,512-->00:03:31,113
Đồng phục của anh là... đồ hầu gái.

26
00:03:31,512-->00:03:32,513
Đồ hầu gái sao?

26
00:03:33,112-->00:03:37,513
Phải. Ở lễ hội, anh làm việc trong
Maid Café nên anh phải mặc như vậy.

26
00:03:38,112-->00:03:39,113
Em cũng muốn xem.

27
00:03:40,512-->00:03:44,113
Quyết định rồi. Cuộc hẹn kế tiếp sẽ là
buổi âm nhạc đường phố 

28
00:03:44,312-->00:03:46,513
do Shiori-chan mặc đồ hầu gái 
đứng ở đối diện nhà ga biểu diễn. 

29
00:03:47,112-->00:03:50,513
Em sẽ ngồi xem và ăn bento.

30
00:03:51,112-->00:03:52,513
Em bảo anh biểu diễn nơi công cộng sao?

31
00:03:53,112-->00:03:54,513
Anh không bao giờ làm thế đâu!

32
00:03:55,112-->00:03:56,513
Anh lúc nào cũng nói thế.

33
00:03:56,712-->00:03:59,113
Nhưng cuối cùng anh luôn đồng ý làm.

33
00:04:00,112-->00:04:01,113
Em rất mong đợi anh biểu diễn đấy.

34
00:04:02,112-->00:04:04,113
Tha cho anh đi.



35
00:04:08,112-->00:04:11,913
Vẫn còn những nơi em chưa đến.
Phòng thể dục, phòng y tế...

36
00:04:12,112-->00:04:17,113
Này Mayuri, giờ về được chưa? Trễ lắm rồi đấy.

37
00:04:18,112-->00:04:19,113
Anh nói đúng. 

37
00:04:20,112-->00:04:23,113
Nhưng em muốn đến một nơi cuối cùng.

38
00:04:24,112-->00:04:25,113
Chúng ta đi chứ?

39
00:04:26,512-->00:04:29,113
Được rồi. Lần cuối thôi đấy.

40
00:04:30,113-->00:04:32,513
Vậy em muốn đi đâu?

41
00:04:35,113-->00:04:37,113
Phòng học của anh, Shidou.

42
00:04:40,213-->00:04:42,513
Em thấy phòng học của anh thế nào?

43
00:04:46,112-->00:04:48,913
Trông cũng bình thường nhỉ.

39
00:04:49,112-->00:04:52,513
Dĩ nhiên. Chỉ là phòng bình thường thôi.

40
00:04:53,213-->00:04:57,113
Đúng vậy. Nhưng em vẫn muốn đến. 

41
00:04:57,813-->00:05:00,513
Dù sao đó là phòng anh hay đến nhất.

42
00:05:02,213-->00:05:07,113
Em ghen tị đấy. Em ước gì có thể học chung với anh.

43
00:05:09,112-->00:05:14,513
Em tưởng tượng nếu chúng ta học cùng lớp.

39
00:05:15,112-->00:05:20,113
Đầu tiên, chúng ta sẽ gặp nhau 
mỗi sáng và đến trường cùng nhau.

40
00:05:21,112-->00:05:26,113
Trong lớp, chúng ta sẽ ném những mảnh giấy
mà giáo viên không biết.

41
00:05:27,112-->00:05:32,113
Giờ ra chơi, nếu chúng ta đói, 
chúng ta sẽ chia nhau bánh mì mua ở căn-tin.

42
00:05:33,112-->00:05:36,513
Đến bữa trưa, chúng ta sẽ ăn bento cùng nhau.

43
00:05:37,112-->00:05:40,513
Và giờ ra về, chúng ta sẽ đi bộ 
và hẹn hò khi đang mặc đồng phục học sinh.

43
00:05:42,812-->00:05:43,513
Chỉ nghĩ đến thôi...

44
00:05:44,912-->00:05:45,913
Mayuri...

45
00:05:46,812-->00:05:50,913
Shidou, em muốn đi học ngay bây giờ.

46
00:05:52,112-->00:05:53,313
Đi học sao?

47
00:05:53,812-->00:05:55,913
Vâng, em nói với anh rồi. 

48
00:05:56,312-->00:06:00,113
Em muốn biết anh hay làm gì ở trường, Shidou.

49
00:06:01,112-->00:06:03,113
Nhưng không có giáo viên ở đây...

50
00:06:04,113-->00:06:06,913
Anh nói gì vậy? Có Shidou-sensei rồi mà.

51
00:06:07,113-->00:06:08,113
Anh à?

52
00:06:08,513-->00:06:10,513
Vào đây. Anh ngồi lên bàn giáo viên đi.

53
00:06:11,113-->00:06:13,113
Khoan! Anh không phải giáo viên...

54
00:06:14,112-->00:06:15,513
Này, đừng kéo anh!

55
00:06:16,112-->00:06:17,513
Mọi thứ sẵn sàng rồi.

56
00:06:18,112-->00:06:24,113
Em sẽ ngồi đây, trước mặt thầy Shidou.

57
00:06:26,512-->00:06:33,113
Shidou-sensei, em muốn hỏi. 
Tình yêu là gì ạ?

58
00:06:33,512-->00:06:37,113
Sao em hỏi anh câu đó?

59
00:06:37,512-->00:06:41,513
Vì học sinh phải hỏi giáo viên chứ.

60
00:06:42,112-->00:06:43,513
Anh không nghĩ đó là lý do đâu.

41
00:06:44,112-->00:06:45,513
Vậy tình yêu là gì ạ?

42
00:06:50,512-->00:06:55,113
Thành thật mà nói, anh không rõ lắm, 

42
00:06:57,112-->00:06:59,113
Nhưng ví dụ như...

43
00:07:00,112-->00:07:03,513
Đó là muốn biết nhiều hơn về ai đó.

44
00:07:03,712-->00:07:06,513
Đó là muốn trở nên đặc biệt với người đó.

45
00:07:07,512-->00:07:09,513
Đó là muốn ở bên người đó nhiều hơn.

46
00:07:11,112-->00:07:14,113
Anh nghĩ thế.

47
00:07:17,113-->00:07:19,113
Nghe tuyệt thật đấy.

48
00:07:21,112-->00:07:25,113
Dù sao em chưa bao giờ 
yêu một cách bình thường cả.

49
00:07:26,112-->00:07:29,113
Và em chắc mình sẽ không bao giờ 
làm được như thế.

50
00:07:30,112-->00:07:31,113
Không phải vậy đâu...

51
00:07:31,312-->00:07:34,,113
Bảo vệ đang đến kìa!

52
00:07:34,312-->00:07:36,113
Trốn nhanh lên! Vào đây này Mayuri!

53
00:07:36,312-->00:07:37,113
Vâng. 

54
00:07:41,112-->00:07:43,913
Shidou, ở đây...

55
00:07:44,112-->00:07:47,513
Không còn cách nào khác.
Chúng ta phải chui xuống bàn giáo viên thôi.

56
00:07:48,112-->00:07:51,913
Này, tay anh... chạm vào...

57
00:07:52,312-->00:07:54,513
Không, anh không cố ý...

58
00:07:54,812-->00:07:55,913
Biến thái.

59
00:07:56,112-->00:07:58,113
Không phải thế.

60
00:08:00,512-->00:08:01,513
Nè Shidou.

61
00:08:02,112-->00:08:03,513
Ông ta đang kiểm tra đấy.

62
00:08:04,113-->00:08:05,513
Một chút thôi... 

62
00:08:06,513-->00:08:10,113
Đây có phải việc anh hay làm không?

63
00:08:10,512-->00:08:12,113
Tất nhiên là không rồi.

64
00:08:14,112-->00:08:16,513
Vậy đây là điều đặc biệt à?

65
00:08:18,112-->00:08:21,513
Phải... Có lẽ vậy...

66
00:08:23,312-->00:08:24,513
Vậy à...

67
00:08:31,912-->00:08:33,513
Có vẻ an toàn rồi.

68
00:08:34,912-->00:08:37,113
Chúng ta về thôi, Mayuri.

69
00:08:37,312-->00:08:38,513
Đợi đã

69
00:08:39,112-->00:08:41,513
Em muốn ở đây với anh thêm chút nữa.

70
00:08:42,312-->00:08:43,313
Mayuri?

71
00:08:44,112-->00:08:49,513
Nghe này Shidou, không quan trọng 
em có là bạn cùng lớp hay không.

72
00:08:50,112-->00:08:54,513
Nếu chúng ta có khoảng thời gian đặc biệt này.

73
00:08:55,112-->00:08:58,113
Như thế là đủ rồi.

74
00:08:59,112-->00:09:00,513
Mayuri

75
00:09:03,112-->00:09:04,113
Được rồi.

76
00:09:05,112-->00:09:06,513
Cảm ơn anh.

77
00:09:09,112-->00:09:14,513
Này Shidou, cảm giác này, lẽ nào...

78
00:09:18,112-->00:09:20,113
Không có gì đâu. 

79
00:09:21,112-->00:09:25,113
Hai ta ở bên nhau thế này
lâu hơn chút nữa nhé?


368
00:13:15,112-->00:13:19,113

