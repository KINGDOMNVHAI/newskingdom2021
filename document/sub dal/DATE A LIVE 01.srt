﻿0
00:00:09,112-->00:00:14,113
30 năm trước, biên giới giữa Trung Quốc và Mông Cổ

1
00:01:03,112-->00:01:07,113
Một trận xung vũ trụ đã tàn phá 
trung tâm lục địa Á - Âu.

2
00:01:09,112-->00:01:13,113
gây ra cái chết của 150 triệu người.

3
00:01:16,112-->00:01:16,913
Và hiện tại.

3
00:03:05,112-->00:03:09,113
Tập 1: ngày 4 tháng 10.

4
00:03:06,112-->00:03:10,113
Tôi đang bị cô em gái của mình hành hạ
đủ kiểu để gọi tôi dậy.

5
00:03:12,112-->00:03:15,113
Kotori, cô em gái yêu dấu của anh.

6
00:03:16,512-->00:03:19,113
Sao vậy, anh trai yêu dấu của em?

7
00:03:20,112-->00:03:22,113
Xuống ngay! Nặng thấy mồ này!

8
00:03:32,112-->00:03:33,113
Anh vừa kêu "Gouf" kìa!

9
00:03:33,312-->00:03:35,113
Đó là tên của một Mobile Suit đấy!

10
00:03:38,112-->00:03:39,513
Này, anh ngủ tiếp đấy à?

11
00:03:40,112-->00:03:43,113
Tỉnh dậy! Tỉnh dậy đi, onii-chan! Dậy ngay!

12
00:03:45,112-->00:03:46,513
Chạy đi, Kotori!

13
00:03:48,112-->00:03:51,113
Anh đã bị nhiễm virus 
"Nếu em không cho anh ngủ thêm 10 phút nữa"

14
00:03:51,212-->00:03:53,113
"anh sẽ cù lét em gái mình tới chết" rồi.

15
00:03:54,112-->00:03:56,113
Hay còn gọi là T-Virus.

16
00:03:57,112-->00:03:58,613
Anh nói sao cơ?

17
00:03:59,112-->00:04:02,113
Chạy đi, Kotori...trong khi anh vẫn 
kiềm chế được bản thân mình!

18
00:04:02,512-->00:04:05,113
Nhưng... chuyện gì sẽ xảy ra với anh?

19
00:04:06,112-->00:04:09,113
Hãy quên anh đi! Chỉ cần em được 
an toàn là anh mãn nguyện rồi...

20
00:04:09,213-->00:04:10,113
Nhanh lên!

21
00:04:11,112-->00:04:12,113
Nhưng mà... Onii-chan!

22
00:04:26,112-->00:04:26,913
May quá.

23
00:04:40,112-->00:04:44,113
Sợ quá đi. T-Virus sẽ nhiễm sang mình mất.

24
00:05:00,112-->00:05:02,113
Bình tĩnh đi, anh không bị gì đâu.

25
00:05:03,112-->00:05:04,513
O...Onii-chan?

26
00:05:05,112-->00:05:09,113
Anh thề đó, em gái yêu dấu của anh.

27
00:05:16,112-->00:05:20,113
Sáng nay, một trận Không gian chấn nhỏ
đã diễn ra ở ngoại ô thành phố Tengu.

28
00:05:20,312-->00:05:22,113
Thiệt hại ước tính không đáng kể.

29
00:05:22,312-->00:05:24,113
và không có thiệt hại về người.

30
00:05:25,112-->00:05:27,113
Hiện nay, nguyên nhân dẫn đến 
các trận Không gian chấn vẫn là bí ẩn.

31
00:05:28,112-->00:05:30,113
Lại một trận nữa à? Lại còn ở gần đây nữa.

32
00:05:30,312-->00:05:31,113
Ừ.

33
00:05:32,112-->00:05:33,113
Không gian chấn.

34
00:05:34,112-->00:05:36,113
Sự hỗn loạn không rõ nguyên nhân trong bầu vũ trụ.

35
00:05:36,812-->00:05:42,113
Đúng như cái tên, khi một khu vực trong
bầu vũ trụ có biến động, mọi thứ sẽ bị phá hủy.

36
00:05:42,512-->00:05:44,113
Không ai biết tại sao nó lại diễn ra.

37
00:05:45,112-->00:05:46,113
Sau đại thảm họa 30 năm trước.

38
00:05:47,112-->00:05:50,113
Đã có vô số trận Không gian chấn diễn ra
trên toàn Trái Đất trong 6 tháng.

39
00:05:52,112-->00:05:54,113
Ở Nhật, nơi mà chúng tôi đang sinh sống,
được mệnh danh là

40
00:05:55,112-->00:05:58,113
Siêu Trung Tâm Không Gian Chấn
Của Phía Nam Kanto.

41
00:06:00,112-->00:06:02,113
25 năm sau, nó không còn diễn ra nữa.

42
00:06:03,112-->00:06:08,113
Nhưng khoản 5 năm trước, 
khi thành phố Tengu của chúng tôi vừa được tái lập.

43
00:06:09,112-->00:06:11,113
Các cơn xung vũ trụ đã quay lại.

44
00:06:12,112-->00:06:13,313
Chuyện còn phức tạp hơn.

45
00:06:13,812-->00:06:16,113
Nhật Bản là nơi chịu nhiều hậu quả nhất.

46
00:06:17,112-->00:06:18,513
Dạo này nó hay xảy ra thật nhỉ?

47
00:06:19,113-->00:06:20,113
Đúng thế thật.

48
00:06:21,112-->00:06:23,113
Có lẽ phải đẩy nhanh tiến độ thôi.

49
00:06:24,112-->00:06:26,113
Ê! Sắp ăn sáng rồi đó!

50
00:06:33,112-->00:06:34,113
Dù gì cũng phải ăn sáng đã chứ.

51
00:06:35,112-->00:06:35,813
Ừ.

52
00:06:36,112-->00:06:37,513
Em yêu anh, Onii-chan!

53
00:06:40,112-->00:06:42,113
Trưa nay em muốn ăn gì nào?

54
00:06:43,112-->00:06:45,113
Bữa ăn 5 sao dành cho trẻ em!

55
00:06:46,112-->00:06:47,113
Cái đó thì mời ra nhà hàng giùm.

56
00:06:48,112-->00:06:50,113
Anh xin lỗi, nhà nghèo không có thứ đó đâu.

57
00:06:50,312-->00:06:52,113
Ế, nhưng em muốn ăn cái đó cơ!

58
00:06:52,312-->00:06:54,113
Làm ơn! Làm ơn!

59
00:06:54,312-->00:06:57,113
Em xin anh đấy, Onii-chan!

60
00:07:02,512-->00:07:04,113
Bữa ăn 5 sao...

61
00:07:05,112-->00:07:06,313
Cùng ăn trưa tại nhà hàng...

62
00:07:07,112-->00:07:08,513
Em không cần phải quá phấn khích vì chuyện đó đâu.

63
00:07:09,112-->00:07:11,513
Em phải vui chứ. Cảm ơn anh đã cho em
ăn ngoài, Onii-chan.

64
00:07:14,112-->00:07:16,513
Tan học nhớ gặp nhau ở đây nhé.

65
00:07:17,312-->00:07:18,113
Được rồi.

66
00:07:19,112-->00:07:20,113
Anh nhớ phải giữ lời đấy nhé!

67
00:07:20,312-->00:07:24,113
Cho dù khủng bố có đánh chiếm nhà hàng
thì cũng phải đến đấy!

68
00:07:25,112-->00:07:26,113
Nếu có chuyện đó thì nhịn ăn trưa nhé.

69
00:07:26,812-->00:07:30,113
Anh hiểu rồi, em mau đến trường đi.

70
00:07:31,512-->00:07:33,113
Đó không phải là Itsuka-kun sao?

71
00:07:33,312-->00:07:34,313
Hai người bọn họ thân thiết ghê.

72
00:07:35,112-->00:07:36,113
Vậy là quả nhiên cậu ta có ý gì đó với em gái mình.

73
00:07:37,112-->00:07:37,913
Tởm quá ta.

74
00:07:39,112-->00:07:40,113
Anh đã hứa rồi nhé!

75
00:07:40,312-->00:07:43,513
Cho dù có Không gian chấn thì anh cũng phải đến đấy.

76
00:07:44,112-->00:07:46,113
Được rồi. Em đi đi.

77
00:07:47,112-->00:07:49,113
Chào buổi sáng, Itsuka.

78
00:07:50,112-->00:07:51,113
Ờ... Tonomachi.

79
00:07:52,112-->00:07:54,113
Mới đầu học kỳ mà trông phong độ ghê ta.

80
00:07:58,112-->00:07:59,513
Cũng có nhiều tin đồn về cậu ta lắm.

81
00:08:00,112-->00:08:02,113
Người ta nói Tonomachi cưa cả gay lẫn les.

82
00:08:02,312-->00:08:03,313
Tởm quá ta.

83
00:08:08,612-->00:08:09,513
Sao vậy?

84
00:08:10,112-->00:08:12,113
Có lẽ tớ vừa thấy cô siêu thần đồng.

85
00:08:12,512-->00:08:14,113
Tởm quá ta.

86
00:08:22,112-->00:08:23,113
Tình cờ nhỉ, Itsuka.

87
00:08:24,812-->00:08:30,513
Tôi được tiếp tục ở cùng lớp với ông
như định mệnh vậy.

88
00:08:31,112-->00:08:32,113
Vậy à?

89
00:08:34,112-->00:08:36,513
À, đợi tí, là người yêu tôi.

90
00:08:37,112-->00:08:38,313
Ông có người yêu khi nào thế?

91
00:08:39,112-->00:08:41,113
Để tớ giới thiệu. Đây.

92
00:08:44,112-->00:08:45,113
Là game Dating Sim mà!

93
00:08:45,512-->00:08:48,113
Người yêu là người yêu. Đừng có lý sự!

94
00:08:48,312-->00:08:52,113
Những trò như thế này rèn cách cư xử
của chúng ta trước mặt con gái đấy!

95
00:08:52,312-->00:08:54,113
Chúng như bí kíp tình yêu ấy.

96
00:08:54,312-->00:08:56,913
Trò: "Ta yêu thôi: Hạt giống bé nhỏ của em"
là một trò thực tế ảo và...

97
00:08:57,112-->00:08:58,113
Itsuka Shidou.

98
00:09:00,312-->00:09:01,113
Tớ à?

99
00:09:01,312-->00:09:02,113
Phải.

100
00:09:04,112-->00:09:05,513
Sao cậu biết tớ?

101
00:09:06,112-->00:09:07,113
Cậu không nhớ sao?

102
00:09:14,112-->00:09:16,113
Này, là ai vậy?

103
00:09:17,112-->00:09:20,113
Đừng nói ông không biết thần đồng
Tobiichi Origami đấy nhé.

104
00:09:21,112-->00:09:23,113
Tobiichi Origami?

105
00:09:26,112-->00:09:29,113
Cô ấy được điểm cao nhất khối, kể cả môn thể dục.

106
00:09:30,112-->00:09:33,113
Và đặc biệt còn là một tuyệt sắc giai nhân.

107
00:09:33,312-->00:09:38,113
Cô ấy luôn có mặt trong danh sách 
3 cô gái tôi muốn hẹn hò.

108
00:09:40,512-->00:09:43,113
Ông mà không biết đến người nổi tiếng
nhất trường thì thật là...

109
00:09:44,112-->00:09:46,113
Mà sao Tobiichi lại biết ông?

110
00:09:47,112-->00:09:48,113
Sao tôi biết được.

111
00:09:56,112-->00:09:58,113
A, Tama-chan kìa!

112
00:09:59,112-->00:09:59,913
Chào buổi sáng!

113
00:10:00,512-->00:10:03,313
Cô sẽ là giáo viên chủ nhiệm năm nay của các em.

114
00:10:04,112-->00:10:06,113
Tên cô là Okamine Tamae.

1
00:10:14,112-->00:10:15,113
Tobiichi Origami?

1
00:10:16,912-->00:10:18,113
Sao cậu lại biết tớ?

1
00:10:19,112-->00:10:20,113
Ánh mắt của cậu như vậy là sao?

1
00:10:21,112-->00:10:23,113
Sao vậy? Cậu muốn gì ở tớ?

1
00:10:26,112-->00:10:28,113
Itsuka, ta cùng về thôi.

1
00:10:28,612-->00:10:30,113
Xin lỗi, tôi có chuyện rồi.

1
00:10:31,112-->00:10:32,113
Ồ? Đi gái gú à?

368
00:10:33,112-->00:10:34,513
Có thể nói như thế. Kotori ấy mà.

368
00:10:35,112-->00:10:36,113
Biết ngay mà.

368
00:10:37,112-->00:10:38,313
Sau nhiều cuộc điều tra.

368
00:10:38,512-->00:10:42,113
Tôi kết luận rằng không có cô gái nào
đồng ý ăn trưa với ông cả.

368
00:10:43,112-->00:10:45,113
Phán như thánh ấy nhỉ.

368
00:10:46,512-->00:10:49,113
Một trận Không gian chấn 
vừa được phát hiện trong khu vực.

368
00:10:49,312-->00:10:50,513
Báo động Không gian chấn sao?

368
00:10:51,212-->00:10:52,113
Đây không phải là diễn tập. 
Xin mau chóng tới hầm trú ẩn gần nhất.























Sắp có một trận Không gian chấn sao?

368
00:10:52,312-->00:10:54,113
Đây không phải là diễn tập. 
Xin mau chóng tới hầm trú ẩn gần nhất.





















Ta xuống hầm trú thôi. 
Ở dưới trường có một cái đấy.

368
00:10:56,112-->00:10:57,113
Tobiichi?

368
00:11:01,112-->00:11:09,113
(Mật ngữ trong quân sự).

68
00:11:10,112-->00:11:12,113
Nhiều khả năng đó chính là Princess!

368
00:11:13,112-->00:11:14,113
Nó tới rồi.

368
00:11:14,512-->00:11:19,513
Tôi nhắc lại: Một trận Không gian chấn 
vừa được phát hiện trong khu vực.

368
00:11:20,112-->00:11:29,113
Đây không phải là diễn tập. 
Xin mau chóng tới hầm trú ẩn gần nhất.

368
00:11:29,512-->00:11:34,113
Bình tĩnh nào mọi người! Đừng quên luật 3 không:
Không chen lấn, không hấp tấp, không "ăn thịt".

368
00:11:34,512-->00:11:36,113
Cô ơi, cô mới phải cần bình tĩnh đó.

368
00:11:37,112-->00:11:38,113
Em...em nói phải.

368
00:11:38,612-->00:11:41,113
Hy vọng Kotori cũng đã tìm được hầm trú.

368
00:11:42,112-->00:11:44,113
Đừng nói là con bé giữ lời thật đấy nhé.

368
00:11:45,512-->00:11:47,113
Cho dù có Không gian chấn thì anh cũng phải đến đấy.

368
00:11:48,112-->00:11:50,513
Em không đứng đợi trước nhà hàng phải không?

368
00:11:52,112-->00:11:53,113
Em đã đến hầm trú gần nhất rồi phải không?

368
00:11:53,812-->00:11:56,113
Bình tĩnh, con bé chỉ không nghe thấy 
chuông reo do quá đông người thôi.

368
00:11:57,112-->00:11:58,113
Chắc chắn là thế.

368
00:11:58,912-->00:12:01,113
Cẩn tắc vô áy náy, kiểm tra 
luôn GPS của con bé vậy.

368
00:12:02,113-->00:12:03,113
Đã chắc chắn thì chắc cho trót, để chắc là con bé...

368
00:12:04,113-->00:12:05,513
Itsuka Kotori
Nhà hàng Danny.

368
00:12:10,112-->00:12:11,113
Này, Itsuka!

368
00:12:18,112-->00:12:19,513
Em yêu anh, Onii-chan!

368
00:12:22,112-->00:12:22,913
Tại sao?

368
00:12:23,112-->00:12:25,113
Cảm ơn anh, Onii-chan!

368
00:12:25,312-->00:12:26,313
Tại sao?

368
00:12:27,312-->00:12:32,113
Tại sao em vẫn đứng chờ anh trong khi
cả thành phố đã di tản?

368
00:12:33,112-->00:12:35,113
Kotori, Kotori, Kotori!

368
00:12:48,112-->00:12:49,513
Chuyện gì vậy?

368
00:13:15,512-->00:13:19,113
Cô ấy đang làm gì ở dưới thế?

368
00:13:29,112-->00:13:30,113
Cả ngươi nữa sao?

368
00:13:54,112-->00:13:58,113
Ngươi cũng đến đây để giết ta sao?

368
00:14:09,112-->00:14:12,113
Ngươi đến để giết ta phải không?

368
00:14:12,512-->00:14:14,313
Nếu đã thế, ta sẽ kết thúc ngươi tại đây.

368
00:14:15,113-->00:14:16,113
Đợi đã! Đợi đã!

368
00:14:17,112-->00:14:18,113
Tôi đâu có thể làm thế được!

368
00:14:18,312-->00:14:18,913
Cái gì?

368
00:14:19,212-->00:14:21,113
Tôi sao lại giết ai được chứ.

368
00:14:22,112-->00:14:23,113
Mà cô là ai vậy?

368
00:14:38,112-->00:14:41,113
Các ngươi vẫn không thấy chuyện này vô nghĩa sao?

368
00:14:56,512-->00:14:58,113
Sao cô gái này...

368
00:15:46,112-->00:15:50,113
Dù có một sức mạnh tiềm tàng, 
nhưng lại trông rất tuyệt vọng.

368
00:15:43,112-->00:15:45,113
Tobiichi Origami?

368
00:15:46,512-->00:15:49,113
Itsuka Shidou.

368
00:15:50,112-->00:15:51,513
C...cậu đang mặc gì thế kia?

368
00:16:28,112-->00:16:30,113
Tọa độ: FJ-1820.

368
00:16:30,812-->00:16:32,113
Mục tiêu: Princess.

368
00:16:32,312-->00:16:34,113
Đã mất dấu mục tiêu.

368
00:16:34,112-->00:16:36,113
Không đủ dữ liệu để tiếp tục truy tìm.

368
00:16:36,312-->00:16:37,113
Cái gì vậy?

368
00:16:39,112-->00:16:40,113
Cũng lâu rồi.

368
00:16:41,112-->00:16:42,113
Cuối cùng chúng ta lại được gặp nhau.

368
00:16:43,112-->00:16:43,913
Cô là ai?

368
00:16:44,112-->00:16:45,113
Em vui lắm.

368
00:16:46,112-->00:16:50,113
Nhưng xin anh đợi thêm một lúc nữa.

368
00:16:51,312-->00:16:56,113
Em sẽ không để anh đi mất nữa.
Em sẽ không để sai lầm đó diễn ra nữa.

368
00:16:57,312-->00:16:58,113
Vì vậy...

368
00:17:08,112-->00:17:09,113
Cậu tỉnh rồi à?

368
00:17:10,112-->00:17:11,613
C...cô là ai?

368
00:17:12,312-->00:17:15,513
Tôi là phân tích viên ở đây, Murasame Reine.

368
00:17:16,112-->00:17:21,113
Không cần phải lo. Dù tôi không phải
là bác sĩ nhưng cũng biết sơ cứu căn bản.

368
00:17:24,112-->00:17:25,113
Không biết có khi lại hay hơn.

368
00:17:26,112-->00:17:30,113
Um... cô có thể nói cho em biết
em đang ở đâu không?

368
00:17:30,612-->00:17:32,113
Bệnh xá của Fraxinus.

368
00:17:33,112-->00:17:35,113
Chúng tôi đã đưa cậu tới khi cậu còn đang bất tỉnh.

368
00:17:36,112-->00:17:38,113
Fraxi...bất tỉnh.

368
00:17:39,112-->00:17:42,113
Phải rồi, mình bị lạc vào giữa một chiến trường.

368
00:17:43,112-->00:17:44,113
Kotori!

368
00:17:44,512-->00:17:47,113
Em gái em! Em đang đi tìm em gái em!

368
00:17:47,512-->00:17:50,113
Nó đang ở bên ngoài một nhà hàng
mà không có hầm trú. Và rồi...

368
00:17:51,112-->00:17:53,113
Bình tĩnh, cô ấy vẫn ổn.

368
00:17:54,112-->00:17:55,313
Ơ? sao cô biết?

368
00:17:56,112-->00:17:58,113
Tôi biết cậu đang muốn biết nhiều thứ.

368
00:17:59,112-->00:18:01,113
Nhưng tôi không phải là người có thể giải thích cho cậu.

368
00:18:02,112-->00:18:03,113
Chỉ huy sẽ trả lời bất cứ câu hỏi nào của cậu.

368
00:18:06,112-->00:18:07,113
Cậu ta tới rồi.

368
00:18:09,112-->00:18:10,113
Cô làm tốt lắm.

368
00:18:11,112-->00:18:16,113
Xin chào, tôi là phó chỉ huy, Kannazuki Kyouhei.

368
00:18:16,512-->00:18:18,113
Hân hạnh được gặp cậu.

368
00:18:27,112-->00:18:28,113
Kotori?

368
00:18:29,112-->00:18:33,113
Chào mừng đến với phòng chỉ huy của em - Ratatosk.

368
00:18:36,112-->00:18:39,113
Kiểm tra đội chống Tinh Linh, sạch sẽ. Kích hoạt ANS.

368
00:18:46,112-->00:18:47,113
Ngắt kết nối.

368
00:18:51,112-->00:18:53,113
Làm tốt lắm, Origami.

368
00:18:54,112-->00:18:55,213
Đội trưởng Kusakabe...

368
00:18:57,112-->00:19:00,113
Em nên quan tâm đến bản thân hơn.

368
00:19:01,112-->00:19:04,113
Thứ mà chúng ta để mất dấu là Tinh Linh

368
00:19:04,412-->00:19:06,113
Nói cách khác là chúng có thể biến mất.

368
00:19:06,112-->00:19:06,913
Và đây là...

368
00:19:07,112-->00:19:08,113
Đợi, đợi đã!

368
00:19:09,112-->00:19:10,113
Gì nữa đây?

368
00:19:10,312-->00:19:13,113
Chưa gì mà đã đòi chỉ huy giải thích lại rồi à?

368
00:19:14,112-->00:19:16,113
Được nghe là anh đã vinh hạnh lắm rồi đấy.

368
00:19:17,112-->00:19:19,513
Giờ thì tạm thời cho phép anh liếm chân em đấy.

368
00:19:20,112-->00:19:20,913
Thật sao?

368
00:19:21,112-->00:19:22,113
Mơ đi!

368
00:19:22,512-->00:19:24,613
Cảm ơn chỉ huy!

368
00:19:26,512-->00:19:28,513
Em có thật là Kotori không?

368
00:19:29,112-->00:19:31,513
Chưa gì anh đã quên mặt em gái mình rồi sao, Shidou?

368
00:19:32,112-->00:19:33,113
Em gọi anh là gì cơ?

368
00:19:33,512-->00:19:36,113
Em không ngờ trí nhớ của anh lại tệ đến thế.

368
00:19:37,112-->00:19:40,113
Có lẽ nên tìm viện dưỡng lão cho anh đi là vừa.

368
00:19:40,512-->00:19:32,113
Em đang làm gì ở đây vậy? Ta đang ở đâu?

368
00:19:43,812-->00:19:47,113
Rồi rồi. Chúng ta sẽ nói chuyện sau.

368
00:19:48,512-->00:19:50,113
Nhưng bây giờ em cần anh phải hiểu.

368
00:19:50,112-->00:19:52,113
Thứ nhất, cô ta là một Tinh Linh.

368
00:19:52,312-->00:19:54,513
Chúng là các thực thể không xuất thân từ thế giới này.

368
00:19:55,112-->00:19:58,513
Khi tới đây, chúng buộc phải 
tàn phá nơi chúng xuất hiện.

368
00:19:59,112-->00:20:00,113
Tàn phá ư?

368
00:20:00,513-->00:20:02,113
Anh vẫn chưa thông à?

368
00:20:03,113-->00:20:04,913
Nói dễ hiểu thì hiện tượng Không gian chấn.

368
00:20:05,113-->00:20:09,113
Được sinh ra khi Tinh Linh đáp xuống Trái Đất.

368
00:20:13,112-->00:20:15,113
Là do cô ta sao?

368
00:20:17,112-->00:20:20,113
Thứ 2, đó là AST.

368
00:20:21,112-->00:20:22,113
Bọn họ là Biệt Đội Phòng Chống Tinh Linh
(Anti-Spirit Team) của Bộ quốc phòng Nhật.

368
00:20:23,112-->00:20:26,113
Nếu một Tinh Linh xuất hiện, họ sẽ bay tới
hiện trường và xử lý vụ việc.

368
00:20:26,912-->00:20:28,113
Đơn giản là họ giết chúng.

368
00:20:29,112-->00:20:29,913
Giết sao?

368
00:20:30,312-->00:20:33,113
Ngươi cũng đến đây để giết ta sao?

368
00:20:35,112-->00:20:40,113
Thứ 3, có một cách khác ngoài AST
để chống lại bọn Tinh Linh.

368
00:20:40,512-->00:20:42,513
Tuy nhiên, bọn em cần anh giúp.

368
00:20:43,112-->00:20:44,113
Anh giúp à?

368
00:20:44,612-->00:20:47,113
Anh sẽ bắt đầu cuộc tập huấn vào ngày mai.

368
00:20:48,112-->00:20:52,613
Em sẽ lo chuyện giấy tờ cho anh. 
Anh cứ đến trường bình...

368
00:20:53,112-->00:20:55,113
Từ từ đã! "Tập huấn" là sao?

368
00:20:55,512-->00:20:57,513
Em định bắt anh làm gì vậy?

368
00:21:00,112-->00:21:02,113
Em không có bảo anh phát biểu ý kiến.

368
00:21:02,312-->00:21:04,513
Em chỉ chấp nhận từ "Đồng ý"của anh thôi.

368
00:21:07,512-->00:21:10,113
Em biết anh là một tên óc nho. Nhưng sao
anh lại lang thang ở ngoài vậy hả?

368
00:21:10,312-->00:21:11,113
Anh muốn chết à?

368
00:21:12,112-->00:21:13,513
Anh đi tìm em đấy!

368
00:21:14,112-->00:21:16,513
Em là người đã nói anh phải đến cho dù
có Không gian chấn cơ mà!

368
00:21:17,112-->00:21:20,113
Thì đúng vậy, nhưng anh phải là 
tên ngốc có số mới tin chuyện đó.

368
00:21:21,112-->00:21:22,113
Tất nhiên là tin rồi!

368
00:21:22,512-->00:21:25,113
Điện thoại của em đứng im trước nhà hàng mà!

368
00:21:26,112-->00:21:26,913
Điện thoại à?

368
00:21:27,112-->00:21:29,513
À, em quên mất chuyện đó.

368
00:21:30,102-->00:21:31,113
Chúng ta hiện đang đứng ngay trên cái nhà hàng đó.

368
00:21:32,112-->00:21:33,113
Tắt màn lọc đi.

368
00:21:37,112-->00:21:39,513
Chúng ta hiện đang ở 15.000 mét cách thành phố Tengu.

368
00:21:41,112-->00:21:43,113
Và đây là phi thuyền Fraxinus.

368
00:22:14,112-->00:22:16,513
Mặc dù chỉ mới 2 ngày kể từ khi bắt đầu học kỳ.

368
00:22:17,112-->00:22:21,113
Chúng ta sẽ có một trợ lý chủ nhiệm.

368
00:22:21,912-->00:22:23,113
Tôi là Murasame Reine.

368
00:22:23,912-->00:22:24,913
Tôi dạy môn Vật Lý.

368
00:22:25,112-->00:22:26,113
Hân hạnh được...

368
00:22:32,112-->00:22:33,113
Phòng vật lý.

368
00:22:33,312-->00:22:35,513
Sao tự nhiên giờ cô lại là một giáo viên ở đây thế?

368
00:22:36,112-->00:22:39,113
Chuyện là sao hả, phân tích viên Murasame?

368
00:22:40,512-->00:22:42,513
Cứ gọi tôi là Reine, Shintarou.

368
00:22:43,112-->00:22:44,513
Không, tên em là Shidou.

368
00:22:46,112-->00:22:48,513
Vậy à, Xin lỗi, Shin...

368
00:22:49,112-->00:22:50,113
Đọc sai hoài vậy!

368
00:22:51,112-->00:22:52,113
Nhưng mà sai chủ đề rồi đấy.

368
00:22:53,112-->00:22:55,513
Tôi sẽ có thể đến chỗ cậu nhanh chóng
nếu có chuyện gì xảy ra.

368
00:22:56,512-->00:22:59,513
Đầu anh mà có nếp nhăn nhiều hơn thì anh hiểu rồi đấy.

368
00:23:00,112-->00:23:01,113
Cái đồ chậm tiêu.

368
00:23:02,113-->00:23:04,113
Mà em đang làm gì ở đây vậy hả?

368
00:23:04,512-->00:23:05,113
Tính chúp học dài hạn hay sao?

368
00:23:05,312-->00:23:07,113
Chuyện đâu đã vào đấy hết cả rồi.

368
00:23:12,112-->00:23:14,113
Vậy giờ em nói cho anh biết 
anh phải làm gì được chưa?

368
00:23:15,113-->00:23:18,113
Một khi đã dùng chương trình tập huấn
này thì anh sẽ hiểu thôi.

368
00:23:18,312-->00:23:19,113
Reine.

368
00:23:30,112-->00:23:32,113
Game Dating Sim sao?

368
00:23:32,512-->00:23:33,513
Giống game mà Tonomachi đang chơi.

368
00:23:34,112-->00:23:35,513
Không phải nó đâu.

368
00:23:35,712-->00:23:37,113
Nhìn cho kỹ đi.

368
00:23:40,112-->00:23:43,113
"Shidou bé bỏng của em"?

68
00:23:46,512-->00:23:50,513
Các sinh vật chuyên gây ra thiên tai đặc thù
cư ngụ ở giữa các thế giới.

368
00:23:51,112-->00:23:56,113
Xuất xứ và mục đích của chúng là một ẩn số.

368
00:23:57,112-->00:24:06,113
Khi xuất hiện ở thế giới này,
chúng gây ra các Không gian chấn.
gây sức tàn phá lớn cho các khu vực.

368
00:24:07,512-->00:24:12,113
Giải pháp 1: Tiêu diệt bằng vũ khí.

368
00:24:13,512-->00:24:20,113
Tuy nhiên, điều này khó mà thực hiện được
do năng lực chiến đấu của chúng quá cao.

368
00:24:22,112-->00:24:23,113
Chào buổi sáng, Onii-chan! Là Lilica đây!

368
00:24:23,312-->00:24:25,113
Chuyện có vẻ như không ổn rồi.

368
00:24:26,112-->00:24:28,513
Nhưng em biết là anh sẽ giải quyết được mà!

368
00:24:29,112-->00:24:29,913
Nè Shidou.

368
00:24:30,112-->00:24:32,513
Cậu không muốn làm hại các Tinh Linh phải không?

368
00:24:33,112-->00:24:36,113
Nhưng nếu cứ để chúng yên thì sẽ có
nhiều Không gian chấn hơn đấy.

368
00:24:37,112-->00:24:38,513
Vậy sao chúng ta không thuyết phục chúng nhỉ?

368
00:24:39,112-->00:24:41,113
Bọn chúng cần phải biết thích thế giới này.

368
00:24:41,312-->00:24:44,113
Người ta nói: "Khi yêu thì thế giới cũng đẹp ra" mà.

368
00:24:45,112-->00:24:46,513
Có nghĩa là, Itsuka-kun...

368
00:24:47,112-->00:24:48,113
Có nghĩa là...

368
00:24:48,312-->00:24:49,313
Có nghĩa là...

368
00:24:50,512-->00:24:51,513
Anh cần phải...

368
00:24:52,112-->00:24:53,113
Hẹn hò! Hẹn hò!

368
00:24:53,512-->00:24:55,113
Hãy cưa đổ các Tinh Linh!

368
00:24:55,912-->00:24:57,113
Bằng các cuộc trò chuyện thân mật...

368
00:24:58,112-->00:24:59,113
Bằng các cuộc hẹn lãng mạn...

368
00:24:59,512-->00:25:01,113
...và bằng sự cuốn hút mãnh liệt của anh!

368
00:25:02,112-->00:25:03,113
Đó là...

368
00:25:04,112-->00:25:05,113
nhiệm vụ...

368
00:25:06,112-->00:25:07,113
đặc biệt...

368
00:25:08,112-->00:25:11,113
của anh!

368
00:25:11,912-->00:25:13,113
Giờ đã hiểu chưa nào?

368
00:25:13,312-->00:25:15,113
Chúc may mắn với cuộc tập huấn nhé!

368
00:25:21,112-->00:25:23,113
Hôm qua cậu đã thấy tớ.

368
00:25:25,112-->00:25:26,113
Đừng kể với ai đấy.

368
00:25:27,112-->00:25:31,113
Có khi sẽ tốt hơn nếu cậu quên những gì
cậu đã thấy hôm qua đi.

368
00:25:32,112-->00:25:34,113
Quên luôn cả cô gái đó sao?

368
00:25:37,112-->00:25:40,113
Này, Tobiichi, cô gái đó có phải...

368
00:25:40,512-->00:25:41,413
Cô ta là một Tinh Linh.

368
00:25:42,112-->00:25:45,113
Kẻ thù mà tớ phải tiêu diệt bằng mọi giá.

368
00:25:46,112-->00:25:48,113
Bọn họ thực sự xấu xa đến thế sao?

368
00:25:52,112-->00:25:53,113
Chỉ vì bọn chúng...

368
00:25:54,112-->00:25:56,113
Mà 5 năm trước, bố mẹ tớ...

368
00:25:57,112-->00:25:58,113
đã mất.
