﻿1
00:00:02,512-->00:00:04,113
Đi đến Đối diện nhà ga?
1) Đồng ý
2) Không đồng ý

1
00:00:08,512-->00:00:11,113
Ừm, chúng ta đã làm mọi thứ trên đường đến trạm ga. 
Rio muốn làm gì tiếp theo nè?

1
00:00:11,512-->00:00:14,513
Papa không có kế hoạch ư?

2
00:00:15,112-->00:00:18,113
Anh đã nghĩ nơi này có nhiều thứ để xem 
hơn là siêu thị. Vậy giờ bắt đầu...

3
00:00:18,312-->00:00:27,513
Nè nè, đó có phải Shidou-kun không nhỉ? 
Cậu đang để Yoshino lại phía sau 
buổi hẹn hò à? Hẹn hò à?

4
00:00:27,912-->00:00:29,513
Hở, giọng nói này...có lẽ nào?

5
00:00:30,112-->00:00:33,513
Shidou-san... Em không ngờ gặp anh ở đây đấy. 

6
00:00:33,912-->00:00:37,113
Yoshino, Yoshinon! 
Hiếm khi thấy em ở trạm ga ấy. 
Em đang đi đâu vậy?

7
00:00:37,512-->00:00:40,113
Thật ra là...

8
00:00:40,512-->00:00:45,113
Xin lỗi nhé, Yoshino. 
Có hơi đông đúc tí và...

9
00:00:45,312-->00:00:49,113
Shidou?! Anh đang làm gì ở đây vậy?

10
00:00:49,512-->00:00:51,113
À, anh đang...

11
00:00:51,512-->00:01:00,113
Papa, hai người này là 
Yoshino-chan và Kotori-chan phải không?
Họ ở đây để chơi với papa và Rio à?

12
00:01:01,112-->00:01:05,113
Shi... Shidou-san... Ai vậy...

13
00:01:05,512-->00:01:08,113
Không lẽ anh đã... bắt cóc em ấy?!

14
00:01:08,512-->00:01:17,113
Anh đã có mọi thứ rồi mà, Shidou-kun.
Không lẽ Yoshino và Kotori-chan 
vẫn chưa thỏa mãn được anh sao?

15
00:01:17,512-->00:01:19,113
Sao em lại nói như thế chứ?

16
00:01:19,512-->00:01:21,513
Bởi vì anh đấy, Shidou.

17
00:01:22,112-->00:01:24,513
Vì Shidou-kun hết đấy.

18
00:01:25,112-->00:01:26,113
Vâng...

19
00:01:26,512-->00:01:28,313
Các em tin anh một chút không được à?

20
00:01:28,912-->00:01:34,113
Mọi người tin papa mà. 
Bởi vì mọi người đều biết rõ papa mà.

21
00:01:34,312-->00:01:35,513
Những lời nói ấy rất ý nghĩa lắm!

22
00:01:36,112-->00:01:42,513
Quan trọng hơn, cô gái này là ai? 
Làm ơn nói với em là anh không bắt cóc 
em ấy và bắt em ấy gọi anh là papa đấy nhé.

23
00:01:42,912-->00:01:44,513
Làm như anh có ấy! Đây là Rio...

24
00:01:46,112-->00:01:50,113
Rio là người thân của Tonomachi-kun.

24
00:01:52,112-->00:01:55,113
Thật vậy sao?

25
00:01:57,112-->00:02:00,113
Đây là sự thật bất ngờ đó!

25
00:02:01,112-->00:02:03,513
Phải không, papa?

25
00:02:04,112-->00:02:07,513
Ừ, đúng vậy. Tonomachi có việc
nên cậu ấy nhờ anh chăm sóc Rio.

25
00:02:08,112-->00:02:14,113
Thì ra là vậy. Thật vui khi thấy 
miếng nam châm rắc rối của anh 
vẫn hiệu quả như mọi khi đấy.

25
00:02:15,112-->00:02:21,113
Một điều nữa, Shidou-kun. 
Yoshino có vài điều làm cậu ấy khó hiểu đấy.

25
00:02:21,512-->00:02:23,113
Là gì vậy, Yoshino?

25
00:02:23,512-->00:02:30,513
Tại sao cậu ấy lại gọi anh là "papa"?

25
00:02:31,112-->00:02:32,313
À, là bởi vì...

25
00:02:33,112-->00:02:40,513
Bởi vì papa giống với papa của Rio. 
Rio gọi bất kì ai giống 
papa của Rio như vậy đấy.

25
00:02:41,112-->00:02:45,113
Thật vậy sao?

25
00:02:45,312-->00:02:46,513
Haha. Đúng vậy, kiểu như vậy ấy.

25
00:02:47,112-->00:02:52,113
Papa ư? Không phải điều đó 
làm anh vui sao, anh trai dễ động lòng.

25
00:02:52,312-->00:02:54,113
Đừng như vậy mà. 
Em không thể nói gì hay hơn à?

25
00:02:55,112-->00:02:59,113
Vâng, được thôi. Vậy em ấy tên gì?

25
00:03:00,112-->00:03:06,113
Rio là Rio! Rất vui được gặp, Kotori-chan.

25
00:03:07,112-->00:03:13,113
Ừ, rất vui được gặp. 
Có vẻ như em đã biết chị 
nên chị sẽ bỏ qua phần giới thiệu.

25
00:03:13,512-->00:03:15,513
Tonomachi đã nói với Rio 
khá nhiều về chúng ta đấy.

25
00:03:16,112-->00:03:21,113
Ấn tượng thật nhỉ. Em ấy có thể nhận ra 
chúng ta trong khi chỉ nghe kể thôi à?

25
00:03:21,312-->00:03:23,513
Anh cũng vậy. Đặc biệt hơn khi 
đây là lần đầu Rio gặp em đấy.

25
00:03:24,112-->00:03:28,113
Nè, Yoshino-chan. 
Thỏ của cậu dễ thương lắm đấy.

25
00:03:29,112-->00:3:34,513
Rio-san... người này không phải thỏ...

25
00:03:35,112-->00:03:44,113
Đúng vậy đấy! Yoshinon trang trọng, 
cao quý ở đây được gọi là Yo-shi-non!

25
00:03:44,512-->00:03:48,113
Um...Yoshinon?

25
00:03:48,512-->00:03:54,113
Đúng vậy, là Yoshinon! 
Thêm "cô" ở phía trước nữa.

25
00:03:54,512-->00:03:56,113
Cô thỏ Yoshinon!

25
00:03:57,112-->00:04:01,113
Tớ đã nói Yoshinon không phải là thỏ!

25
00:04:02,112-->00:04:08,113
Cậu ấy nói đúng đấy. 
Yoshinon là Yoshinon!

26
00:04:09,112-->00:04:17,113
Ừm...Rio hiểu rồi. 
Yoshino và Yoshinon... khó nói nhỉ.

26
00:04:21,112-->00:04:28,113
Hai người đang làm gì vậy? 
Em ấy chỉ là trẻ con thôi mà... 
Mình quên mất, họ không khác nhau mấy.

26
00:04:28,312-->00:04:30,313
Anh không nghĩ em đúng khi nói như vậy đâu.

26
00:04:30,812-->00:04:35,113
Im lặng. Vậy Shidou,
anh đang định đưa em ấy đi đâu?

26
00:04:35,312-->00:04:40,113
Rio phải về nhà khi trời tối. 
Bọn anh không có kế hoạch gì cả
nên chỉ đi dạo đến lúc đó thôi.

26
00:04:40,512-->00:04:44,113
Vậy à? Vậy thì không có vấn đề gì nhỉ. Rio!

26
00:04:44,512-->00:04:46,513
Chị vừa gọi Rio à?

26
00:04:47,112-->00:04:52,113
Sao em không đến và chơi với tụi chị? 
Phải không, Shidou?

26
00:04:52,312-->00:04:54,513
Đúng vậy. Nhưng vậy ổn chứ Rio?

26
00:04:55,112-->00:05:02,113
Được mà. Em ấy có thể đến đó 
chơi với Yoshino. Vậy thì Rio, thế nào?

26
00:05:02,513-->00:05:06,513
Vâng! Rio cũng muốn chơi với Kotori-chan nữa!

26
00:05:07,113-->00:05:17,113
Và... một điều nữa.
Em gọi chị là Kotori-Oneechan, được chứ? 
Onee-sama cũng được.

27
00:05:17,512-->00:05:20,113
Vậy đi thôi, Kotori-chan.

28
00:05:21,112-->00:05:23,113
Em ấy làm ngơ luôn.

29
00:05:23,512-->00:05:25,113
Thôi nào, không tệ lắm mà. Mọi người cùng đi nào.

30
00:05:25,512-->00:05:30,113
Vậy thì hãy để Yoshino 
chỉ cậu cách để có một sức hút hấp dẫn nhé!

31
00:05:31,112-->00:05:35,113
Yo...Yoshinon... làm ơn đừng nói 
những điều kỳ lạ như vậy nữa.

31
00:05:36,112-->00:05:39,513
Và như thế, chúng tôi đã kết thúc 
việc này cùng với mọi người. 
Nó cũng không tệ lắm.

32
00:05:51,112-->00:05:55,513
Chúng tôi đã đến nhà hàng ăn Parfait,
đi dạo các cửa tiệm. Trời đã ngả về chiều.

33
00:05:56,112-->00:05:59,113
Giờ Rio phải đi rồi.

39
00:05:59,313-->00:06:00,513
Em đi gặp Marina à?

40
00:06:01,113-->00:06:03,113
Phải. Con đã hứa rồi.

41
00:06:03,313-->00:06:04,513
Anh hiểu rồi. Hôm nay em vui chứ?

42
00:06:05,113-->00:06:09,113
Con rất vui. Hôm nay con rất vui.

43
00:06:09,313-->00:06:10,513
Vậy thì tốt.

39
00:06:11,113-->00:06:20,513
Vậy hẹn gặp lại nhé papa, Kotori-chan,
Yoshino-chan. Và cả cô thỏ Yoshinon nữa.

40
00:06:21,513-->00:06:29,113
Tớ đã nói tớ không phải thỏ mà...
Mà có nói cũng như không thôi. Tạm biệt!

40
00:06:29,513-->00:06:34,513
Tớ đã có thời gian tuyệt vời, Rio-san. 
Hẹn gặp lại nhé.

40
00:06:34,913-->00:06:36,113
Gặp lại nhé, Rio.

41
00:06:36,313-->00:06:37,513
Hẹn gặp lại, Rio.

41
00:06:38,113-->00:06:41,113
Vâng! Tạm biệt mọi người!

42
00:06:41,513-->00:06:46,113
Quên mất. Có vài chuyện 
tôi muốn hỏi em ấy.
Mà thôi kệ, vẫn còn lần tới mà.

43
00:06:47,113-->00:06:49,113
Rio quên nói với Papa việc này.

44
00:06:49,313-->00:06:50,513
Gì vậy?

45
00:06:51,113-->00:06:53,513
Nếu Papa muốn gặp Rio, 
hãy đến công viên nhé, được chứ?

46
00:06:54,112-->00:06:55,313
Rio thì thầm nhỏ nhẹ bên tai tôi.

47
00:06:55,512-->00:06:56,513
Hiểu rồi.

48
00:06:57,112-->00:06:58,113
Bye bye!

49
00:06:59,512-->00:07:03,513
Shidou, Rio đã nói gì với anh vậy?

50
00:07:03,912-->00:07:05,913
Rio nói Rio muốn đi chơi 
với chúng ta một lần nữa. 

51
00:07:06,112-->00:07:09,513
Không đời nào tôi nói việc Rio đã nói. 
Tôi không có sự lựa chọn nào cả

34
00:07:10,112-->00:07:15,513
Nếu em ấy thật sự là người nhà của 
bạn thân nhất của Shidou 
thì chúng ta sẽ gặp lại em ấy sớm thôi.

35
00:07:16,112-->00:07:17,513
Nè, Kotori. Tonomachi không thật sự 
là bạn thân nhất của anh đâu.

35
00:07:17,812-->00:07:18,913
Hắn chỉ là bạn lan truyền tin đồn thôi.

36
00:07:19,112-->00:07:24,113
Anh đang thô lỗ với bạn mình đấy, Shidou.

37
00:07:24,312-->00:07:26,513
Tôi đã quyết định làm ngơ 
những lời nói của Kotori 
và đi về nhà cùng mọi người.

38
00:07:59,112-->00:07:02,113


39
00:07:59,112-->00:07:02,113


40
00:07:59,112-->00:07:02,113

