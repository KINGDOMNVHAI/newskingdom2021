﻿0
00:00:03,512-->00:00:05,513
Đi đến Tháp Tengu?
1) Đồng ý
2) Không đồng ý

1
00:00:09,512-->00:00:12,513
Nhân dịp này, chúng ta đến tháp Tengu đi.
Em thấy sao Rio?

2
00:00:13,112-->00:00:16,113
Vâng. Con sẽ đi với papa đến bất kỳ đâu.

3
00:00:16,512-->00:00:18,113
Một nơi hoàn hảo để đi chơi đấy.
Em muốn anh nắm tay em chứ?

4
00:00:18,512-->00:00:20,113
Vâng. Con muốn lắm!

5
00:00:20,512-->00:00:24,513
Tôi lấy bàn tay nhỏ nhắn của Rio. 
Tôi nhớ lại một chút, cách đây lâu rồi, 
tôi đã nắm tay của Kotori. 
Không có nhiều cơ hội nắm tay con gái thế này.

6
00:00:25,112-->00:00:27,513
Tay papa ấm lắm.

7
00:00:28,112-->00:00:29,513
Thật sao? Tay em cũng vậy, Rio.

8
00:00:30,112-->00:00:32,513
Papa và Rio giống nhau phải không?

9
00:00:32,812-->00:00:34,113
Phải, giống lắm.

10
00:00:38,112-->00:00:39,513
Chúng ta đi nhanh đấy chứ.

11
00:00:40,112-->00:00:44,113
Vâng. Vì đi cùng papa 
nên con đi nhanh hơn.

12
00:00:46,112-->00:00:49,113
Darling!

13
00:00:49,512-->00:00:53,113
Và một cô bé dễ thương!

14
00:00:54,512-->00:01:00,113
Thắc mắc. Shidou, cậu đang làm gì ở đây?

15
00:01:00,512-->00:01:01,513
À, đây là...

16
00:01:02,112-->00:01:05,513
Papa đang đi dạo với Rio.

17
00:01:07,512-->00:01:17,513
Pa... papa?
Darling, chẳng lẽ cô bé này 
là con gái bí mật của anh sao?

18
00:01:18,112-->00:01:23,513
Kinh ngạc. Shidou, tớ không bao giờ nghĩ 
cậu có một bí mật như vậy.

19
00:01:24,112-->00:01:26,513
Không phải! Mấy cậu nghĩ
tớ có con lớn thế này sao?

20
00:01:27,112-->00:01:33,513
Em không ngờ anh là một 
kẻ hư hỏng như vậy, Darling.

21
00:01:34,112-->00:01:36,513
Cậu có thể ngưng nói những câu
gây hiểu lầm như thế được không?

22
00:01:37,112-->00:01:39,113
Papa là một kẻ hư hỏng sao?

23
00:01:39,512-->00:01:43,513
Không, mấy cậu nhầm rồi! 
Cô bé này là người thân của Tonomachi.
Tớ chỉ đang trông cô bé một lúc thôi.

24
00:01:44,112-->00:01:47,513
Tonomachi-san? Là ai vậy?

25
00:01:48,112-->00:01:50,113
Đó là... bạn cùng lớp của tớ.

25
00:01:51,112-->00:01:58,513
Em không biết đấy...
Dù sao, đây là một cô bé rất dễ thương!

25
00:01:59,112-->00:02:09,113
Đồng ý. Có vẻ người nhà Tonomachi
biết chăm sóc em đấy. Cô bé rất dễ thương.

25
00:02:10,112-->00:02:14,113
Rio dễ thương thật sao, 
Miku-san, Yuzuri-chan?

25
00:02:15,112-->00:02:19,113
Phải. Em rất dễ thương.

25
00:02:19,512-->00:02:23,113
Em biết tên chị à?

25
00:02:24,112-->00:02:29,113
Kinh ngạc. 
Trường học đã bị rò rỉ thông tin sao?

25
00:02:29,312-->00:02:33,513
Có gì lạ đâu, Yuzuru?
Đó là do Tonomachi dạy đấy.
Còn Miku thì nổi tiếng quá mà.

25
00:02:34,112-->00:02:39,513
Đúng thật. Cũng có khả năng đó.

25
00:02:40,112-->00:02:47,113
Cô bé ngoan. Cô bé ngoan.
Phải rồi! 2 người có muốn đi hát
karaoke với bọn em không?

25
00:02:47,512-->00:02:51,113
Karaoke à? Nãy giờ đi bộ cũng mệt rồi.
Em muốn đi không, Rio?

25
00:02:52,112-->00:02:59,113
Miku-chan và Yuzuru-chan
hát hay lắm phải không?
Nếu vậy, Rio muốn nghe!

25
00:03:00,112-->00:03:05,513
Cảm ơn rất nhiều. 
Cô muốn đi không, Yuzuru-san?

25
00:03:06,512-->00:03:13,513
Khẳng định. Tất nhiên. 
Cậu muốn ngồi cạnh tớ không?

25
00:03:14,112-->00:03:17,513
Như thế không công bằng, Yuzuru-san!

25
00:03:18,112-->00:03:20,113
Bọn anh rất muốn nghe em hát đấy Rio.

25
00:03:20,512-->00:03:23,113
Papa muốn nghe Rio hát sao?

25
00:03:23,312-->00:03:24,513
Tất nhiên. Anh muốn nghe em hát lắm.

25
00:03:25,112-->00:03:28,113
Vậy thì tuyệt quá!

25
00:03:38,112-->00:03:40,313
Chúng tôi đi hát karaoke. 
Miku rõ ràng rất giỏi ca hát,
Yuzuru cũng vậy.

25
00:03:40,512-->00:03:43,513
Lần đầu tiên Rio đi hát nhưng cô bé
vẫn có thể hát một mình hoặc với người khác.

25
00:03:44,112-->00:03:46,113
Khi chúng tôi rời quán, 
ngoài trời đã mang màu buổi chiều.

25
00:03:46,512-->00:03:49,113
Giờ Rio phải đi rồi.

39
00:03:49,513-->00:03:51,113
Em đi gặp Marina à?

40
00:03:51,513-->00:03:53,513
Phải. Con đã hứa rồi.

41
00:03:54,113-->00:03:55,513
Anh hiểu rồi. Hôm nay em vui chứ?

42
00:03:56,113-->00:04:00,113
Con rất vui. Hôm nay con rất vui.

43
00:04:00,313-->00:04:01,513
Vậy thì tốt.

39
00:04:02,113-->00:04:07,513
Miku-chan, Yuzuru-chan,
lần sau hát tiếp nữa nhé?

40
00:04:08,113-->00:04:12,113
Được chứ. Chị rất mong ngày đó đấy!

40
00:04:12,513-->00:04:18,113
Đồng tình. Lần tới nên đi chung với
Kaguya và mọi người.

40
00:04:18,513-->00:04:24,113
Đúng đấy! Nếu tất cả chúng ta 
đi chung sẽ vui vẻ hơn nhiều.

40
00:04:24,513-->00:04:27,113
Này mấy cậu... chào cô bé đi.

40
00:04:27,513-->00:04:32,513
Đoàn tụ. Tạm biệt, Rio. Hẹn gặp lại.

40
00:04:33,513-->00:04:37,113
Bye bye, Rio-chan! Khi khác gặp lại nhé!

40
00:04:38,113-->00:04:40,513
Vâng! Lần sau đi hát tiếp nhé!

42
00:04:41,513-->00:04:45,513
Quên mất. Có vài chuyện 
tôi muốn hỏi em ấy.
Mà thôi kệ, vẫn còn lần tới mà.

43
00:04:47,113-->00:04:49,113
Rio quên nói với Papa việc này.

44
00:04:49,513-->00:04:51,113
Gì vậy?

45
00:04:51,513-->00:04:54,513
Nếu Papa muốn gặp Rio, 
hãy đến công viên nhé, được chứ?

46
00:04:54,913-->00:04:56,113
Rio thì thầm nhỏ nhẹ bên tai tôi.

47
00:04:56,312-->00:04:57,313
Hiểu rồi.

48
00:04:57,912-->00:04:59,113
Bye bye!

49
00:05:00,112-->00:05:04,113
Darling, Rio-chan đã nói gì với anh vậy?

50
00:05:04,513-->00:05:07,113
Rio nói Rio muốn đi chơi 
với chúng ta một lần nữa. 

51
00:05:07,313-->00:05:10,513
Không đời nào tôi nói việc Rio đã nói. 
Tôi không có sự lựa chọn nào cả

25
00:05:11,113-->00:05:16,113
Nhận xét. Đúng là một cô bé ngoan phải không?

25
00:05:16,312-->00:05:17,513
Cậu nói đúng.

25
00:05:17,812-->00:05:20,513
Tôi nghĩ về cô bé đó và một nụ cười 
hình thành trên khuôn mặt của tôi.

25
00:02:52,112-->00:02:33,113


25
00:02:52,112-->00:02:33,113


25
00:02:52,112-->00:02:33,113
