﻿0
00:16:51,112-->00:16:53,113
1) Làm sao tớ biết được?
2) Tớ nghĩ cậu đã có cảm tình 
với tớ... hoặc không.

1
00:16:53,512-->00:16:56,513
Tớ nghĩ cậu đã có cảm tình với tớ...hoặc không.

2
00:17:01,512-->00:17:03,113
Hở, có chuyện gì vậy, Marina?

3
00:17:03,512-->00:17:08,113
Đùa thế không vui đâu! 
Sao lại có chuyện đó chứ?

4
00:17:08,512-->00:17:14,113
Ừ, đúng vậy. Đó là một trò đùa. 
Tại cậu hỏi trước mà.

5
00:17:15,112-->00:17:22,513
Ừ...đúng vậy. Nhưng nó vẫn thái quá đấy.
Bởi vì tôi không khác gì Maria cả.

6
00:17:23,112-->00:17:25,513
Vậy tớ nên nói... xin lỗi ư?

7
00:17:26,112-->00:17:29,113
Cậu không cần phải nói. 
Tôi không quan tâm lắm đâu.

8
00:17:30,112-->00:17:34,513
Marina nói và quay mặt lại với tôi. 
Kỳ lạ thật, cô ấy có cảm giác 
khác với Marina mà tôi biết.

9
00:17:35,112-->00:17:40,113
Đúng vậy. Cô ấy có cảm giác khá giống Maria. 
Mối liên kết giữa họ, có lẽ không có gì lạ.

11
00:17:42,112-->00:17:44,113
A, Papa!

368
00:17:44,512-->00:17:48,113
Từ hướng của băng ghế, Rio háo hức suất hiện.

368
00:17:49,112-->00:17:51,113
Chào, Rio. Anh ở đây để gặp Rio nè.

368
00:17:52,112-->00:17:59,113
Rio rất vui khi Papa đến đó. 
Papa sẽ giúp Rio chứ?

368
00:18:00,112-->00:18:03,113
Dù anh có muốn, anh vẫn không biết 
mình đang tìm thứ gì nữa.

368
00:18:04,112-->00:18:11,113
Rio chưa nói là Rio đang tìm 
"thứ quan trọng nhất" sao?

368
00:18:11,512-->00:18:15,513
"Thứ quan trọng nhất" ư? 
Chuyện gì sẽ xảy ra 
nếu Rio tìm được thứ ấy?

368
00:18:16,112-->00:18:19,113
Mọi thứ sẽ biến mất.

368
00:18:19,512-->00:18:21,513
Mọi thứ... sẽ biến mất sao? Vậy là...

368
00:18:24,112-->00:18:31,113
Rio...Chị nghĩ em vẫn chưa nói hết 
cho Itsuka Shidou bí mật của thế giới này.

368
00:18:32,112-->00:18:33,513
Bí mật của thế giới này à?

368
00:18:34,112-->00:18:39,113
Đúng vậy. 
Bí mật của Eden chưa hoàn thiện này.

368
00:18:40,112-->00:18:44,513
Eden? Vậy thế giới này 
quả thật là Eden ư?

368
00:18:45,112-->00:18:50,113
Sao em chưa nói vậy, Rio? 
Chẳng phải em đã đồng ý với chị 
là sẽ nói tất cả cho cậu ấy rồi sao?

368
00:18:50,512-->00:18:55,113
Nếu Papa hứa sẽ giúp, vậy thì sẽ ổn thôi.

368
00:18:55,512-->00:18:59,113
Vậy đấy. Câu trả lời là gì, papa?

368
00:19:00,112-->00:19:04,113
Tớ đang nghĩ về việc giúp cậu. 
Dù sao đi nữa, điều này sẽ chứng minh 
những gì cậu nói.

368
00:19:05,112-->00:19:12,113
Cậu thật thà quá đi. 
Vậy một lần nữa, đó là lý do 
tôi không ghét cậu đấy.

368
00:19:13,112-->00:19:21,113
Rio cũng yêu tính thật thà của Papa lắm! 
Chị có thể nói với Papa về nó rồi. Marina.

368
00:19:22,112-->00:19:32,113
Vậy thì bắt đầu chủ đề chính.
Eden này có giới hạn thời gian.

368
00:19:32,512-->00:19:38,513
Giới hạn thời gian... 
Ý cậu là nếu chúng ta chờ đợi 
chúng có thể rời khỏi Eden này ư?

368
00:19:39,112-->00:19:44,113
Cũng gần như vậy.
Nếu đó là điều cậu muốn vậy thì 
đơn giản thôi, cậu chỉ cần ngồi đợi.

368
00:19:44,512-->00:19:55,113
Thế giới này rất không ổn định 
và sẽ tự biến mất. Theo lời của Rio, 
nó chỉ tồn tại được một tuần nữa thôi.

368
00:19:56,112-->00:20:02,113
Điều này bất ngờ quá. 
Có nghĩa là mọi thứ sẽ tự giải quyết.
Mình chỉ cần chờ đợi và mọi thứ sẽ...

368
00:20:04,112-->00:20:08,113
Đột nhiên, ngực tôi đau nhói. 
Sau khi tôi có thể nghĩ tại sao, 
Marina nói tiếp.

368
00:20:09,113-->00:20:11,513
Tuy nhiên, Rio không muốn như vậy. 

368
00:20:12,112-->00:20:19,113
Em ấy tin rằng thế giới này phải tiếp tục tồn tại. 
Bởi vì đó là mục tiêu của em ấy, 
nhiệm vụ của em ấy.

368
00:20:20,112-->00:20:34,113
Rio muốn mọi người có nhiều niềm vui. 
Papa, Mama và mọi người nữa! 
Và Rio nghĩ, điều đó có thể xảy ra ở đây.

368
00:20:35,112-->00:20:41,113
Nhưng Rinne sẽ không bao giờ thật sự vui vẻ. 
Phải khởi động lại thời gian 
và gánh chịu mọi thứ.

368
00:20:41,312-->00:20:43,513
Thế giới này sẽ tự khởi động lại phải không?

368
00:20:44,112-->00:20:51,513
Cậu đang bình tĩnh thật đấy, Itsuka Shidou. 
Tôi ấn tượng đấy. Tôi có thể ngã lòng 
cậu một lần nữa đấy.

368
00:20:52,112-->00:20:54,513
Cậu không bao giờ thích tớ 
trong tình huống này, mặc dù...

368
00:20:55,112-->00:21:03,113
Cậu để ý ư? 
Nhưng tôi không thể nói gì 
cho dù ý của cậu đúng hay sai đấy.

368
00:21:03,512-->00:21:07,513
Bởi vì Sonogami Rinne không phải là 
người điều khiển Eden này.

368
00:21:08,112-->00:21:11,113
Không phải Rinne sao? Điều đó không thể...

368
00:21:11,512-->00:21:21,113
Papa, thật ra Rio là 
người điều khiển Eden đến bây giờ. 
Rio làm việc rất cố gắng phải không?

368
00:21:21,512-->00:21:23,513
Rio là... "Eden" này ư?

368
00:21:24,112-->00:21:29,113
Tôi chưa bao giờ nghĩ đến khả năng này. 
Và tôi vẫn không thể tin 
những gì mình đang nghe.

368
00:21:30,112-->00:21:41,113
Nhưng Rio không có nhiều sức mạnh. 
Nên... Rio phải tìm " thứ quan trọng nhất ".

368
00:21:42,112-->00:21:45,513
Như em ấy nói. Rio không thể 
bảo hộ thế giới này chỉ với 
sức mạnh của mình. 

368
00:21:46,112-->00:21:54,513
Thế giới này cần một nửa nữa để hoàn tất.

368
00:21:55,112-->00:21:58,113
Và một nửa đó là "thứ quan trọng nhất".
Một mảnh của thế giới này. 

368
00:21:58,312-->00:22:01,513
Bằng chứng để Eden này tồn tại...
Rinne tồn tại.

368
00:22:02,112-->00:22:03,113
Đó là...

368
00:22:07,512-->00:22:10,113
Có gì đó hiện lên trong tâm trí tôi, 
là một chìa khóa. 

368
00:22:10,312-->00:22:14,513
Tôi không chắc chiếc chìa khóa này 
thuộc về Eden hay thế giới thực.
Nhưng chắc chắn đó là "thứ quan trọng nhất".

368
00:22:15,112-->00:22:20,513
Ánh nhìn đó cho thấy cậu đã nhận ra gì đó.
Đó có phải là đáp án không?

36
00:22:21,112-->00:22:25,513
Không, không phải nó. 
Tớ chỉ không thể nghĩ ra gì thôi.

368
00:22:26,112-->00:22:30,113
Vậy à? Được thôi, bỏ qua nó vậy.

368
00:22:30,513-->00:22:32,513
"Bỏ qua nó" ý cậu là sao?

368
00:22:33,113-->00:22:37,512
Tôi không biết. Tự hỏi cảm xúc của cậu đi.

368
00:22:38,112-->00:22:40,513
Marina thật sự làm ngơ nó, 
nghĩa là cô ấy đã biết gì đó chăng?

368
00:22:41,112-->00:22:45,513
Sao vậy, Papa?

368
00:22:46,512-->00:22:49,513
Không có gì đâu. Anh chỉ suy nghĩ thôi.

368
00:22:50,112-->00:22:57,113
Vậy ư? Vậy thì Papa sẽ giúp Rio 
tìm thứ đó chứ?

368
00:22:58,112-->00:23:00,113
Ừ...để xem...

368
00:23:00,512-->00:23:02,513
Itsuka Shidou.

368
00:23:03,112-->00:23:04,113
Hở?

368
00:23:05,112-->00:23:07,513
Cậu sẽ ân hận nếu cậu không nghĩ 
về điều này kỹ càng đấy. 

368
00:23:08,112-->00:23:16,513
Khi thế giới này biến mất, 
Maria, Rio, Sonogami Rinne và tôi 
cũng sẽ biến mất.

368
00:23:17,112-->00:23:18,513
Tớ hiểu.

368
00:23:19,112-->00:23:25,513
Tôi hiểu rất rõ. đó là lý do 
tôi cảm thấy đau nhói. 
Nỗi đau khi biết rằng 
tôi có thể mất mọi thứ một lần nữa.

368
00:23:26,112-->00:23:32,513
Tôi không phải làm gì và nó sẽ tự giải quyết 
Tuy nhiên, tôi sẽ thực sự yên lòng?
Đó có phải là lựa chọn đúng?

368
00:23:33,112-->00:23:40,513
Tâm tri tôi lung lay từ lựa chọn này 
đến lựa chọn khác như một quả lắc đồng hồ. 
Tôi không thể quyết định được.

368
00:23:41,112-->00:23:49,513
Ruler. Rinne có lần đã nói 
niềm vui có thể tìm kiếm ở thế giới này. 
Và tôi đã từ chối nó, nó không khác gì 
một hạnh phúc giả tạo.

368
00:23:50,112-->00:23:56,513
Tuy nhiên, sau đó tôi đã nói những lời
thiếu suy nghĩ. Tôi chưa bao giờ nghĩ 
thời gian của cậu ấy sẽ dừng lại. 

368
00:23:57,113-->00:24:01,513
Và bây giờ, tôi lại có thêm một cơ hội. 
Tôi sẽ để lịch sử lặp lại sao?

368
00:24:02,113-->00:24:04,513
Tôi có thể. Tôi không thể. 
Tôi có thể. Tôi không thể. 
Tôi có thể. Tôi không thể. 

368
00:24:05,113-->00:24:07,513
Một lần nữa và một lần nữa. 
Không thể quyết định được.

368
00:24:08,113-->00:24:11,113
Papa? Papa cảm thấy ổn chứ?

368
00:24:11,513-->00:24:14,513
Ừ, anh ổn. Dù sao thì anh đã quyết định rồi.

368
00:24:15,513-->00:24:19,513
Con người thiếu quyết đoán của cậu 
làm cậu nhanh trí hơn rồi à? 
Cậu nghiêm túc đấy chứ? 

368
00:24:20,113-->00:24:23,513
Hay cậu nghĩ nhiều quá 
nên não cậu nói linh tinh vậy?

368
00:24:24,113-->00:24:27,513
Tại sao lại thế? Tớ vẫn chưa hoàn toàn 
chắc chắn... Nhưng từ bây giờ tớ sẽ giúp. 

368
00:24:28,113-->00:24:32,113
Tớ vẫn muốn ở với hai người 
lâu hơn nữa. Tớ vẫn có nhiều thứ 
muốn nói với hai người.

368
00:24:33,113-->00:24:39,113
Rio yêu Papa lắm! Hãy tìm nó cùng nhau nha!

368
00:24:40,113-->00:24:49,113
Sẽ dễ hơn nếu cậu bỏ rơi bọn tôi...
Cậu thật ngốc quá đi...

368
00:24:50,113-->00:24:56,113
Sao vậy, Marina? Khuôn mặt Marina hơi đỏ đấy.

368
00:24:56,513-->00:25:01,513
Chị chỉ hơi lạnh thôi, 
em không cần phải lo đâu, Rio.

368
00:25:02,113-->00:25:07,113
Marina cười và xoa nhẹ đầu Rio. 
Trông họ giống như chị em vậy.

368
00:25:08,513-->00:25:10,513
Cậu sẽ là một người chị tốt đấy.

368
00:25:11,113-->00:25:19,513
Chị à? Rio không có chị.
Marina là một người bạn!

368
00:25:20,513-->00:25:23,513
Đúng nhỉ. Chỉ là một người bạn.

368
00:25:24,113-->00:25:26,113
Vậy à. Không phải tuyệt sao, Rio?

368
00:25:26,513-->00:25:33,113
Tôi không hiểu "không phải tuyệt sao" là gì. 
Nhưng không phải cậu nên về nhà sớm sao?

368
00:25:33,513-->00:25:38,513
Ah, đã trễ rồi ư? 
Không biết Kotori đã về chưa...
Maria đang đợi tớ ở nhà nữa.

368
00:25:39,113-->00:25:45,513
Tôi phải đưa Rio về ngủ nữa 
nên nhanh lên và đi đi.
Tôi vẫn mong chờ pudding lắm đó.

368
00:25:46,113-->00:25:51,113
Tớ đi ngay. Đổi lại, tớ sẽ giúp cậu 
ngày mai vào ngày nghỉ của tớ. 
Rio đồng ý chứ?

368
00:25:51,513-->00:25:57,113
Không sao ạ. Hãy làm hết mình nhé, Papa.

368
00:25:58,113-->00:25:59,513
Ừ, cùng cố gắng nhé.

368
00:26:00,113-->00:26:07,513
Vậy thì chúng ta có thể 
gặp nhau ở công viên. 
Tôi sẽ giao em ấy cho cậu, Itsuka Shidou.

368
00:26:08,113-->00:26:10,513
Tất nhiên rồi. Giờ tớ đi đây.
Mai gặp lại nhé.

368
00:26:11,113-->00:26:17,113
Vâng, tạm biệt, Papa! 
Rio mong chờ ngày mai lắm!

368
00:26:17,513-->00:26:23,113
Cậu nên đi đi...Gặp lại sau nhé, Shidou.
