﻿0
00:00:01,812-->00:00:03,513
Đi đến Khu dân cư?
1) Đồng ý
2) Không đồng ý

1
00:00:08,112-->00:00:09,913
Mình đã đi đến đây một cách tùy hứng, nhưng...

2
00:00:10,112-->00:00:13,113
Nếu tiếp tục, có khi tôi sẽ về nhà mất.
Tôi phải đi chỗ nào khác.

3
00:00:13,512-->00:00:14,513
Nè papa.

4
00:00:15,112-->00:00:16,513
Gì vậy, Rio?

5
00:00:17,112-->00:00:18,513
Chúng ta đang bị theo dõi.

6
00:00:18,812-->00:00:19,313
Hở?

7
00:00:19,512-->00:00:21,113
Có gì sai sao?

8
00:00:21,312-->00:00:23,513
Hình như anh nghe giọng Origami.
Haha... không thể được, phải không?

9
00:00:24,112-->00:00:25,313
Vậy sao? Papa quay lại đi.

10
00:00:25,512-->00:00:27,113
Quay lại sao?

11
00:00:27,312-->00:00:28,513
Tôi quay lại, nhưng không có ai cả.

12
00:00:28,912-->00:00:30,113
Đừng hù anh như thế...

13
00:00:31,512-->00:00:34,113
Thật là một sự trùng hợp.

14
00:00:34,512-->00:00:37,113
C... Cái gì? O... Origami?
Cậu đang làm gì ở đây?

15
00:00:37,312-->00:00:45,113
Trong lúc tớ vừa đi vừa nghĩ về cậu,
tình cờ tớ gặp cậu. Đúng là định mệnh.

16
00:00:45,312-->00:00:46,513
Không, không. Không phải do định mệnh đâu...

17
00:00:47,112-->00:00:52,913
Papa, người này là... Tobiichi Origami?

18
00:00:53,112-->00:00:54,513
À, Rio. Chờ anh một chút được không?

19
00:00:55,112-->00:01:03,113
Đứa trẻ đó là người nhà cậu à, Shidou?
Tên chị là Tobiichi Origami.
Cứ coi chị là vợ Shidou đi.

20
00:01:03,512-->00:01:06,513
Không, cô bé này là người thân của Tonomachi.
Khoan, cậu vừa nói gì? Đừng nói tào lao chứ.

21
00:01:07,112-->00:01:11,113
Nói tào lao sao? 
Tớ không hiểu cậu đang nói gì.

22
00:01:11,312-->00:01:12,513
Không, không, không, không....

23
00:01:13,112-->00:01:16,113
Papa đã kết hôn với Tobiichi-san sao?

24
00:01:16,512-->00:01:18,113
Tất nhiên là không! 
Bọn anh không có mối quan hệ đó!

25
00:01:18,512-->00:01:22,513
Vậy chị ấy là vợ lẽ của papa sao?

25
00:01:23,112-->00:01:27,513
Đúng vậy. Bọn chị có giấy kết hôn.

25
00:01:28,112-->00:01:30,513
Cậu lấy đâu ra giấy kết hôn?
Anh đã nói rồi. Đây chỉ là bạn cùng lớp thôi.

25
00:01:31,112-->00:01:36,113
Vâng, con biết.
Trông papa buồn cười lắm!

25
00:01:36,513-->00:01:39,113
Nghe này, Origami nói nhiều thứ
gây hiểu lầm lắm. Đừng để ý nhé Rio.

25
00:01:40,112-->00:01:44,113
Vâng. Được rồi!
Sao papa lại cho con biết?

25
00:01:44,512-->00:01:46,513
Không có gì...
Chỉ cần em đừng nghe lời chị ấy thôi.

25
00:01:47,112-->00:01:57,113
Shidou, tớ thắc mắc nếu cô bé này là
người nhà của Tonomachi Hiroto...
tại sao cô bé gọi cậu là papa?

25
00:01:57,512-->00:02:02,113
Vì papa là papa mà.

25
00:02:02,313-->00:02:04,113
Có lẽ vì tớ giống một ông bố chăng?

25
00:02:04,113-->00:02:08,113
Tớ hiểu rồi. Nếu không phải là 
con gái của cậu thì không sao.

25
00:02:08,513-->00:02:11,113
Nếu không phải vậy thì sẽ
có chuyện lớn phải không?

26
00:02:11,513-->00:02:13,913
Tên em là gì?

26
00:02:14,113-->00:02:15,113
Rio!

26
00:02:15,513-->00:02:19,513
Chị hiểu rồi. Bé ngoan, bé ngoan.

26
00:02:20,112-->00:02:26,513
Chị làm em nhột đấy.
Nhưng chị xoa dễ chịu lắm, Tobiichi-san.

26
00:02:27,112-->00:02:32,113
Cảm ơn em. Sao em biết tên chị?

26
00:02:32,512-->00:02:36,113
Em nghĩ papa đã dạy em.

26
00:02:36,512-->00:02:38,513
Chị rất cảm động trước tình yêu của Shidou.

26
00:02:39,112-->00:02:41,513
Cặp đôi gì thế này?
Tốt hơn tôi không nên xen vào...

26
00:02:46,112-->00:02:49,113
Sau khi chúng tôi đi chơi cùng nhau,
thời gian trôi qua và đã về chiều.

26
00:02:49,512-->00:02:51,513
Giờ Rio phải đi rồi.

39
00:02:52,113-->00:02:53,513
Em đi gặp Marina à?

40
00:02:54,113-->00:02:56,113
Phải. Con đã hứa rồi.

41
00:02:56,513-->00:02:58,113
Anh hiểu rồi. Hôm nay em vui chứ?

42
00:02:58,513-->00:03:02,113
Con rất vui. Hôm nay con rất vui.

43
00:03:02,513-->00:03:03,513
Vậy thì tốt.

39
00:03:04,113-->00:03:08,513
Vậy hẹn gặp lại nhé papa, Tobiichi-san.

40
00:03:09,113-->00:03:13,513
Ước gì Shidou và chị sẽ có
một đứa con gái như em.

40
00:03:14,113-->00:03:15,513
Này Origami... Chào đàng hoàng đi.

40
00:03:16,113-->00:03:17,513
Bye bye, Rio.

41
00:03:18,113-->00:03:19,113
Hẹn gặp lại, Rio.

41
00:03:19,513-->00:03:22,513
Vâng! Tạm biệt!

42
00:03:23,513-->00:03:27,513
Quên mất. Có vài chuyện 
tôi muốn hỏi em ấy.
Mà thôi kệ, vẫn còn lần tới mà.

43
00:03:28,113-->00:03:31,113
Rio quên nói với Papa việc này.

44
00:03:31,313-->00:03:32,813
Gì vậy?

45
00:03:33,113-->00:03:36,513
Nếu Papa muốn gặp Rio, 
hãy đến công viên nhé, được chứ?

46
00:03:36,812-->00:03:37,513
Rio thì thầm nhỏ nhẹ bên tai tôi.

47
00:03:37,912-->00:03:38,513
Hiểu rồi.

48
00:03:39,112-->00:03:40,513
Bye bye!

49
00:03:41,512-->00:03:45,113
Shidou, Rio đã nói gì với cậu vậy?

50
00:03:45,512-->00:03:47,513
Rio nói Rio muốn đi chơi 
với chúng ta một lần nữa. 

51
00:03:47,912-->00:03:50,513
Không đời nào tôi nói việc Rio đã nói. 
Tôi không có sự lựa chọn nào cả

26
00:03:51,112-->00:03:55,513
Vậy à? Tớ không nghĩ 
cậu lại thân với cô bé đến thế đấy.

26
00:03:55,912-->00:03:57,113
Cậu nói đúng. Tớ cũng không ngờ đấy.

27
00:03:57,312-->00:03:59,113
Họ không biết nhau nên cũng bình thường thôi.

28
00:03:59,512-->00:04:07,113
Shidou, hãy cẩn thận.
Tớ cảm thấy cô bé sẽ trở thành 
đối thủ mạnh mẽ trong tương lai.

29
00:04:07,312-->00:04:08,513
Thế là sao?

30
00:04:09,112-->00:04:11,113
Nghe Origami nhận xét,
tôi cười miễn cưỡng theo phản xạ.

31
00:03:57,112-->00:03:58,613


32
00:03:59,112-->00:04:02,113


33
00:03:54,112-->00:03:56,113

