﻿0
00:00:00,512-->00:00:02,113
Đi đến Khu mua sắm?
1) Đồng ý
2) Không đồng ý

1
00:00:06,512-->00:00:11,113
Chúng ta đã đến khu mua sắm rồi. Giờ thì sao?

2
00:00:22,112-->00:00:26,513
Những lúc thế này, tôi không có mục tiêu 
rõ ràng nào cả. Chắc là tôi sẽ mua
cái gì đó để uống.

3
00:00:27,112-->00:00:28,513
Nè papa.

4
00:00:39,112-->00:00:43,113
Gì vậy, Rio?

5
00:00:44,112-->00:00:56,113
Phía trước có một người đang nhìn chăm chú
mấy cái bánh bao. Đó là một người
rất tham ăn phải không?

6
00:00:57,112-->00:00:59,513
Một người rất tham ăn à? Ai vậy?

7
00:01:01,112-->00:01:02,513
Tohka?

8
00:01:05,112-->00:01:14,113
Đúng là anh rồi, Shidou!
Em thấy anh đi với người lạ.
Em không biết có nên gọi hay không.

9
00:01:15,112-->00:01:18,113
À, vậy sao? Thật trùng hợp
khi gặp em ở đây.

10
00:01:19,112-->00:01:26,113
Đúng là trùng hợp thật.
Nhân tiện Shidou, cô bé đó là ai vậy?

11
00:01:27,112-->00:01:29,113
Cô bé này là...

12
00:01:30,512-->00:01:37,113
Con sẽ bị ăn. Rio sẽ là đồ ăn sao?

13
00:01:38,112-->00:01:41,513
Không, không. Đúng là Tohka tham ăn,
nhưng không ăn thịt người. Cứ bình tĩnh.

14
00:01:42,112-->00:01:43,513
Thật sao?

15
00:01:45,112-->00:01:48,113
Thật đấy. Không có ai ăn thịt người đâu.

16
00:01:49,112-->00:01:55,113
Nhưng Rio biết. Tokisa...

17
00:01:56,112-->00:01:59,113
Dừng lại! Đó chỉ là giành cho kẻ thù  
hay đại loại vậy thôi!

18
00:02:00,112-->00:02:07,113
Thật sao? Nhưng nếu papa nói 
thì chắc là đúng rồi.

25
00:02:08,112-->00:02:10,513
Ừ. Cảm ơn em.
Giới thiệu với em, đây là Rio.

25
00:02:11,512-->00:02:19,113
Rio à? Tên dễ thương thật đấy!
Trông giống Rinne quá nhỉ.

25
00:02:20,112-->00:02:24,113
Vâng. Rinne là ma...

25
00:02:25,112-->00:02:28,513
À... thực ra cô bé này là người thân 
của Tonomachi. Anh đang trông bé một lúc.

25
00:02:29,512-->00:02:33,513
Người thân của Tonomachi à? 
Trông chẳng giống gì cả.

25
00:02:34,512-->00:02:38,113
Phải. Người thân không giống nhau nhiều.
Quan trọng hơn, em đang làm gì ở đây?

25
00:02:39,112-->00:02:48,113
Em ngửi thấy mùi thơm nên đã đến
cửa hàng bán bánh bao.
Nhìn này! Nó còn nóng đấy! Ngon lắm!

25
00:02:49,112-->00:02:51,113
Ừ, đúng vậy. Trông ngon thật.

25
00:02:52,112-->00:02:54,513
Tôi lừa cô ấy được rồi.
Đến giờ mọi thứ vẫn ổn...

25
00:02:55,512-->00:03:01,513
Papa, người này là... Yatogami Tohka?

25
00:03:02,512-->00:03:05,113
Đúng rồi. Em đừng gọi anh là "papa"
được không Rio?

26
00:03:06,112-->00:03:11,113
Sao vậy? Papa là papa mà.

26
00:03:12,112-->00:03:14,113
Nhưng đừng gọi ở đây...

26
00:03:15,112-->00:03:22,113
Shidou... tại sao Rio gọi anh là papa?

26
00:03:23,112-->00:03:26,113
À... Anh nghĩ chắc là do 
anh đang trông cô bé...

26
00:03:26,512-->00:03:37,113
Anh làm thế để cô bé nghe lời à?
Tóm lại, bây giờ anh đang làm 
ba của cô bé phải không?
Anh tốt thật đấy, Shidou.

26
00:03:38,112-->00:03:46,113
Đúng vậy! Papa rất tử tế!
Papa rất rất tốt bụng!

26
00:03:47,112-->00:03:53,513
Rio nói đúng. Shidou rất tốt bụng.

26
00:03:54,512-->00:03:59,113
Tohka-chan rất tham ăn.

26
00:04:04,112-->00:04:09,113
Nhưng đó là điểm tạo nên
sự dễ thương của chị phải không, Tohka-chan?

26
00:04:15,112-->00:04:20,513
Vậy chị có muốn đi cùng 
bọn em không, Tohka-chan?

26
00:04:21,512-->00:04:25,113
Ý hay đấy. Em có muốn vừa đi dạo
vừa ăn bánh bao không, Tohka?

26
00:04:26,112-->00:04:33,513
Được rồi. Đây là lần đầu tiên 
em ăn bánh bao đấy. Anh muốn thử không?

27
00:04:34,512-->00:04:41,113
Chị chắc chứ? Chị đã ăn rất nhiều
phải không, Tohka-chan?

28
00:04:42,112-->00:04:50,113
Không sao đâu. Chị chỉ vừa ăn
isobe-yaki thôi. Em muốn ăn không?

29
00:04:51,112-->00:05:01,113
Vâng. Cảm ơn chị, Tohka-chan.
Măm măm măm... Ngon lắm!

30
00:05:02,112-->00:05:08,113
Ngon đúng không? 
Isobe-yaki là một trong những món 
yêu thích của chị đấy.

31
00:05:09,112-->00:05:13,113
Chị còn thích món khác à?

32
00:05:14,112-->00:05:20,513
Ừ. Chị cũng thích Kinako nữa.

33
00:05:21,512-->00:05:26,113
Nó ngon lắm, phải không?

34
00:05:27,112-->00:05:29,513
Em muốn thử không?

35
00:05:30,512-->00:05:38,113
Vâng. Tohka-chan cũng thân thiện
giống papa vậy.

36
00:05:39,112-->00:05:46,113
Đúng đấy! Chị thân thiện và không tham ăn đâu!

37
00:05:47,112-->00:05:52,113
Chỉ khi chị đã ăn rồi thôi.

38
00:05:56,112-->00:05:58,113
Hahaha...

39
00:05:59,112-->00:06:02,113
Có gì buồn cười chứ, Shidou?

40
00:06:03,113-->00:06:05,113
Không, không có gì đâu.

41
00:06:06,113-->00:06:10,113
Tốt nhất tôi nên giữ bí mật. 
Tôi không nên nói hai người 
đã gặp nhau ở đây.

42
00:06:15,113-->00:06:18,113
Sau khi đi qua những khu mua sắm 
trong khi Tohka mua thức ăn, 
thời gian trôi qua và đã về chiều.

43
00:06:19,113-->00:06:23,513
Giờ Rio phải đi rồi.

39
00:06:24,513-->00:06:27,113
Em đi gặp Marina à?

40
00:06:28,113-->00:06:31,513
Phải. Con đã hứa rồi.

41
00:06:32,513-->00:06:34,513
Anh hiểu rồi. Hôm nay em vui chứ?

42
00:06:35,513-->00:06:41,513
Con rất vui. Hôm nay con rất vui.

43
00:06:42,513-->00:06:45,113
Vậy thì tốt.

39
00:06:46,113-->00:06:52,113
Vậy hẹn gặp lại nhé papa, Tohka-chan.

40
00:06:53,113-->00:06:57,113
Hẹn gặp lại, Rio.

41
00:06:58,113-->00:07:00,113
Hẹn gặp lại, Rio.

42
00:07:01,113-->00:07:05,513
Quên mất. Có vài chuyện 
tôi muốn hỏi em ấy.
Mà thôi kệ, vẫn còn lần tới mà.

43
00:07:07,113-->00:07:11,113
Rio quên nói với Papa việc này.

44
00:07:12,112-->00:07:14,113
Gì vậy?

45
00:07:15,112-->00:07:19,113
Nếu Papa muốn gặp Rio, 
hãy đến công viên nhé, được chứ?

46
00:07:20,112-->00:07:22,113
Rio thì thầm nhỏ nhẹ bên tai tôi.

47
00:07:23,112-->00:07:25,113
Hiểu rồi.

48
00:07:25,512-->00:07:28,113
Bye bye!

49
00:07:30,112-->00:07:36,113
Rio đã nói gì với anh vậy Shidou?

50
00:07:37,112-->00:07:40,513
Rio nói Rio muốn đi chơi 
với chúng ta một lần nữa. 

51
00:07:41,112-->00:07:44,513
Không đời nào tôi nói việc Rio đã nói. 
Tôi không có sự lựa chọn nào cả

52
00:07:45,112-->00:07:51,113
Vậy à? Rio dễ thương thật đấy.

53
00:07:52,112-->00:07:54,513
Tôi gật đầu và nở nụ cười với Tohka.

54
00:08:00,512-->00:08:07,113
Bình thường luôn rất vui vẻ. 
Hôm nay hơi trống vắng
khi chỉ có hai chúng ta nhỉ.

55
00:08:08,112-->00:08:11,113
Đúng vậy, bữa tiệc ngày hôm qua cũng vậy.

56
00:08:11,512-->00:08:14,513
Tôi đang dọn dẹp sau khi kết thúc bữa ăn tối của chúng tôi. Nó thật tuyệt và thư giãn.

57
00:08:15,112-->00:08:19,113
Hiếm khi tôi nấu ăn với Maria thế này. 
Theo lời Maria, cậu ấy đã học nấu ăn 
từ Rinne ở nhà bên cạnh.

58
00:08:19,512-->00:08:24,113
Tôi cũng nghĩ đến việc đến căn nhà ấy 
nhưng Maria nói tôi không được đến
vì Rinne không muốn tôi tới. 
Có vẻ đó là một buổi học khá hỗn độn.

59
00:08:25,112-->00:08:27,113
Cậu có nhiều bí mật nhỉ, Maria.

60
00:08:28,112-->00:08:33,113
Nhiều bí mật? Ý cậu là sao, Shidou?

41
00:08:34,112-->00:08:38,113
Không phải cậu bí mật hỏi Rinne 
chỉ cậu nấu ăn tốt hơn sao? 
Kể cả việc cậu làm cùng cậu ấy sáng nay nữa.

42
00:08:39,112-->00:08:46,513
Vậy thôi ư? Còn nữa, không phải 
cậu nói thích Rinne nấu ăn hơn tớ sao?

43
00:08:47,512-->00:08:52,513
Không, tớ không nói thế. 
Rinne có phong cách của cậu ấy, 
cậu có của cậu. Cả hai đều rất tuyệt 
khi làm theo cách của mình.

44
00:08:53,112-->00:09:04,113
Tớ chỉ bắt chước giống cậu thôi, Shidou.
Vì thế, tớ không cảm thấy vui trọn vẹn 
khi cậu khen tớ như vậy đâu.

48
00:09:05,112-->00:09:09,513
Đừng nói vậy, Maira. 
Miễn là cậu đặt trái tim cậu 
vào món ăn và mọi người, 
thì cậu sẽ là một đầu bếp giỏi thôi.

49
00:09:10,112-->00:09:19,113
Cảm ơn, Shidou. 
Lần tới, tớ sẽ đưa nó vào 
để cậu thích món ăn của tớ.

50
00:09:20,112-->00:09:22,113
Chắc chắn rồi.

51
00:09:23,112-->00:09:26,513
Nhìn vào nụ cười tinh khiết trên khuôn mặt Maria mà tôi không thể giúp nhưng làm khuôn mặt tôi đỡ xấu hổ và sau đó đôi mắt tôi nhìn vào đồng hồ

52
00:09:27,112-->00:09:31,113
Maria. Cậu không phiền nếu tớ đi dạo 
một tí chứ? Cũng giống hôm qua thôi.

53
00:09:32,112-->00:09:38,113
Tớ không phiền đâu. Tớ sẽ không hỏi 
nơi cậu đi nhưng hãy cẩn thận nhé.

54
00:09:39,112-->00:09:41,113
Tớ hứa, cảm ơn cậu.

113
00:09:42,112-->00:09:45,513
Rio đã nói tôi đến gặp Rio ở công viên. 
Không ai ở đó khi đã trễ như vậy.




114
00:09:50,112-->00:09:52,513
Ủa, Shidou?

114
00:09:53,512-->00:09:56,113
Ồ, Rinne. Mừng cậu về nhà.

114
00:09:57,112-->00:10:02,113
Tớ về rồi, Shidou. 
Cậu định ra ngoài bây giờ ư?

114
00:10:03,112-->00:10:05,113
Um...đúng vậy. Chỉ tập thể dục một ít 
sau khi ăn tối thôi.

1
00:10:06,112-->00:10:15,113
Hôm nay nữa ư? Cậu còn đi như hôm qua sao?
Không lẽ cậu đang giấu chuyện gì sau lưng tớ ư?

1
00:10:16,112-->00:10:18,113
Không, không phải đâu.

1
00:10:19,112-->00:10:27,113
Ổn mà, Rinne. Cậu ấy ăn hơi nhiều 
nên cậu ấy đi dạo để tiêu hóa thôi mà.

1
00:10:28,112-->00:10:28,113
Vậy à? Đừng về trễ đấy. 
Hôm qua cậu về trễ lắm đấy.

1
00:10:41,112-->00:10:44,113
X... Xin lõi cậu. Tớ chắc chắn hôm nay
sẽ về sớm hơn. Tớ đi nhé.

1
00:10:45,112-->00:10:54,113
Ừ, cẩn thận nhé. Tớ sẽ về nhà 
sau khi tớ dọn dẹp xong. 
Và cũng đừng làm Maria-chan lo lắng đấy nhé.

368
00:10:55,112-->00:10:57,113
Tớ biết mà.

368
00:10:58,112-->00:11:01,113
Vậy được rồi, chúc ngủ ngon.

368
00:11:02,112-->00:11:04,513
Ừ, chúc ngủ ngon.

368
00:11:06,112-->00:11:11,313
Thoát chết trong gang tấc nhỉ.

368
00:11:12,112-->00:11:14,113
Ý cậu là sao?

368
00:11:15,112-->00:11:19,113
Không, không có gì quan trọng đâu.

368
00:11:20,112-->00:11:23,113
Maria biết tôi định đi ra ngoài 
gặp Marina rồi ư?

368
00:11:24,112-->00:11:27,113
Chắc tôi lo lắng quá. 
Mặc dù vậy, cậu ấy có thể nghi ngờ 
về việc tôi đang giấu gì đó.

368
00:11:28,112-->00:11:31,113
Tớ đi đây. Ồ phải rồi, cậu có muốn 
tớ mua gì ở cửa hàng tiện lợi không?

368
00:11:32,112-->00:11:38,113
Để tớ nghĩ. Làm phiền cậu mua
vài cái pudding trên đường về nhà được không?

368
00:11:39,112-->00:11:42,113
Pudding, OK.
Cậu còn cần gì nữa không?

68
00:11:43,112-->00:11:50,113
Được rồi cậu. 
Đi cẩn thận nhé. Gặp cậu sau.

368
00:11:51,112-->00:11:53,513
Ừm, gặp lại... Hở?

368
00:11:54,512-->00:11:59,113
Vì lý do nào đó, tôi có cảm giác xấu
về việc Maria vừa nói. 
Sao cũng được, tôi phải nhanh đến công viên.

368
00:12:03,112-->00:12:05,113
Rio có lẽ ở đâu đó quanh đây...

368
00:12:06,112-->00:12:10,113
Rio nói là ở công viên nhưng 
ý Rio là ở công viên nhỏ nào đó
gần đây hay ngay tại đây nhỉ?

68
00:12:11,112-->00:12:14,113
Tôi đã thử công viên nhỏ nhưng 
tôi không thấy Rio đâu cả 
nên tôi đã thử nơi này...

368
00:12:15,112-->00:12:17,513
Có lẽ ý Rio là công viên này.

368
00:12:18,512-->00:12:22,113
Cậu thật sự đã đến, Itsuka Shidou.

368
00:12:23,112-->00:12:25,513
Chào, Marina. Từ hôm qua đến giờ
tớ vẫn chưa gặp cậu.

368
00:12:26,512-->00:12:32,513
Xem lại cách quá thân thiện của cậu đi. 
Tôi không ở đây để gặp cậu đâu.

368
00:12:38,112-->00:12:42,513
Không, thật ra tôi đã đánh liều để gặp cậu!

368
00:12:43,512-->00:12:45,113
Sao cơ?

368
00:12:46,112-->00:12:53,113
Vậy cậu nghĩ cái nào mới là thật? 
Cậu thử đoán xem.

368
00:12:54,512-->00:12:56,113
Tớ...

368
00:12:57,112-->00:13:05,513
1) Làm sao tớ biết được?
2) Tớ nghĩ cậu đã có cảm tình 
với tớ... hoặc không.

368
00:13:22,112-->00:13:25,113
Làm sao tớ biết được?
Chỉ có cậu biết thôi mà.

368
00:13:27,112-->00:13:34,113
Một câu trả lời của học sinh gương mẫu đấy.
Chẳng có gì thú vị cả.

368
00:13:35,112-->00:13:38,113
"Chẳng có gì thú vị" sao? 
Đây đâu phải vấn đề phải không?

368
00:13:39,112-->00:13:45,113
Được rồi, tôi biết rồi.
Tôi không mong đợi gì 
từ đồng minh của công lý đâu.

368
00:13:46,112-->00:13:48,113
Vậy là sao?

368
00:13:49,112-->00:13:53,513
Vì lý do nào đó, 
Marina có vẻ không thích câu trả lời. 
Tôi không biết tại sao.

368
00:13:55,112-->00:13:57,513
A, Papa!

368
00:13:58,512-->00:14:01,113
Từ hướng của băng ghế, Rio háo hức suất hiện.

368
00:14:02,513-->00:14:04,513
Chào, Rio. Anh ở đây để gặp Rio nè.

368
00:14:05,512-->00:14:13,113
Rio rất vui khi Papa đến đó. 
Papa sẽ giúp Rio chứ?

368
00:14:14,112-->00:14:16,513
Dù anh có muốn, anh vẫn không biết 
mình đang tìm thứ gì nữa.

368
00:14:17,512-->00:14:26,113
Rio chưa nói là Rio đang tìm 
"thứ quan trọng nhất" sao?

368
00:14:27,112-->00:14:30,113
"Thứ quan trọng nhất" ư? 
Chuyện gì sẽ xảy ra 
nếu Rio tìm được thứ ấy?

368
00:14:31,112-->00:14:35,113
Mọi thứ sẽ biến mất.

368
00:14:36,112-->00:14:39,113
Mọi thứ... sẽ biến mất sao? Vậy là...

368
00:14:40,112-->00:14:48,513
Rio...Chị nghĩ em vẫn chưa nói hết 
cho Itsuka Shidou bí mật của thế giới này.

368
00:14:49,512-->00:14:51,513
Bí mật của thế giới này à?

368
00:14:52,512-->00:14:58,113
Đúng vậy. 
Bí mật của Eden chưa hoàn thiện này.

368
00:14:59,112-->00:15:02,113
Eden? Vậy thế giới này 
quả thật là Eden ư?

368
00:15:03,112-->00:15:09,113
Sao em chưa nói vậy, Rio? 
Chẳng phải em đã đồng ý với chị 
là sẽ nói tất cả cho cậu ấy rồi sao?

368
00:15:10,112-->00:15:15,113
Nếu Papa hứa sẽ giúp, vậy thì sẽ ổn thôi.

368
00:15:16,112-->00:15:20,113
Vậy đấy. Câu trả lời là gì, papa?

368
00:15:21,112-->00:15:24,513
Tớ đang nghĩ về việc giúp cậu. 
Dù sao đi nữa, điều này sẽ chứng minh 
những gì cậu nói.

368
00:15:25,512-->00:15:33,113
Ahaha! Cậu thật thà quá đi. 
Vậy một lần nữa, đó là lý do 
tôi không ghét cậu đấy.

368
00:15:34,112-->00:15:43,513
Rio cũng yêu tính thật thà của Papa lắm! 
Chị có thể nói với Papa về nó rồi. Marina.

368
00:15:44,512-->00:15:55,113
Vậy thì bắt đầu chủ đề chính.
Eden này có giới hạn thời gian.

368
00:15:56,112-->00:16:00,113
Giới hạn thời gian... 
Ý cậu là nếu chúng ta chờ đợi 
chúng có thể rời khỏi Eden này ư?

368
00:16:01,113-->00:16:06,113
Cũng gần như vậy.
Nếu đó là điều cậu muốn vậy 
thì đơn giản thôi, cậu chỉ cần ngồi đợi.

368
00:16:06,512-->00:16:18,113
Thế giới này rất không ổn định 
và sẽ tự biến mất. Theo lời của Rio, 
nó chỉ tồn tại được một tuần nữa thôi.

368
00:16:19,112-->00:16:23,113
Điều này bất ngờ quá. 
Có nghĩa là mọi thứ sẽ tự giải quyết.
Mình chỉ cần chờ đợi và mọi thứ sẽ...

368
00:16:25,512-->00:16:29,513
Đột nhiên, ngực tôi đau nhói. 
Sau khi tôi có thể nghĩ tại sao, 
Marina nói tiếp.

368
00:16:30,112-->00:16:32,513
Tuy nhiên, Rio không muốn như vậy. 

368
00:16:33,112-->00:16:41,113
Em ấy tin rằng thế giới này phải tiếp tục tồn tại. 
Bởi vì đó là mục tiêu của em ấy, 
nhiệm vụ của em ấy.

368
00:16:42,112-->00:16:57,113
Rio muốn mọi người có nhiều niềm vui. 
Papa, Mama và mọi người nữa! 
Và Rio nghĩ, điều đó có thể xảy ra ở đây.

368
00:16:58,112-->00:17:01,113
Nhưng Rinne sẽ không bao giờ thật sự vui vẻ. 
Phải khởi động lại thời gian 
và gánh chịu mọi thứ.

368
00:17:01,512-->00:17:03,513
Thế giới này sẽ tự khởi động lại phải không?

368
00:17:04,112-->00:17:11,513
Cậu đang bình tĩnh thật đấy, Itsuka Shidou. 
Tôi ấn tượng đấy. Tôi có thể ngã lòng 
cậu một lần nữa đấy.

368
00:17:12,512-->00:17:15,113
Cậu không bao giờ thích tớ 
trong tình huống này, mặc dù...

368
00:17:16,112-->00:17:25,113
Cậu để ý ư? 
Nhưng tôi không thể nói gì 
cho dù ý của cậu đúng hay sai đấy.

368
00:17:25,512-->00:17:29,113
Bởi vì Sonogami Rinne không phải là 
người điều khiển Eden này.

368
00:17:30,112-->00:17:33,113
Không phải Rinne sao? Điều đó không thể...

368
00:17:34,112-->00:17:44,113
Papa, thật ra Rio là 
người điều khiển Eden đến bây giờ. 
Rio làm việc rất cố gắng phải không?

368
00:17:44,512-->00:17:47,113
Rio là... " Eden " này ư?

368
00:17:47,512-->00:17:51,113
Tôi chưa bao giờ nghĩ đến khả năng này. 
Và tôi vẫn không thể tin 
những gì mình đang nghe.

368
00:17:52,112-->00:18:04,113
Nhưng Rio không có nhiều sức mạnh. 
Nên...Rio phải tìm " thứ quan trọng nhất ".

368
00:18:05,112-->00:18:14,113
Như em ấy nói. Rio không thể 
bảo hộ thế giới này chỉ với 
sức mạnh của mình. 

368
00:18:14,512-->00:18:18,113
Thế giới này cần một nửa nữa để hoàn tất.

368
00:18:18,512-->00:18:21,113
Và một nửa đó là "thứ quan trọng nhất".
Một mảnh của thế giới này. 

368
00:18:21,312-->00:18:23,113
Bằng chứng để Eden này tồn tại...
Rinne tồn tại.

368
00:18:24,112-->00:18:26,113
Đó là...

368
00:18:31,112-->00:18:36,113
Có gì đó hiện lên trong tâm trí tôi, 
là một chìa khóa. Tôi không chắc 
chiếc chìa khóa này thuộc về Eden 
hay thế giới thực.
Nhưng chắc chắn đó là "thứ quan trọng nhất".

368
00:18:37,112-->00:18:42,513
Ánh nhìn đó cho thấy cậu đã nhận ra gì đó.
Đó có phải là đáp án không?

36
00:18:43,512-->00:18:46,513
Không, không phải nó. 
Tớ chỉ không thể nghĩ ra gì thôi.

368
00:18:47,112-->00:18:52,113
Vậy à? Được thôi, bỏ qua nó vậy.

368
00:18:53,112-->00:18:55,113
"Bỏ qua nó" ý cậu là sao?

368
00:18:56,112-->00:19:00,512
Tớ không biết. Tự hỏi cảm xúc của cậu đi.

368
00:19:01,512-->00:19:04,113
Marina thật sự làm ngơ nó, 
nghĩa là cô ấy đã biết gì đó chăng?

368
00:19:05,112-->00:19:11,113
Sao vậy, Papa?

368
00:19:12,112-->00:19:14,113
Không có gì đâu. Anh chỉ suy nghĩ thôi.

368
00:19:15,112-->00:19:23,113
Vậy ư? Vậy thì Papa sẽ giúp Rio 
tìm thứ đó chứ?

368
00:19:24,112-->00:19:26,113
Ừ...để xem...

368
00:19:27,112-->00:19:29,513
Itsuka Shidou.

368
00:19:30,512-->00:19:31,513
Hở?

368
00:19:33,112-->00:19:35,513
Cậu sẽ ân hận nếu cậu không nghĩ 
về điều này kỹ càng đấy. 

368
00:19:36,112-->00:19:45,113
Khi thế giới này biến mất, 
Maria, Rio, Sonogami Rinne và tôi 
cũng sẽ biến mất.

368
00:19:46,112-->00:19:48,113
Tớ hiểu.

368
00:19:49,112-->00:19:52,513
Tôi hiểu rất rõ. đó là lý do 
tôi cảm thấy đau nhói. 
Nỗi đau khi biết rằng 
tôi có thể mất mọi thứ một lần nữa.

368
00:19:53,512-->00:19:58,113
Tôi không phải làm gì và nó sẽ tự giải quyết 
Tuy nhiên, tôi sẽ thực sự yên lòng?
Đó có phải là lựa chọn đúng?

368
00:19:59,112-->00:20:04,113
Tâm tri tôi lung lay từ lựa chọn này 
đến lựa chọn khác như một quả lắc đồng hồ. 
Tôi không thể quyết định được.

368
00:20:04,513-->00:20:09,513
Ruler. Rinne có lần đã nói 
niềm vui có thể tìm kiếm ở thế giới này. 
Và tôi đã từ chối nó, nó không khác gì 
một hạnh phúc giả tạo.

368
00:20:10,113-->00:20:14,513
Tuy nhiên, sau đó tôi đã nói những lời
thiếu suy nghĩ. Tôi chưa bao giờ nghĩ 
thời gian của cậu ấy sẽ dừng lại. 

368
00:20:15,113-->00:20:18,513
Và bây giờ, tôi lại có thêm một cơ hội. 
Tôi sẽ để lịch sử lặp lại sao?

368
00:20:19,113-->00:20:21,113
Tôi có thể. Tôi không thể. 
Tôi có thể. Tôi không thể. 
Tôi có thể. Tôi không thể. 

368
00:20:21,513-->00:20:24,113
Một lần nữa và một lần nữa. 
Không thể quyết định được.

368
00:20:24,513-->00:20:27,113
Papa? Papa cảm thấy ổn chứ?

368
00:20:28,113-->00:20:32,113
Ừ, anh ổn. Dù sao thì anh đã quyết định rồi.

368
00:20:33,113-->00:20:37,513
Con người thiếu quyết đoán của cậu 
làm cậu nhanh trí hơn rồi à? 
Cậu nghiêm túc đấy chứ? 

368
00:20:38,113-->00:20:41,113
Hay cậu nghĩ nhiều quá 
nên não cậu nói linh tinh vậy?

368
00:20:42,113-->00:20:44,113
Tại sao lại thế? Tớ vẫn chưa hoàn toàn 
chắc chắn... Nhưng từ bây giờ tớ sẽ giúp. 

368
00:20:44,513-->00:20:48,113
Tớ vẫn muốn ở với hai người 
lâu hơn nữa. Tớ vẫn có nhiều thứ 
muốn nói với hai người.

368
00:20:49,113-->00:20:56,113
Rio yêu Papa lắm! Hãy tìm nó cùng nhau nha!

368
00:20:57,113-->00:21:06,513
Sẽ dễ hơn nếu cậu bỏ rơi bọn tôi...
Cậu thật ngốc quá đi...

368
00:21:07,513-->00:21:15,113
Sao vậy, Marina? Khuôn mặt Marina hơi đỏ đấy.

368
00:21:16,113-->00:21:21,113
Chị chỉ hơi lạnh thôi, 
em không cần phải lo đâu, Rio.

368
00:21:22,113-->00:21:26,113
Marina cười và xoa nhẹ đầu Rio. 
Trông họ giống như chị em vậy.

368
00:21:27,113-->00:21:29,113
Cậu sẽ là một người chị tốt đấy.

368
00:21:30,113-->00:21:39,513
Chị à? Rio không có chị.
Marina là một người bạn!

368
00:21:41,113-->00:21:43,513
Đúng nhỉ. Chỉ là một người bạn.

368
00:21:44,513-->00:21:47,113
Vậy à. Không phải tuyệt sao, Rio?

368
00:21:48,113-->00:21:55,113
Tôi không hiểu "không phải tuyệt sao" là gì. 
Nhưng không phải cậu nên về nhà sớm sao?

368
00:21:56,113-->00:21:59,513
Ah, đã trễ rồi ư? 
Không biết Kotori đã về chưa...
Maria đang đợi tớ ở nhà nữa.

368
00:22:00,113-->00:22:07,113
Tôi phải đưa Rio về ngủ nữa 
nên nhanh lên và đi đi.
Tôi vẫn mong chờ pudding lắm đó.

368
00:22:08,113-->00:22:12,513
Tớ đi ngay. Đổi lại, tớ sẽ giúp cậu 
ngày mai vào ngày nghỉ của tớ. 
Rio đồng ý chứ?

368
00:22:13,113-->00:22:18,513
Không sao ạ. Hãy làm hết mình nhé, Papa.

368
00:22:19,513-->00:22:22,113
Ừ, cùng cố gắng nhé.

368
00:22:23,113-->00:22:30,513
Vậy thì chúng ta có thể 
gặp nhau ở công viên. 
Tôi sẽ giao em ấy cho cậu, Itsuka Shidou.

368
00:22:31,513-->00:22:34,113
Tất nhiên rồi. Giờ tớ đi đây.
Mai gặp lại nhé.

368
00:22:35,113-->00:22:41,113
Vâng, tạm biệt, Papa! 
Rio mong chờ ngày mai lắm!

368
00:22:42,513-->00:22:48,113
Cậu nên đi đi...Gặp lại sau nhé, Shidou.

368
00:22:56,113-->00:23:01,113
Tôi tạm biệt Marina và Rio 
và mua một vài pudding trên đường về nhà. 
Trong khi đó, mọi thứ tôi có thể nghĩ 
là câu trả lời tôi chưa quyết định.

368
00:23:01,513-->00:23:04,113
Tôi đã hứa sẽ giúp Rio 
nhưng tôi đã biết thứ mà Rio đang tìm. 
Vì thế tôi không thể để Rio tìm thấy nó. 

368
00:23:04,313-->00:23:07,113
Kể cả nếu họ đi theo Rinne 
và chuyện gì khác xảy ra, 
tôi không thể để Rio tìm được.

368
00:23:08,513-->00:23:11,113
Mình nên làm gì đây...?

368
00:23:12,112-->00:23:15,113
Tôi tiếp tục vừa đi vừa hỏi 
bản thân câu hỏi đó. 
Cuối cùng, không có câu trả lời nào cả.

368
00:23:20,112-->00:23:24,113
Ngày thứ 3