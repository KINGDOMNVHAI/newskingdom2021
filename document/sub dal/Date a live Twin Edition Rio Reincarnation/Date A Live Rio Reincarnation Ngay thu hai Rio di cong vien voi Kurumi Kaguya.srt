﻿0
00:00:02,512-->00:00:04,113
Đi đến Công viên?
1) Đồng ý
2) Không đồng ý

1
00:00:08,112-->00:00:10,913
Không có ai cả.
Chơi với Rio ở đây là tốt nhất.

2
00:00:11,112-->00:00:13,313
Tôi không dám tưởng tượng 
nếu có ai đó phát hiện và tố cáo tôi.

3
00:00:13,512-->00:00:17,113
Chúng ta sẽ chơi ở đây sao papa?

4
00:00:17,312-->00:00:20,513
Xem nào... Chúng ta ở đây một chút 
rồi đi chỗ nào đông người hơn nhé.

5
00:00:21,112-->00:00:26,113
Vâng! Papa chỉ đường cho con nhé?

6
00:00:26,312-->00:00:27,513
Tuân lệnh, thưa công chúa.

7
00:00:28,112-->00:00:30,913
Rio là hime-sama (công chúa) sao?

8
00:00:31,112-->00:00:33,113
Đúng vậy. Em dễ thương như công chúa vậy.

9
00:00:33,512-->00:00:37,913
Giống như papa là papa phải không?

10
00:00:38,112-->00:00:39,513
Có hơi khác chút... Nhưng mà, gì vậy...

11
00:00:41,112-->00:00:43,113
A...re...?

12
00:00:44,112-->00:00:45,313
Hở? Chuyện gì vậy, Rio?

13
00:00:45,512-->00:00:46,513
Chuyện gì đang xảy ra vậy?

14
00:00:48,512-->00:00:52,513
Bắt được công chúa rồi, Shidou!

15
00:00:53,112-->00:00:55,113
Con bị bắt rồi.

16
00:00:55,312-->00:00:58,513
Kaguya? Cậu nghĩ cậu đang làm gì vậy?

17
00:00:59,112-->00:01:08,113
Cậu nghĩ tôi có thể bình tĩnh sau khi
nghe từ "công chúa" sao? 
Máu của công chúa sẽ tăng sức mạnh cho tôi...

18
00:01:09,112-->00:01:17,113
Cái gì vậy? Ai dám phá khoảnh khắc của ta?

19
00:01:19,112-->00:01:24,113
Chán thật! Cô đang làm đau một cô bé đấy.

20
00:01:24,512-->00:01:28,113
Kurumi? Sao cô làm thế hả?

21
00:01:28,512-->00:01:30,913
Ku... Kurumi. Cậu đang làm gì ở đây?

22
00:01:31,112-->00:01:39,113
Tớ đến giúp cậu thoát khỏi trò đùa của
Kaguya-san đấy. Tớ làm phiền cậu à?

23
00:01:39,312-->00:01:42,113
Không, tớ không rõ những gì đang xảy ra.
Nhưng có lẽ bọn tớ vừa được cậu cứu đấy.

24
00:01:42,512-->00:01:46,513
Cứu con sao? Con không biết.

25
00:01:47,112-->00:01:55,513
Tôi không ngờ cô làm gián đoạn 
nghi lễ thiêng liêng của tôi...
Cô nghĩ đây là trò chơi à?

25
00:01:56,112-->00:02:03,113
Chơi sai cách rồi đấy.
Tại sao cô không tìm hiểu thêm một chút đi?

25
00:02:04,112-->00:02:16,113
Được rồi. Người hầu của tôi, Shidou. 
Cô bé đó là ai? Đó thực sự là công chúa sao?

25
00:02:16,312-->00:02:20,113
Không, cô bé này là Rio. 
Đây là... người thân của Tonomachi.

25
00:02:21,112-->00:02:30,513
Ra là vậy. Đúng rồi!
Đó là hy vọng cuối cùng từ người bạn đời.
Đó là danh tính thực sự của cô bé sao?

25
00:02:30,912-->00:02:32,313
Không, không có những chuyện ngoài lề đó đâu...

25
00:02:32,512-->00:02:35,113
Trên thực tế họ không có quan hệ thân thích.
Vì vậy Tonomachi không phải là bạn đời của tôi.

25
00:02:35,512-->00:02:42,513
Rio là công chúa sao?

25
00:02:43,512-->00:02:48,113
Phải. Em là một cô công chúa rất dễ thương.

25
00:02:49,112-->00:02:54,113
Cảm ơn chị, Kurumin.

25
00:02:57,512-->00:03:00,113
Cái... cái gì vậy? 
Kurumi vừa mới hành động kỳ lạ...

25
00:03:00,512-->00:03:05,513
Cô bé thật sự rất đáng yêu, phải không?
Tên em là gì-nya?

25
00:03:06,112-->00:03:11,513
Nya? Tôi nổi da gà lên rồi đấy...

25
00:03:12,112-->00:03:15,113
Em... em tên gì?

25
00:03:16,112-->00:03:18,113
Tên em là Rio, Kurumin.

25
00:03:19,112-->00:03:26,513
Vậy Rio-san, chị hỏi em nhé.
Sao em gọi chị là Kurumin?

25
00:03:27,112-->00:03:33,113
Kurumin là Kurumin mà.
Chị là Kurumin-pah phải không?

25
00:03:34,112-->00:03:38,113
Chị không hiểu lắm, 
nhưng chị cảm thấy đau nhói đấy.

25
00:03:39,112-->00:03:48,513
Tôi không hiểu lắm, 
nhưng thấy Kurumi bị tấn công bằng một
câu trả lời là hơi hiếm có đấy. 
Tôi vừa xem chuyện thú vị đấy.

25
00:03:49,112-->00:03:50,313
Haha... đúng vậy đấy.

25
00:03:50,512-->00:03:55,113
Có thể Rio nghe Marina kể những chuyện
họ đã trải qua trong không gian ảo.
Vậy nên Kurumi không nhớ gì cả.

25
00:03:55,312-->00:03:59,113
Nhân tiện, em biết tên chị không?

25
00:03:59,512-->00:04:01,513
Kaguya-chan?

25
00:04:01,912-->00:04:07,113
Không phải. Tên thật của chị cơ!

25
00:04:08,112-->00:04:13,113
Ka-chan?

25
00:04:14,112-->00:04:16,513
Chị không già đến thế! 
(Ka-chan nghe giống gọi mẹ)

25
00:04:20,112-->00:04:21,113
Hahaha...

25
00:04:21,312-->00:04:24,513
Không cần phải nói, 
Rio khiến tôi và Kurumi không thể nhịn cười.

26
00:04:29,912-->00:04:34,113
Sau đó, Kurumi bế cô bé trong vòng tay,
Kaguya chơi với cô ấy... 
Cuối cùng trời cũng tối rồi.

26
00:04:34,512-->00:04:37,113
Giờ Rio phải đi rồi.

39
00:04:37,313-->00:04:39,113
Em đi gặp Marina à?

40
00:04:39,313-->00:04:42,113
Phải. Con đã hứa rồi.

41
00:04:42,313-->00:04:43,513
Anh hiểu rồi. Hôm nay em vui chứ?

42
00:04:44,113-->00:04:48,113
Con rất vui. Hôm nay con rất vui.

43
00:04:48,313-->00:04:49,513
Vậy thì tốt.

39
00:04:50,113-->00:04:55,113
Vậy hẹn gặp lại nhé papa, Kurumin, Kaguya-chan.

40
00:04:56,113-->00:04:59,113
Ừ. Chị rất vui đấy.

40
00:04:59,513-->00:05:05,513
Hãy cẩn thận khi em nói tên chị 
cho những người khác đấy, Rio, gia đình của chị.

40
00:05:06,113-->00:05:12,113
Vâng, Kaguya-chan. 
Em sẽ không gọi là Ka-chan nữa.

40
00:05:13,113-->00:05:14,513
Đã nói rồi!

40
00:05:14,913-->00:05:16,513
Haha... Kaguya, chào đàng hoàng đi.

40
00:05:17,113-->00:05:20,113
Ừ, hẹn gặp lại, Rio.

40
00:05:20,513-->00:05:22,113
Hẹn gặp lại, Rio.

41
00:05:22,513-->00:05:25,513
Vâng! Tạm biệt!

42
00:05:26,113-->00:05:30,513
Quên mất. Có vài chuyện 
tôi muốn hỏi em ấy.
Mà thôi kệ, vẫn còn lần tới mà.

43
00:05:32,113-->00:05:34,913
Rio quên nói với Papa việc này.

44
00:05:35,113-->00:05:36,313
Gì vậy?

45
00:05:36,913-->00:05:39,513
Nếu Papa muốn gặp Rio, 
hãy đến công viên nhé, được chứ?

46
00:05:39,913-->00:05:41,113
Rio thì thầm nhỏ nhẹ bên tai tôi.

47
00:05:41,312-->00:05:42,513
Hiểu rồi.

48
00:05:43,112-->00:05:44,313
Bye bye!

49
00:05:45,512-->00:05:50,113
Ara, cô bé đã nói gì với cậu vậy?

50
00:05:50,312-->00:05:52,313
Rio nói Rio muốn đi chơi 
với chúng ta một lần nữa. 

51
00:05:52,512-->00:05:55,513
Không đời nào tôi nói việc Rio đã nói. 
Tôi không có sự lựa chọn nào cả

26
00:05:56,112-->00:06:02,113
Ra là vậy. Em ấy vẫn muốn gặp lại
dù đã có ảnh hưởng xấu quanh đây à?

26
00:06:02,512-->00:06:05,113
Cô đang nói tôi à?

26
00:06:06,112-->00:06:12,513
Ara... Cô nhận ra rồi à?
Tôi nghĩ cô không ý thức được chuyện đó đâu.

26
00:06:13,112-->00:06:17,513
Một người như tôi 
không gì ảnh hưởng đến tôi cả!

26
00:06:18,112-->00:06:24,113
Là sao? Cô đang nói ai vậy?

26
00:06:24,512-->00:06:26,113
Cô và tôi.

26
00:06:28,112-->00:06:39,113
Vậy à? Hôm nay tôi có đủ niềm vui rồi.
Giờ tôi về nghỉ đây.

26
00:06:40,112-->00:06:43,113
Thế là sao?

26
00:06:43,312-->00:06:45,113
Phải. Đó là một cú sốc
với Kurumi chăng?

26
00:06:45,312-->00:06:47,513
Tôi biết điều đó và nở một nụ cười 
cay đắng trên khuôn mặt mình.
