﻿1
00:00:05,512-->00:00:07,513
Đi đến Lớp học?
1) Đồng ý
2) Không đồng ý

2
00:00:12,112-->00:00:14,113
Phù. Tôi không biết rằng 
hôm nay phải trực nhật.
Không còn ai trong lớp cả.

3
00:00:14,512-->00:00:16,113
Giờ phải chuẩn bị bữa ăn tối.
Mình sẽ đi qua siêu thị.

4
00:00:17,112-->00:00:19,113
À, nhưng Rinne nói là sẽ đi cùng tôi.
Tôi nên gọi điện cho cô ấy.

5
00:00:20,112-->00:00:22,113
Không nghe máy à?

6
00:00:22,512-->00:00:25,113
Sao vậy nhỉ? Cô ấy đang bận gì à?

7
00:00:27,512-->00:00:31,113
Cậu trực nhật xong rồi à, Shidou?

8
00:00:32,112-->00:00:34,113
Rinne? Cậu đợi lâu không?
Xin lỗi để cậu đợi.

9
00:00:35,112-->00:00:42,913
Không. Tớ muốn đợi để đi mua đồ chung mà. 

9
00:00:43,112-->00:00:49,113
Hơn nữa nãy giờ tớ ngồi nói chuyện
nên cũng không thấy chán đâu.

10
00:00:50,112-->00:00:52,113
Nói chuyện à? Còn ai nữa sao?

11
00:00:56,112-->00:00:57,513
Gì vậy?

11
00:01:01,112-->00:01:04,513
Shidou, cậu giật mình ghê quá nhỉ.

12
00:01:05,512-->00:01:08,113
Hở? Maria? Nãy giờ cậu ngồi với Rinne à?

13
00:01:09,112-->00:01:15,113
Phải. Bọn tớ đã chia sẻ thông tin rất nhiều.

14
00:01:16,112-->00:01:18,113
Chia sẻ thông tin à?

15
00:01:19,112-->00:01:24,113
Tớ hỏi về những chuyện khi tớ không ở đây.

16
00:01:24,512-->00:01:29,513
Tớ cũng hỏi rất nhiều về chuyện hồi trước.

17
00:01:30,112-->00:01:32,113
Ra vậy. Hai cậu đã thân nhau rồi phải không?

18
00:01:32,512-->00:01:35,113
Rinne không biết những gì xảy ra sau này.
Maria không biết về Rinne. 
Họ cần thời gian để làm quen với nhau.

19
00:01:35,512-->00:01:38,113
Tôi không bao giờ tưởng tượng rằng 
họ sẽ cười với nhau theo cách này.

20
00:01:39,112-->00:01:44,113
Tớ cảm thấy điều gì đó 
trong đôi mắt cậu, Shidou.

21
00:01:47,112-->00:01:55,113
Cậu nói đúng, tớ cảm thấy điều gì đó. 
Một ánh mắt xa xăm... 

22
00:01:56,112-->00:01:58,113
Không, không phải vậy. 
Tớ xong việc rồi. Đi thôi.

23
00:01:59,112-->00:02:07,513
Tớ không nghĩ thế, nhưng thôi kệ.
Chúng ta còn bữa tối phải chuẩn bị đấy.

24
00:02:08,513-->00:02:13,113
Nếu vậy thì đi nhanh lên, Shidou.

25
00:02:14,113-->00:02:16,113
Ừ. Chúng ta đi thôi.

25
00:02:21,112-->00:02:27,113
Mặc dù chỉ vừa mới đây thôi,
tớ cảm thấy thật hoài niệm.

25
00:02:27,513-->00:02:30,113
Maria hạnh phúc nhìn xung quanh
khi cô ấy đang đi bộ trên con đường vắng.

25
00:02:30,512-->00:02:32,113
Trông cậu đang tận hưởng mọi thứ nhỉ, Maria.

25
00:02:33,112-->00:02:41,113
Phải. Tớ không bao giờ nghĩ rằng
mình có thể đi bộ bằng đôi chân mình.

25
00:02:42,112-->00:02:45,113
Nhưng cũng nhiều thứ nguy hiểm hơn đấy.
Ở đây mọi thứ đều là thật.

25
00:02:45,512-->00:02:52,113
Tớ biết mà. Không cần phải dặn đâu.
Cậu hay lo quá mức đấy, Shidou.

25
00:02:52,112-->00:02:58,513
Hai cậu giống như hai anh em nhỉ.

25
00:02:59,112-->00:03:05,113
Rinne, đừng nói thế.

25
00:03:09,112-->00:03:17,113
Tớ không phải và mãi mãi không phải
là em gái Shidou. Đúng không Shidou?

25
00:03:18,112-->00:03:20,113
Ừ... Đúng vậy.

25
00:03:20,512-->00:03:23,513
Giờ tôi mới nhớ, trước đây cũng một chuyện 
tương tự thế này xảy ra khi cô ấy gặp Mana.
Cô ấy tức giận vì tôi không coi cô ấy
như bạn gái mình.

25
00:03:24,112-->00:03:30,113
Em gái có thể rất quan trọng,
nhưng không bao giờ trở thành cô dâu.

25
00:03:31,112-->00:03:38,113
Ra vậy. Cậu muốn là cô dâu 
của Shidou phải không, Maria-chan?

25
00:03:40,512-->00:03:46,113
Phải. Tất cả những gì bọn tớ 
đã có với nhau là một nụ hôn.

25
00:03:47,112-->00:03:49,113
Này Maria!

25
00:03:49,512-->00:03:52,113
Cô ấy nói thật.
Nhưng nói thế có thể gây hiểu lầm.

25
00:03:53,112-->00:03:58,513
Vậy tớ sẽ giúp cậu, Maria-chan.

25
00:03:59,512-->00:04:04,513
Rinne, thật sao?

25
00:04:06,112-->00:04:12,113
Phải. Nếu là trước đây, tớ sẽ nói thế.

26
00:04:16,512-->00:04:22,513
Nhưng xin lỗi. 
Bây giờ tớ không thể nói thế được nữa.

26
00:04:23,112-->00:04:35,113
Tôi muốn là người gần gũi nhất với Shidou.
Tôi muốn là người quan trọng nhất của cậu ấy. 
Dù chuyện đó là không có thực.

26
00:04:35,512-->00:04:38,513
Lời nói của Rinne đâm vào ngực tôi.
Bởi vì tôi đã biết những cảm xúc đó.
Nó giống như tận sâu trong tim tôi.

26
00:04:39,112-->00:04:55,513
Vậy nên dù là cậu, Maria-chan...
Không, dù là Tohka-chan, Tobiichi-san...
hay bất cứ ai, tớ sẽ không nhường nhịn. 
Tớ không muốn bỏ cuộc. 

26
00:04:56,512-->00:05:00,513
Đó là những gì tớ nghĩ đấy.

26
00:05:02,112-->00:05:10,113
Tớ đã từng nghĩ hạnh phúc của Shidou
là đủ với tớ... Tớ thực sự nghĩ thế đấy.

26
00:05:10,512-->00:05:17,513
Thật là lạ... Giờ tớ không còn cảm thấy vậy nữa.

26
00:05:18,512-->00:05:22,113
Không có gì lạ đâu.

26
00:05:26,112-->00:05:38,513
Chuyện đó không có gì lạ đâu.
Đó là điều tự nhiên khi cậu muốn đến
với người mà cậu muốn. Đó mới gọi là tình yêu.

26
00:05:39,512-->00:05:42,513
Maria-chan...

26
00:05:43,512-->00:05:53,513
Đúng vậy. Tớ muốn ở bên cạnh Shidou.
Chắc chắn là vậy.

26
00:05:54,512-->00:06:06,113
Tớ tự hỏi tại sao trước đây
tớ không cảm thấy như thế 
trong thời gian dài như vậy?

26
00:06:06,512-->00:06:08,113
Rinne...

26
00:06:09,112-->00:06:17,113
Vậy là tớ và cậu là đối thủ của nhau rồi.

27
00:06:17,512-->00:06:23,513
Đứng trước cậu...
Không, trước Tohka hay bất kỳ ai.
Tớ sẽ không thua đâu.

28
00:06:24,512-->00:06:29,113
Ừ... Tớ cũng không thua đâu.

29
00:06:39,112-->00:06:42,113
Cả hai bắt đầu cười khi họ nhìn nhau. 
Bầu không khí không thể tả nổi.

30
00:06:42,512-->00:06:45,113
Chúng ta đến siêu thị rồi.

38
00:06:54,512-->00:06:56,513
Hai... hai cậu sao vậy?

39
00:06:57,113-->00:07:01,113
Vậy thì làm nhé, Rinne.

40
00:07:02,112-->00:07:06,113
Được rồi. Làm thôi.

41
00:07:08,112-->00:07:10,113
Này! Hai cậu làm gì vậy?

42
00:07:11,113-->00:07:13,113
Họ đang định làm gì tôi chăng?

43
00:07:17,112-->00:07:22,113
Nước sốt ở nhà cậu vẫn còn phải không, Shidou?

39
00:07:23,112-->00:07:26,113
À, đúng rồi. Tớ nghĩ vẫn còn đủ.

40
00:07:26,512-->00:07:33,513
Nước tương bán ở khu đó phải không?
Hình như đang giảm giá đấy.

41
00:07:35,112-->00:07:44,513
Maria-chan, cậu mang luôn đồ ăn vặt được không?
Nó hơi đắt, nhưng Shidou nên mua mà.

42
00:07:45,512-->00:07:48,113
Thật vậy sao?

43
00:07:49,112-->00:07:51,513
Đúng vậy. Đôi khi bọn tớ cũng mua
chút bánh ăn vặt cho mọi người.

39
00:07:52,112-->00:08:00,113
Tôi không biết... Xin lỗi nhé, Shidou.
Tớ đã không để ý đến điều đó.

40
00:08:00,513-->00:08:04,113
Tớ không thường mua nó đâu.
Thay đổi nước sốt đậu nành thường dùng
sẽ tạo hương vị khác. Vậy nên lâu lâu
tớ lại đổi thương hiệu.

41
00:08:04,513-->00:08:11,113
Ra vậy. Tớ biết thêm điều mới.

42
00:08:12,113-->00:08:14,113
Không có gì xấu cả.

43
00:08:14,513-->00:08:21,113
Đúng thế, Maria-chan. 
Nếu cậu nấu ăn ở nhà Shidou,
cậu sẽ tự biết được những điều đó.

44
00:08:22,113-->00:08:27,513
Rinne chắc biết nhiều lắm nhỉ.

45
00:08:28,113-->00:08:36,513
Không phải thế đâu.
Chúng ta đã có nước tương.
Những thứ chúng ta còn thiếu là...

46
00:08:37,513-->00:08:42,113
Một chút trái cây tráng miệng nữa thôi.

47
00:08:43,112-->00:08:45,113
Đúng vậy. Toàn ăn đồ nấu chín
sẽ cảm thấy mệt mỏi lắm.

48
00:08:45,312-->00:08:48,913
Khi vừa đến siêu thị, 
Maria và Rinne có vẻ hơi buồn. 
Nhưng sau khi nói chuyện nấu nướng,
dần dần họ lấy lại được sự hài hước của mình.

49
00:08:49,112-->00:08:52,113
Có cảm giác có sự đối đầu giữa họ.
nhưng nếu cứ tiếp tục thế này,
hy vọng tôi sẽ sớm được giải thoát.

50
00:08:52,312-->00:09:07,513
Một loại trái cây không thể bỏ lỡ
là cà trái cây. Nó được dùng rất nhiều 
trong các món tráng miệng.

51
00:09:08,112-->00:09:10,513
Ở bên đó đấy. Cậu sang lấy đi.

52
00:09:11,112-->00:09:20,513
Chờ đã. Sao ở kia lại bán đào?
Mùa này không phải mùa đào, 
nhưng trông ngon lắm.

53
00:09:21,112-->00:09:24,113
Đào à? Ở nhà không ăn thường xuyên lắm.
Cậu muốn mua không?

54
00:09:24,512-->00:09:37,513
Nhưng như đã nói Rinne, mùa đào là mùa hè.
Tớ nghĩ mùa này sẽ không có hương vị 
đặc trưng của nó. Chúng ta nên mua cà chua.

55
00:09:38,512-->00:09:41,513
Cũng đúng. Bán trái mùa, lại rẻ nữa
nên chắc không ngon...

56
00:09:42,112-->00:09:51,113
Tớ hiểu ý cậu, Maria-chan,
nhưng tớ nghĩ Tohka-chan và mọi người
sẽ thích đào hơn.

41
00:09:51,512-->00:10:00,113
Trong trường hợp đó, Rinne,  
cà trái cây cũng rất ngon ngọt mà.
(Cà trái cây là một loại cà chua nhỏ,
rất ngọt, sản xuất tại Nhật Bản)

42
00:10:03,112-->00:10:11,513
Nhưng cà trái cây hơi đắt phải không?
Không cần phải mua thứ đắt như vậy.

43
00:10:13,512-->00:10:26,113
Shidou thích thử những thứ mới
nên không sao đâu. Tớ chắc cậu 
chưa bao giờ ăn cà trái cây cả.
Phải không, Shidou?

44
00:10:27,112-->00:10:30,113
Đúng vậy. Nhưng không nhất thiết phải...

45
00:10:32,512-->00:10:35,113
Tôi cảm thấy một cái nhìn sắc lạnh...

46
00:10:35,512-->00:10:37,513
Phải. Tớ rất muốn thử chúng...

47
00:10:38,112-->00:10:43,513
Tớ biết cậu sẽ thích mà, Shidou.

48
00:10:45,512-->00:10:54,513
Nhưng Shidou cũng rất tiết kiệm.
Nhìn kìa Shidou, họ giảm giá 
gần một nửa kìa. Cơ hội lớn đấy.

49
00:10:55,112-->00:10:57,513
Trông rẻ thật, nhưng có thể không ngon...

50
00:11:00,512-->00:11:03,113
Tôi cảm thấy một áp lực rất lớn...

53
00:11:03,512-->00:11:05,513
Phải, chúng ta phải nắm lấy cơ hội này.

54
00:11:06,112-->00:11:12,113
Đúng vậy. Cậu thật thông minh khi mua nó.

58
00:11:21,512-->00:11:24,513
Tôi bắt đầu cảm thấy ớn lạnh...

59
00:11:25,512-->00:11:30,113
Này hai cậu. Sao phải chọn chứ? 
Sao không mua cả hai luôn đi?

60
00:11:37,512-->00:11:40,513
Chúng tôi đã mua xong và đi về nhà.
Maria và Rinne mỗi người cầm 2 túi.
Rất nhiều thứ trong đó.

61
00:11:41,112-->00:11:43,113
Chúng ta mua nhiều hơn tớ nghĩ.
Nhiều thế này có thể ăn hết nổi không?

62
00:11:43,512-->00:11:51,513
Họ ăn khỏe mà. Không sao đâu.
Nếu cậu sợ ăn không hết thì
sao không mời thêm Tonomachi-kun đi?

63
00:11:52,512-->00:11:55,513
Không, không cần đâu.
Mời hắn chỉ mệt thêm thôi.

64
00:11:56,112-->00:12:01,113
Tonomachi-kun nghe thấy chắc buồn lắm đấy.

65
00:12:01,512-->00:12:04,513
Hắn sẽ khóc đấy. Phải nghĩ xem nên 
nấu đống này như thế nào để mai ăn được.

66
00:12:05,112-->00:12:17,513
Tớ sẽ có công thức nấu ăn tốt nhất.
Chúng ta có thể ăn tiếp đồ ăn còn thừa.

70
00:12:18,112-->00:12:23,513
Vậy tớ sẽ giúp cậu được chứ?

71
00:12:24,512-->00:12:29,513
Cậu nói đúng. Nếu làm cùng nhau sẽ tốt hơn.

72
00:12:30,112-->00:12:32,513
Có vẻ tớ không cần giúp rồi.

73
00:12:33,112-->00:12:35,513
Tớ nghĩ là không.

74
00:12:36,512-->00:12:39,113
Không cần đâu.

75
00:12:39,512-->00:12:42,113
Không cần phải nói nhiều lần thế đâu...

76
00:12:42,512-->00:12:45,513
Có vẻ tôi chỉ là nguyên nhân gây phiền toái. 
Dù sao mối quan hệ của họ 
cũng tốt hơn Tohka và Origami.

77
00:12:46,112-->00:12:53,513
Nếu có tớ và Maria-chan rồi thì cậu
không cần phải nấu ăn nữa đấu, Shidou.

78
00:12:54,512-->00:13:01,113
Đúng vậy. Bọn tớ có thể nấu
nhiều món hơn cậu mà, Shidou.

79
00:13:02,112-->00:13:04,113
Đúng vậy. Nhưng tớ nghĩ tớ cũng nên giúp...

80
00:13:05,112-->00:13:15,113
Đừng tỏ ra giống thú cưng bị bỏ rơi như thế.
Bọn tớ không có ý xấu hay gì đâu.

81
00:13:16,112-->00:13:28,513
Khi cậu thanh toán tiền,
tớ đã nói chuyện với Maria-chan.
Và bọn tớ đồng ý giúp đỡ cậu.

84
00:13:29,512-->00:13:31,113
Giúp đỡ tớ sao?

85
00:13:32,113-->00:13:42,113
Trong thế giới này,
cậu sẽ phải suy nghĩ nhiều điều đấy, Shidou.

86
00:13:43,112-->00:13:48,513
Trong điều kiện hiện tại của bọn tớ,
bọn tớ không thể giúp cậu 
ra khỏi thế giới này được.

86
00:13:49,112-->00:13:55,513
Đó là lý do tại sao bọn tớ 
muốn làm tất cả mọi thứ có thể.

87
00:13:56,112-->00:13:58,113
Các cậu...

88
00:13:59,112-->00:14:03,513
Ngoài chúng ta, không ai biết
sự bất thường đang xảy ra cả.

88
00:14:04,112-->00:14:08,113
Vì vậy, cậu chỉ có thể tin tưởng 
bọn tớ thôi, đúng không?

89
00:14:09,112-->00:14:16,513
Đúng vậy. Nhìn cậu lo lắng 
thật khó cho bọn tớ quá, Shidou.

90
00:14:17,112-->00:14:19,513
Ừ, cảm ơn các cậu. 
Giờ tớ chỉ có các cậu thôi.

91
00:14:20,112-->00:14:22,513
Tôi trả lời và đi vội hơn một chút.

92
00:14:23,112-->00:14:28,113
Shidou, cậu nghĩ gì vậy?

93
00:14:29,112-->00:14:32,513
Cậu đang xấu hổ à?

95
00:14:33,112-->00:14:35,513
Rinne trả lời không sai từ phía sau.
Thực sự tôi cũng cảm thấy xấu hổ.
Tuy nhiên, những gì trong đầu tôi 
là một điều gì đó khác.

96
00:14:36,112-->00:14:38,513
Tôi, một lần, đặt dấu chấm hết cho Eden...
và không gian ảo của Maria.

97
00:14:39,112-->00:14:41,513
Đây chỉ là một giấc mơ.
Nhưng thực sự nó là một giấc mơ 
mà tôi có nên thức dậy?

98
00:14:42,112-->00:14:44,113
Trước đây, Rinne đã nói tôi chỉ cần
được hạnh phúc trong thế giới này. 
Tôi đã trả lời ngay lập tức... là không muốn.

99
00:14:44,512-->00:14:47,113
Nhưng một phần trong tôi tự hỏi 
lần này tôi có muốn chọn lại hay không.

100
00:14:47,512-->00:14:50,113
Câu hỏi đó tôi sẽ giữ lại trong đầu.
Vẫn chưa phải lúc để trả lời.

101
00:15:24,812-->00:15:30,513


102
00:15:24,812-->00:15:30,513


