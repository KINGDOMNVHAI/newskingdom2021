﻿0
00:00:01,512-->00:00:02,913
Trời sáng rồi...

368
00:00:03,112-->00:00:06,113
Tôi cảm thấy mình ngủ chưa đủ.
Có lẽ do tôi nghĩ quá nhiều trước khi ngủ.

368
00:00:06,312-->00:00:07,513
Mình nên làm gì bây giờ?

368
00:00:07,912-->00:00:12,113
Tôi nhớ lại tối hôm qua. 
Không chỉ Rinne, Maria, cả Marina cũng ở đây.
Thực tế là họ đã biến mất...

1
00:00:12,312-->00:00:15,113
Và cô bé đó... Rio.
Cô ấy nói tên là Sonogami Rio...

2
00:00:15,312-->00:00:19,513
Cô bé gọi tôi là papa. 
Tôi cảm thấy cô bé rất giống 
Rinne trong dạng Ruler đã tạo ra Eden.

3
00:00:19,812-->00:00:22,513
Mình chắc chắn cô bé biết về tình hình này.
Mình rất muốn gặp cô bé đó lần nữa.

4
00:00:22,912-->00:00:25,113
Nhưng tôi không biết làm sao để
liên lạc với cô bé cả.

5
00:00:25,312-->00:00:29,113
Marina có vẻ cũng biết gì đó.
Nhưng tìm họ rất khó 
khi không có Fraxinus trợ giúp.

6
00:00:29,413-->00:00:31,113
Mình có thể làm gì đây?

7
00:00:34,512-->00:00:37,113
Dậy đi, Shidou! Đến giờ ăn sáng rồi đấy!

8
00:00:37,312-->00:00:40,313
Tohka? Anh dậy rồi. 
Em đói lắm rồi đấy à?

9
00:00:40,512-->00:00:46,113
Em lên đây bế anh xuống đấy!
Em chán ngồi đợi rồi!

10
00:00:46,312-->00:00:48,113
Được rồi. Anh xuống ngay...

11
00:00:51,112-->00:00:53,113
Sao mọi người đứng tụ lại thế này?

12
00:00:53,512-->00:00:59,513
À, Shidou. Chào buổi sáng. Nhìn đi.

13
00:00:59,913-->00:01:02,113
Nhìn gì... Cái gì đây?

14
00:01:02,312-->00:01:08,113
Nhìn hướng Kotori chỉ 
là một đống món ăn từ Tây sang Á.
Quá nhiều cho một bữa sáng đơn giản.

15
00:01:08,312-->00:01:10,113
Đống này có thể cho 10 người ăn đấy.

16
00:01:11,112-->00:01:20,513
Tớ xin lỗi Shidou... 
Tớ nghĩ mọi người ăn nhiều nên tớ làm nhiều quá.
Tớ đã dùng hết đồ ăn cho hôm qua thôi.

17
00:01:21,112-->00:01:28,113
Tớ cũng làm phụ cậu ấy.
Tại tớ vui quá nên tớ làm nhiều.

18
00:01:28,512-->00:01:34,513
Cậu không cần xin lỗi đâu, Maria-chan.
Cậu chỉ giúp thôi nên là lỗi của tớ.

19
00:01:35,112-->00:01:39,513
Được rồi hai cậu. Tớ hiểu tình hình rồi.
Thật sự hai cậu làm hết đống này
chỉ với nguyên liệu hôm qua sao?

20
00:01:40,112-->00:01:47,313
Phải. Vì lỡ làm rồi,
tớ nghĩ thật phí khi bỏ chúng.

21
00:01:47,512-->00:01:49,513
Thế này nhiều hơn tớ nghĩ...
Tớ không ngờ đấy.

22
00:01:49,812-->00:01:54,113
Nếu đống này là từ hôm qua, 
vậy tối qua họ ăn bao nhiêu? 

23
00:01:54,512-->00:02:00,113
Em cũng không ngờ đấy!
Em không nghĩ chúng ta có nhiều đồ ăn
vào bữa sáng đến thế!

25
00:02:00,512-->00:02:02,113
Nhưng chúng ta ăn không hết đâu.

25
00:02:03,112-->00:02:11,513
Ổn thôi. Em đã nghĩ đến chuyện này
nên em đã gọi hỗ trợ.
Họ sẽ đến sớm thôi.

25
00:02:12,112-->00:02:13,313
Em đấy à, Kotori?

25
00:02:13,512-->00:02:18,113
Gì vậy? Anh quên gương mặt 
cô em gái dễ thương của anh à?

25
00:02:18,512-->00:02:22,513
Ý anh không phải thế. 
Hôm qua em không ở chế độ này 
vì Rinne ở đây à?

25
00:02:23,112-->00:02:30,113
Thì ra là vậy. Xin lỗi vì hôm nay
em đã chọn sai chế độ.

25
00:02:30,312-->00:02:32,513
Đừng như thế. Em sẽ luôn là
em gái đáng yêu của anh.

25
00:02:33,112-->00:02:36,113
Anh... anh lại nói thế à?

25
00:02:37,112-->00:02:39,113
Họ đến rồi.

25
00:02:41,112-->00:02:45,113
Xin lỗi vì đã làm phiền, Shidou-san.

25
00:02:46,112-->00:02:49,513
Shidou-kun, bọn em tới giúp đây.

25
00:02:50,512-->00:02:58,113
Tôi đến để tham gia bữa tiệc này đây, Shidou.
Nào, hãy giới thiệu những gì cậu gọi cho tôi đi.

25
00:02:59,112-->00:03:04,513
Hỏi thăm. Chào buổi sáng, Shidou.
Bọn tớ đến giúp cậu đây.

25
00:03:05,512-->00:03:14,113
Chào buổi sáng, mọi người.
Bữa sáng với darling và các cô gái xinh đẹp.
Điều này thật tuyệt vời!

37
00:03:14,512-->00:03:16,513
Chào buổi sáng, mọi người.
Sáng nay sẽ náo nhiệt đấy.

38
00:03:17,112-->00:03:23,113
Đúng thật. Cảm ơn mọi người đã đến.

39
00:03:24,112-->00:03:30,113
Tớ mừng vì mọi người ở đây.
Bữa sáng hôm nay nhiều lắm đấy.
Vậy nên cứ ăn nhiều vào nhé.

40
00:03:31,112-->00:03:33,113
Được rồi. Không còn thời gian nữa đâu.
Chúng ta ăn đi!

41
00:03:33,513-->00:03:36,113
Itadakimasu!

42
00:03:37,113-->00:03:41,113
Cậu vào nhanh quá nhỉ, Tohka-chan?

43
00:03:47,113-->00:03:52,513
Thật tuyệt khi được ăn nhiều thế này.
No thật đấy!

39
00:03:53,113-->00:04:00,513
Tohka, cô ăn nhanh thật đấy.
Cô nên nhai kỹ hơn một chút.

42
00:04:01,113-->00:04:04,113
Tôi đang nhai mà.

43
00:04:05,113-->00:04:08,113
Vậy à? Được rồi...

39
00:04:09,113-->00:04:13,113
Có nhiều món ăn quá.

43
00:04:14,112-->00:04:21,113
Bánh mì, cơm, cháo, thịt...
Nhiều thứ quá nên tớ chẳng biết ăn gì.

44
00:04:22,112-->00:04:28,513
Ngon đấy. Một bữa tiệc phù hợp
cho những người như tôi.

45
00:04:29,512-->00:04:36,513
Chỉ dẫn. Bữa tiệc là dành cho 
bữa trưa hoặc tối, Kaguya.

50
00:04:37,112-->00:04:45,513
Đó là vì tôi đã vượt qua giới hạn.
Các khái niệm thời gian 
không có tác dụng với tôi!

51
00:04:46,512-->00:04:53,113
Tuyệt thật đấy, Kaguya-san. 
Vậy là cô có thể ăn tiếp phải không?

52
00:04:54,112-->00:04:58,113
Cái gì? Sao cô kết luận như thế, Miku?

53
00:04:59,112-->00:05:09,513
Cô vừa nói mà Kaguya-san. 
Cô vượt qua khái niệm về thời gian,
nghĩa là cô không cần thời gian 
để tiêu hóa thức ăn, phải không?

43
00:05:10,512-->00:05:16,513
Không... Tôi chỉ ăn được đến đây thôi.

44
00:05:17,512-->00:05:28,113
Xác minh. Miku, sao chúng ta 
không kiểm tra giới hạn của cô ta nhỉ? 
Có rất nhiều đồ ăn để thử nghiệm mà.

45
00:05:28,512-->00:05:35,113
Chắc chắn rồi. Nhìn mặt cô 
cũng đáng yêu lắm đấy, Kaguya-san.

46
00:05:35,512-->00:05:38,513
Khôngggg!

47
00:05:39,513-->00:05:50,113
Đây là của cậu làm à?
Công thức giống Shidou 
nhưng mùi vị nhẹ hơn. Tớ rất thích.

48
00:05:50,512-->00:05:59,113
Cảm ơn nhé, Rinne. 
Tớ không muốn bắt chước Shidou.
Vậy nên tớ muốn làm theo cách của mình.

51
00:06:00,112-->00:06:11,113
Cậu đã rất chăm chỉ, Maria-chan.
Tớ không nghĩ lại nấu nhiều đến vậy.
Thật vui vì mọi người khen đồ ăn ngon.

52
00:06:12,112-->00:06:14,513
Thế là chơi xấu đấy, Rinne.

53
00:06:15,112-->00:06:20,113
Tớ... chơi xấu sao?

54
00:06:21,112-->00:06:28,113
Tớ sẽ không nói tại sao.
Sao cậu không chú ý đến bản thân mình hơn đi?

55
00:06:29,112-->00:06:33,113
Được rồi...

56
00:06:34,112-->00:06:39,113
Sao vậy Shidou? Anh không ăn nữa à?

57
00:06:39,512-->00:06:41,513
Anh đang nghĩ về mọi người. 
Được rồi, anh ăn đây!

58
00:06:46,912-->00:06:48,113
Đau bụng quá...

59
00:06:48,312-->00:06:52,313
Khi tôi ngồi kế Tohka, 
cô ấy gắp đồ ăn cho tôi 
mỗi khi tôi ăn xong. Giờ tôi no quá.

60
00:06:53,112-->00:06:55,913
Cậu đâu cần phải ăn hết chứ.

61
00:06:56,112-->00:06:57,313
Không, tớ không làm thế được...

62
00:06:57,512-->00:07:00,913
Tôi không nói cô ấy dừng lại.
Nhưng ít nhất tôi phải ăn hết
đống đồ ăn trong dĩa của mình.

65
00:07:01,312-->00:07:02,513
Dù sao tớ cũng mừng vì đã xử lý hết.

66
00:07:03,112-->00:07:07,913
Tớ cũng mừng nữa.

67
00:07:08,112-->00:07:10,113
Mặc dù thực tế là 
Tohka giải quyết hết đống đó.

68
00:07:18,112-->00:07:22,113
Giờ nghỉ trưa, Tohka và mọi người
tụ tập xung quanh bàn của Maria.
Họ nói về bữa sáng nay 
chúng tôi ăn nhiều thế nào.

69
00:07:22,812-->00:07:30,113
Thế à? Các cậu nên gọi tớ sang ăn cùng chứ.

70
00:07:30,512-->00:07:37,113
Xin lỗi vì không gọi cho cậu.
Tớ nghĩ thật ngại nếu gọi sáng sớm này...

71
00:07:37,312-->00:07:41,513
Không cần xin lỗi đâu.

72
00:07:42,112-->00:07:49,513
Tiếc thật. Nếu cô gọi tôi, 
tôi sẽ có mặt ngay lập tức.
Lần sau ít nhất cô hãy để lại tin nhắn.

75
00:07:50,112-->00:07:59,113
Được rồi. Nhưng tôi vẫn không biết
phải liên lạc với cô như thế nào.
Cô cho tôi số điện thoại nhé.

76
00:07:59,513-->00:08:02,113
Được rồi. Tôi sẽ gửi tin nhắn cho cô.

77
00:08:02,513-->00:08:09,113
Cậu gửi à? Tobiichi-san, 
sao cậu biết số điện thoại của Maria-chan?

81
00:08:09,312-->00:08:10,513
Đó là bí mật.

82
00:08:11,113-->00:08:16,313
Vậy à... Vậy tớ không hỏi nữa.

83
00:08:17,112-->00:08:21,513
Origami tuyệt thật. Cô ấy lấy đâu ra
những thông tin đó? Tôi muốn hỏi cô ấy.

84
00:08:22,112-->00:08:31,113
Bữa sáng nay Rinne và Maria 
làm cả núi đồ ăn. Tôi ăn không ngừng đấy.

85
00:08:31,512-->00:08:38,113
Tớ nghĩ cậu không thích.
Nhưng may mà cậu và mọi người rất thích.

86
00:08:38,512-->00:08:44,113
Tuy nhiên, Yatogami Tohka nên biết
tự kiềm chế hơn. 

87
00:08:44,512-->00:08:52,513
Con trai không thích con gái ăn nhiều. 
Shidou đang xem thường cô sau lưng đấy.

87
00:08:53,112-->00:08:58,513
Cô nói gì, Tobiichi Origami?
Shidou không bao giờ nói với tôi như thế!

88
00:08:59,112-->00:09:09,513
Đúng đấy, Tobiichi-san. 
Shidou thích những người vui vẻ khi 
thưởng thức món ăn. Giống tớ nhỉ?

89
00:09:10,113-->00:09:16,513
Thế à? Đúng đấy! 
Thấy sao hả, Tobiichi Origami?

90
00:09:17,112-->00:09:28,113
Không ổn rồi. Sonogami Rinne, 
cô thật sự không thể xem thường.
Lợi thế của bạn thuở nhỏ thật đáng sợ.

91
00:09:28,512-->00:09:37,513
Ara ara. Origami-san, cô bắt đầu để ý
cách cô nói chuyện với người khác à?

92
00:09:38,112-->00:09:41,113
Lời khuyên của cô không thuyết phục lắm.

93
00:09:41,512-->00:09:44,513
Nghe nặng nề thật đấy.

94
00:09:45,112-->00:09:56,513
Hai cậu thôi nào. Nếu lần tới 
chúng ta làm tiệc bánh thì sao?
Chúng ta sẽ có ăn vặt 
buổi trưa hoặc sau giờ học. 

95
00:09:57,113-->00:10:02,113
Đề nghị hấp dẫn đấy. 
Cô thấy sao Origami-san?

96
00:10:02,512-->00:10:06,513
Tôi cũng tham gia. Vậy chúng ta cùng đi.

97
00:10:07,112-->00:10:19,113
Tớ cũng sẽ đến. Đặc biệt là khi có
Rinne-san, Maria-san và Kotori-san,
những người cực kỳ thân mật với Shidou-san.

98
00:10:19,512-->00:10:23,113
Kurumi mỉm cười và liếc nhìn tôi.
Trong khoảnh khắc, tôi lặng lẽ quay đi.

99
00:10:23,312-->00:10:27,513
Các cậu nói thế thì tốt rồi.
Cậu thấy sao, Maria-chan?

100
00:10:28,112-->00:10:32,513
Nếu họ nói thế thì chúng ta 
không làm gì khác được rồi, Rinne.

101
00:10:33,112-->00:10:35,113
Cả Maria và Rinne đều nhìn hướng này.

102
00:10:35,312-->00:10:40,113
Được. Vậy tiếp theo sẽ là bánh kẹp!
Tớ rất muốn đến ngày đó đấy!

103
00:10:40,512-->00:10:46,113
Tớ cũng mong chờ lắm!
Tớ nghĩ tuần tới sẽ thích hợp hơn.

104
00:10:47,112-->00:10:50,113
Chuông reo rồi. Mọi người về chỗ đi.

105
00:10:50,512-->00:10:52,113
Đồng ý.

106
00:10:52,312-->00:10:53,913
Nói chuyện sau nhé.

107
00:10:54,112-->00:10:58,513
Mọi người đều vui vẻ khi nói chuyện
với Maria làm trung tâm. 
Tôi tự hỏi cảnh này sẽ kéo dài bao lâu.
Lồng ngực tôi lại nhức nhối lần nữa.

108
00:11:02,112-->00:11:04,113
Hết giờ học rồi.

109
00:11:05,112-->00:11:08,113
Shidou, nói chuyện được không?

110
00:11:08,312-->00:11:09,913
Maria à? Chuyện gì vậy?

111
00:11:10,112-->00:11:19,113
Hôm nay tớ và Rinne có vài việc phải làm
trước khi về nhà. Bọn tớ sẽ không về trễ đâu.

108
00:11:20,112-->00:11:22,113
Hai người đi chung à...
Được rồi, vui vẻ nhé.

109
00:11:22,512-->00:11:27,113
Ừ, có nhiều thứ tớ muốn được Rinne chỉ.

110
00:11:27,512-->00:11:30,113
Vậy tớ sẽ đi đâu đó để giết thời gian.
Cẩn thận nhé, Maria.

111
00:11:30,512-->00:11:36,313
Tớ hiểu rồi. Tớ nghĩ không có chuyện gì đâu. 
Cậu cũng bảo trọng nhé.

112
00:11:36,813-->00:11:38,113
Chắc chắn rồi. Gặp lại sau.

108
00:11:38,312-->00:11:40,513
Được rồi. Mình có thời gian rảnh.
Mình sẽ đi đâu đây?

109
00:11:46,112-->00:11:48,313
Vào cuối giờ, tôi lên sân thượng một mình, 
nghĩ về việc tôi nên làm tiếp theo.

110
00:11:48,812-->00:11:52,913
Ngay bây giờ, việc tôi ưu tiên là đi tìm Rio. 
Đó là việc ít nhất và tốt nhất
tôi có thể làm trong tình huống này.

111
00:11:53,112-->00:11:54,913
Có thể lắm chứ. 
Mình sẽ cố gắng thêm một lần nữa...

112
00:11:55,112-->00:11:57,513
Việc đấy đỡ hơn là đứng ở đây không làm gì cả. 
Cho đến khi mọi thứ chưa hoàn tất 
tôi sẽ không làm thêm chuyện gì cả.

112
00:12:01,112-->00:12:02,113
Rồi, đến lúc đi rồi.

113
00:12:02,312-->00:12:05,113
Tôi đợi trên sân thượng cho đến khi
mọi người đều đi khỏi trường.

114
00:12:05,312-->00:12:10,113
Dù sao tôi cũng không muốn để
những người bị thay đổi ký ức thắc mắc.
Vậy nên tốt nhất là tự làm một mình. 
Mặc dù sẽ rất tệ nếu mọi người phát hiện... Hở?

1
00:12:10,312-->00:12:11,513
Chờ đã...đó là?!

1
00:12:11,812-->00:12:13,113
Papa!

1
00:12:15,112-->00:12:16,913
Là em của ngày hôm qua...

1
00:12:17,112-->00:12:21,113
Không phải em, là Rio. 

1
00:12:21,312-->00:12:23,513
À, phải rồi...Xin lỗi nhé, Rio. 
Vậy Rio đang làm gì ở đây vậy?

1
00:12:24,112-->00:12:26,513
Rio ở đây để gặp Papa mà!

368
00:12:26,913-->00:12:29,113
Anh ư...? Vậy à. Đúng lúc thật. 
Anh cũng định đi tìm Rio đây.

368
00:12:29,512-->00:12:36,312
Rio vui lắm! Chúng ta đều nghĩ giống nhau.

368
00:12:36,912-->00:12:40,113
Tôi không nói dối việc đi tìm Rio 
nhưng tôi không ngờ Rio tự mình đến chỗ tôi.

368
00:12:40,312-->00:12:41,813
Hôm nay Rio không đi với Marina ư?

368
00:12:42,112-->00:12:48,913
Marina à? Marina sẽ đến khi trời tối cơ!

368
00:12:49,112-->00:12:50,513
Cậu ấy tốt thật nhỉ, phải không?

368
00:12:51,112-->00:12:58,113
Vâng, Marina rất tốt bụng! 
Bởi vì Marina đã nói chị ấy sẽ đi với Rio.

368
00:12:58,512-->00:12:59,913
Không phải tuyệt sao?

368
00:13:00,112-->00:13:05,913
Có vẻ như Rio và Marina đều tiến triển tốt. 
Vậy tại sao họ chỉ gặp nhau lúc trời tối nhỉ?
Cô ấy muốn chăm sóc Rio nhưng sao lại
để Rio đi loanh quanh ở đây?

368
00:13:06,112-->00:13:15,513
Nhưng biết không, Rio có nhiều điều vui 
khi Rio đi với papa lắm ấy! 
Vậy nên Rio sẽ ở đây để đưa papa đi với Rio.

368
00:13:15,912-->00:13:17,113
Đưa anh đi?

368
00:13:18,112-->00:13:25,113
Vâng, với Rio! Papa, papa có thể 
giúp Rio xem thứ đó được không?

368
00:13:25,312-->00:13:26,513
Thứ đó? Thứ mà Rio nói hôm qua ư?

368
00:13:27,112-->00:13:37,113
Đúng vậy, "Thứ quan trọng nhất"!
Rio nhất định phải tìm được.

368
00:13:37,312-->00:13:39,313
Phải tìm được à?

368
00:13:39,512-->00:13:43,113
Nếu nó liên quan đến "Eden", 
có thể nó giúp chúng tôi rời khỏi 
thế giới này như lần trước. 

368
00:13:43,312-->00:13:44,913
Đặc biệt là khi Rio có thể 
liên kết với Rinne...Nhưng.

367
00:13:45,112-->00:13:46,913
Tôi không chắc họ có liên quan 
cho đến khi tôi xác nhận nó.

368
00:13:47,112-->00:13:48,113
Um... Rio?

368
00:13:48,312-->00:13:50,513
Gì vậy, gì vậy, papa?

368
00:13:51,112-->00:13:52,513
Tại sao Rio lại gọi anh là papa?

368
00:13:53,112-->00:14:01,113
Tại sao à? 
Bởi vì Itsuka Shidou là papa của Rio mà. 
Và papa là papa mà.

368
00:14:01,313-->00:14:03,113
Ừm, à phải...đúng vậy.

368
00:14:03,312-->00:14:07,113
Tôi chỉ biết buông một tiếng thở dài 
vào câu trả lời tôi vừa nhận. 
Nó không khác câu trả lời hôm qua.

368
00:14:07,312-->00:14:11,113
Sao vậy, papa? Papa mệt ư?

368
00:14:11,512-->00:14:13,113
Không có gì. Anh có thể hỏi Rio 
một điều nữa được không?

368
00:14:14,112-->00:14:17,513
Vâng, miễn là papa ở lại lâu với Rio là được!

368
00:14:18,112-->00:14:19,513
Ừ... được rồi. 
Chỉ hôm nay thôi nhé, được không?

368
00:14:20,112-->00:14:20,913
Vâng!

368
00:14:21,212-->00:14:23,313
Anh đã muốn hỏi Rio lâu rồi. 
Rio, Rio và Rin...

368
00:14:23,812-->00:14:28,113
Nữ sinh A: Nè nè, chẳng phải đó là 
Itsuka lớp năm hai sao?

368
00:14:28,312-->00:14:37,513
Nữ sinh B: Không phải chứ, 
một cô bé nhỏ nhắn đi với cậu ta sao? 
Vậy tin đồn cậu ấy là một Lolicon là... 

368
00:14:38,112-->00:14:39,913
Nữ sinh C: Không phải chứ...

368
00:14:40,112-->00:14:42,513
Xin lỗi, Rio. Chúng ta sẽ nói chuyện 
sau khi chúng ta đi xa hơn tí nữa.

368
00:14:43,112-->00:14:45,113
Vâng, con hiểu rồi.

368
00:14:45,312-->00:14:48,513
Nếu chúng tôi ở đây, 
những tin đồn sẽ lan rộng hơn. 
Rồi sẽ đến tai các cô gái.

368
00:14:53,112-->00:14:56,513
Bây giờ ổn hơn rồi. 
Anh có chuyện muốn hỏi Rio...

368
00:14:57,112-->00:14:58,113
Vâng?

368
00:14:58,312-->00:15:00,113
Rio, Rio có liên quan gì với Rinne không?

368
00:15:00,512-->00:15:08,113
Rinne? Ý papa là...Sonogami Rinne?

368
00:15:08,312-->00:15:10,313
Ừ, cậu ấy đó, Sonogami Rinne. 
Cậu ấy có họ giống như Rio ấy...

368
00:15:10,512-->00:15:12,113
Rio muốn gặp người ấy!

368
00:15:12,312-->00:15:13,913
Rio muốn...gặp Rinne ư?

368
00:15:14,112-->00:15:18,913
Rio... muốn gặp người ấy.

368
00:15:19,112-->00:15:21,513
Nhưng mà... Rio biết Rinne à?

368
00:15:22,512-->00:15:30,112
Vâng, Rio biết mà. 
Rio biết về người ấy cũng như papa đấy.

368
00:15:31,512-->00:15:37,113
Bởi vì Sonogami Rinne là mama. 
Mama của Rio mà.

368
00:15:37,912-->00:15:38,913
Ma...Mama?

368
00:15:39,113-->00:15:46,513
Vâng, mama! 
Rio gặp papa, Rio cũng muốn gặp mama nữa.

368
00:15:47,112-->00:15:50,513
Tôi sững sờ khi nghe những từ ấy. 
Tôi là bố và Rinne là mẹ? 
Tất cả việc này nghĩa là sao?

368
00:15:51,112-->00:15:56,113
Tôi và Rinne chưa từng có mối quan hệ như vậy 
nên không đời nào chúng tôi có em bé 
ở tuổi học tiêu học như vậy được.

368
00:15:57,112-->00:16:09,113
Rio thật sự muốn gặp mama. 
Nhưng mama đã nói Rio không được đến gặp mama.

368
00:16:09,512-->00:16:15,113
Rio muốn là một cô bé ngoan
nên Rio phải giữ lời hứa với mama.

368
00:16:15,312-->00:16:17,113
Anh hiểu...vậy Rio là một 
cô bé ngoan rồi đấy.

368
00:16:17,312-->00:16:21,113
Tôi nhẹ nhàng xoa đầu Rio. 
Rio thật sự vui vẻ và gần gũi với tôi hơn.

368
00:16:21,512-->00:16:23,913
Rio vẫn là một đứa trẻ. 
Dù tôi hỏi thêm, tôi không nghĩ 
tôi có thể biết thêm về Rio nữa.

368
00:16:24,112-->00:16:27,913
Từ những gì tôi biết, 
Marina có vẻ biết những gì đang xảy ra. 
Có thể tôi nên đi đâu đó và gặp Marina sau.

368
00:16:28,112-->00:16:30,513
Sẽ mất một thời gian trước khi 
Marina đến đưa Rio đi đấy. 
Đến lúc đó, Rio có muốn đi dạo không?

368
00:16:31,512-->00:16:44,113
Vâng, đi thôi nào! Đi dạo cùng với papa!
Cùng với papa! Đi cùng nhau!

368
00:16:44,512-->00:16:46,513
Hahaha... Rio trông háo hức quá nhỉ.

368
00:16:47,112-->00:16:52,113
Vâng, người ta nói trẻ con sẽ rất vui 
khi đi với bố mẹ của chúng! Như lúc này vậy!

368
00:16:52,912-->00:16:56,513
Ai đã nói với Rio như vậy? 
Sao cũng được, điều đó không quan trọng. 
Cùng đi và tận hưởng niềm vui nào, Rio.

368
00:16:57,312-->00:16:58,113


368
00:17:08,112-->00:17:09,113


368
00:17:10,112-->00:17:11,613




368
00:17:30,612-->00:17:32,113


368
00:17:33,112-->00:17:35,113


368
00:17:36,112-->00:17:38,113


368
00:17:39,112-->00:17:42,113


368
00:17:43,112-->00:17:44,113


368
00:17:44,512-->00:17:47,113


368
00:17:47,512-->00:17:50,113


368
00:17:51,112-->00:17:53,113

