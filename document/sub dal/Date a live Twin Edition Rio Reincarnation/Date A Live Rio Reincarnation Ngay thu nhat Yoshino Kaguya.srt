﻿0
00:00:02,112-->00:00:04,113
Đi đến Nhà Itsuka?
1) Đồng ý
2) Không đồng ý

0
00:00:08,112-->00:00:12,513
Cuối cùng cũng xong.
Phù. Hôm nay tôi phải mua 
những thứ đặc biệt để ăn mừng Rinne trở về.

1
00:00:13,112-->00:00:14,113
Hở? Đó là...

3
00:00:14,312-->00:00:15,513
Yoshino, mừng em về nhà.

4
00:00:16,112-->00:00:18,813
Mừng anh về nhà, Shidou-san.

5
00:00:19,112-->00:00:27,113
Mừng anh về nhà, Shidou-kun.
Gặp anh cùng lúc ra khỏi nhà,
phải chăng đây là số phận sao? 

6
00:00:27,512-->00:00:29,813
Thế sao? Em định đi đâu?

7
00:00:30,112-->00:00:35,513
Tớ run quá...

8
00:00:35,812-->00:00:39,113
Bây giờ chỉ cần gặp nhau
tại nơi đã bàn sẵn thôi.

9
00:00:39,312-->00:00:41,113
Gặp nhau tại nơi đã bàn? Với ai cơ?

10
00:00:41,812-->00:00:50,113
Có vẻ có khách không mời
trong cuộc tập hợp của bóng tối.

11
00:00:50,312-->00:00:52,513
A, Kaguya cũng về rồi à. 
Mừng cậu về nhà.

12
00:00:53,112-->00:00:57,513
Shidou-san... anh đi chơi với em được không?

13
00:00:58,112-->00:00:59,313
Anh à?

14
00:00:59,512-->00:01:17,513
Shdiou, tên hầu của tôi.
Bây giờ chúng ta sẽ đi theo
con đường của Chúa, 
vào căn phòng tẩy não ký ức.

15
00:01:18,112-->00:01:21,813
Vào căn phòng tẩy não ký ức à?

16
00:01:22,112-->00:01:27,513
Kaguya-chan muốn rủ anh đi xem phim đấy.

17
00:01:28,112-->00:01:29,513
Yoshinon hiểu được à?

18
00:01:30,112-->00:01:37,113
Kaguya-chan và Yuzuru-chan 
nói chuyện này với nhau trong lớp mà. 

18
00:01:37,512-->00:01:42,113
Giờ sao đây Shidou-kun? 
Anh muốn đi với ai?

19
00:01:42,512-->00:01:44,513
Thế thì... chỉ còn cách đi chung với nhau thôi.

20
00:01:45,112-->00:01:51,113
Đúng là Shidou-kun luôn xử lý 
theo cách này nhỉ! Vậy thì Go Go Go!

21
00:01:52,112-->00:01:57,513
Cậu phải luôn theo sát tôi đấy,
tên hầu của tôi.

22
00:01:58,112-->00:02:02,513
Được đi cùng Shidou-san thật may quá...

25
00:02:02,712-->00:02:03,513
Quyết định vậy nhé. Đi thôi!

25
00:02:03,912-->00:02:05,513
Vâng!

25
00:02:09,112-->00:02:13,513
Ít người xem thật. 
Vậy lát nữa chúng ta lên ghế trên nhé.

25
00:02:14,112-->00:02:16,513
Vâng, may thật đấy.

25
00:02:16,812-->00:02:19,513
Hy vọng phim này hay.
Ít người thế này có khi phim dở lắm.

25
00:02:20,112-->00:02:23,513
Vâng... hy vọng là vậy...

25
00:02:24,112-->00:02:27,513
Hy vọng là thế.

25
00:02:28,112-->00:02:36,113
Bắp rang, nước ngọt...
Cười khúc khích, lựa chọn...

25
00:02:36,512-->00:02:39,513
Kaguya, cậu suy nghĩ trong yên lặng đi.
Yoshino chọn gì?

25
00:02:40,112-->00:02:43,313
Im ngay! Đừng có bơ tôi!

25
00:02:43,812-->00:02:45,113
Đến giờ rồi. Vào thôi.

25
00:02:45,512-->00:02:46,513
Vâng!

25
00:02:50,512-->00:02:54,913
Phim nói về một con quỷ 
bị phong ấn đã hồi sinh.

25
00:02:55,112-->00:03:00,113
Trong phim, Yoshino và Kaguya 
rất tập trung vào phim.
Có vẻ họ khá thích thú.

26
00:03:00,312-->00:03:05,113
Bộ phim lấy từ cuốn tiểu thuyết 
giả tưởng nổi tiếng. 
Vậy nên có nhiều đoạn hơi khó hiểu.

26
00:03:05,812-->00:03:10,513
Nhưng về những cảnh hành động, chiến đấu
và nội dung thì rất hay.

26
00:03:10,912-->00:03:18,113
Và khi phòng chiếu bật đèn, 
tôi nhận thấy bầu không khí 
thỏa mãn với bộ phim.

26
00:03:22,112-->00:03:23,513
Hay thật đấy.

26
00:03:23,912-->00:03:32,113
Ừ. Đôi mắt, bóng tối...
Cậu đã dẫn tôi đến một nơi tuyệt vời.

26
00:03:32,312-->00:03:34,313
Haha, Có lẽ vậy. Còn Yoshino và Yoshinon thì sao?

26
00:03:34,812-->00:03:41,113
Em vui lắm. Phim rất hay!

26
00:03:41,312-->00:03:43,513
Ừ. Vậy còn Yoshino?

26
00:03:44,912-->00:03:46,513
Yoshino?

26
00:03:46,812-->00:03:52,513
Yo-shi-no, phim hết rồi.

26
00:03:53,112-->00:03:58,513
Em... em xin lỗi!

26
00:03:59,112-->00:04:01,113
Ừ. Em thích bộ phim phải không?

27
00:04:01,312-->00:04:03,113
Vâng.

28
00:04:03,512-->00:04:08,113
Vậy giờ sang bên kia xem đi.

38
00:04:08,512-->00:04:11,913
Kaguya tự tin chỉ vào góc hành lang.
Chúng tôi nhìn theo. Đó là...

39
00:04:12,112-->00:04:14,813
Chỗ đó trưng bày mấy món đồ trong phim đấy.
Muốn ra xem không?

40
00:04:15,113-->00:04:18,813
Vâng, ra đó xem đi!

41
00:04:19,113-->00:04:23,313
Vậy đi nhanh lên!

42
00:04:23,813-->00:04:36,113
Ngay cả trí tuệ tuyệt vời của tôi 
cũng dẫn tôi đến câu trả lời!
Một lần nữa...

43
00:04:36,312-->00:04:39,513
Này, chờ đã! Đừng bỏ tôi!

39
00:04:42,112-->00:04:44,513
Yoshino dừng lại trước một sản phẩm
trong góc trưng bày.

40
00:04:44,913-->00:04:46,813
Đó là thanh kiếm của anh hùng 
và quả cầu tiên tri đấy.

41
00:04:47,113-->00:04:50,313
Vâng, trông giống thật...

42
00:04:50,813-->00:04:58,113
Em không nhận ra điều gì
khi xem thứ này sao?

43
00:04:58,312-->00:05:00,513
Cậu đang nói về cái gì vậy?

44
00:05:00,913-->00:05:07,513
Không biết à? Quả cầu pha lê 
chứa một làn sóng sức mạnh lớn.

45
00:05:07,912-->00:05:11,113
Không, đó chỉ là trong phim thôi.

46
00:05:11,312-->00:05:27,513
Đây là món đồ ở thị trấn ban đầu 
của nhân vật chính. Thanh kiếm dành cho
người cuối cùng chống lại ma quỷ.

47
00:05:28,512-->00:05:46,113
Có nghĩa là anh hùng đã phá vỡ 
lời nguyền thanh kiếm.
Trong đó có linh hồn của mọi người sao?

48
00:05:46,312-->00:05:52,113
Mọi người đều ở trong đó đấy! 
Em không biết cũng đúng.

49
00:05:52,912-->00:06:02,913
Ác thần cổ đại tạo ra nhân vật chính
như một người hóa giải bóng tối của lịch sử.

42
00:06:03,312-->00:06:05,113
Này Kaguya... Nếu cậu nói thế...

42
00:06:05,312-->00:06:06,913
Shidou trật tự đi!

43
00:06:07,112-->00:06:24,113
Nhưng đó không phải là bí mật lớn.
Lời nguyền của quỷ đã thể hiện 
thông qua nhân vật chính.
Quả cầu chính là hiện thân của quỷ. 

44
00:06:25,112-->00:06:28,313
Lời nguyền của quỷ...

45
00:06:28,812-->00:06:33,513
Kaguya kể khác hẳn chuyện ban đầu. 
Cô ấy đang kể chuyện hư cấu
để khiến Yoshino tin nó.

46
00:06:34,112-->00:06:36,113
Sẽ rất khó để Yoshino quên nó...

47
00:06:36,313-->00:06:45,513
Tuy nhiên, Thiên Chúa đã sắp đặt
số phận cho anh hùng.
Yoshino có đoán đó là gì không?

48
00:06:47,112-->00:06:51,113
Thánh kiếm... Stardust Overcheer!

49
00:06:51,512-->00:07:09,113
Phải, thanh kiếm này mang sức mạnh tiềm ẩn.
Sức mạnh vô hạn của nó là của Chúa 
và linh hồn của mọi người.
Nó đã phá hủy quả cầu đó.

50
00:07:13,113-->00:07:19,813
Nó chỉ là mô hình thôi. 
Nhưng nó cũng khá giống thật đấy.

52
00:07:20,112-->00:07:22,513
Tuyệt quá...

53
00:07:23,112-->00:07:28,813
Mặc dù khá hay... nhưng Kaguya-chan, 
chuyện đó có thật không?

54
00:07:31,112-->00:07:37,313
Tất nhiên là thật rồi! 
Tại sao em lại hỏi thế?

55
00:07:37,512-->00:07:40,113
Nói xạo.

57
00:07:40,312-->00:07:45,113
Không! Tôi không nói xạo!

58
00:07:45,512-->00:07:47,113
Thế sao cậu lại kích động vậy?

59
00:07:47,312-->00:07:51,113
Không có! Cậu đừng nói nhảm!

60
00:07:52,112-->00:07:56,513
Đúng vậy. Em cảm thấy có sức mạnh trong nó.

61
00:07:56,812-->00:07:58,113
Yo... Yoshino?

62
00:07:58,312-->00:08:02,113
Có phải em cũng có
sức mạnh của thanh kiếm không?

63
00:08:02,312-->00:08:09,113
Yo... Yoshino? Đó chỉ là 
cái mô hình thôi mà. Nó không...

64
00:08:10,112-->00:08:16,113
Kaguya-san vừa nói chuyện đó có thật mà.

65
00:08:16,512-->00:08:24,113
Dĩ nhiên rồi! Chuyện của chị phải thật chứ!

66
00:08:24,312-->00:08:26,113
Tôi đã hoàn toàn hết cách để giải thích.

67
00:08:26,312-->00:08:27,113
Kaguya

68
00:08:27,312-->00:08:30,113
Gì vậy... Shidou?

69
00:08:31,112-->00:08:35,113
Dừng lại đi! Cậu không thấy 
Yoshino đang tin chuyện cậu kể sao?

70
00:08:35,512-->00:08:42,513
Không... Tôi chỉ nghĩ con bé 
đang thích cái mô hình thôi...

71
00:08:43,112-->00:08:46,313
Vậy cậu có lời giải thích phù hợp không?
Nếu không thì Yoshino tin sái cổ đấy.

72
00:08:51,112-->00:08:54,113
Nè Yoshino...

73
00:08:55,112-->00:09:04,113
Đúng như chị nói, Kaguya-san... 
Đây chỉ là mô hình.
Nhưng em có thể cảm thấy năng lượng...

74
00:09:04,513-->00:09:16,513
Chẳng phải chị nói sao? 
Nó không có gì đâu...

75
00:09:17,112-->00:09:26,513
Nhưng mà... Kaguya-san tuyệt thật đấy nhỉ.
Nhờ chị mà em biết thêm rất nhiều.

76
00:09:27,112-->00:09:37,113
Không... Chuyện đó...
Chuyện đó chỉ là chị bịa...

77
00:09:39,112-->00:09:41,813
Kaguya-san thật đáng nể!

77
00:09:42,112-->00:09:50,813
Đúng vậy! Chị lúc nào cũng thế mà!

78
00:09:51,112-->00:09:55,513
Kaguya... Đừng có hùa theo nữa.
Tớ đã nói cậu phải nói thật rồi mà.

79
00:09:56,512-->00:10:03,113
Không thể nào! Xin lỗi!
Tôi không thể nói thật được!

80
00:10:07,812-->00:10:11,813
Trên đường về nhà.
Cô ấy đã nói đó chỉ là một trò đùa.
Yoshino đã hiểu và vào nhà trước.

81
00:10:12,112-->00:10:14,513
Bằng cách nào đó, 
Yoshino đã may mắn nhận ra...

83
00:10:15,612-->00:10:19,513
Tôi xin lỗi...

84
00:10:20,112-->00:10:25,513
Dù sao, điều tốt là Yoshino đã biết sự thật.
Trong tương lai cô bé sẽ chú ý hơn
đến sự nhảm nhí của Kaguya.

85
00:08:12,512-->00:08:14,113




