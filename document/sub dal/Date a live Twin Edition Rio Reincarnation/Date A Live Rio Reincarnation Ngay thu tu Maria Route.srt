﻿0
00:00:21,112-->00:00:26,113
Shidou, chúng ta sẽ muộn 
nếu cậu cứ kéo dài thời gian đấy.

1
00:00:27,112-->00:00:30,113
Xin lỗi, tớ tìm được thứ 
cần tìm rồi! Đợi tớ chút!

2
00:00:34,112-->00:00:41,113
Thật là... Sao cậu không chuẩn bị đồ 
vào buổi tối chứ? Cậu phải ngừng 
thói quen vụng về đi đấy

3
00:00:42,112-->00:00:45,113
Xin lỗi các cậu. Cảm ơn đã đợi tớ.

4
00:00:45,512-->00:00:53,513
Hôm nay chỉ có 3 chúng ta thôi, 
vì thế chúng ta sẽ chịu trách nhiệm 
nếu cậu đi học trễ đấy.

5
00:00:54,512-->00:01:02,113
Và cậu không còn là học sinh tiểu học nữa 
nên đừng quay về nhà lấy đồ để quên nữa.
Mất thời gian lắm đấy.

6
00:01:03,112-->00:01:05,513
Không, tớ thật sự xin lỗi...  
Tớ vừa chợt nhớ ra thôi.

7
00:01:06,112-->00:01:14,113
Hãy ghi nhớ lời cậu nói đi nhé.
Giờ chúng ta phải đến trường đúng giờ đã.
Bắt đầu học tập từ lỗi lầm lầm của cậu, ngốc ạ.

8
00:01:15,112-->00:01:20,113
Không cần phải như vậy lắm đâu, 
Marina... Chúng ta vẫn còn thời gian mà.

16
00:01:21,112-->00:01:24,113
Ừ... Với lại... 
Sao hai cậu lại nắm tay tớ vậy?

17
00:01:25,112-->00:01:31,113
Shidou, cậu không muốn nắm tay chúng tớ ư?

18
00:01:32,112-->00:01:37,113
Không phải tớ không thích... 
Chỉ hơi lạ vì trước giờ 
hai cậu chưa bao giờ làm vậy thôi, phải không?

19
00:01:38,112-->00:01:46,113
Vì Marina bảo cậu bất cẩn nên sẽ tốt hơn 
nếu chúng tớ đưa cậu đi thật nhanh.

20
00:01:48,112-->00:01:54,113
Không phải chúng ta làm việc này 
vì cậu hỏi tớ làm sao để nắm tay Shidou 
một cách tự nhiên ư?

21
00:01:55,112-->00:01:59,113
Tớ đã hỏi việc như vậy sao? 
Sao tớ không nhớ nhỉ.

25
00:02:00,112-->00:02:08,113
Nhớ lại đi... Thật là.... 
Đừng có hiểu lầm nhé, Shidou. 
Tớ nắm tay cậu vì tớ buộc phải thế thôi.

25
00:02:09,112-->00:02:13,513
Cả Marina nữa, cậu Tsundere nhiều lắm đấy.

25
00:02:14,512-->00:02:21,113
Yên lặng đi! Tớ không muốn nghe điều đó 
từ một người đang cố diễn như một người vợ 
có cái miệng độc địa như cậu đâu.

25
00:02:21,512-->00:02:26,113
Haha.... Nếu chúng ta cứ nói chuyện thế này 
thì chúng ta sẽ trễ thật đấy. 
Chúng ta nên đi được chưa?

25
00:02:27,112-->00:02:33,513
Cậu nói đúng, chúng ta đi thôi. 
Marina cứ làm chúng ta phân tâm 
về việc nắm tay mãi.

25
00:02:34,112-->00:02:38,513
Nếu cậu muốn gây chiến thì đến đi. 
Tớ bực mình rồi đấy.

25
00:02:39,112-->00:02:40,513
Hai cậu bình tĩnh lại đi, được chứ?

25
00:02:41,112-->00:02:47,113
Cậu không cần phải lo đâu, Shidou.
Đây chỉ đơn thuần là một cách để 
liên kết tình chị em của chúng tớ thôi mà.

25
00:02:48,112-->00:02:51,113
Mà tớ nghĩ thế nào cũng vậy thôi 
nếu chúng ta cứ luôn như thế này.

25
00:02:52,112-->00:02:55,513
Luôn luôn...ư? Những lời đó có thể đến từ cô ấy
một cách tự nhiên như vậy à?

25
00:02:56,112-->00:02:58,513
Có vẻ hai cậu đã có nhiều niềm vui nhỉ.

25
00:02:59,112-->00:03:02,513
Ừ. Chúng tớ vui lắm.

25
00:03:03,112-->00:03:13,513
Đúng là chẳng bao giờ buồn khi ở bên cậu. 
Mỗi ngày trôi qua, thứ gì đó sẽ xảy đến
với những ngày bình yên này
sẽ không tồn tại với cậu.

25
00:03:14,112-->00:03:16,513
Nhưng như thế mới thú vị mà.

26
00:03:17,112-->00:03:20,113
Có lẽ... đó là lý do mỗi ngày đều... vui vẻ nhỉ.

26
00:03:21,112-->00:03:26,113
Đi thôi nào, Shidou. Hãy chắc chắc là 
cậu sẽ nắm tay chúng tớ thật chặt nhé.

26
00:03:27,112-->00:03:32,113
Cũng đừng rời tay tớ nữa. 
Nếu cậu làm vậy, đừng nghĩ cậu sẽ dễ dàng
thoát khỏi rắc rối đấy.

26
00:03:33,112-->00:03:34,513
Ừ. Đi thôi nào!

26
00:03:35,112-->00:03:41,113
Và như thế, chúng tôi đi nhanh hết sức có thể.
Cảm giác giống nhau từ đôi bàn tay tôi
cho biết họ thật sự là 2 chị em.

26
00:03:43,112-->00:03:48,513
Cuộc sống thường ngày vẫn tiếp tục. 
Chậm rãi, từng bước một. 
Chỉ ở mức vừa phải và không gì đặc biệt cả.

26
00:03:49,112-->00:03:53,513
Tuy nhiên, tôi tin mọi chuyện rồi sẽ ổn thôi. 
Bởi vì đây là lý do tôi chọn thế giới 
mà Rio đã giao lại cho chúng tôi.

26
00:03:54,112-->00:03:55,513
Shidou!!!

26
00:03:56,112-->00:04:01,113
Khi những nụ cười ấy còn tồn tại, 
tôi sẽ không hối tiếc về sự lựa chọn của mình.

26
00:03:22,112-->00:03:13,113

