﻿1
00:00:09,112-->00:00:13,113
Đi đến Khu mua sắm
1) Đồng ý
2) Không đồng ý

2
00:00:17,512-->00:00:24,113
Phù. Cuối cùng cũng xong.

3
00:00:26,112-->00:00:32,113
Phù. Tôi không biết rằng 
hôm nay phải trực nhật.
Không còn ai trong lớp cả.

4
00:00:33,512-->00:00:40,113
Giờ phải chuẩn bị bữa ăn tối.
Mình sẽ đi qua siêu thị.

5
00:00:46,112-->00:00:55,513
Hôm nay tôi phải mua những thứ đặc biệt
để ăn mừng Rinne trở về.
Tôi nên đi nhanh rồi về giúp Rinne.

6
00:00:57,112-->00:01:05,113
Không, khoan đã. Maria và những người khác
cũng về rồi. Rinne sẽ không làm một mình đâu.

7
00:01:06,512-->00:01:13,113
Shidou! Quả nhiên anh vẫn ở đây!

8
00:01:15,112-->00:01:20,113
Tohka? Em không đi cùng mọi người à?

9
00:01:21,512-->00:01:27,513
Ừ! Em muốn quay lại kiểm tra thử!

10
00:01:29,112-->00:01:35,113
Anh xin lỗi. Cảm ơn em.
Em muốn đi siêu thị mua đồ ăn không?

11
00:01:37,512-->00:01:45,113
Mua đồ cho tối nay sao?
Vậy là sẽ có Rinne...

17
00:01:47,112-->00:01:52,113
Không, anh chỉ muốn có ý tưởng
về đồ ăn tối nay thôi.

18
00:01:54,112-->00:02:01,513
Ra là vậy! Tất nhiên là được!
Em cũng đang đói mà!

25
00:02:03,112-->00:02:09,113
Không... Không phải là đi ăn đâu...

25
00:02:11,112-->00:02:17,113
Câu hỏi. Cậu định đi đâu vậy, Shidou?

25
00:02:18,512-->00:02:25,513
À, đến trung tâm mua sắm... Yuzuru?
Cậu cũng phải trực nhật à?

25
00:02:27,112-->00:02:40,513
Xác nhận. Tớ cũng phải trực nhật.
Kaguya đã đi với Yoshino 
để giúp cô bé mạnh dạn hơn.

25
00:02:42,112-->00:02:48,113
Yuzuru cũng đang ở đây. 
Vậy chúng ta sẽ đi chung vậy.

25
00:02:50,112-->00:03:02,113
Trả lời. Tớ cũng muốn đi với Shidou.
Ngoài ra, tớ muốn cậu cùng đến các gian hàng.

25
00:03:04,112-->00:03:08,113
Gian hàng nào?

25
00:03:09,112-->00:03:22,113
Thông tin. Lễ hội Gourmet đang mở 
tại trung tâm mua sắm.
Cậu muốn đến đó không?

25
00:03:24,112-->00:03:33,113
Lễ hội Gourmet? 
Chỗ đó có nhiều đồ ăn không?

25
00:03:35,112-->00:03:50,513
Trả lời. Chỗ đó có các món ăn đặc sản 
của mọi miền trên cả nước.
Tôi nghĩ Tohka sẽ rất thích đấy.

26
00:03:52,112-->00:03:57,513
Nghe hấp dẫn đấy!

26
00:03:59,112-->00:04:03,113
Đúng vậy. Tớ cũng chưa đến đó.

33
00:04:05,112-->00:04:13,513
Nhạy bén. Tớ nghĩ sẽ có đồ ăn lạ ở đó.

39
00:04:15,112-->00:04:19,513
Được rồi, vậy chúng ta sẽ đến xem.

40
00:04:21,213-->00:04:28,513
Nếu tìm được món gì đó cho 
buổi tối nay thì sẽ rất thú vị.

41
00:04:29,513-->00:04:36,513
Không biết ở đó có gì... Hồi hộp thật đấy!

42
00:04:38,113-->00:04:43,113
Này, còn bữa tối thì sao?

44
00:04:44,112-->00:04:50,113
Không sao đâu. Em chỉ ăn một chút thôi.

45
00:04:52,112-->00:04:56,113
Một chút thôi à...

46
00:04:58,112-->00:05:02,113
Vì Tohka đi theo nên tôi có chút lo lắng.

47
00:05:07,512-->00:05:12,113
Mọi người đến đây đông hơn ngày thường.

48
00:05:14,112-->00:05:25,113
Nhận xét. Các gian hàng lễ hội 
khó có thể vào được.

49
00:05:28,112-->00:05:37,113
Shidou, em ngửi thấy mùi gì
rất ngon trong đó!

50
00:05:39,112-->00:05:46,113
Ừ, các gian hàng đang làm đồ ăn đấy.
Đồ ăn ở đó được làm công phu lắm.

51
00:05:48,112-->00:05:56,113
Vậy chúng ta vào đó đi.
Em không thể đợi được nữa!

60
00:05:57,512-->00:06:03,113
Không chỉ Tohka, 
cả Yuzuru cũng hứng thú với chuyện ăn uống.

41
00:06:06,112-->00:06:11,513
Trở lại. Xin lỗi đã để cậu đợi. Đồ ăn đây.

42
00:06:13,112-->00:06:20,113
Ồ, Yuzuru! Cậu mua gì mà nhanh thế?

43
00:06:21,112-->00:06:25,513
Cậu cầm gì trên tay vậy?

44
00:06:27,112-->00:06:38,513
Giới thiệu. Nước ép trái cây thôi.
Tớ định mua nó cho cậu đấy.

45
00:06:40,512-->00:06:52,113
Nước cam à? À, em vừa đến một gian hàng
đang nấu một nồi to. Trong đó có nhiều 
loại trái cây lắm. Không biết đó là gì?

46
00:06:54,112-->00:07:12,113
Chỉ dẫn. Có lẽ là nước sốt ponzu mitsukan
Đó là một loại nước chấm làm từ trái cây,
thường là từ cam, quýt và đường.

51
00:07:14,112-->00:07:21,113
Nghe Yuzuru mô tả lại, tôi và Tohka ngạc nhiên.

61
00:07:23,112-->00:07:28,113
Sao cậu biết rõ thế...?

72
00:07:29,512-->00:07:38,513
Chỉ dẫn. Tớ nghĩ nó sẽ làm các cậu
muốn đi xem lễ hội hơn.

73
00:07:40,112-->00:07:47,513
Vậy nếu cậu muốn, 
chúng ta sẽ đi xem lễ hội nhiều hơn.

74
00:07:49,112-->00:07:57,113
Chấp nhận. Tớ cũng muốn đến chỗ này.

81
00:08:00,112-->00:08:09,113
Món này có vị chua ngọt thật tuyệt!

82
00:08:10,512-->00:08:15,113
Ừ. Món nào em cũng khen ngon hết mà.

83
00:08:17,112-->00:08:23,513
Nhìn Tohka ăn ly Oden một cách sung sướng, 
tôi mở lon uống nước ép cam quýt của Yuzuru.

84
00:08:25,112-->00:08:33,113
À, Tohka có muốn uống nước không?
Thêm chút vị cam chua ngon lắm đấy.

85
00:08:34,112-->00:08:39,513
Ừ, vị cay cay thật hấp dẫn.

86
00:08:41,112-->00:08:54,513
Bình luận. Axit hữu cơ giúp 
phục hồi sau mệt mỏi.
Nó cũng hiệu quả để giảm đau dạ dày.

95
00:08:56,112-->00:09:13,513
Thêm vào. 
Nguyên liệu cam quýt cung cấp vitamin C
giúp ngăn ngừa bệnh tật nữa.

102
00:09:15,112-->00:09:24,113
Sao cô biết nhiều thứ vậy?

103
00:09:25,512-->00:09:31,513
Đúng vậy, tớ ngạc nhiên 
với hiểu biết của cậu đấy.

104
00:09:33,112-->00:09:43,113
Kể chuyện. Trong cuộc thi trước đó 
với Kaguya, bọn tớ đã đi ăn khắp nơi.

105
00:09:44,512-->00:09:51,113
Thi như thế thì làm sao thắng được chứ...

106
00:09:52,512-->00:10:01,513
Hài lòng. Vậy chúng ta thử món bánh cá nhé.

107
00:10:03,112-->00:10:09,113
Cái gì thế này! 
Nó khác hẳn món bánh cá thường!

108
00:10:11,512-->00:10:15,113
Chờ đã Tohka!

1
00:10:21,112-->00:10:32,513
Cảm nhận. Quả nhiên nó khác bánh cá thường.
Nước chấm cũng làm rất vừa.

1
00:10:34,112-->00:10:40,113
Ngon thật. Đây là nước chấm gì vậy?

1
00:10:42,112-->00:10:50,113
Đó là nước thịt đấy. 
Chấm bánh cá với nước thịt 
khác bình thường nhưng mà ngon.

1
00:10:51,112-->00:11:02,113
Ừ! À phải rồi! Shidou cũng ăn nhanh 
đi cho nóng. Để em đút cho anh nhé

368
00:11:03,512-->00:11:07,113
Không... Anh...

68
00:11:08,512-->00:11:15,513
Nhanh nào Shidou. Anh không thích ăn thử à?

368
00:11:17,112-->00:11:21,113
Không... Nhưng mà...

368
00:11:22,112-->00:11:26,113
Làm thế này trước mặt mọi người 
khiến tôi rất xấu hổ.

368
00:11:28,112-->00:11:34,513
Nào! Nói Aaa đi nào! Aaa...!

68
00:11:36,112-->00:11:39,113
Um...

368
00:11:40,512-->00:11:45,513
Tôi đoán mình không có lựa chọn nào...

368
00:11:47,112-->00:11:51,113
Aaaa...

368
00:11:52,512-->00:11:57,113
Thế nào? Ngon phải không?

368
00:11:58,112-->00:12:04,513
Ừ, ngon lắm.
Món bánh cá này có thịt bò trong đó.

368
00:12:06,112-->00:12:12,113
Vậy thì tốt. 
May là anh ăn trúng miếng thịt bò đó đấy.

368
00:12:14,113-->00:12:17,513
Ừ, đúng vậy.

368
00:12:19,112-->00:12:27,113
Ghen tị. Cậu có vẻ thiên vị Tohka quá nhỉ?

368
00:12:28,512-->00:12:34,113
Không, sao vậy? Chỉ là miếng bánh thôi mà...

368
00:12:35,112-->00:12:46,113
Cáo buộc. Vậy nếu tớ làm thế
cũng không sao phải không?

368
00:12:47,512-->00:12:52,313
Tôi nghe nói Yuzuru nói và bối rối.

368
00:12:53,512-->00:12:57,513
Không... Sao cậu nghĩ thế?

368
00:12:59,112-->00:13:05,513
Yêu cầu. Nhanh há miệng ra nào.

368
00:13:07,112-->00:13:11,513
Nhưng ở đây đông người lắm...

368
00:13:13,112-->00:13:23,513
Từ chối. Đừng để ý gì nữa.
Bây giờ nói Aaa đi nào.

368
00:13:25,113-->00:13:29,113
Yuzuru đưa miếng bánh lại gần tôi.

368
00:13:30,512-->00:13:34,113
Aaa... 

368
00:13:35,512-->00:13:44,113
Cảm xúc. Aaa... thật to nào.

368
00:13:45,512-->00:13:49,113
Của cậu cũng ngon lắm!

368
00:13:53,112-->00:14:00,513
Đồng ý. Đồ ăn của tôi là ngon nhất.

368
00:14:02,112-->00:14:11,513
Của tôi mới ngon nhất!
Miếng hình tam giác này là gì vậy?

368
00:14:13,112-->00:14:26,113
Đầu cơ. Đây là... hanpen đấy.
Nó hơi khác một chút.

368
00:14:27,512-->00:14:35,513
Nó cũng giống như bánh cá thôi.
Chỉ khác là Hanpen bỏ vào nấu lên 
nên nó có màu đen.

368
00:14:37,112-->00:14:49,113
Vậy sao? Em thấy nó làm từ bột
giống bánh cá nên em không biết.

368
00:14:50,112-->00:15:00,113
Anh thấy cửa hàng nào cũng cắt nó
hình tam giác thế này. Anh nghĩ làm thế
hương vị của nước dùng sẽ thấm vào trong.

368
00:15:01,512-->00:15:12,513
Thuyết phục. Chắc màu đen đó
là do nấu lên và nước dùng cùng gia vị
trải đều miếng bánh.

368
00:15:14,112-->00:15:22,113
Đó là bí quyết nấu ăn của họ đấy.

368
00:15:25,112-->00:15:33,513
Sâu sắc. Đúng là Shidou.
Xứng đáng làm trụ cột gia đình lắm.

368
00:15:35,112-->00:15:40,513
Haha, cảm ơn cậu. 
Giờ chúng ta kiếm đồ ăn nhẹ nhé.

368
00:15:42,112-->00:15:49,513
Bữa ăn nhẹ? Vậy Oden hồi nãy 
không phải bữa ăn nhẹ à?

368
00:15:51,112-->00:16:04,513
Trả lời. Từ đây về nhà có nhiều 
cửa hàng bánh kẹo mua về làm đồ ăn nhẹ.

368
00:16:06,112-->00:16:17,513
Oden bánh cá ngon đấy.
Nhưng mọi người lại chưa có gì để ăn cả...

368
00:16:19,112-->00:16:25,113
Vậy giờ chúng ta mua cho họ đi.

368
00:16:26,512-->00:16:30,113
Đồng ý!

368
00:15:31,512-->00:15:37,113
Chấp nhận. 
Tớ nghĩ mọi người sẽ rất hài lòng.

368
00:16:39,112-->00:16:46,513
Chỉ ăn nhẹ thôi để dành bụng
cho bữa tối nữa.

368
00:16:48,112-->00:16:57,113
Chán thật đấy.
Nhưng mọi người chắc đang chờ đấy!

368
00:16:58,512-->00:17:05,513
Kỳ vọng. Bọn tớ cũng mong chờ lắm đấy!

368
00:17:07,112-->00:17:13,513
Các cậu muốn mua gì cho bữa tối đây?
Chúng ta mua nhanh để về chuẩn bị bữa tối nữa.

368
00:17:15,112-->00:17:25,113
Ngăn lại. Chờ đã.
Vẫn còn nơi tớ rất muốn đến.

368
00:17:26,512-->00:17:31,113
Cậu muốn đến chỗ nào?

368
00:17:32,512-->00:17:41,113
Cảm nhận. Mùi của gia vị
làm tớ muốn ăn tiếp.

368
00:17:43,112-->00:17:48,113
Yuzuru, cậu bị nhiễm tính tham ăn
của Tohka rồi à?

368
00:17:52,112-->00:18:01,113
Khám phá. Là chỗ này. Hãy vào ăn thử.

368
00:18:03,112-->00:18:09,113
Yuzuru vui vẻ bước vào quán ăn. Đó là...

368
00:18:12,112-->00:18:21,113
Cà ri ramen sao? 
Nó trông giống một tô mì thường thôi mà.

368
00:18:23,112-->00:18:38,113
Xác nhận. Cậu nói đúng.
Nó giống như sự kết hợp 
giữa cà ri cay với mì thông thường.

368
00:18:40,112-->00:18:45,113
Nhưng Yuzuru... cậu không sợ no à?

368
00:18:46,512-->00:18:59,113
Chấp nhận. Vì vậy, chúng ta hãy chia ra để ăn.
Như vậy sẽ không no trước khi ăn tối.

368
00:19:01,112-->00:19:06,113
Đúng vậy, chia ra đi.

368
00:19:07,512-->00:19:12,113
Em vẫn ăn được mà Shidou.

368
00:19:14,512-->00:19:18,113
Tuy nhiên, Tohka...

368
00:19:20,112-->00:19:27,913
Bánh cá, Oden và giờ đến mì...
Đối với tôi, đây giống như bữa tối rồi.

368
00:19:28,512-->00:19:34,513
Tohka ngờ vực nhìn tôi, do dự, 
và nhìn vào bên trong cốc gắp mì ăn.

368
00:19:36,112-->00:19:43,513
Mì này nhiều vị quá nhỉ!

368
00:19:45,112-->00:20:02,113
Hài lòng. Tôi vừa ăn nhiều loại gia vị.
Giờ lưỡi tôi sắp tê vì loạn vị rồi đấy.

368
00:20:04,113-->00:20:09,113
Tớ cũng thấy hơi cay.

368
00:20:10,113-->00:20:15,113
Dù sao, tôi chỉ ăn một chút thôi.

368
00:20:16,113-->00:20:20,113
Này!

368
00:20:22,113-->00:20:29,113
Cho thêm gia vị vào cà ri sẽ mặn hơn đấy.

368
00:20:30,513-->00:20:41,113
Yuzuru nói đúng!
Cà ri ramen giống như món thập cẩm
pha trộn độc đáo vậy.

368
00:20:42,513-->00:20:48,113
Tôi nhớ lớp B nói nó giống như phép lạ vậy.

368
00:20:53,513-->00:20:58,113
Phù. Giờ tớ cảm thấy bụng dạ rối tung rồi đấy.

368
00:21:00,112-->00:21:06,113
Bỏ ly mì vào thùng rác, 
chúng tôi ngồi trên băng ghế 
bên cạnh máy bán hàng tự động. 

368
00:21:07,512-->00:21:13,113
Cuộc hẹn ba người này vẫn tiếp tục.

368
00:21:15,112-->00:21:21,113
Này Shidou, phía đó có mùi gì vậy?

368
00:21:22,512-->00:21:26,513
Hở? Phía đó...

368
00:21:28,112-->00:21:33,113
Tôi quay về phía Tohka chỉ.

368
00:21:34,512-->00:21:40,113
Một tấm khăn. Gian hàng phía trước 
của lễ hội có một tấm khăn phủ phía trước.

368
00:21:41,512-->00:21:46,513
Và trong gian hàng là người đang làm mì Yakisoba.

368
00:21:48,112-->00:21:56,513
Mì Yakisoba? Đó là gì vậy?

368
00:21:58,112-->00:22:14,513
Bình luận. Đó là mì xào Yakisoba.
Âm thanh và mùi vị đó rất đặc trưng.

368
00:22:16,112-->00:22:23,513
Được! Vậy chúng ta vào đó đi!
Đi thôi Shidou, Yuzuru!

368
00:22:25,112-->00:22:31,113
Hả? Này! Chờ đã Tohka!

368
00:22:34,112-->00:22:39,513
Cuối cùng chúng tôi lại ăn thêm.

368
00:22:41,112-->00:22:48,113
Không thể nào cản Tohka ngừng ăn
cho đến khi cô ấy ăn hết cả lễ hội đó.

368
00:22:54,112-->00:23:03,112
Tôi quyết định dành chút thời gian
riêng tư trong phòng mình.
Rinne và Maria đang chuẩn bị bữa tối.

368
00:23:04,112-->00:23:12,113
Tôi nằm lên giường lần đầu kể từ buổi sáng.
Căn phòng tôi ở được tạo ra bởi ảo giác này.

368
00:23:13,112-->00:23:17,113
Đúng như mình nghĩ.

368
00:23:18,112-->00:23:26,113
Khi tôi nói chuyện với bản thân,
tôi mở ngăn kéo bàn ra và xác nhận
thứ bé nhỏ đó vẫn còn trong cái túi.

368
00:23:27,112-->00:23:34,113
Tôi nhớ chính xác, đó là chìa khóa
Kotori nói tôi đưa cho bất kỳ ai tôi muốn.

368
00:23:35,112-->00:23:42,113
Nó không nên ở đây. 
Nó nên thuộc về cô ấy.

368
00:23:43,112-->00:23:52,113
Tôi càng từ chối, tôi càng chiến đấu
với sự thật. Sự thật về thế giới này,
về Maria ở đây và về Rinne ở đây.

368
00:23:53,112-->00:24:00,113
Chìa khóa này đã được đưa cho Rinne.
Lý do vì chúng tôi xem nhau như một gia đình.

368
00:24:01,112-->00:24:10,513
Rinne vẫn nhớ lần gặp cuối cùng, 
về chuyện mọi thứ lặp đi lặp lại.
Cô ấy đã biến mất cùng Eden.

368
00:24:11,512-->00:24:20,113
Tôi phải đưa Rinne thứ này lần nữa.
Và có thể nó sẽ lại đem đến 
những kỷ niệm buồn hơn bất kỳ thứ gì.

368
00:24:21,112-->00:24:25,513
Mình nên làm gì đây?

368
00:24:26,512-->00:24:35,513
Tôi không thể để thế giới này tiếp tục.
Nhưng tôi không muốn bỏ những ngày tháng
tuyệt đẹp tôi được sống cùng Rinne và Maria.

368
00:24:37,512-->00:24:45,113
Onii-chan! Mọi người đã sẵn sàng rồi!

368
00:24:46,112-->00:24:50,113
Được rồi, anh xuống ngay.

368
00:24:51,112-->00:24:57,113
Tôi bỏ chìa khóa vào trong túi 
rồi xuống dưới nhà.

368
00:25:03,112-->00:25:13,113
Yoshino-chan, hôm nay em dễ thương lắm.
Nói chị nghe đi, ngày nào trong tuần
em dễ thương nhất?

368
00:25:14,112-->00:25:27,113
Em... em không biết.
Yuzuru-san, em nên trả lời thế nào?

368
00:25:28,112-->00:25:45,113
Chỉ dẫn. Sư phụ Origami từng nói
không lúc nào con gái không dễ thương.
Phụ nữ sẽ luôn dễ thương vì người họ thích.

368
00:25:46,112-->00:25:57,113
Origami-chan nói hay quá!
Nào Yoshino, đến lượt cậu trả lời
một cách rõ ràng rồi đấy!

368
00:25:58,112-->00:26:06,113
Sao tớ có thể...

368
00:26:10,512-->00:26:25,113
Vậy đây là khiên đen khổng lồ
chứa lửa địa ngục dành cho tối nay à?
Thật hoàn hảo cho những sự sống 
bị đem đi hiến tế.

368
00:26:26,112-->00:26:36,513
Vậy nếu có Tenpan-Kaguya 
để kiểm tra cái chảo thì sao?
Origami, trói Kaguya lại đi.

368
00:26:37,512-->00:26:41,113
Để đấy cho tôi.

368
00:26:44,112-->00:26:52,113
Chờ đã! Tôi không nói các cô
đem tôi ra hiến tế!

368
00:26:53,512-->00:27:04,113
Đừng có khó chịu nữa.
Ra chơi với mọi người đi.
Bọn tôi không làm được nếu cô cứ thế.

368
00:27:05,112-->00:27:17,113
Hiểu rồi. Tôi sẽ im lặng 
chờ đợi đại tiệc tối nay.

368
00:27:19,112-->00:27:27,413
Maria, tôi sẽ quay lại sau khi
trói và ném cô ta ra ngoài.

368
00:27:28,112-->00:27:32,113
Phiền cô vậy.

368
00:27:34,112-->00:27:39,113
Sao lại là tôi?

368
00:27:43,112-->00:27:52,113
Náo nhiệt quá nhỉ.
Rinne-oneechan, chị muốn thêm đĩa nữa không?

368
00:27:54,112-->00:28:08,113
Xem nào. Chị thấy thế là được rồi.
Thêm nữa thì nhiều quá. Thiếu thì lấy sau.

368
00:28:10,512-->00:28:17,113
Thế này được rồi, nhất là khi 
có ngần này người. 

368
00:28:18,512-->00:28:24,113
Mọi người ở đây cả rồi.
Mình nên phát biểu cảm nghĩ thế nào nhỉ.

368
00:28:26,112-->00:28:32,113
Kurumi và Origami cũng ở đây.
Tôi có linh cảm xấu về chuyện này.

368
00:28:34,112-->00:28:40,113
Kotori, Reine-san đâu?
Cô ấy không đến à?

368
00:28:42,112-->00:28:51,113
Reine à? Em nói cô ấy tốt nhất 
đừng đến vì Origami đang ở đây.

368
00:28:53,112-->00:28:57,113
Anh hiểu rồi.

368
00:28:58,112-->00:29:04,113
Sự thật là Reine-san là giáo viên.
Tốt nhất là cô ấy không nên ở đây
khi có Origami.

368
00:29:06,112-->00:29:15,113
Shidou, thật tuyệt khi tối nay 
có nhiều người và nhiều đồ ăn nhỉ!

368
00:29:17,112-->00:29:21,113
Phải, lần đầu tiên chúng ta
tổ chức được thế này đấy.

368
00:29:21,312-->00:29:25,113
Nhưng đừng quên tối nay là buổi tiệc
mừng Rinne về nhà. Nhất là khi em là người
đề xuất ý kiến này.

368
00:29:26,512-->00:29:34,113
Em biết mà! Tối nay sẽ là bữa tiệc lớn đấy!

368
00:29:36,112-->00:29:41,113
Tohka, em có hiểu gì không vậy?

368
00:29:42,112-->00:29:48,113
Chúng ta bắt đầu đi, Onii-chan!
Anh là người ra hiệu bắt đầu bữa tiệc đấy!

368
00:29:50,112-->00:29:55,113
Được rồi được rồi. Để đấy cho anh.

368
00:29:57,112-->00:30:01,113
Mọi người nói tôi là chủ nhà,
thực ra là không phải.

368
00:30:03,113-->00:30:08,113
Vậy trước khi bắt đầu, 
Rinne có muốn phát biểu gì không?

368
00:30:11,113-->00:30:15,113
Tớ à?

368
00:30:18,113-->00:30:24,113
Còn ai nữa? Mọi người đến đây
để mừng cậu trở về mà.

368
00:30:26,112-->00:30:33,113
Vậy thì...

368
00:30:34,512-->00:30:46,113
Cảm ơn mọi người đã đến đây.
Tớ hy vọng sau lần này, 
chúng ta có thể làm bạn với nhau.

368
00:30:48,112-->00:30:51,113
Nghe Rinne nói, tôi nín thở trong khoảnh khắc.

368
00:30:51,512-->00:30:58,113
Những lời Rinne nói đầy cảm giác hối tiếc.
Hối tiếc vì cô ấy biết mọi kết nối 
của mình với mọi người
không gì khác ngoài sự giả dối.

368
00:30:59,512-->00:31:03,113
Tôi nhìn Maria, và ánh mắt chúng tôi gặp nhau.
Cô ấy chỉ phản ứng lại với một nụ cười gượng.

368
00:31:03,512-->00:31:10,113
Cô ấy biết chúng tôi không thể làm gì.
Đó là sự thật duy nhất chúng tôi biết.

368
00:31:12,112-->00:31:22,113
Cậu nói gì vậy, Rinne?
Chẳng phải chúng ta đã là bạn rồi sao?

368
00:31:24,112-->00:31:33,113
À phải, cậu nói đúng.
Cảm ơn nhé, Tohka-chan.
Một lần nữa, mọi người giúp đỡ nhau nhé.

368
00:31:36,112-->00:31:39,113
Ừ, giúp đỡ nhau nhé.

368
00:31:42,112-->00:31:50,113
Cậu vẫn là một trở ngại mà tớ phải vượt qua.
Tuy nhiên, tớ sẽ không yên tâm
nếu cậu biến mất thêm lần nữa.

368
00:31:53,112-->00:32:03,113
Em rất mong chị được giúp đỡ, Rinne-san.
Và em cũng sẽ chăm sóc chị từ bây giờ.

368
00:32:06,112-->00:32:10,113
Giúp đỡ nhau nhé, Rinne-chan.

368
00:32:12,112-->00:32:26,113
Giúp đỡ nhau nhé, Rinne-san.
Tớ thấy mọi thứ xung quanh Shidou-san
thú vị hơn khi có cậu đấy.

368
00:32:28,112-->00:32:31,513
Có vẻ cô không biết mối quan hệ 
của chúng ta là chủ nhân và nô lệ.

368
00:32:32,112-->00:32:44,113
Cho dù cô muốn hay không, 
chúng ta sẽ tồn tại mãi mãi, 
không bao giờ có thể tách ra.

368
00:32:45,512-->00:32:56,113
Tin tưởng. Yuzuru và Kaguya đều nghĩ
cậu là một người bạn quý giá.

368
00:32:57,512-->00:33:08,113
Tớ và cậu đã là bạn từ ngày
chúng ta gặp nhau rồi.
Nhất là khi cậu là một cô gái dễ thương...

368
00:33:10,112-->00:33:20,113
Tớ cũng vậy. Được gặp cậu thật tuyệt, Rinne.

368
00:33:22,112-->00:33:28,513
Cảm ơn mọi người. Tớ vui lắm.

368
00:33:30,112-->00:33:37,113
Mọi người trả lời Rinne một cách
chân thực và mỉm cười. 
Chắc chắn không ai nói dối cả.

368
00:33:38,112-->00:33:43,113
Được rồi, mọi người sẵn sàng chưa?
Cụng ly!

368
00:33:44,112-->00:33:48,113
Cụng ly!

368
00:33:50,112-->00:33:56,513
Cứ như vậy, mọi sự ngại ngùng được xua tan
và bữa tiệc ấm cúng bắt đầu.

368
00:34:03,112-->00:34:08,513
Được, tớ sẽ lấy đống thịt này!

368
00:34:10,112-->00:34:21,513
Tohka-san, đừng lấy mỗi thịt không.
Sẽ ngon hơn nếu lấy thêm rau nữa đấy.

368
00:34:23,112-->00:34:31,513
Yatogami Tohka chẳng ăn gì ngoài thịt.
Sớm muộn gì cô ta cũng thành con heo.

368
00:34:33,112-->00:34:45,113
Con... con heo sao? 
Không, khoan đã Tobiichi Origami!
Đây là thịt bò. Vậy nên tôi sẽ không thành heo.

368
00:34:47,112-->00:34:51,113
Thật phí thời gian.

368
00:34:53,112-->00:35:00,113
Ara ara, tớ có thể thấy
mùi thù địch trong phòng đấy.

368
00:35:02,112-->00:35:10,113
Yoshino-chan, Kotori-chan. 
Chị lấy cho hai em này.

368
00:35:12,112-->00:35:16,513
Waa... Thịt!

368
00:35:18,112-->00:35:25,113
Cảm... cảm ơn chị.

368
00:35:27,112-->00:35:37,513
Đây là viên ngọc quý giấu trong nó.
Đây là đá quý dành cho ai đó tầm cỡ như tôi.

368
00:35:39,112-->00:35:47,113
Chỉ ra. Có phải đó là chơi chữ không...

368
00:35:48,112-->00:35:58,113
Chơi chữ à? À không!
Sao tôi phải chơi chữ để làm giảm
giá trị của mình chứ?

368
00:36:00,112-->00:36:09,513
Thật lòng. Tôi ngả mũ 
trước sự hài hước của cô đấy, Kaguya.

368
00:36:11,112-->00:36:18,513
Tôi đã nói không có chơi chữ gì cả!

368
00:36:20,512-->00:36:28,513
Tôi đã nghĩ có thể tệ hơn thế này,
nhưng mọi người đều cư xử đúng mực.

368
00:36:30,112-->00:36:35,113
Shidou, đồ ăn thế nào?

368
00:36:36,112-->00:36:41,513
Ngon lắm. Mà này Rinne, cậu làm mọi thứ
để bắt đầu bữa tiệc này à?

368
00:36:43,112-->00:36:53,513
Không sao đâu. Tớ thích thế.
Bên cạnh đó, tớ vui vì mọi người
thoải mái với nhau mà.

368
00:36:55,112-->00:37:00,113
Đúng vậy. Họ rất tốt với nhau.

368
00:37:01,512-->00:37:08,513
Nhưng tớ nghĩ mọi người làm thế vì tớ.

368
00:37:10,112-->00:37:13,513
Nghĩa là sao?

368
00:37:15,112-->00:37:26,113
Tohka-chan và Tobiichi-san 
luôn cãi nhau phải không?
Nhưng giờ họ không làm thế.

368
00:37:27,512-->00:37:32,513
Phải rồi. Giờ đến lượt cậu đấy, Rinne.

368
00:37:34,112-->00:37:38,513
Đến lượt tớ gì cơ?

368
00:37:40,112-->00:37:45,513
Đến lượt cậu tham gia với họ.
Không phải cậu muốn 
bên cạnh họ nhiều hơn sao?

368
00:37:47,112-->00:37:55,113
Được rồi. Tớ sẽ làm thế. 
Cậu giúp tớ nhé, Shidou?

368
00:37:56,512-->00:38:02,513
Để đấy cho tớ, tớ cũng sẽ giúp.

368
00:38:04,112-->00:38:09,513
Được thôi, tớ sẽ giúp.

368
00:38:11,112-->00:38:17,113
Cảm ơn các cậu. Đợi tớ nhé.

368
00:38:18,112-->00:38:22,113
Ừ, ra với họ đi.

368
00:38:25,112-->00:38:30,513
Cậu tốt thật đấy, Shidou.

368
00:38:32,112-->00:38:37,513
Cậu nói làm tớ ngại đấy.

368
00:38:39,112-->00:38:45,113
Tuy nhiên, cậu vẫn làm 
trái tim con gái thấy khó chịu đấy.

368
00:38:46,512-->00:38:50,113
Sao cậu nói thế?

368
00:38:52,112-->00:39:01,113
Không, không có gì đâu.
Giờ chúng ta giúp Rinne chứ?

368
00:39:03,112-->00:39:09,113
Đi theo Maria, tôi bắt đầu công việc.
Trong khi đó, Rinne không khó khăn gì
khi tham gia cùng Tohka và mọi người.

368
00:39:13,512-->00:39:20,513
Vậy à? Tohka-chan cũng muốn nấu ăn sao?

368
00:39:22,112-->00:39:32,513
Đúng vậy! Tớ cũng muốn nấu những 
món ăn ngon và giúp Shidou.

368
00:39:34,112-->00:39:41,113
Đúng vậy nhỉ. 
Nếu cậu thật sự muốn, tớ sẽ giúp.

368
00:39:43,112-->00:39:50,513
Vậy cậu chỉ tớ luôn được không, Sonogami Rinne?

368
00:39:52,112-->00:39:57,513
Không phiền đâu... Sao vậy?

368
00:39:59,112-->00:40:09,513
Cách tốt nhất để chiếm trái tim
của đàn ông là qua đường dạ dày.
Và cũng để khiến Yatogami Tohka
thấy khó chịu khi bị vượt mặt.

368
00:40:11,112-->00:40:20,113
Cái gì? Cô lúc nào cũng giành giật thế à?

368
00:40:21,512-->00:40:29,113
Đó chỉ là mục tiêu của tôi.
Tôi không cần cô tham gia.

368
00:40:30,512-->00:36:37,113
Hai cậu bình tĩnh lại nào.

368
00:40:39,112-->00:40:45,113
Rinne luôn làm tốt mọi thứ.
Nhìn cô ấy không thay đổi gì, 
tôi mỉm cười.

368
00:40:47,112-->00:40:52,113
Shidou, trông cậu vui quá nhỉ.

368
00:40:54,112-->00:40:59,113
Haha... Vậy à?

368
00:41:00,112-->00:41:08,113
Phải. Cậu trông rất hạnh phúc khi 
cậu nhìn con gái và cười với họ.

368
00:41:10,512-->00:41:14,113
Không phải thế!

368
00:41:15,112-->00:41:23,113
Tôi không biết ý cô ấy là gì.
Tôi chỉ nhìn và tiếp tục rửa chén.

368
00:41:30,112-->00:41:38,113
Mệt thật. Nhưng mọi người đều rất vui.
Rinne và Maria cũng có thời gian tuyệt vời.

368
00:41:39,112-->00:41:46,113
Dù vậy, tôi không thể để mọi thứ thế này.
Có cả núi việc tôi phải nghĩ.

368
00:41:47,512-->00:41:51,113
Mình nên đi dạo chút vậy.

368
00:41:57,112-->00:42:02,113
Mình vẫn nhớ đây là nơi
mình gặp cô ấy lần đầu tiên.

368
00:42:04,112-->00:42:11,113
Tôi đã quên cô ấy. Nhưng Maria đã trở lại. 
Hình ảnh cô ấy hiện lên trong tâm trí tôi.

368
00:42:12,512-->00:42:18,113
Cô ấy để Maria lại phía sau và biến mất.

368
00:42:23,112-->00:42:32,113
Khi tôi nghĩ về thế giới ảo,
tôi tìm thấy mình ở công viên trên núi.
Chẳng còn ai ở đây giờ này cả.

368
00:42:33,112-->00:42:39,513
Mình đã nghĩ sẽ có ai đó ở đây,
nhưng chẳng có ai cả.

368
00:42:41,112-->00:42:51,113
Trời cũng bắt đầu lạnh rồi.
Nếu tôi ở đây lâu hơn, 
Maria và Rinne sẽ lo lắng. Tôi nên đi về.

368
00:42:52,112-->00:42:58,113
Không phải vội, mình sẽ đi từ từ...

368
00:43:00,112-->00:43:05,113
Đột nhiên, tôi nghe thấy tiếng chân.

368
00:43:07,112-->00:43:15,113
Có thể lắm. Tôi nghĩ vậy.
Nếu là thế giới của Eden thì có thể.

368
00:43:27,512-->00:43:33,113
Tôi quay mặt về phía âm thanh,
nhân vật đang đứng đó là...

368
00:43:38,112-->00:43:44,113
Một cô bé không quen 
với khuôn mặt ngây thơ tinh khiết

368
00:43:45,112-->00:43:50,113
Một cô bé...? Ở ngoài đêm khuya thế này sao?

368
00:43:56,112-->00:44:03,113
Tôi không biết tại sao.
Nhưng một cảm giác lạ từ cô bé.
Cô bé rất giống một người...

368
00:44:04,112-->00:44:10,113
Là papa sao?

368
00:44:11,112-->00:44:14,513
Cái gì?

368
00:44:15,512-->00:44:22,113
Pa... papa? Cô bé này nói gì vậy?

368
00:44:23,112-->00:44:28,113
Có đúng là papa không?

368
00:44:29,112-->00:44:38,113
Khoan... khoan đã.
Anh tên là Itsuka Shidou.
Anh mới chỉ là học sinh trung hoc.
Anh không thể có con gái...

368
00:44:39,112-->00:44:43,513
Itsuka... Shidou?

368
00:44:45,112-->00:44:49,113
Phải! Anh không phải papa em!

368
00:44:53,112-->00:44:58,513
Là papa! Đúng là papa rồi!

368
00:45:00,113-->00:45:05,113
Cô bé chẳng hiểu gì cả...

368
00:45:06,112-->00:45:18,113
Không, Rio hiểu. Itsuka Shidou là papa.
Là papa của Rio.

368
00:45:19,512-->00:45:24,513
Anh là... papa của em? Cái quái gì thế?

368
00:45:26,112-->00:45:30,513
Papa là... papa, phải không?

368
00:45:32,112-->00:45:36,513
Không, anh không phải là...

368
00:45:38,113-->00:45:48,513
Rio phải đi rồi. 
Để con nói với papa một điều.

368
00:45:50,112-->00:45:53,513
Gì vậy?

368
00:45:55,112-->00:46:06,113
Papa phải tìm "điều quý giá nhất"
khiến mọi người hạnh phúc, được chứ?

368
00:46:08,112-->00:46:12,513
Hở? Hạnh phúc... điều quý giá nhất?

368
00:46:14,112-->00:46:20,513
Rio đi đây! Bye bye, papa!

368
00:46:22,112-->00:46:26,113
Khoan... em là ai?

368
00:46:28,112-->00:46:37,513
Rio... là Rio. Sonogami Rio!

368
00:46:39,112-->00:46:43,513
Rio... và Sonogami?

368
00:46:45,112-->00:46:49,113
Giống như Rinne sao?

368
00:46:50,113-->00:46:54,113
Hẹn gặp lại!

368
00:46:59,112-->00:47:05,113
Giống như khi xuất hiện,
cô bé tên Rio bất ngờ chạy đi mất

368
00:47:07,112-->00:47:12,513
Chờ... chờ đã! Rio! Rio!

368
00:47:14,112-->00:47:18,513
Buồn thay, không có tiếng trả lời.

368
00:47:20,112-->00:47:24,113
Cô bé đó là ai chứ?

368
00:47:25,512-->00:47:31,113
Cảm giác từ cô bé, chắc chắn là Tinh Linh.
Và cái tên đó...

368
00:47:32,512-->00:47:37,113
Rio, Sonogami...

368
00:47:38,112-->00:47:44,113
Đó là họ của Rinne. 
Cô bé có liên quan gì đến Rinne à?

368
00:47:45,112-->00:47:53,113
Giờ tôi mới thấy cô bé rất giống Rinne.
Và tại sao cô bé gọi tôi là papa?

368
00:47:55,112-->00:48:04,113
Sao mặt dài ra thế? 
Cậu phải vui lên chứ.

368
00:48:12,112-->00:48:19,113
Ánh mắt đó là gì vậy?
Cậu quên tôi rồi à?

368
00:48:20,512-->00:48:27,113
Ma... Marina? 
Cậu thật sự đang ở thế giới này!

368
00:48:28,112-->00:48:45,113
Phải. Một người đã biến mất 
như tôi đang đứng đây. Khó tin thật nhỉ.
Tiếc là cậu đã thoát khỏi thế giới đó...

368
00:48:46,512-->00:48:53,513
Đừng nói thế... Cậu có biết 
tớ mừng thế nào khi gặp lại cậu không?

368
00:48:55,112-->00:49:03,113
Này! Bỏ tôi ra! Đừng bất ngờ ôm tôi như thế!

368
00:49:04,512-->00:49:09,513
Xin lỗi. Tớ chỉ...

368
00:49:11,112-->00:49:21,513
Không phải "chỉ" không thôi đâu.
Và đừng nói cậu không cố ý!
Tôi không tin cậu, tên bám váy con gái.

368
00:49:23,112-->00:49:27,113
Câu đó nghe đau đấy...

368
00:49:28,512-->00:49:33,513
Giống như 3 đứa bà tám cùng lớp tôi.

368
00:49:35,112-->00:49:41,113
Sao? Cậu muốn gặp tôi à?

368
00:49:42,512-->00:49:46,513
Phải. Và tớ mừng vì được gặp cậu.

368
00:49:48,112-->00:49:52,113
Đừng nói dối tôi.

368
00:49:53,512-->00:49:57,113
Tại sao?

368
00:49:59,112-->00:50:08,513
Nhìn cậu đứng đây giống như bị đông cứng.
Cậu đứng trơ ra nếu tôi không gọi cậu.
Và giờ cậu tùy tiện nói những lời đó.

368
00:50:10,112-->00:50:15,513
Thật đấy... Tớ không biết giải thích thế nào...

368
00:50:17,112-->00:50:27,513
Khỏi giải thích.
Cuối cùng cậu cũng được gặp 
cô con gái bé nhỏ rồi. Phải không, papa? 

368
00:50:29,112-->00:50:33,513
Cậu thấy bọn tớ rồi à, Marina?

368
00:50:35,112-->00:50:44,113
Tôi thấy hết. Hơn nữa, 
tôi biết cô bé đó nhiều hơn cậu đấy.

368
00:50:45,512-->00:50:51,113
Sao cậu biết về cô bé đó?

368
00:50:52,112-->00:50:57,513
Tại sao nhỉ?

368
00:50:59,112-->00:51:06,113
Đúng là cậu... Bỏ đi, cậu nói cho tớ
biết về cô bé đi. Cô bé là ai? 

368
00:51:07,112-->00:51:19,113
Xem nào... Cô bé đang tìm kiếm 
thứ gì đó. Cô bé nói vậy phải không?

368
00:51:20,112-->00:51:26,513
Phải. Cô bé nói là "điều quý giá nhất".

368
00:51:28,112-->00:51:35,513
Đúng vậy. Đó là tất cả những gì
tôi có thể nói hiện giờ.

368
00:51:37,112-->00:51:42,113
Vậy cậu biết gì về thế giới này, Marina?

368
00:51:43,512-->00:51:57,513
Phải, tôi biết. Ít nhất là hơn cậu.
Dĩ nhiên, tôi không phải cô gái dễ dãi
trả lời hết câu hỏi cậu hỏi tôi.

368
00:51:59,112-->00:52:05,513
Cô ấy không thay đổi. 
Dù sao tôi cũng nhớ cách cô ấy nói chuyện.

368
00:52:07,112-->00:52:14,513
Vậy tớ không hỏi nữa.
Vậy cậu định làm gì tiếp đây?

368
00:52:16,112-->00:52:19,513
Tôi à?

368
00:52:21,112-->00:52:26,513
Phải. Tớ đoán cậu không muốn 
gặp lại Maria phải không?

368
00:52:28,112-->00:52:36,513
Phải rồi... Tôi định sẽ giúp cô bé đó.

368
00:52:38,112-->00:52:41,513
Cậu định giúp Rio sao?

368
00:52:43,112-->00:52:50,113
Phải. Tôi không thể để cô ấy một mình.
Cứ xem như tôi đang lo cho em gái đi.

368
00:52:50,512-->00:52:56,513
Đó là cô bé biết vâng lời,
không như cô em kế của tôi.

368
00:52:58,112-->00:53:04,113
Tớ hiểu... Vậy thì tốt.

368
00:53:04,512-->00:53:14,513
Tốt là sao? Tôi chỉ thấy chán
nên tôi theo con bé để giết thời gian thôi.

368
00:53:16,112-->00:53:20,113
Thôi nào, cậu...

368
00:53:21,512-->00:53:32,113
Giờ tôi phải đi rồi.
Tôi không để Rio đợi được 
và cậu phải về sớm phải không?

368
00:53:33,112-->00:53:38,513
Ừ. Tớ nói điều cuối cùng được không?

368
00:53:40,112-->00:53:46,113
Gì vậy? Đừng nói gì ngu ngốc đấy.

368
00:53:47,512-->00:53:53,513
Bộ đồ hợp với cậu lắm.
Tớ chưa bao giờ thấy cậu mặc đồ thường,
nhưng trông cậu dễ thương đấy.

368
00:53:55,112-->00:54:01,513
Nói câu ngu ngốc gì vậy?

368
00:54:03,112-->00:54:07,513
Này! Đừng có ném đá!

368
00:54:09,112-->00:54:15,513
Im đi! Cậu và những lời khen của cậu...

368
00:54:17,112-->00:54:21,113
Lời khen của tớ thì sao?

368
00:54:22,512-->00:54:29,513
Cậu nên nói từ đầu đi.
Cậu đang cố thành kẻ thù của phụ nữ à?

368
00:54:31,112-->00:54:36,113
Tớ không nhớ tớ muốn như thế!
Dù sao, xin lỗi...

368
00:54:37,112-->00:54:42,513
Tôi vừa nhận những gì 
giống hệt Maria nói hồi sáng.

368
00:54:44,112-->00:54:53,513
Tôi không muốn ở đây nữa.
Hẹn gặp lại, Itsuka Shidou.

368
00:54:55,112-->00:54:59,113
Ừ, hẹn gặp lại, Marina.

368
00:55:01,512-->00:55:12,113
Cô ấy đi mất, nhưng giờ tôi đã
có cơ hội gặp lại lần nữa.
Tôi nghĩ sẽ sớm thôi.

368
00:55:13,112-->00:55:19,513
Cuối cùng mình đã giải được
rất nhiều câu hỏi. Mọi thứ khá hơn lúc đầu.

368
00:55:21,112-->00:55:25,513
Cuối cùng, tôi biết một điều.

368
00:55:27,112-->00:55:31,113
Cậu đã thay đổi, Marina...

368
00:55:32,112-->00:55:41,113
Marina vẫn như thế, như chưa từng biến mất.
Tôi vẫn không quên chuyện 
cô ấy để tôi và Maria bên nhau.
Tôi vẫn biết ơn đến bây giờ.

368
00:55:50,112-->00:55:58,113
Shidou, mừng cậu về nhà.
Cậu đi trễ quá. Cậu đi đâu xa lắm à?

368
00:56:00,112-->00:56:06,113
Không gì thoát khỏi cậu nhỉ.
Phải, tớ đi dạo và đã đi khá xa.

368
00:56:07,512-->00:56:16,513
Tôi nghĩ tốt nhất là đừng nói
tôi gặp Rio và Marina.
Tôi vẫn không rõ sự tình, 
vậy nên nói với cô ấy chỉ thêm rắc rối.

368
00:56:18,512-->00:56:24,113
Cậu sao vậy? Có chuyện gì à?

368
00:56:26,112-->00:56:31,113
Không, không có gì đâu.
Tớ chỉ suy nghĩ chút thôi.

368
00:56:32,112-->00:56:37,113
Phải chăng là... về thế giới này...

368
00:56:39,112-->00:56:44,113
Không, không! Không có gì đâu! Thật đấy!

368
00:56:46,112-->00:56:57,113
Vậy à? Dù gì đi nữa, đừng suy nghĩ
để rồi lo lắng quá nhiều. Nếu cậu muốn,
tớ luôn ở đây để nói chuyện với cậu.

368
00:56:59,112-->00:57:04,113
Ừ, được rồi. 
Nhân tiện, Tohka và mọi người đi ngủ rồi à?

368
00:57:05,112-->00:57:15,513
Họ nói chuyện trễ lắm.
Nhưng họ đã về nhà rồi.
Kotori-chan cũng lên giường rồi.

368
00:57:17,112-->00:57:22,113
Được rồi. Cảm ơn cậu đã lo cho họ, Rinne.

368
00:57:24,112-->00:57:31,113
Không có gì đâu. Vậy tớ về nhà đây.

368
00:57:33,112-->00:57:37,513
Ừ. Chúc ngủ ngon, Rinne.

368
00:57:39,112-->00:57:45,113
Ừ. Chúc ngủ ngon, Shidou. 
Hẹn gặp cậu ngày mai.

368
00:57:47,112-->00:57:51,113
Ừ. Hẹn gặp cậu ngày mai.

368
00:57:58,112-->00:58:01,513
Thế giới này... Thiên đường Eden mới này
có quá nhiều thứ tôi không biết.

368
00:58:02,112-->00:58:09,513
Rinne không tạo ra thế giới này,
nhưng nó rất giống của cô ấy.
Mỗi ngày sống với Rinne, Maria và Marina,
rất thật nhưng không thể là thật.

368
00:58:10,112-->00:58:17,113
Rio... cô bé chắc chắn đang giữ 
sự thật của thế giới này.
Tôi không có bằng chứng, nhưng...

368
00:58:18,112-->00:58:23,113
"điều quý giá nhất" à?

368
00:58:24,112-->00:58:33,113
Đó có thể là gì? Ý cô ấy là
điều quý giá nhất của tôi hay của cô ấy?

368
00:58:34,112-->00:58:38,113
Tuy nhiên, nếu là của mình...

368
00:58:40,112-->00:58:46,113
Thứ duy nhất hiện ra trong đầu tôi 
là thứ trong túi nhỏ. Và ý nghĩa trong nó...

368
00:58:47,112-->00:58:51,113
Không thể được...

368
00:58:52,112-->00:58:59,113
Tôi đang đến gần một thứ rất lớn.
Tôi nên ngủ và ngưng nghĩ về nó 
sau khi tôi cảm thấy thoải mái.

368
00:59:01,112-->00:59:05,113
Với suy nghĩ đó, tôi nhắm mắt lại.

368
00:59:10,112-->00:59:15,113
Ngày thứ 2



