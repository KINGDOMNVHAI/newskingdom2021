﻿0
00:00:14,512-->00:00:17,113
Đi đến Hội trường?
1) Đồng ý
2) Không đồng ý

1
00:00:20,512-->00:00:22,113
Phù. Cuối cùng cũng xong.

2
00:00:22,512-->00:00:25,513
Phù. Tôi không biết rằng 
hôm nay phải trực nhật.
Không còn ai trong lớp cả.

3
00:00:26,112-->00:00:28,113
Giờ phải chuẩn bị bữa ăn tối.
Mình sẽ đi qua siêu thị.

4
00:00:30,512-->00:00:33,513
Hôm nay tôi phải mua những thứ đặc biệt
để ăn mừng Rinne trở về.
Tôi nên đi nhanh rồi về giúp Rinne.

5
00:00:33,912-->00:00:37,113
Không, khoan đã. Maria và những người khác
cũng về rồi. Rinne sẽ không làm một mình đâu.

6
00:00:39,512-->00:00:44,513
Ara Shidou-san, cậu còn ở đây à?

7
00:00:45,512-->00:00:48,113
Kurumi? Sao cậu lại ở đây?

8
00:00:49,112-->00:01:03,113
Shidou-san thật là... sao cậu ngạc nhiên thế?
Chẳng lẽ tớ không được 
ở cùng lớp với cậu à?

9
00:01:03,512-->00:01:06,113
Đúng vậy. Xin lỗi...

14
00:01:07,112-->00:01:14,113
Không, không. Tớ muốn hỏi
từ giờ đến tối cậu rảnh không?

15
00:01:15,112-->00:01:17,513
Um... Tớ không bận gì cả.

16
00:01:18,112-->00:01:27,113
Nếu vậy thì tuyệt quá!
Shidou-san, cậu có thể đi mua sắm với tớ không?

17
00:01:27,512-->00:01:29,113
Chỉ hai chúng ta sao?

18
00:01:30,112-->00:01:34,113
Phải, chỉ hai chúng ta thôi.

19
00:01:35,112-->00:01:36,513
Chuyện này...

20
00:01:37,112-->00:01:40,913
Nếu là bình thường, tôi sẽ cần Kotori giúp.
Nhưng Kurumi có lẽ không còn là
Kurumi như mọi ngày nữa.

21
00:01:41,112-->00:01:43,113
Được rồi, tớ sẽ đi.
Vậy cậu định đi đâu?

22
00:01:43,312-->00:01:51,113
Chúng ta sẽ đi tìm chỗ-nào-riêng-tư nhé.

23
00:01:52,112-->00:01:55,113
Tớ có cảm giác bất an...

24
00:01:56,112-->00:01:58,113
Khoan đã.

25
00:01:58,512-->00:02:01,113
Origami?

25
00:02:02,112-->00:02:08,113
Ara... Origami-san vẫn còn ở đây à?

25
00:02:09,112-->00:02:14,113
Đừng giả vờ. Cô biết tôi ở đây.

25
00:02:15,112-->00:02:23,113
Không phải vậy đâu. Tôi chỉ là 
một cô gái dễ thương yếu đuối thôi mà.

25
00:02:28,112-->00:02:34,113
Vậy Shidou, Tokisaki Kurumi
và cậu không được đi với nhau.

25
00:02:34,512-->00:02:36,513
Tại sao vậy?

25
00:02:37,112-->00:02:48,113
Tokisaki Kurumi có thể sẽ cố gắng 
gần gũi quá mức với cậu.
Cậu cần một bạn gái giúp cậu vượt qua cám dỗ.

25
00:02:48,512-->00:02:51,513
Không, chuyện này...

25
00:02:52,112-->00:02:57,113
Cô ta đã làm chuyện đó rất nhiều với cậu rồi.

25
00:02:58,112-->00:03:05,113
Ara ara, cô thông minh đấy.

25
00:03:06,112-->00:03:11,113
Tớ nói thật đấy. Shidou, đừng đi một mình

26
00:03:12,112-->00:03:26,113
Thì ra là thế... 
Vậy là cô muốn đi với cậu ấy sao?
Ở với Origami-san, hai người cũng 
không ít lần gần gũi quá mức đâu.

26
00:03:27,112-->00:03:35,513
Nếu vậy, cô có thể đi theo kiểm tra.
Nhưng tôi đoán cô đang muốn 
đẩy tôi ra để hai người đi chung với nhau.

26
00:03:36,512-->00:03:41,113
Vậy Shidou-san, cậu quyết định đi.

26
00:03:42,112-->00:03:44,513
Vấn đề không phải là chọn ai.
Origami và Kurumi đều đáng sợ như nhau...

26
00:03:45,113-->00:03:48,113
Um... Vậy Origami và Kurumi,
cả 3 chúng ta cùng đi nhé?

26
00:03:48,512-->00:03:53,113
Được rồi. Tớ sẽ bảo vệ cậu.

26
00:03:54,112-->00:04:00,113
Tớ nghĩ chuyến đi này sẽ vui lắm đấy.

26
00:04:01,112-->00:04:04,113
Tôi... tôi có quyết định sai không?

26
00:04:07,512-->00:04:12,113
Kurumi dẫn chúng tôi đến 
một cửa hàng tạp hóa nhỏ. 

26
00:04:13,112-->00:04:21,113
Shidou-san, nhìn này.
Cái này đẹp quá phải không?

26
00:04:21,512-->00:04:23,113
Xem nào... Một cái vòng tay à?

26
00:04:24,112-->00:04:32,113
Phải. Nó đeo vừa tay lắm.

27
00:04:33,112-->00:04:36,113
Trông nó giống một bông hoa màu đó vậy.
Cậu đeo trông hợp đấy.

28
00:04:36,512-->00:04:43,113
Cảm ơn cậu nhiều lắm, Shidou-san.
Vậy còn mặt dây chuyền này?

38
00:04:43,512-->00:04:46,513
Hình cánh đen à? 
Hai cái đi kèm cũng hợp lý đấy.
Nếu cậu hỏi cái nào hợp hơn 
thì tớ thấy vòng tay hợp hơn.

39
00:04:47,112-->00:04:53,113
Vậy tớ sẽ chọn cái vòng.

40
00:04:54,113-->00:04:56,513
Ừ, được đấy. Trông nó rất thanh lịch

41
00:04:57,113-->00:05:04,113
Ara ara, Vậy thì tớ không cần
phải tỏ ra thanh lịch nữa.

42
00:05:04,513-->00:05:07,113
Đừng... đừng nói thế!

43
00:05:07,512-->00:05:17,513
Đùa thôi. Phản ứng của cậu
trông buồn cười lắm đấy.

39
00:05:18,112-->00:05:19,513
Không giống đùa chút nào...

40
00:05:20,113-->00:05:27,113
Bỏ qua đi. Giờ tớ sang đây xem nhé.

41
00:05:31,113-->00:05:33,113
Origami, cậu lấy những gì rồi?

42
00:05:34,113-->00:05:40,113
Đây là những gì cần thiết cho 
cuộc sống của chúng ta trong tương lai.

47
00:05:41,112-->00:05:43,113
Đó là... tấm nệm à? (tiếng Nhật)

48
00:05:43,512-->00:06:01,113
Đúng vậy. Trên tấm nệm ma thuật này,
những vết bẩn sẽ được xóa sạch.
Nếu chúng ta làm chuyện đó cùng nhau,
mọi dấu vết sẽ biến mất.

41
00:06:01,512-->00:06:04,113
Cậu nghĩ gì thế hả?

42
00:06:04,512-->00:06:12,113
Còn bây giờ, hãy thứ nói chuyện
thân mật xem nào... Shidou, anh yêu.

43
00:06:13,112-->00:06:15,113
Không không, đừng gọi kiểu đó!

44
00:06:16,112-->00:06:24,113
Không sao, tớ không cần gọi vậy.
Chỉ cần lên giường thôi.

45
00:06:24,512-->00:06:27,513
Đừng lấy giường tớ làm ví dụ!

46
00:06:28,112-->00:06:31,113
Shidou dễ xấu hổ nhỉ.

49
00:06:31,512-->00:06:33,113
Cậu đang cố cảm nhận từ vị trí của tớ à?

51
00:06:34,112-->00:06:39,113
Thật đáng tiếc. Tớ rất muốn thế.

53
00:06:40,112-->00:06:42,113
Thật là...



54
00:06:47,112-->00:06:52,113
Shidou-san, buổi mua sắm khởi động này vui thật.

55
00:06:53,112-->00:06:57,113
Tớ vui vì được cùng cậu đi mua sắm.

56
00:06:58,112-->00:07:02,113
Ừ, vậy thì tốt rồi.
Vậy tiếp theo chúng ta đi đâu?
Chưa ai có ý tưởng gì sao?

61
00:07:02,312-->00:07:10,513
Đúng vậy. Origami-san, 
cô muốn tham gia một trò chơi không? 

62
00:07:11,112-->00:07:20,113
Trò chơi? Nghe hay đấy.
Là gì tôi cũng chơi.

63
00:07:21,112-->00:07:29,113
Ara, tự tin thật đấy. 
Sẽ có chuyện thú vị đấy.

64
00:07:30,112-->00:07:33,513
Sợ hãi sẽ giết chết cuộc sống của tôi.

65
00:07:34,112-->00:07:37,113
Cuộc nói chuyện kỳ lạ thật...
Cả hai không thấy thế sao?

66
00:07:41,512-->00:07:43,113
Đây là nơi diễn ra trò chơi à?

67
00:07:44,112-->00:07:48,113
Nếu là ở đây, tớ biết mình phải làm gì.

68
00:07:49,112-->00:07:59,113
Cửa hàng này sẽ trở thành buổi
trình diễn thời trang đấy.

81
00:08:00,112-->00:08:03,113
Shidou là đồ tồi.

82
00:08:03,512-->00:08:06,513
Được rồi. 
Giờ mấy cậu thay đồ hay làm gì đó đi.

83
00:08:09,112-->00:08:12,113
Sau đó hai người trao đổi với nhau bằng ánh mắt, 
và từng người đi vào phòng thay đồ.

84
00:08:12,512-->00:08:17,113
Chẳng biết màn trình diễn này có gì.
Nhưng chắc nó cũng có tác dụng giải trí.

85
00:08:22,112-->00:08:27,113
Shidou-san, tớ thay đồ xong rồi.

86
00:08:28,112-->00:08:31,113
Tớ cũng sẵn sàng rồi.

87
00:08:31,512-->00:08:33,513
Vậy sao mấy cậu không ra đi?

88
00:08:34,112-->00:08:40,113
Origami-san, chúng ta cùng ra nhé?

89
00:08:41,112-->00:08:44,513
Chuẩn bị nào... Hai, ba.

90
00:08:48,112-->00:08:50,513
Đó là... đồ lót à?

91
00:08:51,112-->00:08:53,113
Bất ngờ hai cô gái xuất hiện.
Sự xuất hiện của họ đẹp mê hồn,
từ từ bước vào tầm mắt tôi.

92
00:08:53,512-->00:08:58,113
Ara ara, cậu đang nhìn gì thế?

95
00:08:59,112-->00:09:02,113
À không, tớ không... vô tình thôi.

97
00:09:12,512-->00:09:18,513
Tớ thấy bộ đồ lót này đặc biệt đấy.

98
00:09:19,112-->00:09:21,513
Khoan, đợi đã! Sao lại là đồ lót chứ?

99
00:09:22,112-->00:09:31,113
Shidou-san, cậu thấy sao?
Đồ lót là một phần của buổi trình diễn đấy.

100
00:09:31,512-->00:09:33,513
Thế này xấu hổ lắm...

101
00:09:34,112-->00:09:36,113
Tôi hoàn toàn không nghĩ sẽ là đồ lót...

102
00:09:36,512-->00:09:45,113
Đánh giá công bằng nhất là
cả hai cùng mặc một kiểu đồ lót.
Shidou, cậu thấy sao?

103
00:09:46,512-->00:09:53,113
Đứng trước hai cô gái quyến rũ thế này,
cậu không hứng thú gì sao?

104
00:09:53,512-->00:09:57,512
Cậu đang nghĩ gì biến thái cũng được.

105
00:09:58,112-->00:10:00,112
Không, nghĩ gì không quan trọng.
Vấn đề là giờ tôi phải nói gì.

106
00:10:01,112-->00:09:17,113
Shidou-san, lần này cậu không quá 
nhút nhát nữa nhỉ? Khi tớ vừa chuyển đến,
cậu đã rủ tớ đến đây mà.

107
00:10:17,512-->00:10:20,113
Không, lần này khác lần đó.
Cả hai cậu đều đứng trước tớ...
Hai cậu trông đẹp lắm.

108
00:10:20,512-->00:10:25,513
Ara...

109
00:10:26,512-->00:10:29,113
Shidou...

110
00:10:29,112-->00:10:31,513
Có chuyện gì vậy?

111
00:10:32,112-->00:10:42,113
Không, vì cậu nói trò chơi của bọn tớ
nhàm chán nên bọn tớ đang làm cậu vui thôi.

108
00:10:43,112-->00:10:51,113
Từ tận đáy lòng, chác chắn cậu muốn
được bọn tớ làm thế này. Hãy tận hưởng đi.

109
00:10:51,512-->00:10:55,113
Tớ phải nói bao nhiêu lần nữa? 
Cả hai nhìn đẹp lắm. Rất hấp dẫn.

110
00:10:55,512-->00:11:13,113
Shidou-san đang cố gắng đấy nhỉ. 
Tớ thấy cậu khen một cách miễn cưỡng đấy.
Tuy nhiên, cậu có thể trả lời
bằng cơ thể cậu.

111
00:11:13,512-->00:11:21,113
Cơ thể cậu đang rất hứng thú với tớ.
Vì thế nó không nói dối nếu tớ kiểm tra.

112
00:11:22,112-->00:11:31,113
Cô đang áp đặt quá đấy.
Tôi không được kiểm tra sao?

108
00:11:32,112-->00:11:36,113
Không phải thế, tôi...!

109
00:11:37,112-->00:11:45,113
Hay là Shidou-san đang muốn sở hữu 
cơ thể của bọn tớ? Phải không Shidou-san?

110
00:11:45,512-->00:11:48,513
Này mấy cậu... Mấy cậu nói thế...
Mọi ánh mắt đang nhìn chúng ta đấy.

111
00:11:48,812-->00:11:51,113
Một số cô gái xung quanh đang nhìn 
và bàn tán về chúng tôi...

368
00:11:51,512-->00:11:55,113
Được rồi. Tớ sẽ đợi.

368
00:11:56,112-->00:11:58,113
Tớ cũng vậy

368
00:12:04,112-->00:12:06,513
Sau khi thi đồ lót,
cuối cùng cả hai đã chịu ra về.

368
00:12:06,813-->00:12:09,513
Vậy giờ chúng ta về thôi.
Tớ phải chuẩn bị bữa tối nữa.

368
00:12:10,112-->00:12:19,113
Được rồi. Hôm nay vậy là đủ.
Tokisaki Kurumi, lần sau gặp lại.

368
00:12:19,512-->00:12:25,513
Đúng vậy. 
Nếu có cơ hội, chúng ta sẽ đấu tiếp.

368
00:12:27,112-->00:12:29,513
Làm lại nữa à...

368
00:12:30,112-->00:12:35,513
Lần sau tôi sẽ thắng. Cứ chuẩn bị đi.

368
00:12:36,512-->00:12:44,513
Tuyệt đối không có chuyện đó đâu.
Cô đã chuẩn bị gì rồi à?

368
00:12:45,512-->00:12:48,513
Tôi không cần

368
00:12:49,112-->00:12:51,513
Mấy cậu cứ thích đấu đá thế à...
Thôi tớ về đây.

368
00:12:52,112-->00:12:55,113
Vậy hẹn gặp lại, Shidou.

368
00:12:56,112-->00:13:03,113
Vâng. Shidou-san. Lần sau gặp lại,
hy vọng được cậu giúp đỡ nhé.

368
00:13:04,112-->00:13:06,113
Ừ, hẹn gặp lai.

368
00:13:07,112-->00:13:10,113
Hai người họ đã đi.
Nhưng tôi có một cảm giác kỳ lạ.

368
00:13:10,312-->00:13:12,513
Gặp lại sao... ý họ là gì?

368
00:13:12,812-->00:13:15,113
Hỏi vậy cũng bằng thừa.
Về nhà thôi. Origami và Kurumi về rồi.  




