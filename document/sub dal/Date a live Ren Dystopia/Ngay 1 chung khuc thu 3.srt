﻿116
00:00:04,912 --> 00:00:09,113
Shidou, chào buổi sáng.
Tớ đang đợi cậu đến đây.

116
00:00:09,912 --> 00:00:12,313
Chào buổi sáng, Origami.
Cậu đợi tớ có việc gì à?

116
00:00:13,512 --> 00:00:19,113
Tớ có một yêu cầu.
Tớ muốn cậu gọi tên tớ lần nữa.

116
00:00:19,912 --> 00:00:21,313
Hở? Cũng được. Eto... Origami

116
00:00:22,512 --> 00:00:25,113
Làm ơn, thêm lần nữa đi.

116
00:00:26,112 --> 00:00:27,513
Ừ, Origami.

116
00:00:28,112 --> 00:00:31,113
Tiếp theo, tớ muốn cậu có thêm cảm xúc.

116
00:00:32,512 --> 00:00:35,113
Hở... Origami, cậu làm gì vậy?

116
00:00:37,112 --> 00:00:43,113
Này Origami! Mới sáng ra cô đã nói gì vậy?
Shidou thấy khó chịu rồi đấy.

116
00:00:45,512 --> 00:00:51,513
Đây là một việc rất quan trọng.
Nếu cô không hiểu thì đừng xen vào.

116
00:00:52,512 --> 00:00:54,513
Gì... gì cơ?

116
00:00:56,112 --> 00:01:03,113
Không phải. Tôi đã suy nghĩ một lúc,
nhưng chẳng hiểu gì cả. Anh có hiểu không, Shidou?

116
00:01:03,912 --> 00:01:05,513
Anh cũng không hiểu.
Này Origami, đến lúc cậu trả lời rồi đấy.
Cậu đang làm gì với cơ thể tớ vậy?

116
00:01:06,112 --> 00:01:15,113
Cho đến bây giờ, tớ đã nhớ hết mùi và vị của Shidou.
Tuy nhiên, tớ nhận ra như vậy là chưa đủ.

116
00:01:16,112 --> 00:01:29,113
Tớ cũng yêu giọng nói và âm thanh của cậu.
Cho đến giờ, có thể tớ đã bỏ lỡ âm thanh nào đó
do cậu tạo ra vì tớ chưa thử làm vậy. Tớ xin lỗi.

116
00:01:29,912 --> 00:01:31,113
Th... thật sao?

116
00:01:32,112 --> 00:01:34,513
Shidou, anh sẽ tạo ra những âm thanh đó
ngay bây giờ sao?

116
00:01:35,112 --> 00:01:37,113
Anh không chắc lắm.

116
00:01:38,112 --> 00:01:41,113
Ồ! Vậy hóa ra là thật sao?

116
00:01:41,912 --> 00:01:44,113
Chà, tôi không hiểu lắm.
Tôi nghĩ mình bị thuyết phục "vì đó là Origami".
Nó không đi kèm với máy nghe lén nào sao?

116
00:01:44,512 --> 00:01:45,713
Um... Vậy chúng ta về chỗ nhé?

116
00:01:46,112 --> 00:01:49,113
Được rồi. Cảm ơn cậu.

116
00:01:49,712 --> 00:01:51,513
Tôi không hiểu ý của câu nói này.
Cô gái có khuôn mặt như búp bê này là Origami.

116
00:01:52,112 --> 00:01:53,713
Origami là một con người bình thường
cho đến gần đây, trong một lần tình cờ, 
cô ấy đã trở thành Tinh Linh.

116
00:01:53,912 --> 00:01:55,113
Một thiên thần với sức mạnh hủy diệt áp đảo
tên là Metatron. Giờ cô ấy đã theo Ratatoskr.

116
00:01:55,512 --> 00:01:57,513
Tôi vừa nghĩ, sẽ có chuyện gì thay đổi?
Như thường lệ, tôi phải cẩn thận.

116
00:01:57,912 --> 00:02:00,113
Về sự thay đổi, Tohka và Origami thường gọi nhau
bằng tên đầy đủ. Sau sự việc, họ đã gọi nhau
chỉ bằng tên. Đây là một thay đổi tốt.




116
00:02:12,112 --> 00:02:20,113
Nào. Các em về chỗ đi.
Chúng ta bắt đầu tiết sinh hoạt.

116
00:02:21,512 --> 00:02:31,113
Đầu tiên là về thời tiết. Trời lạnh hơn rồi.
Hãy cẩn thận, coi chừng cảm lạnh đấy nhé.

116
00:02:32,112 --> 00:02:46,113
Còn 31 ngày nữa là đến học kỳ 2 rồi.
Hãy cố gắng ôn bài đi. Nếu bị điểm kém,
các em phải học phụ đạo thêm đúng không?

116
00:02:47,112 --> 00:02:55,113
Trong trường hợp đó, các em sẽ được
yêu cầu học trước kỳ lễ Giáng Sinh đấy.

116
00:02:56,112 --> 00:03:04,113
Cô cũng sẽ đồng hành với các em.
Vậy nên đừng để bị một dấu chấm đỏ nhé.

116
00:03:05,112 --> 00:03:10,113
Cô không thích dạy phụ đạo
trong Giáng Sinh năm 29 tuổi đâu.

116
00:03:11,112 --> 00:03:14,113
Tôi như nghe thấy được tấm lòng của người thầy.
Tôi phải cố gắng hết sức vì bản thân
và vì cô giáo của tôi.

116
00:03:15,112 --> 00:03:29,113
Cuối cùng, mùa này sẽ có nhiều sự kiện đấy.
Hãy nhớ, các em đều là học sinh
nên hãy hành động có ý thức nhé.

116
00:03:47,512 --> 00:03:52,113
Shidou, đến giờ ăn trưa rồi!
Hôm nay anh có làm đồ ăn không?

116
00:03:52,912 --> 00:03:54,313
Ồ... xin lỗi, hôm nay anh không làm bento.
Em có thể đi mua không?

116
00:03:55,112 --> 00:04:01,113
Ồ, vậy sao? Tiếc thật, nhưng biết sao được.

116
00:04:02,112 --> 00:04:03,513
Xin lỗi nhé. Anh chỉ làm cho buổi sáng thôi.

116
00:04:04,512 --> 00:04:09,513
Không đâu, đừng lo!
Em nhờ anh cũng nhiều rồi, Shidou!

116
00:04:13,112 --> 00:04:28,113
Tôi thấy có người đang đói.
Hãy gọi tên tôi đi. Hãy ca ngợi tôi đi.

116
00:04:28,912 --> 00:04:30,313
Kaguya và Yuzuru đấy à?
Có chuyện gì vậy? 
Sao các cậu lại qua lớp bọn tớ?

116
00:04:31,112 --> 00:04:43,113
Giải thích. Trận đấu hôm nay quá nóng 
nên bọn tớ đã mua quá nhiều bánh mì.
Bọn tớ chia cho Shidou và những người khác.
Vì vậy, các cậu được cứu rồi đấy.

116
00:04:43,912 --> 00:04:45,313
Ra là vậy. Nếu có vấn đề gì, cứ việc chia sẻ với tớ.

116
00:04:46,112 --> 00:05:01,113
Đừng lo. Cậu luôn có hội mà.
Hơn nữa, trò chơi này vẫn chưa kết thúc.
Hãy để Shidou chọn cái kết của trò chơi này.

116
00:05:02,112 --> 00:05:16,113
Đề xuất. Bọn tớ đã cạnh tranh mua bánh mì 
trước các học sinh khác. 
Tất nhiêu cậu là người đánh giá kết quả.
Nếu không, đây không phải là một trò chơi.

116
00:05:17,112 --> 00:05:26,113
Tớ nghi ngờ mùi vị của trò chơi hôm nay đấy.
Shidou, cậu chọn bánh mì của Kaguya hay Yuzuru?

116
00:05:26,912 --> 00:05:28,313
Thật tốt khi được nhận bánh mì,
nhưng đây là một trách nhiệm cần phải nghiêm túc...

116
00:05:29,112 --> 00:05:33,313
Shidou... Em cũng đang đói...

116
00:05:33,912 --> 00:05:36,113
Vậy à? Vậy cho Tohka chọn trước đi.
Tớ không phiền đâu.

116
00:05:37,112 --> 00:05:46,113
Sự thú vị rõ như lòng bàn tay tôi.
Tôi đã có lời tiên tri về nước đi này của Shidou rồi.

116
00:05:47,112 --> 00:05:59,113
Dự đoán. Bọn tớ đã mua Kinako-pan cho Tohka rồi.
Cậu hãy ăn cái này đi. Nếu Shidou không chọn
thì không phải là cuộc thi nữa.

116
00:06:00,112 --> 00:06:06,113
Ồ! Kinako-pan! Cảm ơn nhé, Kaguya, Yuzuru!

116
00:06:09,912 --> 00:06:22,113
Hãy ra ngoài tận hưởng đi.
Sau những thử thách khắc nghiệt, 
tôi đã có được nó, hương vị của vua, 
loại cốt lết ngon nhất!

116
00:06:23,112 --> 00:06:33,113
Một lớp quần áo bao bọc miếng thịt ngon ngọt.
Ngoài ra còn có bắp cải, rau các loại.
Sandwich là vô địch!

116
00:06:34,112 --> 00:06:52,113
Chế nhạo. Cái gì mà sandwich vô địch chứ.
Bánh mì Yakisoba cổ điển, phổ biến trong trường, 
hương vị truyền thống, nhiều học sinh không thể cưỡng lại.

117
00:06:53,112 --> 00:07:09,113
Giải thích. Yakisoba ngọt và mặn rất ngon.
Bánh mì kẹp mì sợi giúp bổ sung vị ngọt và mặn.
Phần gừng đỏ bên trên tạo điểm nhấn 
bằng cả hương vị và màu sắc.

116
00:07:09,912 --> 00:07:12,313
2 người này rất thích các trận thi đấu.
Vì vậy họ sẽ không cho phép tôi trả lời mơ hồ
như cả hai đều ngon hay thỏa thuận.
Tôi phải chọn một trong hai.

116
00:07:13,112 --> 00:07:17,113
Um... Nghe mọi người nói chuyện, tớ lại thấy đói.

116
00:07:18,112 --> 00:07:28,113
Nữa à? Nhưng vẫn còn đây, bánh mì dưa, 
bánh mì cà ri... Tớ sẽ cho cậu 
tất cả mọi thứ. Cứ ăn bao nhiêu tùy thích!

116
00:07:29,512 --> 00:07:38,113
Gì cơ? Còn nhiều vậy sao? Cảm ơn nhiều!
Tớ rất cảm ơn vì nhiều đồ ăn thế này.

116
00:07:42,112 --> 00:07:44,313
Tohka thực sự muốn ăn hết chỗ bánh này.
Nếu tôi chần chừ, bánh mì của cả 2
cũng bị ăn sạch.

116
00:07:44,912 --> 00:07:46,313
Mình nên chọn cái nào?

116
00:07:46,512 --> 00:07:56,113
1) Bánh sandwich cốt lết của Kaguya.
2) Bánh mì Yakisoba của Yuzuru.

116
00:07:57,512 --> 00:07:59,113
Tớ chọn bánh sandwich cốt lết của Kaguya.

116
00:08:00,112 --> 00:08:02,313
Chọn đúng lắm, Shidou!

116
00:08:03,912 --> 00:08:16,113
Chỉ có sandwich kẹp cốt lết 
mới có thể thưởng thức nhiều hương vị được.
Bụng của Shidou đang khóc rồi.
Giờ cái bánh sandwich này là của cậu.

116
00:08:16,912 --> 00:08:19,313
Ồ, cảm ơn nhé Kaguya. Tớ thích mùi vị này.
Món này có thể thử nhiều vị khác nhau 
cùng với cốt lết.

116
00:08:20,112 --> 00:08:28,313
Thất vọng. Tớ đã nhầm rồi.
Hôm nay tớ chấp nhận thua.

116
00:08:29,112 --> 00:08:34,113
Kaguya, Yuzuru, cảm ơn 2 cậu.
Chúng rất ngon!

116
00:08:35,112 --> 00:08:45,113
Tuy nhiên, dù học ở trường rất nhiều,
nhưng tớ lại chưa từng xuống ăn trưa.
Tớ nghĩ cũng nên xuống thử...

116
00:08:46,112 --> 00:08:56,113
Không, tớ không thể ăn thế này mãi được...
Tớ chỉ nghĩ, hôm nay ăn nhẹ cũng không sao...

116
00:08:56,912 --> 00:08:59,313
Ồ! Ý kiến hay đấy!

116
00:09:00,512 --> 00:09:12,113
Kinh ngạc. Đúng là Tohka...
Tớ không nghĩ đây là bữa trưa đấy.
Đây là một món ăn nhẹ thì đúng hơn...

116
00:09:13,112 --> 00:09:19,113
Không sao đâu. Ăn đồ ngọt có thể
cải thiện việc học của cậu đấy.

116
00:09:20,112 --> 00:09:27,113
Chỉ điểm. Kaguya không học sau khi ăn nhẹ đâu.
Cô ấy sẽ chợp mắt đấy.

116
00:09:27,912 --> 00:09:30,313
Tóm lại, trường học không phải nơi để ăn
vì chúng ta đến để học...
Buổi chiều hãy học cho đúng cách đấy.

116
00:09:30,912 --> 00:09:39,113
Ừ, em hiểu rồi.
Chúng ta học lớp gì vào buổi chiều vậy?
Nếu là lớp nấu ăn thì tuyệt...

116
00:09:39,712 --> 00:09:41,113
Có thật là em hiểu rồi không? 

116
00:09:41,512 --> 00:09:43,313
Em hiểu rồi mà!



116
00:09:46,112 --> 00:09:48,113
1) Bánh sandwich cốt lết của Kaguya.
2) Bánh mì Yakisoba của Yuzuru.

116
00:09:48,312 --> 00:09:50,513
Tớ chọn bánh mì Yakisoba của Yuzuru.

116
00:09:51,112 --> 00:10:06,113
Rõ ràng. Bánh mì Kisuba dành cho học sinh.
Tớ nghĩ Shidou thích hương vị quen thuộc hàng ngày.
Vậy thì, hãy ăn bánh mì Yakisoba này.

116
00:10:06,912 --> 00:10:09,313
Ồ, cảm ơn nhé Yuzuru. Tớ thích mùi vị này.
Ở trường hầu như ai cũng ăn Yakisoba cả.
Đây là mùi vị truyền thống đấy.

116
00:10:10,112 --> 00:10:19,313
Không thể nào...
Tớ cứ nghĩ lần này mình sẽ chắc thắng chứ.
Được thôi, lần sau tớ sẽ không thua đâu.

116
00:10:20,112 --> 00:10:24,113
Kaguya, Yuzuru, cảm ơn 2 cậu.
Chúng rất ngon!

116
00:10:28,912 --> 00:10:30,513
Cuối cùng giờ học cũng kết thúc.
Giờ thì, mình nên đi đâu đây?