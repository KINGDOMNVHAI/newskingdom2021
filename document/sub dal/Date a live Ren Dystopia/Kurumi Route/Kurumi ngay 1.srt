﻿0
00:00:00,512 --> 00:00:02,513
Đi đến Tháp Tengu?
1) Đồng ý
2) Không đồng ý

1
00:00:06,112 --> 00:00:08,313
Đã lâu rồi mình không đến nơi này...

2
00:00:08,712 --> 00:00:10,113
Tôi muốn đi dạo một chút quanh thành phố.

3
00:00:10,312 --> 00:00:12,513
Tôi biết Kotori đã dặn tôi.
Nhưng đi dạo một chút thì có gì đâu.
Tôi nghĩ đi xem xét xung quanh cũng tốt.

4
00:00:12,912 --> 00:00:15,513
Tất nhiên, tôi không có mục tiêu cụ thể nào.
Chỉ đơn giản là đi kiểm tra để yên tâm hơn.
Nếu không có gì xảy ra thì tốt.

5
00:00:15,912 --> 00:00:17,513
Chà, chẳng có vấn đề gì cả...

6
00:00:18,112 --> 00:00:21,513
Ara ara, tình cờ thật đấy, Shidou-san.

7
00:00:22,112 --> 00:00:23,113
Waa!

8
00:00:25,512 --> 00:00:31,113
Cậu không cần phải hoảng hốt như thế đâu.

9
00:00:32,112 --> 00:00:33,513
Ku... Kurumi? Sao cậu lại ở đây?

10
00:00:34,112 --> 00:00:49,113
Tớ không được đi dạo sao?
Hơn nữa, chúng ta cũng không phải người lạ.
Tớ chào cậu cũng là bình thường mà.

11
00:00:50,112 --> 00:00:51,513
Lỡ như cậu định...

12
00:00:52,112 --> 00:01:04,113
Vậy mà cậu lại ngạc nhiên và nghi ngờ tớ.
Tớ buồn lắm. Tớ sắp khóc đấy.

13
00:01:04,512 --> 00:01:07,113
Tớ xin lỗi...
Nhưng tớ muốn cậu nói chuyện với tớ bình thường,
không phải bất ngờ nói từ đằng sau đâu.

14
00:01:07,512 --> 00:01:10,513
Như thế thì chán lắm.

15
00:01:11,112 --> 00:01:12,513
Lý do hay quá nhỉ!

16
00:01:13,112 --> 00:01:24,113
Không phải vậy đâu. 
Tớ nghĩ chỉ có cách đó mới khiến cậu thốt ra
âm thanh dễ thương như vậy thôi.

17
00:01:26,912 --> 00:01:35,113
Nhưng cậu không vui sao?
Cậu cũng muốn bị trêu chọc 
từ phía sau, phải không?

18
00:01:36,112 --> 00:01:38,113
Bị trêu chọc gì chứ?
Cậu xem cơ thể của tớ là gì vậy?

19
00:01:40,912 --> 00:01:42,713
Chết tiệt, cô ấy toàn trêu chọc tôi.
Nếu cô ấy muốn thì tôi chiều.

20
00:01:43,112 --> 00:01:45,513
Tớ không ngạc nhiên lắm đâu.
Kurumi không phải người đầu tiên làm tớ giật mình.
Nếu cậu muốn gây bất ngờ, cậu phải làm cách khác.

21
00:01:46,112 --> 00:01:51,113
Ara... Vậy tớ phải làm gì đây?

22
00:01:51,912 --> 00:01:54,113
Đúng rồi. Chào buổi sáng, đến trường, đến lớp.
Nếu tình cờ gặp nhau sau giờ học thì đi chung...

23
00:01:54,712 --> 00:01:56,113
Giống như hiện giờ này.

24
00:01:56,512 --> 00:02:05,113
Ara ara... Sau đó cậu sẽ phong ấn 
sức mạnh Tinh Linh của tớ phải không?

25
00:02:05,912 --> 00:02:08,113
Cũng đúng. Nhưng cậu không có một chút
mong muốn được sống như người bình thường sao?

25
00:02:09,112 --> 00:02:17,513
Cậu đã nói nhiều về chuyện đó.
Tớ đến chỉ gây thêm rắc rối thôi.

25
00:02:18,112 --> 00:02:19,513
Tớ sẽ xem đó là một lời khen.

25
00:02:20,112 --> 00:02:32,113
Đó là một lời khen mà.
Cứ thoải mái chấp nhận đi.
Dù sao chúng ta đã có chuyến du hành thời gian cùng nhau mà.

25
00:02:32,712 --> 00:02:34,113
Kurumi đang nói về lần đó.

25
00:02:34,712 --> 00:02:36,113
Khi tôi xoay sở trong tuyệt vọng,
Kurumi đã đưa tôi du hành thời gian
nhờ thiên thần Zafkiel.

25
00:02:36,512 --> 00:02:38,113
Lúc đó nếu Không có Kurumi, 
chúng tôi không thể 
sống trong hòa bình như lúc này.

25
00:02:38,712 --> 00:02:40,113
Phải, tớ thật sự rất cảm ơn cậu vì chuyện đó.
Nhờ cậu mà mọi người có thể sống hạnh phúc.

25
00:02:40,912 --> 00:02:42,513
Miễn là cô ấy còn nhắm đến sức mạnh Tinh Linh
trong người tôi, tôi không thể không cảnh giác.

25
00:02:43,512 --> 00:02:47,113
Như vậy là tốt rồi.

25
00:02:48,112 --> 00:02:59,113
Nhưng đừng quên, tớ đã nói về việc
"thay đổi lịch sử" là điều tối kỵ.

25
00:03:00,112 --> 00:03:09,113
Đó là quy luật trái với tự nhiên.
Một dòng chảy ngược lại các quy tắc của thế giới.
Mọi thứ đều gắn kết với nhau, đúng không?

26
00:03:09,712 --> 00:03:12,113
Thế là sao? Thay đổi lịch sử có gì sai chứ?

26
00:03:12,912 --> 00:03:14,113
Vậy sao?

26
00:03:16,512 --> 00:03:26,113
Đó là cậu nghĩ thế thôi.
Những gì chúng ta làm đều có hậu quả cả.
Đừng quên đấy.

26
00:03:27,112 --> 00:03:29,113
Đó là một lời đe dọa sao?
Tôi tự hỏi liệu có điều gì sắp xảy ra không?

26
00:03:29,912 --> 00:03:32,113
Nhưng Kurumi nói đúng.
Mặc dù là bất khả kháng, nhưng thay đổi lịch sử
là việc không thể tha thứ.

28
00:03:32,312 --> 00:03:33,513
Đừng xem nhẹ hành động này.

26
00:03:33,912 --> 00:03:35,513
Mà dù có hiểu điều đó đi chăng nữa,
tôi cũng đã lựa chọn để cứu mọi người.

26
00:03:36,112 --> 00:03:41,113
Vậy là... cậu không có chuyện gì cả à?

26
00:03:42,112 --> 00:03:43,113
Có gì cơ?

26
00:03:44,112 --> 00:03:55,113
Không, tớ không biết liệu có liên quan gì không.
Nhưng những ngày này, tớ cảm giác 
có sức mạnh Tinh Linh ồn ào ở đâu đó.

26
00:03:55,712 --> 00:03:57,113
Ồn ào sao?

27
00:03:58,112 --> 00:04:07,113
Đó là do trực giác của con gái đấy.
Shidou-san có cảm thấy gì không?

38
00:04:07,712 --> 00:04:09,113
Tớ có phải con gái đâu.

39
00:04:09,712 --> 00:04:17,113
Đúng nhỉ. Tớ nhầm rồi.
Shiori-san có cảm thấy gì không?

40
00:04:17,713 --> 00:04:19,113
Nó không thay đổi về mặt sinh học đâu!

41
00:04:20,113 --> 00:04:31,113
Mà tớ thật sự không cảm thấy gì cả.
Vậy nên tớ chỉ muốn nói để cậu chú ý.

42
00:04:32,913 --> 00:04:35,113
Đây là gì vậy? Tôi ước mình 
có thể nói điều này ngay từ đầu.
Đây không phải việc dễ dàng...



43
00:04:35,512 --> 00:04:36,713
Kurumi...

39
00:04:37,112 --> 00:04:38,513
Gì vậy?

40
00:04:39,113 --> 00:04:40,313
Cậu làm tớ ngạc nhiên rồi đấy.

41
00:04:41,113 --> 00:04:43,113
Ara ara...

42
00:04:45,113 --> 00:04:58,113
Đúng rồi nhỉ...
Tớ không muốn ai cướp mất trái cây 
chưa đến mùa thu hoạch của mình đâu.
Trường hợp này là cậu đấy.

43
00:04:58,912 --> 00:05:01,113
Tôi không biết nên sợ hay nên xấu hổ nữa...

44
00:05:01,512 --> 00:05:12,113
Vậy nên cậu nhớ cẩn thận, đừng để bị tớ giết đấy.

46
00:05:15,712 --> 00:05:17,513
Mặc dù hơi bối rối, nhưng tôi chỉ nói những gì mình nghĩ.

47
00:05:17,912 --> 00:05:19,913
Có phải cậu tới vì cậu lo lắng cho tớ không?

48
00:05:20,112 --> 00:05:21,713
Dù sao đây cũng là một lời cảnh báo.
Hãy ghi nhớ chúng.

49
00:05:21,912 --> 00:05:23,513
Khi tôi nghĩ vậy, tôi đã có thể bước tiếp.

