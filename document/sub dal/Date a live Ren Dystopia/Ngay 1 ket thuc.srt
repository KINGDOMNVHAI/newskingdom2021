116
00:00:03,312 --> 00:00:05,113
Haa... Hôm nay nhiều chuyện quá.

116
00:00:05,512 --> 00:00:11,113
Anh vất vả rồi.
Có vẻ không có gì xảy ra cả.
Em yên tâm rồi.

116
00:00:11,912 --> 00:00:14,113
Không, Kotori, em nghe rồi phải không?
Anh đã nói có rất nhiều chuyện...

116
00:00:14,912 --> 00:00:20,113
Phải. Như mọi khi, anh không thích làm
những điều tốt với mọi người sao?

116
00:00:20,912 --> 00:00:24,513
Làm điều tốt gì chứ... 
Tất cả đều rối như mọi ngày.
Em có thể hiểu mọi việc
trước trước khi hỏi tại sao mà.

116
00:00:24,912 --> 00:00:35,113
Anh rất dễ bị cuốn vào rắc rối.
Thậm chí anh còn tự cuốn vào.
Anh không có việc gì cần làm ở nhà sao?

116
00:00:36,112 --> 00:00:37,513
Anh không được tin tưởng chút nào sao?

116
00:00:38,112 --> 00:00:42,113
Ara, giờ anh chịu đối diện với em rồi.
Theo một góc nào đó, em tin anh.

116
00:00:43,112 --> 00:00:45,113
Theo một góc nào đó...
Vậy là em hài lòng rồi à?

116
00:00:45,912 --> 00:00:50,113
Tất nhiên rồi. Em đã hy vọng
không có gì xảy ra như thế này.

116
00:00:51,312 --> 00:01:01,113
Em sắp ngủ rồi. Anh cũng nên ngủ sớm đi.
Nếu không, anh sẽ không thực hiện ước mơ
đánh thức em vào buổi sáng, đúng không?

116
00:01:02,112 --> 00:01:03,513
Ừ... anh biết rồi. Chúc ngủ ngon, Kotori.

116
00:01:04,112 --> 00:01:06,113
Chúc ngủ ngon, Shidou.






116
00:01:13,112 --> 00:01:14,313
Tôi nhìn khung cảnh xung quanh...

116
00:01:14,712 --> 00:01:16,113
Mọi thứ xung quanh trở nên méo mó...

116
00:01:16,712 --> 00:01:18,113
Tôi không biết mình đang ở đâu.
Tôi không biết tại sao điều này lại xảy ra.

116
00:01:18,912 --> 00:01:20,313
Không. Tôi không biết gì về thế giới này cả...

116
00:01:26,312 --> 00:01:28,513
Một cô gái xuất hiện trong bóng tối.
Một cô gái trông giống như một tù nhân, 
mặc đồ bịt mắt và bị xích...

116
00:01:31,912 --> 00:01:33,113
Cậu đã làm việc đó rồi sao?

解けたか

116
00:01:34,112 --> 00:01:36,113
Tôi cảm thấy như đang nghe một giọng nói yếu ớt.
Giọng nói như người sắp chết.

116
00:01:46,912 --> 00:01:48,513
Đột nhiên, cô gái cười lớn.

116
00:01:49,112 --> 00:02:03,113
Lạ thật đấy. Thật buồn cười
khi nhận thức được về bản thân mình.
Thế giới thật sự có những điều
không thể thực hiện được.

116
00:02:03,912 --> 00:02:16,513
Cơ thể này vẫn còn bị kiềm chế,
Nhận thức, nói chuyện, rên rỉ. 
Tôi đã cảm thấy tốt hơn. Đủ để "bắt đầu" rồi.

116
00:02:17,112 --> 00:02:19,113
Cô gái này... tôi không cảm thấy 
sợ hãi hay đau đớn gì cả.
Cô ấy có vẻ kỳ lạ hơn vẻ ngoài của mình.

116
00:02:20,112 --> 00:02:29,113
Đã vậy thì, hãy vén bức màn ra.
Ngừng khóc và bắt đầu tấn bi kịch.
Ngừng cười và bắt đầu tấn bi kịch.

116
00:02:30,112 --> 00:02:37,113
Từ giờ cậu có thể thấy 
một lễ hội dành cho các cô gái xinh đẹp.

116
00:02:38,112 --> 00:02:46,113
Những điều ước luôn tươi đẹp.
Không ai biết hậu quả của nó sẽ thế nào.

116
00:02:46,912 --> 00:02:53,113
Lan tỏa hy vọng cho các cô gái.
Đó là thiên đường hay nhà tù
trong suy nghĩ của các cô gái?

116
00:02:54,112 --> 00:03:05,113
Tôi là Ren, sẽ hỗ trợ các cậu như một đối tác.
Tôi hy vọng tất cả sẽ có
khoảng thời gian tận hưởng vui vẻ.




