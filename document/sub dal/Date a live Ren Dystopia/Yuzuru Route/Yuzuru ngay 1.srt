﻿25
00:03:38,112 --> 00:03:39,513
Đi đến Công Viên?
1) Đồng ý
2) Không đồng ý

25
00:03:43,112 --> 00:03:45,313
Sau khi kết thúc giờ học, tôi đã đến công viên.

26
00:03:45,712 --> 00:03:47,113
Tôi đến đây chẳng vì mục đích gì cả.
Ở đây yên tĩnh, rất tốt để đi dạo.

26
00:03:51,112 --> 00:03:59,113
Kỳ lạ. Có phải Shidou không?
Cậu đang làm gì ở đây?

38
00:04:00,112 --> 00:04:02,113
Ồ, Yuzuru. Tớ chỉ đi lang thang dạo chơi thôi.

39
00:04:02,512 --> 00:04:04,113
Tớ đi dạo một chút. Còn cậu đang làm gì ở đây?

40
00:04:04,513 --> 00:04:11,113
Trả lời. Yuzuru cũng vậy.
Tớ cũng có chút thời gian rảnh rỗi.

41
00:04:12,113 --> 00:04:14,113
Thế à? Kaguya không đi cùng sao?

42
00:04:14,713 --> 00:04:30,113
Giải trình. Hôm nay cô ấy giúp đỡ CLB.
Kaguya trông vậy mà đáng kinh ngạc thật.
Đôi khi cô ấy là linh vật đấy.

43
00:04:30,712 --> 00:04:32,713
Ra là vậy. Kaguya có kỹ năng vận động tốt.
Cô ấy được coi là một linh vật sao?

39
00:04:33,112 --> 00:04:35,113
Tất nhiên đó là một trò đùa.
Tôi không thể tưởng tượng được...

40
00:04:35,713 --> 00:04:42,113
Câu hỏi. Vậy Shidou, cậu định làm gì?

41
00:04:42,713 --> 00:04:44,113
Hở? Làm gì cơ?

42
00:04:44,913 --> 00:05:01,113
Nản lòng. Nam nữ gặp nhau trong công viên sau giờ học.
Chẳng lẽ cậu chỉ định chào hỏi rồi tạm biệt sao?
Cậu muốn con gái nói thẳng ra à?

43
00:05:03,112 --> 00:05:05,113
Haha, tớ hiểu rồi.
Vậy cậu muốn đi dạo với tớ không?

44
00:05:05,712 --> 00:05:09,113
Đồng ý. Ừ, nhờ cậu.

45
00:05:16,112 --> 00:05:18,113
Dù sao, thật hiếm khi gặp cậu ở chỗ này.
Yuzuru có đến đây thường xuyên không? Công viên này ấy.

46
00:05:18,912 --> 00:05:28,113
Phản ứng. Chà, không nhiều lắm.
Tớ thích những nơi yên tĩnh hơn là những nơi ồn ào.

48
00:05:28,912 --> 00:05:30,113
Vậy à? Tớ cũng vậy.

49
00:05:30,912 --> 00:05:42,113
Hiểu biết. Ra là vậy.
Cậu thấy Kotori quá ồn ào và cậu không thoải mái.
Tớ sẽ báo cáo lại.

50
00:05:42,912 --> 00:05:45,113
Này này... làm ơn đừng.
Tớ sẽ bị giết mỗi sáng đấy.

51
00:05:46,112 --> 00:05:55,113
Mỉm cười. Vậy nếu cậu không muốn tớ mách, 
cậu có thể nắm tay không?

52
00:05:55,912 --> 00:05:57,113
Hở? Ừ... Được thôi...

53
00:05:58,512 --> 00:06:05,113
Ấm áp... Ấm thật đấy.

41
00:06:06,112 --> 00:06:07,713
Mà trời dạo này bắt đầu lạnh rồi

42
00:06:08,512 --> 00:06:13,113
Liên quan. Nhưng tớ đã gặp rắc rối.

43
00:06:14,112 --> 00:06:15,313
Cậu gặp rắc rối sao?

44
00:06:16,112 --> 00:06:31,113
Suy nghĩ. Tớ nói tớ đến đây vì lý do tương tự như cậu.
Nói cách khác, ý cậu là: "Yuzuru phiền phức giống Kaguya."
Tớ có thể sẽ bị bỏ rơi.

45
00:06:31,912 --> 00:06:34,113
Đừng. Tớ không hề nghĩ thế.

46
00:06:34,512 --> 00:06:45,513
Từ chối. Tớ không thể không lo lắng về nó.
Cậu đã bắt được điểm yếu của tớ.
Cậu muốn tớ làm gì để đổi lấy sự im lặng đây?

47
00:06:46,713 --> 00:07:01,113
"Khoanh tay" hay "véo má"? Hay là "gối đùi"?
Aaa... Chuyện gì sẽ xảy ra với Yuzuru đây?

60
00:07:01,512 --> 00:07:03,113
Đã nói là tớ sẽ không làm vậy đâu.

61
00:07:05,312 --> 00:07:06,313
Sao... sao vậy?

62
00:07:06,712 --> 00:07:11,513
Thở dài. Không có gì đâu.

63
00:07:12,312 --> 00:07:14,113
Không có gì mà cậu lại phản ứng vậy sao?

64
00:07:14,512 --> 00:07:24,113
Làm lơ. Đã nói là không có gì mà.
Tớ không nghĩ sự hài hước của cậu tệ đến thế.

65
00:07:24,312 --> 00:07:25,513
Thế... là sao?

66
00:07:26,112 --> 00:07:28,113
Rõ ràng Yuzuru không có được câu trả lời như ý muốn.
Nếu Kotori hỗ trợ thì tốt hơn.

67
00:07:28,712 --> 00:07:30,513
Xin lỗi. Có lẽ tớ đã nói sai rồi.
Cậu sẽ cho tớ cơ hội sửa đổi chứ?

68
00:07:30,912 --> 00:07:41,113
Suy nghĩ. Cậu không sai, 
nhưng cậu có thể trả lời câu này được không?

69
00:07:41,512 --> 00:07:42,513
Ừ. Là gì vậy?

70
00:07:43,512 --> 00:07:53,113
Cám dỗ. Hôm nay, tớ không có gì để làm cả.
Còn cậu thì sao, Shidou?

71
00:07:55,512 --> 00:07:57,113
Không lẽ đây là...

72
00:07:57,512 --> 00:07:59,713
Tớ sẽ đi mua đồ ăn. Cậu muốn tối nay ăn gì?
Được rồi. Giờ đến khu mua sắm mua đồ và...



81
00:08:01,512 --> 00:08:03,113
Thật sự có chuyện gì đó rồi.
Cậu ấy đang làm bộ mặt "yare yare" chán chường.

82
00:08:04,112 --> 00:08:11,113
Suy nghĩ. Shidou, không lẽ cậu không xem
Yuzuru là một phụ nữ sao?

83
00:08:11,912 --> 00:08:12,513
Hở?

84
00:08:13,512 --> 00:08:24,113
Từ chối. Không có gì đâu.
Tớ cũng không đi mua sắm đâu.
Xin lỗi vì đã làm phiền.

85
00:08:24,912 --> 00:08:26,113
Ừ...ừ...

86
00:08:26,312 --> 00:08:27,713
Tôi cảm thấy có chút ẩn ý trong lời nói.


87
00:08:28,112 --> 00:08:29,713
Có lẽ tôi đã làm gì sai rồi.
Sau này tôi cần bù đắp tốt hơn.

88
00:08:31,112 --> 00:08:32,113

