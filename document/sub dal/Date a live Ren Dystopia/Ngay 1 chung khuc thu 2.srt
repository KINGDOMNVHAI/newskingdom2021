﻿116
00:00:04,512 --> 00:00:06,313
Bên ngoài trời đã trở lạnh.
Sắp đến tháng 12 rồi.

116
00:00:09,112 --> 00:00:15,113
Shidou-san, chào buổi sáng.

116
00:00:16,512 --> 00:00:22,113
Yahoo, Shidou-kun.
Lời chào của Yoshino không làm tim em 
cảm thấy ấm áp lên đâu.

116
00:00:22,912 --> 00:00:24,513
Yoshino và Yoshinon.
Sáng nay các em tính đi đâu vậy?

116
00:00:25,512 --> 00:00:30,113
Em đến đón Natsumi-san ạ.

116
00:00:31,112 --> 00:00:32,313
À, ra là vậy.

116
00:00:35,112 --> 00:00:45,113
Gì vậy? Anh đang nghĩ em có vẻ ngốc 
khi đi ra ngoài trước và đợi ở ngoài phải không?

116
00:00:45,912 --> 00:00:47,313
Anh có nghĩ thế đâu.
Em có hẹn với Yoshino phải không?

116
00:00:48,112 --> 00:00:56,313
Vâng... đúng vậy ạ. Em muốn chào anh 
nên đã để Natsumi-san phải đợi.

116
00:00:57,512 --> 00:01:03,113
Anh thích ai hơn nào? Người đến đón anh
hay người đến chào anh?

116
00:01:04,112 --> 00:01:08,513
Yo... Yoshinon. Thích ai hơn... sao?

116
00:01:10,112 --> 00:01:17,113
Shidou-kun được 2 cô gái dễ thương
đợi ở ngoài cửa đấy nhé.

116
00:01:17,912 --> 00:01:20,313
haha, đúng vậy nhỉ.
Vậy thì, lần sau nhớ qua chào anh cùng với Natsumi nhé.

116
00:01:21,112 --> 00:01:27,113
Vâng, cảm ơn anh. Vậy chúng em đi nhé.

116
00:01:28,112 --> 00:01:30,513
Sáng nay có vài thứ thay đổi.
Kotori dặn nếu có chuyện gì, hãy báo cáo với con bé.
Những điều này dường như xảy ra hàng ngày 
với tôi cho đến năm ngoái.

116
00:01:31,112 --> 00:01:33,113
Nhưng mà, đã lâu rồi không có
sự kiện gì nóng hổi. Hơn nữa, đây là yêu cầu
của cô em gái dễ thương. Hãy cẩn thận!









116
00:01:40,112 --> 00:01:46,113
Shidou, chào buổi sáng!
Sao hôm nay anh dậy sớm hơn mọi ngày vậy?

116
00:01:46,912 --> 00:01:49,313
Chào buổi sáng, Tohka.
Hôm nay Natsumi đến gọi anh dậy.
Em ấy còn phụ anh bữa sáng nên mọi việc nhanh hơn.

116
00:01:50,112 --> 00:01:59,113
Gì cơ? Đừng làm vậy, Natsumi!
Em cũng có thể thức dậy và ăn sáng...
Nhưng sẽ rất phiền nếu ngày nào 
cũng qua ăn như vậy.

116
00:02:00,112 --> 00:02:02,513
Haha. Em luôn được chào đón mà.
Nhưng đừng ngại nói với anh trước.
Làm bữa sáng cần nhiều thời gian đấy.

116
00:02:03,113 --> 00:02:10,113
V... vâng... em xin lỗi.
Đồ ăn anh nấu rất ngon.

116
00:02:10,912 --> 00:02:13,313
Không, anh rất thích được nấu ăn.
Tohka giúp anh làm những món ăn ngon đấy.
Lần sau, chúng ta hãy ăn cùng nhau từ sáng đến tối nhé.

116
00:02:14,112 --> 00:02:20,113
Thật không? Anh hứa rồi đấy nhé.
Em rất mong chờ đấy!

116
00:02:20,912 --> 00:02:22,513
Tohka vui vẻ như thời tiết thường ngày,
như không có chuyện gì xảy ra cả.





116
00:02:28,712 --> 00:02:39,313
Chào, đến sớm nhỉ, Heart of Friend của tôi!
Tohka-chan cũng vậy. Hôm nay cũng vậy.
Tôi muốn xem các ông đi học vào buổi sáng.

116
00:02:40,112 --> 00:02:42,513
Người đang đứng nói chuyện là Tonomachi.
Hắn ta là bạn cùng lớp với chúng tôi.

116
00:02:42,712 --> 00:02:43,913
Ồ, Tonomachi đấy à? Hôm nay tôi khỏe.

116
00:02:44,112 --> 00:02:53,113
Chào buổi sáng.
Shidou. Tonomachi đã cho anh xem gì vậy?
Em cũng muốn xem.

116
00:02:53,912 --> 00:02:56,313
À... không, anh nên giải thích thế nào nhỉ?
Nói đơn giản, Tohka và anh đang ở cùng một nơi.

116
00:02:56,912 --> 00:03:05,113
Ra là vậy. Nhưng tại sao 
Tonomachi lại bận tâm nói vậy?

116
00:03:05,912 --> 00:03:07,313
Anh cũng không biết.

116
00:03:08,112 --> 00:03:19,113
Tohka-chan dám hỏi ông thay vì hỏi tôi 
khi tôi ở ngay trước mặt. Tôi ghen tị với ông.
Nhưng tôi không chỉ ghen tị với Heart of Friend.

116
00:03:19,912 --> 00:03:21,513
Tonomachi, câu nói Heart of Friend đó là gì vậy?

116
00:03:22,112 --> 00:03:32,113
Ông đùa đấy hả? Ông không hiểu
tiếng Anh đơn giản như vậy sao?
Hãy để tôi nói cho ông biết.
Đó là bạn tri kỷ đấy!

116
00:03:32,912 --> 00:03:34,313
Tôi nghĩ Heart of Friend sẽ là 
"Trái tim của người bạn" mà.

116
00:03:35,112 --> 00:03:48,113
Ông nghe tôi nói Heart of Friend rồi mà.
Nói thật, ông ngốc nên tôi nói cũng vậy.
Ông có thể nói tiếng Anh sao?

116
00:03:48,912 --> 00:03:50,113
Không phải thế...
Tôi nghĩ từ đó ít phổ biến.

116
00:03:51,112 --> 00:04:04,113
Không không, có rất nhiều câu chuyện 
về việc bắt đầu bằng tiếng Anh.
Đậu đại học, trở nên nổi tiếng...
Bên cạnh đó, bây giờ là thời điểm tốt để bắt đầu.

116
00:04:04,912 --> 00:04:06,313
Đó là phiên bản của lớp hội thoại tiếng Anh.
Ông sẽ cảm nhận được nó.

116
00:04:07,112 --> 00:04:16,113
Ngoài ra, còn có các cô gái 
trên thế giới phải không?
Tôi cũng muốn chào đón các cô gái ngoại quốc.

116
00:04:17,112 --> 00:04:19,313
Ra là vậy, Làm việc chăm chỉ để trở nên nổi tiếng 
không có gì là xấu. Thật bất ngờ khi
hắn có thể nghĩ ra mục đích này.

116
00:04:19,912 --> 00:04:22,113
Dù vẫn còn bất khả thi. Tuy nhiên, hãy ủng hộ
Tonomachi khi hắn đang cố gắng hết mình.

116
00:04:22,312 --> 00:04:24,513
Đúng là Tonomachi. Tôi nghĩ đã gần đến lúc ông
thành thạo tiếng Anh và trở nên nổi tiếng rồi đấy.

116
00:04:24,712 --> 00:04:35,513
Tôi biết rồi! Heart of Friend!
Mọi người đều nói tôi ngốc, nhưng tôi sẽ làm thế.
Nếu ông muốn nói tiếng Anh, hãy gặp tôi bất cứ lúc nào.

116
00:04:35,912 --> 00:04:37,513
Tôi sẽ gặp ông sớm thôi, trái tim của bạn tôi.

116
00:04:38,112 --> 00:04:45,113
Vậy giờ tôi lên lớp đây.
Nếu có vấn đề gì, hãy hỏi tôi nhé. Chau!

116
00:04:45,912 --> 00:04:48,113
Chau nghe giống tiếng Ý. (Ciao)
Mà thế mới đúng là Tonomachi.

116
00:04:53,112 --> 00:04:59,113
Shidou, có chuyện gì với Tonomachi vậy?
Cậu ta trông khác mọi ngày.

116
00:04:59,712 --> 00:05:02,113
Vậy à? Anh thấy Tonomachi thế là bình thường mà.
Dù sao, chúng ta phải đi thôi.

116
00:05:03,112 --> 00:05:09,113
Anh nói Heart of Friend đi.
Không biết nghe thế nào nhỉ.

116
00:05:09,912 --> 00:05:11,513
Làm ơn đừng nhắc đến nó nữa.