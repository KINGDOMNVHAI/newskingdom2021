﻿0
00:00:00,312 --> 00:00:02,113
Đi đến Sân Thượng?
1) Đồng ý
2) Không đồng ý

1
00:00:05,912 --> 00:00:07,113
Haa... Thời tiết thật đẹp.

2
00:00:07,312 --> 00:00:09,113
Khi tôi lên sân thượng, không khí trở nên
thoáng mát hơn rất nhiều

3
00:00:09,512 --> 00:00:11,113
Chỉ là nghỉ ngơi thôi, nhưng cảm giác thật lạ 
khi nằm dưới bầu trời.

4
00:00:11,512 --> 00:00:17,513
Cậu đang có tâm trạng không tốt sao, Shidou?

5
00:00:18,112 --> 00:00:20,113
Giọng nói này là...

6
00:00:20,512 --> 00:00:25,113
Nhìn đâu thế hả? Ở đây này! Yahoo!

7
00:00:26,912 --> 00:00:28,313
Kaguya đấy à?

8
00:00:28,912 --> 00:00:33,513
Vẻ mặt gì vậy? Cậu ngạc nhiên lắm à?

9
00:00:34,112 --> 00:00:35,713
Giờ cậu có kiểu nhảy từ trên xuống à?
Nguy hiểm lắm đấy! Phải cẩn thận chứ!

10
00:00:36,112 --> 00:00:44,113
Con gió này không cần phải cẩn thận.
Tớ là bầu trời và cơn gió mà.

11
00:00:44,912 --> 00:00:47,113
Cậu leo lên đó vì cậu nghe tiếng bước chân
của tớ à? Để xuất hiện một cách hoành tráng sao?

12
00:00:48,112 --> 00:00:52,113
Tớ không làm thế! Cậu nghe thấy sao?

13
00:00:52,712 --> 00:00:54,513
Phản ứng dễ thương nhỉ. Cậu đang làm gì trên này vậy?

14
00:00:55,112 --> 00:01:03,513
Cậu biết mà.
Từ ngai vàng trên bầu trời này, 
tớ có thể ngắm nhìn hạ giới.

15
00:01:04,112 --> 00:01:06,513
Tóm lại, cậu đang ngắm nhìn 
tất cả mọi người trong trường. Đúng không?

16
00:01:06,912 --> 00:01:19,513
Cậu không cần phải nói thẳng đâu.
Mà... đúng vậy. Nhìn đi Shidou.
Mọi người đều bận rộn như những công nhân chăm chỉ.

17
00:01:20,112 --> 00:01:22,113
Ừ, có lẽ họ bận học hay sinh hoạt CLB.
Học sinh năm 3 cũng sắp có kỳ thi.

18
00:01:22,512 --> 00:01:32,113
Điều đó cũng rất quan trọng.
Tuy nhiên, thời gian sẽ không trở lại. 
Điều gì sẽ xảy ra nếu cứ để nó trôi qua?

19
00:01:32,912 --> 00:01:34,713
Nhưng chẳng lẽ họ không cần làm việc chăm chỉ
để vào được một trường đại học tốt 
hoặc làm việc cho một công ty tốt sao?

20
00:01:35,112 --> 00:01:37,513
Không phải thế.

21
00:01:38,512 --> 00:01:39,513
Hở?

22
00:01:40,112 --> 00:01:51,113
Tớ là người thích cạnh tranh.
Tớ cũng thường thi đấu. Nhưng đó là để cho vui.

23
00:01:51,512 --> 00:02:02,113
Đã một thời gian kể từ khi tớ bắt đầu đi học.
Tớ tự hỏi đây là gì?
Mọi người không có lựa chọn nào khác ngoài việc học.

25
00:02:02,512 --> 00:02:03,513
Vậy sao?

25
00:02:04,112 --> 00:02:14,113
Có những thứ tớ muốn làm hoặc quan tâm.
Tớ sẽ thoát ra khỏi cuộc thi mà mọi người đang làm hoặc từ bỏ nó.

25
00:02:14,912 --> 00:02:27,113
Có quá nhiều việc phải làm ngay từ đầu.
Tớ cảm thấy mình không có thời gian để thực hiện.
Vì nhiều lý do, tớ chẳng làm được gì.






25
00:02:27,712 --> 00:02:30,113
Ra là vậy. Tóm lại, nguyên nhân 
khiến bài kiểm tra trước không tốt
là do Kaguya muốn làm điều mình muốn.

25
00:02:30,912 --> 00:02:33,113
Đã bảo cậu không cần phải nói thẳng ra mà!

25
00:02:34,112 --> 00:02:35,513
Tớ đùa thôi.

25
00:02:36,512 --> 00:02:37,113
Thực ra, tôi không biết Kaguya đang nói gì.

25
00:02:37,712 --> 00:02:39,113
Trông cậu thế này, tớ hơi bất ngờ và
nghĩ về nhiều thứ.

25
00:02:39,912 --> 00:02:41,113
Trông tớ thế nào?

25
00:02:41,712 --> 00:02:43,113
Tôi không kìm nén được.
Tiếng nói trong lòng tôi bất chợt phát ra.

25
00:02:43,712 --> 00:02:45,113
Không, không có gì đâu.
Điều cậu nói có thể đúng.

25
00:02:45,712 --> 00:02:47,713
Nếu cậu đã quyết định bản thân sẽ trở nên tốt hơn.
Có lẽ mọi người cũng sẽ yên tâm.

25
00:02:48,112 --> 00:02:53,513
Shidou cũng nhìn ra sao, quân sư của tớ?




25
00:02:54,912 --> 00:03:08,113
Đúng là quân sư của tớ.
Tớ sẽ cho thấy tớ là mảnh ghép
để thống trị thế giới.
Hãy cùng nhau thống trị thế giới nào!

25
00:03:08,712 --> 00:03:10,113
Tầm vĩ mô đã phát triển đáng kể rồi...

25
00:03:10,512 --> 00:03:12,113
Nhưng phải thế này mới giống Kaguya,
một người hay nói chuyện viễn vông.

25
00:03:12,512 --> 00:03:14,113
Cảm ơn vì sự cố gắng của cậu!
Mục tiêu là chinh phục thế giới!
Và hãy làm cho thế giới hạnh phúc nhé!

25
00:03:15,112 --> 00:03:23,113
Tinh thần tốt lắm!
Vì vậy, chúng ta nên bắt đầu 
những việc để thống trị thế giới!

25
00:03:23,712 --> 00:03:25,113
Hở? À phải rồi...

25
00:03:25,512 --> 00:03:27,113
Thống trị thế giới... Cụ thể cậu sẽ làm gì?

25
00:03:27,512 --> 00:03:29,513
Trở thành chính trị gia và làm về luật pháp?
Hay là một triệu phú và thay đổi nền kinh tế?
Cậu sẽ làm gì?

25
00:03:29,712 --> 00:03:30,913
Đến giờ học rồi nhỉ?

25
00:03:31,112 --> 00:03:34,113
Rốt cuộc vẫn phải quay lại học sao?



