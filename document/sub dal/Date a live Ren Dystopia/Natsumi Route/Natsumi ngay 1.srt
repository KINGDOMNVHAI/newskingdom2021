﻿0
00:00:01,112 --> 00:00:03,113
Đi đến Khu Dân Cư
1) Đồng ý
2) Không đồng ý

1
00:00:07,312 --> 00:00:09,113
Hôm nay tôi không mua sắm gì cả
mà chỉ về thẳng nhà.

2
00:00:09,912 --> 00:00:12,513
Khi đến khu dân cư, 
tôi nghe thấy tiếng chạy của ai đó.

3
00:00:19,112 --> 00:00:21,113
Natsumi? Sao em chạy vội thế?
Chuyện gì xảy ra à?

4
00:00:21,912 --> 00:00:25,113
Cứu... cứu em với Shidou!
Có người đuổi theo em!

5
00:00:25,912 --> 00:00:27,113
Có người đuổi theo sao?

6
00:00:27,312 --> 00:00:28,513
Sự căng thẳng xuất hiện không thể nói thành lời

7
00:00:28,912 --> 00:00:30,313
Ai có thể đuổi theo Tinh Linh? AST hay DEM?
Không, hay là Kurumi...

8
00:00:31,112 --> 00:00:35,113
Natsumi-san kia rồi!

9
00:00:40,912 --> 00:00:42,313
Là Miku sao?

10
00:00:43,112 --> 00:00:48,113
Em vừa nói còn gì!
Em đang bị truy đuổi thật mà!

11
00:00:49,112 --> 00:01:00,113
Hóa ra Natsumi-san tìm darling!
Hôm nay đúng là may mắn! Itadakimasu!

14
00:01:14,112 --> 00:01:15,513
Miku bắt Natsumi bằng cơ thể của mình.
Điều đó thật tồi tệ.

15
00:01:15,912 --> 00:01:17,313
Này này, dừng lại đi, Miku...

16
00:01:17,912 --> 00:01:23,113
Em xin lỗi, darling. Tại em hơi sốt.

17
00:01:26,512 --> 00:01:27,513
Xỉu

18
00:01:28,912 --> 00:01:30,313
Không hề thay đổi. Mọi thứ chỉ như ngày hôm qua.

19
00:01:30,912 --> 00:01:32,313
Vậy là... nãy giờ cậu đuổi theo em ấy à?

20
00:01:32,912 --> 00:01:39,113
Đúng rồi! Em định tặng một món quà
ngọt ngào vì bữa sáng nay đấy!

21
00:01:40,112 --> 00:01:48,113
Shidou mới là người nấu bữa sáng.
Em chỉ giúp thôi mà. Tại sao em...?

22
00:01:49,112 --> 00:01:57,113
Nếu thế thì em sẽ cảm thấy bị bỏ rơi, đúng không?
Có nhiều lắm, đừng ngại!

23
00:01:58,112 --> 00:02:05,113
Vậy tại sao chị lại đuổi theo em?
Chị chỉ cần đưa đồ ngọt là xong thôi mà.

24
00:02:06,112 --> 00:02:16,113
Ừ, phải rồi nhỉ. Khi chị thấy Natsumi-san, 
chị đã không kiềm chế được...

25
00:02:17,112 --> 00:02:18,513
Giống một con cá mập ngửi thấy mùi máu.

25
00:02:19,112 --> 00:02:24,113
Mà bỏ qua tiểu tiết đi. Đây, xin mời!

25
00:02:25,112 --> 00:02:29,113
Món... món quà này có mục đích gì?

25
00:02:30,112 --> 00:02:43,113
Trước tiên là đánh giá tình hình với đồ ngọt. 
Sau đó là mời đến một bữa tiệc trà,
Cuối cùng, chúng mình sẽ là những người bạn thân thiết, 
tặng quà cho nhau mỗi khi có sự kiện.
Đó là một kế hoạch rất hoàn hào.

25
00:02:44,112 --> 00:02:46,113
Nghe có mùi nguy hiểm...

25
00:02:47,112 --> 00:02:55,113
Chị lỡ miệng nói ra hết rồi.
Đúng là "Viên ngọc trai của thành phố Tengu"...
Natsumi-san... đáng sợ quá!

25
00:02:56,112 --> 00:03:00,113
Không, em có hỏi đâu...

26
00:03:00,912 --> 00:03:02,313
Thực ra là đang giả vờ đấy...

26
00:03:03,112 --> 00:03:17,113
Không phải đâu! Đó là thứ chứng minh
chị không có ý đồ gì cả! 
Nếu Natsumi-san không nhận, chị... chị...




26
00:03:18,112 --> 00:03:24,113
Em... em hiểu rồi...
Em sẽ nhận nó... được chưa...?

26
00:03:25,112 --> 00:03:28,113
Cảm ơn em nhiều lắm!

26
00:03:29,112 --> 00:03:32,113
Tại sao chị lại cảm ơn em
vì đã tặng cho em?

26
00:03:33,712 --> 00:03:42,113
Còn đây là dành cho darling và Kotori.
Đồ ăn em tự làm đấy.

26
00:03:43,112 --> 00:03:44,713
Cảm... cảm ơn cậu.
Tớ sẽ nói với Kotori như vậy.

26
00:03:46,112 --> 00:03:51,113
Anh nhớ ăn nó trước khi đi ngủ nhé!
Hẹn gặp lại!

32
00:03:53,512 --> 00:03:55,113
Nói xong, Miku rời đi.

33
00:03:57,912 --> 00:03:59,313
Nhưng cô ấy nghĩ ra điều gì đó và quay lại.

34
00:04:08,112 --> 00:04:11,113
Đây là nụ hôn tạm biệt đấy!

35
00:04:13,112 --> 00:04:16,113
Như thường lệ, cô ấy giống một cơn bão...

36
00:04:20,112 --> 00:04:21,513
Em không sao chứ, Natsumi...?



37
00:04:22,112 --> 00:04:25,113
Một... một chút thôi...

40
00:04:26,213 --> 00:04:35,113
Nhưng mà, em đang thấy bối rối.
Có vẻ em đã xử lý đúng cách. Anh thấy thế nào?

41
00:04:35,913 --> 00:04:38,113
Em sẽ không gặp rắc rối gì đâu.
Miku cũng sẽ không làm phiền em đâu.

42
00:04:38,913 --> 00:04:44,113
Em biết mà. Vì em biết
nên em mới nói với anh đấy.

43
00:04:45,112 --> 00:04:50,113
Anh cũng không phải lo lắng nhiều về em đâu...

39
00:04:52,912 --> 00:04:53,713
Natsumi...

40
00:04:54,113 --> 00:04:55,513
Natsumi vẫn rời đi như trước.

41
00:04:56,113 --> 00:04:58,113
Em ấy không quen với việc được yêu thương chăm sóc sao?
Tôi không biết cảm giác đó thế nào.
Lời chia tay của Natsumi đầy nặng nề.

42
00:04:58,713 --> 00:05:00,513
Nhưng Natsumi có lẽ không ghét tôi.
Em ấy sẽ quen dần với điều đó thôi.

43
00:05:00,712 --> 00:05:02,513
Thật lòng, tôi mong một ngày nào đó,
chúng tôi có thể trao đổi nhiều hơn.
