﻿0
00:00:00,512 --> 00:00:02,513
Đi đến Đồi Núi?
1) Đồng ý
2) Không đồng ý

0
00:00:06,112 --> 00:00:08,513
Thời tiết thật đẹp.
Ở đây có thể ngắm cả thành phố.
Bất kỳ ai cũng nên đến đây 1 lần.

1
00:00:14,112 --> 00:00:25,113
Shidou-kun đó sao? Sao anh lại lên đây?
Không lẽ anh nhớ Yoshino?

2
00:00:26,112 --> 00:00:33,113
Yoshinon!
Shidou-san, sao anh lại lên đây ạ?

3
00:00:33,912 --> 00:00:36,313
Anh chỉ muốn thay đổi không khí thôi.
Lên đây là tốt nhất đấy. Thế còn em thì sao?

4
00:00:37,112 --> 00:00:41,113
Em cũng đang ngắm thành phố thôi.

5
00:00:41,912 --> 00:00:43,313
Yoshino cũng đến để ngắm cảnh à?

6
00:00:44,112 --> 00:00:56,513
Vâng. Vẫn còn nhiều nơi em chưa biết.
Nên em muốn ngắm nhìn nhiều nơi hơn.

7
00:00:57,512 --> 00:01:08,113
Nhưng mà, em vẫn còn hơi sợ 
đến nơi mà em không biết...
Vì vậy, em nghĩ nên ngắm nhìn từ xa.

8
00:01:09,112 --> 00:01:16,113
Yoshino là một cô bé cẩn thận,
luôn chuẩn bị cho mọi thứ.

9
00:01:16,912 --> 00:01:18,313
Ừ, thế mới là Yoshino.
Anh chỉ hơi lo vì không biết em ở đâu.

10
00:01:19,112 --> 00:01:21,113
Nhưng anh rất bất ngờ khi em đến đây.
Trước giờ anh cứ nghĩ đây là chỗ của riêng anh.

11
00:01:22,112 --> 00:01:27,113
Vâng! Nhưng không chỉ một mình em đâu.



12
00:01:27,712 --> 00:01:29,113
Sao cơ?

13
00:01:30,112 --> 00:01:43,113
Natsumi-san... đi chơi với cậu ấy rất vui.
Vì vậy em muốn biết thêm về thành phố
để giúp Natsumi-san cho đúng cách.

14
00:01:43,912 --> 00:01:45,313
Yoshino...

15
00:01:45,912 --> 00:01:48,113
Yoshino thật tuyệt vời.
Em suy nghĩ trưởng thành rất nhiều đấy.

16
00:01:49,112 --> 00:02:03,113
Natsumi-san biết rất nhiều điều mà em chưa biết.
Em muốn học Natsumi-san và làm anh bất ngờ.

25
00:02:03,912 --> 00:02:05,313
Ra là vậy. Anh cũng nghĩ Natsumi biết rất nhiều đấy.

25
00:02:05,912 --> 00:02:07,313
Natsumi dường như đã đi vòng quanh thế giới trước khi phong ấn.
Tôi nghĩ em ấy có rất nhiều kiến ​​thức.

25
00:02:07,912 --> 00:02:10,113
Bởi vì Natsumi nói: "Yoshino giới thiệu cho em. 
Nhưng không hiểu sao em không thể giả vờ 
là em không biết gì được". Ra là vậy.

25
00:02:11,112 --> 00:02:17,113
Vâng! Em là... học sinh năm cuối rồi!

25
00:02:17,912 --> 00:02:20,113
Tính theo thời gian đến đây, Yoshino là Tinh Linh lớn tuổi hơn.
Tôi đã không nhận ra Yoshino có thể 
trở thành một người chị rất tốt.

25
00:02:20,512 --> 00:02:22,113
OK, cố gắng lên, Yoshino-senpai.

25
00:02:26,112 --> 00:02:36,113
Có ổn không đấy, Shidou-kun?
Shidou-kun nên giúp cậu ấy 
sớm bắt kịp với Natsumi-san đi.

25
00:02:37,112 --> 00:02:39,113
Thế thì đáng lo đấy.
Anh có thể hướng dẫn em biết nhiều hơn về thành phố này.
Có điều gì em muốn học không?

25
00:02:40,112 --> 00:02:42,113
Như vậy ổn không ạ?

25
00:02:42,912 --> 00:02:44,513
Anh vẫn hiểu thành phố này mà.
Yoshino-senpai có muốn anh chỉ không?

25
00:02:45,112 --> 00:02:50,113
Vậy nhờ anh giúp đỡ.

25
00:02:51,112 --> 00:02:58,113
Cảm ơn nhé, Shidou-kun.
Anh ghi điểm với Yoshino nhiều hơn
nhờ lòng tốt của anh đấy.

25
00:02:59,112 --> 00:03:13,113
Yo... Yoshinon!
Nếu vậy, em muốn biết những con đường
mà anh thường đi và các kỷ niệm của anh.

26
00:03:13,712 --> 00:03:15,513
Nếu em ấy nói những điều tốt về chúng tôi với Natsumi,
bản thân Natsumi sẽ tỏ ra không muốn nghe 
dù thực chất em ấy cũng muốn.

26
00:03:15,912 --> 00:03:18,313
Đơn giản nhất nhé. 
Trước tiên, ngôi nhà mái xanh đó là nhà anh.
Tòa nhà bên cạnh là chung cư nơi mọi người sống.

26
00:03:18,712 --> 00:03:21,113
Trước khi đi học, anh thường gặp 
Tohka, Kaguya, Yuzuru trước cổng vào buổi sáng.
Trường học đi trên con đường từ đó. Đằng kia kìa.

26
00:03:21,712 --> 00:03:25,113
Anh chị hằng ngày đi học chung ạ?

26
00:03:25,912 --> 00:03:28,313
Không phải hằng ngày đâu.
Tohka cũng vậy. Kaguya và Yuzuru cũng thường đi trước.

26
00:03:28,912 --> 00:03:30,513
Họ còn có cả thi xem ai đến trường trước nữa đấy!

26
00:03:31,112 --> 00:03:39,113
Em rất vui vì mọi người vẫn như thường ngày ạ.

26
00:03:39,912 --> 00:03:46,113
Mọi người đều không ngừng cố gắng.
Giống như em vừa rồi vậy.

26
00:03:47,112 --> 00:03:54,513
Vậy các anh chị đi đâu sau giờ học ạ?
Bọn em thỉnh thoảng hay gặp nhau trong thành phố.

26
00:03:55,112 --> 00:03:57,113
Phải rồi. Bọn anh đi mua sắm và làm nhiều thứ khác.

26
00:03:57,712 --> 00:03:59,513
Anh đến khu mua sắm ở đó.
Em có thể ra đường trước nhà ga.

38
00:04:00,112 --> 00:04:15,113
Anh lúc nào cũng có con gái từ sáng sớm đến chiều tối.
Ghen tị thật đấy. Cứ thế này thì khó đến lượt Yoshino lắm.

39
00:04:15,712 --> 00:04:17,113
Không, không phải. Anh còn có Tonomachi mà.

40
00:04:17,913 --> 00:04:27,113
Thật vậy sao? Nào nào, cậu nên yêu cầu
Shidou-kun đưa cậu đi đâu đó đi chứ.

41
00:04:27,913 --> 00:04:33,113
Không đâu. Chỉ cần ở cạnh
Shidou-san là tớ vui rồi.

42
00:04:34,113 --> 00:04:36,313
Đừng ngại, Yoshino.
Nơi nào em không thể tự đến một mình
thì anh sẽ dẫn em đi.

43
00:04:37,112 --> 00:04:40,113
V... vâng. Em hiểu rồi.

39
00:04:41,112 --> 00:04:49,113
Ngoài Natsumi-san ra, thành phố này
đầy những chiến tích của Shidou-san đấy.

40
00:04:49,713 --> 00:04:51,113
Khoan, khoan. Đừng nói những thứ
gây hiểu lầm như vậy.

41
00:04:52,113 --> 00:05:01,113
Shidou-san, anh đừng lo.
Natsumi-san đã biết những chuyện đó rồi.

44
00:05:01,512 --> 00:05:02,313
Có lẽ vậy thật...

45
00:05:03,112 --> 00:05:09,113
Vậy... ano... Em xin phép ạ.
Giờ em phải đi rồi.

46
00:05:09,912 --> 00:05:17,113
Cảm ơn shidou-kun vì hôm nay nhé.
Anh đừng quên đưa Yoshino đi đấy.

47
00:05:17,712 --> 00:05:19,113
Ừ, anh biết rồi. Hẹn gặp lại.
