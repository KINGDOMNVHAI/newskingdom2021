﻿0
00:00:00,312 --> 00:00:02,113
Đi đến Cổng Trường
1) Đồng ý
2) Không đồng ý

1
00:00:06,312 --> 00:00:08,113
Hôm nay tôi sẽ làm gì?
Tôi vừa đi vừa suy nghĩ một cách mơ hồ...

2
00:00:08,912 --> 00:00:11,513
Darlinggggg!

3
00:00:11,912 --> 00:00:14,113
Khi tôi đột nhiên nghe thấy giọng nói như vậy,
tôi cảm giác có thứ gì đó 
đang tiến đến từ phía sau.

6
00:00:15,912 --> 00:00:17,113
Tôi nhanh chân né khỏi chỗ đó.

7
00:00:17,712 --> 00:00:19,113
Giọng nói này... Cô ấy là...

8
00:00:22,112 --> 00:00:26,113
Tại sao anh lại né chứ, darling?

9
00:00:26,912 --> 00:00:28,313
Đúng là Miku. Có chuyện gì vậy?

10
00:00:30,712 --> 00:00:39,113
Đi ra ngoài chứ gì nữa. Đi-ra-ngoài.
Em, darling cùng tất cả mọi người!

11
00:00:39,712 --> 00:00:40,313
Đi... đi ra ngoài sao?

12
00:00:41,112 --> 00:00:52,113
Sự cô đơn đã đến giới hạn rồi!
Nhanh lên nào! Em phải bổ sung darling
càng sớm càng tốt...

13
00:00:52,912 --> 00:00:55,313
Bổ sung darling gì chứ...
Dù chúng ta quen nhau, 
bất ngờ ôm chầm lấy tớ là hơi quá đấy.

14
00:00:56,112 --> 00:01:02,113
Em xin lỗi... Anh không thể làm
những việc anh không thích.

15
00:01:02,912 --> 00:01:04,313
Thế là sao?

16
00:01:05,112 --> 00:01:14,113
Vì vậy, em quyết định làm những gì em thích.
Darling và các Tinh Linh luôn được chào đón.

17
00:01:15,112 --> 00:01:17,113
Vậy là... sao?

18
00:01:17,912 --> 00:01:19,313
Bằng cách nào đó, tôi không thực sự hiểu.

19
00:01:19,712 --> 00:01:21,113
Mà... không cần gọi họ đâu.

20
00:01:21,712 --> 00:01:30,113
Em đã cất công đến đây rồi mà!
Anh gọi các Tinh Linh-san đến đây đi!

21
00:01:30,912 --> 00:01:32,513
Lại còn thế sao?
Dù tớ có kiếm cũng chưa chắc họ ở đây!

22
00:01:33,112 --> 00:01:41,113
Không sao đâu. Tinh Linh-san và mọi người
sẽ rất vui. Origami-san cũng vậy mà.

22
00:01:41,912 --> 00:01:44,113
Nghe có vẻ nguy hiểm theo cách khác...

23
00:01:44,512 --> 00:01:56,113
Em không muốn bị darling bỏ rơi đâu.
Darling, người đã đến gặp em
trong bộ đồ con gái, anh đi đâu rồi?

24
00:01:56,912 --> 00:01:58,313
Cậu... cậu ép tớ...

25
00:01:59,112 --> 00:02:01,113
Tớ hiểu rồi. Tớ sẽ quay lại.
Và đừng nhảy xồ vào họ đấy nhé!

25
00:02:01,912 --> 00:02:05,113
Vâng! Em yêu anh, darling!

25
00:02:08,112 --> 00:02:10,113
Để Miku đi một mình, tôi sợ cô ấy
sẽ cưa các cô gái khác. Đành phải vậy thôi.

25
00:02:10,312 --> 00:02:13,113
Ở trường còn có Tohka, Origami, Kaguya, Yuzuru.
Kaguya và Yuzuru đã về chưa?
Nếu họ vẫn ở đó, họ có thể sẽ xuất hiện 
trong các hoạt động của CLB và các buổi tụ tập dành cho con gái.

25
00:02:13,312 --> 00:02:15,113
Tohka nói cô ấy gặp nhóm Ai, Mai, Mii.
Origami có lẽ cũng đã về.

25
00:02:15,912 --> 00:02:17,113
Nào Miku, tớ đi đây...

25
00:02:20,512 --> 00:02:22,313
Rõ ràng cô ấy đang chờ đợi như thú săn mồi.
Cô ấy... đang nghe ngóng ai đó chăng?

25
00:02:25,112 --> 00:02:30,113
Origami-sannnn!

25
00:02:31,112 --> 00:02:32,513
Sát khí...

25
00:02:38,112 --> 00:02:41,513
Nhanh quá! Cảnh giác thật đấy, Origami-san!

25
00:02:42,512 --> 00:02:46,113
Miku. Cô tính làm gì?

25
00:02:47,112 --> 00:02:56,113
Đâu có định làm gì đâu!
Cơ thể Tinh Linh mới của Origami-san có thay đổi gì không?
Tôi muốn kiểm tra bằng cách tiếp xúc thôi.

25
00:02:56,912 --> 00:02:58,313
Cái cớ khủng khiếp gì thế này?

25
00:02:59,112 --> 00:03:04,113
Việc kiểm tra Ratatoskr đã thực hiện rồi.
Không cần cô đâu.

25
00:03:05,112 --> 00:03:12,113
Đừng nói vậy mà.
Origami-san cũng có thể chạm vào tôi mà.

26
00:03:13,112 --> 00:03:15,113
Tôi không hiểu thế nghĩa là gì

26
00:03:15,912 --> 00:03:18,113
Thôi nào Miku. Đây là cổng trường đấy.
Mọi người đang nhìn kìa.

26
00:03:19,112 --> 00:03:21,113
Shidou...

26
00:03:21,512 --> 00:03:22,713
Hở? Gì vậy Origami?

26
00:03:23,112 --> 00:03:31,113
Tớ thấy có vẻ tớ đột nhiên bị ốm.
Có thể là do sức mạnh Tinh Linh. Tớ muốn cậu kiểm tra.

26
00:03:31,912 --> 00:03:33,113
Sao giờ cậu lại nói khác vậy?

26
00:03:33,912 --> 00:03:35,113
Tôi thấy họ có chút tâm đầu ý hợp...

26
00:03:36,112 --> 00:03:45,113
Darling thật kỳ cục!
Nhân có 3 chúng ta ở đây, hãy hòa quyện màu sắc này đi!

26
00:03:46,112 --> 00:03:58,113
Shidou là người duy nhất có thể chạm vào tôi.
Chỉ 2 người có thể tạo ra mọi thứ rồi. Hẹn gặp lại.

26
00:04:04,112 --> 00:04:12,113
Buồn quá. Origami-san đúng là rất cứng rắn.

26
00:04:12,912 --> 00:04:20,113
Không còn cách nào khác.
Hôm nay em sẽ cho Shiori-san leo lên người nhé?

38
00:04:20,912 --> 00:04:22,113
Cái gì vậy? Cậu đã đọc loại sách gì thế?

39
00:04:23,112 --> 00:04:37,113
Trời trở lạnh và nỗi nhớ da diết.
Những cô gái nhỏ nhắn và dễ thương.
Em muốn làn da được cọ xát với nhau.

40
00:04:37,913 --> 00:04:39,313
Lúc nào cũng nói những chuyện vô lý.
Thế mới đúng là Miku.

41
00:04:40,113 --> 00:04:42,313
Tớ nghĩ mọi người sẽ hòa hợp với nhau 
khi trò chuyện. Chẳng phải được họ vây quanh
là cậu sẽ hạnh phúc sao?

41
00:04:43,113 --> 00:04:51,113
Mà thôi, hôm nay vậy là tốt rồi. Hẹn gặp anh sau.

42
00:04:52,113 --> 00:04:53,513
Cô ấy vẫn bận rộn như mọi ngày.

43
00:04:54,112 --> 00:04:55,513
Dù sao, tôi đã không hình dung ra
sự bận rộn của cô ấy từ lúc mới gặp.
Đó là ưu điểm khó ai sánh bằng của Miku.

39
00:04:56,112 --> 00:04:57,513
Theo cách nào đó, 
đây cũng là sự tiến bộ... phải không?


