﻿0
00:00:00,112 --> 00:00:03,113
Đi đến Lớp Học?
1) Đồng ý
2) Không đồng ý

1
00:00:07,112 --> 00:00:09,513
Shidou, cùng về nhà đi.

2
00:00:10,112 --> 00:00:12,113
Ừ, hôm nay anh cũng không bận gì cả.

3
00:00:15,512 --> 00:00:17,313
Sao vậy Tohka?

4
00:00:18,112 --> 00:00:24,113
Không, em chỉ nghĩ mỗi khi 
em muốn về cùng anh, sẽ có ai đó đến phá nữa...

5
00:00:24,912 --> 00:00:26,513
Đúng vậy. Lúc nào cũng có ai đó đến.
Cảm giác như đã lâu lắm rồi chúng ta mới về chung.

6
00:00:27,112 --> 00:00:34,113
Phải, hôm nay em rất vui!
Nên em muốn đi qua chỗ này cùng anh.

7
00:00:34,912 --> 00:00:36,113
Đi qua chỗ nào cơ?

8
00:00:36,912 --> 00:00:45,113
Hôm nay, anh được Kaguya và Yuzuru mời bánh mì đúng không?
Em nghĩ nên tặng một chiếc bánh mì cho họ.

9
00:00:45,312 --> 00:00:47,513
Ừ, ý đó hay đấy. Tặng đồ ăn để cảm ơn.
Vậy giờ đi mua bánh mì nhé?

11
00:00:55,112 --> 00:00:57,113
Vậy em định đi đâu, Tohka?

12
00:00:57,912 --> 00:01:09,113
Đầu tiên là tiệm bánh mì em yêu thích.
Cửa hàng món ăn vặt nổi tiếng ở lối vào khu mua sắm.
Bánh mì cà ri và Yakisoba rất ngon.

13
00:01:09,912 --> 00:01:12,113
Thế chẳng phải là quá nhiều sao?
Anh sẽ về nhà với cái bụng no căng mất.

14
00:01:12,712 --> 00:01:27,113
Tiếp theo là cửa hàng gần đó bán bánh mì
hình động vật và khuôn mặt rất thú vị.
Hình ảnh dễ thương rất được trẻ em yêu thích!

15
00:01:27,912 --> 00:01:29,313
Có phải là bánh mì gấu trúc không?
Đó là loại bánh mì quen thuộc mà.
Nhưng nó cũng rất đa dạng.

16
00:01:30,112 --> 00:01:41,313
Tiếp theo là tiệm bánh xa hơn!
Đó là một nhà hàng ăn tại chỗ hay mang về.
Có loại bánh mì có tên khó đọc như sandwich hay donut.

17
00:01:41,912 --> 00:01:44,113
Haha. Đúng là Tohka.
Em tìm hiểu kỹ về bánh mì thật.
Vậy giờ quay lại thứ tự ban đầu đi!

18
00:01:45,112 --> 00:01:46,513
Vậy đi thôi!

19
00:01:47,112 --> 00:01:49,113
Tohka có vẻ giỏi hơn tôi rồi.
Chúng tôi định bước vào cửa hàng...

20
00:01:55,912 --> 00:02:00,513
Ồ, Natsumi phải không?
Em cũng đi mua sắm à?

25
00:02:00,712 --> 00:02:03,113
Em vừa kêu "uge" phải không?

25
00:02:03,112 --> 00:02:19,113
Em kêu khi em bất ngờ gặp ai đó.
Nhưng không phải em không thích gặp anh chị.
Cứ xem như đó là âm thanh bình thường 
khi làm việc với em đi

25
00:02:19,912 --> 00:02:21,513
Thế à? Bọn anh đang tính đi mua sắm.
Em có muốn đi cùng không?

25
00:02:22,112 --> 00:02:35,113
À không... Em... em chỉ đi dạo thôi.
Vậy nên... thôi em đi đây.

25
00:02:37,112 --> 00:02:38,513
Này... Natsumi!

25
00:02:39,112 --> 00:02:41,113
Này, chờ đã Natsumi!

25
00:02:49,112 --> 00:02:52,113
Sao... sao anh chị đuổi theo em?

25
00:02:52,712 --> 00:02:59,113
Em còn hỏi sao?
Sao em lại đột nhiên bỏ chạy?

25
00:03:00,112 --> 00:03:07,113
Không... không phải em chạy...
Mà là... chẳng phải anh chị đang hẹn hò sao?

26
00:03:09,112 --> 00:03:15,113
Không phải hẹn hò, chỉ đi mua sắm thôi.
Shidou và chị đang nghiên cứu về bánh mì!

26
00:03:16,112 --> 00:03:29,113
Nếu đúng như vậy, em là người phá rối.
Chị muốn 2 người đi chung. 
Em không nên tham gia.

26
00:03:30,112 --> 00:03:37,113
Đúng là chị muốn đi 2 người.
Nhưng thêm nữa thì có sao đâu?

26
00:03:40,112 --> 00:03:47,113
Natsumi là bạn của chị.
Mua sắm cùng bạn bè chẳng có gì lạ cả.

26
00:03:48,112 --> 00:03:51,113
Vậy...

26
00:03:52,112 --> 00:03:53,513
Lần này Tohka thắng rồi đấy.
Em đi cùng đi, Natsumi.

26
00:03:54,112 --> 00:04:00,113
Em biết rồi. Nhưng không phải
vì em thích đâu nhé.

26
00:04:00,912 --> 00:04:03,513
Anh biết rồi. Đi thôi nào.
Đi trễ sẽ không còn bánh mì nào nữa đâu.

38
00:04:09,112 --> 00:04:20,113
Ồ, bánh mì kinako giờ
có thể kết hợp với bánh gạo à?

39
00:04:21,112 --> 00:04:27,113
Không biết bằng cách nào, 
nhưng chắc chắn đó là hàng hiếm.
Kaguya và Yuzuru sẽ rất thích!

40
00:04:27,913 --> 00:04:29,513
Lúc nãy, anh có thấy một loại bánh mì ngon.
Em có muốn mua nhiều và nếm thử 
trước khi cho Kaguya và Yuzuru không?

41
00:04:30,513 --> 00:04:34,113
Món này chắc chắn họ sẽ thích!

42
00:04:35,113 --> 00:04:39,113
Chị sung sức thật đấy!

43
00:04:39,912 --> 00:04:44,113
Sao trông em mệt mỏi thế?

39
00:04:45,112 --> 00:04:56,113
Không... Vì em đã tham gia vào
cuộc trò chuyện giữa Tohka với nhân viên.
Em không đam mê bánh mì như chị...

40
00:04:57,113 --> 00:05:03,113
Có phải vậy không? Như vậy là sai sao?

46
00:05:04,112 --> 00:05:12,513
À không, em không có ý đó...
Chị không có lỗi gì đâu.

47
00:05:14,112 --> 00:05:21,113
Với lại, khi em mệt mỏi, 
ăn uống đầy đủ là tốt nhất đấy!
Em cũng nếm thử đi!

48
00:05:22,112 --> 00:05:25,113
Không... em thật sự...

49
00:05:26,512 --> 00:05:29,113
Em không thích sao?

50
00:05:30,112 --> 00:05:35,113
Em xin lỗi. Em sẽ nhận nó.

51
00:05:36,112 --> 00:05:39,113
Ừ. Hãy ăn nó đi.

52
00:05:40,112 --> 00:05:41,513
Haha, Tohka đúng là chững chạc thật.

53
00:05:44,112 --> 00:05:46,113
Bó tay thật...

54
00:05:48,912 --> 00:05:51,113
Sao? Có ngon không?

55
00:05:52,112 --> 00:05:56,113
Um... cũng không tệ.

56
00:05:57,112 --> 00:06:05,113
Natsumi đã có nhận xét rồi!
Chúng ta đưa bánh mì cho Kaguya và Yuzuru thôi!

41
00:06:06,912 --> 00:06:10,113
Cả... cả em nữa sao?

42
00:06:11,112 --> 00:06:17,113
Chúng ta ở cùng chung cư mà! Đi nào!
Cảm ơn anh về ngày hôm nay nhé, Shidou!

43
00:06:18,512 --> 00:06:20,113
Ừ, hẹn gặp lại.


