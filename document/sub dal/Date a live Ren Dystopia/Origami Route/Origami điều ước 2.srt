﻿0
00:00:04,112 --> 00:00:08,113
Tôi nghĩ hôm qua tôi ngủ thiếp đi rồi.
Tôi phải làm theo giải pháp của Kotori.

1
00:00:08,512 --> 00:00:10,513
Hở, điện thoại à?
Ai lại gọi từ sáng sớm vậy nhỉ?

2
00:00:11,112 --> 00:00:12,513
Origami à? Chuyện gì vậy nhỉ?
Gọi từ sớm thế này thật bất thường.

3
00:00:13,112 --> 00:00:14,513
Alo, Origami đó à?

4
00:00:15,112 --> 00:00:17,113
Alo, Itsuka-kun à?

5
00:00:19,712 --> 00:00:21,313
Một giọng nói rất dịu dàng.
Nhưng giọng nói của Origami... đây sao?

6
00:00:21,712 --> 00:00:27,113
Xin lỗi vì đã gọi cậu sớm như vậy.
Tớ muốn nói chuyện với cậu một chút.

7
00:00:27,712 --> 00:00:29,713
Chuyện gì vậy? Cậu cứ hỏi đi.

8
00:00:30,112 --> 00:00:42,113
Ano... Tớ muốn nói vài chuyện,
nhưng giải thích qua điện thoại khó lắm...
Nếu cậu rảnh, tớ có thể gặp cậu không?

Bạn có thể biết bằng cách nhìn vào nó,
nhưng thật khó để giải thích rằng đó là một cuộc điện thoại. .. ..
Nếu có thời gian, tôi có muốn gặp bạn không?

9
00:00:42,712 --> 00:00:44,513
Ừ, tớ hiểu rồi. Tớ không phiền đâu.

10
00:00:44,712 --> 00:00:50,113
Cảm ơn cậu. Hẹn gặp cậu tại chỗ hôm qua nhé.

11
00:00:50,512 --> 00:00:53,113
Có gì đó hơi lạ. Phải chăng cô ấy áy náy
vì những gì đã làm?

12
00:00:53,512 --> 00:00:55,113
Không, Origami chủ động làm vậy mà.

13
00:00:55,512 --> 00:00:57,513
Thôi, cứ đi ra ngoài đã.
Origami chắc cũng đang vội.




14
00:01:03,112 --> 00:01:05,113
Tôi đang trên đường đi gặp Origami,
tôi nhìn thấy bóng dáng ai đó.

15
00:01:05,512 --> 00:01:06,513
Đó là...

16
00:01:09,112 --> 00:01:11,113
Xin chào, Itsuka Shidou.



17
00:01:12,112 --> 00:01:14,113
Đó là Ren.

18
00:01:14,512 --> 00:01:16,313
Chào buổi sáng, Ren.
Mắt cậu đã lành chưa?

19
00:01:16,712 --> 00:01:27,513
À, ừ, cảm ơn cậu đã quan tâm.
Bên mắt còn lại sẽ khỏi nhanh thôi.
Cậu không cần quan tâm đâu.

20
00:01:28,112 --> 00:01:30,113
Quan tâm đến cậu là điều tất nhiên mà.
Tại sao cậu lại ở đây?

21
00:01:31,112 --> 00:01:36,113
Tất nhiên là tôi đang tìm Shidou.
Chuyện về Origami đấy.

22
00:01:37,112 --> 00:01:38,113
Origami đã xảy ra chuyện gì sao?

23
00:01:39,112 --> 00:01:45,113
Sự xuất hiện của Origami thật kỳ lạ.
Giống như một người khác vậy.

24
00:01:46,112 --> 00:01:47,513
Một người khác? Ý cậu là sao?

25
00:01:48,112 --> 00:01:57,113
Cách nói chuyện không còn lạnh lùng nữa.
Trước đây, cô ấy không hề nói về điều gì khác ngoài Shidou.
Nhưng giờ cô ấy khiêm tốn và lịch sự.

25
00:01:58,112 --> 00:02:00,113
Chẳng lẽ là Origami đến từ thế giới khác.
Vì ký ức được chia sẻ 
nên cô ấy đã có một nhân cách khác.

25
00:02:01,112 --> 00:02:03,113
Nếu ai đó không hiểu tình hình đã xảy ra
sẽ trông rất khó hiểu.
Cô ấy đã nói những gì nhỉ?

25
00:02:04,112 --> 00:02:07,513
Dường như cậu đang suy tư điều gì đó phải không?

25
00:02:08,512 --> 00:02:10,113
Hở? À... không có gì...

25
00:02:10,512 --> 00:02:22,113
Vậy thì tôi để việc này cho cậu đấy, Shidou.
Nếu là cậu, sẽ không có vấn đề gì xảy ra đâu.
Cậu sẽ chấp nhận Origami thôi.

25
00:02:22,512 --> 00:02:24,513
Nhưng mà... Phải rồi.
Không có vấn đề gì đâu. Origami vẫn là Origami.
Giờ tớ phải đến gặp Origami đây.

25
00:02:25,112 --> 00:02:33,113
Thật là tệ khi làm mất thời gian của cậu.
Cậu nên tự thấy Origami thì tốt hơn, đúng không?

25
00:02:38,112 --> 00:02:39,713
Sao cô ấy cứ vòng vo thế nhỉ?

25
00:02:40,112 --> 00:02:42,113
Dù sao, bây giờ tôi không có lựa chọn nào khác
ngoài việc xem chuyện gì đã xảy ra với Origami.





25
00:02:48,512 --> 00:02:50,113
Khi tôi đến điểm hẹn, Origami vẫn chờ sẵn từ trước.

25
00:02:50,512 --> 00:02:52,113
Không, Origami này trông khác.
Tôi không thể nhầm lẫn Origami này được.

25
00:02:52,512 --> 00:02:54,513
Nhưng... tôi không chắc lắm.
Bởi vì... đã một thời gian dài kể từ lúc
chúng tôi gặp nhau rồi.

25
00:02:57,112 --> 00:02:59,113
Itsuka-kun

26
00:03:00,112 --> 00:03:01,513
Chào cậu, Origami.

26
00:03:02,112 --> 00:03:03,513
Quả nhiên, Đây là Origami của thế giới khác.

26
00:03:03,912 --> 00:03:05,113
Origami với mái tóc dài...

26
00:03:05,312 --> 00:03:07,113
Cậu là... Origami của thế giới khác phải không?
Mái tóc dài đó...

26
00:03:07,512 --> 00:03:12,513
Ừ... Mới sáng ngủ dậy, tớ đã thấy vậy rồi.

26
00:03:13,112 --> 00:03:14,113
Gì cơ?

26
00:03:15,112 --> 00:03:19,113
Không phải... Tóc giả... là tóc giả đấy.

26
00:03:20,112 --> 00:03:22,113
Haha, chắc là vậy. Làm gì có chuyện 
tóc dài ra chỉ sau một đêm chứ.

26
00:03:23,112 --> 00:03:30,113
Phải rồi...
Bình thường không như vậy đâu.

26
00:03:30,512 --> 00:03:31,713
Origami?

26
00:03:32,112 --> 00:03:35,113
Không... không có gì đâu...

26
00:03:36,112 --> 00:03:37,713
Vậy à? Tớ mong là vậy.
Vậy cậu muốn gặp tớ có chuyện gì vậy?

26
00:03:38,112 --> 00:03:40,113
À, đúng rồi.

27
00:03:41,112 --> 00:03:51,113
Itsuka-kun. Cậu có biết tớ hiện giờ
là sự kết hợp của "tớ của thế giới ban đầu" và
"tớ của thế giới này" không?

28
00:03:51,512 --> 00:03:52,713
Ừ, tớ biết.

29
00:03:53,112 --> 00:03:54,513
Suy cho cùng, tôi là nguyên nhân của tình huống này.

30
00:03:55,112 --> 00:03:56,513
Bởi vì tôi đã cứu ba mẹ của Origami trong quá khứ.
Thế giới đã rẽ sang một nhánh khác.

31
00:03:56,912 --> 00:03:58,513
Giờ Origami hiện tại đã có
ký ức của Origami của cả hai thế giới.

32
00:03:59,112 --> 00:04:00,513
Um... vậy chuyện này có vấn đề gì sao?

38
00:04:01,112 --> 00:04:09,113
Tớ và "tớ" khác đang bị trộn lẫn ý thức.

39
00:04:10,112 --> 00:04:17,113
Sáng nay ngủ dậy, ý thức của "tớ" kia
cảm thấy rất yên tĩnh.

40
00:04:18,113 --> 00:04:22,113
Vậy nên tớ mới ở đây.

41
00:04:23,113 --> 00:04:24,713
Vậy cậu là nhân cách khác của Origami à?
Như vậy có nghĩa là...
Nhân cách kia... đã biến mất sao?

42
00:04:25,113 --> 00:04:34,113
Không, tớ không nghĩ cô ấy ngủ sâu đến vậy.
Tuy nhiên... tớ không biết làm sao để gọi cô ấy.

43
00:04:35,112 --> 00:04:36,513
Ra là vậy.

39
00:04:36,912 --> 00:04:38,313
Origami giờ đã trưởng thành rồi sao?
Không thể nào, hành động của cô ấy hôm nay
thật đáng xấu hổ.

40
00:04:38,713 --> 00:04:40,113
Một cô gái không có suy nghĩ lạnh lùng
và biến thái về tôi. Đây có phải là Origami không vậy?

41
00:04:40,513 --> 00:04:42,113
Cậu có biết nguyên nhân là gì không?

42
00:04:42,513 --> 00:04:46,113
Nguyên nhân à?　A...

43
00:04:46,712 --> 00:04:48,113
Gì vậy? Cậu nhớ ra gì sao?

39
00:04:48,512 --> 00:04:55,113
Không biết có phải nguyên nhân hay không.
Nhưng gần đây, tớ hay có những giấc mơ kỳ lạ.

40
00:04:55,713 --> 00:04:57,113
Giấc mơ kỳ lạ à?

41
00:04:57,513 --> 00:05:11,113
Ừ... Tớ không biết làm thế nào.
Nhưng tớ cảm thấy như mình đang nói
những điều ước của mình trong giấc mơ.
Phải rồi... Cả "tớ" kia cũng vậy.

47
00:05:11,512 --> 00:05:12,713
Ra là vậy. 

48
00:05:13,112 --> 00:05:15,513
Những giấc mơ là biểu hiện của một ý thức sâu sắc.
Không có gì lạ nếu Origami thay đổi ý thức...

49
00:05:16,112 --> 00:05:18,113
Origami, trong giấc mơ đó, cậu có nói gì
liên quan đến điều ước không?

50
00:05:20,112 --> 00:05:22,113
Origami hiện tại là kết quả
của việc điều ước thành hiện thực.
Nếu điều ước khác được thực hiện,
cậu có thể trở lại trạng thái ban đầu, phải không?

51
00:05:23,112 --> 00:05:30,113
Ra là vậy. Điều ước... của tớ...

52
00:05:30,712 --> 00:05:32,313
Phải, điều ước cậu đã ước hôm qua là gì?

53
00:05:32,912 --> 00:05:41,113
Um... để tớ nhớ lại...
Đó là... "Tôi muốn đáp lễ cho Itsuka-kun!"

56
00:05:42,312 --> 00:05:43,513
Tớ à?

57
00:05:44,112 --> 00:05:50,113
Thật đấy...
Vì "tớ" kia đã ước điều ước kỳ lạ. Nên tớ muốn...

58
00:05:51,112 --> 00:06:03,113
Nhưng đúng là tớ cần trả ơn cậu.
Không chỉ vì chuyện hôm qua, mà cả cha mẹ tớ nữa.
Điều đó khiến tớ quyết định ước như vậy...

42
00:06:04,112 --> 00:06:07,513
Tớ luôn tự hỏi làm sao để trả ơn cậu.

43
00:06:08,112 --> 00:06:09,113
Origami...

44
00:06:09,512 --> 00:06:11,113
Ra là vậy... Vậy quyết định thế nhé.

45
00:06:13,912 --> 00:06:15,513
Cậu có muốn trả ơn tớ ngay bây giờ không?




47
00:06:27,513 --> 00:06:29,113
Hôm nay, chúng ta cùng đi ăn nhé.

48
00:06:32,112 --> 00:06:34,113
Sao vậy? Đâu cần phải nghiêm trọng lên như thế.
Cậu không thích sao?

49
00:06:34,512 --> 00:06:37,113
À... không... Không phải vậy đâu.

50
00:06:38,712 --> 00:06:49,113
Vì tớ đang bị buộc phải xuất hiện.
Tớ tự hỏi liệu cậu có muốn làm gì đó với tớ không.
Chẳng hạn như hình phạt dành cho người giúp việc,
hay đeo một cái vòng cổ...

51
00:06:49,712 --> 00:06:52,113
Cậu nói gì vậy, Origami?
Sao tớ lại yêu cầu mấy thứ đó chứ.

52
00:06:52,512 --> 00:06:58,113
Đúng rồi nhỉ. Tớ xin lỗi...

53
00:06:59,112 --> 00:07:02,513
Eto... Itsuka-kun, cậu muốn ăn gì?

60
00:07:03,512 --> 00:07:05,513
Đúng rồi. Vậy thì hôm nay 
hãy làm món bít tết hamburger nhé.

61
00:07:05,912 --> 00:07:08,513
Một món ăn cổ điển của Đức.
Người nghĩ ra ý tưởng này thật tuyệt.
Tớ thấy ghen tị với ý tưởng đó đấy.

61
00:07:09,112 --> 00:07:16,513
Tớ cũng nghĩ vậy.
Vậy tớ gọi món đó luôn.

65
00:07:17,112 --> 00:07:19,113
Cậu cũng ăn hamburger à?

66
00:07:19,312 --> 00:07:23,513
Ừ, tớ muốn ăn giống như Itsuka-kun...

67
00:07:26,112 --> 00:07:33,113
Không... không có gì đâu...
Nếu cậu chọn món đó vậy là giống hôm qua rồi.

68
00:07:34,112 --> 00:07:35,713
Haha, đúng rồi nhỉ.
Lần sau tớ sẽ chọn món cậu thích.

69
00:07:43,712 --> 00:07:46,113
Đây là gì vậy? Origami trở nên kỳ lạ 
từ khi có đồ ăn. Tôi liếc nhìn cô ấy.
Có vẻ cô ấy không thoải mái.

70
00:07:46,512 --> 00:07:48,513
Này Origami, có chuyện gì vậy?
Hay là cậu không thích cái bánh hamburger này?

71
00:07:49,312 --> 00:07:54,113
Hở? À không có gì đâu. Đừng bận tâm

72
00:07:54,312 --> 00:07:56,113
Origami nói vậy, nhưng cô ấy vẫn có vẻ bồn chồn.

73
00:07:56,512 --> 00:07:58,513
Origami, cậu có gì muốn nói sao?
Cứ nói bất kỳ điều gì cậu muốn đi.

74
00:07:59,112 --> 00:08:05,113
Cậu... cậu có thể hứa không?

75
00:08:06,112 --> 00:08:07,513
Ừ, tớ hứa.

76
00:08:08,512 --> 00:08:17,113
Chuyện là... Nói sao nhỉ... 
Tớ đã hy vọng có đồ ăn dính trên mặt cậu.

85
00:08:19,512 --> 00:08:24,113
Xin lỗi... Tớ... Thật kỳ lạ

86
00:08:25,112 --> 00:08:27,713
Thật ra Origami suy nghĩ như vậy là bình thường.
Nhìn cô ấy khiêm tốn như vậy thật kỳ lạ.

87
00:08:28,112 --> 00:08:30,113
Nhưng tôi không muốn để thế này.
Tôi không muốn bị thiên hạ bàn tán như hôm qua...

88
00:08:30,512 --> 00:08:32,513
Chính xác hơn, Origami bây giờ có thể
đang muốn đi xa hơn nữa.
Tôi không muốn bị để ý như lần trước nữa.

89
00:08:32,912 --> 00:08:34,513
Trước khi nghe thêm điều gì từ cô ấy,
nếu tôi không làm gì thì sẽ rất tệ.

90
00:08:34,912 --> 00:08:37,113
À, phải rồi. Cậu có thể 
ăn bánh hamburger của tớ mà.
Nào... Aaa...

91
00:08:37,512 --> 00:08:41,113
Tớ có thể... làm vậy sao?

92
00:08:42,112 --> 00:08:44,113
Ừ. Dù sao Origami kia cũng muốn vậy mà.

93
00:08:44,512 --> 00:08:52,113
Vậy... Itadakimasu... Aaaa

94
00:08:54,912 --> 00:08:58,113
Um... Ngon quá.

95
00:08:58,512 --> 00:09:00,313
Haha, đều là hamburger cả mà.

98
00:09:00,712 --> 00:09:05,113
Nó có vị của Itsuka-kun...

99
00:09:05,312 --> 00:09:06,113
Hở?

100
00:09:07,112 --> 00:09:14,113
Không, không có gì cả!
Vậy đến lượt tớ nhé.
Itsuka-kun, đây, há miệng ra nào.

101
00:09:15,112 --> 00:09:17,113
Cả tớ nữa sao?
Không, không cần đâu.

102
00:09:17,512 --> 00:09:20,513
Quả nhiên là... cậu không thích rồi...

103
00:09:21,112 --> 00:09:23,513
Không phải như thế...
Aaa... Được rồi. Tớ sẽ ăn mà.

104
00:09:24,112 --> 00:09:28,113
Ừ. Nào, Itsuka-kun, xin mời.

105
00:09:30,912 --> 00:09:33,113
Ừ, ngon thật. Vị Ponzu rất dễ ăn.
(Ponzu: nước sốt làm từ cam hoặc chanh)

106
00:09:33,512 --> 00:09:50,113
Thật kỳ lạ... chúng ta đang làm gì vậy nhỉ?
Nhưng... việc này cũng thú vị.
Đúng như Itsuka-kun nói, làm thế này thật xấu hổ.

107
00:09:50,712 --> 00:09:53,113
Đúng là xấu hổ thật.
Vì chúng ta đang ăn cùng một món mà.
Chẳng có ai trao đổi món ăn giống nhau cả.

108
00:09:53,312 --> 00:09:55,113
Nhưng mà, mình không phủ nhận rằng
việc này cũng rất vui.



112
00:09:59,112 --> 00:10:01,113
Cuối cùng, chúng tôi quá xấu hổ
nên không thể ở lại nhà hàng gia đình được nữa.

113
00:10:01,512 --> 00:10:03,513
Không phải chúng tôi làm phiền 
khách hàng hay nhân viên.
Chỉ là chúng tôi chỉ tự ý thức về điều đó.

114
00:10:04,112 --> 00:10:05,513
Sau khi rời khỏi nhà hàng,
chúng tôi vẫn còn ngại ngùng.
Nhưng có vẻ đi bộ trên con đường này 
cũng giúp chúng tôi nhẹ nhõm hơn một chút 

1
00:10:06,112 --> 00:10:08,113
Itsuka-kun

1
00:10:09,112 --> 00:10:10,713
Sao vậy, Origami?

1
00:10:11,312 --> 00:10:17,113
Tớ không biết nói gì...
nhưng tớ xin lỗi vì chuyện hôm nay.
Xin lỗi vì để cậu vướng vào chuyện của tớ.

1
00:10:18,112 --> 00:10:20,113
Cậu nói gì thế? Đối với tớ, cậu rất quan trọng.
Tớ làm vì tớ muốn vậy mà.

1
00:10:28,512 --> 00:10:30,113
Cậu không sao chứ, Origami?

1
00:10:30,512 --> 00:10:42,113
Tớ cảm thấy lạ lắm.
Tớ hơi mệt, vừa thấy hạnh phúc và lo lắng.
Cho tớ nghỉ một chút là ổn thôi.

1
00:10:43,112 --> 00:10:45,113
Này... cậu trông không khỏe đâu.
Cũng sắp tới nhà tớ rồi. Để tớ đưa cậu về.

368
00:10:46,112 --> 00:10:49,113
Tớ không sao đâu mà...

368
00:10:50,112 --> 00:10:51,513
Cậu còn không đứng nổi nữa kìa.
Không cần phải giấu đâu.






368
00:10:54,712 --> 00:10:56,313
Tôi bế Origami trong khi cô ấy ôm lấy tôi.

368
00:10:56,712 --> 00:11:02,113
Khoan đã, Itsuka-kun.
Tớ có nặng không? Cậu không cần phải bế tớ đâu...

368
00:11:02,712 --> 00:11:04,113
Cậu không nặng đâu. Nếu cậu nặng...

68
00:11:04,912 --> 00:11:06,913
1) Thì Tohka nặng hơn
2) Thì Kaguya nặng hơn
3) Thì Yuzuru nặng hơn
4) Thì Miku nặng hơn

368
00:11:07,112 --> 00:11:08,713
Khoan, nếu tôi nói thế
thì sẽ dẫn đến bad end mất!

368
00:11:08,912 --> 00:11:10,313
Tôi suýt nói ra, nhưng tôi từ bỏ ý định đó!

368
00:11:10,712 --> 00:11:12,113
Nếu tớ nặng thì sao?

368
00:11:13,112 --> 00:11:15,113
Um... cậu biết đấy... Việc của tớ là
chăm sóc các Tinh Linh. Nên chăm sóc cậu
là chuyện đương nhiên mà.

368
00:11:15,512 --> 00:11:21,113
Cậu ngầu đấy, Itsuka-kun.

368
00:11:22,112 --> 00:11:23,713
Có phải do cách tớ nói chuyện không?

368
00:11:24,112 --> 00:11:30,113
Phải, cậu nói vậy nghe thật ngầu.

368
00:11:31,112 --> 00:11:40,113
Xin lỗi nhé. Tớ không muốn gây rắc rối cho cậu...
Không cần làm vậy đâu. Cậu có thể 
đặt tớ xuống được không?





368
00:11:40,912 --> 00:11:42,113
Mình nên làm gì đây?

368
00:11:42,512 --> 00:11:43,513
1) Tớ muốn đụng chạm vào Origami.
2) Đưa Origami về nhà an toàn.

368
00:11:44,112 --> 00:11:46,513
Có vẻ cô ấy sẽ từ chối dù mình có nói gì đi nữa.
Vì vậy mình nên im lặng và đưa cô ấy về nhà.

368
00:11:48,113 --> 00:11:52,113
Um... Itsuka-kun...

368
00:11:53,112 --> 00:11:54,313
Không sao đâu. Cứ để tớ lo.

368
00:11:54,712 --> 00:11:56,113
Tôi ngưng nói và thể hiện cho cô ấy thấy
quyết định của tôi khi đưa cô ấy về nhà.

368
00:11:56,512 --> 00:11:58,113
Nhưng có vẻ thân nhiệt cô ấy ấm lên.
Càng nghĩ về nó, tôi càng cảm thấy ấm hơn.

368
00:12:00,713 --> 00:12:02,113
Chuyện gì vậy?

368
00:12:02,513 --> 00:12:05,513
Không, xin lỗi. Không có gì đâu.

368
00:12:06,112 --> 00:12:08,113
Cô ấy có vẻ không ổn lắm.
Tôi phải sớm đưa cô ấy về.

368
00:12:14,112 --> 00:12:15,513
Cô ấy thở dốc. Cô ấy có sao không vậy?

368
00:12:16,112 --> 00:12:17,513
mlem... mlem...

368
00:12:18,112 --> 00:12:19,513
Hở? Cái gì vậy?

368
00:12:20,112 --> 00:12:23,113
Chỉ... chỉ một chút thôi...

368
00:12:24,112 --> 00:12:25,513
O...Origami... cậu có ổn không đấy?

368
00:12:27,112 --> 00:12:31,113
Tất... tất nhiên là tớ ổn!

368
00:12:31,512 --> 00:12:33,513
Được rồi... Chúng ta sắp đến nhà rồi. Cố lên nhé.

368
00:12:34,112 --> 00:12:36,113
Ừ, cảm ơn cậu.

368
00:12:37,112 --> 00:12:39,513
Cô ấy thật sự không ổn chút nào.
Tôi phải nhanh lên thôi.

368
00:12:43,512 --> 00:12:45,513
Tôi đưa Origami về nhà trong khi
cô ấy càng lúc càng kỳ lạ.




368
00:12:51,112 --> 00:12:52,713
Origami, cậu cảm thấy sao?

368
00:12:53,112 --> 00:12:55,113
Ừ, tớ khá hơn rồi.

368
00:12:56,112 --> 00:12:58,113
Thế thì tốt rồi. Vậy mong muốn
được đáp lễ của cậu đã được
thực hiện rồi phải không?

368
00:12:59,312 --> 00:13:08,113
Có lẽ... tớ đã làm được rồi.
Tớ thắc mắc liệu đây có phải 
do "tớ" kia làm không nữa...

368
00:13:08,512 --> 00:13:11,113
Phải rồi.　Nếu ngày mai vẫn chưa cải thiện được
tình trạng này, cậu cứ gọi cho tớ nhé.

368
00:13:11,512 --> 00:13:13,513
Ừ, tớ xin lỗi.

368
00:13:14,312 --> 00:13:16,113
Không cần phải xin lỗi đâu.
Khi gặp khó khăn, chúng ta phải giúp đỡ nhau mà.

368
00:13:16,312 --> 00:13:18,113
Nhưng câu chuyện đó thật kỳ ​​lạ.
Giấc mơ đó ảnh hưởng đến cả tớ sao?

368
00:13:18,712 --> 00:13:27,513
Ừ. Tớ cũng không nhớ chi tiết.
Tớ không chắc đó có thực sự
là nguyên nhân hay không nữa...

368
00:13:27,912 --> 00:13:29,513
Origami, cậu đã ước điều gì trong giấc mơ vậy?

368
00:13:31,312 --> 00:13:33,113
Thôi nào. Chẳng phải cậu nói 
cậu đã thực hiện điều ước của mình sao?
Những điều ước cậu đã từng ước là gì?

368
00:13:34,112 --> 00:13:35,513
Và người thực hiện điều ước đó là ai?
Chẳng lẽ là thần linh?

368
00:13:36,112 --> 00:13:37,713
Thần linh...

368
00:13:39,112 --> 00:13:46,513
Phải. Chắc chắn là đã có ai đó ở đó...
Làm sao có thể thực hiện điều ước được.

368
00:13:47,112 --> 00:13:51,113
Xin lỗi nhé. Tớ không thể nhớ rõ nữa.

368
00:13:51,912 --> 00:13:53,713
Vậy à? Nhưng những giấc mơ thường là vậy.

368
00:13:54,112 --> 00:13:56,113
Ngay cả tớ mỗi khi thức dậy,
theo thời gian, nội dung của giấc mơ
dần dần bị quên mất.

368
00:13:56,312 --> 00:13:58,113
Nhưng vì lý do nào đó,
khi thấy một giấc mơ vui vẻ,
tớ vẫn nhớ mơ hồ và cảm thấy đáng tiếc

368
00:13:58,512 --> 00:14:02,113
Chắc chắn là vậy rồi.

368
00:14:02,512 --> 00:14:05,113
Dù sao cậu cứ nghỉ ngơi
cho đến khi bình tĩnh là được.
Nếu cậu cảm thấy mệt,
cứ nói Kotori sắp xếp xe cho cậu là được.

368
00:14:06,112 --> 00:14:09,113
Cảm ơn nhé, Itsuka-kun.

368
00:14:10,112 --> 00:14:15,513
Đến hiện giờ, cậu vẫn ổn chứ?

368
00:14:16,112 --> 00:14:18,113
Hở? Sao vậy?

368
00:14:18,312 --> 00:14:31,113
Từ giờ đến ngày mai, "tớ" kia có thể
sẽ trở lại. Trong khi chúng ta
còn thời gian... Cái gối đó...

368
00:14:31,312 --> 00:14:32,513
Cái gối nào?

368
00:14:33,112 --> 00:14:37,113
Không, không có gì đâu...
Tớ quên mất...

368
00:14:38,112 --> 00:14:39,513
Ừ... Chúc cậu may mắn

368
00:14:42,112 --> 00:14:44,113
I... Itsuka-kun!

368
00:14:45,712 --> 00:14:47,513
Thôi nào, hãy nằm xuống đi.
Cậu không khỏe lắm đâu.

368
00:14:48,112 --> 00:14:50,113
Ừ... ừ...

368
00:14:52,112 --> 00:14:57,513
Không ổn rồi... Giấc ngủ... đang đến...






368
00:15:05,112 --> 00:15:13,513
Một cơ thể, hai trái tim.
Hai điều ước của một cô gái bé nhỏ,
hai điều ước đã được hoàn thành.

368
00:15:14,112 --> 00:15:20,113
Và lần này, điều ước đã làm thay đổi
diện mạo của cô gái như thế nào?

368
00:15:28,112 --> 00:15:34,113
Chào mừng ngài đã đến buổi tối hôm nay.
Ngài có hài lòng về điều ước không?

368
00:15:35,512 --> 00:15:37,113
Ren-san...

368
00:15:38,912 --> 00:15:40,113
Vâng?

368
00:15:41,112 --> 00:15:51,113
Nó như một giấc mơ vậy.
Một trải nghiệm... rất kỳ lạ.

368
00:15:51,912 --> 00:15:58,113
Tôi chỉ có thể biết mọi chuyện qua ý thức.
Bây giờ tôi lại còn được xuất hiện ở ngoài.

368
00:15:58,712 --> 00:16:03,113
Đó cũng giống như một giấc mơ vậy đấy.

368
00:16:04,112 --> 00:16:12,113
Vậy đây có phải là năng lực của Ren-san không?
Tôi đã thấy mọi chuyện trở nên rõ ràng hơn.

368
00:16:13,112 --> 00:16:23,113
Ngài nói đúng. Ngài muốn đáp lễ cho Shidou-sama.
Và tôi đã hoàn thành điều ước của ngài.

368
00:16:24,112 --> 00:16:31,113
Chính ngài đã ước điều ước đó,
không phải là Origami-sama khác đâu.

368
00:16:34,112 --> 00:16:44,113
Hãy yên tâm đi.
Điều ước thứ hai đã thành hiện thực.
Một Origami-sama khác sẽ sớm thức dậy.

368
00:16:44,512 --> 00:16:58,113
Giờ chúng ta quay lại vấn đề chính
Ngài chỉ còn một điều ước cuối cùng.
Nào, điều ước của ngài là gì?

368
00:16:59,112 --> 00:17:01,113
Tôi...





368
00:17:04,512 --> 00:17:06,913
Bản gốc (origin) hồi sinh rồi đây.

368
00:17:09,512 --> 00:17:17,113
Vì tôi không được ước điều ước thứ hai,
tôi phải ước điều ước cuối cùng.

368
00:17:17,312 --> 00:17:28,113
Không... điều ước của tôi là bộc phát thôi...
Cô còn muốn gì nữa? Lại bị giam cầm
hay người giúp việc nữa à?

368
00:17:28,512 --> 00:17:35,113
Đừng lo lắng. Tôi đã có rất nhiều thời gian
để suy nghĩ. Cô sẽ nghe rất thuyết phục.

368
00:17:35,512 --> 00:17:41,113
Vậy... vậy sao? Vậy điều ước là gì?

368
00:17:42,112 --> 00:17:45,113
Tôi sẽ làm đồ nội thất của Shidou.

368
00:17:45,512 --> 00:17:51,113
DỪNG LẠI! Nghe đến đó là tôi hiểu rồi!

368
00:17:52,112 --> 00:17:56,113
Sao nữa? Cô là bà già à?

368
00:17:56,512 --> 00:18:02,113
Không, câu chuyện về "đồ nội thất" 
nghe không bình thường chút nào...

368
00:18:03,112 --> 00:18:11,113
Rõ ràng có một sự hiểu lầm.
Tôi là một cái ngăn kéo
cất đồ đạc cá nhân của Itsuka-kun.
Tôi không nói tôi muốn lưu trữ nó đâu nhé.

368
00:18:12,112 --> 00:18:19,113
Bất cứ thứ gì, thú cưng hoặc một chiếc ghế,
đều là vì lợi ích của Shidou.

368
00:18:20,112 --> 00:18:25,113
Thế thì sao chứ?
Nó chẳng khác gì các điều ước khác gì cả!

368
00:18:26,112 --> 00:18:30,113
Nhưng... điều ước của cô là gì?

368
00:18:31,112 --> 00:18:39,113
Đó là... tôi chỉ muốn Itsuka-kun
được khỏe mạnh thôi... vậy được chứ?

368
00:18:40,112 --> 00:18:48,513
Điều đó cũng quan trọng đấy, 
nhưng nó hơi trừu tượng. Cô nên nói cụ thể hơn.

368
00:18:49,112 --> 00:18:52,513
Đó là...

368
00:18:53,512 --> 00:19:01,113
Vậy là đồ nội thất là tốt nhất.
Origami là giấc mơ. Origami là thú cưng.
Origami là mọi thứ.

368
00:19:02,112 --> 00:19:05,113
Nhưng tôi không thích nó.





368
00:19:08,512 --> 00:19:15,113
Lần này có vẻ rất khó khăn đấy.
Các ngài đã quyết định được chưa?

368
00:19:16,112 --> 00:19:24,113
Xin... xin lỗi. Tôi vẫn chưa quyết định được.
Cô có thể cho tôi thêm thời gian không?

368
00:19:25,112 --> 00:19:34,113
Trong trường hợp này, ngài có thể nói ra
mong muốn của ngài được không?
Có thể người kia sẽ đổi ý.

368
00:19:35,112 --> 00:19:46,113
Phải rồi nhỉ...
Tôi chỉ muốn Itsuka-kun được khỏe mạnh.
Đó là điều ước của tôi.

368
00:19:47,112 --> 00:19:55,113
Tôi cũng có ý tưởng khác.
Chúng tôi sẽ luôn giữ liên lạc với nhau.

368
00:19:56,112 --> 00:20:07,513
Ra là vậy. Có vẻ điều ước lần này đã khác.
Cả hai dường như đều vì tình cảm dành cho Shidou

368
00:20:08,513 --> 00:20:21,113
Vậy đơn giản thôi.
Origami-sama và Origami-sama khác.
Các ngài chỉ cần hỏi Shidou-sama
điều ước nào tốt hơn là được.

368
00:20:22,113 --> 00:20:25,113
Có thể làm được như vậy sao?

368
00:20:26,113 --> 00:20:34,113
Vâng. Nếu Origami-sama khao khát mạnh mẽ đến vậy,
ngài có muốn làm rõ điều ước này không?

368
00:20:35,112 --> 00:20:45,113
Có chứ, tôi đang rất bối rối.
Vì vậy, tôi muốn thử cả 2 điều ước
và nhận được câu trả lời từ Shidou-kun.

368
00:20:46,112 --> 00:20:54,113
Mong muốn được chấp nhận. Điều ước cuối cùng
là một điều ước không thể đoán trước được.

368
00:20:55,112 --> 00:20:59,113
Cảm... cảm ơn cô...

368
00:21:00,112 --> 00:21:07,113
Vậy xin chào ngài.
Hẹn gặp lại ngài vào một dịp khác!

368
00:21:18,112 --> 00:21:23,113
Như vậy... là đã ổn... chưa nhỉ?
