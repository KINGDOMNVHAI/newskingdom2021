﻿0
00:00:05,112 --> 00:00:06,313
Waaa...

1
00:00:06,912 --> 00:00:09,113
Trời sáng rồi.
Hôm nay tôi không bị gọi dậy.
Tôi không ngủ quên đâu nhé!

2
00:00:09,512 --> 00:00:14,113
Vẫn kịp giờ phải không?
Làm bữa sáng nhanh nào!

3
00:00:17,112 --> 00:00:19,113
Cái gì đây? Một cái hộp à?

4
00:00:19,312 --> 00:00:21,313
Đúng vậy. Một chiếc hộp nhỏ
được đặt ở đầu giường.

5
00:00:21,712 --> 00:00:23,713
Một chiếc hộp nhỏ với họa tiết con rắn
nằm gọn trong lòng bàn tay.
Tôi không hay biết gì khi ngủ.

6
00:00:24,112 --> 00:00:26,313
Con rắn trông có vẻ rùng rợn.
Đó có phải là biểu tượng xấu xa của ai đó không?

7
00:00:26,512 --> 00:00:27,513
Không. Có khi ai đó để quên chăng?
Mình nên hỏi lại.

8
00:00:27,912 --> 00:00:29,713
Sau khi thay quần áo, tôi rời khỏi phòng.





9
00:00:35,912 --> 00:00:37,313
Kotori, em dậy chưa?
Anh xin lỗi. Anh ngủ quên...

10
00:00:37,912 --> 00:00:40,313
Nhắc mới nhớ, Kotori không đến đánh thức tôi dậy.
Em ấy đi đâu chăng?

11
00:00:40,912 --> 00:00:42,313
Nhìn kỹ, có một sổ ghi chú trên bàn. Cái gì vậy?

12
00:00:42,312 --> 00:00:44,313
"Em đến Fraxinus nên em phải đi trước.
Em không gọi anh dậy vì không có thời gian.
Không biết anh có bị trễ giờ không?
Kotori" 

13
00:00:44,912 --> 00:00:46,513
Hahaha... Con bé đi trước rồi à?
Mình không thể đi muộn được.




14
00:00:52,112 --> 00:00:54,113
Được, chiếc hộp
rắc rối đang được đặt trên bàn.

15
00:00:54,512 --> 00:00:55,713
Origami dường như đã đến rồi.
Cậu ấy đến sớm thật đấy.

16
00:00:56,112 --> 00:00:57,713
Suy nghĩ kỹ thì 
Origami cũng có khả năng gửi chiếc hộp đó.

17
00:00:58,112 --> 00:00:59,713
Người có thể vào phòng tôi vào ban đêm.
Khả năng cao là Origami.

18
00:01:00,112 --> 00:01:01,513
Và có thể Origami đã để quên nó ở lại.
Tôi nghĩ mình không sai đâu.

19
00:01:01,712 --> 00:01:03,513
Tôi cảm thấy như mình bị tê liệt.
Một cô gái nhà lành lại đi 
lẻn vào phòng con trai vào nửa đêm.




20
00:01:05,312 --> 00:01:07,113
Chào buổi sáng, Origami

21
00:01:08,112 --> 00:01:09,513
Chào buổi sáng

22
00:01:10,112 --> 00:01:12,113
Tớ có chuyện này muốn hỏi.
Origami, cái hộp này...

23
00:01:14,112 --> 00:01:16,513
Không một chút phản ứng.
Biểu cảm không thay đổi nhiều.
Nhưng cậu có cảm thấy xấu hổ với lương tâm không?

24
00:01:17,112 --> 00:01:20,113
Cảm ơn cậu. Tớ có thể mở nó không?

25
00:01:21,112 --> 00:01:22,513
Ể? Ừ, tớ không phiền đâu.

25
00:01:29,712 --> 00:01:31,113
Vừa rồi... là gì vậy?

25
00:01:34,112 --> 00:01:35,113
Ể? Aa... sao cậu lại thở dài?

25
00:01:35,712 --> 00:02:37,113
Thứ vừa rồi... chỉ một mình tôi thấy sao?

25
00:01:38,112 --> 00:01:41,113
Ra là vậy, tớ hiểu rồi.

25
00:01:42,512 --> 00:01:44,113
Cậu hiểu gì cơ?

25
00:01:45,112 --> 00:01:54,113
Cậu tặng món quà này cho tớ với ý nghĩa:
đây là thứ lưu trữ kỷ niệm của hai đứa.

25
00:01:55,112 --> 00:02:02,513
Thật lãng mạn.
Tớ sẽ để vào đây một tấm hình của chúng ta.
Không biết có vừa không nhỉ.

25
00:02:03,512 --> 00:02:06,113
Trí tưởng tượng vẫn phong phú như thường lệ...
Nhưng khoan, cậu đã từng chụp hình với người khác sao?

26
00:02:07,112 --> 00:02:12,113
Đúng vậy. Vì lý do nào đó,
tớ có rất nhiều ảnh nhìn vào máy ảnh




26
00:02:13,112 --> 00:02:15,113
Nghe giống selfie thì đúng hơn...

26
00:02:15,712 --> 00:02:24,113
Tất nhiên là có nhiều người khác trong ảnh.
Và có cậu xuất hiện ở đằng xa.

26
00:02:25,112 --> 00:02:26,713
Vậy à? Nhưng khoan! không phải vậy!
Vậy nghĩa là cậu không để
chiếc hộp này ở nhà tớ sao?

26
00:02:27,512 --> 00:02:36,113
Giờ tớ đã nhận chiếc hộp này từ cậu,
vậy nên nó đã là của tớ.
Đóng gói những kỷ niệm với Shidou
còn quan trọng hơn cuộc sống này.

26
00:02:36,512 --> 00:02:38,113
Chà... tôi phải giải quyết hiểu lầm này
càng sớm càng tốt.

26
00:02:38,512 --> 00:02:41,113
Vậy thì... cậu có thể trả lại tớ không?
Tớ không thể cho nó được
vì nó có thể thuộc về người khác.
Xin lỗi vì sự hiểu lầm.

26
00:02:42,112 --> 00:02:48,113
Xin lỗi, tớ cũng đã sai rồi.
Trả lại cậu này.

26
00:02:49,112 --> 00:02:50,113
Ừ... ừ...

26
00:02:50,512 --> 00:02:52,113
Tôi thấy Origami thật chững chạc.
Không hiểu sao tôi lại băn khoăn về điều đó.

26
00:02:52,512 --> 00:02:54,113
Có lẽ do tôi sợ làm mất lòng cô ấy.
Nhưng cô ấy giữ khoảng cách thật tuyệt.

26
00:02:54,312 --> 00:02:56,513
Tớ... tớ không biết dùng từ nào để nói cả...
Bữa nào tớ mời cậu đi ăn để xin lỗi nhé.

27
00:02:57,512 --> 00:03:01,113
Thật không? Vậy ngày mai thì sao?

28
00:03:01,312 --> 00:03:03,513
Sớm quá vậy!
Nhưng nếu trễ thì không thành ý lắm.
Đi sớm sẽ tốt hơn.

29
00:03:03,912 --> 00:03:06,513
Ngày mai cũng không sao.
Nhưng tớ không đi được những chỗ giá cao đâu.
Đừng kỳ vọng quá nhiều.

35
00:03:07,112 --> 00:03:12,513
Được đi chơi hai người cùng Shidou
là điều tớ mong chờ nhất rồi.

36
00:03:12,712 --> 00:03:14,113
Được... Được rồi.
Vậy ngày mai nhé.

37
00:03:16,512 --> 00:03:19,113
Đúng là rắc rối. Không biết đi ăn 
hai người liệu có ổn không?

38
00:03:19,512 --> 00:03:21,113
Nhưng nếu chiếc hộp
không phải của Origami thì là của ai?

39
00:03:21,312 --> 00:03:23,513
Khi hỏi ai đó về việc này,
tôi phải đảm bảo là không bị hiểu nhầm
thành quà tặng họ.

40
00:03:38,913 --> 00:03:40,113
Nghỉ trưa rồi. Hôm nay ăn gì đây?

41
00:03:42,513 --> 00:04:46,113
Shidou, đến giờ ăn trưa rồi!

42
00:03:46,113 --> 00:03:48,513
Hôm qua anh không có thời gian làm.
Em có thể đến căn-tin trường được không?
Xin lỗi, gần đây anh không làm bento.

43
00:03:49,112 --> 00:03:53,113
Anh nói gì vậy? Em không để ý đâu.

39
00:03:54,112 --> 00:04:02,113
Nhưng nó khác lắm. Cơm bento ngon hơn. 
Em lúc nào cũng muốn ăn nó.

40
00:04:02,513 --> 00:04:09,113
Đó là lý do em không trách anh
vì không làm bento.

41
00:04:10,113 --> 00:04:16,113
Không, chuyện trách móc này thật kỳ lạ.
Em nên nói sao đây?

42
00:04:16,913 --> 00:04:19,113
Không sao đâu, anh hiểu rồi.
Cảm ơn Tohka, em thật tốt.

43
00:04:19,912 --> 00:04:26,113
Vậy sao? Nếu vậy, em sẽ càng tốt hơn vì anh.

39
00:04:26,912 --> 00:04:29,113
Haha, cảm ơn em nhé.
Nhân tiện, Origami thì sao?
Cậu có muốn đến căn-tin không?

40
00:04:31,913 --> 00:04:34,113
Tớ sẽ kiềm chế.

41
00:04:34,913 --> 00:04:36,113
Chuyện gì vậy? Cậu đâu có bệnh gì đâu.

42
00:04:36,913 --> 00:04:45,113
Tớ muốn ăn thật ngon với Shidou vào ngày mai
Vậy nên từ bây giờ tớ sẽ nhịn.

43
00:04:46,112 --> 00:04:53,113
Gì cơ? Cô nhịn ăn một bữa
mà không chết sao, Origami?

44
00:04:54,112 --> 00:05:00,113
Đó không phải vấn đề lớn.
Một người có thể sống 7 ngày chỉ uống nước.

45
00:05:01,112 --> 00:05:06,913
Gì... gì cơ?
Cơ thể con người tuyệt vời vậy sao?

46
00:05:07,112 --> 00:05:09,113
Không, Tohka cũng đã từng sống mà không ăn cơm
trước khi được tôi hướng dẫn về món ăn mà.

47
00:05:09,512 --> 00:05:10,713
Phải, đó là câu chuyện hiện tại.

48
00:05:10,912 --> 00:05:13,113
Origami, đừng bỏ bữa chỉ vì đi ăn với tớ.
Nếu cậu không có thể trạng tốt
thì không thể ăn ngon được, đúng không?

49
00:05:13,912 --> 00:05:17,113
Tớ hiểu rồi. Tớ nghĩ cậu nói đúng

50
00:05:18,712 --> 00:05:26,113
Thế là tốt đấy. Cảm giác đói khó chịu lắm.
Nhân tiện, Shidou...

51
00:05:27,112 --> 00:05:28,313
Gì vậy?

52
00:05:29,912 --> 00:05:36,513
Origami vừa nói mai sẽ đi ăn với anh.
Thế nghĩa là sao?

53
00:05:37,112 --> 00:05:39,113
Chỉ là hiểu lầm thôi.
Anh chỉ ăn cơm với Origami
để bù đắp cho cậu ấy.

54
00:05:40,112 --> 00:05:48,113
Vậy sao? Bù đắp à? Hmm, em hiểu rồi.

55
00:05:48,912 --> 00:05:56,113
Em cũng muốn Shidou ăn cơm cùng
để bù đắp cho Shidou.

56
00:05:57,112 --> 00:06:06,113
Ồ, phải rồi. Em muốn bù đắp cho anh
vì anh đã đối xử tốt với em.

58
00:06:06,912 --> 00:06:09,513
Haha, cảm ơn nhé Tohka.
Lần này Origami làm trước.
Lần tiếp theo nhờ em đấy.

59
00:06:10,112 --> 00:06:12,113
Cứ để cho em.

41
00:06:14,512 --> 00:06:16,113
Vậy cậu có đi xuống
căn-tin trường không, Origami?

42
00:06:16,512 --> 00:06:21,113
Hôm nay, tớ sẽ để cậu ăn cùng Tohka.

43
00:06:22,112 --> 00:06:25,113
Thế có được không?

44
00:06:26,112 --> 00:06:37,113
Tôi không biết nữa.
Tôi sẽ ăn, nhưng ăn tiết kiệm
để tăng thời gian ăn với Shidou.

45
00:06:37,712 --> 00:06:39,513
V... Vậy sao? Tớ không nói là không thể, 
nhưng hãy ăn uống hợp lý.
Nếu không cậu sẽ bị ốm đấy.

46
00:06:40,112 --> 00:06:42,113
Tớ sẽ chú ý

47
00:06:42,513 --> 00:06:44,513
Origami, cô ấy giờ đã nhường Tohka...
Tôi rất mừng khi cô ấy thay đổi.





57
00:06:54,512 --> 00:06:55,513
Anh về rồi.

58
00:06:56,112 --> 00:07:01,113
Mừng anh đã về.
Ara, anh đã làm việc gì tốt à?

59
00:07:02,112 --> 00:07:03,513
Sao em hỏi vậy?

60
00:07:04,112 --> 00:07:09,113
Em không biết. Em chỉ thấy anh có vẻ vui.

61
00:07:10,112 --> 00:07:12,513
Cũng không có gì tốt lắm đâu.
Chỉ là ngày mai anh sẽ đi ăn với Origami.
Hẹn hò... đại loại vậy.

62
00:07:13,112 --> 00:07:20,113
Thế à? Mà cũng đúng. Hẹn hò đều đặn 
với Tinh Linh là rất quan trọng.

63
00:07:21,112 --> 00:07:30,113
Nhưng hãy cẩn thận. Chúng ta vẫn chưa biết rõ.
Origami là một trong những Tinh Linh
quan trọng nhất cần để ý lúc này.

64
00:07:31,112 --> 00:07:38,113
Mặc dù anh đã giải quyết,
nhưng em lại nghĩ nhiều về điều đó.
Anh làm vậy đã tạo ra một lịch sử sai lệch.

65
00:07:38,512 --> 00:07:40,513
Đúng vậy, Origami hiện tại
có một lịch sử phức tạp...

66
00:07:40,512 --> 00:07:42,713
Nhưng ngược lại, đó có thể là một cuộc gặp gỡ tốt.
Anh muốn nói chuyện nhiều hơn
với Origami hiện giờ.

67
00:07:43,112 --> 00:07:44,513
Những gì anh có thể làm
là làm cách như mọi ngày... hẹn hò.

68
00:07:45,112 --> 00:07:53,113
Anh hiểu là tốt rồi.
Mai em sẽ đến Ratatoskr làm việc.
Origami giao cho anh đấy.

69
00:07:54,112 --> 00:08:01,113
Nếu có chuyện gì, hãy báo em ngay.
Em sẽ giúp hết sức có thể.

70
00:08:01,512 --> 00:08:03,513
Cám ơn em. Anh sẽ cố gắng quan tâm đến họ
nhiều nhất có thể.

71
00:08:04,112 --> 00:08:08,113
Chúc anh may mắn. À, còn nữa.

72
00:08:09,112 --> 00:08:10,113
Gì vậy?

87
00:08:11,112 --> 00:08:21,113
Ngay cả khi sức mạnh của Origami vẫn bình thường,
nếu Shidou gặp nguy hiểm, hãy gọi cho em.
Đặc biệt cẩn thận khi hai người vào phòng kín.

88
00:08:22,112 --> 00:08:27,113
Khả năng đó là có thể.
Em sẽ chuẩn bị giải pháp khác với thường lệ.

89
00:08:28,112 --> 00:08:29,113
Ừ, đúng vậy.

90
00:08:37,712 --> 00:08:54,113
Giờ thì, đây là giấc mơ, thực tế hay ảo ảnh?
Một thế giới tự do, nơi mọi thứ trở thành sự thật.
Điều ước hôm nay sẽ được thực hiện...

98
00:09:02,912 --> 00:09:11,113
Cô đã tỉnh dậy rồi.
Vị khách của chúng ta hôm nay là... Tobiichi Origami

99
00:09:12,112 --> 00:09:14,113
Cô là ai?

100
00:09:15,112 --> 00:09:21,113
Thật thất lễ khi đánh thức cô
vào tối muộn thế này.

101
00:09:22,112 --> 00:09:27,113
Tôi là cư dân của thế giới này 
và là một người xây dựng giấc mơ. (Dream Contractor)

102
00:09:27,512 --> 00:09:35,113
Một chú hề đáng thương 
như một hướng dẫn viên của hy vọng.
Tên tôi là Ren. Hãy nhớ lấy.

103
00:09:36,112 --> 00:09:40,113
Ren...　người xây dựng giấc mơ.

104
00:09:41,912 --> 00:09:53,113
Chính xác!
Nhờ ngài, tôi mới thoát khỏi phong ấn.
Lòng biết ơn này, tôi không thể diễn tả bằng lời.

105
00:09:54,112 --> 00:10:05,113
Do đó, với năng lực của bản thân, 
tôi có thể ban cho ngài 3 điều ước
để tỏ lòng biết ơn.

114
00:10:06,112 --> 00:10:07,513
Điều ước sao? (願い - negai - muốn)

1
00:10:08,112 --> 00:10:21,113
Đúng vậy! Xin đừng ngại mà hãy nói cho tôi biết.
Không quan trọng giấc mơ đó có vô lý thế nào.
Hãy thể hiện mong ước của bản thân đi.

1
00:10:22,112 --> 00:10:31,113
Chỉ cần một câu nói thầm,
mọi thứ sẽ đúng theo ý ngài!
Nào, điều ước của ngài là gì?




1
00:10:33,112 --> 00:10:36,113
Điều ước... của tôi...

1
00:10:40,112 --> 00:10:45,113
Chuyện này là sao?
Mình đang mơ à?

1
00:10:46,112 --> 00:10:56,113
Nhưng đây có thể chỉ là giấc mơ.
Nếu có điều ước tôi muốn thực hiện,
mình muốn biến Shidou trở thành thú cưng...

1
00:10:57,112 --> 00:10:59,113
Đợi đã!!!!

1
00:11:00,112 --> 00:11:06,113
Cô là... "Tôi"? Sao cô lại ra đây?

368
00:11:07,112 --> 00:11:12,113
Tôi ra để ngăn "Tôi" lại!
Đừng có ước điều ước như thế chứ!

368
00:11:13,112 --> 00:11:14,513
Tại sao?

368
00:11:15,112 --> 00:11:16,513
Tại sao nữa à?

368
00:11:17,112 --> 00:11:25,113
Shidou với đôi tai dễ thương,
vẫy vẫy làm nũng một cách ngây thơ.

68
00:11:25,712 --> 00:11:29,113
"Tôi" không thấy mình vô vị quá sao?

368
00:11:32,112 --> 00:11:39,113
Không được! Cô không thể tự ý 
biến cậu ấy thành thú cưng như vậy!

368
00:11:40,112 --> 00:11:48,113
Vậy cô gái tên Ren này là ai?
Cô ấy sẽ làm gì để biến điều ước của cô thành hiện thực?

368
00:11:49,112 --> 00:11:56,113
Dù là mơ đi nữa, thế này là rất bất thường.
Cô nên cẩn thận.

368
00:11:56,912 --> 00:12:07,113
Nhìn giấc mơ này, có lẽ nó liên quan
sâu sắc đến ý thức. Nếu cô ước điều gì kỳ lạ,
ngay cả với chính mình, nó cũng được thực hiện phải không?

68
00:12:07,912 --> 00:12:15,113
Vì vậy, nếu cô quan tâm đến
sức khỏe của Itsuka-kun,
Tôi nghĩ cô nên ước được hòa bình.

368
00:12:15,912 --> 00:12:24,113
Và trên hết... cô sẽ có thêm thời gian
ở bên cạnh Itsuka-kun.

368
00:12:25,112 --> 00:12:31,113
Uh, "Tôi" nói đúng

36
00:12:32,112 --> 00:12:34,113
Cô đã hiểu rồi à?

368
00:12:35,112 --> 00:12:41,513
Thay vì biến Shidou thành thú cưng,
có lẽ tôi nên là thú cưng của Shidou

368
00:12:42,512 --> 00:12:45,113
Cô có nghe tôi nói không đấy?

368
00:12:46,112 --> 00:12:58,113
Có chứ. Mỗi ngày được Shidou cho ăn.
Thôi nào Origami. Đó là một người bạn đời.
Tôi sẽ được ở cạnh Shidou. Tôi muốn nói điều ước này.

368
00:12:59,112 --> 00:13:05,113
Điều ước đó có ổn không đấy?
Cô sẽ hoàn toàn bị xem là một con chó đấy!

368
00:13:05,912 --> 00:13:20,113
Nhưng phần thưởng rất xứng đáng.
Tôi có thể chịu đựng được, không sao cả. 
Đồ ăn vẫn ổn mà.

368
00:13:21,112 --> 00:13:25,113
Tôi không nghĩ Itsuka-kun sẽ nói vậy đâu...

368
00:13:26,112 --> 00:13:35,113
Đó là lý do tại sao tôi cần yêu cầu 
một cách thẳng thắn. Tôi muốn vậy
vì nó rất ổn thỏa, vẹn cả đôi đường.

368
00:13:36,112 --> 00:13:38,113
Nhưng... nhưng mà...

368
00:13:38,712 --> 00:13:44,113
Đến giờ cô không hiểu những gì tôi nói sao?
Đúng là một con mèo con không hiểu chuyện gì cả.

368
00:13:47,112 --> 00:13:49,113
Cô còn muốn nói gì nữa không?

368
00:13:50,112 --> 00:13:53,113
Đó...

368
00:13:57,512 --> 00:14:05,113
Nhưng... nhưng mà... nhưng mà...
Tôi không thể tưởng tượng ra cảnh đó...

368
00:14:09,112 --> 00:14:10,513
Quyết định rồi.

368
00:14:11,112 --> 00:14:20,113
Tôi đã thấy nó.
Một sự xung đột trong thời gian dài.
Ngài đã thống nhất rồi à?

368
00:14:21,512 --> 00:14:23,113
Sao cô biết?

368
00:14:24,112 --> 00:14:39,313
Trong ngài, tôi có thể cảm thấy
sự tồn tại của một ý thức khác.
Hai người... Ngài có một trái tim kỳ lạ.

368
00:14:40,112 --> 00:14:51,113
Đó là những gì tôi muốn nói.
Ngài và người kia đã hội ý những gì?
Tôi không thể biết được.

368
00:14:52,112 --> 00:15:02,513
Chính vì vậy... Nào, điều ước của ngài.
Từ miệng của ngài, lời nói của ngài,
xin hãy cho tôi biết điều ước đó.

368
00:15:03,712 --> 00:15:07,113
Điều ước của tôi... là...

368
00:15:07,512 --> 00:15:09,513
Là...

368
00:15:10,112 --> 00:15:13,113
Làm thú cưng của Shidou.

368
00:15:16,112 --> 00:15:18,113
Làm thú cưng của Shidou.

368
00:15:19,112 --> 00:15:33,113
Không, tôi nghe thấy rồi.
Ra là vậy, ra là vậy. Đó là một điều ước sâu sắc.
Điều ước của ngài, tôi đã nghe thấy rồi.

368
00:15:39,112 --> 00:15:41,113
Cảm ơn cô.

368
00:15:42,112 --> 00:15:54,113
Ngài không cần cảm ơn đâu.
Hãy tận hưởng mỗi ngày với tâm nguyện của ngài.
Với điều này, chúng ta sẽ gặp lại nhau!

368
00:16:02,712 --> 00:16:08,113
Tận hưởng mỗi ngày...
Nếu đây là một giấc mơ có thật.

