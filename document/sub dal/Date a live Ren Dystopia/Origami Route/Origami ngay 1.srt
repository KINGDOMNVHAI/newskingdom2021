116
00:00:02,712 --> 00:00:03,713
Đi đến Khu Mua Sắm
1) Đồng ý
2) Không đồng ý

116
00:00:08,312 --> 00:00:10,313
Khu mua sắm nằm trên đường đi học về.
Có rất nhiều loại cửa hàng trong thị trấn.
Đây là một địa điểm tốt.

116
00:00:10,712 --> 00:00:12,313
Khi đến nơi, đợt bán hàng đặc biệt sẽ bắt đầu.
Sự phấn khích này khó có thể diễn tả
trừ khi có mặt ở đó. Đây là nơi tuyệt vời của tôi.

116
00:00:12,912 --> 00:00:15,113
Siêu thị rất tốt, nhưng có nhiều cửa hàng tư nhân 
trong khu mua sắm, có giá rẻ và sản phẩm
chính hãng, mùi vị cũng rất ổn.

116
00:00:15,312 --> 00:00:17,713
Hôm nay có rất nhiều người.
Vì vậy tôi cần mua nhiều hơn.
Đây giống như một gia đình vậy.

116
00:00:17,912 --> 00:00:19,513
Nhanh mua đồ rồi về thôi.

116
00:00:20,912 --> 00:00:22,113
Um... Tối nay có bao nhiêu người nhỉ?
Kotori và mọi người thì chắc chắn rồi. 
Miku và Origami...

116
00:00:24,912 --> 00:00:26,113
Cậu gọi tớ à?

116
00:00:26,512 --> 00:00:28,513
Waa! O... Origami, cậu đến từ lúc nào vậy?

116
00:00:29,112 --> 00:00:31,513
Tình cờ thôi. Cậu cũng đi mua sắm à?

116
00:00:32,912 --> 00:00:34,713
Tớ chỉ mua đồ nấu cơm thôi.
Cậu cũng vậy à?

116
00:00:35,112 --> 00:00:39,113
Đúng vậy, tớ đang đi đến thiên đường

116
00:00:39,712 --> 00:00:41,113
Hể, vậy à?

116
00:00:41,912 --> 00:00:47,113
Đúng vậy. Đó là kỷ niệm của thế giới trước.
(thế giới của Origami tóc dài)

116
00:00:47,912 --> 00:00:48,713
À, ra là vậy...

116
00:00:49,112 --> 00:00:50,913
Chính xác hơn, thế giới này hơi khác 
so với thế giới trước.

116
00:00:51,112 --> 00:00:53,313
Kurumi có khả năng đưa tôi về quá khứ.
Bằng cách cứu ba mẹ của Origami,
thế giới sau đó đã thay đổi vì tôi.

116
00:00:54,712 --> 00:00:56,113
Và rồi, Origami trước mặt tôi
đang có ký ức kết hợp giữa Origami thế giới cũ
và Origami của thế giới mới.

116
00:00:56,312 --> 00:00:58,513
Vì vậy, đây không phải nơi cô ấy thích
ở thế giới này. Nó giống như kỷ niệm
mà cô ấy đã từng biết ở thế giới trước.
Một tình huống kỳ lạ.

116
00:00:59,112 --> 00:01:09,513
Tớ biết người chủ cửa hàng,
nhưng người chủ không biết tớ.
Tớ không phàn nàn gì cả. Tiếc thật.

116
00:01:09,912 --> 00:01:12,313
Ra là vậy... Chuyện đó không thể tránh khỏi.
Vậy cậu định mua gì vậy?

116
00:01:13,112 --> 00:01:14,513
Nguyên liệu nấu ăn.

116
00:01:15,112 --> 00:01:17,113
Nguyên liệu nấu ăn dễ mua mà...?

116
00:01:17,912 --> 00:01:19,513
Đó là một bài thuốc.

116
00:01:20,112 --> 00:01:22,113
A... tớ hiểu rồi.　Trông như thuốc Bắc vậy. 
Nhưng không phải Origami hôm nay đến ăn cùng bọn tớ sao?

116
00:01:23,112 --> 00:01:24,113
Đi chứ.

116
00:01:24,912 --> 00:01:26,113
Hở? Nhưng nguyên liệu nấu ăn này...

116
00:01:26,912 --> 00:01:35,113
Không cần phải giải thích.
Tớ mua thêm một ít nguyên liệu 
để cho vào các món ăn của cậu.

116
00:01:35,912 --> 00:01:38,113
Đừng, cậu không thấy kỳ lạ sao?
Tại sao phải cho thêm thuốc?
Để bình thường vẫn ngon mà.

116
00:01:38,912 --> 00:01:45,513
Không sao đâu. Không có gì nguy hiểm cả.
Chỉ là thêm một chút để cậu cảm thấy
tràn đầy năng lượng thôi.

116
00:01:46,512 --> 00:01:48,513
Không, không ổn chút nào?
Cậu định cho gì vào thế?

116
00:01:49,112 --> 00:01:53,113
Một triết gia đã nói thế này:

116
00:01:54,112 --> 00:01:55,113
Hở?

116
00:01:55,912 --> 00:02:00,113
"Mọi loại thuốc đều hợp pháp
cho đến khi được hợp pháp hóa"

116
00:02:01,112 --> 00:02:03,113
Rõ ràng câu đó là của một kẻ nguy hiểm.
Tớ sẽ không cho cậu bỏ vào đâu.

116
00:02:03,912 --> 00:02:10,513
Đùa thôi. Kiểu đùa truyền thống mà.
Tớ chỉ đến hiệu thuốc mua thuốc bình thường.

116
00:02:11,112 --> 00:02:12,513
Thật... thật không đấy?

116
00:02:13,112 --> 00:02:18,113
Tớ không bỏ gì vào món ăn
tại nhà cậu đâu. Với lại...

116
00:02:19,112 --> 00:02:20,113
Với lại sao?

116
00:02:20,912 --> 00:02:27,113
Nếu thật sự có ý định đó,
tớ sẽ không nói trước đâu.

116
00:02:27,912 --> 00:02:29,113
Tớ không thể yên tâm được.
Không biết khi nào đồ ăn bị bỏ thuốc...

116
00:02:30,112 --> 00:02:33,113
Tớ hiểu rồi. Chỉ là thuốc mỡ thôi.

116
00:02:33,912 --> 00:02:35,113
Không phải vấn đề đó...

116
00:02:36,112 --> 00:02:40,113
Đó cũng là đùa thôi.
Tớ chỉ cố gắng làm quen nơi này.

116
00:02:40,912 --> 00:02:42,113
Vậy à?

116
00:02:43,112 --> 00:02:51,113
Đúng vậy. Ngay từ đầu, 
nếu không cố gắng hết sức thì sẽ 
không tìm ra thuốc hiệu quả cao được.

116
00:02:52,112 --> 00:02:54,113
Thế rốt cuộc cậu đang nói thật hay đùa vậy?

116
00:02:54,112 --> 00:29:55,513
Thật là... Đây vẫn là Origami như thường lệ.

116
00:02:55,912 --> 00:02:57,513
Nhưng thế này là sao nhỉ?
Nếu Origami đúng là Origami,
có lẽ tôi cảm thấy nhẹ nhõm hơn.








