﻿0
00:00:04,712 --> 00:00:06,313
Thời tiết thật đẹp

1
00:00:06,712 --> 00:00:08,513
Hôm nay tôi sẽ đi hẹn hò với Origami.

3
00:00:09,712 --> 00:00:12,113
Tuy nhiên, ở đó, tôi cảm thấy
có một ánh mắt nhìn theo và dừng lại.


3
00:00:17,712 --> 00:00:20,113
Một cô gái mặc đồ như một chú hề.
Toàn thân băng bó khắp người.

4
00:00:20,312 --> 00:00:21,313
Cô gái đó là...

5
00:00:21,512 --> 00:00:22,513
Ren à?

6
00:00:22,912 --> 00:00:26,513
Đáng lẽ tôi nên đi, nhưng tôi lại
lẩm bẩm cái tên Ren với cô gái này.
Tôi biết chúng tôi đã gặp nhau
nhưng không nhớ mình đã gặp ở đâu cả.
Một cảm giác gì đó rùng rợn.

7
00:00:27,112 --> 00:00:36,113
Cậu biết tên tôi sao?
Rất vui được gặp cậu, 
nhưng chúng ta đã từng gặp nhau chưa?

8
00:00:37,112 --> 00:00:39,113
À không, không có gì.
Tớ nghĩ chúng ta đã từng gặp nhau rồi.
Tên cậu là Ren phải không?

9
00:00:40,112 --> 00:00:47,113
Đúng vậy. Tên tôi là Ren.
Cậu tên là Itsuka Shidou phải không?

10
00:00:47,712 --> 00:00:49,513
Phải, đúng vậy.
Cậu cũng biết tên tớ sao?

11
00:00:50,112 --> 00:00:59,113
Đúng rồi. Thật là kỳ lạ.
Phải chăng kiếp trước chúng ta là
người yêu của nhau?

12
00:00:59,713 --> 00:01:01,113
Chắc không phải đâu...

13
00:01:01,513 --> 00:01:02,513
Vậy là quả nhiên chúng tôi đã từng gặp nhau sao?

14
00:01:03,112 --> 00:01:04,513
Không, tôi không nhớ gì cả.

15
00:01:05,112 --> 00:01:06,513
Mà không có gì đâu.
Vậy sao cậu đi theo tớ?

16
00:01:07,112 --> 00:01:18,113
Không, nó không phải vấn đề.
Quan trọng hơn, giờ cậu phải nhanh lên.
Cậu đi hẹn hò với Origami phải không?

17
00:01:19,112 --> 00:01:20,513
Hở? Sao cậu biết?

18
00:01:21,112 --> 00:01:29,113
Tôi ghen tị với cậu đấy. 
Một cô gái dễ thương muốn làm
bất cứ điều gì cho Shidou

19
00:01:30,112 --> 00:01:32,513
Cô ấy biết Origami sao?
Chắc chắn Origami có thể đã hỏi cô ấy rất nhiều.
Nhưng cách diễn đạt đó hơi gây hiểu lầm...

20
00:01:33,112 --> 00:01:34,513
Đúng là Origami rất dễ thương đấy nhỉ.
Cô ấy đã giúp đỡ tớ rất nhiều.

21
00:01:35,112 --> 00:01:43,113
Vậy sao? Cô ấy nói 
muốn trở thành thú cưng của Shidou.

25
00:01:44,512 --> 00:01:46,513
Khoan... đợi một chút.
Origami đã nói vậy sao?

25
00:01:47,112 --> 00:01:48,113
Không phải sao?

25
00:01:49,112 --> 00:01:50,313
Nhưng mà...

25
00:01:50,712 --> 00:01:52,513
Tớ nghĩ cậu không nên
nói điều đó ở nơi công cộng!

25
00:01:53,112 --> 00:01:56,513
Cậu nghĩ sẽ có nhiều người tin là thật sao?

25
00:01:57,112 --> 00:02:10,113
Đừng lo lắng quá nhiều về bản thân.
Tôi chỉ nói chuyện với cậu một lúc thôi.
Hẹn gặp lại các cậu ở ngã rẽ cuộc đời

25
00:02:13,112 --> 00:02:14,513
Nghĩa là sao? Cô ấy lo lắng
cho Origami à?

25
00:02:15,112 --> 00:02:16,113
Cũng đến giờ rồi. Mình phải đi thôi.

25
00:02:16,512 --> 00:02:18,513
Tôi suy nghĩ về Ren, 
nhưng tôi không thể đến muộn được.

25
00:02:18,712 --> 00:02:20,713
Không, vẫn còn rất nhiều điều để nói.
Nhưng vì đó là Origami. Vì vậy còn khá sớm.
Tôi nghĩ mình sẽ có đáp án sau.

25
00:02:21,112 --> 00:02:22,513
Có cần phải vội đến thế không?

25
00:02:23,112 --> 00:02:24,513
Tôi vừa tự hỏi vừa đến chỗ hẹn với Origami.







25
00:02:32,112 --> 00:02:33,713
Được rồi.　Tôi đã đến đúng giờ.
Nhưng... Orgami đã...

25
00:02:37,112 --> 00:02:38,713
Quả nhiên vẫn như mọi khi...

25
00:02:39,112 --> 00:02:40,313
Origami. Cậu đợi tớ có lâu không?

25
00:02:40,712 --> 00:02:43,113
Origami luôn đến rất sớm.
Không biết lần này, cô ấy đến từ mấy giờ...
Hy vọng là không lâu lắm

25
00:02:44,512 --> 00:02:47,113
Không. Tớ cũng vừa tới thôi.

25
00:02:48,112 --> 00:02:50,113
Vậy... vậy à? Vậy giờ chúng ta đi chứ?

25
00:02:52,112 --> 00:02:54,113
Vậy hôm nay chúng ta sẽ đi đâu?

25
00:02:54,512 --> 00:02:56,513
Vụ này nó còn rắc rối hơn là mình nghĩ đây

25
00:02:56,712 --> 00:02:58,113
Eto... Tớ thường đến nhà hàng gia đình.
Xin lỗi cậu vì đó là nơi
mà tớ có thể nghĩ đến thôi.

25
00:02:58,512 --> 00:03:08,113
Tại sao cậu phải xin lỗi chứ?
Tớ đã bảo rồi mà. Tớ rất vui khi đi cùng cậu
cho dù đi đến đâu đi chăng nữa.

25
00:03:08,512 --> 00:03:11,113
V...Vậy à?

25
00:03:11,512 --> 00:03:19,513
Ừ. Kể cả phòng của tớ cũng được.
Ngay cả khi hai chúng ta bị mắc kẹt trong đó
và bị ép hôn để thoát ra.

26
00:03:19,913 --> 00:03:21,513
Làm ơn! Như vậy mà cậu cũng nghĩ ra được à?

26
00:03:26,712 --> 00:03:34,113
Are... Không phải Shidou-kun đây sao?
Có cả Origami-san nữa... Trông lãng mạn quá nhỉ...




26
00:03:36,112 --> 00:03:42,113
Xin chào. Shidou-san. Origami-san.
Anh chị đang đi chơi à?

26
00:03:43,112 --> 00:03:45,113
Yoshino và Yoshinon đấy à?
Anh đang đi chơi với Origami.
Bọn anh đang nghĩ xem nên đi ăn ở đâu đây.

26
00:03:45,513 --> 00:03:55,113
Shidou-kun ngày nào cũng hẹn hò với con gái nhỉ.
Sao anh không thử đổi khẩu vị
với một cô thỏ, cùng nhau cao chạy xa bay xem?

26
00:03:56,512 --> 00:03:58,113
Yo... Yoshinon!!!

26
00:03:59,112 --> 00:04:01,113
Ờ... Chắc không có chuyện đó đâu.
Cơ mà em có muốn đi ăn cùng tụi anh không?




26
00:04:01,712 --> 00:04:10,113
Ồ... Được vậy luôn sao?
Bốn người đi cùng nhau có vẻ không ổn đâu nhỉ.
Hay chị cho em mượn anh ấy đi?

26
00:04:11,112 --> 00:04:20,113
Đừng mà... Xin lỗi. Shidou-san. Origami-san.
Xin lỗi đã làm phiền anh chị.

26
00:04:20,712 --> 00:04:22,113
Không có gì.

26
00:04:22,512 --> 00:04:30,113
Chị cũng không cảm thấy phiền đâu.
Dù sao thì việc này cũng một phần
giúp Shidou phong ấn linh lực của các Tinh Linh mà.

26
00:04:31,112 --> 00:04:32,513
Origami....

27
00:04:33,112 --> 00:04:38,113
Sau cùng thì, miễn có tớ ở bên cạnh cậu là được.

28
00:04:39,112 --> 00:04:40,513
Có hơi bất ngờ đấy.
Không biết sau này sẽ như thế nào đây.

29
00:04:41,112 --> 00:04:42,513
Ngoài ra...

30
00:04:43,112 --> 00:04:44,513
Ngoài ra...?

31
00:04:46,112 --> 00:04:55,113
Tất cả đều phụ vào cách Shidou đối xử với mọi người.

32
00:04:56,112 --> 00:04:58,113
Đúng là Origami có khác.
Quả thật rất khó để phủ nhận nó.

33
00:04:58,512 --> 00:05:10,513
Ra là vậy. Origami-san quả là thiên tài.
Nhưng mà, cho Yoshinon biết liệu có ổn không?
Yoshino sẽ trở nên mạnh mẽ lắm đó.

39
00:05:11,512 --> 00:05:19,513
Không thành vấn đề. Nếu đó là điều
mà Shidou muốn làm với mọi người
thì chị có thể hiểu được cảm giác của cậu ấy.

39
00:05:20,112 --> 00:05:24,113
Cho nên, tình huống này cũng không khác gì
cậu ấy làm với chị cả.

40
00:05:25,112 --> 00:05:31,113
Quả nhiên Origami-san tuyệt thật nhỉ.

41
00:05:32,112 --> 00:05:36,113
Chúng ta cần phải học hỏi nhiều đây.

42
00:05:36,512 --> 00:05:46,513
Hmm... Thôi thì em để cho chị mượn anh ấy hôm nay vậy.
Nhưng mà đừng quên, thỏ khi cô đơn sẽ chết đấy.

42
00:05:47,112 --> 00:05:50,113
Are? Sao chuyện này giống như
một câu chuyện cổ tích gì đó thế nhỉ?

43
00:05:51,112 --> 00:05:58,113
Thật ra, nếu con thỏ Yoshino
được Shidou-kun nhặt về, có khi nó đã chết rồi.
Vậy nên hãy chăm sóc nó cẩn thận nhé.

43
00:05:59,112 --> 00:06:03,113
Yo... Yoshinon... Đừng có nói
mấy điều kì lạ đó với Shidou-san mà...

39
00:06:04,112 --> 00:06:10,113
Vậy... Cậu để cho hai người họ như vậy
liệu có ổn không đó?

40
00:06:11,112 --> 00:06:15,113
V... Việc này thì...




41
00:06:16,512 --> 00:06:18,513
Haha. Em không cần phải lo lắng đâu.

42
00:06:19,112 --> 00:06:21,513
Nếu mà có một chú thỏ nào đó như Yoshinon
thì anh sẽ không ngần ngại
ôm lấy chú thỏ dễ thương ấy đâu.

43
00:06:27,512 --> 00:06:31,113
Wow.... Đại thành công rồi.

39
00:06:32,112 --> 00:06:34,113
Không... Ý anh không phải thế...
Ý anh là sẽ rất tệ nếu vật nuôi
không được chăm sóc một cách kĩ càng.


40
00:06:34,512 --> 00:06:44,113
K...Không. Em không để tâm đâu. 
Tuy nhiên... được anh khen dễ thương
là em vui rồi

41
00:06:44,512 --> 00:06:46,113
Ể?

42
00:06:47,112 --> 00:06:53,113
K...Không có gì đâu ạ.
Vậy thì... Em xin phép đi trước ạ!

43
00:06:54,112 --> 00:06:56,113
Bảo trọng!

44
00:07:05,112 --> 00:07:06,513
Sao vậy Origami?

45
00:07:07,112 --> 00:07:16,113
Xem xét. Chọn con đường đó có vẻ mạo hiểm đấy.
Một khi đã lấn vào thì không thoát ra đâu.

45
00:07:16,512 --> 00:07:19,113
Không không. Yoshino luôn cố gắng và khiêm tốn.
Em ấy như một con thỏ bé nhỏ và rất ngọt ngào.

46
00:07:19,512 --> 00:07:23,113
Ra là vậy. Shidou là người cuồng thỏ.

47
00:07:23,512 --> 00:07:25,113
Cậu kết luận kiểu gì vậy?

48
00:07:25,512 --> 00:07:27,313
Mà tóm lại... 
Chúng ta nhanh đi tìm chỗ nào để ăn thôi.






70
00:07:34,112 --> 00:07:36,113
Vẫn là nhà hàng gia đình
mà tôi thường hay tới.

71
00:07:36,312 --> 00:07:38,513
Chúng tôi chủ yếu là tự ăn.
Nhưng hiện tại tôi là học sinh cấp 3.
Để giao lưu với bạn bè,
tôi thường đến nhà hàng gia đình.

72
00:07:38,912 --> 00:07:41,113
Đây cũng là nhà hàng ưa thích của Kotori.
Đôi khi 2 người chúng tôi cũng đến đây.

73
00:07:41,512 --> 00:07:42,513
Cậu muốn gọi món gì, Origami?

74
00:07:43,112 --> 00:07:48,513
Shidou định gọi món gì?
Tớ không phiền việc gọi cùng món với cậu đâu.

75
00:07:49,112 --> 00:07:52,113
Tớ à? Có lẽ tớ sẽ ăn Hamburger.
Không hiểu sao quán ăn gia đình lại có món này.
Nhưng mà món đó ở đây là đặc sản đó.

76
00:07:52,512 --> 00:07:55,513
Ừ. Vậy cũng được.

77
00:07:56,112 --> 00:07:58,513
Tớ thắc mắc liệu có nên gọi món đặc biệt hơn không.
Chúng ta có thể xem hương vị của nhà hàng.
Nó phải rẻ và ngon.

78
00:07:59,112 --> 00:08:04,513
Vậy là cậu cố học hỏi hương vị
của nhà hàng này à. Nghe có vẻ hay đấy.

81
00:08:04,912 --> 00:08:06,513
Haha... Không có gì đâu.
Mà tớ chắc cậu cũng không cần
quan tâm mấy đâu. Vậy cậu gọi món chưa?

82
00:08:07,112 --> 00:08:09,113
Tôi gọi nhân viên 
và đặt 2 suất bánh Hamburger.

83
00:08:09,512 --> 00:08:12,113
Cậu thấy thế nào? Cảm thấy thoài mái không?
Có lẽ quán này phù hợp với gia đình hơn thì phải.
Mà tớ đưa cậu đến đây mà. Tớ đang nói gì vậy nhỉ?

84
00:08:13,112 --> 00:08:28,113
Không có chuyện đó đâu. Tớ thấy vui lắm.
Gia đình. Nhà hàng gia đình này.
Với hai người chúng ta
thì đến nhà hàng này là rất hợp lý rồi.



85
00:08:28,512 --> 00:08:30,113
Cậu không cần phải nói vậy đâu...

86
00:08:31,112 --> 00:08:40,113
Nếu cậu đã đến nhà hàng này với tớ
thì chúng ta cũng là gia đình rồi.
Đâu cần thiết là gia đình
thì phải cùng huyết thống đúng không?

87
00:08:41,112 --> 00:08:43,113
Chờ chút. Cái định nghĩa về gia đình nó hơi bị lạ đấy. Cậu đang nói về kiểu gia đình gì vậy?

88
00:08:43,512 --> 00:08:45,513
Rốt cuộc thì tôi vẫn không biết
Origami đang nói gì
về vụ gia đình hay huyết thống này cả.

89
00:08:46,112 --> 00:08:50,113
Tôi có thể suy ra được phần nào
về chuyện huyết thống. Nhưng cậu ấy
có cần nói một cách nghiêm trọng vậy không?

90
00:08:51,112 --> 00:08:53,513
Món Hamburger tới rồi này
Chúng ta nên ăn đi kẻo nguội mất.

91
00:08:54,512 --> 00:08:56,113
Sao vậy, Origami?

92
00:08:56,512 --> 00:09:12,113
Không có gì. Shidou cứ ăn Hamburgur đó đi.
Mở miệng ra, nếm hương vị của miếng thịt nóng hồi,
và nhớ đừng làm vương vải nước sốt đấy.
Tớ sẽ quan sát cho đến khi nào cậu ăn xong.



93
00:09:12,512 --> 00:09:14,513
Cậu không cần phải làm vậy đâu?
Ý tớ là tại sao cậu cần phải làm vậy chứ?

94
00:09:15,512 --> 00:09:18,513
Bởi vì... Nó là giấc mơ của tớ.

95
00:09:18,912 --> 00:09:20,113
Giấc mơ?

98
00:09:21,112 --> 00:09:24,113
Không có gì đâu. Cậu cứ ăn đi.

99
00:09:24,512 --> 00:09:26,513
Cậu không nên làm vậy ở đây đâu.
Làm vậy chỉ khiến tớ khó xử thôi.

100
00:09:27,112 --> 00:09:29,113
Vậy tớ không nên ở đây à?

101
00:09:30,112 --> 00:09:31,513
Không, không phải vậy.

102
00:09:32,112 --> 00:09:33,113
Vậy à...

103
00:09:34,112 --> 00:09:35,713
Hình như, biểu cảm của cậu ấy không được vui nhỉ.

104
00:09:35,913 --> 00:09:38,113
Có lẽ tôi nên tìm một lý do nào đó
để nói cho Origami hiểu.

105
00:09:38,312 --> 00:09:39,713
Sắc mặt cậu ấy cũng có vẻ không được tốt lắm.
Tôi bắt đầu thấy mình có lỗi quá.

106
00:09:40,113 --> 00:09:41,913
Thôi thì cứ tạm thời như vậy đi.
Cảm giác như mình dẫn thú cưng đi ăn vậy.

107
00:09:42,112 --> 00:09:43,513
Cậu đang nghĩ gì à?

108
00:09:43,712 --> 00:09:45,113
Có gì sao?

109
00:09:45,512 --> 00:09:47,113
À... Không có gì đâu.

110
00:09:47,512 --> 00:09:49,113
Hôm nay tôi cứ là lạ sao ấy nhỉ.

111
00:09:51,912 --> 00:09:53,113
Sao vậy Origami?

113
00:09:53,512 --> 00:09:58,113
Shidou, có hạt cơm dính trên mặt cậu kìa

114
00:09:58,312 --> 00:09:59,513
Ể? Thật à? Nó ở chỗ nào vậy?

1
00:10:00,112 --> 00:10:04,513
Giữ yên đó. Cứ để cho tớ. Tớ sẽ lấy nó xuống.

1
00:10:05,112 --> 00:10:06,513
A... Vậy nhờ cậu vậy.

1
00:10:09,112 --> 00:10:11,113
Ê...? Gì vậy? Sao cậu lại xích ghế qua bên này...

1
00:10:11,512 --> 00:10:12,513
Liếm.

1
00:10:13,112 --> 00:10:14,113
Hả?

1
00:10:14,512 --> 00:10:16,113
À... Origami!?




1
00:10:16,512 --> 00:10:18,113
Sao vậy?

368
00:10:18,512 --> 00:10:20,513
C... Còn hỏi tại sao nữa à?
Cậu nhất định phải liếm sao?

368
00:10:21,112 --> 00:10:27,113
Cách này là cách chắc chắn nhất
để lấy hạt cơm xuống. Không còn cách nào đâu.

368
00:10:27,512 --> 00:10:29,513
Không không...
Không có vụ liếm láp như mấy con cún đâu.

368
00:10:30,112 --> 00:10:31,513
Cậu ấy muốn trở thành cún đến vậy à?

368
00:10:31,712 --> 00:10:32,713
Cậu lại làm gì nữa vậy?

368
00:10:33,112 --> 00:10:36,113
Chuyện kỳ lạ này là sao vậy?
Lý do gì khiến Origami cư xử như vậy?
Rốt cuộc là sao?

368
00:10:36,312 --> 00:10:37,113
Shidou.

368
00:10:37,712 --> 00:10:39,113
A..A... Gì vậy?

68
00:10:40,112 --> 00:10:45,113
Bây giờ cậu rảnh chứ? Có một nơi tớ muốn đi cùng Shidou.

368
00:10:45,512 --> 00:10:47,113
Bây giờ á...? Chúng ta còn chưa ăn xong mà?

368
00:10:48,112 --> 00:10:54,113
Việc này khá là quan trọng với tớ.
Cho nên, có lẽ chúng ta nên đi ngay đi.

368
00:10:55,112 --> 00:10:56,113
Nhưng mà...

368
00:10:56,512 --> 00:10:58,513
Rốt cuộc là chuyện gì mà cậu ấy gấp gáp vậy nhỉ?
Thôi thì cứ chiều theo cậu ấy vậy.

68
00:10:58,712 --> 00:11:00,113
Không được ư?

368
00:11:00,513 --> 00:11:01,513
Không phải là không được...

368
00:11:02,113 --> 00:11:07,113
Vậy à? Vậy thì bây giờ tớ muốn đi mua một vài món đồ.

368
00:11:20,112 --> 00:11:29,113



368
00:11:13,112 --> 00:11:15,113
Sau bữa ăn, chúng tôi đi đến phố mua sắm.
Cậu ấy bảo cần phải mua gì đó.

368
00:11:17,512 --> 00:11:26,113
Xin lỗi đã để cậu chờ. Đi thôi.
Tớ về nhà cậu được chứ?
Hay là đến nhà tớ cũng được

368
00:11:27,112 --> 00:11:29,113
Hở? Có chuyện gì sao?

368
00:11:29,512 --> 00:11:31,113
Cậu không cần phải quan tâm việc đó đâu.

368
00:11:32,112 --> 00:11:34,113
Xin lỗi vì đã hỏi. Cậu định làm gì à?

368
00:11:34,512 --> 00:11:39,113
Bí mật. Nhưng cậu không cần
lo lắng về việc đó đâu.

368
00:11:39,512 --> 00:11:41,513
Thật sự mà nói thì... Nhà của mình
có lẽ sẽ an toàn hơn nhà của Origami.

368
00:11:42,112 --> 00:11:43,113
Vậy đến nhà tớ đi.

368
00:11:44,112 --> 00:11:46,113
Tớ hiểu rồi. Vậy đi thôi.




368
00:12:33,112 --> 00:12:35,113



368
00:11:52,112 --> 00:11:54,113
Lạy trời... Cuối cùng cũng về đến nhà rồi.

368
00:11:54,312 --> 00:11:56,513
Dù may mắn hay đen đủi, trong nhà không có ai cả.
Origami đang ở trong nhà vệ sinh.
Bây giờ tôi chỉ có một mình trong phòng khách.

368
00:11:56,712 --> 00:11:58,513
Không biết cậu ấy định làm gì nhỉ? Đã nửa tiếng rồi mà vẫn chưa thấy cậu ấy quay lại.

368
00:11:58,712 --> 00:12:01,113
Có lẽ mình nên nằm nghỉ tí vậy. Cứ ngồi chờ đợi cũng chả đi đến đâu đâu.

368
00:12:02,112 --> 00:12:05,113
Shidou. Để cậu phải chờ rồi.

368
00:12:05,512 --> 00:12:07,113
A...Cậu về rồi...à...

368
00:12:10,112 --> 00:12:11,113
Tôi không biết nói gì luôn

368
00:12:11,512 --> 00:12:14,113
Sao? Dễ thương chứ?

368
00:12:14,512 --> 00:12:16,713
O... Origami-san? Cái bộ đồ thỏ này là sao vậy?

368
00:12:17,112 --> 00:12:18,513
Không. Dễ thương. Dễ thương hết mức luôn.
Tôi thật sự đang không biết
mình nên vui hay buồn nữa.

368
00:12:19,112 --> 00:12:21,513
Nhưng hơn hết là, 
sự bối rối và bất ngờ đánh bại tôi rồi. 
Rốt cuộc thì cậu đang làm gì vậy, cô thỏ?
Hay là do mình đang tưởng tượng nhỉ.





368
00:12:21,712 --> 00:12:31,113
Thì cậu bảo rằng cậu thích thỏ mà.
Tai cún thì chắc là nổi hơn,
nhưng nó có vẻ không hợp lắm.
Nên tớ mua một cái tai thỏ luôn.

368
00:12:31,512 --> 00:12:33,513
Ý tớ không phải như vậy.
Nhưng cậu có tai cún à?

368
00:12:34,112 --> 00:12:40,113
Trong tủ đồ của con gái có nhiều thứ bí mật
mà cậu không hề biết đâu.

368
00:12:40,512 --> 00:12:42,713
V... Vậy à? Vậy... Bộ đồ này là sao?
Đây là cách mà cậu trả ơn tớ à?

368
00:12:43,112 --> 00:12:47,513
Không sai. Đây là cách tớ trả ơn từ bây giờ.

368
00:12:48,112 --> 00:12:49,113
Hở?

368
00:12:49,512 --> 00:12:55,113
Bây giờ tớ chỉ là một chú thỏ thôi.
Tớ chỉ biết lắng nghe và tuân lệnh cậu thôi.



368
00:12:58,512 --> 00:13:01,113
Thôi... Khỏi cũng được.
Tớ vừa thấy một mớ Figure của cậu rồi.
Nên có lẽ chúng ta không cần cái này đâu.

368
00:13:01,512 --> 00:13:11,113
Có người bảo tớ hãy làm những gì
mình có thể để cho cậu xem bộ đồ thỏ này.
Cho nên cậu hãy ra lệnh gì đó cho tớ đi.

368
00:13:11,512 --> 00:13:13,113
Không không. Làm sao mà tớ làm việc đó được chứ.

368
00:13:13,512 --> 00:13:19,113
Ngài không thể ra mệnh lệnh cho em sao? Chủ nhân.

368
00:13:19,512 --> 00:13:21,113
C... Cái cảm giác gì thế này?

368
00:13:21,512 --> 00:13:23,513
Không thể phủ nhận việc này thích thật.
Có lẽ mình nên làm gì đó nhưng đừng quá lố là được.
Nhưng sao lại là mình cơ chứ?

368
00:13:24,112 --> 00:13:25,513
Vậy... Vậy cậu muốn tớ ra lệnh à?

368
00:13:26,112 --> 00:13:30,113
Vâng. Xin ngài đấy. Chủ nhân.

368
00:13:30,512 --> 00:13:31,713
Cậu kì lạ thật đấy.

368
00:13:32,112 --> 00:13:33,513
Giờ thì... Mình nên ra lệnh gì đây?

368
00:13:34,112 --> 00:13:36,113
1) Cho ăn cà rốt
2) Chạm vào cô ấy

368
00:13:36,712 --> 00:13:39,113
Chắc là cậu ấy ăn cà rốt được.
Thỏ thì chắc là thích cà rốt.

368
00:13:39,512 --> 00:13:41,513
Vậy thì đến giờ ăn rồi.
Tớ sẽ cho cậu ăn cà rốt nhé.

368
00:13:42,112 --> 00:13:43,113
Tớ vui lắm.

368
00:13:43,512 --> 00:13:45,113
Tôi cầm lấy củ cà rốt ở trên bàn. Và đưa nó trước Origami.

368
00:13:45,512 --> 00:13:47,113
Lại đây nào. Origami. Món cà rốt mà cậu thích nè, cậu có muốn nó không?

368
00:13:47,512 --> 00:13:48,513
Tớ muốn.

368
00:13:48,912 --> 00:13:50,513
Hể? Tớ không nghe rõ.
Nếu vậy thì lại năn nỉ tớ đi nào.

368
00:13:51,112 --> 00:13:55,113
Xin hãy cho tớ củ cà rốt khổng lồ đó.

368
00:13:55,512 --> 00:13:56,713
Củ này ư? Là củ cà rốt mà cậu muốn đây ư?

368
00:13:57,112 --> 00:14:06,113
Đúng vậy. Tớ muốn thứ đó của chủ nhân. Xin hãy cho Origami đi ạ. Chủ nhân.




368
00:14:06,512 --> 00:14:08,513
Ngoan lắm. Ngoan lắm. Lại đây và lấy nó đi nào.

368
00:14:11,712 --> 00:14:13,113
Cậu đang làm gì vậy!?

368
00:14:13,512 --> 00:14:16,113
Tớ chỉ đùa thôi.
Cậu không được ăn củ cà rốt đó.
Đó là cà rốt sống...

368
00:14:16,312 --> 00:14:17,113
Như mơ vậy.

368
00:14:16,312 --> 00:14:17,113


368
00:14:18,112 --> 00:14:20,513
Cái kiểu ác mộng gì thế này?
Cô ấy đang ăn củ cà rốt đó.
Tôi thật sự không biết phải làm gì nữa.

368
00:14:24,512 --> 00:14:26,513
Nè, Origami. Cậu tận hưởng đủ chưa?
Tớ nghĩ là ta nên dừng lại được rồi đó.

368
00:14:27,112 --> 00:14:28,513
Thêm một chút nữa thôi.

368
00:14:29,112 --> 00:14:31,113
Có vẻ như Origami đang vui hơn bình thường.
Cậu ấy đang tận hưởng nó à?

368
00:14:31,512 --> 00:14:33,513
Chắc cậu ấy thật sự tận hưởng nó rồi.
Mình chỉ cần đứng nhìn thôi.




368
00:14:39,112 --> 00:14:41,513
Tôi đã có một khoảng thời gian vui vẻ với Origami
Tôi đã tận hưởng nó một lúc.
Thành thật mà nói, cảm giác hôm nay có gì đó sai sai.

368
00:14:42,112 --> 00:14:48,113
Shidou, hôm nay tớ rất vui. 
Cảm ơn cậu. Tặng cậu này.

368
00:14:49,512 --> 00:14:52,113
Đây là... Bộ đồ hóa trang Origami mặc.
Không phải cái này là đồ cho con gái sao?

368
00:14:52,512 --> 00:14:54,513
Tớ chịu á. Nếu mà tớ nhận món đồ này thì tớ sẽ gặp rắc rối cho mà xem. 

368
00:14:55,112 --> 00:15:01,113
Nhưng mà cậu có vẻ thích nó mà. Tớ tự hỏi không biết sẽ ra sao nếu cậu mặc nó nhỉ.

368
00:15:01,512 --> 00:15:03,113
Không, không đời nào mà tớ mặc nó đâu.
Ý tớ là, tại sao tớ lại phải mặc nó chứ.
Đây rõ ràng là đồ con gái mà.

368
00:15:03,512 --> 00:15:05,513
Kotori mà nhìn thấy là chết đấy.
Cậu làm ơn mang về đi...

368
00:15:06,112 --> 00:15:12,113
Tớ hiểu rồi. Vậy nếu khi nào cậu muốn mặc
thì hãy nói tớ. Tớ sẽ mang đến ngay.

368
00:15:13,112 --> 00:15:14,513
Tớ đã bảo là không đời nào rồi mà!

368
00:15:15,112 --> 00:15:22,113
Đùa thôi. Tớ sẽ trả nó lại.
Vậy nhé. Hôm nay thật sự vui lắm.




368
00:15:22,512 --> 00:15:23,513
Ừ... Ừ... Hẹn gặp lại.

368
00:15:27,512 --> 00:15:29,513
Mình còn chẳng thể mặc vừa nó nữa.
Mà thôi vậy. Dù sao thì
cậu ấy cũng chịu mang về là được rồi.

368
00:15:30,112 --> 00:15:32,113
Sau khi nhìn thấy Origami mặc đồ thỏ đó.
Bỗng nhiên mình cũng thấy rạo rực.

368
00:15:32,512 --> 00:15:34,113
Phải chăng mình không nhận ra bản thân mình...
rằng mình là một đứa cuồng thỏ sao?






368
00:15:45,112 --> 00:15:47,113
Em về rồi.

368
00:15:48,112 --> 00:15:49,513
Ồ, Kotori, mừng em về.

368
00:15:50,512 --> 00:15:52,513
Ánh nhìn đó là sao?

368
00:15:53,112 --> 00:16:01,113
Không có gì đâu.
Nhân tiện, Origami thế nào rồi?
Có gì lạ không?

368
00:16:02,112 --> 00:16:08,113
Không, buổi hẹn chắc vẫn tốt đẹp.
Nhưng có gì khác với mọi khi không?

368
00:16:08,512 --> 00:16:11,513
À... Bọn anh đã đi chơi cùng nhau,
hầu như là không có vấn đề gì cả.
Anh cũng có gặp Yoshino nữa.
Em không cần phải lo lắng đâu.

368
00:16:12,112 --> 00:16:18,113
Thế thì về cô gái thỏ dễ thương thì sao?
Trông anh có vẻ thích lắm.
Sao không vượt rào luôn đi?

368
00:16:19,112 --> 00:16:20,513
Khoan...Khoan đã... Không lẽ nào!? Em nhìn thấy hết à!?

368
00:16:21,112 --> 00:16:30,113
Anh đang nói gì thế?
Dù anh có ở đâu đi chăng nữa,
hay ở nhà thì tất nhiên em đều biết chứ.

368
00:16:31,112 --> 00:16:39,113
Trông anh có vẻ tận hưởng lắm nhỉ?
Không cần phải lo đâu.
Cứ làm những gì anh hay làm đi.

368
00:16:39,513 --> 00:16:41,513
Không có chuyện đó đâu!
Anh chỉ đơn giản là khen cậu ấy
dễ thương thôi mà. Em mong đợi điều gì khác à?

368
00:16:42,112 --> 00:16:44,113
Tóm lại, lúc đó anh cũng chẳng biết nữa.
Anh nghĩ mình chỉ cần đối xử tốt với cậu ấy
như một thú cưng trong nhà là được.

368
00:16:44,512 --> 00:16:47,113
Vậy là anh thừa nhận rồi à...

368
00:16:47,712 --> 00:16:49,513
Thật là... Anh đã bảo 
không phải rồi mà...

368
00:16:49,712 --> 00:16:51,513
Có lẽ tôi nên im lặng thì hơn.
Bây giờ tôi thật sự không biết mình đang nói gì nữa.

368
00:16:52,112 --> 00:16:58,113
Không lẽ anh ấy thật sự thích bộ đồ thỏ đó à?
Có khi nào mình nên thử mặc không ta?

368
00:16:58,512 --> 00:16:59,513
Hể?

368
00:17:00,512 --> 00:17:11,613
K... Không có gì hết.
Dù sao thì mức độ linh lực của Origami
cũng ở mức ổn định. Em cũng không 
phàn nàn gì về sở thích của anh đâu.

368
00:17:11,712 --> 00:17:20,113
Tuy nhiên, hạn chế để cho các Tinh Linh
nhìn thấy bộ dạng đó nhé.

368
00:17:20,512 --> 00:17:22,313
Đã bảo là anh không cố ý làm vậy rồi mà...

368
00:17:22,712 --> 00:17:24,513
Quả thật lời nói của tôi 
không có tác dụng gì cả. Tôi hiểu tôi quá mà.

368
00:17:24,712 --> 00:17:26,513
Thật sự không hiểu hôm nay tôi bị gì nữa.
Thôi thì lần sau cẩn thận hơn vậy.




368
00:17:34,112 --> 00:17:46,113
Đây là câu chuyện về thiên thần và ác quỷ,
một cô gái với hai điều ước.
Một vì bản thân và một vì người khác.

368
00:17:46,512 --> 00:17:52,113
Hai suy nghĩ trái ngược nhau
và cô ấy đã chọn đi theo những gì mách bảo.

368
00:18:01,112 --> 00:18:06,113
Giờ thì, câu chuyện sẽ thế nào đây?

368
00:18:07,112 --> 00:18:16,113
Ngài có hài lòng với điều ước
của mình không, Origami-sama?

368
00:18:17,112 --> 00:18:23,113
Tôi rất hài lòng. Tôi chưa bao giờ nghĩ
đó sẽ là một giấc mơ có thật.

368
00:18:24,112 --> 00:18:32,113
Vậy thì còn gì bằng.
Tôi rất vui vì Origami-sama thích nó.

368
00:18:33,112 --> 00:18:34,513
Con mắt.

368
00:18:35,112 --> 00:18:36,513
Vâng?

368
00:18:38,112 --> 00:18:44,513
Một bên mắt của cô đã lộ ra.
Ngày hôm qua cô che cả 2 bên mà.

368
00:18:46,112 --> 00:18:57,513
À... Cái này à? Nó giúp tôi nhìn được 
con đường đến giấc mơ của ngài.
Nếu ngài cảm thấy không thoải mái,
tôi có thể che nó lại.

368
00:18:58,512 --> 00:19:01,113
Không cần đâu. Việc đó không quan trọng.

368
00:19:02,112 --> 00:19:11,113
Vậy thì. Cô nói cô có 3 điều ước. Vẫn còn 2 điều ước nữa thành hiện thực, đúng không?


368
00:19:12,412 --> 00:19:19,113
Quả đúng là vậy. Với điều ước tiếp theo không cần phải do dự đâu.

368
00:19:20,112 --> 00:19:25,113
Thật ư? Vậy điều ước tiếp theo của tôi sẽ là...




368
00:19:30,112 --> 00:19:37,113
Trần truồng và bị nhốt bởi Shidou, đeo một chiếc vòng cổ,
Tôi muốn được vượt qua như tôi mong muốn.

368
00:19:38,112 --> 00:19:40,113
Tôi đã nói không được rồi mà!

368
00:19:41,112 --> 00:19:48,113
Lại xuất hiện. Lời thì thầm của ác quỷ
dẫn tôi đến ý nghĩ sai lầm.

368
00:19:49,112 --> 00:19:55,113
Ai là ác quỷ chứ hả?
Không... tôi là quỷ nhưng không ác...

368
00:19:56,112 --> 00:20:06,113
Tóm lại, "Tôi" không được ước điều ước đó.
Tôi phải ước cẩn thận điều ước lần này...

368
00:20:05,113 --> 00:20:15,113
Cực kỳ cẩn thận. Tôi thích Shidou,
nhưng đôi khi tôi muốn trở thành một thiếu nữ.

368
00:20:15,712 --> 00:20:20,513
Thiếu nữ đâu? Nó hoàn toàn 
tồi tệ hơn mong muốn của hôm nay đấy!

368
00:20:21,112 --> 00:20:29,113
Nếu cô đột nhiên ước điều như vậy,
Itsuka-kun sẽ biết đấy! Ít nhất, chúng ta 
hãy ước một điều nhẹ nhàng thôi!

368
00:20:30,112 --> 00:20:33,113
Cô nói cũng có lý.

368
00:20:34,112 --> 00:20:37,113
Cô... cô hiểu rồi à?

368
00:20:38,112 --> 00:20:48,113
Cô thật tin người.
Nói cách khác, thay vì bị gò bó,
tốt hơn là trở thành hầu gái của Shidou

368
00:20:50,512 --> 00:20:59,113
Một ngày nọ, trong khi đang dọn dẹp căn nhà,
cô hầu gái Origami làm vỡ bộ đồ ăn quý giá của Shidou.

368
00:21:00,112 --> 00:21:11,113
Tiền lương sẽ không được trả. Hình phạt bắt đầu.
Hầu gái bị nhốt vào nhà kho
và Shidou cười khoái trá...

368
00:21:12,112 --> 00:21:14,113
Cuối cùng câu chuyện vẫn vậy mà!

368
00:21:16,112 --> 00:21:20,113
Quyết định vậy đi. Điều ước sẽ được thực hiện ngay lập tức.

368
00:21:20,712 --> 00:21:22,113
Khoan...

368
00:21:25,112 --> 00:21:31,113
Có vẻ ngài đã đi đến quyết định cuối cùng.
Ngài có thể cho tôi biết điều ước của ngài không?

368
00:21:32,112 --> 00:21:35,113
Tôi là người hầu của Shidou...

368
00:21:39,112 --> 00:21:42,113
Tôi muốn đáp lễ cho Itsuka-kun!

368
00:21:43,112 --> 00:21:50,113
Không giống điều ước trước.
Lần này điều ước thật bình thường.
À không. Xin lỗi.

368
00:21:51,112 --> 00:21:59,113
Đó có phải là một Origami-sama khác đúng không?
Tôi hiểu, tôi hiểu rồi.

368
00:22:00,112 --> 00:22:07,513
Được rồi. Đó hẳn là điều ước của Origami-sama.
Hãy biến nó thành sự thật nào.

368
00:22:13,112 --> 00:22:20,513
Origami-sama, người có một điều ước
tuyệt vời vào ngày mai. Tôi đã khiến nó
được thực hiện lên Shidou-sama.

368
00:22:21,112 --> 00:22:26,113
Vâng, đây là những gì tôi đang có. Hẹn gặp lại!

368
00:22:27,112 --> 00:22:32,113
Tôi chưa nói gì mà.

368
00:22:40,112 --> 00:22:46,113
Cô ta đã bắt được mình.
Không thể nào, mình không thể
kiểm soát cơ thể mình.

368
00:22:47,112 --> 00:22:55,113
Đáp lễ cho Shidou...
Không tệ lắm, nhưng điều ước nghe khá mơ hồ.

368
00:22:56,112 --> 00:22:58,513
Chuyện gì đang xảy ra vậy?

368
00:22:36,112 --> 00:22:39,113


