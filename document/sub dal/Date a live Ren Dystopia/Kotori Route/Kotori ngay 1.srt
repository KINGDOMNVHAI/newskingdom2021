﻿0
00:00:06,112 --> 00:00:08,113
Đi đến Nhà Itsuka?
1) Đồng ý
2) Không đồng ý

1
00:00:12,712 --> 00:00:14,313
Tôi quyết định về nhà
ngay sau khi tan học.

2
00:00:14,712 --> 00:00:16,113
Tôi đã mua sắm xong cho ngày hôm nay.
Thật tốt khi được thư giãn ở nhà.

3
00:00:17,112 --> 00:00:21,113
Ara, Shidou, anh về rồi.
Hôm nay anh về sớm nhỉ.

4
00:00:22,112 --> 00:00:23,113
Ừ, anh về rồi đây.

5
00:00:23,912 --> 00:00:25,113
Hở? Ruy băng màu đen. Đây là Kotori chỉ huy.

6
00:00:25,512 --> 00:00:27,313
Bình thường, em ấy đến trường trung học
với ruy băng trắng. Hiếm khi em ấy đổi nơ
khi vừa tan học.

7
00:00:28,112 --> 00:00:33,113
Gì vậy? Sao anh nhìn em ghê vậy?
Nếu anh có gì muốn nói thì nói đi.

8
00:00:34,112 --> 00:00:36,313
Anh thấy em đeo nơ đó nên nghĩ
không biết có chuyện gì về Tinh Linh không.

9
00:00:37,112 --> 00:00:48,113
Đây đâu phải trang bị chống Tinh Linh đâu.
Mùa đông đến rồi nên em muốn 
đeo gì đó cho ấm thôi.

10
00:00:48,712 --> 00:00:56,113
Em vẫn có thể làm mọi việc với nơ trắng.
Chỉ là thay đổi cảm giác thôi.

11
00:00:56,912 --> 00:00:58,113
Ra là vậy.

12
00:00:58,512 --> 00:01:00,113
Nếu em ấy đeo nơ trắng, 
các quyết định khác nhau sẽ được thực hiện
dựa trên cảm xúc nhất thời.

13
00:01:00,912 --> 00:01:04,113
Shidou, anh đang nghĩ cái gì
thô lỗ phải không?

14
00:01:04,912 --> 00:01:07,313
Không, không phải thế.
Về đồ sưởi ấm... Máy điều hòa có tác dụng không?

15
00:01:07,912 --> 00:01:18,113
Về cơ bản là ổn, nhưng em muốn có chút thay đổi.
Natsumi đang rất muốn có kotatsu

16
00:01:18,912 --> 00:01:20,113
Còn ai yêu cầu gì nữa không?

17
00:01:21,112 --> 00:01:31,113
Đối với Miku, cô ấy yêu cầu
"Hãy gửi Tinh Linh-san đến mỗi ngày".
Em không ngờ được nước đi này đấy.

18
00:01:31,912 --> 00:01:33,113
Ừ... Đúng là kiểu của Miku.

19
00:01:34,112 --> 00:01:44,113
Ngoài ra, còn tiền tiêu vặt nữa.
Anh có muốn mua quần áo riêng cho trời lạnh không?

20
00:01:44,912 --> 00:01:47,113
Đúng rồi. Anh muốn sớm có một chiếc áo khoác mới.
Lâu rồi anh chưa mua áo.

21
00:01:47,912 --> 00:01:50,113
Anh vẫn có thể mặc nó mà.

22
00:01:50,712 --> 00:01:52,113
Em đối xử với anh khác hẳn với Tinh Linh đấy.

23
00:01:52,512 --> 00:02:05,113
Em đùa thôi. Anh tính khi nào đi mua?
Em sẽ chọn một chiếc áo khoác ôm sát và dễ thương.
Ngoài ra, anh thấy đấy, chân em cũng bị lạnh.

25
00:02:05,512 --> 00:02:07,513
Cái đó dành cho con gái mà.
Đừng nói là em mua cho Shiori-chan đấy.

25
00:02:08,112 --> 00:02:14,113
Anh đã có đồ mặc, 
còn Shiori-chan không có quần áo ấm đúng không?

25
00:02:14,912 --> 00:02:17,113
Ngay từ đầu, Shiori đã không nên xuất hiện thường xuyên rồi.

25
00:02:17,912 --> 00:02:26,113
Em cũng mong vậy. Dù sao, để mọi người
sống như một con người, có rất nhiều điều để suy nghĩ.

25
00:02:26,912 --> 00:02:28,313
Không sao đâu... Em cũng có việc của mình mà.

25
00:02:29,112 --> 00:02:39,113
Việc của em chỉ có chút vấn đề thôi.
Công việc ở Ratatoskr hầu hết
được Reine và những người khác làm giúp.

25
00:02:40,112 --> 00:02:52,113
Có những Tinh Linh ích kỷ như Origami và Miku,
Ích kỷ nói hơi quá, nhưng phải cẩn thận với họ.

25
00:02:53,112 --> 00:03:03,113
Yoshino và Natsumi, 
họ đang dần thân nhau và tích lũy kiến thức.
Em có thể đoán được điều này.

25
00:03:03,912 --> 00:03:06,113
Tôi không nghĩ nhiều đến vậy.
Khi số lượng Tinh Linh cần bảo vệ tăng lên,
Kotori cũng nghĩ nhiều hơn 
về việc họ sẽ giao tiếp với nhau thế nào.

25
00:03:06,512 --> 00:03:08,113
Kotori, đừng làm quá lên như vậy.

25
00:03:09,112 --> 00:03:18,113
Em không làm quá đâu.
Tuy nhiên, gần đây em đã yêu cầu 
Shidou và mọi người cố gắng hết sức,
em cũng phải cố gắng hơn một chút.

26
00:03:18,912 --> 00:03:21,113
Em nói gì vậy?
Em mà không cố gắng mới là chuyện lạ đấy.
Anh còn không cố bằng em mà.

26
00:03:21,912 --> 00:03:24,113
Anh nói gì thế?

26
00:03:24,912 --> 00:03:28,113
Kotori đã như vậy trong một thời gian dài.
Vì em ấy có trách nhiệm lớn nên luôn làm việc quá sức.
Mặc dù Kotori cũng là một trong những Tinh Linh được bảo vệ.

26
00:03:28,512 --> 00:03:30,113
Nếu Kotori quan tâm đến mọi người nhiều như vậy,
Tôi phải chăm sóc Kotori tốt hơn.

26
00:03:30,512 --> 00:03:32,113
Anh sẽ làm thêm món em thích.
Vẫn còn thời gian nên anh làm công phu hơn chút.

26
00:03:33,112 --> 00:03:38,113
Hôm nay em muốn có món tráng miệng.

26
00:03:38,912 --> 00:03:40,313
OK, cứ để anh lo. Anh sẽ làm nhanh thôi.

26
00:03:41,112 --> 00:03:43,513
Em mong chờ đấy.

26
00:03:51,512 --> 00:03:55,113
Nè Shidou. Anh nhớ những gì em yêu cầu sáng nay không?

26
00:03:57,512 --> 00:03:59,513
Sáng nay à? Fraxinus không hoạt động 
nên phải cẩn thận, phải không?

38
00:04:00,112 --> 00:04:05,113
Phải. Anh thấy sao?
Mọi người hôm nay có vấn đề gì không?

39
00:04:05,912 --> 00:04:08,313
Anh chưa gặp hết tất cả mọi người,
nhưng có vẻ chẳng có vấn đề gì.

40
00:04:08,513 --> 00:04:10,113
Yoshino và Natsumi vui vẻ đi chơi.
Tohka và Origami đã bớt gay gắt hơn với nhau.

41
00:04:10,913 --> 00:04:20,113
Ra là vậy. Thật khó tin khi
Tohka và Origami có thể cải thiện.
Thật đáng mừng.

42
00:04:20,513 --> 00:04:22,513
Phải, anh cảm thấy mọi người đều có tổ chức hơn.
Cũng nhờ em cải cách tư tưởng, 
quan tâm đến mọi người đấy.

43
00:04:23,112 --> 00:04:34,113
Anh không cần khen đâu.
Em chỉ đang làm công việc của mình thôi.
Em phải cẩn thận để mọi người sống vui vẻ.

39
00:04:34,712 --> 00:04:36,313
Anh khen thật đấy. Và hơn nữa,
em không có thời gian để nghỉ ngơi.

40
00:04:37,113 --> 00:04:47,113
Vậy giờ nghỉ ngơi thôi.
Anh cũng đi nghỉ đi.
Anh thấy muốn đánh thức em khó thế nào chưa?

41
00:04:48,113 --> 00:04:49,713
Ừ, chúc ngủ ngon.

