﻿0
00:00:17,912-->00:00:20,113
Shidou, Shidou,
đến suối nước nóng rồi à?

1
00:00:20,312-->00:00:24,113
Tất nhiên là không, 
vẫn còn sớm lắm, Tohka.

2
00:00:24,512-->00:00:26,113
Phải chờ ra khỏi bến xe đã.

3
00:00:26,512-->00:00:32,113
Suối nước nóng... Hay nói đúng hơn,
đó là nhà trọ nơi chúng ta sẽ ở lại.

5
00:00:32,512-->00:00:34,113
Xem nào...

6
00:00:34,812-->00:00:36,513
Hình như chúng ta sắp đến rồi đấy.

7
00:00:36,813-->00:00:41,513
Vậy à? Vậy nhà trọ này như thế nào vậy?

8
00:00:42,112-->00:00:44,113
Có giống nhà trọ lần trước 
chúng ta đi với trường không?

9
00:00:44,312-->00:00:46,513
Đây là nhà trọ mà Reine-san đã đặt

10
00:00:47,112-->00:00:50,113
Vậy nên anh cũng chẳng biết. 
Anh cũng hồi hộp lắm.

11
00:00:52,112-->00:00:53,513
Thú vị quá nhỉ.

12
00:00:54,112-->00:00:55,513
Được, nhanh lên nào!

13
00:00:58,112-->00:01:03,513
Shidou, Shidou. 
CÓ rất nhiều khói tỏa ra. Đó là gì vậy?

14
00:01:04,112-->00:01:05,913
Có phải là suối nước nóng Manju không?

15
00:01:06,112-->00:01:07,513
Bản đồ viết thế đấy.

16
00:01:08,112-->00:01:11,113
Suối nước nóng Manju? Là gì vậy?

18
00:01:11,312-->00:01:13,513
Nó khác gì với suối nước nóng bình thường?

19
00:01:14,112-->00:01:18,313
Họ sử dụng suối nước nóng 
để làm nguồn nhiệt nấu ăn.

20
00:01:18,912-->00:01:23,313
Anh nghĩ nó cũng không khác
Manju bình thường đâu.

21
00:01:23,812-->00:01:25,513
Nhưng hầu hết các đặc sản địa phương
đều nấu theo cách này.

22
00:01:25,912-->00:01:26,913
Ra là vậy.

23
00:01:27,112-->00:01:34,113
Em nghe Kotori nói trước khi đi.
"Đặc sản địa phương" là những thứ 
em không thể ăn ở nhà, phải không?

24
00:01:34,712-->00:01:38,113
Nếu vậy, em muốn nếm thử Manju ở đây!

25
00:01:38,512-->00:01:41,513
Anh nghĩ chúng ta sẽ được ăn
trong bữa tối thôi.

25
00:01:42,112-->00:01:45,513
Nếu em muốn thì cứ ăn thử đi.
Đừng ăn nhiều quá đấy.

25
00:01:45,912-->00:01:47,313
Ừ, được rồi!

25
00:01:49,112-->00:01:52,113
Sao rồi Tohka? Manju ngon chứ?

25
00:01:55,912-->00:01:58,513
Anh nói chúng không khác Manju bình thường.

25
00:01:58,912-->00:02:02,513
Nhưng so với Manju bình thường,
em thấy nó rất ngon!

25
00:02:03,112-->00:02:04,513
Vậy thì tốt...

25
00:02:05,112-->00:02:09,313
Người ta nói đặc sản thường không tốt lắm,
nhưng điều đó không hoàn toàn đúng.

25
00:02:10,112-->00:02:13,513
Anh nghĩ những thứ đặc sản
có những cái tốt riêng.

25
00:02:15,112-->00:02:18,513
Nếu anh nghĩ đặc sản địa phương rất ngon,
em muốn ăn nhiều nữa.

25
00:02:20,112-->00:02:23,113
Shidou! Chỗ đó lại ghi là
"đặc sản địa phương" kìa!

25
00:02:23,912-->00:02:28,113
Suối nước nóng Udon?
Nó có vị gì nhỉ?

25
00:02:28,312-->00:02:30,113
Suối nước nóng Udon à?

25
00:02:30,312-->00:02:33,113
Đây là những người nấu mì Udon 
nhờ nguồn nước sao?

25
00:02:33,512-->00:02:35,113
Cách này phổ biến mà.

25
00:02:35,512-->00:02:39,113
Chờ đã, Tohka!
Em vừa mới ăn xong mà!

25
00:02:40,112-->00:02:42,113
Nhưng đây là mì Udon, phải không?

25
00:02:42,312-->00:02:46,513
Nó trôi tuột đi khi em ăn,
vậy nên bụng em chẳng còn gì đâu.

25
00:02:47,112-->00:02:48,513
Em vẫn có thể ăn tối được.

25
00:02:49,112-->00:02:51,913
Dù sao vẫn còn phải đi một quãng đường
nữa để đến nhà trọ mà!

25
00:02:52,112-->00:02:54,513
Đến nhà trọ cũng không xa nữa đâu.

25
00:02:56,112-->00:02:58,113
Thôi được, nếu chỉ là Udon thì em đi đi.

25
00:02:58,312-->00:03:00,113
Được! Vậy nhanh lên nào!

26
00:03:02,112-->00:03:07,513
Shidou, kế bên Udon còn có
suối nước nóng khác kìa.

26
00:03:08,512-->00:03:10,313
Đây là Gyoza onsen.

26
00:03:11,312-->00:03:15,113
Tohka? Em làm ơn dừng lại được không?

26
00:03:18,512-->00:03:23,512
Cuối cùng, cô ấy nếm thử tất cả mọi thứ.
Cô ấy ăn một loạt các món khác nhau.

26
00:03:23,912-->00:03:30,513
Manju, Udon, Gyoza và Senbei...
Tất cả mọi thứ thật sự rất ngon.

26
00:03:31,112-->00:03:34,113
Những món đó toàn đồ hấp và nấu không đấy.

26
00:03:34,512-->00:03:37,113
Em có chắc em ăn nổi không?

26
00:03:37,912-->00:03:43,313
Về khoản ăn thì không thành vấn đề.
Em vẫn chưa no đâu!

26
00:03:43,912-->00:03:47,113
Em vẫn có sức ăn ấn tượng
như mọi ngày đấy, Tohka...

26
00:03:48,112-->00:03:52,113
Giờ chúng ta đến nhà trọ đi.

27
00:03:53,112-->00:03:54,113
Vẫn còn sớm quá.

28
00:03:55,812-->00:03:57,513
Shidou, đó là gì vậy?

29
00:03:58,812-->00:04:03,113
Lại đồ ăn nữa à? Em phải dừng lại đi.

39
00:04:03,312-->00:04:05,113
Không, đây không phải đồ ăn

40
00:04:05,513-->00:04:07,313
Ngôi nhà có hình dạng kỳ lạ này là gì?

41
00:04:07,913-->00:04:09,113
Một ngôi nhà à?

42
00:04:10,513-->00:04:13,113
Đây không hẳn là ngôi nhà ..

43
00:04:14,112-->00:04:16,113
Hình như nó ấy rất nổi tiếng...

39
00:04:16,512-->00:04:19,513
Nó ghi chữ "kho báu" kìa!
Nó phải là một cái gì đó khá hiếm đấy!

40
00:04:20,113-->00:04:23,513
Không, đợi Tohka!
Đó không phải nơi em được vào đâu!

41
00:04:24,113-->00:04:25,113
Tại sao?

42
00:04:25,513-->00:04:27,113
Nói thế nào nhỉ...

43
00:04:27,912-->00:04:30,313
Vì chưa đủ tuổi nên có nhiều chỗ 
chúng ta không được vào đâu.

39
00:04:30,813-->00:04:32,313
Là vậy đấy.

40
00:04:33,113-->00:04:36,113
Em phải đợi lớn tuổi hơn nữa, hiểu không?

41
00:04:36,513-->00:04:38,313
Nếu vậy thì không còn cách nào khác.

42
00:04:38,913-->00:04:42,113
Đúng vậy! Không còn cách nào khác đâu!
Chúng ta không còn cách nào khác đâu!

43
00:04:43,912-->00:04:46,113
Chúng ta đi thôi, Tohka

44
00:04:48,112-->00:04:51,513
Em rất tò mò, nhưng nếu anh 
nhất quyết không cho thì em không đòi nữa.

44
00:04:53,912-->00:04:56,113
Có rất nhiều nơi khác để đi mà.

44
00:04:56,512-->00:05:00,113
Ngay cả những nhà trọ
nơi chúng ta sẽ ở lại cũng nổi tiếng đấy.

44
00:05:00,313-->00:05:02,113
Cái gì? Thật sao?

46
00:05:02,512-->00:05:07,513
Vậy thì đừng lãng phí thời gian
ở đây nữa, Shidou! Đến nhà trọ nhanh đi!

48
00:05:08,812-->00:05:11,113
Tohka, đừng có chạy!

49
00:05:11,312-->00:05:16,113
Thật là... cô ấy đã biết
nhà trọ đó ở đâu đâu.

51
00:05:16,312-->00:05:19,513
Shidou! Đây này! Đây này! Nó ở đây này!

52
00:05:20,112-->00:05:22,113
Anh biết rồi. Anh đến ngay.

53
00:05:26,512-->00:05:28,913
Nhà trọ này có vẻ tốt đấy.

54
00:05:29,112-->00:05:33,113
Một khách sạn, trông giống như một ngôi nhà lớn.

55
00:05:33,512-->00:05:38,513
Nó khác với lần đi với trường.
Nó cũng là một nhà trọ à?

56
00:05:40,512-->00:05:45,513
Không biết em có hiểu không 
nhưng đây là sự khác biệt 
giữa phong cách Nhật Bản và phương Tây.

57
00:05:46,912-->00:05:48,113
Em không hiểu.

58
00:05:49,112-->00:05:52,113
Vậy ở trong đó có trò chơi gì không?

59
00:05:52,512-->00:05:57,513
Không, không có trò chơi gì đâu.
Chỗ này không có trò chơi giải trí gì cả.

41
00:05:59,112-->00:06:02,513
Vậy chúng ta sẽ làm gì trong nhà trọ?

42
00:06:03,112-->00:06:10,113
Chúng ta làm gì à? Tắm, ăn....
hoặc ngủ trong một căn phòng lớn.

44
00:06:10,512-->00:06:13,113
Vậy là nó không khác bình thường sao?

45
00:06:13,512-->00:06:17,113
Trong chuyến đi dã ngoài, 
chúng ta đã làm rất nhiều thứ.

46
00:06:17,512-->00:06:24,113
Chỗ này là nơi để thư giãn 
trong môi trường dễ chịu, loại bỏ mệt mỏi.

47
00:06:25,313-->00:06:28,513
Vậy đó là những gì chúng ta nhận được
khi ở nhà trọ sao?

48
00:06:29,112-->00:06:32,513
Nếu vậy, em sẽ cố gắng tận hưởng nó như thế!

49
00:06:32,912-->00:06:33,513
Ừ!

50
00:06:33,912-->00:06:36,113
Các phòng tắm vẫn là thứ anh thích nhất.

51
00:06:36,912-->00:06:38,813
Dù sao nó cũng là suối nước nóng mà.

53
00:06:39,112-->00:06:41,113
Người ta có thể duỗi tay chân 
một cách thoải mái.

54
00:06:41,512-->00:06:44,113
Giống như lần trước chúng ta đi phải không?

55
00:06:45,112-->00:06:47,513
Nào, Shidou! Hãy đi tắm đi!

56
00:06:47,913-->00:06:49,113
Khoan đã, khoan đã.

57
00:06:49,312-->00:06:52,313
Đầu tiên chúng ta phải để
đồ đạc lại trong phòng đã..

58
00:06:52,912-->00:06:55,113
Được rồi. Shidou, chúng ta làm nhanh nào!

59
00:06:55,812-->00:06:57,513
Anh đã nói đừng có chạy!

60
00:06:58,112-->00:07:01,113
Tohka chẳng hiểu gì cả.

61
00:07:03,912-->00:07:08,513
Được! Đi tắm suối nước nóng thôi!
Em chờ lâu lắm rồi!

63
00:07:09,312-->00:07:12,113
Có vẻ phòng tắm đẹp lắm đấy.
Phải tắm thử mới được.

65
00:07:14,112-->00:07:19,113
Thực ra Shidou, em đã nghe từ Reine-san...

67
00:07:19,912-->00:07:22,513
Nhưng ở suối nước nóng, 
có một số chỗ ghi là "bể tắm chung" phải không?

68
00:07:24,112-->00:07:28,913
Nếu vậy thì...
chúng ta có thể tắm chung sao?

70
00:07:29,112-->00:07:30,113
Cái gì?

71
00:07:30,312-->00:07:33,113
Reine-san lại nói nữa...

72
00:07:33,312-->00:07:37,113
Không, không quan trọng.
Tohka, nghe anh nói này.

74
00:07:37,512-->00:07:40,113
Các tấm bảng treo ở đó,
em hiểu ý nghĩa của chúng không?

75
00:07:41,112-->00:07:43,113
Nó ghi là Nam và Nữ

76
00:07:43,513-->00:07:47,513
Nó cũng giống như lần
đi dã ngoại hay phòng tắm công cộng à?

77
00:07:48,112-->00:07:51,513
Ừ, đúng vậy.
Như em thấy, nó không phải bể tắm chung.

78
00:07:51,812-->00:07:53,113
Nam và nữ sẽ phải đi riêng.

79
00:07:53,312-->00:07:54,313
Vậy à?

80
00:07:55,912-->00:07:57,513
Thế thì tiếc thật.

81
00:07:57,912-->00:07:59,413
Em nói cái gì?

82
00:07:59,813-->00:08:01,313
Không có gì! Không có gì đâu!

83
00:08:02,112-->00:08:04,113
Dù sao, chúng ta vào nhanh đi, Shidou!

84
00:08:05,112-->00:08:07,513
Anh sẽ ở đây. Em qua đó đi.

85
00:08:08,112-->00:08:10,113
Vậy gặp anh sau nhé, Shidou.

86
00:08:10,912-->00:08:12,513
Ừ, hẹn gặp lại.

87
00:08:17,112-->00:08:18,513
Nó thực sự là một phòng tắm tốt.

88
00:08:20,112-->00:08:22,313
Mình có lẽ nên tắm nước thường trước.

89
00:08:22,913-->00:08:25,513
Nhưng tắm suối nước nóng 
ngay sẽ cảm thấy đã hơn.

90
00:08:29,112-->00:08:31,513
Hơn nữa, phong cảnh ở đây thật đẹp.

91
00:08:34,112-->00:08:40,113
Dù là một chuyến đi ngắn,
nhưng mình thực sự có lý do để đi.

92
00:08:41,112-->00:08:43,513
Bên cạnh đó, mình không cần phải 
chuẩn bị bữa tối.

93
00:08:44,112-->00:08:46,513
Giờ là lúc thư giãn...

94
00:08:49,112-->00:08:51,513
Có người ở đây rồi à?

95
00:08:52,112-->00:08:53,513
Xin chào!

96
00:08:55,112-->00:08:56,513
Giọng nói đó?

97
00:08:57,112-->00:08:58,113
Là anh à, Shidou?

98
00:08:58,312-->00:09:01,113
To... Tohka? Sao em lại vào nhà tắm nam?

99
00:09:01,312-->00:09:03,513
Em đã vào đúng phòng như anh chỉ mà.

100
00:09:04,112-->00:09:07,313
Thay đồ xong, em thấy bể tắm này thì em vào thôi.

101
00:09:08,112-->00:09:09,113
Thì ra...

102
00:09:09,312-->00:09:12,113
Đó là phòng thay đồ cho bể tăm chung à?

103
00:09:13,112-->00:09:15,513
Nhưng có anh ở đây làm em yên tâm lắm.

104
00:09:16,112-->00:09:19,113
Thật thú vị khi vào một bể tắm lớn như thế này!

105
00:09:21,112-->00:09:24,513
Đừng có đứng lên mà không quấn khăn như thế!

106
00:09:26,112-->00:09:27,513
Phải rồi.

107
00:09:28,112-->00:09:30,513
Nhưng mà Shidou...

108
00:09:31,112-->00:09:36,113
Kotori nói chúng ta không vào
bồn tắm chỉ với một chiếc khăn ...

109
00:09:36,512-->00:09:38,313
Không, đó là sự thật, nhưng ...

110
00:09:39,112-->00:09:42,513
Vậy thì em cần thêm 1 cái 
để lót ở lưng phải không?

111
00:09:42,912-->00:09:45,113
Như vậy, khi dựa vào sẽ đỡ nóng hơn.

108
00:09:45,312-->00:09:48,513
Em nói đúng. Chúng ta lấy thêm đi.

110
00:09:52,812-->00:09:55,513
Vậy em vào đi, Shidou.

111
00:09:57,112-->00:09:58,113
Ừ...

112
00:10:01,112-->00:10:03,113
Sướng quá...

113
00:10:04,112-->00:10:07,113
Em cảm thấy nhẹ nhàng, thoải mái quá...

114
00:10:08,112-->00:10:09,813
Đúng vậy.

1
00:10:11,112-->00:10:15,113
Phong cảnh ở đây thật tuyệt đấy, Shidou.

1
00:10:15,512-->00:10:17,113
Đúng vậy.

1
00:10:18,112-->00:10:19,313
Shidou?

1
00:10:20,112-->00:10:22,513
Nãy giờ anh cứ nói mấy câu tương tự.

1
00:10:23,112-->00:10:29,513
Và anh cứ nhìn lên tường nãy giờ.
Trên tường có gì hay à?

1
00:10:30,113-->00:10:32,313
Không, không có gì đâu! Đừng lo.

368
00:10:32,712-->00:10:33,913
Vậy à?

368
00:10:34,312-->00:10:36,513
Vậy anh nhìn lên trời đi, Shidou.

368
00:10:37,112-->00:10:40,513
Nhìn kìa, có một ngôi sao trên trời đấy.

368
00:10:42,912-->00:10:44,113
Đúng vậy.

368
00:10:44,912-->00:10:46,113
Không, cứ thế này...

368
00:10:46,812-->00:10:50,113
Nhìn kìa Shidou! Anh thấy không?
Có cái gì di chuyển trong rừng đấy.

368
00:10:50,512-->00:10:54,113
Anh nghĩ đến lúc anh phải ra ngoài rồi.

368
00:10:55,112-->00:10:58,513
Anh đã nói suối nước nóng để thư giãn mà?

368
00:10:58,812-->00:11:01,513
Nãy giờ em không thấy anh thư giãn gì cả!

368
00:11:05,112-->00:11:07,113
Đúng vậy...

368
00:11:08,912-->00:11:13,513
Sao vậy, Shidou?
Tim anh đập nhanh lắm.

68
00:11:14,112-->00:11:16,113
Anh cảm thấy không khỏe à?

368
00:11:16,512-->00:11:19,113
Không phải! Anh bình thường!

368
00:11:19,512-->00:11:21,113
Nếu anh nói thế thì tốt rồi.

368
00:11:22,912-->00:11:28,113
Nhìn xem, Shidou!
Có con chim đậu ngay bể tắm kìa!

68
00:11:28,312-->00:11:29,513
Phải.

368
00:11:29,912-->00:11:32,513
Không được... mình không thể tập trung được.

368
00:11:34,312-->00:11:37,113
Anh không sao chứ, Shidou?

368
00:11:37,312-->00:11:39,113
Không có gì!

368
00:11:39,512-->00:11:41,313
Thôi anh ra trước đây!

368
00:11:41,512-->00:11:43,113
Nếu anh đứng dậy đột ngột thế thì...

368
00:11:47,112-->00:11:50,313
Xin lỗi, Tohka!
Anh đi trước đây!

368
00:11:50,813-->00:11:53,113
Khoan đã Shidou! 
Nếu anh ra thì cho em ra với!

368
00:11:55,112-->00:11:57,113
Anh ấy đi rồi...

368
00:11:58,112-->00:12:01,113
Shidou kỳ lạ thật.

368
00:12:05,113-->00:12:08,113
Cuối cùng mình cũng có thể bình tĩnh lại.

368
00:12:08,513-->00:12:10,113
Bữa tối có chưa?

368
00:12:10,812-->00:12:12,313
Đúng rồi, Tohka.

368
00:12:13,112-->00:12:16,313
Chúng ta sẽ đi ăn ở ngoài.
Em muốn đi không?

368
00:12:16,512-->00:12:21,513
Ừ! Em rất muốn đi đấy!
Em thắc mắc món ăn địa phương có gì đấy.

368
00:12:21,912-->00:12:23,113
Đặc sản à?

368
00:12:24,112-->00:12:29,113
Anh đoán em đã biết rồi.
Nhưng vẫn còn những món em chưa ăn đâu.

368
00:12:32,112-->00:12:34,513
Hồi hộp quá! Nhanh lên nào!

368
00:12:38,112-->00:12:42,113
Nhìn kìa, Shidou! Nhiều món quá!

368
00:12:43,112-->00:12:47,513
Thật ấn tượng!
Anh không nấu được nhiều món thế này đâu!

368
00:12:48,112-->00:12:49,313
Mọi thứ trông rất ngon.

368
00:12:50,112-->00:12:51,313
Itadakimasu!

368
00:12:51,512-->00:12:53,513
Ừ, Itadakimasu!

368
00:12:54,112-->00:12:55,513
Nước sốt đậu nành...

368
00:12:55,912-->00:12:56,913
Là gì vậy?

368
00:12:58,812-->00:13:01,113
Xin lỗi... Em lỡ nhúng tay vào rồi.

368
00:13:01,912-->00:13:03,113
À không...

368
00:13:03,512-->00:13:06,313
Anh cũng xin lỗi, anh...

368
00:13:06,912-->00:13:09,113
Anh ăn đi, Shidou.

368
00:13:09,512-->00:13:11,513
Ừ, em ăn đi.

368
00:13:21,112-->00:13:24,113
Em... Nói thế nào nhỉ...

368
00:13:24,812-->00:13:26,113
Ngon thật đấy.

368
00:13:27,112-->00:13:29,313
Phòng này sang thật đấy.

368
00:13:30,112-->00:13:35,113
Có thể là do anh ở đây.

368
00:13:36,712-->00:13:42,113
Khi anh ở bên em...
Tất cả mọi thứ dường như 
tốt hơn nhiều so với bình thường.

368
00:13:42,812-->00:13:44,113
Tohka...

368
00:13:48,112-->00:13:49,513
Là do cùng với anh à?

368
00:13:51,312-->00:13:52,313
Ừ.

368
00:13:58,112-->00:14:01,113
Anh no rồi. Đồ ăn ngon thật đấy.

368
00:14:02,112-->00:14:05,113
Các loại rau Tempura rất thơm ngon.

368
00:14:05,312-->00:14:06,513
Anh có làm được như thế không, Tohka?

368
00:14:06,812-->00:14:11,313
Anh có thể làm Tempura,
nhưng chắc là không ngon như ở đây đâu.

368
00:14:11,812-->00:14:14,113
Vậy sao? Tiếc thật đấy.

368
00:14:17,112-->00:14:19,113
Đây là những gì Reine-san sắp đặt sẵn sao?

368
00:14:20,112-->00:14:26,513
Shidou. Nệm của chúng ta liền sát nhau.
Tại sao vậy?

368
00:14:29,112-->00:14:31,113
Anh đâu biết được?

368
00:14:31,512-->0:14:37,113
Điều đó có nghĩa là...
Em có thể ngủ với anh sao?

368
00:14:37,312-->00:14:38,513
Không phải.

368
00:14:39,112-->00:14:42,113
Nhìn trông giống vậy thật, nhưng mà...

368
00:14:42,312-->00:14:43,813
Thật sao?

368
00:14:44,112-->00:14:46,113
Em có thể ngủ với anh chứ?

368
00:14:49,912-->00:14:54,113
Dường như tim em bắt đầu đập rất nhanh.

368
00:14:55,112-->00:14:57,313
Tohka, có nghiêm trọng không? 

368
00:14:57,812-->00:14:59,513
Nghiêm trọng? Tất nhiên rồi!

368
00:15:00,112-->00:15:04,113
Mà có lẽ giờ vẫn còn sớm lắm.

368
00:15:04,512-->00:15:09,113
Em nói đúng.
Em muốn đi dạo một chút không?

368
00:15:09,512-->00:15:10,813
Ý hay đấy!

368
00:15:11,113-->00:15:17,113
Em vẫn chưa buồn ngủ đâu.
Chưa kể còn nhiều chỗ chúng ta chưa xem nữa.

368
00:15:17,312-->00:15:18,513
Em nói đúng.

368
00:15:19,112-->00:15:22,513
Chúng ta đã đến đây rồi,
hãy đi ngắm cảnh nhiều nhất có thể đi.

368
00:15:24,512-->00:15:28,113
Và cũng hy vọng là 
cô ấy sẽ quên việc ngủ chung với mình.

368
00:15:28,512-->00:15:30,113
Anh vừa nói gì?

368
00:15:30,312-->00:15:33,313
Không có gì!
Nào, đi thôi, Tohka!

368
00:15:33,912-->00:15:35,513
Được, đi thôi!


368
00:15:50,112-->00:15:51,113


368
00:15:50,112-->00:15:51,113

