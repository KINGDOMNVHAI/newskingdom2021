﻿0
00:00:04,112-->00:00:08,513
Tohka! Ăn sáng nhanh lên rồi còn đi học nào!

1
00:00:08,812-->00:00:09,813
Jiiiii~

2
00:00:09,912-->00:00:12,113
Kotori, em đeo nơ chưa đấy?

3
00:00:12,912-->00:00:13,913
Jiiiii~

4
00:00:14,112-->00:00:19,513
Chán thật! Dù đang trong trò chơi,
mình còn bận rộn hơn thường lệ!

5
00:00:19,813-->00:00:21,513
Chẳng ra thể thống gì cả!

6
00:00:22,112-->00:00:23,113
Jiiiii~

7
00:00:26,112-->00:00:27,913
Ra vậy.

8
00:00:28,112-->00:00:32,513
Gì vậy Maria? 
Cậu chăm chú xem gì nãy giờ vậy?

9
00:00:32,812-->00:00:35,513
Tớ đang xem hành vi của cậu, Shidou.

10
00:00:35,912-->00:00:42,513
Tớ so sánh các dữ liệu hành động tớ có
và xác minh tính chính xác 
của những dữ liệu này.

11
00:00:43,112-->00:00:48,113
Tớ không hiểu lắm, 
nhưng cậu đã chuẩn bị đi học chưa đấy?

12
00:00:49,112-->00:00:51,113
Vâng. Việc chuẩn bị đã hoàn tất.

13
00:00:51,312-->00:00:56,513
Thế à? Tuyệt thật đấy, Maria. 
Cậu biết tự giác đấy.

14
00:00:57,112-->00:01:01,513
Vâng. Tớ đã chọn quần lót đẹp
để đề phòng tình huống bất ngờ.

16
00:01:02,112-->00:01:04,513
Tại sao cậu phải làm thế?

17
00:01:04,912-->00:01:10,512
"Luôn luôn phải chuẩn bị vì không biết
chuyện ấy sẽ xảy ra lúc nào. 
Đặc biệt là trong trường học."

18
00:01:10,812-->00:01:15,513
Tỷ lệ xuất hiện "biến thái-may mắn"
của Shidou là rất khác thường"

19
00:01:16,112-->00:01:17,313
Đó là những gì Tokisaki Kurumi đã dạy tớ.

20
00:01:17,812-->00:01:22,713
Kurumi? Nhưng những thứ đó không đúng đâu Maria.

21
00:01:22,912-->00:01:27,113
Dù sao, chúng ta phải đi học thôi.

22
00:01:27,512-->00:01:29,113
Vâng. Tớ hiểu rồi.

23
00:01:30,112-->00:01:32,113
Đúng là một buổi sáng vất vả.

24
00:01:33,112-->00:01:36,813
Này Tohka, không nhanh lên 
là trễ giờ học đấy!

25
00:01:37,112-->00:01:39,113
Em nghe thấy không đấy?

25
00:01:40,112-->00:01:44,113
Nhanh lên đi, Tohka!

25
00:01:45,112-->00:01:50,913
Shidou hướng dẫn các bên thứ ba
thường xuyên hơn những gì ghi trong dữ liệu.

25
00:01:51,112-->00:01:55,513
Có lẽ điều này là do tình hình hiện tại?

25
00:01:56,112-->00:02:01,113
Không, đơn giản hơn, 
có thể các dữ liệu không đầy đủ.

25
00:02:02,112-->00:02:05,113
Tôi nghĩ mình nên quan sát đầy đủ hơn.

25
00:02:05,313-->00:02:06,513
Kết thúc.

25
00:02:09,512-->00:02:12,113
Giờ nghỉ trưa rồi à?

25
00:02:12,912-->00:02:18,113
Đi học dù đang trong trò chơi
đúng là cảm giác lạ.

25
00:02:19,112-->00:02:24,113
Và hơn nữa, bữa trưa luôn là
những lúc mình phải xử lý rắc rối.

25
00:02:25,112-->00:02:26,113
Jiiiii~

25
00:02:27,912-->00:02:33,513
Shidou ở trường dường như ít nói chuyện
với bạn bè có trong dữ liệu.

25
00:02:34,112-->00:02:38,713
Tôi đoán là vì tất cả bạn bè đều là NPC.
(Non-Player Character)

25
00:02:38,912-->00:02:43,113
Cậu ấy không thích nói chuyện
với những nhân vật ảo.

25
00:02:43,312-->00:02:44,313
Kết thúc.

25
00:02:45,912-->00:02:49,513
Giờ mình làm gì nhỉ.

25
00:02:51,112-->00:02:54,313
Không, quan trọng nhất là
tối nay nên ăn gì.

25
00:02:54,712-->00:03:01,113
Không cần phải lo lắng quá.
Mình sẽ chọn món này.

26
00:03:02,112-->00:03:03,113
Jiiiii~

26
00:03:04,312-->00:03:09,713
Shidou sau giờ học
phát sinh nhiều nghi ngờ về hành động của mình,
theo những gì dữ liệu ghi lại.

26
00:03:09,912-->00:03:14,913
Tôi đoán đây là do không có
sự trao đổi thông tin với bạn bè.

26
00:03:15,112-->00:03:19,113
và cũng không có sự giúp đỡ từ Ratatoskr
về sự xuất hiện của Tinh Linh

26
00:03:19,313-->00:03:20,313
Kết thúc.

26
00:03:21,112-->00:03:24,113
Này, cậu ở đó à, Maria?

26
00:03:25,112-->00:03:28,113
Tớ ngạc nhiên khi cậu phát hiện ra đấy, Shidou.

26
00:03:28,512-->00:03:32,513
Cả ngày hôm nay, tớ có cảm giác
như bị ai đó theo dõi mọi lúc mọi nới.

26
00:03:33,912-->00:03:37,313
Vậy Maria, sao cậu lại đi theo tớ?

26
00:03:38,812-->00:03:42,113
Tớ hiểu rồi. Shidou, cậu rất nhạy cảm 
với sự hiện diện của những người khác.

26
00:03:43,112-->00:03:46,113
Đó là một thông tin mới. Ghi lại.

26
00:03:46,712-->00:03:52,113
Ghi... ghi lại à? Nghĩa là sao?

27
00:03:52,512-->00:03:56,113
Tớ đang theo dõi cậu
và so sánh với các dữ liệu mà tớ có.

28
00:03:56,512-->00:03:59,113
Tớ không hiểu lắm.

29
00:04:00,112-->00:04:06,113
Các hành động của cậu trong thế giới này 
khác với những người khác, 
dựa trên phỏng đoán từ các dữ liệu.

30
00:04:07,112-->00:04:13,513
Tớ cần cập nhật chính xác
và ghi đè lên dữ liệu với các mới.

38
00:04:14,112-->00:04:17,113
Tớ hiểu rồi. Có vẻ phức tạp nhỉ.

39
00:04:18,112-->00:04:21,513
Vâng. Tớ chỉ muốn tìm hiểu về cậu.

40
00:04:22,113-->00:04:25,913
Tớ thấy những dữ liệu đó
chẳng dựa vào bất kỳ nguồn gốc đáng tin nào cả.

41
00:04:26,113-->00:04:29,113
Tớ chỉ đang sống như thường nhật thôi.

42
00:04:29,513-->00:04:34,513
Đúng là không có bằng chứng thuyết phục
chỉ ra rằng các dữ liệu đó chính xác.

43
00:04:35,112-->00:04:37,113
Nó không thể tránh khỏi việc có sự khác biệt.

39
00:04:38,112-->00:04:41,113
Chính vì vậy, những hành động 
của cậu rất có ý nghĩa.

40
00:04:41,813-->00:04:43,113
Thế à?

41
00:04:44,113-->00:04:48,113
Nhưng dù cậu kiểm tra sự khác biệt,
nó sẽ có ích chứ?

42
00:04:48,313-->00:04:49,313
Tất nhiên.

43
00:04:50,112-->00:04:57,113
Những hành vi khác nhau thường xảy ra với
các tình huống đặc biệt và kích thích mạnh mẽ.

39
00:04:57,613-->00:05:02,113
Điều này cũng được áp dụng khi 
phát triển tình cảm.

40
00:05:03,113-->00:05:11,113
Tớ nghĩ nó có thể phỏng đoán được 
Shidou có thể cảm nhận tình yêu khi 
ghi nhận tất cả các mô hình dữ liệu.

41
00:05:11,513-->00:05:13,113
Thật vậy sao?

42
00:05:14,113-->00:05:16,913
Vậy điểm "khác nhau" là gì?

44
00:05:17,112-->00:05:20,513
Tớ muốn biết. Đối với ví dụ này:

45
00:05:21,512-->00:05:26,513
Theo các dữ liệu, 
Shidou quan tâm đến ecchi.

46
00:05:27,112-->00:05:31,113
Tớ đã quan sát và có thể thấy điều đó.

47
00:05:31,312-->00:05:32,513
Nghĩa là sao?

48
00:05:32,812-->00:05:35,513
Shidou quan tâm đến ecchi.

49
00:05:36,112-->00:05:40,513
Nói cách khác,
Shidou sẽ quan tâm...

50
00:05:40,712-->00:05:41,913
đến những thứ không đứng đắn.

51
00:05:42,112-->00:05:43,313
Sao cậu lại nghĩ thế?

52
00:05:43,812-->00:05:45,713
Đó là điều quan trọng tớ muốn xác minh.

53
00:05:45,912-->00:05:49,313
Không không không! 
Tại sao cậu lại muốn xác nhận việc này?

54
00:05:49,813-->00:05:53,313
Cho đến giờ, tớ không thấy bất kỳ
hành vi nào xác nhận nó, nhưng ...

55
00:05:53,513-->00:05:56,313
Tớ nghĩ việc Shidou không quan tâm 
hay quan tâm đến ecchi...

56
00:05:56,512-->00:06:01,513
sẽ có ảnh hưởng lớn đến quá trình
tạo ra tình yêu và làm cho nó phát triển.

57
00:06:02,112-->00:06:06,313
Trong thực tế, tớ đã ghi nhận 
nhiều trường hợp cả hai có liên quan chặt chẽ.

58
00:06:06,512-->00:06:11,113
Phải, đúng là thế, nhưng...

41
00:06:11,312-->00:06:14,513
Những bộ phim truyền hình,
những sản phẩm giải trí khác...

42
00:06:15,112-->00:06:18,113
đều mô tả mối liên hệ 
giữa tình yêu và tình dục.

43
00:06:18,512-->00:06:23,113
Tớ đã nhận thức được điều gì đó
liên quan đến ham muốn cơ bản trong con người.

44
00:06:23,812-->00:06:25,513
Nhưng mà Shidou, điểm khác nhau là gì?

45
00:06:26,112-->00:06:27,313
Đó là...

46
00:06:28,112-->00:06:32,113
Nhưng mà khoan đã! Đây không phải chuyện
có thể nói ở trên đường phố đông người thế này!

47
00:06:34,513-->00:06:37,113
Cậu ấy chạy mất rồi.

48
00:06:38,112-->00:06:41,513
Shidou, có gì đó cậu muốn giấu sao?

49
00:06:42,512-->00:06:44,113
Nó như thế nào nhỉ?

50
00:06:45,112-->00:06:49,113
Nếu vậy, mình cần phải tính toán cẩn thận hơn.

51
00:06:51,112-->00:06:54,113
Hôm nay đúng là một ngày cực kỳ vất vả.

52
00:06:54,512-->00:06:58,513
Maria dường như theo dõi mình mọi lúc.

53
00:06:59,112-->00:07:03,113
Mình cảm thấy kiệt sức.

54
00:07:03,812-->00:07:05,113
Mình nên đi tắm thôi.

60
00:07:07,512-->00:07:10,113
Có cái gì trong phòng tắm.

61
00:07:11,112-->00:07:13,113
Kotori để quên gì à?

62
00:07:13,312-->00:07:14,813
Đây là gì?

63
00:07:17,112-->00:07:20,513
Đây là... không thể nào!

64
00:07:21,112-->00:07:26,513
Ai lại để một... 
một quyển tạp chí khiêu dâm ở đây?

65
00:07:26,812-->00:07:28,113
Jiiiii~

66
00:07:28,312-->00:07:30,113
Đây là...

67
00:07:33,112-->00:07:38,313
Mình có nên đọc không?

68
00:07:38,813-->00:07:41,513
Mình nên xem hay nên bỏ qua nó?

69
00:07:42,112-->00:07:43,113
Jiiiii~

70
00:07:44,112-->00:07:49,513
Nhưng khoan đã... Đây rõ ràng
là một thứ đáng ngờ.

71
00:07:51,112-->00:07:53,513
Không, chắc mình nghĩ hơi quá...

72
00:07:54,112-->00:07:57,513
Nhưng ai lại để đây chứ?

73
00:07:58,112-->00:08:05,113
Giờ thì sao nào. 
Cậu sẽ xem nó hay vứt bỏ nó?

74
00:08:05,512-->00:08:08,513
Từ đó mình sẽ ghi vào dữ liệu.

81
00:08:09,112-->00:08:13,113
Nhưng... nhưng...

82
00:08:15,112-->00:08:16,113
Được rồi!

83
00:08:17,112-->00:08:20,113
Cậu ấy đã quyết định điều gì đó.
Cậu ấy định làm gì vậy?

84
00:08:24,112-->00:08:26,513
Chính xác. Chính xác rồi, phải không?

85
00:08:27,112-->00:08:31,113
Điều này quá rõ ràng.
Đó là một cái bẫy!

86
00:08:32,112-->00:08:33,113
Ra là vậy.

87
00:08:34,112-->00:08:38,513
Thất bại là do mình đã để Shidou 
biết kế hoạch của mình.

88
00:08:39,112-->00:08:42,513
Mình cần phải thay đổi cách tiếp cận.

90
00:08:46,512-->00:08:51,113
Mình không biết đang xảy ra chuyện gì nữa.

90
00:08:51,812-->00:08:57,113
Nghĩ lại thì... có lẽ mình đã để lãng phí nó...

90
00:08:57,812-->00:08:58,913
Không, không!

91
00:08:59,112-->00:09:02,113
Shidou, tớ vào được chứ?

92
00:09:02,512-->00:09:06,113
Ma... Maria? Được rồi.

98
00:09:07,912-->00:09:09,513
Có chuyện gì không?

99
00:09:10,112-->00:09:12,113
Mọi thứ đều bình thường.

100
00:09:12,512-->00:09:15,113
Nếu tớ bán khỏa thân,
đó sẽ là sở thích của cậu sao Shidou?

101
00:09:15,312-->00:09:17,513
Tớ không bao giờ nói thế!

102
00:09:18,112-->00:09:20,313
Vậy chúng ta sẽ để dành dịp khác nhé.

103
00:09:20,512-->00:09:22,513
Còn có dịp khác nữa sao?

104
00:09:23,112-->00:09:26,113
Vậy có chuyện gì thế?
Cậu muốn nói gì à?

105
00:09:26,312-->00:09:29,113
Vâng. Tớ muốn đưa cậu cái này.

106
00:09:30,112-->00:09:31,113
DVD à?

107
00:09:31,312-->00:09:35,313
Vâng. Đó là DVD ecchi. DVD đen đấy.

108
00:09:35,512-->00:09:37,113
Được rồi, dừng lại ngay!

109
00:09:37,112-->00:09:40,113
Đổi tên thì cũng như nhau cả thôi!

110
00:09:40,312-->00:09:41,713
Tớ nhầm rồi.

111
00:09:41,913-->00:09:42,913
Nó là đĩa Blu-ray.

108
00:09:43,112-->00:09:45,113
Cũng như nhau thôi!

109
00:09:46,112-->00:09:49,113
Gọi ngắn gọn, nó là một đĩa DVD.

110
00:09:49,512-->00:09:51,513
Đĩa DVD ecchi.

111
00:09:52,112-->00:09:56,113
Dĩ nhiên nó chỉ dành cho
những ai đủ 18 tuổi.

112
00:09:56,312-->00:09:58,513
Tớ không muốn lấy nó!

108
00:09:58,812-->00:10:02,113
Không thể được! Thật kỳ lạ!

109
00:10:02,513-->00:10:05,813
Tớ đã chọn nó sau khi phân tích 
chi tiết thị hiếu của cậu, Shidou.

113
00:10:06,112-->00:10:09,313
Người ta gọi là "DVD dành cho bạn."

114
00:10:09,512-->00:10:12,513
Làm sao cậu tìm ra được khẩu vị của tớ?

1
00:10:13,112-->00:10:15,513
Cậu không cần nói, không cần nói đâu!

1
00:10:16,112-->00:10:18,113
Tớ nghĩ nếu cậu không thể quyết định
bằng một phương tiện in ấn,

1
00:10:18,312-->00:10:21,113
Có lẽ với video sẽ làm cậu kích thích hơn.

1
00:10:21,512-->00:10:24,513
Quả nhiên người để tạp chí
trong phòng tắm là cậu, phải không?

1
00:10:25,112-->00:10:31,513
Vâng. Cậu đã vứt bỏ nó.
Tớ thấy cậu đang cố gắng kiềm chế.

1
00:10:31,913-->00:10:33,713
Đây là kết quả của việc
tìm cách có được thông tin chính xác.

1
00:10:33,912-->00:10:35,913
Cậu nói trực tiếp quá đấy!

368
00:10:36,112-->00:10:38,313
Ecchi là không được phép!

368
00:10:38,512-->00:10:40,513
Nó bị cấm sao?

368
00:10:41,112-->00:10:45,313
Có vẻ thực tế khác với dữ liệu.

368
00:10:46,112-->00:10:50,113
Dường như cậu đang bướng bỉnh 
từ chối đối tượng này.

368
00:10:51,112-->00:10:51,913
Và giả thiết được đặt ra là Shidou...

368
00:10:52,112-->00:10:53,113
cậu là một tên ecchi đích thực.

368
00:10:53,312-->00:10:55,113
Cậu đặt giả thiết gì vậy?

368
00:10:55,312-->00:10:59,113
Và tớ không muốn thế. 
Vì nó... không nên...

368
00:11:00,112-->00:11:02,513
Nếu cậu không thích 
những cuốn sách hay video...

368
00:11:03,112-->00:11:06,113
vậy chắc cậu thích chạm trực tiếp vào tớ.

368
00:11:06,312-->00:11:07,513
Cái gì?

68
00:11:08,112-->00:11:11,113
Thật bất ngờ khi nhìn thấy 
khuôn mặt xấu hổ của cậu.

368
00:11:11,512-->00:11:15,113
Cậu không hứng thú với cơ thể tớ sao?

368
00:11:15,512-->00:11:16,913
Chờ chút đã, Maria...

368
00:11:17,112-->00:11:19,113
Thế này... gần quá...

368
00:11:19,512-->00:11:22,513
Khi tớ tiếp cận cậu, cậu không thích sao, Shidou?

68
00:11:23,112-->00:11:25,113
Tớ không thích, nhưng ...

368
00:11:25,512-->00:11:27,513
Chờ đã Maria, đừng đè tớ...

368
00:11:30,912-->00:11:32,513
Cậu thấy sao, Shidou?

368
00:11:33,112-->00:11:34,913
Thế này không ổn, không ổn...

368
00:11:35,112-->00:11:37,113
Tớ có thể cảm thấy mọi thứ...

368
00:11:37,312-->00:11:38,712
Hãy làm những gì cậu muốn đi.

368
00:11:38,912-->00:11:39,913
Maria-san?

368
00:11:40,112-->00:11:45,113
Shidou, cậu có hứng thú đến tớ không?

368
00:11:45,512-->00:11:49,513
"Hứng thú"... Không, đó không phải thế.

368
00:11:50,112-->00:11:51,513
Ví dụ như ở đây.

368
00:11:52,112-->00:11:53,513
Không... không phải...

368
00:11:54,112-->00:11:57,113
Hay là ở đây?

368
00:11:57,812-->00:11:59,513
Cậu thấy sao, Shidou?

368
00:12:00,113-->00:12:03,113
Cậu không cảm thấy kích thích sao?

368
00:12:04,113-->00:12:06,313
Không tốt... không tốt chút nào...

368
00:12:07,112-->00:12:11,113
Maria, nếu chúng ta tiếp tục thế này 
sẽ không ổn đâu...

368
00:12:12,112-->00:12:13,513
Tớ thì thấy ổn.

368
00:12:14,112-->00:12:19,113
Tớ cũng đã có những thông tin như vậy,
vì vậy tớ nghĩ rằng tớ có thể 
làm cậu thỏa mãn, Shidou.

368
00:12:19,312-->00:12:24,513
Tớ đã nói đừng...
Việc này không được phép!

368
00:12:25,112-->00:12:28,513
Shidou, nếu cậu muốn, 
hãy cứ làm những gì cậu thích đi.

368
00:12:29,112-->00:12:32,513
Tớ hoàn toàn không phiền gì đâu.

368
00:12:33,112-->00:12:34,313
Ma... Maria?

368
00:12:35,112-->00:12:39,113
Nếu cậu nói thế thì...

368
00:12:40,112-->00:12:41,513
Hãy làm với tớ...

368
00:12:41,712-->00:12:43,113
những gì cậu thích.

368
00:12:45,812-->00:12:46,913
Không, không được!

368
00:12:47,112-->00:12:48,513
Dừng lại đi!

368
00:12:49,112-->00:12:50,613
Shidou?

368
00:12:51,112-->00:12:55,113
Shidou, tớ không đủ tốt sao?

368
00:12:56,112-->00:12:58,113
Cậu sai rồi! Không phải thế!

368
00:12:58,512-->00:13:00,513
Vậy lý do là gì?

368
00:13:01,112-->00:13:06,713
Để làm những việc đó phải đi từng bước!

368
00:13:07,112-->00:13:10,113
Thật vậy sao?

368
00:13:10,512-->00:13:11,813
Tớ hiểu rồi

368
00:13:12,112-->00:13:15,513
Cậu hiểu rồi à?

368
00:13:15,912-->00:13:19,513
Tớ đã kết luận quá vội vàng.

368
00:13:20,112-->00:13:23,513
Nếu tớ đã làm cậu khó chịu...

368
00:13:24,112-->00:13:27,513
Không! Tớ không thấy khó chịu gì cả, nhưng...

368
00:13:28,112-->00:13:31,113
Tớ hồi hộp lắm đấy biết không?

368
00:13:31,312-->00:13:34,513
Nếu vậy thì tớ rất mừng.

368
00:13:35,112-->00:13:36,913
Tớ làm tim cậu đập nhanh hơn à?

368
00:13:37,112-->00:13:44,513
Đúng vậy. Tớ hoàn toàn 
vướng vào kế hoạch của cậu, Maria.

368
00:13:44,912-->00:13:48,113
Vậy trường hợp này là "thành công" phải không?

368
00:13:48,312-->00:13:51,113
Thật là... cậu học những thứ này ở đâu thế?

368
00:13:52,112-->00:13:56,113
Thôi giờ trễ rồi. 
Cậu về phòng ngủ đi.

368
00:13:56,812-->00:13:57,813
Được rồi.

368
00:13:58,312-->00:14:01,113
Vậy chúc ngủ ngon nhé, Shidou.

368
00:14:01,812-->00:14:03,313
Ừ, chúc ngủ ngon.

368
00:14:05,112-->00:14:10,113
Maria làm cho mình hết hồn.

368
00:14:11,812-->00:14:14,513
Dù sao, cảm giác đó...

368
00:14:15,112-->00:14:18,513
Không, đủ rồi. Mình đi ngủ thôi.

368
00:14:26,112-->00:14:29,113
Chẳng hiểu sao mình không ngủ được.

368
00:14:30,112-->00:14:34,513
Giờ mình mới nhớ, DVD, không, là đĩa Blu-ray.

368
00:14:35,112-->00:14:37,513
Nó vẫn còn ở đó à?

368
00:14:38,112-->00:14:41,513
Mình chỉ xem chút thôi...

368
00:14:42,112-->00:14:44,113
Jiiiii~

368
00:14:47,112-->00:14:49,913
Rõ ràng cậu ta đang cố gắng che giấu

368
00:14:50,112-->00:14:53,513
Quả nhiên Shidou rất quan tâm đến ecchi.

368
00:14:54,112-->00:14:57,913
Thực sự tôi có thể thấy 
nhiều câu trả lời dễ hiểu,

368
00:15:58,112-->00:15:00,513
như nhịp tim tăng nhanh của cậu ta.

368
00:15:02,112-->00:15:06,113
Tuy nhiên, hành vi của Shidou
khác với lời nói.

368
00:15:06,512-->00:15:10,513
Để hiểu họ, tôi cần nhiều dữ liệu hơn.

368
00:15:11,112-->00:15:12,113
Kết thúc.

368
00:15:13,112-->00:15:14,513
Bổ sung thông tin.

368
00:15:15,112-->00:15:19,113
Trong lúc tiếp xúc trực tiếp
với Shidou, tôi cảm thấy một cảm giác lạ.

368
00:15:20,112-->00:15:25,113
Một cái gì đó giống như 
cảm giác thoải mái kèm theo đau nhói.

368
00:15:25,512-->00:15:31,513
Về vấn đề này, tôi nghĩ nên xác minh thêm.

368
00:15:31,813-->00:15:33,113
Kết thúc.

368
00:15:50,112-->00:15:51,113


368
00:15:50,112-->00:15:51,113


