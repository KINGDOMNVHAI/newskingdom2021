﻿0
00:00:02,512-->00:00:06,113
Mừng ngài đã về, chủ nhân! (Goshujin-sama).

1
00:00:07,812-->00:00:10,113
Chủ nhân, mời ngài ngồi!

2
00:00:11,112-->00:00:15,113
Khi chủ nhân chọn món xong, 
xin vui lòng gọi cho tôi.

3
00:00:16,112-->00:00:18,113
Xin lỗi vì sự chậm trễ, thưa chủ nhân.

4
00:00:19,112-->00:00:21,113
Đây là phần cơm trứng đặc biệt của ngài.

5
00:00:22,112-->00:00:27,113
Bây giờ, tôi sẽ dùng phép thuật
để món cơm trứng ngon hơn.

6
00:00:28,112-->00:00:31,113
Hãy trở nên ngon hơn nào! Moe, moe kyun!

7
00:00:32,112-->00:00:38,113
Thưa chủ nhân, món ăn này vẫn còn nóng.
Để tôi thổi cho ngài nhé?

8
00:00:41,112-->00:00:43,113
Xin mời ngài thưởng thức!

9
00:00:44,112-->00:00:48,113
Moe moe, búa, bao, kéo...

10
00:00:49,112-->00:00:51,113
Tôi thua rồi.

11
00:00:52,112-->00:00:54,113
Sau đây, tôi sẽ vẽ hình lên món ăn nhé!

12
00:00:55,112-->00:00:58,113
Chủ nhân, ngài đi đâu vậy?

13
00:00:59,112-->00:01:01,113
Hẹn gặp lại, thưa chủ nhân!

14
00:01:05,112-->00:01:07,113
Chán thật...

15
00:01:08,112-->00:01:09,113
Có chuyện gì vậy, Kotori?

16
00:01:10,112-->00:01:15,113
Tôi tưởng đây là quán cà phê 
bình thường, hóa ra là Maid Cafe à.

17
00:01:16,112-->00:01:19,113
Tôi tự hỏi tại sao lại có người
làm được công việc này.

18
00:01:21,112-->00:01:25,113
Không tin được là có loại bánh 
không vị ngọt này.

19
00:01:26,112-->00:01:29,113
Thật mừng vì được ở nhà đấy!

20
00:01:32,112-->00:01:32,913
Tohka.

21
00:01:33,112-->00:01:35,113
Hở? Gì vậy Kotori?

22
00:01:36,112-->00:01:39,113
Chuyện gì mà cô thở dài vậy?

23
00:01:40,112-->00:01:43,113
Tôi đang suy nghĩ một chút...

24
00:01:45,112-->00:01:47,113
Cô suy nghĩ chuyện gì?

25
00:01:48,112-->00:01:51,113
Nếu cô đồng ý, cô có thể nói chuyện với tôi.

25
00:01:52,112-->00:01:54,513
Ratatoskr là một tổ chức 
chăm sóc Tinh Linh tốt.

25
00:01:55,112-->00:02:00,113
Nếu có gì làm phiền cô,
giúp đỡ là nhiệm vụ của chúng tôi.

25
00:02:01,112-->00:02:03,113
Không, mọi thứ đều ổn, nhưng ...

25
00:02:03,512-->00:02:06,113
Nếu cô không muốn nói, tôi sẽ không ép đâu.

25
00:02:07,112-->00:02:10,113
Nhưng cô có thể tin tưởng tôi, 
dù chỉ một chút thôi cũng được.

25
00:02:11,112-->00:02:14,113
Được rồi. Tôi sẽ nói.

25
00:02:15,112-->00:02:20,113
Những gì Tohka lo lắng
thường là về thức ăn hoặc Shidou.

25
00:02:20,512-->00:02:22,113
Tôi muốn có tiền.

25
00:02:23,112-->00:02:26,113
T...thật bất ngờ là cô quan tâm
đến vấn đề nghiêm túc và thực tế như vậy.

25
00:02:27,112-->00:02:29,113
Không, không có gì.

25
00:02:29,512-->00:02:32,113
À, phải rồi. Cô không có tiền 
để mua đồ ăn à?

25
00:02:33,112-->00:02:34,113
Đồ ăn đâu có vấn đề gì đâu.

25
00:02:35,112-->00:02:36,113
Ừ, phải rồi.

25
00:02:37,112-->00:02:39,513
Tiền à...

25
00:02:40,512-->00:02:44,113
Cô nói cô không có tiền, nhưng tôi đã cho
cô tiền trợ cấp hàng tháng, phải không?

25
00:02:45,112-->00:02:47,113
Cô tiêu hết tất cả tiền tháng này rồi à?

25
00:02:47,512-->00:02:48,513
Không, tôi vẫn còn.

25
00:02:49,112-->00:02:53,113
Để ăn bánh mì đậu mỗi ngày, 
tôi đã lên kế hoạch tiêu tiền.

25
00:02:53,512-->00:02:55,113
Tất cả đều được viết trong một cuốn nhật ký!

26
00:02:55,512-->00:02:56,513
Thật phi thường.

26
00:02:57,512-->00:03:01,113
Vậy cô cần tiền để mua gì?

26
00:03:02,112-->00:03:05,113
Tôi có việc phải làm.

26
00:03:05,512-->00:03:07,113
Tôi muốn có nhiều tiền hơn 
so với trợ cấp của tôi.

26
00:03:08,112-->00:03:09,113
Tại sao?

26
00:03:10,112-->00:03:13,513
À... là vì...

26
00:03:14,112-->00:03:15,113
Khó nói quá à?

26
00:03:15,512-->00:03:18,113
Đó là...

26
00:03:19,112-->00:03:21,113
Cô giữ bí mật với Shidou được không?

26
00:03:22,112-->00:03:23,113
Được rồi, không sao đâu.

26
00:03:24,112-->00:03:26,113
Tôi muốn mua một món quà...

26
00:03:26,312-->00:03:28,113
dành cho Shidou.

27
00:03:29,112-->00:03:33,513
Tôi đọc trong một cuốn sách, 
một món quà là một cái gì đó
để trao cho người thân và...

28
00:03:34,112-->00:03:36,113
người mà mình yêu.

29
00:03:37,112-->00:03:38,113
Ừ, đúng là vậy.

30
00:03:39,112-->00:03:44,113
Và như cuốn sách nói, một món quà
phải được mua bằng tiền bạn kiếm được.

31
00:03:44,512-->00:03:45,513
Hiểu rồi.

32
00:03:46,112-->00:03:48,513
Đó là lý do tại sao 
cô đột nhiên muốn có tiền à.

33
00:03:49,112-->00:03:52,113
Kotori, làm sao để kiếm tiền vậy?

34
00:03:53,112-->00:03:57,513
Nếu là một công việc bình thường 
thì là công việc bán thời gian.

35
00:03:58,112-->00:04:00,113
Bán thời gian sao?

36
00:04:00,513-->00:04:02,113
Đó là một công việc làm thêm.

37
00:04:02,313-->00:04:05,113
Mọi người đi làm thêm để có tiền.

38
00:04:05,512-->00:04:09,113
Ra là vậy! Được, tôi sẽ đi làm thêm!

39
00:04:10,112-->00:04:11,113
Ừ, cố gắng lên nhé.

40
00:04:11,313-->00:04:12,113
Ừ!

41
00:04:13,113-->00:04:16,113
Vậy giờ làm thêm là làm những gì?

42
00:04:16,513-->00:04:19,113
Câu hỏi đó khó trả lời đấy...

43
00:04:19,512-->00:04:22,113
Tôi chưa bao giờ đi làm vì tôi 
vẫn còn học trung học mà.

39
00:04:23,112-->00:04:26,113
Vậy tôi có thể làm gì?

40
00:04:26,513-->00:04:29,513
Sẽ tốt hơn nếu cô có ai đó để hỏi.

41
00:04:30,113-->00:04:31,113
Cô có nghĩ nên hỏi Shidou không?

42
00:04:31,313-->00:04:34,113
Không được! Tôi không thể để Shidou biết!

43
00:04:34,512-->00:04:38,113
Nhưng nếu cô nói, có khi cô sẽ
kiếm được công việc đấy.

39
00:04:39,512-->00:04:40,513
Chào buổi chiều.

40
00:04:41,113-->00:04:42,113
Đây rồi!

41
00:04:42,513-->00:04:44,113
Hở? Gì vậy?

42
00:04:47,113-->00:04:50,113
Tớ ngạc nhiên khi các cậu
muốn có việc làm thêm đấy.

43
00:04:51,112-->00:04:55,113
Cảm ơn Rinne đã kiếm chỗ làm thêm nhé.

44
00:04:56,112-->00:05:01,113
Chỉ là một người bạn đã hỏi tớ 
có quen ai cần việc làm thêm không thôi.

45
00:05:02,112-->00:05:04,513
Tôi không bao giờ tưởng tượng 
là một Maid Cafe.

46
00:05:05,112-->00:05:08,513
Nhưng mấy bộ đồ này dễ thương thật đấy!

47
00:05:09,112-->00:05:12,513
Vậy à. Ừ, mấy bộ đồ này dễ thương thật.

48
00:05:13,112-->00:05:16,113
Xin lỗi, Kotori-chan. Chị nên
nói cho em biết trước.

49
00:05:16,512-->00:05:18,513
Không sao đâu.

50
00:05:19,112-->00:05:24,113
Maid Cafe không sao cả. 
Chỉ là em ngạc nhiên thôi.

51
00:05:24,512-->00:05:27,113
Em làm những công việc 
căng thẳng quen rồi mà.

52
00:05:27,512-->00:05:31,513
Kotori, bộ đồ hợp với cô lắm! Cô rất 
dễ thương với những đường ren đó đấy!

53
00:05:32,112-->00:05:34,513
Thật à? Cô cũng rất dễ thương đấy Tohka!

54
00:05:35,112-->00:05:36,513
Cảm ơn nhé!

55
00:05:37,112-->00:05:40,113
Rinne, cảm ơn cậu đã may 
tất cả bộ đồ này cho bọn tớ.

56
00:05:40,512-->00:05:44,513
Đừng bận tâm. Cậu muốn mua một món quà 
cho Shidou, phải không Tohka-chan?

57
00:05:45,112-->00:05:45,913
Phải!

58
00:05:46,112-->00:05:50,513
Tớ chắc chắn cậu ấy sẽ rất vui
khi nhận được quà của cậu đấy Tohka-chan.

59
00:05:51,112-->00:05:51,913
Thật không?

60
00:05:52,112-->00:05:52,913
Ừ.

60
00:05:53,112-->00:05:56,513
Gác chuyện đó sang một bên.
Tôi đi theo liệu có sao không?

60
00:05:57,112-->00:06:00,513
Có sao đâu! Nếu là Kotori-chan
thì sẽ có nhiều khách hơn đấy!

41
00:06:01,112-->00:06:02,513
Cảm ơn nhé, Rinne-oneechan!

42
00:06:03,112-->00:06:07,113
Thực ra là để hỗ trợ. Mình không thể 
yên tâm khi để Tohka đi một mình.

43
00:06:08,112-->00:06:12,113
Và cũng có thể mình sẽ mua một món quà.

44
00:06:13,112-->00:06:15,513
Thật vui khi làm việc với Rinne và Kotori!

45
00:06:16,112-->00:06:17,513
Chúng ta sẽ cố gắng làm việc nhé!

46
00:06:18,112-->00:06:18,913
Ừ!

47
00:06:20,113-->00:06:22,113
Tôi cũng phải làm việc vì Shidou.

48
00:06:24,112-->00:06:28,113
Đúng như Rinne nói. Mình phải làm việc
chăm chỉ và kiếm được tiền!

49
00:06:29,112-->00:06:33,113
Dù sao chúng ta đã chấp nhận 
phải làm hết khả năng của mình.

50
00:06:35,112-->00:06:40,113
Tôi thắng rồi phải không?
Ngài có muốn chơi nữa không?

51
00:06:41,112-->00:06:43,513
Ngài không muốn chơi nữa à?

52
00:06:44,112-->00:06:45,113
Thật là xấu hổ.

53
00:06:45,512-->00:06:49,113
Sau khi đã chơi rất nhiều,
tôi sẽ cho một cái bắt tay...

54
00:06:50,112-->00:06:52,113
Vâng, Kyuu~!

57
00:06:53,112-->00:06:56,113
Thăm một lần nữa nhé? Thưa chủ nhân.

58
00:06:57,112-->00:06:59,113
Xin lỗi vì sự chậm trễ, chủ nhân.

60
00:07:00,512-->00:07:04,113
Đây ạ. Đó là một cái 
parfait tràn đầy tình yêu.

61
00:07:05,112-->00:07:07,513
Ngài muốn tôi cho ngài ăn à?

62
00:07:08,112-->00:07:12,113
Được rồi. Chỉ một miếng thôi đấy.

63
00:07:12,512-->00:07:13,513
Nói "Aaa" nào.

64
00:07:14,112-->00:07:16,113
Có ngon không, chủ nhân?

65
00:07:18,312-->00:07:21,113
Kotori-chan và Tohka-chan đang
làm việc khá chăm chỉ.

66
00:07:22,112-->00:07:25,113
Nhìn thấy cảnh này thật bất ngờ.

67
00:07:28,112-->00:07:30,513
"Một khi em đã quen thì 
công việc này dễ dàng đến bất ngờ."

68
00:07:31,112-->00:07:33,513
Công việc này không còn gì khác à?

69
00:07:34,112-->00:07:36,513
Thôi nào, thôi nào, Kotori-chan. 
Cười lên, cười lên.

70
00:07:37,112-->00:07:41,113
Hầu gái phải làm việc với
nụ cười đón nhận tình yêu.

71
00:07:42,112-->00:07:48,513
"Nụ cười đón nhận tình yêu, làm tất cả 
mọi thứ chủ nhân yêu cầu" phải không?

72
00:07:49,112-->00:07:50,113
Đúng đấy, đúng đấy!

73
00:07:51,112-->00:07:53,113
Kotori, nụ cười rất quan trọng đấy!

74
00:07:54,112-->00:07:57,113
Nếu chúng ta không mỉm cười thì sẽ 
không thể khiến người khác mỉm cười được!

75
00:07:58,112-->00:07:58,913
Tôi hiểu rồi!

81
00:08:00,112-->00:08:03,113
Sau tất cả, nếu khách hàng 
nở nụ cười thì chúng ta đã làm tốt!

82
00:08:03,512-->00:08:06,113
Hình như hơi khác với những gì tớ vừa nói.

83
00:08:07,112-->00:08:11,113
Một khách hàng đang gọi kìa. Tớ đi cho!

84
00:08:12,112-->00:08:13,513
Em cũng cố gắng lên, Kotori-chan.

85
00:08:14,112-->00:08:19,113
Tất nhiên. Một khi em chấp nhận 
một cái gì đó, em sẽ làm hết khả năng.

86
00:08:20,112-->00:08:25,113
Mặc dù đã quen nhưng nó nhàm chán 
như bất kỳ điều bình thường khác.

87
00:08:25,512-->00:08:27,513
Nở một nụ cười à?

88
00:08:29,112-->00:08:31,113
Có cần phải làm gì đó khác nữa không?

89
00:08:32,112-->00:08:32,913
Kotori, Kotori!

90
00:08:33,112-->00:08:34,513
Chuyện gì vậy?

91
00:08:35,512-->00:08:38,113
"Tsundere" là gì vậy?

92
00:08:39,112-->00:08:41,113
Kotori, "Tsundere" là gì vậy?

93
00:08:41,512-->00:08:43,113
Không cần phải lặp lại nó đâu. 
Tôi nghe rồi.

94
00:08:44,112-->00:08:46,113
Nhưng sao cô đột nhiên hỏi vậy?

95
00:08:46,512-->00:08:52,113
Một khách hàng yêu cầu tôi 
diễn xuất như một Tsundere.

96
00:08:53,112-->00:08:55,513
Và Rinne có vẻ đang bận 
nên tôi hỏi cô.

97
00:08:56,112-->00:08:58,113
Sao cô lại hỏi tôi chứ?

98
00:08:58,112-->00:09:00,113
Vậy "Tsundere" là gì?

99
00:09:01,112-->00:09:10,113
Là cách thể hiện thái độ phủ nhận
của một số người. Mặc dù thật sự
họ rất thích điều đó.

101
00:09:11,112-->00:09:13,113
Tôi hiểu rồi

102
00:09:14,112-->00:09:17,513
Gì vậy? Sao cô nhìn tôi ghê vậy?

103
00:09:18,112-->00:09:19,513
Ra là vậy...

104
00:09:20,112-->00:09:22,113
Tại sao cô nhìn tôi với ánh mắt đó?

105
00:09:23,112-->00:09:26,113
Tôi sẽ làm được...

106
00:09:27,112-->00:09:28,113
Ờ... đi đi.

107
00:09:29,312-->00:09:31,113
Xin lỗi vì sự chậm trễ, thưa chủ nhân!

108
00:09:33,512-->00:09:36,113
Có mỗi việc gọi món mà phải 
suy nghĩ nhiều thế sao?

109
00:09:37,112-->00:09:38,513
Đúng là đồ ngốc!

109
00:09:39,112-->00:09:42,113
Đó có phải là bản chất của con người không?

110
00:09:43,112-->00:09:47,113
Mình tạo cho cô ấy ấn tượng à?

110
00:09:47,512-->00:09:50,113
Thật sự chỉ có vậy thôi à?

111
00:09:51,112-->00:09:55,113
Vâng, điều này có thể là điều tốt nhất
có thể cho một người như ngài.

108
00:09:56,112-->00:09:59,113
Khi tôi quay lại lần nữa,
ngài phải gọi món đấy.

110
00:10:00,112-->00:10:03,113
Nếu tôi mà được gọi không lý do,
tôi sẽ không đến đâu!

113
00:10:04,112-->00:10:07,513
Chẳng giống tsundere gì cả, chỉ giống
bắt chước những gì tôi làm thôi!

114
00:10:08,112-->00:10:13,113
Nhưng tôi tưởng Tsundere là
phải hành động như cô chứ Kotori?

1
00:10:14,112-->00:10:15,513
Tất nhiên là không rồi!

1
00:10:16,112-->00:10:18,113
Các cậu đang làm gì vậy?

1
00:10:18,512-->00:10:19,513
Trở thành tsundere.

1
00:10:20,112-->00:10:21,113
Gì cơ?

1
00:10:22,112-->00:10:25,113
Có một khách hàng yêu cầu 
cô ấy có phong cách tsundere.

1
00:10:26,112-->00:10:29,113
Tớ tưởng ở đây không có dịch vụ đó...

1
00:10:30,112-->00:10:31,113
Nhờ có Kotori, ông ta rất vui đấy!

368
00:10:32,112-->00:10:36,113
V... vậy à. Dù sao cô cũng gặp may thôi.

368
00:10:36,512-->00:10:39,113
Mặc dù tôi có hơi cảm xúc lẫn lộn.

368
00:10:40,112-->00:10:42,513
À, nhưng điều này cũng khá hữu ích, phải không?

368
00:10:43,112-->00:10:43,913
Ý em là sao?

368
00:10:44,112-->00:10:49,113
Nếu chủ nhân yêu cầu, bọn em có thể
cho những ai muốn tình yêu tham gia.

368
00:10:50,112-->00:10:53,113
Nếu họ yêu cầu chị làm tsundere, 
chị cũng sẽ tham gia.

368
00:10:53,512-->00:10:57,513
Nếu chị làm thế, em chắc chắn
khách hàng sẽ rát vui!

368
00:10:58,112-->00:11:01,113
Điều đó có thể đúng, nhưng có ổn không
khi làm vậy mà không xin phép?

368
00:11:02,112-->00:11:08,113
Được mà. Dù sao chúng ta cũng khiến
khách hàng nở nụ cười thôi.

368
00:11:08,513-->00:11:11,113
Ngoài ra, những công việc giống nhau
khiến em cảm thấy nhàm chán lắm.

368
00:11:15,112-->00:11:19,113
Làm khách hàng vui là tốt, nhưng đừng 
quá đà và làm phiền họ, được chứ?

68
00:11:20,112-->00:11:21,113
Tất nhiên rồi!

368
00:11:21,512-->00:11:25,113
Chúng ta có thể làm bất cứ điều gì
để khách hàng nở nụ cười!

368
00:11:25,512-->00:11:28,513
Quả nhiên cậu đang hiểu sai gì đó rồi...

368
00:11:29,112-->00:11:32,113
Có vẻ như họ đang gọi. Tôi sẽ ra.

368
00:11:32,512-->00:11:34,113
Ừ! Kotori, cố gắng lên!

68
00:11:35,512-->00:11:38,513
Ngài cần gì, thưa chủ nhân?

368
00:11:40,112-->00:11:42,113
Vâng? Dojikko? (cô gái vụng về)

368
00:11:42,512-->00:11:45,113
Rinne, Dojikko là gì?

368
00:11:45,512-->00:11:50,113
À... Là những người vụng về, hậu đậu,
ngốc nghếch. Có lẽ vậy chăng?

368
00:11:51,112-->00:11:53,113
À, ra vậy.

368
00:11:54,112-->00:11:56,113
Dojikko phải không?

368
00:11:57,112-->00:12:01,113
Kotori, sao cô nhìn tôi ghê thế?

368
00:12:03,113-->00:12:07,113
Aaa! Tôi ngã mất rồi!

368
00:12:08,113-->00:12:10,113
Không sao đâu! Em không sao cả!

368
00:12:13,513-->00:12:17,113
Em đã nói em không sao rồi mà!

368
00:12:19,113-->00:12:21,113
Kotori, cô làm tốt lắm!

368
00:12:22,113-->00:12:25,513
Thế à? Đó là vì tôi có 
một ví dụ rất điển hình.

368
00:12:25,512-->00:12:27,113
Thật à?

368
00:12:27,312-->00:12:28,313
Đúng vậy.

368
00:12:28,513-->00:12:36,113
Kotori-chan, em đã cố gắng,
nhưng có chắc là em không vấp ngã
khi đang mang đồ uống đấy chứ?

368
00:12:36,512-->00:12:40,113
Em sẽ không làm như thế! 
Dojikko đâu cần đến mức đó!

368
00:12:41,112-->00:12:43,113
Được rồi! Vậy thì không sao.

368
00:12:43,512-->00:12:45,113
Tuyệt thật đấy, Kotori.

368
00:12:45,512-->00:12:47,513
Được! Tôi cũng sẽ cố gắng như cô!

368
00:12:48,113-->00:12:50,513
Không có yêu cầu gì 
tôi không đáp ứng được cả!

368
00:12:51,112-->00:12:53,113
Ừ, chúng ta hãy làm cùng nhau 
và giành chiến thắng!

368
00:12:58,112-->00:13:03,113
Hãy trở nên ngon hơn nào! Moe, moe nyan, nyan!

368
00:13:04,113-->00:13:07,113
Với phép thuật này, nó đã trở thành 
một cái bánh siêu ngon, nyan.

368
00:13:07,512-->00:13:11,113
Cuối cùng, mời chủ nhân tận hưởng 
thời gian của ngài ở đây, nyan.

368
00:13:12,112-->00:13:13,513
Neko-mimi Maid (hầu gái đeo tai mèo)?

368
00:13:14,112-->00:13:17,513
Đúng như mong đợi từ một Maid Cafe,
luôn có đầy đủ các mặt hàng.

368
00:13:18,112-->00:13:20,113
Tại sao em đeo kính?

368
00:13:20,512-->00:13:24,113
Em đeo vì em được yêu cầu
biến thành kuudere đeo kính.

368
00:13:25,112-->00:13:27,113
Tất nhiên là kính giả.

368
00:13:27,513-->00:13:31,113
Có vẻ như chỗ này đã thành 
một Cosplay Cafe, phải không?

368
00:13:32,112-->00:13:36,113
Được rồi. Nếu chúng ta làm thế này,
khách hàng sẽ đến đông hơn đấy.

368
00:13:36,513-->00:13:41,113
Ừ, đúng là khách hàng đã tăng lên, nhưng...

368
00:13:41,512-->00:13:46,513
Thay vào đó, khách hàng càng lúc
càng có những yêu cầu kỳ lạ hơn.

368
00:13:47,112-->00:13:49,113
Điều gì xảy ra vậy 2 người-nyan?

368
00:13:49,512-->00:13:54,113
To... Tohka-chan, nếu cậu không tiếp khách, 
cậu có thể nói chuyện bình thường mà.

368
00:13:54,512-->00:13:55,513
Nyan! Tại tớ quen rồi!

368
00:13:56,112-->00:14:00,113
Được rồi! Chúng ta 
vẫn đáp ứng được hết mà!

368
00:14:00,512-->00:14:01,513
Đúng vậy, nhưng...

368
00:14:02,112-->00:14:06,113
Ồ! Hình như có khách mới đến. Để tôi ra.

368
00:14:07,113-->00:14:09,113
Mừng ngài đã về, chủ nhân!

368
00:14:10,112-->00:14:11,513
Mời ngài ngồi đây ạ.

368
00:14:12,112-->00:14:18,113
Chúng tôi có phục vụ các loại dere 
theo ý muốn. Ngài có yêu cầu gì không?

368
00:14:18,512-->00:14:21,113
Ngài làm ơn nhắc lại được không ạ?

368
00:14:21,512-->00:14:24,113
Hở? Imouto Maid? (Hầu gái em gái)

368
00:14:24,512-->00:14:29,113
Nghĩa là tôi phải gọi ngài 
là Onii-chan, phải không?

368
00:14:32,112-->00:14:35,113
Ngài có thể ra yêu cầu nào khác không?

368
00:14:36,112-->00:14:41,113
Tôi có thể làm Neko-mimi Maid 
hoặc Dojikko Maid mà.

368
00:14:41,512-->00:14:43,113
Chuyện gì đã xảy ra vậy Kotori-chan?

368
00:14:44,112-->00:14:47,113
Không, chỉ là có lý do 
tôi không thể đóng vai Imouto...

368
00:14:48,512-->00:14:50,113
Tôi đã nói rồi đấy!

368
00:14:51,112-->00:14:52,513
Kotori-chan?

368
00:14:53,112-->00:14:56,113
Thật không may, em không thể 
thực hiện yêu cầu này!

368
00:14:56,512-->00:14:59,113
N... này, Kotori-chan, tại sao không?

368
00:14:59,512-->00:15:01,113
Chúng ta đã nói chúng ta sẽ
thực hiện yêu cầu của mọi người mà.

368
00:15:01,512-->00:15:04,513
Em đã nói rồi đấy! Em sẽ không làm đâu!

368
00:15:05,112-->00:15:06,513
Ko... Kotori-chan, bình tĩnh lại đi!

368
00:15:06,713-->00:15:09,113
Vì vậy, tôi quyết định không làm đâu!

368
00:15:10,112-->00:15:12,113
Hả? Tsundere?

368
00:15:12,512-->00:15:15,113
Đừng đùa với tôi! Không phải
tôi đang diễn đâu!

368
00:15:16,112-->00:15:18,113
Đừng, Kotori-chan! 
Em không thể nói như thế!

368
00:15:18,512-->00:15:22,513
Chỉ có một người tôi gọi Onii-chan là...

368
00:15:23,112-->00:15:25,113
Xin... xin lỗi, đó là... xin chờ một chút!

368
00:15:25,512-->00:15:29,113
Chủ nhân! Xin ngài đừng đi!

368
00:15:32,112-->00:15:38,113
Xin lỗi, Rinne-oneechan. 
Em đã quá tức giận...

368
00:15:38,512-->00:15:41,113
Em không cần phải la chủ nhân như thế.

368
00:15:41,512-->00:15:43,513
Vâng, em biết.

368
00:15:44,112-->00:15:44,913
Vậy tại sao...

368
00:15:45,112-->00:15:45,913
Nhưng mà...

368
00:15:46,512-->00:15:51,113
Chỉ có một người duy nhất em gọi
là Onii-chan là Shidou thôi.

368
00:15:51,512-->00:15:53,113
Kotori-chan...

368
00:15:55,112-->00:15:56,513
Xảy ra chuyện gì vậy?

368
00:15:57,112-->00:15:59,113
Xin lỗi! Tớ vô ý quá!

368
00:16:00,112-->00:16:02,113
Khoan đã, Tohka-chan?

368
00:16:03,112-->00:16:06,113
Chẳng lẽ cậu đã ăn hết đồ ăn rồi sao?

368
00:16:06,512-->00:16:09,113
Phải. Tớ đã hỏi cậu rồi.

368
00:16:09,512-->00:16:11,113
Sao cậu có thể làm thế?

368
00:16:12,112-->00:16:17,113
Tohka, ngay cả khi cô đã hỏi, 
có những chuyện cô được phép 
và không được phép!

368
00:16:17,512-->00:16:19,113
Tôi xin lỗi.

368
00:16:19,512-->00:16:22,113
Thôi chuyện lỡ rồi. 
Giờ chúng ta dọn dẹp ở đây đi.

368
00:16:23,112-->00:16:27,113
Tớ sẽ giúp cậu Tohka, 
lấy nước và khăn lau đi.

8
00:16:28,112-->00:16:30,113
Ừ, được rồi.

368
00:16:34,112-->00:16:35,513
Mình phải làm một cái gì đó!

8
00:16:36,112-->00:16:41,113
Chúng ta đã cố gắng đến giờ này.
Nhưng giờ lại vướng những chuyện này.

368
00:16:41,512-->00:16:43,513
Nước lau nhà và khăn đây!

368
00:16:44,112-->00:16:45,113
À, chờ một chút, Tohka-chan!

368
00:16:45,512-->00:16:47,513
Sàn nhà đang ướt đấy! Đừng có chạy!

368
00:16:47,712-->00:16:49,113
Véo!!!!!!!

368
00:16:51,112-->00:16:52,113
Lạnh quá!

368
00:16:55,112-->00:16:58,113
Tại sao cứ có chuyện xảy ra vậy?

368
00:16:59,112-->00:17:01,113
Xin lỗi. Là lỗi của tôi.

368
00:17:03,112-->00:17:05,113
Ướt hết cả rồi...

368
00:17:05,512-->00:17:07,513
Tớ cũng ướt rồi...

368
00:17:08,112-->00:17:11,113
Chúng ta phải làm gì đây? Cứ thế này, 
chúng ta có lẽ bị đuổi việc đấy!

368
00:17:12,112-->00:17:15,113
Trước mắt, chúng ta hãy hỏi
xem có quần áo để thay không.

368
00:17:15,512-->00:17:17,113
Phải, cậu nói đúng

368
00:17:18,112-->00:17:20,113
Xin lỗi quản lý...

368
00:17:21,112-->00:17:22,113
Chuyện là thế này...

368
00:17:23,112-->00:17:26,113
Gọi cả 3 chúng tôi sao?

368
00:17:27,112-->00:17:28,113
Vâng...

368
00:17:35,112-->00:17:38,113
Họ đã ngưng dịch vụ đó rồi.

368
00:17:39,112-->00:17:40,113
Vậy à.

368
00:17:41,112-->00:17:43,113
Nhưng mà quản lý cũng không được 
tức giận như vậy phải không?

368
00:17:44,112-->00:17:46,113
Khách hàng đã rất vui mà phải không?

368
00:17:46,512-->00:17:47,513
Chẳng có ích gì đâu!

368
00:17:48,112-->00:17:52,113
Chúng ta đã làm mất khách hàng,
đồ ăn hư hỏng...

368
00:17:53,112-->00:17:55,113
Phải, đúng là vậy, nhưng mà...

368
00:17:55,512-->00:18:00,113
Có lẽ nguyên nhân gây ra sự khó chịu 
cho mọi người là do tớ đã 
dẫn mọi người đến đây.

368
00:18:01,112-->00:18:02,113
Đó là...

368
00:18:02,312-->00:18:06,113
làm tăng gánh nặng cho các nhân viên khác.

368
00:18:08,112-->00:18:09,513
Nhưng chúng ta phải làm gì đây?

368
00:18:10,112-->00:18:16,113
Tất cả những gì chúng ta có
là bộ đồng phục hầu gái chúng ta 
đã nhận thay cho thanh toán.

368
00:18:17,112-->00:18:21,113
Chúng ta nhận nó vì họ nói 
rửa các chất tẩy rửa sẽ rất đắt.

368
00:18:23,112-->00:18:26,113
Nhờ vậy mà chúng ta không bị bắt đền.

368
00:18:27,112-->00:18:29,113
Phải, đúng là vậy, nhưng mà...

368
00:18:29,512-->00:18:34,113
Nếu chúng ta không có tiền, 
chúng ta không thể mua quà cho Shidou.

368
00:18:34,512-->00:18:39,113
Đúng vậy. Mặc dù chuyện không may,
nhưng chúng ta chẳng được nhận lương nữa.

368
00:18:40,112-->00:18:43,113
Tôi muốn tặng một món quà cho Shido.

368
00:18:44,112-->00:18:47,513
Bây giờ, tôi chẳng còn 
can đảm nhìn mặt Shidou.

368
00:18:48,112-->00:18:51,113
Tôi cũng vậy.

368
00:18:52,112-->00:18:54,113
Không có gì đâu!

368
00:18:55,112-->00:19:00,113
Tớ cũng đã nghĩ đến chuyện 
tặng Shidou một món quà...

368
00:19:01,112-->00:19:06,113
Tặng một món quà bằng tiền
do chính mình kiếm được...

368
00:19:08,112-->00:19:11,113
Nhưng thứ duy nhất chúng ta có 
trong tay là bộ đồng phục hầu gái.

368
00:19:12,112-->00:19:14,113
Ồ! Phải rồi!

368
00:19:15,112-->00:19:17,113
Tớ có ý này!

368
00:19:17,512-->00:19:20,113
Gì vậy? Ý tưởng gì vậy?

368
00:19:21,112-->00:19:26,113
Chúng ta sẽ mặc đồ hầu gái
và phục vụ Shidou.

368
00:19:27,112-->00:19:29,113
Tớ chắc Shidou sẽ rất vui đấy.

368
00:19:30,112-->00:19:31,113
Thật không?

368
00:19:31,312-->00:19:35,513
Có thật Shidou sẽ vui nếu chúng ta 
mặc bộ đồng phục này không?

368
00:19:35,812-->00:19:37,513
Ừ. Chắc chắn là vậy!

368
00:19:38,112-->00:19:40,113
Nếu vậy thì tớ sẽ mặc nó!

368
00:19:41,112-->00:19:42,513
Kotori, cô cũng sẽ mặc với bọn tôi nhé!

368
00:19:43,112-->00:19:44,113
Tôi cũng phải mặc sao?

368
00:19:44,512-->00:19:47,513
Tại sao không chứ? Nếu chúng ta 
cùng làm thì càng vui chứ sao!

368
00:19:48,112-->00:19:50,113
Có vẻ tôi không thể thay thế được, phải không?

368
00:19:50,512-->00:19:53,513
Được rồi. Tôi sẽ mặc.

368
00:19:53,912-->00:19:55,513
Cảm ơn Kotori-chan!

368
00:19:56,112-->00:19:59,513
Nếu Shidou không đi quá giới hạn.

368
00:20:00,112-->00:20:02,113
Em sẽ có thể làm mọi thứ em muốn.

368
00:20:03,512-->00:20:05,113
Nhưng cũng cần phải điều độ chứ, phải không?

368
00:20:05,513-->00:20:11,113
Rinne, tớ muốn nấu món gì đó cho Shidou.
Cậu có thể chỉ tớ làm món Omurice không?

368
00:20:12,113-->00:20:15,113
Được chứ. Tớ sẽ chỉ đến khi cậu biết làm.

368
00:20:16,113-->00:20:17,113
Vậy cùng làm nào.

368
00:20:18,112-->00:20:20,113
Trên đường về nhà, chúng ta 
sẽ mua nguyên liệu nhé?

368
00:20:20,512-->00:20:22,113
Ừ! Quyết định vậy đi!

368
00:20:23,112-->00:20:25,113
Chúng ta phải chuẩn bị 
một kế hoạch chi tiết.

368
00:20:25,512-->00:20:29,113
Vì tớ đã bỏ thời gian để làm bộ đồ
hầu gái, tớ sẽ nghĩ kế hoạch.

368
00:20:29,512-->00:20:31,113
Chúng ta phải làm gì?

368
00:20:32,512-->00:20:36,113
Có lẽ em sẽ làm đổ cà phê lên đầu 
anh ấy trong khi em làm dojikko.

368
00:20:37,112-->00:20:39,113
Bỏ những việc làm nguy hiểm đó đi.

368
00:20:40,113-->00:20:42,113
Không sao đâu. Dù sao đó là Shidou mà.

368
00:20:42,512-->00:20:45,113
Tôi đã chờ đợi lâu lắm rồi đấy.

368
00:20:45,512-->00:20:48,113
Vậy mình sẽ làm gì để 
giải trí với Shidou đây?

368
00:20:49,112-->00:20:51,113
Kotori-chan, có vẻ em đang tự
thưởng cho chính mình, phải không?

368
00:20:52,112-->00:20:54,113
Không phải vậy, nhưng mà...

368
00:20:54,512-->00:20:57,513
Chỉ là nếu em làm, em muốn 
làm cho ra trò thôi!


368
00:20:58,112-->00:21:01,513
Chị nghĩ chị có thể hiểu 
em cảm thấy thế nào.

368
00:21:02,113-->00:21:05,513
Mình thắc mắc không biết họ sẽ
làm gì với Shidou.

368
00:21:07,112-->00:21:10,113
Nhưng mà khi chúng ta xuất hiện 
trong bộ đồ hầu gái

368
00:21:10,512-->00:21:12,113
Shidou chắc chắn sẽ rất ngạc nhiên.

368
00:21:12,112-->00:21:13,513

