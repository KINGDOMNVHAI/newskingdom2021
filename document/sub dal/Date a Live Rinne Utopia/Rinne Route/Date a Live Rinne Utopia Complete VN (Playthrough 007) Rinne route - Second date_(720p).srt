﻿1
00:00:11,112-->00:00:53,513
Tôi vẫn còn thời gian. 
Tôi có nên hẹn hò tiếp không?

2
00:00:54,512-->00:00:57,513
Dạo này Rinne thường hay có biểu hiện lạ.

3
00:00:58,513-->00:01:01,113
Tôi cảm thấy rất lo lắng không biết
cô ấy có chuyện gì không.

4
00:01:02,112-->00:01:05,113
Dù sao thì mình cũng nên về nhà!

5
00:01:10,112-->00:01:14,513
Chuyện gì vậy? Tôi đã bấm chuông nhưng
tôi cảm thấy lo lắng, không muốn vào nhà.

6
00:01:15,512-->00:01:20,113
Tại sao tôi phải lo chứ? Tôi có nên đi thăm 
cô bạn hàng xóm của mình không?

7
00:01:21,112-->00:01:47,113
Aaa! Suy nghĩ nhiều làm gì! Nếu tôi lo lắng 
cho Rinne thì cứ đi thăm cô ấy đi!

8
00:01:48,112-->00:01:52,113
Nhưng tôi lại nghĩ sẽ tốt hơn khi đi thăm vào lúc khác.

9
00:01:53,112-->00:01:56,113
Đến nhà mà không báo trước... Hồi nhỏ thì
còn có thể... Bây giờ thì...

10
00:01:57,112-->00:02:00,113
Cậu đang làm gì vậy Shidou?

20
00:02:01,112-->00:02:05,113
Waa! Ri... Rinne? Cậu đứng đó từ lúc nào?

21
00:02:06,112-->00:02:10,113
Tớ vừa về thôi. Tớ vừa đi mua đồ ăn tối.

22
00:02:11,112-->00:02:13,113
Vậy... vậy à...

23
00:02:15,112-->00:02:17,513
Cậu đang cần gì à?

24
00:02:18,512-->00:02:22,113
Hở, không, không hẳn...
Tớ chỉ muốn nói chuyện với cậu...

25
00:02:23,112-->00:02:31,113
Hở? Tớ à? Nhưng mà... tớ bình thường mà.
Tớ có làm gì không đúng à?

26
00:02:32,112-->00:02:36,113
Tất nhiên là không có gì. Chẳng lẽ tớ 
phải có lý do mới được nói chuyện với cậu à?

26
00:02:37,112-->00:02:42,113
Cũng được. Vậy cậu muốn nói gì?

26
00:02:43,112-->00:02:48,113
Hở? À... Xin lỗi. Cũng chẳng có gì quan trọng đâu.

26
00:02:49,112-->00:02:54,113
Shidou... Cậu lúc nào cũng thế.

26
00:02:55,112-->00:02:59,113
À... ờ... xin lỗi... À, tớ nên về nhà.
Lúc khác gặp lại nhé.

26
00:03:00,112-->00:03:02,113
Aaa, tôi đang làm cái gì thế...

26
00:03:03,112-->00:03:07,513
Gặp lại sau nhé. Tớ sẽ sang chuẩn bị bữa tối.

26
00:03:08,512-->00:03:11,113
Ờ, tớ sẽ đợi cậu.

26
00:03:12,112-->00:03:15,113
Ừ, tạm biệt nhé.


