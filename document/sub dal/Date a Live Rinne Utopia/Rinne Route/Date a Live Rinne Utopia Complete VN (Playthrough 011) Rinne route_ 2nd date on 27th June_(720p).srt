﻿21
00:00:03,112-->00:00:05,113
Đi đến Tháp Tengu
1) Đồng ý
2) Không đồng ý

1
00:00:09,112-->00:00:13,113
Gần đây Rinne hành động kỳ lạ quá..
Có chuyện gì với cô ấy vậy? Tôi không thể
nghĩ không có chuyện gì được...

1
00:00:16,112-->00:00:20,513
A, sao tôi lại đến công viên chứ?
Vừa đi vừa nghĩ, đôi chân đưa tôi tới đây. 
Chẳng sao cả. Chỗ này rất tốt để ngồi suy nghĩ...

2
00:00:21,512-->00:00:24,113
Hở? Đó chẳng phải là... Rinne sao?

3
00:00:29,112-->00:00:32,113
Rinne, cậu đang làm gì thế?

4
00:00:33,112-->00:00:36,113
A! Shidou?

5
00:00:37,112-->00:00:40,113
Không phải cậu có việc quan trọng phải làm à?

6
00:00:41,112-->00:00:46,513
À, ừ... Tớ... tớ làm xong rồi.

7
00:00:47,512-->00:00:49,113
Vậy à...

8
00:00:50,112-->00:00:52,113
Sao cậu ngồi một mình ở đây vậy?

9
00:00:53,113-->00:01:02,113
Ừ, um... Tớ đang suy nghĩ một chút...
Còn cậu Shidou, tại sao cậu lại ở đây?

13
00:01:03,112-->00:01:07,113
Ừ, tớ cũng vậy. Tớ cũng đang suy nghĩ một chút.

14
00:01:08,112-->00:01:16,113
Cậu cũng đang suy nghĩ à? 
Cậu đang nghĩ gì vậy?

15
00:01:17,112-->00:01:19,113
Hở, à, đó là... Khoan đã, sao tớ phải nói chứ?

16
00:01:20,113-->00:01:23,113
Khuôn mặt của Rinne khiến tôi 
muốn buột miệng nói ra.

17
00:01:24,113-->00:01:29,113
Sao vậy? Ngay cả bạn thuở nhỏ của cậu
mà cậu cũng không nói được sao?

18
00:01:30,113-->00:01:35,113
Kh... không phải thế. Đó là...
tớ đang nghĩ không biết tối nay 
cậu làm món gì. Chỉ có vậy thôi.

19
00:01:36,112-->00:01:43,113
Vậy à. Shidou, cậu là một người tham ăn à?
Cậu thực sự nghĩ về chuyện đó à?

20
00:01:44,112-->00:01:46,113
Chẳng lẽ tớ không được nghĩ về đồ ăn à?

21
00:01:47,112-->00:01:57,113
Không phải vậy. Tớ thấy cậu nghĩ giống 
Tohka-chan. Tớ tự hỏi phải chăng cậu và cô ấy
sống cùng nhau nên bắt đầu giống nhau rồi?

22
00:01:52,112-->00:02:01,113
Hở? À... Có vẻ đúng vậy thật.

23
00:02:02,112-->00:02:05,513
Tớ cũng thích Shidou mà.

24
00:02:06,512-->00:02:09,113
Cậu vừa nói gì?

24
00:02:10,112-->00:02:14,113
Kh... không có gì đâu! Không có gì đâu!

24
00:02:15,112-->00:02:18,113
Ờ... ừ, vậy bữa tối nay thế nào?

24
00:02:19,112-->00:02:31,113
À, vâng... Tớ đang suy nghĩ về bữa tối nay
nên làm gì cho mọi người.


24
00:02:32,112-->00:02:35,113
Haha... vậy là cậu cũng chẳng khác gì tớ!

24
00:02:36,112-->00:02:47,113
Không được sao? Tớ luôn nghĩ những điều như thế,
Dạng như "Nấu ăn sau khi kết hôn" hoặc
"Cân bằng dinh dưỡng" vậy.

24
00:02:48,112-->00:02:50,513
Thật lòng cảm ơn cậu. Và tớ xin lỗi vì đã
để cậu ở lại một mình.

26
00:02:51,513-->00:02:56,113
Được rồi, được rồi. 
Tớ làm thế bởi vì tôi thích thế thôi.

26
00:02:57,112-->00:03:01,113
Hiểu rồi... Vậy cậu có muốn
tớ nên mua gì cho bữa tối không?

26
00:03:02,112-->00:03:10,113
Đừng lo, tớ có thể đi một mình.
Tốt hơn là cậu nên kiếm ai đó vui chơi đi.

26
00:03:11,112-->00:03:13,113
Hở, không, nhưng mà...

26
00:03:14,112-->00:03:20,113
Được rồi, được rồi. Tớ sẽ đi mua đồ, được chứ?

26
00:03:21,112-->00:03:23,513
A, này... Rinne!

26
00:03:24,112-->00:03:29,113
Hẹn gặp lại vào bữa tối!

26
00:03:30,112-->00:03:33,113
Tôi không có tâm trạng để mời ai đó đi chơi cả...
Sau khi nghỉ ngơi ở đây, tôi sẽ về nhà...

26
00:03:40,112-->00:03:42,113
Tôi thắc mắc không biết bữa tối đã xong chưa?

26
00:03:47,112-->00:03:50,113
Rinne? Có chuyện gì mà cô ấy thở dài như vậy?
Có vẻ hơi khó để nói chuyện với cô ấy bây giờ...

26
00:03:51,112-->00:03:56,113
Tại sao...? Tại sao không được...?

26
00:03:57,112-->00:04:03,113
Nếu cứ tiếp tục như vậy,
tất cả sẽ là vô ích...

38
00:04:04,112-->00:04:10,113
Mình sẽ không bỏ cuộc. Không, mình không thể!

41
00:04:11,113-->00:04:14,113
Rinne?

42
00:04:15,113-->00:04:22,113
Hả? Shi... Shidou? Cậu... vừa nghe thấy gì?

43
00:04:23,112-->00:04:26,113
Ừ, xin lỗi... Tớ không có ý nghe trộm đâu...

43
00:04:27,112-->00:04:30,513
À... Phải rồi...

43
00:04:31,512-->00:04:35,113
Cậu không cần phải buồn 
vì những chuyện như vậy!

43
00:04:39,512-->00:04:44,113
Tớ không biết cậu đã bỏ qua những gì, 
nhưng thất bại là chuyện bình thường! 
Tớ cũng làm và thất bại mà phải không?

43
00:04:45,112-->00:04:48,113
Cậu đang nói về cái gì vậy?

43
00:04:49,112-->00:04:52,513
Hở? Ờ... hình như cậu...
đã không thành công việc gì đó phải không?

43
00:04:53,512-->00:05:00,113
À... phải rồi. Ừ, đúng vậy.

45
00:05:04,112-->00:05:11,113
Quan trọng hơn, bữa ăn tối đã sẵn sàng.
Cậu gọi mọi người đến ăn được rồi.

46
00:05:12,112-->00:05:14,113
À, được rồi...

47
00:05:19,112-->00:05:24,113
Phù... Đồ ăn Rinne nấu luôn ngon.
Mặc dù cô ấy nấu hơi nhiều quá.

49
00:05:24,512-->00:05:27,113
Em vào nhé.

50
00:05:31,112-->00:05:33,113
Gì vậy Kotori?

51
00:05:34,112-->00:05:43,113
Có sóng Tinh Linh xuất hiện trong thành phố.
Và em có nói với anh là anh có một 
sức mạnh Tinh Linh rất lớn trong người phải không?

52
00:05:44,112-->00:05:48,113
Phải. Đó trông giống như là kết giới 
mạnh mẽ của Tinh Linh nào đó. Rồi sao nữa?

53
00:05:49,112-->00:05:58,113
Phải. Và dường như ai đó là người gây ra.
Anh không thể kiểm soát sức mạnh to lớn này.

54
00:05:59,112-->00:06:01,113
Ý em là sao?

55
00:06:02,112-->00:06:14,113
Reine đã phân tích các sóng Tinh Linh.
Nhưng sự biến động của sóng năng lượng quá lớn 
và rất không ổn định. 

55
00:06:14,512-->00:06:17,113
Hơn nữa, những biến động
trở nên lớn hơn dần lên mỗi ngày.

42
00:06:18,112-->00:06:21,113
Vậy tức là nó có thể mạnh yếu thất thường? 
Trong trường hợp đó, chúng ta có thể 
yên tâm một chút...

43
00:06:22,112-->00:06:26,113
Anh thực sự là một thằng ngốc. 
Nó hoàn toàn ngược lại.

44
00:06:26,512-->00:06:28,113
Hở?

45
00:06:29,812-->00:06:36,113
Chúng ta đang nói đến việc nhiều khả năng
chúng tôi phải đối mặt với một Tinh Linh. 
Do đó bất ổn này cực kỳ nguy hiểm.

46
00:06:37,112-->00:06:43,113
Nếu sức mạnh này quá lớn,
anh sẽ đi ra khỏi tầm kiểm soát...

47
00:06:44,113-->00:06:46,513
Vậy điều gì sẽ xảy ra?

48
00:06:47,112-->00:06:54,113
Thành phố này có thể sẽ biến mất 
chỉ trong khoảnh khắc.

49
00:06:55,112-->00:06:57,513
Điều đó không thể được!

50
00:06:58,113-->00:07:07,513
Ngay cả khi nó không mạnh như 
Không gian chấn 30 năm trước nhưng nó
đã xảy ra ở Nhật Bản.

50
00:07:08,113-->00:07:13,113
Trước khi điều đó xảy ra, chúng ta 
không có sự lựa chọn nhưng vẫn 
tìm ra giải pháp, phải không?

51
00:07:14,112-->00:07:27,113
Điều lạ lùng là các Tinh Linh đang ở trong
kết giới này cũng trở nên không ổn định.

60
00:07:31,112-->00:07:43,113
Một sức mạnh Tinh Linh rất lớn bao phủ Tengu. 
Anh có thể hiểu thành phố giờ đã 
trở thành một lỗ đen Không gian chấn.

61
00:07:44,112-->00:07:46,313
Một lỗ đen sao?

62
00:07:47,112-->00:08:01,113
Anh không biết sap? Một Không gian chấn 
được hình thành khi một khu vực bị biến động
bởi trọng lực và sức mạnh Tinh Linh.
Và kết quả là sự sụp đổ trên chính vùng đó.

81
00:08:02,112-->00:08:04,113
Hả?

82
00:08:05,112-->00:08:18,113
Điều này tương tự như vậy. Nếu một Tinh Linh 
sở hữu một sức mạnh rất lớn mà không kiểm soát
được thì có thể tạo ra biến động.

83
00:08:19,112-->00:08:24,113
Hả... Anh không hiểu lắm nhưng mà...
Tóm lại có nghĩa là nếu Tohka và những
cô gái khác tham gia vào sự "bóp méo" này 
có thể dễ gây mất kiểm soát hơn phải không?

84
00:08:25,112-->00:08:39,113
Có thể. Nhưng đó chỉ là một phỏng đoán.
Ngoài ra chúng ta cũng không biết lý do
ai đó xây dựng kết giới này. Chúng ta 
không thể kết luận gì cả.

85
00:08:40,112-->00:08:43,113
Đó có nghĩa là... một vấn đề rất phức tạp, phải không?

86
00:08:43,512-->00:08:58,113
Phải. Nhưng em đã nói những gì anh phải làm
rồi phải không? Ratatoskr tiếp tục nghiên cứu. 
Anh phải dành thời gian hẹn hò với các
Tinh Linh để ổn định mọi chuyện.

92
00:08:59,112-->00:09:01,113
Em nói đúng. Việc đó quan trọng hơn.

99
00:09:02,112-->00:09:12,113
Miễn là anh hiểu là tốt rồi. 
Chỉ cần chúng ta giúp Tohka và các Tinh Linh
ổn định và tiếp tục tìm hiểu thì sẽ ổn thôi.

100
00:09:13,112-->00:09:15,113
Ừ... Anh hiểu rồi

101
00:09:16,112-->00:09:19,113
Vậy nhé. Chúc anh ngủ ngon.

102
00:09:20,112-->00:09:22,113
Ờ... ờ...

103
00:09:25,112-->00:09:27,113
Chuyện này đã trở thành 
một vấn đề khó giải quyết...

104
00:09:28,112-->00:09:32,113
Nhưng giờ tôi không muốn suy nghĩ nữa. 
Mai tôi cũng phải dậy sớm đấy!

105
00:09:34,112-->00:09:36,113
Đi ngủ thôi...

106
00:09:43,112-->00:09:46,113
Chuyện gì trong đêm tối này vậy?
Tiếng gì vậy?

107
00:09:46,512-->00:09:49,113
Tôi không nghĩ nữa. Tôi đi ngủ đây.

108
00:09:58,112-->00:10:07,113
Thật bất tiện phải không? Thực tế là 
bạn chỉ có thể biết những gì
mắt thấy tai nghe thôi...

114
00:10:08,112-->00:10:15,113
Thiên đường Eden này không ổn định.

1
00:10:16,112-->00:10:20,113
Nghĩa là mọi thứ là vô ích sao?

1
00:10:21,112-->00:10:27,113
Không, không thể được!
Tôi không chấp nhận một cái gì đó như thế. 
Thế giới của tôi là hoàn hảo!

1
00:10:28,112-->00:10:35,113
Nếu nguồn năng lượng không ổn định, 
tôi phải làm gì?

1
00:10:36,112-->00:10:47,113
Tôi đã biết câu trả lời cho câu hỏi đó.
Phải. Tôi cần các Tinh Linh...

1
00:10:48,112-->00:10:56,113
Đúng vậy... Tôi không biết làm thế nào 
bạn thất bại trong việc nhận ra 
một cái gì đó đơn giản như vậy...

1
00:10:56,512-->00:11:01,113
Dường như sự thiếu kiên nhẫn của tôi 
cũng ảnh hưởng đến suy nghĩ của tôi...

368
00:11:02,112-->00:11:05,113
Tôi chắc chắn về điều này...

68
00:11:12,112-->00:11:16,113
Ngày 28 tháng 6

368
00:11:13,112-->00:11:14,113


368
00:11:14,512-->00:11:19,513


368
00:11:20,112-->00:11:29,113




368
00:11:48,112-->00:11:50,513






