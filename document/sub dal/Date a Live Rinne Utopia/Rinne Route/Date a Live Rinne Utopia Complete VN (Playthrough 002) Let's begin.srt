﻿1
00:00:32,112-->00:00:35,113
Cậu tìm thấy một Tinh Linh, dù có một sức mạnh 
tiềm tàng, nhưng lại trông rất tuyệt vọng.

1
00:00:44,512-->00:00:47,113
Shidou đồng ý cộng tác với Ratatoskr 
để phong ấn Tinh Linh, nhưng ...

1
00:00:55,112-->00:00:58,113
Shidou không có kinh nghiệm về tình yêu,
Cậu đã nhận được sự giúp đỡ từ những người khác 
và bắt đầu hẹn hò với các Tinh Linh.

1
00:01:11,112-->00:01:14,113
Bạn cùng lớp của cậu, Tobiichi Origami, 
là thành viên của AST, một tổ chức
có nhiệm vụ tiêu diệt Tinh Linh.

1
00:01:15,112-->00:01:17,113
Không biết khi nào sức mạnh 
Tinh Linh ra khỏi tầm kiểm soát.

1
00:01:18,512-->00:01:22,113
Nhưng ngày qua ngày, dù sự lo lắng chưa kết thúc,
Shidou và cô gái vẫn tiếp tục sống trong hòa bình...

1
00:01:37,112-->00:01:44,313
Lúc này tôi đang mở cửa lên sân thượng,
Tôi nhận ra điều này đã bắt đầu.
Tôi run lên do bầu không khí căng thẳng của nơi này.

2
00:01:53,112-->00:01:56,313
Shi...Shidou...

3
00:02:05,112-->00:02:06,113
To...Tohka!

4
00:02:15,112-->00:02:20,313
Tohka đang mặc bộ đồ Tinh Linh sáng rực rỡ. 
Ánh sáng màu tím phát ra từ bộ Thiên Phục của cô.

5
00:02:21,512-->00:02:26,113
Shi...Shidou...chạy...chạy đi!

6
00:02:28,112-->00:02:31,313
Anh không thể bỏ em lại đây được!

7
00:02:32,112-->00:02:35,313
nếu tôi có thể làm một cái gì đó. Nếu tôi...

8
00:02:38,112-->00:02:41,313
Tôi bước lên phía trước, cố gắng chạy đến cô ấy,
nhưng cơ thể tôi đã không di chuyển như tôi muốn.

9
00:02:44,112-->00:02:47,313
Trước dòng chảy dữ dội của ánh sáng liên tục 
tỏa ra từ cơ thể Tohka, tôi vẫn tiếp tục tiến tới.

10
00:02:55,112-->00:03:02,113
Nhưng ngay lập tức, trước áp lực đáng kinh ngạc,
nếu như tiếp tục tiến sâu vào, cảm giác 
như sức mạnh đó đang tấn công cơ thể tôi.

11
00:03:03,112-->00:03:06,113
Nhưng tôi không thể đứng nhìn cô ấy như vậy.

12
00:03:09,112-->00:03:15,113
Trong áp lực đó, từng bước chân vững chắc  
một lúc sau đã giúp tôi lại gần hơn với Tohka.

13
00:03:20,112-->00:03:24,113
Tôi nhận ra rằng một sức mạnh chảy ra khỏi tôi

13
00:03:25,112-->00:03:26,513
Sức mạnh đó vẫn không kết thúc...

14
00:03:27,112-->00:03:33,313
Đừng đến gần em... Nếu anh đến gần em, anh sẽ...

15
00:03:36,112-->00:03:42,113
Tohka đứng bất động, tay cô nắm chặt một 
thanh kiếm lớn, Sandalphon, để cố gắng
giữ cơ thể của mình đang rung lên.

16
00:03:43,112-->00:03:48,113
Mái tóc của cô như màn đêm rất hợp với 
khuôn mặt xinh đẹp đó. Cô như một con búp bê 
mang biểu hiện của nỗi đau đớn và tuyệt vọng.

17
00:03:49,112-->00:03:53,113
Dù em không muốn, anh vẫn...

18
00:03:54,113-->00:03:57,113
Tôi đã đi đến gần Tohka hơn, 
như thể tôi bị kéo trên mặt đất.

19
00:03:58,112-->00:04:00,313
Anh không bao giờ ... 
không bao giờ để em một mình!

20
00:04:01,112-->00:04:11,113
Shidou...! Em... không muốn... giết anh!

21
00:04:15,112-->00:04:23,513
Khuôn mặt Tohka đau đớn thốt lên bằng 
một giọng nói khó có thể diễn tả.
Hào quang phát ra từ cô ấy đã dữ dội hơn trước.

22
00:04:26,112-->00:04:28,313
To... Tohka!


23
00:04:32,112-->00:04:38,313
Tôi phải đối mặt với dòng chảy ánh sáng 
ngày càng mạnh, có thể cuốn bay tôi. 
Tôi vẫn đến và mở rộng bàn tay tôi với cô ấy.

24
00:04:41,112-->00:04:44,113
Shi... dou!

25
00:04:46,112-->00:04:50,113
Tohka với khuôn mặt tuyệt vọng 
gọi tên tôi một cách yếu ớt.

26
00:04:52,112-->00:04:56,113
Trong một khoảnh khắc, một sức mạnh 
lạ chảy ra khỏi tôi

27
00:05:02,112-->00:05:09,113
Và như để trả lời cho chuyện này, đột nhiên 
Sandalphon mà Tohka vẫn đang nắm chặt, bắt đầu 
tỏa sáng chói mắt và phát ra nhiều năng lượng hơn.

28
00:05:11,112-->00:05:15,313
Nếu chuyện này tiếp tục, Tohka sẽ...

29
00:05:18,112-->00:05:21,313
Tohkaaaa!!!!!

30
00:05:22,512-->00:05:24,313
Shidoooouuuu!!!!!

31
00:05:33,112-->00:05:40,313
Sau tiếng gọi của Tohka, mọi thứ bị bao phủ 
một màu trắng xóa và ý thức tôi trống rỗng.

32
00:05:49,112-->00:05:55,313
Tôi luôn luôn... theo dõi cậu...

33
00:05:59,112-->00:06:01,313
Hở?

34
00:06:05,112-->00:06:08,313
Giọng nói đó...?

35
00:06:09,113-->00:06:10,113
Một giọng nói quen thuộc mà tôi không nhớ...

36
00:06:12,113-->00:06:14,113
Nhưng có một điều mà tôi biết rõ...

37
00:06:15,113-->00:06:18,113
Đó là một cảm giác rất ấm áp... 
thể hiện lòng tốt của người đó...

38
00:06:26,113-->00:06:29,113
Đôi mắt từ từ mở ra, tôi trông thấy 
một gương mặt quen thuộc...

39
00:06:30,112-->00:06:32,313
Shidou...

40
00:06:34,112-->00:06:36,313
Tohka...

41
00:06:39,112-->00:06:43,113
Shidou! Shidou! Shidou...!

42
00:06:45,112-->00:06:46,113
...Uida! 

43
00:06:47,112-->00:06:50,113
To...Tohka! ... đau...anh đau quá!

44
00:06:52,112-->00:06:55,113
Em...em xin lỗi, Shido! 

45
00:06:56,512-->00:07:00,113
...Không sao!...aida ...aida 

46
00:07:01,112-->00:07:03,113
Nhưng chuyện gì đã xảy ra vậy?

47
00:07:04,112-->00:07:07,113
Tôi nhìn xung quanh tôi.
Có vẻ như tôi đang ở phòng mình.

48
00:07:08,112-->00:07:13,113
Chuyện gì vậy Shidou? Chắc hẳn là... 
anh cảm thấy không khỏe phải không?

49
00:07:15,512-->00:07:21,113
Cô gái đang quan tâm đến tôi tên là Yatogami Tohka. 
Mặc dù trông yếu đuối nhưng cô đang sở hữu
một sức mạnh rất lớn. Cô là một Tinh Linh.

50
00:07:23,112-->00:07:27,113
Khoảng thời gian tôi sống với Tohka 
và nhìn thấy nụ cười rạng rỡ của cô,
Tôi đã quyết định phải giúp đỡ cô ấy.

51
00:07:31,112-->00:07:37,113
Bạn có thể để nỗi buồn và tinh thần bị tổn thương.
Nhưng cần phải có phương pháp để khắc phục.

52
00:07:38,112-->00:07:46,113
Và sau đó tôi đã phong ấn sức mạnh của Tohka. 
Bây giờ khi tôi nghĩ về nó, tôi nhận ra 
Tohka là sự khởi đầu của những ngày hẹn hò của tôi.

53
00:07:48,112-->00:07:51,113
Không, không sao đâu. 
Vậy mọi chuyện là...

54
00:07:59,112-->00:08:04,113
Tôi định hỏi về những gì đã xảy ra, nhưng
trước khi tôi kịp hỏi thì Tohka đột nhiên 
chạy ra khỏi phòng.

55
00:08:07,112-->00:08:11,113
...Vậy, bây giờ là... Hmm, tôi hiểu rồi.

56
00:08:14,112-->00:08:17,113
To...Tohka?

1
00:08:20,112-->00:08:25,113
Được rồi! Shidou! Cứ ở yên đó nhé!

1
00:08:26,112-->00:08:30,113
G...gì cơ? Ý em là sao, Tohka?

1
00:08:31,112-->00:08:39,113
Nằm yên và không được di chuyển! 
Để anh khỏe lại anh cần phải... 
ngủ với người khác phải không?

1
00:08:41,112-->00:08:43,113
Đợi... đợi chút đã!

1
00:08:46,112-->00:08:48,113
Hm, gì vậy？

1
00:08:51,112-->00:08:53,113
"Ngủ với người khác" là sao? 
Em định làm gì vậy, Tohka?

1
00:08:57,112-->00:09:01,113
Hở? Shidou... Không phải anh cảm thấy không khỏe sao?

1
00:09:03,112-->00:09:06,113
Ừ, anh thật sự cảm thấy không khỏe,
toàn thân đau nhức. Nhưng...

1
00:09:08,112-->00:09:13,113
Vậy thì anh cần phải được giữ ấm cơ thể!
Giữ ấm rất quan trọng! Rất quan trọng!

1
00:09:14,112-->00:09:16,113
Hả?

1
00:09:18,112-->00:09:23,113
Để sưởi ấm, cách tốt nhất là ôm nhau! 
Ôm nhau khi khỏa... thân...

1
00:09:25,112-->00:09:29,113
Cái gì? Tohka? Dừng lại, dừng lại!
Anh bảo dừng lại! Bình tĩnh nào!

1
00:09:31,112-->00:09:40,113
Tại sao anh từ chối em, Shidou ...?
Hay anh không thích em sưởi ấm cho anh?

1
00:09:42,112-->00:09:44,113
Vấn đề không phải vậy...

1
00:09:50,112-->00:09:52,113
Shidou, anh tỉnh dậy rồi sao?

1
00:09:55,112-->00:09:57,113
Ch... chào Kotori!

1
00:10:06,412-->00:10:11,113
Thật là, anh làm em lo lắm đấy...

1
00:10:13,112-->00:10:15,113
Ko...tori?

1
00:10:18,112-->00:10:21,113
Chuyện đó nghĩa là sao?

1
00:10:23,112-->00:10:29,113
Không phải lỗi của anh đâu... 
Đó là lỗi của em. Xin lỗi ...

1
00:10:30,112-->00:10:33,113
Nghĩa là sao?

1
00:10:35,112-->00:10:43,113
Không lẽ Shidou... anh không nhớ gì cả sao?

1
00:10:45,112-->00:10:49,113
Không, anh không nhớ bất cứ điều gì cả.

1
00:10:51,112-->00:10:55,113
Kotori cũng xem xét tình hình.
Tohka sau đó quay sang tôi.

1
00:10:58,112-->00:11:10,113
May quá. Có vẻ anh vẫn bình thường.
Trí nhớ tệ hại của anh là ví dụ điển hình.

1
00:11:13,112-->00:11:15,113
Mm...! Anh xin lỗi về điều đó!

1
00:11:17,112-->00:11:25,113
Em tự hỏi liệu em có nên đưa anh đi bệnh viện không? 
Nhưng vì đó là do bẩm sinh nên chắc là đã muộn rồi.

1
00:11:27,112-->00:11:29,113
Không cần đâu!

1
00:11:32,112-->00:11:38,113
Mà thôi, Quan trọng nhất là có vẻ như anh đang  
khỏe hơn em mong đợi. Giống như hiện giờ...

1
00:11:40,112-->00:11:41,113
Hở?

1
00:11:42,112-->00:11:47,113
Mặc dù anh đã hồi phục, nhưng anh đang muốn
làm gì trên giường với Tohka vậy?

1
00:11:49,112-->00:11:53,113
Không phải vậy! Tohka đang lo lắng cho anh!

1
00:11:55,112-->00:12:02,113
Thôi đi. Anh đang cố gắng đổ lỗi cho người khác 
chỉ vì anh không thể kiểm chế ham muốn của mình à?

1
00:12:03,512-->00:12:06,113
Em có nghe anh nói không đấy?

1
00:12:08,112-->00:12:14,113
Đúng là ngốc... Tohka, cô không cần phải 
lo lắng thêm cho anh hai ngốc nghếch của tôi nữa đâu.

1
00:12:16,112-->00:12:23,113
Không cần sao?
Nhưng Kotori, cô vừa nói nếu tôi ngủ với anh ấy 
thì anh ấy sẽ hồi phục nhanh hơn phải không?

1
00:12:25,112-->00:12:30,113
Đó là sự thật... Nếu cô muốn làm
thì cũng không có vấn đề gì đâu.

1
00:12:31,112-->00:12:34,113
N... này, Kotori!

1
00:12:37,112-->00:12:42,113
Nếu anh muốn được ôm nhiều hơn, em có thể...

1
00:12:44,112-->00:12:50,113
Không, Tohka! Anh khỏe rồi! Chỉ cần anh quan tâm 
đến anh là anh thấy khỏe rồi! Cảm ơn em!

1
00:12:52,112-->00:12:57,113
Shidou... Thật vậy sao?

1
00:12:58,112-->00:13:02,113
Ừ, thật vậy đấy!

1
00:13:08,112-->00:13:11,113
Hở? Có chuyện gì à?

1
00:13:14,112-->00:13:19,113
Không, không có gì đâu. Anh cảm thấy 
tốt hơn là em vui rồi!

1
00:13:25,112-->00:13:28,113
Vậy giờ... Anh muốn biết chuyện gì đã xảy ra vậy?

1
00:13:30,112-->00:13:33,113
Anh thật sự không nhớ 
bất cứ chuyện gì đã xảy ra à?

1
00:13:34,112-->00:13:36,113
Không, hoàn toàn không.

1
00:13:37,112-->00:13:42,113
Vậy em nghĩ tốt nhất là em nên kiểm tra anh.

1
00:13:46,112-->00:13:52,113
Con bé nói chuyện với tôi nãy giờ,
có ánh mắt sắc nét như kim đâm xuyên qua
người tôi là Itsuka Kotori. Đó là em gái tôi.

1
00:13:54,112-->00:13:59,113
Cô em gái-sama này có thể thay đổi nhân cách 
phụ thuộc vào màu của nơ cột tóc.

1
00:14:01,112-->00:14:09,113
Khi đeo băng trắng, con bé là một cô em gái 
rất ngây thơ và dễ thương nhưng...

1
00:14:11,112-->00:14:18,113
Khi đeo băng đen... nói cách khác, như lúc này, 
đó là lúc con bé biến thành một người 
với một cái lưỡi biết múa mà bạn không thể cãi lại.

1
00:14:21,112-->00:14:28,113
Tôi gọi đây là chế độ chỉ huy. Nhưng thực sự Kotori
là người chỉ huy đáng kính, người chịu trách nhiệm về
phi thuyền Fraxinus, một phần của tổ chức Ratatoskr,
có nhiệm vụ bảo vệ các Tinh Linh.

1
00:14:30,112-->00:14:36,113
Kotori là chỉ huy của Ratatoskr, tổ chức 
có nhiệm vụ bảo vệ các Tinh Linh. Nhưng 
trên thực tế cô có năng lực của một Tinh Linh.

1
00:14:38,112-->00:14:42,113
Đây là một điều được phát hiện gần đây, nhưng
chỉ được phát hiện khi con bé phát ra sức mạnh,
kết quả là con bé trở thành một Tinh Linh.

1
00:14:44,112-->00:14:51,113
Có lẽ vì lý do này mà Kotori, mặc dù vẫn còn là 
một học sinh trung học, nhưng con bé vẫn đảm bảo
trách nhiệm của một người chỉ huy.

1
00:14:53,112-->00:14:58,113
Bạn có thể rất vững vàng, nhưng... Khi một 
đứa em gái ngây thơ biến thành thế này, 
thật sự gây sốc đối với tôi.

1
00:15:00,112-->00:15:06,113
Nhưng ... khi tôi mất hy vọng 
và phương hướng, người đứng bên tôi như 
một người thân trong gia đình là Kotori.

1
00:15:08,112-->00:15:15,113
Vì vậy nếu Kotori thay đổi như thay đổi, tôi sẽ
làm bất cứ điều gì cho con bé. Chắc chắn là vậy.

1
00:15:17,112-->00:15:22,113
Xin chào, Shidou-kun! Có vẻ như anh khỏe rồi!

1
00:15:24,112-->00:15:26,113
Shi... Shidou-san...!

1
00:15:28,112-->00:15:30,513
Ờ... ờ! Em cũng đến à Yoshino.

1
00:15:32,112-->00:15:38,113
Ano... anh thực sự... không sao chứ?

1
00:15:39,112-->00:15:43,113
Ừ, dường như chẳng có gì phải lo lắng!
Trừ cái đầu của anh ấy!

1
00:15:44,112-->00:15:46,113
To... Tohka...

1
00:15:50,112-->00:15:52,113
Không có gì ... Được rồi.

1
00:15:54,112-->00:15:58,113
Em rất vui vì... anh không sao.

1
00:16:01,112-->00:16:05,113
Bỏ qua chuyện về đầu anh đi...

1
00:16:08,112-->00:16:13,113
Cô bé cầm trên tay một con thỏ trắng 
là Yoshino, Tinh Linh đã được tôi phong ấn.

1
00:16:14,112-->00:16:18,113
Đó là một cô bé rất ngoan ngoãn 
và hay sợ hãi trước những người lạ.

1
00:16:20,112-->00:16:25,113
Hiện giờ cô bé đã có thể nói chuyện với tôi và
các cô gái khác, nhưng để làm được điều đó 
cũng mất khá nhiều thời gian.

1
00:16:27,112-->00:16:32,113
Và kết quả là một cái gì đó dạng như thế này.

1
00:16:35,112-->00:16:38,113
Hừm...

1
00:16:41,112-->00:16:43,113
Xin lỗi, xin lỗi. Yoshinon cũng đến à.

1
00:16:45,112-->00:16:53,113
Mặc dù em là người đầu tiên chào anh,
nhưng anh đã làm ngơ em nãy giờ!
Chứng tỏ anh không quan tâm đến em phải không?

1
00:16:56,112-->00:17:01,113
Xin lỗi, anh mải nói chuyện 
với Yoshino nên anh quên mất.

1
00:17:04,112-->00:17:08,113
Đúng vậy, khó ai có thể quên mất Yoshinon.

1
00:17:10,112-->00:17:16,113
Yoshinon là một nhân cách khác của Yoshino,
được thể hiện qua con rối thỏ trắng này. 
Cậu ấy là bạn thân của Yoshino.

1
00:17:18,112-->00:17:23,113
Yoshino là một Tinh Linh không bao giờ làm tổn thương 
người khác và là một cô bé rất tốt bụng... 
Không, nói đúng hơn là cô bé rất dịu dàng.

1
00:17:25,112-->00:17:32,113
Tuy nhiên, do sợ hãi những người không quen biết, 
khi hoảng loạn, cô bé có thể đóng băng mọi thứ.
Cô bé vẫn là một Tinh Linh có sức mạnh rất lớn.

1
00:17:34,112-->00:17:40,113
Yoshinon, ngước lại rất thích nói chuyện, dường như 
đó là tính cách được tạo ra để giúp Yoshino 
không làm tổn thương người khác một cách vô ý thức.

1
00:17:42,112-->00:17:45,113
Và kết quả là thành ra thế này đây. 

1
00:17:48,112-->00:17:50,113
Cậu cảm thấy thế nào, Shin?

1
00:17:54,112-->00:17:58,113
Re... Reine-san? Sao... cô... lại gần vậy?

1
00:18:04,112-->00:18:10,113
Người phụ nữ vừa kiểm tra tôi
là Murasame Reine-san.

1
00:18:11,112-->00:18:17,113
Đó là một nhà phân tích xuất sắc của Ratatoskr,
cánh tay phải của Kotori, chịu trách nhiệm 
cho việc phân tích Tinh Linh và thực hiện giám sát họ.

1
00:18:19,112-->00:18:24,113
Cô ấy rất giỏi... nhưng có vấn đề là cô ấy
vẫn gọi sai tên tôi bao lâu nay.

1
00:18:25,112-->00:18:37,113
Đột nhiên, cô ấy chuyển đến trường tôi, 
trung học Raizen, với vị trí giáo sư vật lý.
Cô còn là trợ giảng cho giáo viên lớp tôi. 
Vì vậy chúng tôi gặp nhau nhiều lần.

1
00:18:38,112-->00:18:42,113
Nếu tôi nói rằng tôi không quan tâm đến 
con gấu bông luôn trong túi ngực của cô ấy
thì đó là lời nói dối nghiêm trọng... 

1
00:18:42,512-->00:18:46,113
Nhưng mà, mặc dù cô ấy khá kỳ lạ, 
tôi nghĩ đó là một người tốt và có thể tin tưởng.

1
00:18:47,112-->00:18:53,113
Shin, cậu đang dần hồi phục, nhưng cậu vẫn chưa...

1
00:18:56,112-->00:18:59,113
Waa! Re... Reine-san!

1
00:19:01,112-->00:19:07,113
Aaa, Xin lỗi. Gần đây tôi có hơi thiếu ngủ.

1
00:19:10,112-->00:19:13,113
Em biết! Reine-san, cô cần phải nghỉ ngơi...

1
00:19:14,112-->00:19:21,113
Không cần thiết đâu. Quan trọng hơn, cậu cần
phải đến phòng y tế của Fraxinus. Tôi sẽ
chăm sóc tình trạng thể chất đang xấu đi của cậu.

1
00:19:23,112-->00:19:28,113
Hở? À, vâng. Cảm ơn cô rất nhiều... 
Đợi một chút! Reine-san!

1
00:19:30,112-->00:19:34,113
Gì vậy? Tự nhiên cậu hoảng hốt vậy ...

1
00:19:36,112-->00:19:41,113
Tình trạng thể chất sao cơ?
Rốt cuộc hiện giờ mọi chuyện là sao?
Làm ơn giải thích cho em đi!

1
00:19:42,113-->00:19:52,113
Đột nhiên Tohka ôm em khi em vừa tỉnh dậy! 
Kotori và Yoshino vào phòng em! Giờ thì 
bảo em vào phòng y tế! Hay là em đang mắc một
căn bệnh nghiêm trọng hoặc...

1
00:19:59,113-->00:20:02,113
Giờ thì anh bình tĩnh lại chưa?

1
00:20:03,113-->00:20:07,113
Cái gì...! Này...!

1
00:20:09,113-->00:20:11,113
Hà... hà... hà...

1
00:20:13,113-->00:20:15,113
Cậu không sao chứ, Shin?

1
00:20:18,113-->00:20:24,113
Xin lỗi! Em hơi bất ngờ! Này Kotori! 
Đột nhiên em đẩy Reine-san đè ngực vào mặt anh...

1
00:20:26,113-->00:20:32,113
Em thấy anh hoảng loạn quá nên em cung cấp 
cho anh "phương thuốc đặc biệt".
Anh còn khiếu nại gì nữa?

1
00:20:33,113-->00:20:37,113
Vậy ngực của Reine-san là 
"phương thuốc đặc biệt" à?

1
00:20:39,113-->00:20:43,113
Nếu việc đó có thể giúp cậu thì tôi không phiền đâu.

1
00:20:51,113-->00:20:59,113
Anh nghe rồi đấy. Nhìn mặt anh trông có vẻ
anh muốn làm lại phải không Shidou?

1
00:21:01,113-->00:21:07,113
Aaaa, thôi anh ổn. Không cần đâu. Cảm ơn.

1
00:21:10,113-->00:21:15,113
Mà thôi! Giờ không phải lúc nói chuyện đó!
Giờ anh muốn biết chuyện gì đang xảy ra!

1
00:21:17,113-->00:21:22,113
Giải thích? Shidou, anh thực sự 
không nhớ bất cứ điều gì sao?

1
00:21:24,113-->00:21:28,113
Hở? Không hề...

1
00:21:31,113-->00:21:36,113
Tôi đột nhiên cảm thấy không thoải mái.
Trước tình hình này, chắc chắn là cái gì đó đã xảy ra.

1
00:21:38,113-->00:21:44,113
Nhưng dù cố gắng nhớ, tôi không thể nhớ 
bất cứ điều gì. Hơn nữa, tôi cũng không nhớ 
làm sao tôi lại ở trong tình huống này.

1
00:21:45,113-->00:21:50,113
Nhưng nếu tôi phải nói những gì tôi nhớ...
Tôi chỉ nhớ mình đã có một giấy mơ kỳ lạ, mặc dù...

1
00:21:51,113-->00:21:56,113
... điều đó có lẽ không có gì đáng nói.

1
00:21:57,113-->00:22:02,113
Mà sau khi trải qua 3 ngày trên giường, 
có lẽ anh đã trở nên già hơn rồi.

1
00:22:04,113-->00:22:10,113
Cái... gì? 3 ngày? Anh ngủ suốt 3 ngày rồi sao?

1
00:22:11,113-->00:22:18,113
Phải. Reine, giải thích cho ông anh ngốc này đi.

1
00:22:21,113-->00:22:30,113
Shin, cậu đã ngủ trong phòng 
y tế Fraxinus 3 ngày qua.

1
00:22:32,113-->00:22:39,113
Sau khi tình trạng của anh đã ổn định, 
các bác sĩ đã cho phép về nhà vào sáng nay.

1
00:22:42,113-->00:22:48,113
Đó là một vấn đề thực sự đấy anh biết không?
Tohka nói đó là lỗi của mình và cô ấy khóc suốt.

1
00:22:48,213-->00:22:55,113
Yoshino cũng khóc theo và mọi thứ mất kiểm soát... 
Tất cả là lỗi của anh đấy, Shidou.

1
00:22:57,113-->00:23:05,113
Anh đã nói anh không biết gì cả. Tại sao
anh lại ngất? Anh không biết chuyện gì đã xảy ra.

1
00:23:06,113-->00:23:14,113
Shidou... anh đã cố gắng giúp em... 
Lúc đó em đã mất kiểm soát...

1
00:23:15,113-->00:23:20,113
Tohka... mất kiểm soát?
"Mất kiểm soát" nghĩa là sao...?

1
00:23:30,113-->00:23:33,113
Mình nhớ rồi.

1
00:23:36,113-->00:23:40,113
Đúng vậy, sau đó tôi...

1
00:23:42,113-->00:23:48,113
Điều đó xảy ra khi tôi đang nói chuyện 
với Tohka trong lớp học như mọi ngày.

1
00:23:49,113-->00:23:55,113
Chúng tôi đã tán gẫu về những thứ như gần đây 
tôi không được khỏe hoặc các cửa hàng chúng tôi 
thường đi có menu mới...

1
00:23:57,113-->00:24:00,113
Tại thời điểm đó ... Tohka đột nhiên 
tỏ ra khó chịu và chạy ra khỏi lớp học.

1
00:24:01,113-->00:24:09,113
Tôi nhanh chóng đuổi theo, và leo cầu thang 
dẫn lên sân thượng... Tôi tìm thấy Tohka...

1
00:24:13,113-->00:24:18,113
Shi...Shidou...chạy...chạy đi!

1
00:24:20,113-->00:24:23,113
Anh không thể bỏ em lại đây được!

1
00:24:27,113-->00:24:34,113
Đừng đến gần em... Nếu anh đến gần em, anh cũng sẽ...

1
00:24:36,113-->00:24:39,113
Dù em không muốn, anh vẫn...

1
00:24:43,113-->00:24:46,113
Anh không bao giờ ... 
không bao giờ để em một mình!

20
00:24:47,113-->00:24:55,113
Shidou...! Em... không muốn... giết anh!

20
00:24:56,113-->00:25:00,113
Tohkaaaa!!!!!

30
00:25:03,112-->00:25:06,113
Shidoooouuuu!!!!!

30
00:25:15,112-->00:25:18,113
Dường như anh đã nhớ ra rồi.

30
00:25:19,112-->00:25:21,113
Ờ... ờ...

30
00:25:24,112-->00:25:30,113
Thật là, anh luôn làm em điên lên đấy.
Anh có hiểu ý em không?

30
00:25:32,112-->00:25:36,113
Khó có thể tin được rằng anh vẫn còn sống.

30
00:25:37,112-->00:25:40,113
Xin... xin lỗi...

30
00:25:42,112-->00:25:45,113
Shidou...

30
00:25:47,112-->00:25:51,113
Nhưng anh không thể bỏ mặc Tohka trong
tình trạng như vậy, phải không?

30
00:25:53,112-->00:26:00,113
Và em biết rồi đấy, dù anh có bị thương
thì cơ thể anh vẫn có thể...

30
00:26:02,112-->00:26:04,113
Uida...!

30
00:26:06,112-->00:26:12,113
Hm, "Uida" cái gì. Anh vẫn muốn 
làm anh hùng chính nghĩa à?

30
00:26:13,112-->00:26:19,113
Em đang làm cái quái gì vậy!? Em đá anh
như thế... là nguy hiểm lắm đấy! Hụ hụ...!

30
00:26:20,112-->00:26:27,113
Mặc dù anh có thể phục hồi, nhưng anh
là một thằng ngốc làm việc không có suy nghĩ và
cơ thể anh vẫn có giới hạn. 

30
00:26:27,212-->00:26:29,113
Em không còn cách nào khác ngoài việc 
dạy trực tiếp trên cơ thể anh phải không?

30
00:26:31,112-->00:26:33,113
Umm...

30
00:26:35,112-->00:26:44,113
Khả năng của Kotori là khả năng phục hồi
vết thương tuyệt vời. Khi tôi bị chấn thương,
sức mạnh tự động làm lành vết thương. Tôi có 
sức mạnh này vì tôi đã phong ấn sức mạnh của Kotori.

30
00:26:45,112-->00:26:51,113
Vì vậy, đến nay tôi đã làm được một số chuyện
điên rồ khác nhờ sức mạnh này. Nhưng thực sự 
tôi không biết mức độ mà sức mạnh này có thể 
phục hồi vết thương của tôi đến đâu.

30
00:26:53,112-->00:26:57,113
Xin lỗi... Đây hoàn toàn là lỗi của anh.

30
00:26:59,112-->00:27:05,113
Mà thôi, dù sao anh cũng là thằng ngốc
chuyên đi giúp đỡ người khác thôi mà.

30
00:27:07,113-->00:27:10,113
Anh chẳng còn gì để nói cả.

30
00:27:13,113-->00:27:20,113
Nhận ra rồi à? Bây giờ thì anh không thể 
làm những gì anh muốn nữa rồi.

30
00:27:27,113-->00:27:32,113
Bây giờ anh có thể chết một cách dễ dàng.

30
00:27:33,113-->00:27:36,113
Hở? Nhưng trong người anh có sức mạnh của em mà...

30
00:27:37,113-->00:27:43,113
Sau khi bị mắc kẹt trong sức mạnh của Tohka,
Bọn em đã mời mọi người đến Fraxinus 
kiểm tra và đã có kết quả.

30
00:27:45,113-->00:27:57,113
Theo kết quả phân tích, sức mạnh mất kiểm soát
đã không gây thiệt hại một cách kỳ diệu. 
Tuy nhiên, nó đã trở thành một vấn đề nghiêm trọng.

30
00:27:59,113-->00:28:01,113
Vấn đề à?

30
00:28:02,113-->00:28:13,113
Đúng vậy. Shin, sức mạnh Tinh Linh trong 
cơ thể cậu đang chảy ngược lại. Tất cả
sức mạnh Tinh Linh cậu đã phong ấn từ trước đến giờ.

30
00:28:14,113-->00:28:19,113
Nhưng... nhưng mà, đây không phải là 
lần đầu tiên sức mạnh Tinh Linh đảo ngược.

30
00:28:22,113-->00:28:31,113
Đúng là sức mạnh Tinh Linh đảo ngược đã
xảy ra, nhưng chỉ là tạm thời phải không?

30
00:28:34,113-->00:28:43,113
Nếu lần này cũng giống như các lần trước thì
không có vấn đề. Nhưng lần này thì khác một chút.

30
00:28:44,113-->00:28:52,113
Bọn em tiếp tục kiểm tra nhưng không thấy gì mới.
Bọn em không biết chuyện gì đang xảy ra.

30
00:28:53,113-->00:29:04,113
Theo như bọn em kiểm tra Tohka và Yoshino,
bọn em cũng không thấy gì.
Shidou, anh không có ý tưởng gì sao?

30
00:29:05,113-->00:29:12,113
Không... anh không biết gì cả. Lúc đó
anh cảm thấy giống như một dòng điện chạy
ra khỏi người anh. Chỉ vậy thôi.

30
00:29:14,113-->00:29:19,113
Cậu không cảm thấy bất kỳ điều gì bất thường à?

30
00:29:20,113-->00:29:24,113
Vâng... Lúc đó em chỉ cảm thấy mệt mỏi 
một chút, chẳng còn gì nữa.

30
00:29:26,113-->00:29:34,113
Điều đó dường như không có gì nhiều. 
Ngoài ra, bọn em không tìm thấy bất cứ 
điều gì là khi phân tích anh ở Fraxinus.

30
00:29:36,113-->00:29:45,113
Đó là sự thật. Chúng tôi đang tiến hành 
rà soát mọi thứ, nhưng có vẻ như vậy.

30
00:29:47,113-->00:29:50,113
Xin lỗi. Mặc dù chúng ta đang nói về em,
nhưng em chẳng giúp gì được.

30
00:29:51,113-->00:29:59,113
Không cần phải xin lỗi đâu. Sau mọi chuyện,
chuyện này giống như một tai nạn bất ngờ.

30
00:30:00,113-->00:30:10,113
Shin, trường hợp này có thể giải thích rằng 
sức mạnh Tinh Linh vẫn đang bất ổn. Và khi
sức mạnh tiếp tục chảy ngược, sức mạnh 
Tinh Linh bảo vệ cậu đã biến mất.

30
00:30:11,113-->00:30:13,113
Điều đó có nghĩa là sao?

30
00:30:15,113-->00:30:26,113
Anh không còn bất tử nũa. Vì vậy, 
nếu anh mà gặp phải một tình huống nguy hiểm 
đến tính mạng, chắc chắn anh sẽ chết.

30
00:30:27,113-->00:30:29,113
Cái...!

30
00:30:31,113-->00:30:34,113
Cuối cùng anh cũng hiểu rồi phải không?

30
00:30:36,113-->00:30:41,113
Có nghĩa là nếu anh bị tấn công, anh sẽ... chết?

30
00:30:43,113-->00:30:49,113
Đúng vậy. Một lần nữa và một lần nữa, anh không 
làm gì khác ngoài lo lắng và lo lắng.

30
00:30:51,113-->00:31:04,113
Nếu Shidou chết... Nếu Shidou không còn
khả năng đó... em sẽ rất... rất buồn...!

30
00:31:06,113-->00:31:14,113
Hở...! Đừng lo lắng Tohka! Thôi nào! 
Quan trọng là bây giờ anh thấy ổn rồi!

30
00:31:16,113-->00:31:25,113
Anh mới là người phải lo lắng nhiều hơn một chút đấy,  
Shidou. Anh phải biết là Tohka không phải là 
người duy nhất không thể kiểm soát sức mạnh.


30
00:31:31,113-->00:31:36,113
Giống như Tohka, Yoshino cũng đã mất kiểm soát.

30
00:31:37,113-->00:31:39,113
Hả? Yoshino cũng vậy sao?

30
00:31:40,113-->00:31:53,113
Cũng giống Tohka, nguyên nhân vẫn chưa được biết rõ.
Căn hộ dành cho Tinh Linh giờ không còn sử dụng
được nữa vì mọi thứ đã đông lạnh cả rồi.

30
00:31:55,113-->00:32:00,113
Em... xin... lỗi...!

30
00:32:02,113-->00:32:08,113
Đừng lo Yoshino. Quan trọng hơn là 
em không sao chứ?

30
00:32:08,113-->00:32:16,113
Vâ... vâng...! Em... vẫn... ổn...!

30
00:32:17,113-->00:32:19,113
Anh hiểu rồi. Anh rất vui.

30
00:32:21,113-->00:32:27,113
Yaah! Ngay cả với em, đó cũng là 
một bất ngờ lớn đấy!

30
00:32:29,113-->00:32:36,113
Trước khi bọn em nhận ra nó, một sức mạnh 
nhanh chóng chảy vào bọn em, và bọn em 
không thể dừng lại.

30
00:32:37,113-->00:32:40,113
Xin lỗi, đó là lỗi của anh.

30
00:32:41,113-->00:32:48,113
Áichàchà! Được rồi, được rồi!
Sau tất cả, chúng ta vẫn là bạn mà.

30
00:32:49,113-->00:32:52,113
Ờ... ờ... cảm ơn nhiều, Yoshinon.

30
00:32:54,113-->00:33:10,113
Mọi chuyện giờ đã tạm ổn, nhưng Tohka và Yoshino
sẽ lên Fraxinus để kiểm tra lần cuối.
Và sau đó họ sẽ ở nhà chúng ta.

30
00:33:12,113-->00:33:15,113
À, được rồi. không vấn đề gì... Hả?

30
00:33:17,113-->00:33:25,113
Sao anh hoảng hốt vậy? Chẳng phải Tohka 
đã sống ở đây một thời gian sao?

30
00:33:26,113-->00:33:31,113
Đúng là vậy, nhưng... cũng rất khó đấy...
Ý anh là... đã có nhiều chuyện xảy ra...

30
00:33:33,113-->00:33:50,113
Ara, em thấy anh đang mừng thầm phải không? 
Vậy là anh đang chờ đợi điều gì xảy ra khi họ ở đây à?

