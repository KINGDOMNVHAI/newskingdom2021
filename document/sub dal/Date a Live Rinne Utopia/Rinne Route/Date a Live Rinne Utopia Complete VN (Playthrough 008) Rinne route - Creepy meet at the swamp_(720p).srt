﻿0
00:00:05,112-->00:00:08,113
Đúng như kế hoạch, 
tất cả bọn họ đều có mặt như đã hẹn...

1
00:00:09,112-->00:00:14,113
Tohka, Origami, Rinne... và cả Kotori, Yoshino.
Chúng ta chỉ thiếu Tonomachi thôi...

2
00:00:15,112-->00:00:18,113
Người bày trò thì vẫn chưa thấy đâu!

3
00:00:19,112-->00:00:27,113
O... Onii-chan... về nhà đi...

4
00:00:28,112-->00:00:31,113
Hở, chúng ta chỉ đứng ở đây thôi. Em sao vậy?

5
00:00:32,112-->00:00:35,113
Em sợ à, Kotori?

6
00:00:36,112-->00:00:39,113
Em không sợ!

7
00:00:40,112-->00:00:45,113
Thật à? Hồi nhỏ, mỗi khi nghe truyện ma,
em còn không dám đi vệ sinh một mình, nhớ không?

8
00:00:46,112-->00:00:54,113
Em... em không còn nhỏ nữa!

9
00:00:55,112-->00:01:00,113
Được rồi. Đừng khóc nữa, Kotori! Chúng ta sẽ đợi
Tonomachi thêm một lúc nữa. Nếu cậu ta không đến
thì chúng ta sẽ về, được chứ?

13
00:01:01,112-->00:01:06,113
Shidou không cần quá lo cho Kotori-chan đâu.

14
00:01:07,112-->00:01:10,113
Không, tớ chỉ làm con bé bớt sợ thôi.

15
00:01:11,112-->00:01:19,113
Không sao đâu, Kotori-chan. 
Bọn chị đi cùng em mà phải không?

16
00:01:20,112-->00:01:25,113
V... vâng... Em sẽ cố.

17
00:01:26,112-->00:01:33,113
Đây chắc chắn không phải là Kotori luôn đá tôi.
Thật sai lầm khi nghe lời Tonomachi... Nhưng nghĩ lại
thì Kotori vẫn đang ở cùng chúng tôi mà...

18
00:01:34,112-->00:01:42,113
Chị Ko... Kotori-san..., chị... không sao chứ?

19
00:01:43,112-->00:01:54,113
Shidou-kun, anh không cần quá lo đâu!
Mặc dù em hiểu vấn đề nhưng nếu họ không sợ 
thì chuyến đi này vô nghĩa đấy.

20
00:01:55,112-->00:01:59,113
Yoshinon, anh không lo chuyện đó...

21
00:01:59,113-->00:02:04,113
Shidou, tớ sợ quá.

22
00:02:05,112-->00:02:08,513
Hở...? Origami!

23
00:02:10,112-->00:02:19,113
To... Tobiichi Origami! Cô nghĩ cô đang 
làm gì vậy? Tránh xa Shidou ngay lập tức!

24
00:02:20,112-->00:02:29,113
Không được. Tôi không thể di chuyển được.
Vậy nên tôi phải bám vào một ai đó.

25
00:02:34,112-->00:02:37,113
Tôi có thể cảm thấy chúng! 
Tôi có thể cảm thấy khu vực đó của Origami!

26
00:02:38,112-->00:02:42,113
Từ... từ từ đã nào, Origami!

26
00:02:45,112-->00:02:51,513
Shi... Shidou! Em... em cũng sợ lắm!

26
00:02:52,512-->00:02:57,113
Hở! Tohka? Sao em lại sợ?

26
00:02:58,112-->00:03:01,113
To... Tohka?

26
00:03:02,112-->00:03:10,513
Shi... Shidou! Em... em sợ nhiều hơn!

26
00:03:11,512-->00:03:16,113
Aaaaa, đủ rồi! Dừng lại, dừng lại!
Em cũng bắt chước sao Tohka?

26
00:03:17,112-->00:03:21,113
Em sẽ không thua Tobiichi Origami đâu!

26
00:03:22,112-->00:03:25,113
Lúc... lúc này không phải lúc đùa đâu!

26
00:03:26,112-->00:03:32,113
Shidou, tớ sợ quá, không di chuyển được.

26
00:03:33,112-->00:03:37,113
Nghe này! 2 cậu làm ơn nghe tớ đã! Bỏ ra nào!

26
00:03:38,112-->00:03:43,113
Nghe này, mấy cậu quên mất mục đích chính
rồi à? Chúng ta đến đây là để 
cúng lễ vật cho Tengu-Ushi.

26
00:03:44,112-->00:03:49,113
Tớ vẫn đang đợi cậu làm đây.

27
00:03:50,112-->00:03:55,513
Xin... xin lỗi Shidou... 
Em quên mất.

28
00:03:56,512-->00:04:03,113
Aaa, tại sao? Tại sao luôn thành ra thế này?
Tất cả là do Tonomachi!
Nhất định tôi phải cho hắn một trận.

29
00:04:04,112-->00:04:07,113
N... này Shidou...

32
00:04:08,512-->00:04:11,113
Gì vậy Rinne?

33
00:04:12,112-->00:04:21,113
Trong khi chờ đợi Tonomachi-kun,
Cậu thấy có nên xem lại đồ lễ 
đã mua đủ chưa không?

35
00:04:22,112-->00:04:25,113
Ồ! Ý hay đấy! Nice idea!

38
00:04:27,112-->00:04:32,113
Ừ! Nghe được đấy, Rinne!

39
00:04:33,112-->00:04:36,513
Vậy à? Cảm ơn nhé.

40
00:04:37,512-->00:04:43,113
Được rồi! Vậy bắt đầu từ Tohka.
Em đã mua những gì?

41
00:04:44,112-->00:04:50,113
Ừ! Những gì em mua là đây!

42
00:04:51,112-->00:04:55,113
To... Tohka-chan, cái gì vậy?

43
00:04:56,112-->00:05:04,113
Đó là một chiếc bánh kẹp thịt nướng 
em mua ở siêu thị. Ngay cả khi nó 
là thịt nguội thì nó vẫn rất ngon!

45
00:05:05,112-->00:05:10,113
Waa, trông ngon quá... Mai mốt tớ sẽ ăn thử xem.

46
00:05:11,112-->00:05:21,113
Ừ, tớ biết cậu cũng sẽ thích mà Rinne!
Nếu cậu đi cửa hàng thì tớ sẽ chỉ cho!
Tớ nhớ hết mùi thịt và gia vị rồi.

47
00:05:22,112-->00:05:26,113
Ừ, tớ rất mong chờ đấy!

48
00:05:30,512-->00:05:35,113
Bây giờ tới lượt cậu. Rinne, cậu mua gì vậy?

49
00:05:36,112-->00:05:40,113
Eto... Lúc đầu thì tớ không biết nên mua gì.

50
00:05:42,112-->00:05:49,113
Sau đó tớ nghĩ là nên mua cái gì đó 
để ăn thì tốt hơn.

51
00:05:55,112-->00:05:57,113
Waa... một quả dưa hấu à?

52
00:05:58,312-->00:06:09,113
Ừ... Vì nó trông rất ngon nên tớ mua nó 
mà không cần suy nghĩ... Như vậy có được không?

53
00:06:10,112-->00:06:12,513
Không, không sao đâu! Cũng được mà.

54
00:06:14,112-->00:06:17,113
Vậy thì may quá.

55
00:06:18,112-->00:06:24,113
Ừ, dù sao thì chúng ta cũng mang
đồ lễ đến cho Tengu-Ushi mà phải không? 
Tớ nghĩ có thêm nhiều lựa chọn cũng tốt.

56
00:06:25,512-->00:06:29,113
Ừ, đúng vậy. Không có vấn đề gì phải không?

57
00:06:30,112-->00:06:35,113
Ừ, đúng đấy. Vậy còn Origami? Cậu mang gì vậy?

58
00:06:37,112-->00:06:41,113
Hoa loa kèn trắng để cúng lễ.

59
00:06:42,112-->00:06:46,113
Hee, đúng là học sinh giỏi nhất trường.

60
00:06:47,112-->00:06:48,513
Cậu không thích à?

41
00:06:49,512-->00:06:52,113
Hở, không ... Ý tớ là tốt mà.

42
00:06:53,112-->00:06:55,113
Tớ hiểu rồi.

42
00:06:56,112-->00:06:58,113
Ờ... Ừ...

42
00:06:59,112-->00:07:02,113
Được rồi, bây giờ là đến Yoshino.

43
00:07:03,112-->00:07:11,513
Hở... Vâng... Của em đây...

62
00:07:12,512-->00:07:15,113
Hở? Một một con bò... nhồi bông ?

63
00:07:16,112-->00:07:21,113
Hở... eto

64
00:07:22,112-->00:07:29,113
Không được sao, Shidou-kun?
Anh có thấy chỗ hở phía dưới không?

65
00:07:30,112-->00:07:33,113
À, đúng vậy. Dạng như một con rối
đeo ở tay phải không? (Giống Yoshinon)

66
00:07:34,112-->00:07:42,113
Bọn em đã hỏi ý kiến Reine-san.
Cô ấy nói có lẽ Tengu-Ushi muốn có một người bạn.

67
00:07:43,112-->00:07:46,513
Đúng vậy...

68
00:07:47,512-->00:07:50,113
Hee, Reine-san...

69
00:07:51,112-->00:08:02,113
Em... em cũng đã cố gắng... nói chuyện
với mọi người... nhưng không được...

70
00:08:03,112-->00:08:07,113
Yoshino đúng là quá nhút nhát
trong việc nói chuyện với người khác.

83
00:08:08,112-->00:08:15,113
Vì vậy, bọn em nghĩ họ sẽ trở thành bạn của nhau.
Cả 2 đều có sừng bò mà phải không?

84
00:08:16,112-->00:08:22,113
Sống một mình... chắc cô đơn lắm.

85
00:08:23,112-->00:08:27,113
Em nói đúng. Anh cũng thấy đây là
một việc tốt. Em rất biết quan tâm 
đến người khác đấy, Yoshino.

86
00:08:28,112-->00:08:31,113
V... vâng...

87
00:08:32,112-->00:08:34,513
Cuối cùng là...

89
00:08:40,112-->00:08:43,113
Em mang theo gì vậy, Kotori?

90
00:08:43,112-->00:08:46,113
Thế còn Onii-chan thì sao?

91
00:08:47,112-->00:08:50,113
Được rồi. Vậy anh sẽ cho xem trước. Đây!

92
00:08:51,112-->00:08:55,513
Anh có mua một cái bánh trái cây đây.
Anh mua trong tiệm bánh xịn đấy.

93
00:08:56,512-->00:09:02,113
Shidou! Cái bánh đó trông ngon quá!

94
00:09:03,112-->00:09:12,113
Bánh ngọt này trẻ con thích lắm đấy.

98
00:09:11,112-->00:09:17,113
Vậy à. Vậy lúc nào đó 
tôi cũng phải đi ăn mới được!

99
00:09:17,513-->00:09:20,113
Ừ, vậy còn em thì sao Kotori?

100
00:09:21,112-->00:09:24,113
Vâng...

101
00:09:27,112-->00:09:31,113
Em có mang theo... cái này!

102
00:09:36,112-->00:09:40,113
Um... Khoan đã! Cái đó là nơ cột tóc mà!

103
00:09:41,112-->00:09:46,113
Đôi mắt của anh giống như hai lỗ hổng 
trong một miếng gỗ vậy Onii-chan.

104
00:09:47,112-->00:09:50,113
Ý em là sao?

105
00:09:51,112-->00:09:59,113
Anh khọng biết nơ cột tóc này à? 
Nó là hàng hiếm có đấy.

Esta paleta, sabes? Es distita de la de siempre.
Es unapaleta de venta limitada por regiones.

106
00:10:00,112-->00:10:02,113
Vậy à...

113
00:10:03,112-->00:10:06,113
Mình chẳng hiểu gì cả...

114
00:10:07,112-->00:10:12,113
A! Nhưng vì cái này là lễ vật nên em
sẽ không cho Onii-chan đâu!

1
00:10:13,112-->00:10:16,113
Vui quá nhỉ... Anh không cần nó đâu!

1
00:10:20,512-->00:10:23,113
Sao em ngạc nhiên vậy?

1
00:10:25,112-->00:10:29,113
Chúng tôi đợi một lúc nữa. Nhưng có chuyện gì vậy?
Tại sao Tonomachi vẫn chưa xuất hiện?

1
00:10:34,112-->00:10:37,513
Đã tối rồi... Đã quá thời gian hẹn rồi 
mà Tonomachi vẫn chưa xuất hiện...

1
00:10:38,512-->00:10:42,113
Có thể hắn ta đã cho cả đám leo cây...
Không, hắn ta muốn đi chơi chung 
với các cô gái mà...

1
00:10:44,112-->00:10:48,113
Shidou, có chuyện gì xảy ra với Tonomachi vậy?

1
00:10:49,112-->00:10:54,113
Anh không biết. Anh đã gọi
nhưng không thấy trả lời.

368
00:10:53,112-->00:10:56,513
Vậy à.

368
00:10:58,112-->00:11:06,113
Shidou-kun, có khi nào anh ấy bị lạc không?

368
00:11:07,112-->00:11:10,113
Ừ, anh cũng nghĩ vậy.

368
00:11:11,112-->00:11:19,113
Có thể lắm. Nhưng chính Tonomachi-kun
đã hẹn chúng ta tới đây cơ mà.

68
00:11:20,112-->00:11:22,113
Đúng là vậy.

368
00:11:23,112-->00:11:31,113
Có thể là lời nguyền bí ẩn của Tengu-Ushi.

368
00:11:32,512-->00:11:35,113
Cậu nói những điều đáng sợ đó
mà không hề chớp mắt à?

368
00:11:36,112-->00:11:44,113
Một người bạn đã không đến. Tớ nghĩ rằng 
đây là một vụ mất tích bí ẩn khác.

368
00:11:50,112-->00:11:53,113
Ko... Kotori? Nếu em sợ quá thì em
có muốn về nhà không?

368
00:11:54,112-->00:11:57,513
Em không sợ đâu!

368
00:11:58,512-->00:12:01,113
Nhưng mà... em đang run như cầy sấy kìa.

368
00:12:02,113-->00:12:06,113
Cái đó là do lạnh thôi!

368
00:12:07,113-->00:12:13,113
À, hiểu rồi, hiểu rồi! Vậy anh sẽ xem xét
xung quanh rồi quay lại. Nếu anh không thấy gì
lạ thì chúng ta sẽ về nhà, được chứ?

368
00:12:14,112-->00:12:18,113
Thật không Onii-chan?

368
00:12:19,112-->00:12:24,113
Thật. Chúng ta sẽ không đợi Tonomachi nữa.
Mọi người cứ ở đây đợi tớ nhé.

368
00:12:25,112-->00:12:28,113
Ừ, em hiểu rồi!

368
00:12:37,512-->00:12:40,113
Mmmm... Chỗ này mình chưa đi xem à?

368
00:12:41,112-->00:12:43,113
Này Tonomachi!

368
00:12:44,112-->00:12:46,113
Ông đâu rồi?

368
00:12:47,112-->00:12:49,113
Ông có ổn không? Nếu ông ở đây thì trả lời đi.

368
00:12:50,112-->00:12:55,113
Thật là vô ích, mình không tìm thấy gì.
Chẳng thấy Tonomachi đâu cả...

368
00:12:56,112-->00:13:00,113
Cũng chẳng có gì bí ẩn ở đây hết.
Ngay từ đầu, hắn ta bịa ra mọi chuyện...

368
00:13:01,112-->00:13:06,113
Rõ... rõ ràng là bóng ma không tồn tại...
Và... và thậm chí nếu có, mình cũng không sợ...

368
00:13:10,112-->00:13:12,113
Hở? Cái... cái gì vậy?

368
00:13:13,113-->00:13:17,113
A, Shidou!

368
00:13:18,112-->00:13:21,113
Hả? Ri... Rinne? Sao lại...?

368
00:13:22,112-->00:13:30,113
Ừ... ừ. Tớ đi theo cậu đấy. Nhưng hình như
tớ đã làm cậu giật mình phải không?

368
00:13:31,112-->00:13:34,113
Đúng vậy đấy... Làm thế có hại cho tim 
lắm đấy, cậu biết không?

368
00:13:35,112-->00:13:46,113
Tớ nghe thấy tiếng ai đó đi bộ,
và đến xem thì thấy cậu ở đây.
Nhưng cũng may... trong đêm tối thế này,
đi một mình cũng hơi sợ.

368
00:13:47,112-->00:13:50,113
Ừ... ừ... cậu nói đúng.

368
00:13:51,112-->00:13:56,113
Có chuyện gì vậy? Mặt cậu 
tái xanh kìa. Cậu không sao chứ?

368
00:13:57,112-->00:14:00,113
Hở... không có gì đâu. 
Tại tớ hơi lo thôi. Tớ ổn mà!

368
00:14:01,112-->00:14:07,113
Thật không? Nhưng mà còn Tonomachi-kun đâu?

368
00:14:08,112-->00:14:10,513
Tớ không biết nữa.


368
00:14:11,512-->00:14:15,113
Vậy tớ đi chung với cậu được không?

368
00:14:16,112-->00:14:19,113
Hở? Cậu không quay lại với mọi người sao?

368
00:14:20,112-->00:14:24,113
Nếu tớ muốn vậy thì tớ đã không đến đây.

368
00:14:25,112-->00:14:28,113
Ừ... ừ... cũng được. Vậy chúng ta cùng đi nhé.

368
00:14:32,112-->00:14:35,113
Ừ... Vậy chúng ta đi sang bên kia đi.
Nhớ để ý xung quanh đấy.

368
00:14:36,113-->00:14:39,113
Ừ được rồi.

368
00:14:40,112-->00:14:43,113
Nè Shidou?

368
00:14:44,112-->00:14:45,513
Gì cơ?

368
00:14:46,512-->00:14:50,113
Hôm nay đi bơi vui thật đấy, phải không?

368
00:14:51,112-->00:14:53,113
Ừ, mùa hè mà được đi bơi thì sướng lắm đấy.

368
00:14:54,112-->00:15:00,113
Tohka-chan lúc ở trong cửa hàng bánh kẹo 
trông dễ thương thật đấy.

368
00:15:01,112-->00:15:06,113
Cô ấy rất thích đồ ăn mà. Nếu cô ấy
không sáng mắt lên mới là chuyện lạ đấy.

368
00:15:07,112-->00:15:10,513
Đúng vậy.

368
00:15:11,512-->00:15:14,113
Có chuyện gì à?

368
00:15:15,112-->00:15:19,113
Không có gì đâu, không có gì đâu.

368
00:15:20.112-->00:15:23.113
Tớ cũng vui khi cả ba chúng ta đều đi
chung. Thật sự buổi hẹn hò rất vui đấy.

368
00:15:24,112-->00:15:34,113
Ừ, tớ cũng rất vui đấy! Mặc dù lúc đầu 
tớ nghĩ tớ có thể khiến các cậu không thoải mái...

368
00:15:35,112-->00:15:38,113
Nhưng cậu đã rất thích buổi hẹn hò phải không?

368
00:15:39,112-->00:15:50,113
À ...! Eto ... Chỉ là ... Đó là lần đầu tiên 
tớ đi bơi với Tohka-chan... và cũng đã 
có thời gian ở bên cậu, Shidou ...

368
00:15:51,112-->00:15:54,113
À... chuyện đó thật là...

368
00:16:00,512-->00:16:03,113
Này Rinne.

368
00:16:04,112-->00:16:07,113
Gì vậy Shidou?

368
00:16:08,512-->00:16:11,113
Ừ... sau này chúng ta sẽ đi hẹn hò nữa nhé?

368
00:16:12,112-->00:16:19,113
Ừ, chúng ta sẽ đi chơi với nhau...
chỉ có 2 chúng ta...

368
00:16:20,512-->00:16:23,113
Hở?

368
00:16:24,112-->00:16:29,113
À, không có gì! Ý tớ là 2 chúng ta
cùng với những người khác.

368
00:16:30,112-->00:16:33,113
Ờ. Quay lại vấn đề chính. Chúng ta
vẫn chưa tìm thấy Tonomachi.

368
00:16:34,113-->00:16:38,113
Đúng vậy... Cậu có muốn đi tiếp không?

368
00:16:39,112-->00:16:42,113
Đi tiếp chắc cũng chẳng thấy gì... 
Hay là quay lại nhé?

368
00:16:43,112-->00:16:49,113
Cậu nói đúng. Có lẽ Kotori-chan 
và những người khác đang lo lắng đấy.

368
00:16:53,112-->00:16:59,113
A! Cuối cùng anh cũng về! 
Onii-chan, anh đã đi đâu vậy?

368
00:17:00,112-->00:17:04,513
Xin... xin lỗi... Anh đã đi tìm cậu ấy.
Phải không Rinne?

368
00:17:05,512-->00:17:08,113
Ừ... ừ...

368
00:17:09,112-->00:17:15,113
Điều gì sẽ xảy ra nếu anh và Rinne
mất tích trong lúc đi tìm chứ?

368
00:17:16,112-->00:17:18,113
Anh xin lỗi...

368
00:17:19,112-->00:17:22,513
Xin lỗi nhé Kotori-chan...

368
00:17:23,512-->00:17:28,113
Để chuyện đó sang một bên. Rốt cuộc 
Tonomachi đang ở đâu? Chẳng lẽ...

368
00:17:33,112-->00:17:35,513
Này, Tonomachi!

368
00:17:36,512-->00:17:39,113
Tôi đã đi quanh đây lần nữa 
nhưng vẫn không thấy gì...

368
00:17:40,112-->00:17:44,113
Onii-chan! Chúng ta cùng nhau đi lễ nhé!

368
00:17:45,112-->00:17:47,113
Ờ... ờ. Bây giờ đi đi.

368
00:17:50,112-->00:17:57,113
Mình nghĩ ai đó đang gọi mình...
Cuối cùng mình cũng thấy giá trị của mình rồi!

368
00:17:58,112-->00:18:05,113
Dù bị muỗi đốt nhưng thế cũng đáng. 
Nào, mau lên đi!

368
00:18:06,112-->00:18:12,113
Khi thời cơ đến, khi các cô gái hoảng sợ,
đó là lúc mình thể hiện bản lĩnh!

368
00:18:12,512-->00:18:19,113
Và mình sẽ tận hưởng cảm giác tuyệt vời 
khi làm anh hùng của họ!

368
00:18:20,112-->00:18:24,113
Gì vậy?

368
00:18:36,112-->00:18:39,113
Đó là gì vậy? Tớ nghe thấy tiếng hét
từ phía đó... A, Tonomachi!


368
00:18:40,112-->00:18:44,113
Này, sao ông đến trễ thế? Ông đã ở đâu?

368
00:18:45,112-->00:18:50,113
XUẤT HIỆN RỒI!

368
00:18:53,112-->00:18:56,113
N... này? Ông đi đâu thế?

368
00:18:57,512-->00:19:01,113
xuất hiện, xuất hiện rồi! Xuất hiện rồi!

368
00:19:02,112-->00:19:03,513
Cái gì xuất hiện cơ?

368
00:19:04,512-->00:19:08,113
Tôi không biết! Nhưng có cái gì đó ở đó đấy!

368
00:19:09,112-->00:19:14,113
Lại một trò đùa nữa à? Có phải ông
đang cố gắng chứng minh Tengu-Ushi có thật
để hù dọa mọi người phải không?

368
00:19:15,112-->00:19:20,113
Ông... ông đang nói gì vậy Itsuka?
Tôi xin lỗi vì đến trễ mà!

368
00:19:21,112-->00:19:23,113
Không, không, ông không hiểu à?

368
00:19:24,112-->00:19:28,113
D... dù sao, tôi phải về đây!
Ông cũng nên về nhanh đi, Itsuka!

368
00:19:38,112-->00:19:42,113
Này Kotori? Em không sao chứ?

368
00:19:44,112-->00:19:49,113
Có... có cái gì đó... vừa xuất hiện ạ...?

368
00:19:50,112-->00:19:53,113
Không, anh không biết nữa. Nhưng chắc
là Tonomachi lại dọa thôi.

368
00:19:58,512-->00:20:06,513
Aaa ... Anh định làm gì đây Shidou-kun?
Anh không thấy Yoshino dễ thương đang sợ hãi sao?

368
00:20:07,513-->00:20:12,113
Không phải lúc nói chuyện này.
Chúng ta để lễ vật lại và đi về thôi.

368
00:20:13,112-->00:20:18,113
Vâng! Được rồi!

368
00:20:19,112-->00:20:29,113
Ừ, em cũng thấy đói rồi.
Tobiichi Origami, cô định đi đâu đấy?

368
00:20:30,112-->00:20:40,113
Không phải việc của cô. Cô đã có quá nhiều
thời gian ở bên cãnh Shidou rồi.

368
00:20:41,112-->00:20:46,113
Cô... cô nói gì? Tôi không để cô 
đi chung với anh ấy đâu!

368
00:20:47,112-->00:20:51,113
Này! 2 cậu lại cãi nhau...

368
00:20:59,512-->00:21:02,113
O... Onii-chan?

368
00:21:03,112-->00:21:08,113
Shi... Shidou-san?

368
00:21:09,512-->00:21:15,113
Vừa rồi là tiếng gì vậy?

368
00:21:16,112-->00:21:22,113
Một tiếng thét. Nó có lẽ là Tengu-Ushi...

368
00:21:30,512-->00:21:35,113
N ... không có! Anh không nghe thấy gì cả! 
Cậu cũng thế phải không Rinne?

368
00:21:36,112-->00:21:44,113
Hở? Eto... Không! Tớ không nghe thấy 
gì hết... chắc là vậy.

368
00:21:45,102-->00:21:51,113
Nhưng chúng ta về mà chưa làm gì cả
cũng không ổn. Chúng ta để lại lễ vật
rồi mới về phải không?

368
00:21:52,112-->00:21:58,113
Ờ... cậu nói đúng! Vậy chúng ta để lễ vật
lên bàn rồi cầu nguyện và...

368
00:22:03,112-->00:22:05,113
Hở?

368
00:22:08,112-->00:22:11,113
Hả? Cái quái gì vậy?

368
00:22:12,112-->00:22:18,113
Đây là Tengu-Ushi à? Sức mạnh tuyệt thật!

368
00:22:22,112-->00:22:25,113
Không... không phải! 
Đây chỉ là... một con cá sấu thôi!

368
00:22:28,112-->00:22:31,113
Aaaa... xỉu...

368
00:22:32,112-->00:22:37,113
Em... em không sao chứ Kotori-chan? Kotori-chan?

368
00:22:48,112-->00:22:57,113
Khôôôông...! Yoshino, cậu phải bình tĩnh!

368
00:23:01,912-->00:23:04,113
Shidou, tớ sợ quá.

368
00:23:05,112-->00:23:07,513
Cậu nghĩ đây là lúc để làm việc này à?

368
00:23:08,512-->00:23:12,113
Shidou... Tớ không muốn rời khỏi cậu.

368
00:23:13,112-->00:23:17,113
Tobiichi Origami! Cô nghĩ cô đang làm gì thế hả?

368
00:23:18,113-->00:23:25,113
Cô có thể đi chiến đấu với nó.
Còn Shidou và tôi sẽ phát triển 
tình yêu của chúng tôi.

368
00:23:28,112-->00:23:32,513
Em cũng không chịu thua đâu Shidou!

368
00:23:33,512-->00:23:36,113
Tohka? Em làm gì vậy?

368
00:23:37,112-->00:23:41,113
Tôi đang nghĩ đây đúng là một tình huống 
hoàn hảo... Không được! Đây không phải lúc
để suy nghĩ về chuyện đó!

368
00:23:42,113-->00:23:45,113
Này... 2 cậu có thể... bỏ ra được không?
Đây không phải là lúc...!

68
00:23:49,512-->00:23:52,113
Cái... cái... gì vậy? Tiếng gì vậy?

368
00:23:53,112-->00:23:55,513
Tengu-Ushi...

368
00:23:56,512-->00:23:58,513
Nó biến mất rồi.

368
00:23:59,512-->00:24:02,113
Nhưng cái gì ...? Nó là một trò đùa ... phải không?

368
00:24:03,112-->00:24:06,113
Không thể có cái gì biến mất hoàn toàn được... 

368
00:24:07,112-->00:24:10,113
Vừa rồi... Đó là...?

368
00:24:11,512-->00:24:15,113
Shi... Shidou! Anh đi đâu thế?

368
00:24:16,112-->00:24:18,113
Xin lỗi Tohka! Em chờ anh một chút!

368
00:24:19,112-->00:24:22,113
Đó là... Chẳng lẽ là cô ấy?

368
00:24:27,112-->00:24:33,113
Ara ara, có vẻ như tơ xuất hiện 
không đúng rồi, Shidou-san.

368
00:24:35,112-->00:24:38,113
Ku... Kurumi? Tại sao...?

368
00:24:39,112-->00:24:47,113
Xin lỗi vì làm gián đoạn khi cậu đang rất vui,
nhưng tớ sẽ không ở lại lâu đâu.

368
00:24:48,112-->00:24:51,113
Này... này, khoan đã! Kurumi!

368
00:24:52,112-->00:25:03,113
Tiếc là tớ không có thời gian bây giờ để
gặp cậu, Shidou-san. Thật sự tớ muốn nói
chuyện nhiều hơn với cậu... Xin lỗi.

368
00:25:10,112-->00:25:13,113
Này! Hay là... Kurumi đã làm những chuyện này?

368
00:25:14,112-->00:25:17,113
Không có thời gian để ở lại và nghĩ nữa!
Mình phải nhanh về thôi!

368
00:25:24,112-->00:25:26,113
Mọi người... vẫn ổn chứ?

368
00:25:27,112-->00:25:30,113
Shidou, không sao chứ?

368
00:25:31,112-->00:25:33,113
Ờ... Ừ...

368
00:25:34,112-->00:25:39,513
Shidou! Có phải là Tengu-Ushi không?

368
00:25:40,512-->00:25:44,513
Anh đã nói với em đó là một con cá sấu ...
Khoan đã! Đúng vậy đấy! Đó là...

368
00:25:45,512-->00:25:51,113
Shi... Shidou! Đến đây nhanh lên! Kotori-chan ...!

368
00:25:52,112-->00:25:55,113
Cái...? Ko... Kotori...?

368
00:25:56,112-->00:25:58,513
Không sao chứ?

400
00:25:59,512-->00:26:04,113
Tớ nghĩ con bé chỉ ngất đi thôi, nhưng mà...

400
00:26:05,112-->00:26:07,113
Xin... xin lỗi Kotori...

400
00:26:08,112-->00:26:11,113
Giờ phải đưa con bé về Fraxinus gặp Reine-san...

400
00:26:12,112-->00:26:15,513
Này! Shidou-kun!

400
00:26:16,512-->00:26:19,113
Hở? Yoshino?

400
00:26:20,112-->00:26:27,113
Shi...Shidou-san...

400
00:26:28,112-->00:26:31,113
Sao vậy, Yoshino?

400
00:26:32,112-->00:26:43,513
Yoshino đã chịu đựng tất cả rất tốt.
Nếu cô ấy mà khóc thì mọi thứ đã bị đóng băng rồi.

400
00:26:44,512-->00:26:48,113
Em... em nói đúng. Em đã làm rất tốt, Yoshino!
Em đúng là một cô gái mạnh mẽ!

400
00:26:49,112-->00:26:57,113
Cảm... ơn... anh... rất... nhiều...

400
00:26:58,113-->00:27:02,113
Được rồi! Chúng ta không cần phải làm
nghi lễ gì nữa. Cứ để lễ vật lại rồi về thôi.

Muy bien! Las ofrendas quedan canceladas! 
Vamos a dejar esto acá por hoy!

400
00:27:03,112-->00:27:12,113
Cậu nói đúng. Giờ cũng đã quá trễ rồi.
Hơn nữa mọi người cũng đã mệt mỏi rồi.

400
00:27:13,112-->00:27:18,113
Ừ, đúng đấy. Sau khi về nhà, em sẽ ăn một chút...

400
00:27:19,112-->00:27:29,113
Tohka-chan, nến chị dũng cảm giống Yoshino thì
chị đã nhận được lời khen của Shidou-kun rồi phải không?

400
00:27:30,112-->00:27:37,113
Cái gì? Có đúng không Shidou?
Nếu em can đảm hơn thì em sẽ được khen phải không?

400
00:27:38,112-->00:27:42,113
Hở... không phải... Anh...

400
00:27:43,112-->00:27:48,513
Lúc nãy cô đã rất nhát gan đấy.

400
00:27:49,512-->00:27:53,113
Ý cô là sao?

400
00:27:54,112-->00:27:57,113
Mấy cậu còn cãi nhau bao nhiêu lần nữa đây?

400
00:27:58,112-->00:28:05,113
Được rồi, được rồi. Nhờ có họ mà tớ đã bớt sợ 
và quên dần những chuyện vừa xảy ra đấy.

400
00:28:06,112-->00:28:09,113
Ừ, cậu nói đúng.

400
00:28:14,112-->00:28:20,113
Yoshino và mọi người đã về nhà. Kotori bị ngất
nên tôi đã đưa đến Fraxinus.

400
00:28:21,112-->00:28:25,113
Các nhân viên y tế ngay lập tức đưa
con bé đi chữa trị. Tôi ngồi đợi trong phòng
điều khiển. Một lúc sau, Reine-san bước vào.

400
00:28:28,112-->00:28:32,113
Shin, tôi nên cho bao nhiêu muỗng đường?

400
00:28:33,112-->00:28:36,113
Hở? À... Vâng, cô cứ cho như mọi khi đi, Reine-san.

400
00:28:37,112-->00:28:40,113
Tôi hiểu rồi.

400
00:28:43,512-->00:28:47,113
Tôi nhận thấy cô ấy đã cho quá nhiều nhưng
tốt hơn là nên giả vờ như không có gì.

400
00:28:48,112-->00:29:04,113
Shin, cậu đã hẹn hò với các Tinh Linh.
Tuy nhiên, tình hình hiện nay đã tăng nguy cơ 
mất kiểm soát sức mạnh của cậu. Cậu biết chứ?

400
00:29:05,512-->00:29:08,113
Vâng...

400
00:29:09,112-->00:29:20,113
Không có vấn đề gì thì tốt. Tuy nhiên,
hãy nhớ những hành động của cậu đều
ảnh hưởng lớn đến tình trạng tinh thần.

400
00:29:21,112-->00:29:25,113
Vâng... xin lỗi... Vậy Kotori và
các cô gái khác vẫn ổn chứ?

400
00:29:26,112-->00:29:32,113
Các nhân viên y tế đang làm việc.
Có vẻ kết quả tốt.

400
00:29:33,112-->00:29:36,113
May quá...

400
00:29:37,112-->00:29:52,113
Kotori ngất xỉu có lý do nên
cậu không bị mất kiểm soát.
Nếu không, có lẽ bây giờ chỗ này đã cháy rụi.

400
00:29:53,112-->00:29:55,113
Sao? Vậy là lỗi do em sao?

400
00:29:56,112-->00:30:00,113
Tại sao tôi luôn khiến Kotori gặp chuyện?

400
00:30:01,112-->00:30:14,113
Shin, nếu cậu buồn, Tinh Linh cũng sẽ 
lo lắng. Hãy chấp nhận thất bại 
và học hỏi nó cho lần sau, được chứ?

400
00:30:15,112-->00:30:18,113
V... vâng! Cô nói đúng...

400
00:30:19,112-->00:30:22,113
Nhưng mà cô cho đường vào hơi nhiều quá rồi đấy!

400
00:30:23,112-->00:30:29,113
Vậy à? Tôi vẫn thường cho như thế này mà.

400
00:30:30,112-->00:30:32,513
Cô uống ngọt nhiều quá đấy!

400
00:30:33,512-->00:30:41,113
Vậy à...........

400
00:30:42,112-->00:30:46,113
Tôi thì lại thấy thiếu đấy.

400
00:30:47,112-->00:30:49,113
Hở?

400
00:30:50,112-->00:31:00,113
Dù sao, sau đó tôi sẽ bổ sung thêm ...
Shin, những hiện tượng xảy ra ở Tengu
vẫn chưa được phân tích xong.

400
00:31:01,113-->00:31:10,513
Fraxinus không như chúng ta muốn vì tình hình
Trục trặc hiện nay. Tình trạng này
Chúng tôi cũng đang rất lo lắng.

400
00:31:11,512-->00:31:14,113
Vâng, em hiểu.

400
00:31:15,112-->00:31:24,113
Tuy nhiên, chúng tôi cũng đã phát hiện ra
sóng Tinh Linh rất lớn bao phủ Tengu.
Đó có thể là một kết giới rất mạnh.

400
00:31:24,512-->00:31:31,113
Sóng Tinh Linh đó thậm chí còn mạnh hơn
cả những sóng Tinh Linh bình thường.

400
00:31:32,112-->00:31:36,113
Vậy chuyện gì đang xảy ra trong thành phố?

400
00:31:37,112-->00:31:44,113
Đó là điều mà bọn em đang điều tra.
Vì vậy anh đừng nghĩ tất cả 
đều là lỗi của anh.


400
00:31:45,112-->00:31:48,113
Ko... Kotori? Em khỏe chưa?

400
00:31:49,112-->00:31:54,113
Đây không phải lúc mà em 
nằm trên giường mãi, đúng không?

400
00:31:55,112-->00:32:09,513
Mặc dù đi cùng anh đã lãng phí thời gian, 
nhưng vì sự ổn định của các Tinh Linh
nên em mới đi theo. Hay là anh muốn chết?

400
00:32:10,512-->00:32:13,113
Grrr ...! Ừ... anh xin lỗi...

400
00:32:14,112-->00:32:20,113
Mà... dù sao thì buổi đi chơi cũng vui...

400
00:32:21,112-->00:32:25,113
Hở, chứ không phải em đã sợ đến ngất xỉu à...

400
00:32:26,112-->00:32:33,113
Dù sao thì anh vẫn phải nhớ là luôn
làm cho tinh thần của các Tinh Linh ổn định.

400
00:32:34,112-->00:32:37,113
Ừ... anh hiểu ... Anh sẽ cố gắng.

400
00:32:38,112-->00:32:43,113
Được rồi, Reine, chúng tôi về nhà đây.

400
00:32:40,512-->00:32:53,113
Được rồi. Cũng tối rồi.
Tôi sẽ đưa 2 cậu đến gần nhà.

400
00:32:59,112-->00:33:01,113
Aaaa... Mình mệt quá...

400
00:33:02,112-->00:33:06,113
Hôm nay đã có rất nhiều rắc rối. Nhưng nếu 
tôi nghĩ về những chuyện kỳ lạ, tôi sẽ 
không thể ngủ ngon. Tốt hơn là quên đi.

400
00:33:07,112-->00:33:11,113
Nhưng ... một kết giới trong thành phố Tengu à?
Chuyện gì đang xảy ra mới được chứ?

400
00:33:12,112-->00:33:16,113
Nghĩ nhiều cũng chẳng được gì. 
Điều duy nhất mình có thể làm hiện giờ
là khiến Tohka và mọi người vui vẻ.

400
00:33:17,112-->00:33:20,113
Chắc chắn sẽ còn nhiều vấn đề đấy...

400
00:33:23,112-->00:33:24,113
Aaaa...

400
00:33:25,112-->00:33:29,113
Mình nhớ ra rồi. Sáng mai có buổi hội thao.
Tại sao nó lại tổ chức trước giờ kiểm tra?
Đùa nhau à?

400
00:33:30,112-->00:33:40,113
Được rồi. Mình đi ngủ thôi...

400
00:33:42,112-->00:33:45,113
Mình nghĩ rằng tôi nghe thấy cái gì đó.
Chẳc mình nghe nhầm...

400
00:33:53,112-->00:33:56,113
Tại sao?

400
00:33:57,112-->00:34:09,113
Tại sao trí nhớ... vẫn còn?
Chẳng lẽ việc khởi động lại không hoàn chỉnh...

400
00:34:10,112-->00:34:18,113
Sức mạnh của Thiên Đường Eden trở nên không ổn định...
Tại sao? Tại sao nó đã xảy ra?

400
00:34:19,112-->00:34:23,113
Vì vậy, tôi không thể bảo vệ...

400
00:34:24,112-->00:34:29,113
Tôi phải tránh... chuyện đó xảy ra...

400
00:34:30,112-->00:34:32,113
Cô... cô là ai?

400
00:34:38,112-->00:34:43,113
Ngày 27 tháng 6

400
00:34:46,112-->00:34:49,113
Shidou...