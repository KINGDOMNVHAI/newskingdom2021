﻿0
00:00:03,112-->00:00:08,113
Shidou... dậy đi nào...

1
00:00:12,112-->00:00:15,113
Shidou, cậu dậy chưa?

2
00:00:17,112-->00:00:19,113
Rinne đấy à?

3
00:00:20,112-->00:00:22,113
Chào buổi sáng, Shidou.

4
00:00:26,112-->00:00:33,113
Sao vậy Shidou? Sao nhìn tớ chằm chằm vậy?

5
00:00:34,112-->00:00:37,113
À, không... Không có gì. Chỉ là...

6
00:00:38,112-->00:00:46,113
Cậu vẫn còn buồn ngủ à? 
Nhanh dậy rửa mặt và ăn sáng đi.

7
00:00:47,112-->00:00:49,113
Ừ...

8
00:00:56,112-->00:00:59,113
Dù cố gắng quên đi nhưng tôi không thể
không nghĩ đến buổi nói chuyện với Kotori tối qua.

9
00:01:00,112-->00:01:02,113
Khi tôi suy nghĩ, tôi đã
liên tưởng đến Rinne...

13
00:01:03,112-->00:01:05,113
Sẽ không ngạc nhiên khi tôi 
cảm thấy có chuyện gì đó tồi tệ...

14
00:01:11,112-->00:01:14,113
Chuyện đó có liên quan gì đến
những hành động kỳ lạ của Rinne không?

15
00:01:15,112-->00:01:19,113
Shidou? Cậu xong chưa?

16
00:01:20,112-->00:01:23,113
Được rồi! Tớ ra ngay đây!

17
00:01:28,112-->00:01:30,113
Xin lỗi vì để cậu đợi lâu, Rinne!

18
00:01:31,112-->00:01:33,113
Được rồi, không sao đâu.

19
00:01:34,112-->00:01:36,113
Ừ, chúng ta đi thôi.

20
00:01:37,112-->00:01:39,113
Ừ.

21
00:01:58,112-->00:02:01,113
Tôi nên nói chuyện gì bây giờ?
Đầu tôi hoàn toàn trống rỗng!

25
00:02:02,112-->00:02:04,113
Ummm...

25
00:02:05,112-->00:02:07,113
Này Shidou...

25
00:02:08,112-->00:02:11,113
Gì... gì cơ?

25
00:02:12,112-->00:02:15,113
Sau giờ học cậu vẫn rảnh chứ?

25
00:02:16,112-->00:02:19,113
Ừ, tớ rảnh. Nếu cậu muốn hẹn hò
thì tớ có thể đi được.

25
00:02:20,112-->00:02:25,113
Cảm ơn Shidou. Vậy chiều nay gặp nhé?

25
00:02:26,112-->00:02:28,113
Được rồi!

25
00:02:36,112-->00:02:38,113
Hết giờ học rồi à?

25
00:02:39,112-->00:02:42,113
Tôi cảm thấy thời gian hôm nay
trôi quá nhanh. Cứ như chỉ trong chớp mắt.

25
00:02:43,112-->00:02:46,113
Shidou? Cậu vẫn chưa sẵn sàng để đi à?

25
00:02:47,112-->00:02:50,113
Không... Ri...Rinne?

25
00:02:51,112-->00:02:55,113
Thôi nào, thôi nào, thời gian không đợi
chúng ta phải không? Nhanh lên, nhanh lên nào!

25
00:02:56,112-->00:03:00,113
Đợi chút, Rinne! 
Cậu không cần phải vội như vậy!

26
00:03:01,112-->00:03:08,113
Tớ vừa nói rồi đấy. Thời gian 
không đợi đâu. Làm nhanh lên rồi đi nào.

26
00:03:09,112-->00:03:12,113
Được rồi.

26
00:03:18,112-->00:03:20,113
Này Rinne?

26
00:03:24,112-->00:03:26,113
Um... Cuộc hẹn ngày hôm qua...
Tớ xin lỗi vì mọi thứ.

26
00:03:27,112-->00:03:34,113
Không, tớ mới là người sai. 
Tớ xin lỗi.

26
00:03:35,112-->00:03:39,113
Không, thật sự là tớ đã không thể 
tập trung vào cuộc hẹn. Tớ đã bị 
phân tâm suy nghĩ về những thứ khác...

26
00:03:40,112-->00:03:46,113
Nếu cậu nói vậy thì được rồi!
Coi như chúng ta hòa nhé. Được không?

26
00:03:47,112-->00:03:50,113
Cậu nói đúng. Chúng ta hòa. Hahaha...

26
00:04:01,112-->00:04:04,113
Bây giờ tớ nghĩ. Hồi trước tớ cũng đã 
từng đến công viên này chơi với Kotori.

38
00:04:05,112-->00:04:15,113
Đúng vậy. Các cậu xích đu, bập bênh
hay cầu tuột phải không?

39
00:04:16,112-->00:04:20,113
Ừ. Con bé nói là nguy hiểm và
lo lắng cho chúng ta. Chúng ta
đã không chơi nữa.

40
00:04:21,213-->00:04:23,113
Ừ...

41
00:04:24,113-->00:04:27,113
Hồi đó nghe thật phiền phức, nhưng bây giờ
đó là những kỷ niệm rất quý giá.

42
00:04:28,113-->00:04:35,113
Cậu nói đúng. Khi cậu lớn lên,
có những thứ không còn lấy lại được.

43
00:04:36,112-->00:04:39,113
Đúng vậy. Nhưng mà... hồi đó 
cậu thích nhất trò gì nhỉ?

39
00:04:43,112-->00:04:46,113
Tớ đã cố gắng nhớ lại
những gì cậu thích ở công viên này.

40
00:04:47,213-->00:04:54,113
Chuyện đó thì... Thôi nào, 
cậu phải nhớ chứ, phải không? 
Kỷ niệm của chúng ta mà.

41
00:04:55,213-->00:04:58,113
Hở? Um... Tớ không nhớ.

42
00:04:59,213-->00:05:03,513
Có thể đã quá trễ rồi.

44
00:05:04,513-->00:05:06,113
Hở?

45
00:05:07,113-->00:05:16,113
Không có gì ... Shidou, cậu hay quên thật.
Dù sao thì quá khứ cũng là quá khứ rồi.

46
00:05:17,112-->00:05:21,113
Ừ, xin lỗi cậu. Tớ là một kẻ rất tệ, 
không xứng đáng làm bạn thuở nhỏ của cậu.

47
00:05:22,112-->00:05:28,113
Không phải vậy đâu.
Không ai nhớ được hết mọi thứ cả.

48
00:05:29,112-->00:05:32,113
Ừ, cậu nói đúng...

49
00:05:33,112-->00:05:37,113
Hãy nhìn xem, cậu cũng đã quên những thứ
liên quan đến mọi người phải không?

50
00:05:38,112-->00:05:42,113
Tớ chẳng làm gì được. Nó giống như 
biến mất một cách vô thức.

51
00:05:42,112-->00:05:49,113
Được rồi, được rồi. Chắc chắn 
cậu đang hứng thú với điều gì khác rồi...

52
00:05:50,112-->00:05:52,113
Thật không?

53
00:05:53,112-->00:05:57,113
Ừ. Cảm ơn cậu...

54
00:05:58,112-->00:06:00,113
Hở?

55
00:06:01,113-->00:06:09,113
Không có gì. Hôm nay có một nơi tớ muốn đi.
Cậu đi cùng tớ chứ, Shidou?

41
00:06:10,112-->00:06:12,113
Ừ... Dĩ nhiên rồi!

42
00:06:19,112-->00:06:21,113
Chỗ mà cậu muốn đi... là chỗ này à?

43
00:06:25,112-->00:06:30,113
Tớ không có ý gì đâu.
Haha... Không khí yên tĩnh này khiến
tớ cảm thấy yên bình hơn đấy.

44
00:06:34,112-->00:06:36,113
Rinne?

45
00:06:37,112-->00:06:41,113
Này Shidou. Tớ có thể hỏi cậu điều này không?

46
00:06:42,112-->00:06:44,113
Hở? Gì vậy??

47
00:06:45,113-->00:06:55,113
Ở công viên, cậu có nói
"Phân tâm suy nghĩ về những thứ khác".
Là chuyện gì vậy?

48
00:06:56,113-->00:06:58,113
À... Về chuyện đó...

49
00:06:59,112-->00:07:04,113
Tớ nghĩ có lẽ tớ đã làm sai điều gì đó...

61
00:07:05,112-->00:07:08,113
À, không... Sự thật là khi
hẹn hò với cậu, tớ đã nhớ ra điều gì đó...

62
00:07:14,112-->00:07:19,113
"Nhớ ra điều gì đó"... là gì vậy?

63
00:07:20,112-->00:07:25,113
Lúc đầu, tớ nghĩ đó chỉ là những nơi 
quen thuộc... Nhưng dường như chúng ta 
đã lặp đi lặp lại. Tớ đã có những kỷ niệm
dường như đã xảy ra trước đó.

64
00:07:26,112-->00:07:28,113
"Xảy ra trước đó sao"?

65
00:07:29,112-->00:07:33,113
Vâng, tớ biết là rất kỳ lạ.
Nhưng, cậu biết không? Có một cái gì đó 
đã làm phiền tớ trong suốt những ngày qua.

66
00:07:34,112-->00:07:37,113
Vậy điều đó... là...

67
00:07:38,112-->00:07:42,113
Xin lỗi, có lẽ tớ đã hơi thiếu tôn trọng
cậu, Rinne. Đúng là vậy! Hôm nay tớ lúc nào
cũng xin lỗi cả, phải không? Haha ...

68
00:07:43,112-->00:07:46,113
Này Shidou...

69
00:07:49,112-->00:07:55,513
Khi cậu nhận ra rằng cậu đã làm sai, 
cậu có nghĩ rằng sẽ có kết quả tốt không?

70
00:07:56,512-->00:07:59,113
Hở? Cậu đang nói về cái gì vậy?

71
00:08:00,112-->00:08:12,113
Cậu phải cố gắng để chung sống 
với mọi người. Nếu cậu làm sai, 
khoảng cách với họ sẽ dần lớn hơn.

82
00:08:15,112-->00:08:20,113
Vì vậy... Tớ nghĩ chỉ còn một cách.

83
00:08:21,112-->00:08:29,113
Tớ muốn bắt đầu tất cả mọi thứ từ đầu.

84
00:08:30,112-->00:08:32,113
Hở... Rinne?

85
00:08:33,112-->00:08:39,113
À, xin lỗi. Hãy quên đi... Không có gì đâu.

86
00:08:40,112-->00:08:42,113
Vậy à?

87
00:08:43,112-->00:08:46,113
Tôi cảm giác có vẻ khá nghiêm trọng...

88
00:08:46,112-->00:08:50,113
Shidou, cậu đợi tớ một chút được không?

89
00:08:51,112-->00:08:53,113
Sao phải đợi? Cậu đi đâu à?

90
00:08:55,112-->00:09:01,113
Bí-mật. Đừng có tò mò bí mật
của con gái như thế.

99
00:09:02,112-->00:09:04,113
Xin lỗi...

100
00:09:05,112-->00:09:09,113
Cậu lại nói xin lỗi nữa kìa.

102
00:09:13,112-->00:09:18,113
Xin lỗi nhé. Cậu sẽ phải tự chăm sóc
bản thân mình đấy.

103
00:09:19,112-->00:09:22,113
Ừ, được rồi. Tớ sẽ cẩn thận...

104
00:09:23,112-->00:09:25,113
Haha, ngay cả Rinne cũng hay nói
xin lỗi mà, phải không?

105
00:09:26,112-->00:09:29,113
Hở? Đợi chút đã.
Tại sao Rinne lại xin lỗi?

107
00:09:34,112-->00:09:37,113
Rinne, cậu đi đâu rồi...?

108
00:09:38,512-->00:09:41,113
Ch... chuyện gì vậy?

109
00:09:44,112-->00:09:46,113
Ch... chuyện gì đang xảy ra vậy?

110
00:09:53,112-->00:09:56,113
Sức mạnh khổng lồ này... là gì?

111
00:09:57,112-->00:10:00,113
Cô là... người trong giấc mơ... của tôi?

113
00:10:01,112-->00:10:07,313
Đúng vậy... tôi và cậu đã có giấc mơ cùng nhau...

114
00:10:08,112-->00:10:12,113
Nhưng... cô là ai? Tôi... tôi đang tỉnh hay mơ?

1
00:10:16,112-->00:10:18,113
Tại sao cô lại xuất hiện trong giấc mơ của tôi?

1
00:10:19,912-->00:10:29,113
Cậu không cần phải biết điều đó.
Chỉ một lúc nữa, cậu sẽ quên tất cả.

1
00:10:30,112-->00:10:32,113
Này, đợi đã! Dừng lại...!

1
00:10:33,112-->00:10:46,113
Tạm biệt, Itsuka Shidou. Hãy quay lại 
những ngày bình thường. Và sau đó, chắc chắn
giấc mơ tiếp theo của cậu là một giấc mơ đẹp...

1
00:10:51,112-->00:10:54,113
Một luồng ánh sáng lớn lao thẳng về phía tôi.

1
00:10:55,112-->00:10:57,113
Tôi không thể tránh né nó. 
Tất cả kết thúc như thế này sao?


368
00:10:58,112-->00:11:01,113
Khi tôi cảm thấy cái chết đến gần,
những gì xuất hiện trong tâm trí tôi là...

368
00:11:01,512-->00:11:05,113
1) Cuộc hẹn của tôi với Rinne
2) Các cuộc hẹn với tất cả các cô gái.