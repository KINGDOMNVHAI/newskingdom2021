﻿0
00:00:02,112-->00:00:05,113
Shidou... dậy đi nào...

1
00:00:07,512-->00:00:09,113
Rinne đấy à?

2
00:00:10,112-->00:00:12,113
Chào buổi sáng, Shidou.

3
00:00:13,112-->00:00:17,113
Ừ, chào buổi sáng, Rinne... Có gì mới không?

4
00:00:18,112-->00:00:25,113
"Có gì mới không" là sao? 
Tớ không nhảy samba trên người cậu đâu.
Cậu vẫn còn ngái ngủ?

5
00:00:26,112-->00:00:30,113
À, không... Không phải thế...
Chỉ là gần đây tớ hay mơ thấy những thứ kỳ lạ.

6
00:00:31,112-->00:00:41,113
Vậy à... Cậu thay đồ rồi xuống nhanh đi.
Bữa sáng đã xong rồi đấy.

7
00:00:42,112-->00:00:44,113
Ờ... ờ...

8
00:00:51,112-->00:00:53,113
Sao tớ chỉ có một mình? Những người khác đâu?

9
00:00:54,112-->00:01:00,113
Hôm nay, tất cả bọn họ đều có việc  
để làm. Họ đi hết cả rồi.

10
00:01:01,112-->00:01:03,113
Hở? Lại nữa sao?

11
00:01:04,112-->00:01:07,113
Tất cả họ đều đi... có nghĩa là 
fraxinus có thể đang kiểm tra y tế 
hoặc một cái gì đó.

14
00:01:08,112-->00:01:12,113
Súp miso đây. Vẫn còn nóng đấy.

15
00:01:13,112-->00:01:16,113
Ồ, cảm ơn nhé... Cảm ơn vì bữa ăn.

16
00:01:17,112-->00:01:20,113
Không có gì.

17
00:01:21,112-->00:01:24,113
Tất cả họ đều có việc để làm...
Tôi tự hỏi mình nên làm gì?

18
00:01:25,112-->00:01:28,113
Chẳng lẽ Tohka và các cô gái khác
bị mất kiểm soát và đưa họ đến Fraxinus?
Mình mong không có gì xảy ra ...

19
00:01:34,112-->00:01:36,113
Chúng ta đi thôi.

20
00:01:37,112-->00:01:39,113
Được rồi.

21
00:01:40,112-->00:01:44,113
Không có ai ở nhà nên cảm giác yên ắng quá.

22
00:01:45,112-->00:01:51,113
Không sao cả phải không?
Tớ và cậu có thêm thời gian với nhau.

23
00:01:52,112-->00:01:54,113
Thật vậy sao?

24
00:01:55,112-->00:01:57,113
Thật vậy đấy!

25
00:02:03,112-->00:02:05,113
Này Shidou.

25
00:02:06,112-->00:02:09,113
Gì vậy?

25
00:02:10,112-->00:02:13,113
Sau giờ học hôm nay cậu có rảnh không?

25
00:02:14,112-->00:02:16,113
À, có.

25
00:02:17,112-->00:02:21,113
Vậy chúng ta có thể cùng đi chơi được không?

25
00:02:22,112-->00:02:24,113
Ừ, tất nhiên rồi.

25
00:02:25,112-->00:02:29,113
Thật không? Họ sẽ không giận chứ?

25
00:02:30,112-->00:02:33,113
Tớ không có lý do gì để từ chối cả.
Mà ai sẽ giận chứ?

25
00:02:33,112-->00:02:37,113
Tohka-chan hay Kotori-chan chẳng hạn.

25
00:02:38,112-->00:02:41,113
À, không... tớ sẽ nói với họ sau.

25
00:02:42,112-->00:02:49,113
Nếu vậy thì tớ rất vui.
Sự thật là tớ hơi lo một chút.

25
00:02:50,112-->00:02:52,113
Cậu lo lắng vì chuyện gì?

25
00:02:53,112-->00:02:59,113
Thì là vì cuộc hẹn hôm qua... 
tớ đã ngất đi và phá hỏng cuộc hẹn...

25
00:03:00,112-->00:03:05,113
Tớ đã nói với cậu không phải lo lắng 
về chuyện đó! Bạn không thể ngừng nghĩ về nó sao? 
Rõ ràng, cậu phải ưu tiên cho sức khỏe.

25
00:03:06,112-->00:03:14,113
Cậu lại lo cho tớ nữa rồi.
Tôi muốn cuộc hẹn hôm nay
vui vẻ hơn so với ngày hôm qua.

26
00:03:15,112-->00:03:19,113
Được rồi. Nhưng sẽ không có chuyện gì hết 
được không? Nếu có chuyện gì thì cậu phải
về nghỉ ngơi ngay đấy.

26
00:03:20,112-->00:03:25,113
Ừ, tớ biết rồi. Cậu hay lo lắng quá đấy, Shidou.

26
00:03:26,112-->00:03:29,113
Không tốt sao? Khi nhìn cậu buồn, 
tớ cũng buồn theo.

26
00:03:35,112-->00:03:37,113
À, không... Tại vì... tớ là 
bạn thuở nhỏ của cậu mà...

26
00:03:38,113-->00:03:44,113
Ừ, tớ biết. Tớ không nghĩ gì đâu.

37
00:03:58,112-->00:04:00,113
Phù... Cuối cùng cũng xong buổi học.

38
00:04:01,112-->00:04:03,513
Có lẽ vì lý do kỳ thi nên
có vẻ tất cả mọi người đều căng thẳng.

39
00:04:04,512-->00:04:08,113
Mặc dù có một mà có vẻ là ... Oh, và nói về Rome ...
Nó có vẻ là một mối phiền toái. Best tôi giấu.

Aunque hay uno que parece no estarlo... Ah, y hablando de Roma...
Parece que va a ser una molestia. Mejor me escondo.

40
00:04:10,113-->00:04:18,113
Itsuka? Ông đã đi đâu thế?
Cả buổi học tôi không thấy ông đâu cả.

41
00:04:19,113-->00:04:29,113
Tên khốn... mỗi ngày ông đi gặp
với một cô gái xinh đẹp? Thậm chí tôi còn 
chẳng được đi với bạn gái tôi. Chết tiệt!

42
00:04:30,113-->00:04:34,113
Đây là thời điểm để tôi hỏi hắn
"Vị hôn phu của ông là ai?", 
Nhưng chắc chắn hắn ta sẽ khó chịu hơn.

43
00:04:35,112-->00:04:46,113
Chúa ơi! Tại sao chỉ cho hắn mỗi ngày là một 
thiên đường? Hãy cho con người yêu! Làm ơn!

39
00:04:47,112-->00:04:50,113
TÔI NGUYỀN RỦA ÔNG!

40
00:04:53,113-->00:04:55,113
Là sao?

41
00:04:56,113-->00:04:59,513
Shidou? Có chuyện gì vậy?

42
00:05:00,513-->00:05:03,113
Waaa? Ri... Rinne? Cậu đừng 
làm tớ giật mình như thế!

45
00:05:04,112-->00:05:07,113
Cậu đang trốn ai à?

46
00:05:08,112-->00:05:11,113
Ừ... nếu Tonomachi lôi kéo tớ,
Tớ cảm thấy tớ sẽ gặp thêm rắc rồi.

47
00:05:12,112-->00:05:19,113
Cậu nói cứ như mọi chuyện kinh khủng lắm vậy.

48
00:05:20,112-->00:05:23,113
Vậy à? Mà hôm nay chúng ta 
làm gì đây? Cậu muốn đi đâu?

49
00:05:24,112-->00:05:27,113
Vậy cậu muốn đi đâu Shidou?

50
00:05:28,112-->00:05:31,113
Hở? Vậy cũng được... Cũng chẳng có
gì lớn cả... Nhưng mà...

51
00:05:32,112-->00:05:36,113
Tớ sẽ đi theo cậu, Shidou.

52
00:05:37,112-->00:05:39,113
Tớ hiểu rồi. Vậy chúng ta đi nhé?

53
00:05:40,112-->00:05:42,113
Ừ!

54
00:05:50,112-->00:05:54,113
tôi tự nói với bản thân mình, đầu tiên 
tôi muốn cô ấy mua vài bộ quần áo để 
phù hợp với một buổi hẹn hò...

55
00:05:55,112-->00:05:58,113
Giờ tớ thấy hơi đói rồi. 
Cậu có muốn ăn không, Rinne?

56
00:05:59,112-->00:06:03,113
Um... Cậu muốn ăn ở đâu, Shidou?

41
00:06:04,112-->00:06:09,113
Để xem nào... Có lẽ là bánh Kinako.
Món này có vị ngọt rất tinh tế. Tohka rất thích!

42
00:06:10,112-->00:06:13,113
A, Tohka-chan?

43
00:06:14,112-->00:06:16,113
Sao cơ?

44
00:06:17,112-->00:06:21,113
Không, tớ thấy cũng được đấy.

45
00:06:22,112-->00:06:25,113
Cũng có nhiều món khác đấy.
Cậu có chắc muốn ăn món Kinako này không?

46
00:06:26,112-->00:06:29,113
Được rồi, được rồi! Cứ đi đi!

47
00:06:30,113-->00:06:32,113
Ờ... ờ...

48
00:06:36,112-->00:06:39,113
Xin chào quý khách!

49
00:06:40,112-->00:06:43,113
Cho 2 cái bánh Kinako.

50
00:06:44,112-->00:06:48,113
Chúng tôi xin lỗi. Hôm nay chúng tôi bán hết rồi.

51
00:06:49,112-->00:06:51,113
À, vậy à...

52
00:06:52,112-->00:06:57,113
Anh có thể chọn món bánh khác trong
thực đơn của chúng tôi không?

53
00:06:58,112-->00:07:02,113
Có những món mới ra lò đấy ạ.

61
00:07:03,112-->00:07:05,113
Ồ, nghe được đấy. Cậu thấy sao, Rinne?

62
00:07:07,112-->00:07:11,113
Nếu cậu muốn thử thì tớ cũng muốn thử.

63
00:07:12,112-->00:07:14,113
Được rồi. Cho tôi 2 cái.

64
00:07:15,112-->00:07:18,113
Vâng. Cảm ơn quý khách.

65
00:07:22,112-->00:07:25,113
Xin lỗi nhé, Kinako hết rồi.

66
00:07:26,112-->00:07:30,113
Ừ, nhưng cái này cũng ngon mà phải không?

67
00:07:31,112-->00:07:33,513
Cậu ăn thử đi.

70
00:07:34,512-->00:07:38,113
Mấy cái bánh này mới từ trong lò ra
nên chắc chắn phải ngon rồi.

71
00:07:39,112-->00:07:41,113
Để ăn thử xem...

72
00:07:42,112-->00:07:45,113
Tớ cũng ăn đây.

73
00:07:46,112-->00:07:50,113
Ngon thật! Bánh mới ra lò và kem rất ngọt!

74
00:07:51,112-->00:07:57,113
Quả thật rất ngon. Đúng không Shidou?

75
00:07:58,112-->00:07:59,513
Ừ.

76
00:08:00,513-->00:08:04,113
Tôi cảm thấy... Tôi đã ăn cái bánh này 
với ai đó trước kia...

81
00:08:11,112-->00:08:14,113
Chuyện gì thế Shidou?

82
00:08:15,112-->00:08:18,113
Không, không có gì. Tớ đang nghĩ
tớ quyết định khi chọn cái bánh này.

83
00:08:19,112-->00:08:23,113
Đúng vậy. Cái này chắc ngon không kém
Kinako phải không?

84
00:08:24,112-->00:08:26,113
Ừ...

85
00:08:27,113-->00:08:29,113
Vậy chúng ta đi đâu tiếp đây?

86
00:08:30,112-->00:08:35,113
Cậu muốn đi đâu Shidou?

87
00:08:36,112-->00:08:39,113
Suy nghĩ đã... Nếu đúng theo kế hoạch
thì chúng ta đi ra trung tâm trò chơi đi.

88
00:08:40,112-->00:08:44,113
Vậy chúng ta đến trung tâm trò chơi đi!

89
00:08:45,112-->00:08:47,113
Ờ... đi thôi.

90
00:08:48,112-->00:08:50,113
Ừ.

91
00:08:55,112-->00:09:00,113
Sau khi đi qua nhiều con phố dưới ánh nắng,
mồ hôi chúng tôi đã biến mất khi vào trong
phòng máy lạnh của trung tâm trò chơi.

92
00:09:01,112-->00:09:04,113
Tuyệt thật đấy, phải không?

99
00:09:05,112-->00:09:08,113
Ừ. Trung tâm trò chơi là chỗ 
tránh nóng mùa hè rất tuyệt vời.

100
00:09:09,112-->00:09:14,113
Cậu nói đúng. Ở trong này mát thật đấy.

101
00:09:15,112-->00:09:18,113
Mặc dù vậy, khi cậu chơi thua,
có khi cậu sẽ nóng trở lại đấy.

102
00:09:19,112-->00:09:22,113
Vậy à?

103
00:09:23,112-->00:09:26,113
Hở? Cậu đã bao giờ đi chơi ở đây chưa?
Tớ không nhớ nữa.

104
00:09:27,112-->00:09:32,113
À, không... Đây là lần đầu tiên đấy.

105
00:09:33,112-->00:09:35,113
Vậy à... Nhưng sao tớ không biết nhỉ?

106
00:09:36,112-->00:09:40,113
Tôi đột nhiên nghĩ, có vẻ như gần đây
tôi cũng đã đưa ai đó lần đầu
đến trung tâm trò chơi...

107
00:09:41,112-->00:09:44,113
Shidou, chuyện gì vậy?

108
00:09:45,112-->00:09:49,113
Hở, không, không có gì.
Quan trọng hơn là cậu thích chơi trò gì?

109
00:09:50,112-->00:09:59,113
Vì đây là lần đầu tiên tớ đến đây 
nên không biết nhiều... Cậu thích chơi
trò gì, Shidou?

113
00:10:00,112-->00:10:03,113
Tớ à? Thường thì tớ sẽ chơi trò gắp thú.

114
00:10:04,112-->00:10:10,113
Là các con thú nhồi bông trong cái hộp đó à?

1
00:10:11,112-->00:10:14,113
Đúng đó. Cậu muốn thử không?

1
00:10:15,112-->00:10:20,113
Không. Tớ xem cậu chơi cũng được.

1
00:10:21,112-->00:10:25,113
Nhưng vì đây là lần đầu tiên cậu đến 
trung tâm trò chơi thì cậu phải chơi cho vui. 
Thắng hay thua đâu có quan trọng.

1
00:10:26,112-->00:10:32,113
Được rồi, cậu không cần lo cho tớ đâu.
Mà cậu muốn lấy con thú bông nào?

1
00:10:33,112-->00:10:36,113
Hở? À, xem nào... Um, con đằng kia nhé?

1
00:10:37,112-->00:10:44,113
Con đó à? Trông dễ thương đấy. 
Được rồi, tớ sẽ cổ vũ cậu!

368
00:10:45,112-->00:10:46,513
Ờ... ờ...

368
00:10:47,512-->00:10:53,113
Sự thật là tôi muốn chọn thứ mà 
các cô gái thích. Rất ít khi tôi lại 
chọn thứ mình thích khi đi hẹn hò cả.

368
00:10:54,112-->00:10:56,113
Sẽ thật tuyệt nếu cậu có được nó, phải không?

368
00:10:57,112-->00:11:00,113
Ờ, được rồi. Tớ sẽ cố gắng!

368
00:11:06,112-->00:11:11,113
Ồ! Gần chút nữa, chút nữa!

368
00:10:12,112-->00:11:14,113
Ừ, một chút nữa!

368
00:11:15,112-->00:11:18,113
Cố lên Shidou!

368
00:11:19,112-->00:11:21,113
Ừ... ừ...

368
00:11:25,112-->00:11:28,113
Được rồi, tôi đã có động lực.
Nếu tôi thắng, tôi sẽ có quà cho Rinne!

368
00:11:29,112-->00:11:31,113
A!

368
00:11:33,112-->00:11:37,113
Tiếc thật đấy.

368
00:11:38,112-->00:11:41,113
Hôm nay thế là đủ rồi...
Chúng ta đi thôi.

368
00:11:42,112-->00:11:45,113
Shi... Shidou? Cậu chắc chứ?

368
00:11:46,112-->00:11:48,113
Ừ, không sao đâu, không sao đâu.

368
00:11:54,112-->00:11:56,113
Xin lỗi nhé, Rinne.

368
00:11:57,112-->00:12:01,113
Hở? Tại sao cậu xin lỗi?

368
00:12:02,113-->00:12:06,113
Tớ định lấy con gấu Teddy đó làm quà cho cậu
nhưng tớ đã không làm được.

368
00:12:07,113-->00:12:12,113
Không sao đâu, miễn là 
chúng ta vui là được rồi.

368
00:12:13,112-->00:12:15,113
Ừ, cảm ơn cậu.

368
00:12:16,112-->00:12:21,113
Ừ. Giờ chúng ta đi đâu đây?

368
00:12:22,112-->00:12:26,113
Um... Chúng ta đã đi ăn, 
đã chơi game. Vậy bây giờ thì...

368
00:12:27,112-->00:12:29,113
Giờ thì sao?

368
00:12:30,112-->00:12:33,113
Còn cậu thì sao Rinne? Cậu muốn đi đâu?

368
00:12:34,112-->00:12:39,113
Vậy cậu đưa tớ đến nơi cậu thích đi Shidou.

368
00:12:40,112-->00:12:43,113
À... Ừ, tớ cũng có biết một chỗ, nhưng...

368
00:12:44,112-->00:12:48,113
Vậy thì tốt. Cứ đưa tớ tới đi.

368
00:12:49,112-->00:12:52,113
Ờ... Được rồi. Đi nào.

368
00:12:53,112-->00:12:55,113
Ừ!

368
00:13:00,112-->00:13:02,113
Ở đây này, Rinne.

368
00:13:04,112-->00:13:07,113
Tớ hiểu rồi. Là ở sườn núi này à?

368
00:13:08,112-->00:13:11,113
Đúng vậy. Nhìn từ đây,
phong cảnh thật tuyệt vời đúng không?

368
00:13:12,112-->00:13:16,113
Tớ cũng có thể xem sao?

368
00:13:17,112-->00:13:21,513
Haha, cậu không cần nghĩ nhiều thế đâu.
Nhưng đừng biến rất nhiều trên lan can. 
Nguy hiểm lắm đấy.

368
00:13:22,112-->00:13:25,113
Được rồi, tớ sẽ cẩn thận.

368
00:13:28,112-->00:13:36,113
Cảnh thật sự rất đẹp. Cứ như thể 
thành phố ngay bên cạnh tớ...

368
00:13:37,112-->00:13:40,113
Từ đây cậu có thể có nhìn toàn cảnh
thành phố Tengu. Tớ thích cảnh đẹp này.

368
00:13:41,112-->00:13:48,113
Ừ. So với khi chúng ta ở thành phố,
không khí lạnh hơn nhiều, cảm giác thật tuyệt vời.

368
00:13:49,112-->00:13:51,113
Này, Rinne?

368
00:13:54,112-->00:13:56,113
Gì vậy?

368
00:13:57,112-->00:13:59,113
Hôm nay là lần đầu tiên cậu nói với tớ.

368
00:14:00,212-->00:14:02,113
Cậu đang nói về cái gì vậy?

368
00:14:03,212-->00:14:05,113
Cậu cho tới biết những gì cậu thích...

368
00:14:06,113-->00:14:13,113
À... Tớ không nên nói sao?
Có lẽ tớ hơi ích kỷ ...

368
00:14:14,112-->00:14:16,113
Không, không phải thế. Ngược lại cơ.

368
00:14:19,112-->00:14:23,113
Tớ... tớ muốn cậu thưởng thức nhiều 
thứ hơn. Tớ nghĩ hôm nay chúng ta 
đã có một cuộc hẹn tốt.

368
00:14:27,112-->00:14:30,113
Chỉ là sau những trò chơi vừa rồi,
tớ thấy tớ chỉ làm những thứ tớ thích.
Cậu thấy không? Tớ muốn cậu làm những gì 
cậu thích để cậu vui hơn.

368
00:14:32,112-->00:14:35,113
Tại sao lại không được?

368
00:14:36,112-->00:14:37,513
Hở?

368
00:14:38,512-->00:14:44,113
Tại sao chúng ta không thể làm 
những gì cậu thích hả Shidou?

368
00:14:45,112-->00:14:47,113
Gần đây tớ đã nói với cậu, tớ muốn cậu và tớ...

368
00:14:48,113-->00:14:57,113
Những gì cậu cảm thấy vui, đối với tớ
đều rất quan trọng! Tớ hài lòng với
việc đi cùng cậu và làm những gì cậu thích!

368
00:14:58,112-->00:15:00,113
Thật... thật vậy sao?

368
00:15:01,112-->00:15:15,113
Thật vậy đấy! Thật của thật đấy!
Tại sao cậu lại nói như thế?
Cậu không hiểu sao? Chúng ta là bạn thời thơ ấu.

368
00:15:16,112-->00:15:19,113
Không, tớ xin lỗi. Nhưng mà tớ 
không thể hiểu được.

368
00:15:25,112-->00:15:28,113
Hôm nay như vậy là đủ rồi... 
Chúng ta về nhà chứ?

368
00:15:29,512-->00:15:31,113
Ừ...

368
00:15:33,112-->00:15:35,113
Aaaa, tại sao lại thành ra thế này?

368
00:15:36,112-->00:15:39,113
Nhưng... như tôi đã nghĩ. Nếu Rinne và tôi có
một cuộc hẹn mà cả 2 đều tận hưởng
thì cả 2 đều sẽ rất vui...

368
00:15:40,112-->00:15:44,113
Và... tôi nghĩ Rinne đang muốn tôi
làm điều gì đó... Nhưng là việc gì
mà tôi không biết...

368
00:15:45,112-->00:15:48,113
Rinne, tại sao sự việc trở nên phức tạp như vậy?
Cậu từ chối hẹn hò 2 lần, mời cô gái khác 
hẹn hò với tớ. Sau đó lại chủ động mời tớ. 
Cậu bị ngất vì thiếu máu. Và giờ cậu muốn tớ
làm những gì tớ thích.

368
00:15:49,112-->00:15:52,113
Buổi hẹn hò hôm nay coi như cũng được...

368
00:15:58,112-->00:16:00,113
"Một bữa ăn mà giống như nhai cát" là cụm từ
được mô tả ngay bây giờ phải không?

368
00:16:01,112-->00:16:05,113
Các bữa ăn tối được Rinne nấu luôn ngon 
như mọi khi. Hôm nay tôi không thể cảm nhận 
được gì ngoài sự lo lắng của mình...

368
00:16:07,112-->00:16:09,113
Ai vậy?

368
00:16:11,512-->00:16:13,513
Shidou...

368
00:16:14,512-->00:16:17,113
Rinne đấy à?

368
00:16:18,112-->00:16:22,113
Hiện giờ cậu rảnh không?

368
00:16:23,112-->00:16:25,113
Sao vậy?

368
00:16:26,112-->00:16:34,113
Tớ xin lỗi. Vì chuyện ngày hôm nay 
và cả ngày hôm qua.

368
00:16:35,112-->00:16:37,113
Hở?

368
00:16:38,112-->00:16:45,113
Tớ hoàn toàn không biết gì về việc
hẹn hò với một đứa con trai cả.

368
00:16:46,112-->00:16:49,113
Không, đó không phải lỗi của cậu.
Tớ mới phải xin lỗi.

368
00:16:50,112-->00:16:53,113
Thật vậy sao?

368
00:16:54,112-->00:16:59,113
Ừ... Mọi thứ đang tốt đẹp cho đến khi
tớ phá hỏng nó. Lẽ ra tớ không nên nói thế.

368
00:17:00,112-->00:16:11,113
Này, Shidou. Ngày mai, chúng ta
tiếp tục hẹn hò chứ? Tớ muốn cho cậu thấy
tớ là một người đáng tin cậy.

368
00:17:12,112-->00:17:14,113
Ờ, tất nhiên là tớ sẽ đi!

368
00:17:15,112-->00:17:21,113
Ừ... Cảm ơn cậu.

368
00:17:22,112-->00:17:26,113
Hẹn gặp cậu vào ngày mai.
Chúc ngủ ngon, Shidou.

368
00:17:27,112-->00:17:29,113
Ờ. Chúc ngủ ngon.

368
00:17:32,112-->00:17:34,113
Haa... Tôi đã cảm thấy hơi sợ...

368
00:17:35,112-->00:17:38,113
Nhưng tôi vui. Tôi rất vui 
vì Rinne muốn hẹn hò nữa.

368
00:17:40,112-->00:17:42,113
Waa!

368
00:17:43,112-->00:17:45,113
Là em đây.

368
00:17:48,112-->00:17:51,113
Gì vậy? Em cũng đến à?

368
00:17:52,112-->00:17:55,113
"Cũng đến" nghĩa là sao?

368
00:17:56,112-->00:17:59,113
Không, không có gì. Quan trọng hơn,
em muốn nói gì vậy?

368
00:18:00,112-->00:18:04,113
Em có một trường hợp nên nói cho anh biết.

368
00:18:08,112-->00:18:12,113
Trong phân tích hôm nay, 
bọn em nhận thấy một cái gì đó kỳ lạ.

368
00:18:13,112-->00:18:15,113
Là gì vậy?

368
00:18:16,112-->00:18:25,113
Đó là về phản ứng của sóng Tinh Linh
chúng ta đang theo dõi... Hôm qua và 
hôm nay, trong một khoảng thời gian, 
nó không ổn định đáng kể. 

368
00:18:25,512-->00:18:30,113
Và các đường cong của sóng Tinh Linh 
cũng gần như bằng nhau.

368
00:18:31,112-->00:18:34,113
"Khoảng thời gian" mà em nói đến là gì?

368
00:18:35,112-->00:18:38,113
Một khoảng thời gian vào buổi chiều.

368
00:18:39,112-->00:18:40,513
Vậy à...

368
00:18:41,513-->00:18:51,113
Hiện nay, Reine phân tích chi tiết
dữ liệu sóng Tinh Linh, nhưng...
thật sự có một cái gì đó kỳ lạ.

368
00:18:52,112-->00:18:54,113
Rất lạ là sao?

368
00:18:55,112-->00:19:00,113
Nó giống như là... biểu đồ trạng thái 
tinh thần khi đang hẹn hò...

368
00:19:01,112-->00:19:03,113
Hở?

368
00:19:04,112-->00:19:09,113
Và hơn nữa, tâm trạng đó giống như
một buổi hẹn hò không thành công. 

368
00:19:13,112-->00:19:22,513
Rất kỳ lạ phải không? Em cảm thấy giống như
em đang theo dõi một buổi hẹn hò với Tinh Linh.

368
00:19:23,512-->00:19:26,113
Phải chăng có sự trùng hợp?

368
00:19:27,112-->00:19:32,113
Đừng so sánh với "người nào đó".
Em không phải là một đứa ngu ngốc đâu.

368
00:19:33,112-->00:19:36,113
"Người nào đó" là ai chứ?

368
00:19:37,112-->00:19:40,113
Một cái gì đó như thế. 
Em chỉ tự hỏi mình thôi.

368
00:19:41,112-->00:19:44,113
Em không đang nói anh đấy chứ?

368
00:19:45,112-->00:19:55,113
Anh nhận ra rồi à? Trong dữ liệu Fraxius  
cũng lưu các cuộc hẹn thất bại của anh
khi hẹn hò với Tinh Linh.

368
00:19:56,112-->00:20:00,113
Sau khi phân tích các dữ liệu từ 
các cuộc hẹn không thành công của anh
với Tinh Linh, 

368
00:20:00,512-->00:20:06,113
phân tích các mô hình sóng Tinh Linh 
của ngày hôm qua và hôm nay...
Bọn em thấy có một trùng hợp đến kỳ lạ.

368
00:20:07,112-->00:20:10,113
Hôm qua và hôm nay... 
dữ liệu về hẹn hò thất bại?

368
00:20:11,112-->00:20:15,113
Gì cơ? Anh có ý tưởng gì à?

368
00:20:16,112-->00:20:19,113
Không, không có gì...

368
00:20:20,113-->00:20:26,113
Dù sao... Bọn em sẽ tiếp tục
với việc phân tích tình hình. Do đó...

368
00:20:27,113-->00:20:30,113
Còn anh chỉ cần làm những gì 
cần làm thôi phải không?

368
00:20:31,112-->00:20:38,113
Phải. Miễn là anh hiểu và không có vấn đề
gì là được. Em đi ngủ đây. Chúc ngủ ngon.

368
00:20:39,112-->00:20:41,113
Ừ, chúc ngủ ngon.

368
00:20:44,112-->00:20:46,113
Hôm qua và hôm nay... 
Tôi chỉ đi hẹn hò với Rinne

368
00:20:47,112-->00:20:50,113
Và hơn thế nữa, không thể nói các cuộc hẹn 
đó thành công. Ngược lại, đã có nhiều vấn đề xảy ra...

368
00:20:51,112-->00:20:53,113
Chúng đều thất bại.

368
00:20:54,112-->00:20:58,113
Kotori nói đã cócác dữ liệu của các cuộc hẹn 
không thành công của tôi. Có nghĩa là nó có 
liên quan đến tôi phải không? 
Nếu vậy, có nghĩa là...

368
00:20:59,112-->00:21:02,113
Hahaha! Đó không phải là sự thật...
Không thể có chuyện đó được...

368
00:21:03,112-->00:21:06,113
Bởi vì cái suy nghĩ vừa lóe lên trong đầu
tôi rất vô lý. Chuyện đó không thể được phải không?

368
00:21:07,112-->00:21:10,113
Bạn thời thơ ấu của tôi... 
là một Tinh Linh sao...?

368
00:21:11,112-->00:21:14,113
Chúng tôi đã là bạn trong một thời gian dài, 
từ khi chúng tôi còn nhỏ. Nếu như thế 
thì lẽ ra tôi phải biết.

368
00:21:15,112-->00:21:20,113
Tôi không thể ngừng suy nghĩ về chuyện đó.
tôi càng quay cuồng với mọi thứ xung quanh...
Tôi nên ngủ thôi.

368
00:21:27,512-->00:21:41,113
Không thể được... Không thể được... 
Không thể được, không thể được, không thể được!
Cuối cùng cậu vẫn không thể ra quyết định sao?

368
00:21:42,112-->00:21:49,113
Nếu cứ thế này... Thiên đường Eden sẽ ...
Thiên đường của tôi sẽ...

368
00:21:50,102-->00:21:56,113
Tôi không còn lựa chọn nào khác...

368
00:21:57,112-->00:22:00,113
Tự tay tôi, với đôi bàn tay này...

368
00:22:07,112-->00:21:39,513
Ngày 30 tháng 6


