﻿0
00:00:05,112-->00:00:09,113
Shidou... dậy đi nào.

1
00:00:11,512-->00:00:13,113
Trời sáng rồi à?

2
00:00:14,112-->00:00:17,113
Cậu dậy chưa?

3
00:00:18,112-->00:00:21,113
Ừ... Rinne, chào buổi sáng.

4
00:00:22,112-->00:00:26,513
Ừ! Chào buổi sáng, Shidou! 
Hôm nay cũng là một ngày đẹp trời, phải không?

5
00:00:27,512-->00:00:31,113
Buổi sáng tốt lành. Nhưng tớ cảm thấy 
có gì đó kỳ lạ đã xảy ra.

6
00:00:32,112-->00:00:37,513
Hở? Vậy sao? Nhưng tớ không nghĩ như vậy.

7
00:00:38,512-->00:00:40,113
Thật không?

8
00:00:41,112-->00:00:49,113
Thật đấy. À, có chuyện này Shidou...
Sau giờ học cậu rảnh không?

9
00:00:50,112-->00:00:52,113
À, tớ rảnh...

10
00:00:53,112-->00:00:57,113
Nhưng nếu tôi có kế hoạch 
cho các cuộc hẹn thì không đâu.

11
00:00:57,112-->00:01:04,113
Vậy thì may quá. Bữa sáng đã xong rồi.
Cậu xuống nhanh nhé, được chứ?

12
00:01:05,112-->00:01:07,113
Ờ... ờ...

13
00:01:11,112-->00:01:14,113
Hở? Mọi người đâu cả rồi?

14
00:01:15,112-->00:01:28,113
À, phải rồi. Yoshino-chan cảm thấy 
bị bệnh đột ngột. Kotori-chan đưa em ấy 
đến bệnh viện vào sáng sớm. Tớ cũng muốn 
đi theo nhưng họ bảo tớ nên ở nhà với cậu.

15
00:01:29,112-->00:01:32,113
Yoshino đi bệnh viện sao? Tình hình sao rồi?

16
00:01:33,112-->00:01:38,113
Kotori-chan nói không có gì nghiêm trọng, nhưng...

17
00:01:39,112-->00:01:42,113
Nhưng vẫn còn đáng lo ngại, phải không?

18
00:01:42,512-->00:01:44,113
Phải...

19
00:01:45,112-->00:01:48,113
Nhiều khả năng là Kotori đưa cô bé
lên Fraxinus. Mọi thứ sẽ ổn thôi. Nhưng mà...

20
00:01:49,112-->00:01:52,113
Vậy còn Tohka? Cô ấy vẫn đang ngủ à?

21
00:01:53,112-->00:02:01,113
À không. Hôm nay Tohka 
đã đến trường trước rồi.

25
00:02:02,112-->00:02:05,113
Hở? Thật sao? Sao lại thế...!

25
00:02:05,512-->00:02:11,113
Tớ cũng ngạc nhiên khi thấy
cậu và Tohka-chan không đi cùng nhau đấy.

25
00:02:12,112-->00:02:14,113
À, không, ý tớ không phải thế.

25
00:02:15,112-->00:02:19,113
Có khi nào Yoshino và Tohka... 
Không, nếu vậy Kotori đã đưa Tohka đi rồi.

25
00:02:20,112-->00:02:24,113
À, cậu ngồi đi. Tớ sẽ dọn bữa sáng.

25
00:02:25,112-->00:02:27,113
À, được rồi. Cảm ơn cậu.

25
00:02:28,113-->00:02:31,113
Hôm nay không có ai cả.
Chỉ có chúng tôi ăn sáng cùng nhau.

25
00:02:34,112-->00:02:41,113
Để cậu đợi lâu. Tớ đã hâm nóng súp miso. 
Cơm nấu chín rồi đấy. Cậu thấy sao?

25
00:02:42,112-->00:02:45,113
Cảm ơn cậu. Itadakimasu.

25
00:02:46,112-->00:02:52,112
Xin lỗi nhé. Sáng nay khá bận rộn.
Tớ chỉ có thể chuẩn bị món salad 
và thịt xông khói thôi.

25
00:02:53,112-->00:02:56,113
Không, thế này là đủ rồi. Hơn nữa, nếu Yoshino
bị bệnh, vậy chắc đã có nhiều việc phải làm...

25
00:02:57,112-->00:03:00,113
Tôi cảm thấy rằng tôi là người 
phải xin lỗi, vì tôi chỉ ngủ.

25
00:03:01,112-->00:03:07,113
À, còn cái này. 
Tớ đã chuẩn bị cho cậu rồi.

26
00:03:08,112-->00:03:11,113
Ồ, bento à? Cảm ơn nhé.
Cậu luôn giúp đỡ tớ.

26
00:03:12,112-->00:03:15,113
Không có gì đâu.

26
00:03:16,112-->00:03:18,113
Ờ... ờ...

26
00:03:24,112-->00:03:27,113
Sao vậy?

26
00:03:28,112-->00:03:31,113
Không, cảm ơn vì bữa ăn...

26
00:03:32,112-->00:03:34,113
Cậu đi trước đi. Tớ chuẩn bị ngay đây.

26
00:03:34,512-->00:03:36,513
Ờ...

26
00:03:37,112-->00:03:43,113
Tôi nghĩ rằng sẽ tốt hơn nếu tôi làm bữa trưa 
cho cô ấy. Nhưng khi cô ấy làm cả hai, 
tôi chẳng có gì để làm cả.

26
00:03:44,112-->00:03:51,113
Sao vậy Shidou? Cậu vừa mới 
đỏ mặt kìa. Cậu lại sốt à?

26
00:03:52,112-->00:03:55,113
À, không, không có gì đâu!

26
00:03:56,112-->00:03:58,113
Thật không?

27
00:03:59,112-->00:04:01,113
Thật đấy.

33
00:04:02,112-->00:04:07,113
Shidou, cậu lạ lắm.

34
00:04:08,112-->00:04:10,112
Xin lỗi.

35
00:04:11,112-->00:04:17,113
Thôi nào, không có gì để xin lỗi cả.
Cậu đang rất kỳ lạ đấy.

36
00:04:25,112-->00:04:27,613
Này, đến giờ rồi đấy! 
Chúng ta đi thôi, Rinne!

37
00:04:28,112-->00:04:32,113
Được rồi! Tớ đến đây! Chờ một chút!

38
00:04:33,112-->00:04:37,113
Vẫn còn thời gian, không cần phải vội đâu!

39
00:04:38,112-->00:04:41,113
Vâng, xin lỗi vì sự chậm trễ. 
Chúng ta đi chứ?

40
00:04:42,113-->00:04:44,113
Ờ... ờ...

41
00:04:49,113-->00:04:51,113
Hở? Gì vậy Rinne? Nhanh lên nào!

42
00:04:52,213-->00:05:01,113
Đươc rồi. Đợi tớ khóa cửa đã... Hở?

43
00:05:02,112-->00:05:07,113
Sao bây giờ? Hình như tớ mất chìa khóa rồi.

39
00:05:08,113-->00:05:10,113
Hả?

47
00:05:11,112-->00:05:17,113
Làm sao đây? Đó là thứ rất quan trọng!

48
00:05:18,312-->00:05:22,113
Ri... Rinne! Từ từ nào! Có thể cậu vẫn
để nó ở trong nhà đấy!

49
00:05:23,112-->00:05:28,113
À! Phải rồi... cậu nói đúng!

50
00:05:36,112-->00:05:39,513
Cậu không thấy sao?

51
00:05:40,112-->00:05:50,113
Xin lỗi nhé Shidou... Tại tớ không cẩn thận... 
Đó là chìa khóa mà cậu đã làm cho tớ...

52
00:05:51,112-->00:05:53,113
Đừng lo lắng! Thật ra là tớ yên tâm rồi!

53
00:05:54,113-->00:06:03,113
Nghĩa là sao? Cậu đang vui 
vì tớ mất chìa khóa à?

41
00:06:04,112-->00:06:07,113
Haha, không phải thế. Vì tớ nghĩ 
ngay cả cậu cũng có thể mắc lỗi như vậy. 
Bởi vì cậu luôn cố gắng làm đúng mọi thứ 
bằng cách này hay cách khác.

42
00:06:08,112-->00:06:13,113
Cậu nghĩ là tớ không đáng tin cậy sao?

43
00:06:14,112-->00:06:20,113
Tất nhiên cậu đáng tin cậy! Cậu luôn
giúp đỡ tớ rất nhiều, Rinne! Vì vậy, 
Từ bây giờ tớ cũng muốn giúp cậu!

44
00:06:21,112-->00:06:24,113
Ừ, được rồi.

45
00:06:25,112-->00:06:29,113
Giờ cậu thử nhớ lại xem Rinne...
Sau khi trở về nhà, cậu đã làm gì?

51
00:06:30,113-->00:06:41,513
Vâng... làm bữa ăn sáng và cơm hộp...
ăn sáng với cậu. Sau đó rửa sạch mọi thứ... 
và sau đó...

52
00:06:42,512-->00:06:45,113
Sau đó sao?

53
00:06:46,112-->00:06:51,113
A! Đúng rồi! Shidou, tớ nhớ rồi!

54
00:06:52,112-->00:06:55,113
N... này, Rinne?

60
00:07:06,112-->00:07:11,113
Tớ thấy rồi Shidou!

61
00:07:11,112-->00:07:14,113
Ừ! Mừng quá! Nó ở đâu vậy?

63
00:07:16,112-->00:07:21,513
Nó ở trong bồn tắm.

64
00:07:22,112-->00:07:25,113
Vậy à. Dù sao, tớ rất mừng 
vì cậu đã được tìm thấy.

65
00:07:26,112-->00:07:32,513
Tớ luôn mang theo nó mọi nơi. Chắc là tớ
đã quên nó một lúc nào đó.

66
00:07:33,512-->00:07:36,113
À, tớ xin lỗi... Chắc là vì tớ giục cậu.

67
00:07:37,112-->00:07:49,113
Không, không phải là lỗi của cậu Shidou. 
Đó là lỗi của tớ. Vậy nên tớ hy vọng cậu bỏ qua
cho tớ. Tớ sẽ không phạm lỗi nữa!

71
00:07:50,112-->00:07:53,513
Cậu không cần phải nghiêm trọng như vậy.
Không phải ai đánh cắp nó. Mà cậu cũng 
đã tìm thấy rồi còn gì.

72
00:07:54,512-->00:07:58,113
Tớ không thể... như vậy...

73
00:07:59,112-->00:08:01,113
Hở?

74
00:08:02,113-->00:08:15,113
Đây là bằng chứng cho thấy cậu...
Cậu đã tin tưởng tớ... coi tớ như một người
trong gia đình... coi tớ như một người 
quan trọng phải không? 

74
00:08:15,513-->00:08:22,113
Do đó, tớ sẽ không tha thứ 
cho bản thân mình. Tớ đang ích kỷ quá sao?

82
00:08:23,112-->00:08:26,113
Không phải đâu. Đó là thứ quan trọng
đối với cậu phải không?

83
00:08:27,112-->00:08:31,113
Đối với Rinne, chìa khóa đó có ý nghĩa
tương tự như nơ cột tóc của Kotori.
Đây là một tín hiệu tốt. Chúng tôi sẽ tiếp tục
như trước, cùng nhau sống như một gia đình.

84
00:08:32,112-->00:08:38,113
Ừ. Cảm ơn Shidou. Tớ sẽ đi khóa cửa.

85
00:08:39,113-->00:08:41,113
Ờ... Nhờ cậu vậy.

86
00:08:42,112-->00:08:48,113
Và đây là lần đầu tiên chúng ta đi chung với nhau.

87
00:08:52,112-->00:08:54,513
Tớ cũng rất vui vì cậu tìm thấy nó...

88
00:08:55,512-->00:09:00,113
Ừ. Lúc đó tớ nghĩ "mình phải làm gì bây giờ?"

99
00:09:01,112-->00:09:03,513
Cảm giác này là gì?...

100
00:09:04,512-->00:09:08,113
Thật kỳ lạ... Trước khi Tohka 
và các cô gái khác đến, Rinne và tôi 
đã có khoảng thời gian cùng nhau đi học...

101
00:09:09,112-->00:09:14,513
Shidou sao vậy? Cậu đang nghĩ gì thế?

102
00:09:15,513-->00:09:18,113
À, không có gì! Tớ đang nghĩ trời nóng quá.

103
00:09:19,112-->00:09:28,113
Cậu nói đúng. Ve sầu bắt đầu kêu
làm cho cậu thấy nóng hơn phải không?

106
00:09:29,112-->00:09:31,113
Ờ... đúng vậy.

107
00:09:32,312-->00:09:43,113
Nhưng mùa hè cũng tốt. Cậu có thể đi chơi 
tại hồ bơi như chúng ta đã làm.
Chúng ta sẽ ăn kem và mì lạnh hiyashi chuka.

108
00:09:44,112-->00:09:47,113
Cậu có vẻ cũng tham ăn giống Tohka nhỉ?

109
00:09:49,112-->00:10:01,113
Tớ chỉ đang nói về đồ ăn mùa hè thôi. 
Và cậu nói những điều đó là cậu cũng 
xúc phạm Tohka-chan, phải không?

113
00:10:01,112-->00:10:06,113
Xin lỗi, xin lỗi. Trước khi cậu la tớ 
thì tớ nên chạy! Tớ đi trước nhé! 
Hẹn gặp cậu ở trường!

114
00:10:07,112-->00:10:10,113
A, này! Đừng có chạy!

1
00:10:11,112-->00:10:14,113
Haha, Tớ xin lỗi, nhưng có những lúc
chạy trốn chiến thắng!

1
00:10:15,113-->00:10:18,113
Được lắm Shidou!

1
00:10:22,512-->00:10:25,113
Phù! Tớ đến rồi!

1
00:10:26,112-->00:10:31,113
Shidou, tớ bực mình lắm, cậu biết không?
Tự nhiên cậu chạy đi và bỏ tớ lại...

1
00:10:32,112-->00:10:36,113
Xin lỗi, xin lỗi. Tớ thấy nó thú vị!
Phù, tớ toát hết mồ hôi rồi này.

1
00:10:37,112-->00:10:49,113
Đây là những gì cậu muốn là Shidou. 
Dù là vận động tốt cho sức khỏe nhưng giờ
người chúng ta đầy mồ hôi.

368
00:10:50,112-->00:10:54,113
Hello hai người!

368
00:10:55,112-->00:10:58,113
À, chào buổi sáng, Tonomachi-kun.

368
00:10:59,113-->00:11:02,113
Chào buổi sáng, Tonomachi.
Ông trông khỏe mạnh từ sáng sớm nhỉ.

368
00:11:03,112-->00:11:12,113
Đó là sáng nay tôi đã ăn 10 quả trứng! 
Nhờ đó năng lượng của tôi rất dồi dào đấy!

68
00:11:13,112-->00:11:16,113
Này, ăn thế có hại lắm đấy.

368
00:11:17,112-->00:11:28,113
Này này, những người lo cho tôi thường
ghen tị với cơ thể tôi lắm đấy. 
Có phải không Itsuka? Ông đang ghen tị phải không?

368
00:11:29,112-->00:11:32,113
Ai thèm ghen tị với một người như ông?

368
00:11:33,112-->00:11:39,113
Cậu thực sự hài hước đấy, Tonomachi-kun.

368
00:11:40,112-->00:11:48,113
Ồ! Tớ biết. Cậu nhận thấy phải không?
Sự sáng chói không thể phủ nhận của tớ!

68
00:11:49,112-->00:11:51,113
Ông uống thuốc chưa đấy?

368
00:11:52,112-->00:12:04,113
Rinne-chan, cậu sẽ nói cho tớ biết 
nếu có điều gì xảy ra đúng không? 
Cậu luôn dành thời gian cho Itsuka là vì
hắn ta biết bí mật gì đó của cậu phải không?

368
00:12:05,113-->00:12:12,113
Nếu không phải như vậy mà cậu cứ đi
theo hắn thì đúng là kỳ lạ!

368
00:12:15,112-->00:12:23,113
Um... Tớ không nghĩ cậu ấy biết bí mật
hay bất cứ điều gì của tớ đâu... 
Xin lỗi nhé, Tonomachi-kun.

368
00:12:24,112-->00:12:34,113
Không thể nào... Chết tiệt! 
Tại sao luôn là Itsuka? Tôi sẽ không chấp nhận! 
Chắc chắn tôi sẽ không bao giờ chấp nhận nó!

368
00:12:38,112-->00:12:44,113
Cậu nghĩ Tonomachi-kun sẽ ổn nếu chúng ta 
để cậu ta đi một mình không?

368
00:12:45,113-->00:12:48,113
Đừng lo lắng. Một lúc nữa, hắn sẽ trở về 
với một khuôn mặt không có gì xảy ra.

368
00:12:50,112-->00:12:52,513
Cậu nói đúng.

368
00:12:53,513-->00:12:57,113
Thật là... Tonomachi nói về "bí mật". 
Chúng ta là bạn thời thơ ấu, phải không Rinne?

368
00:12:58,113-->00:13:05,113
Shidou? Cậu nói thế 
làm tớ cảm thấy hơi buồn đấy.

368
00:13:06,112-->00:13:09,113
À, không... ý tớ không phải thế.

368
00:13:10,112-->00:13:15,113
Nếu cậu nói những điều như vậy,
ý cậu là cậu muốn xua đuổi tớ?

368
00:13:16,113-->00:13:20,113
Không, thực sự xin lỗi! Tớ sai rồi!

368
00:13:21,112-->00:13:25,113
Cậu nói thật phải không? 
Tớ tha thứ cho bạn.

368
00:13:26,112-->00:13:28,113
Haha... Tớ đã được cứu ...

368
00:13:34,512-->00:13:37,113
Bằng cách nào đó tôi đã có thể vượt qua
buổi học nhàm chán trong buổi sáng!

368
00:13:38,112-->00:13:41,113
Giờ là giờ ăn trưa.

368
00:13:42,112-->00:13:45,113
Shi... Shidou!

368
00:13:46,112-->00:13:49,113
Ồ, Tohka! Em đã ngủ tất cả buổi học, phải không?
Giớ là đến buổi trưa rồi đấy?

368
00:13:50,112-->00:13:58,113
Ừ! Thật ra, đã có một cái gì đó
xảy ra ngày hôm qua..

368
00:13:59,112-->00:14:04,113
Shidou... cái gì vậy?

368
00:14:05,112-->00:14:08,113
Hở? À, cái này à? Đây là Rinne chuẩn bị 
cho anh ngày hôm nay.

368
00:14:09,112-->00:14:14,113
Vậy... vậy à.

368
00:14:15,112-->00:14:18,113
Ừ. Vậy có chuyện gì vậy? Em đang định
nói về chuyện gì phải không?

368
00:14:19,112-->00:14:25,113
Không... không có gì! Không có gì!

368
00:14:26,112-->00:14:28,113
Ơ... này...

368
00:14:30,112-->00:14:33,113
Giống như mình luôn làm cái gì đó 
khiến mọi thứ tệ hơn...

368
00:14:34,512-->00:14:37,113
Mình không thể nghĩ gì với cái 
dạ dày trống rỗng... Giờ mình ăn trưa thôi.

368
00:14:39,112-->00:14:42,113
Um, ngon thật! Rinne làm luôn rất ngon.

368
00:14:49,112-->00:14:54,113
Giờ chúng ta bắt đầu với giờ học buổi chiều.

368
00:14:55,812-->00:14:58,113
Đến giờ học rồi à?
Ngắn thật đấy. Vừa mới đây thôi mà.

368
00:14:59,113-->00:15:05,113
À, hôm nay cô có việc quan trọng 
để nói với các em.

368
00:15:06,112-->00:15:10,113
"Việc quan trọng ", gì vậy nhỉ?

368
00:15:11,112-->00:15:19,113
Tất cả các em đều biết rằng trong thành phố này
có hệ thống báo động JSDF phải không?

368
00:15:20,112-->00:15:23,113
JSDF? Phản ứng của tôi khi nghe từ đó là
nhìn Origami. Nhưng cô ấy thậm chí còn 
không biểu cảm khuôn mặt của mình.

368
00:15:24,112-->00:15:30,113
Bọn em biết. Nhưng chuyện gì xảy ra vậy ạ?


368
00:15:31,112-->00:15:39,113
Khi có một thông tin liên lạc cho các trường học,
cô nghĩ cô cần phải nói với các em.

368
00:15:40,112-->00:15:52,113
Cũng có rất nhiều thông tin nhưng...
Hiện nay, các thiết bị truyền thông của Tengu, 
dường như không hoạt động.

368
00:15:53,112-->00:15:55,513
Điều đó có nghĩa rằng AST là không hoạt động?

368
00:15:56,512-->00:16:00,113
Origami không thay đổi biểu cảm
cô ấy tất nhiên đã biết điều này, phải không? 
Đúng vậy, điều đó không có gì lạ.

368
00:16:01,112-->00:16:10,113
Do đó... có thể là những báo động
Không gian chấn bị trì hoãn, hoặc không hoạt động.

368
00:16:11,112-->00:16:16,113
Điều đó có nghĩa là tình hình khá xấu, phải không?
Không chỉ có các Tinh Linh hay Fraxinus, 
AST cũng đang bị ảnh hưởng của cái gì đó.

368
00:16:17,112-->00:16:28,113
Vì vậy nên cô khuyên các em nên ở trong nhà,
hạn chế đi ra ngoài đường nếu không cần thiết.

368
00:16:29,112-->00:16:41,113
Mặc dù chúng ta không biết nơi ẩn náu khi
có chuyện xảy ra, nhưng các em
hãy bình tĩnh và hành động theo "3 không".

368
00:16:42,112-->00:16:50,113
"Không xô đẩy, không chạy nhảy, không hỗn loạn".
Nhớ chưa?

368
00:16:51,112-->00:16:53,113
Chuyện gì đang xảy ra trên 
toàn thành phố Tengu vậy?

368
00:16:54,113-->00:16:57,113
Tôi tự hỏi tôi có nên hỏi... Origami?

368
00:17:01,112-->00:17:09,112
Được rồi. Giờ chúng ta 
bắt đầu buổi học chiều nhé.

368
00:17:14,112-->00:17:16,113
Mình nên làm gì hôm nay đây?

368
00:17:17,112-->00:17:20,113
À... Tôi đã có kế hoạch của tôi 
cho sau giờ học với Rinne...

00:17:21,112-->00:17:23,113
Rinne đang ở đâu nhỉ?

368
00:17:24,112-->00:17:26,113
Shi-dou!

368
00:17:27,112-->00:17:28,113
Waaa!

368
00:17:29,112-->00:17:32,113
Chuyện gì vậy?

368
00:17:33,112-->00:17:37,113
Không, không có gì. Quan trọng hơn,
chúng ta tan trường rồi

368
00:17:38,112-->00:17:41,513
À, phải rồi. Cậu định làm gì?

368
00:17:42,512-->00:17:46,113
Cậu mời tớ mà
Cậu định đi đâu? Mua sắm?

368
00:17:47,112-->00:17:51,113
Chúng ta có thể đi một chút không?

368
00:17:52,112-->00:17:54,113
Được... được rồi.

368
00:17:55,112-->00:17:59,113
Vậy đi theo tớ.

368
00:18:00,112-->00:18:02,113
Được rồi.

368
00:18:05,512-->00:18:07,113
Vậy có chuyện gì vậy?

368
00:18:08,112-->00:18:11,113
Nghe này Shidou...

368
00:18:12,112-->00:18:16,113
Cậu có muốn... đi hẹn hò không?

368
00:18:17,112-->00:18:18,113
Sao cơ?

368
00:18:19,112-->00:18:21,113
Hả?

368
00:18:22,112-->00:18:27,113
Khuôn mặt đó của cậu là sao?

368
00:18:28,112-->00:18:30,113
Chỉ là ngày hôm qua mặc dù tớ mời cậu...

368
00:18:31,112-->00:18:35,113
Những ngày vừa rồi mặc dù tôi mời cô ấy đi,
cô ấy lại gọi người khác đi cùng và bỏ chúng tôi lại,
như thể cố gắng tránh mặt tôi...

368
00:18:36,113-->00:18:39,113
Sao cậu tự nhiên thay đổi suy nghĩ vậy?

368
00:18:40,112-->00:18:46,113
Điều đó không cần phải hỏi. 
Suy nghĩ của con gái thay đổi nhanh lắm.

368
00:18:47,112-->00:18:50,113
Th... thay đổi quá dễ dàng...

368
00:18:51,112-->00:18:55,113
Vậy cậu có... muốn đi không?

368
00:18:56,113-->00:18:59,113
Hở? À, tất nhiên là có. Tớ chẳng có
lý do nào để từ chối cả.

368
00:19:01,112-->00:19:03,113
Thật vậy à? May quá!

368
00:19:04,112-->00:19:07,113
Ờ... ờ...

368
00:19:10,112-->00:19:12,113
Và tôi đã cùng Rinne đi hẹn hò.

368
00:19:16,112-->00:19:20,113
Hở... Đây là...?

368
00:19:21,112-->00:19:24,113
Gì vậy Rinne? Cậu đang tìm kiếm gì à?

368
00:19:25,112-->00:19:33,113
À, phải rồi... Chờ một chút.
Tớ chắc chắn là ở quanh đây, nhưng...

368
00:19:34,112-->00:19:40,113
À! Ở đó! Shidou, tớ muốn thử!

368
00:19:41,112-->00:19:44,113
À, đó là cây kem. Cậu nói là 
ăn kem vào mùa hè rất ngon phải không?

368
00:19:45,112-->00:19:47,113
Ừ!

368
00:19:48,112-->00:19:51,113
Được rồi. tớ sẽ mua chúng. 
Cậu thích ăn vị gì, Rinne?

368
00:19:52,112-->00:19:57,113
À, không, không cần mua đâu.

368
00:19:58,112-->00:19:59,513
Hở?

368
00:20:00,513-->00:20:03,113
Sao cô ấy lại từ chối nhỉ...

368
00:20:04,113-->00:20:10,113
Tớ... tớ nghĩ là tớ sẽ tự chọn. Được không?

368
00:20:11,113-->00:20:14,113
Được rồi... Nếu vậy thì cậu chọn đi.

368
00:20:15,112-->00:20:17,113
Được rồi. Để tớ chọn.

368
00:20:18,112-->00:20:21,113
Cậu ấy không biểu lộ thế này trước đây?

368
00:20:22,112-->00:20:25,113
Chút nữa tớ quay lại nhé!

368
00:20:26,112-->00:20:28,113
Ừ. Tớ sẽ đợi ở đây.

368
00:20:29,112-->00:20:32,113
Được rồi.

368
00:20:40,112-->00:20:43,513
Xin lỗi vì để cậu đợi lâu.

368
00:20:44,512-->00:20:48,113
Ờ, Sao lâu thế? Cậu mất nhiều thời gian 
hơn tớ nghĩ... Hở?

368
00:20:50,112-->00:20:55,113
Tớ đã mua thêm một số thứ khác.

368
00:20:56,112-->00:21:00,113
Thế thì sẽ tốn tiền lắm đấy.

368
00:21:01,112-->00:21:08,113
Vì tớ muốn thử tất cả các vị.
Shi... Shidou, cậu sẽ giúp tớ phải không?

368
00:21:09,112-->00:21:13,113
Được rồi. Cậu cứ việc chọn lựa. Ăn nhanh lên
trước khi chúng chảy ra đi.

368
00:21:14,112-->00:21:17,513
Cảm... cảm ơn nhé. Tớ sẽ cố gắng!

368
00:21:18,512-->00:21:21,113
Sao tự nhiên cậu nói "cố gắng" vậy?

368
00:21:22,112-->00:21:25,113
Chuyện này là gì? Tôi cảm thấy trước đây 
tôi cũng đã làm điều gì đó tương tự thế này...

368
00:21:26,112-->00:21:30,113
Shidou? Cậu muốn mua thêm à?

368
00:21:31,112-->00:21:33,513
Ừ, đúng vậy! Tiếc là ở đây không có 
cả núi kem để ăn!

368
00:21:34,512-->00:21:38,113
Chúng ta hãy đi đến công viên! 
Nhanh lên Rinne! Kem sẽ chảy ra đấy!

368
00:21:39,112-->00:21:42,113
Ừ... ừ!

368
00:21:50,512-->00:21:53,113
Aaaa, ngon quá! Không gì tuyệt vời hơn
ăn kem vào mùa hè phải không?

368
00:21:54,112-->00:22:00,113
Nó lạnh... và rất ngon...

368
00:22:01,112-->00:22:03,113
Này, Rinne! Nhìn cậu ăn, tớ cũng
đang "tan chảy" ra đấy!

368
00:22:04,102-->00:22:11,113
Ngon thật...! Nó rất ngon!

368
00:22:12,112-->00:22:15,113
Ừ, nó ngon lắm. Nhưng lạnh quá.
Đầu tớ thấy nhức vì lạnh.

368
00:22:16,512-->00:22:19,513
Cô bé đó là Yoshino?

368
00:22:20,512-->00:22:23,113
Tại sao là Yoshino? Chẳng lẽ tôi đã 
đi ăn kem với cô bé sao?

368
00:22:28,112-->00:22:31,113
Tôi có những kỷ niệm. Nhưng là khi nào?
Làm sao tôi lại hẹn hò với Yoshino?

368
00:22:32,112-->00:22:34,113
Này, Rinne... Có vẻ hơi lạ
khi tớ hỏi, nhưng...

368
00:22:35,112-->00:22:38,113
Chuyện này thực sự rất khó khăn.

368
00:22:39,112-->00:22:41,113
Hở? Cậu vừa nói gì?

368
00:22:42,112-->00:22:49,113
À, kem của cậu chảy ra tay rồi kìa.

368
00:22:53,112-->00:22:55,113
Ri... Rinne?

368
00:22:56,312-->00:23:00,113
Không. Để yên nào...

368
00:23:02,112-->00:23:07,112
Đợi một chút... sẽ sạch... ngay thôi...

368
00:23:08,112-->00:23:10,112
Nhưng... nhưng mà...

368
00:23:14,512-->00:23:17,113
Cũng tương tự như vậy... 
một cái gì đó tôi đã làm trước đây...?

368
00:23:20,112-->00:23:23,113
Ri... Rinne! Đủ... đủ rồi!

368
00:23:24,113-->00:23:32,513
Quả nhiên là... Tớ vẫn chưa đủ, phải không?

368
00:23:33,112-->00:23:44,113
1) Yêu cầu một lời giải thích về Rinne.
2) Lo lắng về Rinne.

368
00:23:45,113-->00:23:55,113
N... này, Rinne, chuyện gì vậy? 
Cậu không sao chứ?

368
00:23:58,113-->00:24:07,113
Hở? À, ừ... không sao cả...
Có vẻ tớ ăn kem hơi nhiều quá thôi.

368
00:24:08,113-->00:24:12,113
Vậy à... Cậu không cần ăn quá nhiều.
Nếu cậu cứ cố ép bản thân thì 
không tốt chút nào đâu phải không?

368
00:24:13,112-->00:24:18,113
Ừ... ừ. Đó là sự thật, phải không?

368
00:24:23,112-->00:24:25,113
Hả? Này! Rinne?

368
00:24:27,112-->00:24:35,113
Xin... xin lỗi. Có lẽ do tớ hơi 
thiếu máu nên chóng mặt quá.

368
00:24:36,112-->00:24:38,113
Thật vậy không?

368
00:24:39,112-->00:24:48,513
Ừ, tớ ổn, tớ ổn mà... Nếu tớ nghỉ ngơi một chút...
tớ sẽ sớm khỏe lại thôi...

368
00:24:49,112-->00:24:51,513
Nếu không có vấn đề gì lớn...
Thì bây giờ, chúng ta hãy nghỉ ngơi một chút.

368
00:24:52,512-->00:24:58,113
Hãy tha thứ cho tớ, Shidou. Mặc dù 
đây là cuộc hẹn của chúng ta, tớ lại không khỏe...

368
00:24:59,112-->00:25:02,113
Không sao, không sao. 
Sức khỏe của cậu quan trọng hơn.

368
00:25:03,112-->00:25:10,113
Cảm ơn nhé... Nhưng mà tớ vẫn chưa
mút xong kem chảy ở tay phải không?

368
00:25:11,112-->00:25:14,113
Chuyện đó không thành vấn đề. 
Tay tớ sạch rồi mà.

368
00:25:15,112-->00:25:20,113
Ừ... Cảm ơn nhé Shidou.

368
00:25:21,112-->00:25:26,113
Rinne, rốt cuộc cô ấy làm sao vậy?
Có lẽ tôi đã quá phụ thuộc vào cô ấy chăng?

368
00:25:27,112-->00:25:29,113
Shidou?

368
00:25:30,112-->00:25:32,113
Hở? Có chuyện gì?

368
00:25:33,312-->00:25:38,113
Tớ đã nghỉ ngơi đủ rồi. Tôi cảm thấy tốt hơn rồi...
Chúng ta về chứ?

368
00:25:39,112-->00:25:44,113
Vậy à... Vậy chúng ta về đi. Nhưng nếu cậu 
tiếp tục cảm thấy mệt thì nói tớ biết ngay. 
Được không? Tớ sẽ đưa về nhà. 

368
00:25:45,112-->00:25:50,113
Được rồi. Cảm ơn nhé Shidou.

368
00:25:51,112-->00:25:53,113
Ờ... ờ...

368
00:26:00,112-->00:26:04,113
Shidou, bữa tối thế nào?

368
00:26:05,112-->00:26:08,113
Hở? À, vẫn ngon như mọi khi.
Nếu bạn mở nhà hàng, chắc chắn sẽ đắt khách đấy.

368
00:26:09,112-->00:26:14,113
Cậu nói hơi quá rồi đấy.

368
00:26:22,112-->00:26:24,113
N... này Rinne?

368
00:26:25,112-->00:26:28,113
Gì vậy?

368
00:26:29,112-->00:26:32,113
Um... Bắt đầu từ ngày mai, cậu biết không? 
Tớ sẽ lo bữa sáng, bữa tối và công việc nhà. 
Trong khoảng thời gian này, cậu không cần 
giúp đâu. Được không?

368
00:26:37,112-->00:26:41,113
Ừ, hôm nay cậu đã ngất trong công viên 
phải không? Lúc đó tớ nghĩ... có lẽ cậu
làm việc nhiều quá...

368
00:26:42,112-->00:26:45,113
Không phải vậy đâu.

368
00:26:46,112-->00:26:51,113
Nhưng cậu phải biết là việc nhà này là
việc tớ phải làm. Ngay cả cậu cũng phải
về nhà mình mà phải không?

368
00:26:52,112-->00:26:58,113
Không phải vậy! Tớ đã nói với cậu rồi!
Tớ làm vì tớ thích thế.

368
00:26:59,113-->00:27:02,113
Dù là vậy, chúng ta cũng đang 
trong mùa thi. Nếu để cậu làm mọi thứ
một mình thì không tốt chút nào.

368
00:27:07,112-->00:27:10,113
Nếu để cậu làm mọi thứ thì cậu sẽ lại
ngất đi. Chuyện đó không tốt đâu.

368
00:27:11,112-->00:27:19,113
Tớ... tớ hiểu rồi. Vậy là sau mọi chuyện,
tớ chỉ khiến cậu bận tâm phải không?

368
00:27:20,112-->00:27:23,113
Không, cậu nhầm rồi! 
Cậu đã giúp đỡ tớ rất nhiều!

368
00:27:24,112-->00:27:28,513
Vậy thì... ngày mai tớ có thể đến được không?

368
00:27:29,513-->00:27:34,113
Hở, được rồi... ít nhất tớ muốn cậu 
nghỉ ngơi trong giai đoạn thi cử này...

368
00:27:35,113-->00:27:40,113
Cậu không muốn tớ ở đây phải không?
Tớ gây phiền toái đến cậu à?

368
00:27:41,113-->00:27:44,113
Cậu sai rồi! Tớ đã nói ý tớ không phải thế!

368
00:27:45,113-->00:27:51,113
Tớ không sao đâu. Vậy nên cậu không cần
phải lo lắng gì hết, được chứ?

368
00:27:52,112-->00:27:55,113
Không được, dù cậu nói tớ không lo lắng 
thì tớ vẫn lo cho cậu...

368
00:27:55,112-->00:28:00,113
Đủ rồi! Tớ đã nói là tớ không sao mà!

368
00:28:01,112-->00:28:03,113
Ri... Rinne?

368
00:28:04,512-->00:28:12,113
Tớ sẽ chăm sóc cho sức khỏe của tớ!
Tớ sẽ không làm phiền cậu nữa, Shidou!
Vì vậy...!

368
00:28:13,112-->00:28:16,113
Này... nậu quan tâm đến tớ sao?
Nhưng đó chính là lý do tại sao tớ...

368
00:28:17,112-->00:28:18,513
Shidou...

368
00:28:19,512-->00:28:24,113
Aaa, được rồi! Tớ đồng ý! Tớ đồng ý!
Nhưng cậu không được làm việc quá sức mình
đâu đấy, nhớ chưa?

368
00:28:25,112-->00:28:33,513
Được rồi. Cảm ơn Shidou. 
Và hãy tha thứ cho tớ nếu tớ làm gì sai.

368
00:28:34,512-->00:28:37,113
Được rồi, đừng lo!

368
00:28:44,112-->00:28:48,113
Tôi bắt đầu học ôn thi nhưng... chẳng được gì.
Tôi chẳng tập trung được gì cả.

368
00:28:49,112-->00:28:52,113
Có phải vì tôi vẫn chưa hoàn toàn bình phục?
Mặc dù tôi cố gắng nhét chữ vào đầu, 
tôi vẫn không hiểu gì cả...

368
00:28:53,112-->00:28:56,113
Vào những lúc như thế này, thay vì tiếp tục ép buộc,
tôi nên thư giãn một chút và sau đó học lại.

368
00:28:57,112-->00:29:00,113
Đầu tiên tôi nên đi tắm.

368
00:29:11,512-->00:29:14,113
Hả? Ko... Kotori?

368
00:29:19,112-->00:29:23,113
Khoan... khoan đã! N... này, em có thể 
nghĩ chuyện này không thể tránh khỏi!
Đầu tiên, em hãy quên...

368
00:29:24,112-->00:29:31,113
Đồ... ĐỒ BIẾN THÁI!!!!!

368
00:29:40,112-->00:29:43,113
Uida, uida, đau quá... Anh đã nói với em 
mà phải không? Anh không có ý đó. Em hiểu lầm rồi...

368
00:29:44,112-->00:29:51,113
Em biết. Nếu anh làm có mục đích,
chắc chắn anh sẽ không làm một mình.

368
00:29:55,112-->00:30:05,113
Em đã suy nghĩ và không còn nghi ngờ 
việc anh có thể trở thành trung tâm 
của chương trình tranh luận về đêm. 
Một "sự thật kinh khủng" hấp dẫn nhất.

368
00:30:06,112-->00:30:08,113
sự thật kinh khủng gì?

368
00:30:09,112-->00:30:18,513
Em sẽ nói thế này: "Đây không phải là trò đùa.
Nhiều lúc anh ấy làm những trò biến thái.
Đó là lý do tại sao anh ấy luôn bị gọi là 
"Itsuka thú tính'. 

368
00:30:19,112-->00:30:25,113
Nhưng tôi không bao giờ có thể tưởng tượng 
anh ấy lại làm chuyện loạn luân với em gái mình"

368
00:30:26,112-->00:30:28,113
Đó là điều mà anh có thể tưởng tượng 
khá sinh động đấy!

368
00:30:29,112-->00:30:38,113
Nếu anh không muốn thế thì anh phải gõ cửa chứ. 
Ngay cả con nít cũng biết thế mà.

368
00:30:39,112-->00:30:43,113
Gõ cửa à...
Dù sao anh cũng sẽ cẩn thận hơn. Nhưng mà...

368
00:30:44,112-->00:30:46,413
Gì nữa?

368
00:30:47,112-->00:30:50,113
Em, tất cả bọn em phải không?

368
00:30:53,112-->00:30:56,113
Um... em có một khuôn mặt khá mệt mỏi
trong nhà tắm. Chẳng lẽ...

368
00:30:57,112-->00:31:03,113
Nếu anh quan tâm đến em,
có nghĩa là em tới số rồi phải không?

368
00:31:04,512-->00:31:07,113
Này, em có biết anh đang thưc sự lo lắng không?

368
00:31:08,112-->00:31:13,113
Em ổn. Vấn đề nằm ở chỗ khác kìa.

368
00:31:14,112-->00:31:17,113
Em đã điều tra được gì à?

368
00:31:18,112-->00:31:24,113
Hôm nay đã có một phản ứng lớn trong 
sức mạnh Tinh Linh khổng lồ mà anh biết.

368
00:31:25,112-->00:31:28,113
Có chuyện gì xảy ra với các Tinh Linh à?

368
00:31:29,112-->00:31:33,113
Vẫn không thể xác định đó có phải là
một Tinh Linh mới hay không...

368
00:31:34,113-->00:31:37,113
Vậy à. Vậy câu trả lời đó là gì?

368
00:31:38,112-->00:31:46,113
Mặc dù chỉ là tạm thời, cho đến nay 
các sóng Tinh Linh vẫn không ổn định. Nhưng mà...

368
00:31:47,112-->00:31:49,113
Nhưng mà sao?

368
00:31:50,112-->00:31:57,113
Cuối cùng, tình hình bất ổn 
thậm chí còn lớn hơn trước.

368
00:31:58,112-->00:32:01,113
Vậy là có lý do nào đó khiến
sức mạnh Tinh Linh ngoài tầm kiểm soát.

368
00:32:02,112-->00:32:11,113
Đó là những gì em đã nói trước đây.
Nhưng thực sự có khả năng thời gian 
đang lặp đi lặp lại.

368
00:32:15,112-->00:32:19,113
Nhưng chuyện gì đang xảy ra chứ?

368
00:32:20,112-->00:32:23,113
Anh cũng không tưởng tượng được.

368
00:32:24,112-->00:32:39,113
Đó là sự thật. Chúng ta phải nhanh chóng tìm ra 
cách để phá vỡ kết giới này. Quan trọng nhất là
chúng ta phải tránh xảy ra hậu quả khó lường.

368
00:32:40,112-->00:32:43,113
Kotori có vẻ như thật sự bị dồn vào chân tường...

368
00:32:44,112-->00:32:56,513
Tuy nhiên, thời gian của chúng ta ít hơn
chúng ta tưởng. Chúng ta cần tìm ra cái đuôi
của kẻ thù và tóm lấy nó.

368
00:32:57,512-->00:33:00,113
Nếu em cần giúp đỡ gì, cứ nói anh.

368
00:33:01,112-->00:33:13,113
Shidou, anh là một trong những người 
kiểm tra các Tinh Linh. Hãy chắc chắn phải chú ý
và không được rời mắt khỏi họ.

368
00:33:14,112-->00:33:16,113
Anh hiểu rồi.

368
00:33:17,112-->00:33:21,113
Vâng, hôm nay thế là đủ rồi. Em đi ngủ đây.

368
00:33:22,112-->00:33:24,113
Ừ, chúc ngủ ngon...

368
00:33:25,112-->00:33:30,113
chúc ngủ ngon, Onii-chan.

368
00:33:35,112-->00:33:37,113
Với kẻ thù không ổn định à...

368
00:33:38,112-->00:33:41,113
Những chuyện này chẳng thể nào hiểu được.

368
00:33:48,112-->00:33:52,113
Sau tất cả, đều là vô ích sao?

368
00:33:53,112-->00:33:59,113
Tôi không thể giải quyết việc này...

368
00:34:00,112-->00:34:06,112
Nhưng tại sao? Tại sao tôi không thể thành công?

368
00:34:07,112-->00:34:13,112
Sau những gì tôi làm...
tại sao vẫn chưa chọn Tinh Linh nào?

368
00:34:14,112-->00:34:19,112
Tôi vẫn đang làm điều gì sai sao?

368
00:34:20,112-->00:34:24,512
Kết quả tôi muốn...

368
00:34:25,512-->00:34:34,112
Trong trường hợp đó, điều tiếp theo tôi sẽ làm là ...

368
00:34:38,112-->00:34:45,112
Ngày 29 tháng 6

  