﻿1
00:00:00,112-->00:00:01,513
1) Tớ muốn đi mãi mãi.
2) Tớ không thể đi mãi mãi.

1
00:00:02,512-->00:00:22,113
Không Rinne. Chuyện đó... là không thể.

1
00:00:24,112-->00:00:28,113
Tại sao...?

1
00:00:29,112-->00:00:31,113
Tại vì...

1
00:00:34,112-->00:00:38,113
Tôi cũng tự hỏi tại sao. Cứ như
miệng tôi đang tự ý trả lời vậy.

1
00:00:40,112-->00:00:45,113
Sau mọi chuyện, thật sự tôi không có lý do
gì để từ chối Rinne. Cô ấy là bạn thuở nhỏ 
quý giá của tôi. Tôi muốn giữ cô ấy bên mình
nhiều hơn, nhiều hơn nữa.

1
00:00:47,112-->00:00:51,113
Vẫn còn chưa muộn nếu tôi xin lỗi cô ấy, 
cô ấy sẽ tha thứ cho tôi. Tôi cũng muốn 
tiếp tục sống cùng Rinne.

1
00:00:53,112-->00:00:57,113
Hôm nay thật sự rất vui. Và nếu tôi 
có thể hẹn hò cùng với Rinne mãi mãi,
điều đó sẽ thật sự... rất... tuyệt...

1
00:01:00,112-->00:01:04,113
Ký ức của tôi đang mờ dần.
Chúng đang trở về cùng một lúc.

1
00:01:05,112-->00:01:08,113
Cứ như ảo giác cả thế giới đang mờ đi.

1
00:01:11,112-->00:01:18,113
Quả nhiên... tớ không thể trở thành
một phần gia đình cậu phải không?

1
00:01:19,112-->00:01:21,113
Rinne...

1
00:01:25,112-->00:01:28,113
Mọi thứ biến mất sau khi Rinne nói.

1
00:01:32,112-->00:01:34,113
Mình đã trở về rồi à?

1
00:01:35,112-->00:01:38,113
Đúng vậy. Tôi đã trở lại nơi có 
những rễ cây. Và sau đó...

1
00:01:40,112-->00:01:48,113
Cậu đã thoát khỏi thiên đường Eden, Paradise Lost.

1
00:01:49,112-->00:01:52,113
Tớ không hiểu lắm. Nhưng vừa rồi là...

1
00:01:57,112-->00:02:05,113
Vậy à... Bây giờ mọi thứ đều ổn rồi Shidou.

1
00:02:06,112-->00:02:08,113
Hở?

1
00:02:09,112-->00:02:11,113
Có nghĩa là ...?

1
00:02:12,112-->00:02:14,113
Chuyện... chuyện gì vậy?

1
00:02:15,112-->00:02:24,113
Cậu đã phá hủy Paradise Lost. 
Tớ không... còn gì nữa...

1
00:02:25,112-->00:02:27,113
Rinne, cậu nghe tớ nói này...

1
00:02:28,112-->00:02:31,113
Tớ nghĩ những buổi hẹn hò là cơ hội
để tớ được nhìn thấy cậu cười.
Sự thật là tớ muốn chúng ta
sẽ ở bên nhau mãi mãi.

1
00:02:32,112-->00:02:37,113
Dù cậu có là một phần gia đình tớ 
hay không thì người gần gũi nhất với tớ 
trong thế giới này chính là cậu đấy Rinne!

1
00:02:38,112-->00:02:42,113
Cho dù tớ không sống trong thế giới này nữa,
dù tớ có ra khỏi đây, thì sự thật đó
vẫn không thay đổi! Cậu vẫn rất quan trọng với tớ!

1
00:02:43,112-->00:02:48,113
Chúng ta cùng về nhà nào, Rinne!
Hãy bắt đầu từ đầu, từ bây giờ
trong thế giới thực. Tất cả sẽ 
tốt đẹp mà không cần Thiên đường này.

1
00:02:49,112-->00:02:53,113
Cậu không phải sợ cô đơn. Tớ sẽ
ở bên cậu! Tớ tin cậu có thể
tìm thấy hạnh phúc của riêng mình!

1
00:02:54,112-->00:02:56,113
Shidou...

1
00:03:00,113-->00:03:02,113
Rinne, cậu hiểu tớ nói chứ?

1
00:03:03,112-->00:03:17,113
Tớ đã hoàn toàn mất kiểm soát
Thiên đường Eden. Nếu điều đó xảy ra, 
không biết chuyện gì sẽ xảy ra với thế giới này.

1
00:03:18,112-->00:03:20,113
Cái gì? Không có cách nào ngăn nó lại sao?

1
00:03:21,112-->00:03:26,113
Vẫn còn một cách.

1
00:03:27,112-->00:03:29,113
Thật à? Cách gì?

1
00:03:30,112-->00:03:33,113
Hãy phong ấn tớ đi.

1
00:03:37,112-->00:03:49,113
Nếu cậu làm vậy, kết giới sẽ biến mất 
và mọi sức mạnh sẽ bị xóa bỏ.
Chỉ có cậu mới làm được thôi, Shidou!

1
00:03:50,112-->00:03:53,113
Cậu nói đúng. Nếu tớ phong ấn cậu...
Nhưng khả năng của tớ chỉ làm được
khi tớ khiến Tinh Linh yêu tớ thôi.

1
00:03:54,112-->00:04:03,113
Shidou, sau mọi chuyện, 
cậu vẫn lo lắng về chuyện đó sao?

1
00:04:04,112-->00:04:09,113
Không sao đâu. Mọi chuyện sẽ ổn thôi.

1
00:04:10,112-->00:04:14,113
Được! Tớ hiểu rồi! Bằng cách này, tất cả
chúng ta có thể quay trở lại. Cậu sẽ
được cứu, phải không?

1
00:04:15,112-->00:04:24,113
Đúng vậy. Tớ chắc chắn chúng ta sẽ
được sống cùng nhau. Đây là cách duy nhất.

1
00:04:27,112-->00:04:33,113
Shidou, không có thời gian để... cậu do dự đâu.

1
00:04:34,112-->00:04:37,113
Tớ hiểu rồi! Vậy tớ làm đây...

1
00:04:38,112-->00:04:44,113
Được rồi. Cuối cùng... 
tớ cũng đã có những giấc mơ đẹp nhất.

1
00:04:50,112-->00:04:54,113
Tại sao... trông Rinne rất buồn...?

1
00:05:07,112-->00:05:10,113
Hình như kết giới đã biến mất?
Chúng ta có thể trở về với thế giới cũ?

1
00:05:15,112-->00:05:21,113
Phải. Thiên đường Eden đã hoàn thành nhiệm vụ.

1
00:05:22,112-->00:05:26,113
Nhìn xung quanh tôi, Tohka và các cô gái khác 
đã tỉnh lại. Mặc dù họ có vẻ kiệt sức 
nhưng không ai bị thương.

1
00:05:27,112-->00:05:30,113
Mọi chuyện đã ổn rồi... Này!

1
00:05:36,112-->00:05:39,113
Rinne... cơ thể cậu... đang biến mất?

1
00:05:40,113-->00:05:50,113
Sự thật là tớ... sẽ không thể... 
sống cùng cậu... và về nhà với cậu...

1
00:05:51,113-->00:05:54,113
Nghĩa là sao? Cậu đã nói là 
nếu phong ấn cậu thì sẽ...

1
00:05:55,112-->00:06:07,113
Tớ đã nói dối... Xin lỗi cậu.
Tớ nghĩ... nếu tớ nói thật...
thì cậu sẽ không bao giờ hôn tớ...

1
00:06:08,112-->00:06:11,113
Đúng vậy! Đúng là vậy! Nhưng mà...

1
00:06:12,112-->00:06:22,113
Cậu thấy không? Tớ... tớ hiểu rõ về cậu, Shidou. 
Vì dù sao... tớ cũng là bạn thuở nhỏ của cậu.

1
00:06:22,112-->00:06:26,113
Đúng vậy! Cậu là bạn thuở nhỏ của tớ!
Vì vậy, đừng bỏ cuộc! Làm ơn đi!
Từ bây giờ chúng ta có thể sống cùng nhau rồi!

1
00:06:27,112-->00:06:39,113
Viễn cảnh "sống cùng nhau" đó không bao giờ
tồn tại phải không? Vì vậy "từ bây giờ" đó
không bao giờ có được.

1
00:06:40,112-->00:06:42,113
Cậu đang nói dối phải không?
Đó... đó không phải là sự thật!

1
00:06:43,112-->00:07:04,113
Shidou... cậu đã dạy tớ rất nhiều thứ.
Cậu đã dạy tớ... phải biết vượt qua nỗi buồn. 
Cậu đã dạy tớ... nếu tớ vượt qua được, 
tớ có thể thấy những điều hạnh phúc hơn.

1
00:07:05,112-->00:07:06,513
Đúng vậy!

1
00:07:07,512-->00:07:12,113
Được rồi... không cần phải nói gì cả.

1
00:07:13,112-->00:07:28,113
Hãy tha thứ cho tớ. Tớ chỉ cần...
được nhìn thấy khuôn mặt cậu thôi, Shidou.

1
00:07:29,112-->00:07:32,113
Haha... Với bộ mặt đầy nước mắt này
thì quá rõ rồi phải không?

1
00:07:33,112-->00:07:38,113
Đúng vậy Shidou. Cậu nói khi buồn,
người ta thường khóc.

1
00:07:38,512-->00:07:53,113
Đây là... thứ người ta gọi là... nước mắt.
Tớ chưa bao giờ thấy cho đến lúc này.

1
00:07:54,112-->00:07:57,113
Đúng vậy! Còn vô số điều cậu không biết đấy,
Rinne! Vì vậy cậu không thể... ra đi... lúc này.

1
00:07:58,112-->00:08:06,113
Nè Shidou... Cậu có thể lắng nghe tớ không?

1
00:08:07,112-->00:08:09,113
Ừ, tớ sẽ nghe mọi thứ. Gì vậy?

1
00:08:10,112-->00:08:21,113
Cậu biết không? Tớ rất yêu những ngày
sống chung với cậu.

1
00:08:22,112-->00:08:43,113
Bây giờ tớ nghĩ thiên đường của tớ có thể
là sai lầm. Nhưng... những ngày 
sống cùng cậu... tuy thật ngắn...
nhưng tớ thật sự hạnh phúc...

1
00:08:44,112-->00:08:47,113
Vậy nếu sau này chúng ta 
cùng sống với nhau thì sao?

1
00:08:49,112-->00:09:03,113
Vậy thì chắc chắn tớ rất vui. Tuy nhiên...
những cô gái khác chắc sẽ giận đấy.

1
00:09:04,112-->00:09:06,113
Haha ... Đúng vậy thật.

1
00:09:07,112-->00:09:16,113
Vì vậy, bây giờ các cô gái khác còn 
chưa nghe chúng ta, tớ muốn nói với cậu...

1
00:09:17,112-->00:09:19,113
Gì vậy? Tớ đang nghe đây.

1
00:09:20,112-->00:09:28,113
Cậu biết không? Tớ mãi mãi...

1
00:09:29,112-->00:09:34,113
yêu cậu, Shidou...

1
00:09:42,112-->00:09:46,113
Rin...ne? Rinne... Rinneeeee!!!!

1
00:09:47,112-->00:09:49,113
Khoảnh khắc đó, một cảm giác như cơ thể 
tôi đang bị không khí xâm nhập vào.

1
00:09:58,112-->00:10:01,113
Chúng tôi đã ra khỏi Tháp Tengu mới trước khi
nó sụp đổ. Chúng tôi nhận ra mình đang ở ngọn đồi.

1
00:10:02,112-->00:10:06,113
Tohka và mọi người được Fraxinus kiểm tra y tế. 
Kurumi biến mất, Origami đến AST xác nhận tình hình.

1
00:10:07,112-->00:10:10,113
Và cuối cùng... Tôi đã một mình.

1
00:10:12,112-->00:10:17,113
Cuối cùng ... Rinne đã biến mất.
Tất cả đều kết thúc. 
Cứ như một câu chuyện cổ tích.

1
00:10:18,112-->00:10:21,113
Nhưng tôi vẫn không hiểu.
Tại sao cô ấy tạo kết giới?
Tại sao cô ấy muốn giết tôi?

1
00:10:22,112-->00:10:25,113
Sự kết thúc này để lại rất nhiều câu hỏi.

1
00:10:27,112-->00:10:30,113
Cậu không sao chứ, Shin?

1
00:10:31,112-->00:10:34,113
Không ạ. Em không bị sao cả.

1
00:10:35,112-->00:10:44,113
Vậy à. Sẽ tốt hơn nếu chúng ta nói chuyện.
Sự việc này còn gì nữa không?

1
00:10:45,112-->00:10:47,113
Không ạ. Mọi người có phát hiện gì không?

1
00:10:48,112-->00:10:59,113
Có. Khi cậu ở trong kết giới của Ruler, 
tôi đã có vài kết quả phân tích.

1
00:11:00,112-->00:11:03,113
Thật không? Làm ơn nói cho em biết!
Kết giới đó là gì?

1
00:11:06,112-->00:11:09,113
Reine-san!

1
00:11:10,112-->00:11:20,113
Thông tin này có thể cậu sẽ không vui.
Cậu chắc là cậu có muốn nghe chứ?

1
00:11:22,112-->00:11:25,113
Biểu hiện của Reine-san như muốn cảnh báo tôi, 
trong một khoảnh khắc... tôi đã lo sợ. 

1
00:11:25,312-->00:11:27,513
Nhưng... tôi muốn hiểu những gì Rinne nghĩ,
những gì Rinne muốn. Ít nhất tôi cảm thấy
tôi có thể đến gần Rinne hơn.

1
00:11:28,512-->00:11:31,113
Chị nói đi, làm ơn.

1
00:11:32,112-->00:11:39,113
Nếu cậu đã sẵn sàng thì tôi sẽ nói.

1
00:11:40,112-->00:11:48,113
Đầu tiên, có vẻ như 
Ruler khác với các Tinh Linh khác.

1
00:11:49,112-->00:11:51,113
Khác với các Tinh Linh khác sao?

1
00:11:52,112-->00:12:00,113
Phải. Mặc dù cô ấy giống Tinh Linh 
nhưng hoàn toàn khác biệt.

1
00:12:01,112-->00:12:15,113
Tôi không thể nói chi tiết trừ phi tôi điều tra 
thêm. Nhưng cô ấy là tập hợp của
một lượng lớn năng lượng Tinh Linh có ý thức.

1
00:12:16,112-->00:12:18,113
Ý cô là sao?

1
00:12:19,112-->00:12:27,113
Cô ấy là một dạng năng lượng. Với Tinh Linh bình 
thường, khi họ bị phong ấn, họ vẫn giữ lại 
sức mạnh trong cơ thể. 

1
00:12:27,512-->00:12:31,113
Nhưng cô ấy chỉ là tập hợp sức mạnh 
Tinh Linh tạo ra khi xảy ra mất ổn định.

1
00:12:32,112-->00:12:35,113
Và khi phong ấn, cô ấy sẽ biến mất...

1
00:12:36,112-->00:12:39,113
Phải! Thật không may.

1
00:12:40,112-->00:12:43,113
Không sao. Vậy còn kết giới?

1
00:12:44,112-->00:12:47,513
Khi nghe về chuyện buồn vừa xảy ra. 
Tôi liền thay đổi chủ đề.

1
00:12:48,512-->00:12:59,113
Nếu tôi đúng, kết giới Eden xung quanh
thành phố Tengu có một sức mạnh khác bao phủ chúng.

1
00:13:00,112-->00:13:02,113
Sức mạnh đặc biệt... giống như 
Thiên Thần của mọi người?

1
00:13:03,112-->00:13:10,113
Phải. Sức mạnh rất lớn. Tuy nhiên
nó rất khác biệt.

1
00:13:11,112-->00:13:14,113
Vậy chính xác sức mạnh của kết giới đó là gì?

1
00:13:15,112-->00:13:21,113
Sức mạnh của Eden là...

1
00:13:22,112-->00:13:25,113
Sức mạnh tái tạo lại thế giới.

1
00:13:26,112-->00:13:28,113
Sức mạnh tái tạo lại thế giới?

1
00:13:29,112-->00:13:31,313
Nghĩa là sao? Một sức mạnh khổng lồ
như vậy mà không ai nhận ra cả.

1
00:13:32,112-->00:13:43,113
Chính xác là kết giới đó có thể tái tạo
thế giới ở trong đó cho đến khi 
cậu đạt được một kết quả khiến cậu hài lòng.

1
00:13:44,112-->00:13:52,113
Trong Thiên đường Eden, cậu nhớ lại
những ký ức cũ phải không?

1
00:13:53,112-->00:13:54,113
Vâng...

1
00:13:55,112-->00:13:57,113
Tôi cũng nhớ ra khi cô ấy nói
hay làm những hành động tương tự.

1
00:13:58,112-->00:14:01,113
Không chỉ có tôi. Tất cả mọi người cũng như vậy.

1
00:14:02,112-->00:14:06,113
Em đã nhớ lại nó. Em nhớ lại lúc
em nói chuyện với Tohka và Origami.
Chẳng lẽ là nó sao?

1
00:14:07,112-->00:14:12,113
Phải. Tất cả họ đều như vậy. Họ đều 
ở trong thế giới do Eden thay đổi.

1
00:14:12,512-->00:14:25,113
Họ đều nhận ra ký ức cũ
khi cậu cố gắng thay đổi thế giới.

1
00:14:28,512-->00:14:33,113
Tất cả họ đều vậy sao? Thật ra khi tôi
gặp nguy hiểm, tôi không thể làm gì nhưng
tôi đã nghĩ về họ Tất cả ký ức của tôi 
đều như một giấc mơ.

1
00:14:34,112-->00:14:38,113
Dù vậy, tôi vẫn không hiểu. Tôi biết 
sức mạnh của kết giới đó kỳ lạ, nhưng...

1
00:14:39,112-->00:14:42,113
Ruler... Rinne, mục đích cô ấy 
tạo kết giới là để làm gì?

1
00:14:43,112-->00:14:45,113
Đó là...

1
00:14:46,112-->00:14:55,113
Từ bây giờ, tôi sẽ nói những gì tôi đoán.
Tôi không chắc đó có phải sự thật hay không đâu.

1
00:14:56,112-->00:14:58,113
Vâng

1
00:14:59,112-->00:15:06,113
Sức mạnh tái tạo thế giới có một quy luật.

1
00:15:07,112-->00:15:09,113
Quy luật sao?

1
00:15:10,112-->00:15:29,113
Phải. Thế giới có thể tái tạo lại 
đúng nguồn gốc của nó. Tuy nhiên, 
nó cần một điều kiện bắt buộc.

1
00:15:30,112-->00:15:39,113
Trong ngắn hạn, nếu "cái gì đó" xảy ra, 
thế giới sẽ được tái tạo lại 
đúng như nguồn gốc của nó.

1
00:15:40,112-->00:15:42,513
"Cái gì đó" là cái gì?

1
00:15:43,512-->00:15:47,113
Vì lý do nào đó, tim tôi đập mạnh hơn.

1
00:15:48,112-->00:15:55,113
Điều kiện để tái tạo thế giới là...

1
00:15:56,112-->00:16:00,113
Shin, cái chết của cậu.

1
00:16:01,113-->00:16:03,113
Hả?

1
00:16:04,112-->00:16:06,113
Cái chết? Cái chết của tôi sao?

1
00:16:07,112-->00:16:09,113
Tôi không hiểu những gì Reine-san nói. 
Chẳng lẽ tôi đã...

1
00:16:16,112-->00:16:19,113
Đúng rồi! Lúc đó, từ lúc đó... từ lúc
tôi mất sức mạnh phục hồi của Kotori,
tôi đã chết khi sức mạnh của Tohka mất kiểm soát.

1
00:16:20,112-->00:16:24,113
Tôi đã làm lại mọi thứ nhiều lần.
Ký ức lưu lại và lại tiếp tục.

1
00:16:25,112-->00:16:28,113
Thế giới tái tạo lại khi em chết sao?

1
00:16:29,112-->00:16:36,513
Phải. Khoảnh khắc cậu chết, thế giới sẽ
quay trở lại lúc kết giới vừa được tạo.

1
00:16:37,112-->00:16:42,113
Shin, cậu là chìa khóa của thế giới này.

1
00:16:43,112-->00:16:46,113
Đợi đã! Em là chìa khóa sao?
Tại sao lại thế?

1
00:16:47,112-->00:16:53,113
Đầu tiên, chẳng phải Ruler cố giết em sao?
Hơn nữa, cô ấy còn khiến em 
mất sức mạnh Tinh Linh nữa.

1
00:16:54,112-->00:17:00,113
Sai rồi. Cậu đã hiểu sai rồi.

1
00:17:01,112-->00:17:03,113
Sai sao? Nghĩa là sao?

1
00:17:04,112-->00:17:10,113
Shin, cậu có nhớ ngày Tohka 
mất kiểm soát sức mạnh không?

1
00:17:11,112-->00:17:12,513
Ngày Tohka mất kiểm soát sức mạnh?

1
00:17:13,512-->00:17:22,113
Phải. Ngày mà Tohka mất kiểm soát
ở trên sân thượng. Cậu nhớ không?

1
00:17:23,112-->00:17:25,113
Mất kiểm soát trên sân thượng?

1
00:17:38,112-->00:17:41,113
Đúng rồi. Em đã ở đó!

1
00:17:42,112-->00:17:48,113
Phải. Eden có lẽ đã được tạo 
vào thời điểm đó.

1
00:17:49,112-->00:17:52,113
Sao... sao chị biết?

1
00:17:53,112-->00:17:58,113
Nếu không phải vào lúc đó
thì có lẽ cậu đã chết rồi.

1
00:17:59,112-->00:18:02,113
Em... em đã chết sao?

1
00:18:03,112-->00:18:11,513
Cậu không nhớ gì từ lúc đó trở đi phải không?
Từ lúc Tohka mất kiểm soát trở đi. 

1
00:18:12,512-->00:18:17,113
Vâng. Từ lúc đó em hoàn toàn khỏe mạnh.
Khi em kiểm tra trên Fraxinus, không có gì
bất thường cả. Không phải sao?

1
00:18:18,112-->00:18:29,113
Phải. không có gì bất thường cả.
Chuyện không có gì bất thường 
không thường xuyên xảy ra đâu.

1
00:18:30,112-->00:18:33,113
Kể cả Fraxinus cũng không biết.
Đó có thể là gì?

1
00:18:34,112-->00:18:43,113
Đây chỉ là giả thuyết của tôi,
nhưng hơn cả Tinh Linh, cậu là 
điều kiện gây biến động lớn nhất.

1
00:18:44,112-->00:18:46,113
Hở?

1
00:18:47,112-->00:18:57,113
Cuộc sống của cậu bắt đầu từ khi 
cậu gặp Tinh Linh. Những cuộc hẹn hò với
Tinh Linh làm giảm căng thẳng và áp lực.

1
00:18:57,512-->00:19:01,113
Một trận chiến diễn ra ngay trong 
cuộc sống của cậu.

1
00:19:02,112-->00:19:06,113
Khoan đã, Reine-san! Chị đang nói gì vậy?

1
00:19:07,112-->00:19:18,113
Lý do sức mạnh Tinh Linh quay trở về 
với họ có lẽ không hẳn do kết giới.

1
00:19:19,112-->00:19:30,113
Khả năng là vì tình trạng tinh thần
tụt xuống đột ngột dẫn đến 
hậu quả không lường trước được.

1
00:19:31,112-->00:19:33,113
Vậy là...

1
00:19:35,112-->00:19:39,113
Sức mạnh Tinh Linh đảo ngược
không phải là do kết giới.

1
00:19:40,112-->00:19:44,113
Kết giới của Rinne được tạo ra 
vào lúc Tohka mất kiểm soát.

1
00:19:45,112-->00:19:49,113
Sức mạnh tái tạo thế giới của Eden...
và nút bấm là cái chết của mình.

1
00:19:50,112-->00:19:53,113
Mọi thứ đều kết nối với nhau.

1
00:19:54,112-->00:19:56,113
Vậy là... Rinne...

1
00:19:58,112-->00:20:10,113
Tôi không biết Sonogami Rinne đến từ đâu,
hay cô ấy có liên kết gì với cậu.
Tuy nhiên, một điều tôi dám chắc là...

1
00:20:11,112-->00:20:14,113
Sức mạnh Tinh Linh thoát ra khỏi mình
và quay về với Tinh Linh. Dĩ nhiên là có cả
khả năng phục hồi của Kotori.

1
00:20:15,112-->00:20:20,113
Và khi đó, tôi hoàn toàn rất dễ chết khi
sống với Tohka, Kotori và Yoshino 
với sức mạnh của họ. Hơn nữa, 
Origami và Kurumi cũng rất nguy hiểm.

1
00:20:21,112-->00:20:25,113
Tôi vẫn nhớ không biết bao nhiêu lần 
tôi gặp nguy hiểm, chết đi 
và tái tạo lại thế giới.

1
00:20:29,112-->00:20:32,113
Rinne... Rinne không phải là lý do
khiến sức mạnh Tinh Linh đảo ngược...

1
00:20:33,112-->00:20:35,113
Cô ấy không nhốt chúng ta 
vì lợi ích bản thân. 

1
00:20:36,113-->00:20:38,113
Mọi thứ đều là vì tôi.

1
00:20:39,113-->00:20:42,113
Cô ấy làm việc chăm chỉ, chỉ để 
ngăn tôi không phải chết!

1
00:20:43,112-->00:20:45,113
Rinne...

1
00:20:46,112-->00:20:56,113
Đúng vậy. Sonogami Rinne 
được tạo ra là để bảo vệ cậu.

1
00:21:00,112-->00:21:04,113
Em... không biết gì cả... và em đã...
nói thế... với Rinne... Em... Em...

1
00:21:05,112-->00:21:10,113
Đừng khóc nữa, Shin

1
00:21:11,112-->00:21:13,113
Reine-san...

1
00:21:14,112-->00:21:32,113
Tôi nghĩ cô ấy không quan tâm những người
khác nghĩ gì. Nhưng Shin, riêng cậu,
đừng làm cô ấy buồn và thất vọng.

1
00:21:36,112-->00:21:53,113
Làm ơn đấy, Shin. Sonogami Rinne muốn 
bảo vệ cậu. Hối tiếc hay xin lỗi không làm 
cô ấy trở lại, nhưng lòng biết ơn 
sẽ khiến cô ấy vui hơn.

1
00:21:54,112-->00:21:57,113
Vâng...

1
00:21:58,112-->00:22:06,113
Cậu có vẻ bình tĩnh rồi. 
Giờ tôi đưa cậu cái này.

1
00:22:07,112-->00:22:09,113
Hở? Đây là...

1
00:22:10,112-->00:22:22,313
Thứ này rơi ra khi cậu thoát ra khỏi
Tháp Tengu mới. Cậu có biết cái gì không?

1
00:22:23,112-->00:22:25,313
Đây là thứ mà tôi đã đưa cho cô ấy.

1
00:22:28,112-->00:22:32,313
Tớ sẽ giữ kỹ nó.

1
00:22:37,112-->00:22:40,113
Reine-san, chị có thể cho em 
ở một mình được không?

1
00:22:45,112-->00:22:58,113
À, phải rồi. Kết giới Eden đã biến mất và
thế giới sẽ trở về dạng ban đầu.

1
00:22:59,112-->00:23:06,113
Nếu có điều gì cậu muốn làm thì 
hãy nhanh lên. Gặp lại sau nhé, Shin.

1
00:23:08,112-->00:23:10,113
Cảm ơn chị rất nhiều.

1
00:23:16,112-->00:23:19,113
Dù đây chỉ là giấc mơ, nhưng tôi
vẫn nhớ đã hẹn hò với Rinne ở đây.

1
00:23:20,112-->00:23:22,113
Khoảnh khắc đó sẽ không còn nữa.
Rinne đã ở bên mình ở đây.

1
00:23:23,112-->00:23:26,113
Tôi mở cái túi Reine-san đưa cho tôi.
Và tôi thấy cái chìa khỏa quen thuộc.

1
00:23:27,112-->00:23:31,113
Đúng...đây là bằng chứng Rinne là bạn thuở nhỏ
của tôi. Tôi không thể quên gương mặt của Rinne
khi tôi đưa chìa khóa cho cậu ấy. 
Cậu ấy ngạc nhiên, nhưng rất hạnh phúc.

1
00:23:32,112-->00:23:35,113
Cậu ấy đã rất hoảng hốt 
khi để quên chìa khóa ở nhà.

1
00:23:39,112-->00:23:41,113
Chẳng phải cậu đã nói cậu sẽ chăm sóc 
tớ sao, Rinne? Cậu sẽ nhận trách nhiệm đó mà!

1
00:23:43,112-->00:23:48,113
Nếu cậu không làm vậy, sẽ không còn ai gọi tớ
dậy mỗi sáng... không còn ai nấu ăn với tớ...
và tớ sẽ không nhìn thấy nụ cười của cậu nữa!

1
00:23:50,112-->00:23:53,513
Tại...sao? Cậu đã nói cậu là bạn thuở nhỏ 
của tớ, và cậu sẽ luôn ở bên tớ mà!

1
00:23:54,512-->00:24:00,113
Rinneeeeee!!!!!!

1
00:24:03,112-->00:24:15,113
Cảm ơn Shidou! Ở bên cậu, tớ thật sự rất hạnh phúc!

1
00:24:32,112-->00:24:41,113
Được rồi. Lớp chúng ta ngừng lại tại đây.
Giờ các em có thể về được rồi.

1
00:24:46,112-->00:24:49,113
Này Tohka, giờ chúng ta đi nhé?

1
00:24:50,112-->00:24:52,113
Ừ!

1
00:24:57,112-->00:24:59,112
Um... Anh cảm thấy có gì đó...

1
00:25:00,113-->00:25:04,113
Sao vậy Shidou?

1
00:25:05,112-->00:25:09,113
À... Có thể do trời lạnh thôi.
Quan trọng hơn là anh vui vì có em đi cùng.

1
00:25:10,112-->00:25:14,113
Xin lỗi Shidou... Em mừng vì anh không sao.

1
00:25:15,112-->00:25:18,113
Không, không cần phải lo cho anh...
Em vẫn kiểm soát được mình chứ?

1
00:25:19,112-->00:25:23,113
Gần đây, trên sân thượng, tôi phát hiện
Tohka bị mất kiểm soát sức mạnh. Tuy nhiên, 
dù tôi đã dùng tất cả sức lực của mình, 
nhưng tôi không thể thay đổi tình hình.

1
00:25:24,112-->00:25:27,113
Mặc dù sự việc xảy ra tại trường học nhưng
Kotori nói với tôi không có nạn nhân nào.
Tôi rất mừng vì không có ai bị thương.

1
00:25:32,112-->00:25:36,113
Thôi nào, đừng buồn như thế chứ.
Em cười lên xem nào.

1
00:25:37,112-->00:25:39,113
Ừ!

1
00:25:40,112-->00:25:43,113
Đúng rồi, đúng rồi. Phải như vậy chứ.

1
00:25:47,112-->00:25:50,113
Này Tohka.

1
00:25:51,112-->00:25:54,113
Sao vậy Shidou?

1
00:25:55,112-->00:25:58,113
Anh không biết... 
Em có cảm thấy gì lạ không?

1
00:25:59,112-->00:26:04,113
Có gì cơ? Em thấy bình thường mà.

1
00:26:05,112-->00:26:09,113
Không, không phải vậy. Tôi không biết 
diễn tả thế nào... Tôi cảm thấy một thứ
gì đó đã mất. Nhưng tại sao tôi cảm thấy chứ?

1
00:26:13,112-->00:26:17,113
Không có gì... Phải rồi, ở khu vực đối diện
nhà ga có Tháp Tengu đấy. Đó là một tòa nhà lớn.

1
00:26:18,112-->00:26:22,113
Có tòa nhà đó sao?

1
00:26:23,112-->00:26:27,113
Đúng vậy... Nếu em muốn thì chúng ta
đến đó đi. Chỗ đó cũng tiện đường đi về mà.

1
00:26:28,112-->00:26:31,113
Thật không?

1
00:26:32,112-->00:26:34,113
Ừ. Em muốn đi đến khi nào cũng được...

1
00:26:35,112-->00:26:40,113
Thế à! Vậy thì chúng ta đi thôi Shidou!

1
00:26:41,112-->00:26:44,113
Này! Chờ đã, Tohka!

1
00:26:45,112-->00:26:48,113
Thật là... Nhưng tại sao chứ?
Tôi cảm thấy như tôi quên mất một điều...
một điều rất quan trọng.

1
00:26:49,112-->00:26:52,113
Một cái gì đó rất quý giá... 
Và tôi không thể nhớ được...

1
00:26:53,112-->00:26:57,113
Này Shidou! Đi nhanh lên!

1
00:26:58,112-->00:27:00,112
Được rồi! Anh đến đây!

1
00:27:01,113-->00:27:03,113
Đúng vậy, tôi không nên nghĩ đến nó nữa.

1
00:27:04,112-->00:27:08,113
Chắc chắn một lúc nào đó tôi sẽ nhớ ra.
Nếu đó là một cái gì đó rất quan trọng 
với tôi, chắc chắn tôi sẽ nhớ ra...



1
00:29:29,112-->00:29:39,513
Tớ sẽ quên tất cả... tất cả... 
Như thể tớ chưa bao giờ xuất hiện.
Như thể chưa có gì xảy ra. 

1
00:29:40,112-->00:29:45,113
Sau khi tớ, một cơ thể kỳ lạ, 
biến mất khỏi thế giới... 
tất cả mọi thứ sẽ trở lại bình thường...

1
00:29:46,113-->00:29:54,113
Vậy là... vậy là tốt rồi, phải không Shidou?

1
00:30:00,112-->00:30:02,113
Cô là...?

1
00:30:03,112-->00:30:09,113
Cô đã hoàn thành... phần việc của cô rồi.

1
00:30:13,112-->00:30:18,113
Bây giờ cô hãy ngủ ngon nhé...
