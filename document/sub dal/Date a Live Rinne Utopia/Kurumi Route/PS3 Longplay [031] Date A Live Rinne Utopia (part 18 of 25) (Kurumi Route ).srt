﻿368
00:00:11,112 --> 00:00:17,113
Anh xin lỗi... 
Anh không muốn làm em lo lắng...

368
00:00:20,112 --> 00:00:29,113
Không là thế nào? 
Bây giờ anh không còn sức mạnh nữa!
Em nói với anh bao nhiêu lần rồi?

368
00:00:31,112 --> 00:00:36,113
Xin... xin lỗi... Chuyện đó, anh sai rồi.

368
00:00:38,112 --> 00:00:44,113
Nhưng dù sao, e rất mừng vì anh an toàn...

368
00:00:45,112 --> 00:00:48,113
Phải...

368
00:00:51,112 --> 00:00:58,113
Em sẽ kiểm tra tháp ngay lập tức. Thế nên...

368
00:01:00,112 --> 00:01:11,113
Vì những hành động của anh,
có khả năng Tinh Linh sẽ chạy mất.
Vậy nên lần tới hãy cho em biết.

368
00:01:13,112 --> 00:01:17,113
Được... được rồi... anh sẽ nhớ.

368
00:01:19,512 --> 00:01:32,113
Có vẻ như chuyện này có liên quan
với kết giới bao trùm thành phố Tengu...
Tất nhiên, vẫn chưa biết đó là gì.

368
00:01:35,112 --> 00:01:38,113
Đúng vậy... Trông cậy vào em đấy.

368
00:01:41,112 --> 00:01:55,113
Vâng. Đây là một chút tiến triển...
Hãy để việc này lại cho em.
Anh vẫn tiếp tục lo cho các Tinh Linh.

368
00:01:57,112 --> 00:02:00,113
Ừ, được rồi...

368
00:02:03,112 --> 00:02:18,113
Và em cũng nói nhiều về Kurumi rồi.
Kurumi rất nguy hiểm. 
Lần này anh không sao, những hãy cẩn thận hơn.

368
00:02:20,112 --> 00:02:23,113
Anh cũng sẽ nhớ điều này.

368
00:02:25,112 --> 00:02:31,113
Nhưng... anh không thể để em phát hiện ra Kurumi...

368
00:02:38,112 --> 00:02:42,113
Ngày 30 tháng 6





368
00:02:52,112 --> 00:02:54,113
Shidou...

368
00:03:02,112 --> 00:03:05,113
Shidou, dậy đi.

368
00:03:10,512 --> 00:03:15,513
Kotori? Có chuyện gì vậy? Vẫn còn sớm mà...

368
00:03:17,512 --> 00:03:24,113
Em muốn báo cáo. Nói chuyện với em
một chút trước khi ăn sáng đã.

368
00:03:26,112 --> 00:03:31,113
Được rồi. Chờ anh chút.

368
00:03:37,112 --> 00:03:44,113
Vậy em muốn báo cáo chuyện gì? 
Có phát hiện mới hay là... 
vụ tấn công tối qua?

368
00:03:46,112 --> 00:03:49,113
Là cả hai.

368
00:03:51,112 --> 00:03:54,113
Cả hai à?

368
00:03:57,112 --> 00:04:08,113
Khi đến gần di tích, một thiên sứ 
xuất hiện giống như muốn can thiệp. Phải vậy không?

368
00:04:10,112 --> 00:04:13,113
Đúng vậy.

368
00:04:15,112 --> 00:04:20,513
Sau đó, nó biến mất không dấu vết.

368
00:04:22,512 --> 00:04:28,113
Chắc chắn rồi... Vậy nó là thứ gì?
Nó là... Tinh Linh sao?

368
00:04:31,112 --> 00:04:42,113
Vẫn chưa có gì đảm bảo.
Nhưng theo những gì chứng kiến,
nó rất giống với Tinh Linh.

368
00:04:44,112 --> 00:04:47,113
Ý em là sao?

368
00:04:50,112 --> 00:05:07,113
Về sức mạnh, nó gần giống với Tinh Linh.
Dù vậy, nó chỉ là thực thể được tạo ra
từ Tinh Linh có ý chí tự do như Tohka.

368
00:05:08,112 --> 00:05:11,113
Hở?

368
00:05:14,112 --> 00:05:25,113
Giống như bộ não của anh như côn trùng,
anh vẫn có thể hành động tự do 
như một con người.

368
00:05:27,512 --> 00:05:31,113
So sánh kiểu gì vậy?

368
00:05:34,112 --> 00:05:46,113
Mà thôi. Tóm lại, nó chắc chắn
đã được tạo ra bởi sức mạnh Tinh Linh
dựa vào những gì anh mô tả.

368
00:05:48,112 --> 00:05:55,113
Nó giống như một "Golem" 
hoặc "Người bảo vệ" trong các trò chơi.

368
00:05:57,112 --> 00:06:02,113
Nó giống như một thực thể vậy.

368
00:06:03,512 --> 00:06:11,113
Shidou, anh có biết nó là gì
sau khi gặp tình huống như vậy không?

368
00:06:13,112 --> 00:06:16,113
Xin lỗi...

368
00:06:19,112 --> 00:06:28,113
Nếu anh nói đúng, có lẽ... nó là... 
một thứ có nhiệm vụ phòng thủ.

368
00:06:30,112 --> 00:06:32,113
Phòng thủ sao?

368
00:06:35,112 --> 00:06:42,113
Đó là tượng đài, thứ phát sáng vào thời điểm đó.

368
00:06:44,112 --> 00:06:50,113
Vậy à... Nhưng thứ đó tạo ra cái gì?

368
00:06:52,112 --> 00:07:02,113
Phải, em chỉ biết đến thế.
Nhưng ít nhất em chắc chắn 
nó có liên quan đến kết giới.

368
00:07:05,112 --> 00:07:14,113
Em đã kiểm tra sóng Tinh Linh
quanh tượng đài vào thời điểm anh bị tấn công.

368
00:07:16,112 --> 00:07:20,113
Vậy em đã phát hiện ra điều gì chưa?

368
00:07:22,112 --> 00:07:31,113
Dạng sóng đang yếu đi ở tháp Tengu...
Nó trùng với kết giới bao quanh thành phố Tengu.

368
00:07:39,512 --> 00:07:49,113
Nó có kích thước khác nhau,
nhưng em đã kiểm tra.
Kết quả là nó giống hệt nhau.

368
00:07:51,512 --> 00:07:55,113
2 sóng đó... giống nhau sao?

368
00:07:48,112 --> 00:08:04,113
Sự hiện diện của cả kết giới 
và người bảo vệ có liên quan đến câu hỏi đó.
Em chắc chắn chúng liên kết đến nhau.

368
00:08:04,512 --> 00:08:16,113
Thật dễ dàng để nói có khả năng 
cả di tích và người bảo vệ đều được tạo ra.

368
00:08:18,512 --> 00:08:28,113
Câu trả lời cho việc này là... 
các di tích phải là nền tảng để giữ kết giới.

368
00:08:30,112 --> 00:08:37,113
Trước đây em đã đề cập về di tích?
Vậy nếu anh có thể...

368
00:08:39,512 --> 00:08:49,113
Có lẽ vậy. Nhưng em e rằng nó không đơn giản.

368
00:08:51,112 --> 00:08:55,113
Ý của em là... không chỉ có một...?

368
00:08:57,512 --> 00:09:10,113
Đúng vậy. Ở nơi nào đó khắp Tengu
cũng có những thứ tương tự như thế.

368
00:09:12,112 --> 00:09:19,113
Vậy là giờ chúng ta phải tìm những cái khác.
Đây mới chỉ là mở đầu.

368
00:09:21,112 --> 00:09:33,113
Phải. Vẫn còn một chặng đường dài để đi.
Nhưng điều này cũng tùy thuộc vào anh đấy.
Anh hãy tiếp tục hẹn hò.

368
00:09:35,112 --> 00:09:39,113
Ừ, anh hiểu...

368
00:09:41,512 --> 00:09:52,113
Tất nhiên, nếu anh biết điều gì,
hãy báo cáo ngay.
Nhưng đừng làm thế nữa, được không?

368
00:09:54,112 --> 00:09:57,113
Ừ, anh biết...

368
00:09:59,512 --> 00:10:06,113
Giờ chỉ còn ý định của kẻ thù.
Em ước gì mình biết được...

368
00:10:08,512 --> 00:10:14,113
Kẻ thù à? Là người bảo vệ sao?

368
00:10:16,112 --> 00:10:32,113
Em đang nói về kẻ chủ mưu.
Lý do khiến chúng ta bị nhốt trong kết giới.
Không, chuyện này chưa kết thúc! 
Anh phải làm những gì cần làm!

368
00:10:34,112 --> 00:10:42,113
Uh... Anh xin lỗi. Anh sẽ hợp tác 
càng nhiều càng tốt. Anh muốn làm sớm. 
Mọi người đang vật lộn với chuyện này...

368
00:10:44,512 --> 00:10:53,113
Một lần nữa, anh giờ không còn 
khả năng hồi phục. 
Tuyệt đối không được làm thế nữa.

368
00:10:56,112 --> 00:10:59,113
Ừ, anh hứa.

368
00:11:02,112 --> 00:11:08,113
Thực sự... Em lo lắng cho anh
đến không ngủ được đấy...

368
00:11:10,112 --> 00:11:12,513
Hở?

368
00:11:15,512 --> 00:11:19,113
Không có gì đâu!

368
00:11:22,112 --> 00:11:24,113
Ừ... ừ...








368
00:11:37,112 --> 00:11:38,513
Đi đến Lớp Học?
1) Đồng ý
2) Không đồng ý

368
00:11:42,112 --> 00:11:47,113
Hay là thử lên sân thượng đi... 
Kurumi có lẽ đang ở trên đó...

368
00:11:49,512 --> 00:11:53,113
Mà đợi đã... Mình đang nói gì vậy?

368
00:11:54,512 --> 00:11:58,513
Nghe cứ như câu nói của con gái cuồng yêu vậy... 
Xấu hổ quá đi...

368
00:11:59,112 --> 00:12:02,113
Hở?

368
00:12:29,512 --> 00:12:34,113
Cuối cùng cũng tìm thấy em rồi..

368
00:12:37,112 --> 00:12:42,113
Hở? Chuyện gì vậy, Tama-chan... 
À không, Sensei...?

368
00:12:44,512 --> 00:12:48,113
Biết là Itsuka-kun đang nghỉ ngơi 
nên cô sẽ tìm cách khác 

368
00:12:48,512 --> 00:12:53,113
nhưng hạn chót của bài viết 
lớp Khoa Học Xã Hội là hôm nay đấy. Em biết chứ?

368
00:12:55,112 --> 00:12:57,113
Ah...

368
00:13:00,112 --> 00:13:05,113
Em là người duy nhất chưa nộp bài đấy.

368
00:13:07,112 --> 00:13:13,513
Sensei... Cô có thể đợi em 
một chút được không? Em sẽ quay lại ngay...

368
00:13:15,512 --> 00:13:22,513
Không được! Cô sẽ không để em thoát khỏi vụ này đâu!

368
00:13:24,512 --> 00:13:33,113
Xin em hãy nộp trong hôm nay đi! 
Cô sẽ đợi ở phòng giáo viên... Em phải đến đó!

368
00:13:35,112 --> 00:13:38,113
Haaa...

368
00:13:41,112 --> 00:13:44,113
Không còn cách nào khác...
Đành phải làm vậy.

368
00:13:50,112 --> 00:13:57,113
Rồi, em làm tốt lắm. Lần sau đừng quên nữa nhé?

368
00:13:59,112 --> 00:14:05,113
Vâng ạ, xin lỗi cô. Vậy thì em xin phép cô!

368
00:14:16,112 --> 00:14:20,113
Hah... Đã trễ thế này rồi à...

368
00:14:21,512 --> 00:14:27,113
Mặt trời sắp lặn rồi... 
Có vẻ hôm này sẽ không có cuộc hẹn nào cả...

368
00:14:31,112 --> 00:14:38,113
Nếu cậu không phiền, tớ có thể giúp cậu bây giờ chứ?

368
00:14:40,112 --> 00:14:45,113
Kurumi!? Vậy là cậu đã ở đây quanh trường cả ngày hôm nay à!?

368
00:14:47,512 --> 00:14:55,113
Chúc một ngày tốt lành, Shidou-san. 
Tối hôm quả thật là vui nhỉ.

368
00:14:58,112 --> 00:15:05,113
Ừ, em gái tớ đã phát cáu vì việc đó đấy. 
Mà tất nhiên là nhờ có cậu mà tớ được an toàn. Cảm ơn cậu.

368
00:15:08,112 --> 00:15:11,113
Kiểm tra lại lời thoại

368
00:15:13,112 --> 00:15:20,113
Mà nhân tiện... Lúc trước tớ đã nói là cậu không nên 
đi loanh quanh trường rồi mà. Cậu sẽ gặp rắc rối đấy.

368
00:15:23,112 --> 00:15:29,113
Ara... Nhưng tớ đã  cố tránh những cuộc chạm mặt rồi mà.

368
00:15:32,112 --> 00:15:44,113
Thật sự thì tớ đã muốn gặp cậu sớm hơn... 
Tớ phải kiên nhẫn rồi đấy!

368
00:15:46,112 --> 00:15:49,113
Vậy... vậy à?

368
00:15:52,112 --> 00:15:58,113
Vậy thì... Cậu có muốn hẹn hò với tớ không?

368
00:16:00,112 --> 00:16:04,113
Nhưng mà... Không phải vì cậu yêu tớ đấy chứ?

368
00:16:06,112 --> 00:16:15,113
Ai biết được. Tất cả là tùy thuộc vào cậu đấy.

368
00:16:17,112 --> 00:16:23,113
Ừm... Nhưng bây giờ cũng trễ rồi thì biết đi đâu đây...?

368
00:16:24,512 --> 00:16:31,113
Tớ không phiền nếu "làm luôn" ở đây đâu

368
00:16:32,112 --> 00:16:36,113
Hở? "Ở đây" nghĩa là... trong phòng học này ư?

368
00:16:38,512 --> 00:16:47,113
Phải. Không tuyệt sao? 
Tớ sẽ được làm bạn cùng lớp với cậu.

368
00:16:49,112 --> 00:16:55,113
Không đâu. Cậu đã là bạn cùng lớp của tớ rồi mà. 
Dù đó chỉ là một thời gian ngắn...

368
00:16:57,112 --> 00:17:03,113
Đừng bận tâm đến những chi tiết nhỏ nhặt chứ.

368
00:17:05,112 --> 00:17:17,113
Tớ cũng hơi hối tiếc về chuyện đó đấy. 
Dù sao thì tất cả sẽ ổn nếu tớ có thể 
làm bạn cùng lớp với cậu và hiểu hơn về nhau.

368
00:17:19,112 --> 00:17:22,113
Nếu vậy sao chúng ta không thử đi?

368
00:17:24,112 --> 00:17:27,113
Sao cơ?

368
00:17:29,112 --> 00:17:37,113
Khi chỉ có hai chúng ta ở đây... 
Hãy giả vờ là vậy đi, nếu cậu cảm thấy ổn.

368
00:17:38,512 --> 00:17:48,113
Nghe vậy... Tớ cảm thấy vui lắm.

368
00:17:49,512 --> 00:18:02,113
Tớ có cần phải giới thiệu lại không? 
Tớ là Tokisaki Kurumi. Rất mong được giúp đỡ.

368
00:18:03,112 --> 00:18:10,113
Are. Không phải giống như lúc trước sao? 
Tiếp đến cậu sẽ nói " Tớ là một tinh linh "

368
00:18:12,112 --> 00:18:19,113
Đó là một ấn tượng khá là lạ so với điều mà 
một học sinh bình thường có thể nói nhỉ.

368
00:18:20,512 --> 00:18:25,113
T... Tớ hiểu... Đúng là vậy.

368
00:18:37,112 --> 00:18:42,113
Ano... Shidou-san.

368
00:18:43,512 --> 00:18:47,113
Gì vậy?

368
00:18:49,112 --> 00:18:55,113
Bạn cùng lớp thì phải làm gì với nhau đây?

368
00:18:57,112 --> 00:19:01,113
Ờ thì... Tớ nghĩ là...?

368
00:19:03,112 --> 00:19:09,113
Thiệt là... Đến cậu cũng không biết à.

368
00:19:11,112 --> 00:19:15,513
Đó là vì tớ chưa bao giờ nghĩ đến việc này cả.

368
00:19:17,112 --> 00:19:22,113
Hay là chúng ta hãy làm những việc mà mọi người hay làm ở trường nhỉ?

368
00:19:24,112 --> 00:19:32,113
Đúng rồi nhỉ... Vậy thì việc này thì sao?





368
00:19:37,512 --> 00:19:41,113
Shidou-san?

368
00:19:43,112 --> 00:19:47,113
V-Vâng... Kurumi-sensei?

368
00:19:48,512 --> 00:19:53,113
Ở chỗ này em làm sai rồi nè.

368
00:19:55,112 --> 00:20:00,113
Ahh! X... Xin lỗi cô...

368
00:20:02,112 --> 00:20:07,113
Không được đâu đó!

368
00:20:15,112 --> 00:20:20,113
Gần quá! Mặt cậu ấy gần quá!

368
00:20:21,112 --> 00:20:27,113
Và em lại lam sai nữa rồi nè.

368
00:20:29,112 --> 00:20:31,113
Hể...?

368
00:20:34,112 --> 00:20:40,113
Khi chỉ có hai chúng ta... 
Thì em phải gọi cô là "Kurumi" chứ.

368
00:20:42,512 --> 00:20:46,113
Ku... Kurumi... Sensei...

368
00:20:53,512 --> 00:20:58,113
N... Ngực của cô... Em có cảm nhận nó kìa!

368
00:20:59,112 --> 00:21:05,113
Sai rồi. Em đang "chạm" nó đấy!

368
00:21:12,112 --> 00:21:24,113
Nè, chúng ta vẫn chưa xong đâu nhỉ? 
Em phải bỏ việc dùng từ "Sensei" đi chứ... 
Nhanh lên, nhanh lên...

368
00:21:26,112 --> 00:21:29,113
K... Kurumi...

368
00:21:31,112 --> 00:21:39,113
Tốt lắm. Bây giờ thì cô cho em 
một phần thưởng nho nho nhé... Thế này thì sao?

368
00:21:41,512 --> 00:21:46,113
Đ... Đợi đã...!? Đừng có cởi đồ!

368
00:21:50,512 --> 00:21:57,113
Thiệt là... Sao lại đừng khi chúng ta 
đến đoạn hay nhất chứ...

368
00:21:58,512 --> 00:22:04,113
D... Dù sao thì, Kurumi... 
Đây không phải là kiểu "Bạn cùng lớp" cậu muốn chứ?

368
00:22:06,512 --> 00:22:12,113
Đúng nhỉ. Giờ cậu mới nói à?

368
00:22:14,112 --> 00:22:22,113
Bên cạnh đó, Kurumi... 
Cậu học cách dựng tình huống đó ở đâu vậy?

368
00:22:23,112 --> 00:22:32,113
Ở đâu ư...? Ờ thì...  Là cuốn sách cậu đọc 
khi cậu ở trong tiệm sách ấy.

368
00:22:34,512 --> 00:22:42,113
Tiệm sách ư!? Cậu nên nói gì đó khi thấy tớ chứ... 
Không, ý tớ không phải vậy! Urgh....

368
00:22:43,512 --> 00:22:48,113
Việc này thật sự làm cậu sốc à?

368
00:22:50,112 --> 00:22:55,113
Sẽ thật tốt nếu cậu có thể xóa phần ký ức đó giúp tớ đấy.

368
00:22:57,112 --> 00:23:02,113
Tớ sẽ để nó tự trôi qua vậy...

368
00:23:10,112 --> 00:23:14,113
Sao vậy?

368
00:23:15,512 --> 00:23:20,113
Không có gì... Tớ chỉ đang nghĩ là "Phải như thế này mới đúng chứ "

368
00:23:27,512 --> 00:23:36,113
Làm những việc vô nghĩa, vòng vo các thứ...
Cùng vui vẻ về việc gì đó, đó mới là "Bạn cùng lớp" đấy.

368
00:23:37,112 --> 00:23:41,113
Ra là vậy à.

368
00:23:43,112 --> 00:23:48,113
Ừ... là vậy đấy. Cậu không nghĩ vậy sao?

368
00:23:50,512 --> 00:23:55,113
Ah... Bây giờ tớ mà không về là có chuyện cho xem.

368
00:23:57,112 --> 00:24:04,113
Vậy ư...? Tớ muốn ở với cậu thêm ít lâu nữa cơ... 

368
00:24:06,512 --> 00:24:12,113
Ah... Um, xin lỗi cậu... Nếu tớ không lãng phí nhiều thời gian thì...

368
00:24:13,512 --> 00:24:20,113
Không, không sao đâu. Thật ra...

368
00:24:21,112 --> 00:24:24,113
Hở?

368
00:24:26,112 --> 00:24:39,113
Cảm ơn cậu vì ngày hôm nay. Tớ thật sự vui đấy. 
Chỉ trong khoảng thời gian ngắn, 
tớ đã có thể trở lại làm bạn cùng lớp với cậu rồi...

368
00:24:41,112 --> 00:24:44,113
À, phải...

368
00:24:46,112 --> 00:24:52,113
Bạn bè là thứ nhất thiết phải có nhỉ.

368
00:24:54,112 --> 00:24:58,113
Ừ... ừ. Họ thật tuyệt.

368
00:25:00,112 --> 00:25:07,113
Vậy thì... Tạm biệt nhé, Shidou-san.

368
00:25:09,112 --> 00:25:12,113
Ừ, hẹn gặp cậu sau.

368
00:25:18,112 --> 00:25:23,113
Kurumi, cậu ấy có vẻ rất vui... 
Như một người con gái bình thường vậy...

368
00:25:25,112 --> 00:25:31,113
Tuy nhiên... Kurumi có bí mật của cậu ấy... 
Đó có thể là thứ xa lạ với mình.

368
00:25:32,512 --> 00:25:39,113
Mình sẽ không biết cho đến khi cậu ấy nói sự thật. 
Nhưng nếu có thể, mình muốn cậu ấy giải thích 
từ quan điểm của cậu ấy.

368
00:25:41,112 --> 00:25:49,113
Mình muốn có thể cùng nhau cười đùa với Kurumi. 
Hình ảnh đó với mình, thì nó là bằng chứng rằng 
mình không gặp nguy hiểm, như những gì Kotori đã nói.





368
00:25:52,112 --> 00:25:55,113
Ủa, Kurumi?

368
00:25:58,112 --> 00:26:01,113
Shidou-san.

368
00:26:03,512 --> 00:26:07,113
Cậu đang làm gì ở đây?

368
00:26:09,512 --> 00:26:16,113
Tớ chỉ ngồi suy nghĩ thôi.

368
00:26:18,112 --> 00:26:21,113
Shidou-san?

368
00:26:23,112 --> 00:26:26,113
Gì vậy?

368
00:26:28,112 --> 00:26:35,113
Shidou-san... cậu nghĩ gì về tớ?

368
00:26:36,512 --> 00:26:39,113
Hở?

368
00:26:42,112 --> 00:26:47,113
Cậu nghe tớ hỏi chứ?

368
00:26:49,112 --> 00:26:53,113
Sao đột nhiên lại thế này?

368
00:26:59,512 --> 00:27:04,113
Tớ... tớ... cậu...

368
00:27:05,112 --> 00:27:10,113
1) Tớ muốn phong ấn cậu
2) "Kẻ thù", tớ nghĩ vậy
3) ...tớ không biết nữa.

368
00:27:11,112 --> 00:27:17,113
Thành thật mà nói, tớ không biết
cậu là người thế nào...

368
00:27:18,112 --> 00:27:22,113
Bây giờ cũng vậy... Tớ vẫn chưa hết nghi ngờ cậu...

368
00:27:30,112 --> 00:27:36,113
Nhưng tớ tin cậu không phải người xấu.

368
00:27:37,512 --> 00:27:43,113
Vì vậy... tớ không biết nữa. 
Đây là cảm nghĩ của tớ hiện giờ.

368
00:27:45,112 --> 00:27:50,113
Vậy cậu vẫn sẽ không cho tớ biết thêm về cậu sao?

368
00:27:52,512 --> 00:27:55,113
Chuyện đó...

368
00:27:58,112 --> 00:28:05,113
Bây giờ, tớ sẽ không ép cậu.
Tớ sẽ đợi đến khi cậu muốn tự bày tỏ.

368
00:28:07,112 --> 00:28:14,113
Giờ tớ chỉ có thể nói với cậu một điều.

368
00:28:16,112 --> 00:28:25,113
Một ngôi đền ở ngoại ô.
Tớ sẽ cho cậu xem một thứ.

368
00:28:27,112 --> 00:28:30,113
Được rồi.

368
00:28:32,112 --> 00:28:37,113
Vâng, giờ tớ phải đi rồi.

368
00:28:39,112 --> 00:28:44,113
Ừ. Đi một mình có ổn không đấy?

368
00:28:45,512 --> 00:28:54,113
Cậu hỏi lạ nhỉ.
Chẳng phải tớ là Tinh Linh sao?

368
00:28:56,512 --> 00:29:00,113
Dù là Tinh Linh, cậu vẫn là một cô gái.

368
00:29:03,112 --> 00:29:13,113
Cậu không đối xử như hôm qua nữa.
Tớ cảm thấy rất vui.

368
00:29:15,512 --> 00:29:19,113
Kurumi... tớ tin cậu.




368
00:29:27,112 --> 00:29:34,113
Sau đó, tôi nghĩ về việc Kurumi 
sẽ mở lòng với tôi như thế nào.
Không có câu trả lời chính đáng nào được đưa ra cả.

368
00:29:36,112 --> 00:29:40,113
Sau tất cả, cậu không thể làm 
bất cứ điều gì sao...?

368
00:29:42,112 --> 00:29:47,113
Tôi bây giờ, chỉ có cách để giúp Kurumi.

368
00:29:49,112 --> 00:29:52,113
...Đó là hẹn hò

Không thể để tình cảm nguội lạnh.
Mình muốn hỗ trợ cho trái tim Kotori

368
00:29:54,112 --> 00:29:59,113
Tôi sẽ tìm hiểu những gì về Kurumi mà tôi không biết.

368
00:30:01,112 --> 00:30:09,113
Cô ấy có vẻ trầm tư... 
hay thậm chí là trông cô đơn...
Kurumi cũng chỉ là một cô gái bình thường.

368
00:30:11,112 --> 00:30:14,113
Được rồi, hãy đến ngôi đền vào ngày mai.

368
00:30:17,112 --> 00:30:20,113
Ai vậy?

368
00:30:25,512 --> 00:30:29,113
Xin lỗi nhé, là tớ đây.

368
00:30:31,112 --> 00:30:36,113
Rinne? Sao cậu... chuyện gì đã xảy ra?

368
00:30:38,512 --> 00:30:43,113
Shidou này, tớ muốn nghe một chuyện.

368
00:30:45,512 --> 00:30:48,513
Cậu muốn nghe chuyện gì?

368
00:30:50,512 --> 00:30:58,113
Vâng, tớ muốn nghe nó.
Shidou... là chuyện cậu đang đối mặt.

368
00:31:01,112 --> 00:31:04,113
Ý cậu là sao?

368
00:31:06,512 --> 00:31:12,113
Cậu sẽ là người... hỗ trợ cho Tokisaki-san.

368
00:31:13,512 --> 00:31:17,113
Ừ, nhất định rồi.

Phải. Tớ không muốn nhìn thấy ai buồn.
Tôi muốn làm mọi người hạnh phúc.

368
00:31:20,112 --> 00:31:26,113
Vậy à? Vậy đó là câu trả lời của cậu.

368
00:31:28,512 --> 00:31:32,113
Phải. Tớ sẽ làm thế.

368
00:31:45,112 --> 00:31:51,113
Này Shidou, cậu có thể hứa không?

368
00:31:53,112 --> 00:31:57,113
Hứa sao? Hứa gì cơ?

368
00:32:00,112 --> 00:32:11,113
Trong tương lai, cậu sẽ yêu quý Tokisaki-san.
Bất kể chuyện gì xảy ra... cậu sẽ luôn như vậy.

368
00:32:13,512 --> 00:32:19,113
Sao tự nhiên cậu nói vậy?
Dù cậu không nói thì...

368
00:32:21,512 --> 00:32:28,113
Xin cậu đấy, tớ muốn cậu hứa với tớ.
Hãy nghĩ về Tokisaki-san...

368
00:32:28,512 --> 00:32:35,113
Nếu không, một ngày nào đó cậu sẽ rời xa cô ấy.

368
00:32:38,112 --> 00:32:41,113
Rinne...

368
00:32:43,512 --> 00:32:47,113
Làm ơn...

368
00:32:48,512 --> 00:32:56,113
Được, tớ hứa. Là một người đàn ông, 
đã nói là phải giữ lời.
Tớ sẽ luôn quan tâm đến Kotori.

368
00:32:58,512 --> 00:33:04,113
Tớ tin cậu sẽ làm được nếu cậu cố gắng.

368
00:33:06,112 --> 00:33:10,113
Haha, tớ cũng nghĩ vậy.

368
00:33:12,112 --> 00:33:24,113
Tớ tin cậu... Shidou.
Đừng quên... Đừng bao giờ quên đấy.

368
00:33:26,512 --> 00:33:30,113
Ừ, tất nhiên rồi.

368
00:33:32,512 --> 00:33:39,513
Nếu cậu thất hứa, cậu sẽ phải làm lại đấy nhé.

368
00:33:41,512 --> 00:33:44,113
Ừ, tớ có nghe rồi!

368
00:33:47,512 --> 00:33:51,113
Thế à? Cuối cùng nó đã được thực hiện rồi.

368
00:33:54,112 --> 00:33:59,113
Cái gì? Thực hiện gì cơ?

368
00:34:01,112 --> 00:34:04,113
Tớ xin lỗi.

368
00:34:07,512 --> 00:34:10,113
Nhưng mà...

368
00:34:18,112 --> 00:34:25,113
Sẽ ổn thôi. Nếu cậu cố gắng...
mọi thứ sẽ ổn thôi.

368
00:34:39,112 --> 00:34:42,513
Cái... cái gì vậy?

368
00:34:44,512 --> 00:34:49,113
Này Rinne... Cậu vừa làm gì...

368
00:34:50,512 --> 00:34:55,513
Này... cậu đâu rồi? Rinne về rồi à?

368
00:34:57,112 --> 00:35:02,113
Đã quá muộn rồi. Đi ngủ thôi.

368
00:35:15,112 --> 00:35:21,113
Ngày 1 tháng 7





368
00:35:25,112 --> 00:35:29,113
Ngôi đền này có vẻ ổn.

368
00:35:31,112 --> 00:35:37,113
Kể cả bây giờ, mình vẫn nên cẩn thận sao?
Kotori cũng nói thế...

368
00:35:39,112 --> 00:35:46,113
Nhưng bằng cách nào đó, mình tự quyết định
đến đây một mình... Sao thế nhỉ? Sao không có ai ở đây?

368
00:35:48,112 --> 00:35:51,513
Kurumi có vẻ chưa tới...

368
00:35:53,112 --> 00:35:57,113
Shidou-san?

368
00:35:59,112 --> 00:36:06,113
Vậy là, cuối cùng cậu cũng tới.

368
00:36:08,112 --> 00:36:11,113
Kurumi...

368
00:36:13,512 --> 00:36:22,113
Tiếc thật đấy... có lẽ, tôi nghĩ rằng 
khi chúng ta thấy mình như thế tôi phải từ bỏ.

368
00:36:24,112 --> 00:36:28,113
Hở? Ý cậu là sao?

368
00:36:30,112 --> 00:36:36,113
À, không... không có gì quan trọng đâu.

368
00:36:38,112 --> 00:36:44,113
Quan trọng hơn... Hôm nay là buổi hẹn hò
của chúng ta phải không?

368
00:36:46,512 --> 00:36:50,113
Ừ, đúng vậy.

368
00:36:52,112 --> 00:36:58,113
Trong trường hợp đó... cậu có muốn 
chúng ta hẹn hò ở ngôi đền này không?

368
00:37:00,512 --> 00:37:03,113
Ở đây luôn sao?

368
00:37:06,112 --> 00:37:12,113
Si... siento que aqui nos podemos relajar.

Vâng ... Tớ thấy chúng ta có thể thư giãn ở đây.

368
00:37:14,112 --> 00:37:20,113
Ừ. đúng vậy. Nhưng chúng ta 
không thể ăn uống hay đi dạo ở đây.

368
00:37:23,112 --> 00:37:29,113
Nhưng đây là nơi rất yên bình cho 2 chúng ta.

368
00:37:31,512 --> 00:37:34,513
Hở?

368
00:37:38,112 --> 00:37:42,113
Thôi quên nó đi.

368
00:37:44,112 --> 00:37:48,113
Kurumi, cậu muốn ở đây
có phải vì cậu muốn nói gì đó phải không?

368
00:37:51,112 --> 00:37:55,113
Đó là bí mật.

368
00:37:58,512 --> 00:38:01,113
Bí mật sao?

368
00:38:03,112 --> 00:38:09,513
Một cô gái có bí mật là những gì họ thích.

368
00:38:11,512 --> 00:38:16,113
Cậu có thích điều đó không, Shidou-san?

368
00:38:18,112 --> 00:38:22,113
Đã, là nó như thế?

368
00:38:24,112 --> 00:38:27,113
Phải.

368
00:38:28,712 --> 00:38:33,513
Vâng, Kurumi chắc chắn 
có một sự hài hước ma quỷ.

368
00:38:35,512 --> 00:38:41,113
Đó có phải là ở dưới cùng của đây không?

Eso esta al fondo de aqui?

368
00:38:42,512 --> 00:38:47,113
Wow này, nó thực sự làm cho tôi rất hạnh phúc.

Wow esto, me hace realmente muy feliz.

368
00:38:49,512 --> 00:38:52,113
Làm cậu hạnh phúc sao?

Te hace feliz?

368
00:38:56,112 --> 00:39:03,513
Phải, tớ nghĩ điều này thật thú vị...
Thật vui khi được ở bên bạn Kurumi và tôi muốn nói chuyện với bạn.

Bueno, creo que relamente esto es divertido...
Es divertido estar contigo Kurumi y me gusta hablar contigo.

368
00:39:06,112 --> 00:39:09,513
Tớ hiểu rồi.

368
00:39:11,512 --> 00:39:15,113
Còn cậu thì sao, Kurumi?

368
00:39:17,112 --> 00:39:23,113
Đó là bí mật.

368
00:39:25,112 --> 00:39:29,113
Vâng, những bí mật của các cô gái là từ ngữ khá tiện lợi...

368
00:39:31,112 --> 00:39:37,113
Phải, tớ cũng nghĩ vậy.

368
00:39:39,512 --> 00:39:46,113
Quan trọng nhất là Shidou-san sẽ không bận tâm
nghe câu chuyện này chứ?

Mas importante Shidou-san no te importaria
escuchar esta historia?

368
00:39:48,112 --> 00:39:52,113
Câu chuyện gì?

Esa historia?

368
00:39:54,112 --> 00:40:03,513
Si, cerca de tu casa, de la escuela y de la gente Shidou-san...

Phải, gần nhà bạn, trường học và người Shidou-san ...

368
00:40:05,512 --> 00:40:11,113
Tôi không nghĩ nó có dầu mỡ nhưng vẫn ...

Pues no creo que sea grasioso pero aun asi...

368
00:40:13,112 --> 00:40:22,113
Không, không sao đâu. Shidou tôi muốn dạy cho bạn rất nhiều thứ.

No, esta bien. Shidou quiero ensenarte muchas cosas.

368
00:40:24,112 --> 00:40:28,113
Không sao, tôi hiểu, rằng bạn sẽ dạy tôi.

Esta bien, lo entiendo, que me ensenararas.

368
00:40:48,112 --> 00:40:32,113
Cảm ơn cậu rất nhiều.

368
00:40:48,112 --> 00:40:32,113
Vâng ... bây giờ, nếu bạn muốn chúng ta nói về trường học.

Bueno... por ahora, si quieres hablamos sobre la escuela.

368
00:40:48,112 --> 00:40:32,113
Si me parece bien.

368
00:40:48,112 --> 00:40:32,113


368
00:40:48,112 --> 00:40:32,113


368
00:40:48,112 --> 00:40:32,113


368
00:40:48,112 --> 00:40:32,113


368
00:40:48,112 --> 00:40:32,113


368
00:40:48,112 --> 00:40:32,113


368
00:40:48,112 --> 00:40:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:41:48,112 --> 00:41:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113


368
00:42:48,112 --> 00:42:32,113