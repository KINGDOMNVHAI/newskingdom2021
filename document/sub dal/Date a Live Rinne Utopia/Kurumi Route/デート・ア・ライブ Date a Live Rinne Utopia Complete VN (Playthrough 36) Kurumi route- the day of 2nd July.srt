﻿0
00:00:04,912 --> 00:00:05,913
Sáng rồi...

1
00:00:06,112 --> 00:00:07,713
Nhưng... mình đã thực sự tỉnh dậy chưa?

2
00:00:07,912 --> 00:00:09,513
Chuyện xảy ra hôm qua... giống như một giấc mơ...

3
00:00:12,312 --> 00:00:17,113
Tớ sẽ nói với cậu tất cả những gì tớ biết.

4
00:00:17,312 --> 00:00:30,713
Đây là sức mạnh giống tháp Tenguu mới.
Một sự tồn tại siêu việt với sức mạnh to lớn.
Một người như thế đã mở ra vụ việc này.

5
00:00:30,912 --> 00:00:39,713
Tớ là một Tinh Linh chưa bị phong ấn... 
Cậu giải cứu tớ khỏi "Nightmare" 
có thể là chìa khóa kích hoạt
một sức mạnh chưa từng thấy.

6
00:00:39,912 --> 00:00:40,113
Tình huống vừa rồi tương tự như lần trước 
chúng ta tự cứu mình. Điều gì đó kỳ lạ
đang xảy ra trong thế giới này.

7
00:00:40,312 --> 00:00:51,513
Không phải vì "Nightmare" đã có được sức mạnh,
mà kỳ lạ là cậu đã cứu tớ với sức mạnh đó.

8
00:00:51,712 --> 00:00:57,913
Và rồi... nếu tớ ở một mình, 
tớ tin tớ có thể gây ảnh hưởng nhiều hơn.

11
00:00:59,112 --> 00:01:00,713
Không... những lời của Kurumi 
không phải là giải pháp đúng...

12
00:01:00,912 --> 00:01:02,113
Chuyện đó... nó thực sự đã xảy ra...

13
00:01:02,312 --> 00:01:03,513
Nhưng có một điều...

14
00:01:03,712 --> 00:01:05,113
Một điều chắc chắn mình vẫn nhớ, đó là...

15
00:01:06,512 --> 00:01:15,513
Tớ không nói dối cậu, Shidou-san.
Nếu có Shidou-san với sức mạnh của cậu, tớ có thể sống sót.

16
00:01:15,912 --> 00:01:27,113
Nhưng... giờ thì khác.
Từ khi cậu bước vào cuộc đời tớ, 
cậu đã rất quan trọng với tớ.

17
00:01:27,913 --> 00:01:31,913
Shidou-san, cảm ơn cậu rất nhiều.

18
00:01:32,112 --> 00:01:35,713
Tớ rất vui và tớ yêu cậu rất nhiều...

19
00:01:38,712 --> 00:01:39,713
Nó là thật...

20
00:01:39,913 --> 00:01:41,313
Kurumi mà mình đã ở cùng những ngày qua 
chắc chắn có tồn tại!

21
00:01:41,512 --> 00:01:43,513
Nhưng... một khi cơ thể biến mất, 
Kurumi cũng sẽ biến mất...

22
00:01:43,712 --> 00:01:44,913
Mình nên làm gì... Mình nên làm gì...

23
00:01:45,112 --> 00:01:45,913
Chẳng có cách nào cả.

24
00:01:46,112 --> 00:01:48,513
Nhưng... mình chỉ muốn gặp Kurumi ngay bây giờ.
Thật là tàn nhẫn khi nói lời tạm biệt như thế!

25
00:01:48,712 --> 00:01:50,513
Mình phải đi tìm Kurumi.
Nếu gặp lại cô ấy, mình sẽ...





25
00:01:56,512 --> 00:01:57,713
Kurumi! Này, Kurumi!

25
00:01:57,912 --> 00:01:59,513
Cậu có ở đây không? Kurumi, ra đây đi!

25
00:02:03,912 --> 00:02:05,513
Không có ở đây... mình đi kiếm chỗ khác...



25
00:02:13,912 --> 00:02:15,513
Cô ấy luôn đến đây. Hôm nay thì sao?

25
00:02:15,712 --> 00:02:17,113
Kurumi! Cậu có ở đây không!

25
00:02:17,312 --> 00:02:18,313
Kurumi, không ở đây sao?

25
00:02:22,712 --> 00:02:23,313
Vẫn không thấy đâu...

25
00:02:23,512 --> 00:02:25,113
Nếu vậy... chỉ còn cách tìm đại thôi!

25
00:02:30,312 --> 00:02:31,113
Kurumi!

25
00:02:31,312 --> 00:02:32,313
Kurumi, cậu ở đâu?

25
00:02:37,712 --> 00:02:39,113
Ra đây đi, Kurumi!

25
00:02:39,312 --> 00:02:40,513
Cậu có ở đây không? Kurumi!

25
00:02:46,112 --> 00:02:47,513
Mình đã tìm khắp nơi, nhưng...

25
00:02:47,912 --> 00:02:48,313
Vẫn không được, mình mệt quá rồi...

25
00:02:49,112 --> 00:02:49,913
Kurumi...

25
00:02:50,112 --> 00:02:52,113
A... Shidou-san!

25
00:02:52,312 --> 00:02:53,113
Hở...

25
00:02:57,112 --> 00:02:58,513
Kurumi... Sao cậu lại ở đây...?

25
00:02:59,312 --> 00:03:03,113
Tớ nghĩ mình không nên gặp cậu bây giờ, nhưng...

25
00:03:03,312 --> 00:03:07,513
Không hiểu sao, những bước chân lại hướng về đây.

25
00:03:08,512 --> 00:03:09,713
Hở, từ khi nào...?

25
00:03:10,112 --> 00:03:13,113
Từ sáng nay rồi...

25
00:03:13,312 --> 00:03:15,313
Đúng rồi, tớ xin lỗi. Đã tối thế này rồi...

25
00:03:15,912 --> 00:03:19,513
Không lẽ... cậu đang tìm tớ sao?

25
00:03:19,912 --> 00:03:22,313
Phải... Nhưng giờ thì tốt rồi.
Thật mừng khi gặp lại cậu...

25
00:03:25,112 --> 00:03:26,113
Cậu đã... tìm tớ sao?

25
00:03:26,912 --> 00:03:35,913
Phải... tớ nghĩ tớ cũng như các Tinh Linh khác...
Tớ muốn gặp lại Shidou-san...

26
00:03:36,112 --> 00:03:36,913
Kurumi... 

26
00:03:40,112 --> 00:03:42,113
Shidou, Kurumi: A... ano...

26
00:03:42,312 --> 00:03:43,113
A...

26
00:03:44,112 --> 00:03:46,113
Sao... sao vậy?

26
00:03:46,312 --> 00:03:47,313
À không... Cậu nói trước đi...

26
00:03:47,512 --> 00:03:51,113
Thôi... Cậu nói đi...

26
00:03:53,712 --> 00:03:55,113
Vậy... vào nhà nhé?

26
00:03:55,512 --> 00:03:57,113
Vâng...







26
00:04:02,112 --> 00:04:03,313
Mời cậu.

26
00:04:03,912 --> 00:04:08,713
Cảm ơn cậu... Trà rất ngon.

26
00:04:08,912 --> 00:04:11,113
Thật à? Trà cũng như người ta làm thôi mà.

26
00:04:11,312 --> 00:04:13,113
Tớ không nghĩ thế.

27
00:04:14,112 --> 00:04:19,913
Với cậu, nó có gì đó rất đặc biệt.

28
00:04:20,112 --> 00:04:21,113
À vâng...

29
00:04:21,912 --> 00:04:23,113
Shidou-san...

30
00:04:23,312 --> 00:04:23,913
Hở?

31
00:04:24,112 --> 00:04:28,913
Tớ đã nghĩ mình sẽ không gặp cậu nữa.

32
00:04:29,112 --> 00:04:39,513
Nếu gặp cậu và biến mất trước mặt cậu,
tớ không thể chịu đựng được.

33
00:04:41,113 --> 00:04:49,713
Nhưng tớ không thế.
Tôi không thể không gặp cậu...

34
00:04:49,913 --> 00:04:51,113
Cậu đã lầm. Tớ cũng muốn gặp cậu.

35
00:04:51,512 --> 00:04:56,313
Shidou-san, tớ có một thỉnh cầu.

36
00:04:56,912 --> 00:04:59,113
Nếu cậu là một cô gái, đừng thỉnh cầu.
Tớ sẽ lắng nghe bất cứ điều gì.

37
00:04:59,312 --> 00:05:08,913
Có lẽ việc này sẽ khiến cậu 
có trải nghiệm còn đau đớn hơn bây giờ.
Tuy nhiên... cậu có thể lắng nghe tớ không?

38
00:05:09,112 --> 00:05:09,913
Ừ.

39
00:05:10,112 --> 00:05:18,513
Tớ muốn cậu ở lại với tớ cho đến khi tớ biến mất.

40
00:05:18,912 --> 00:05:19,913
Kurumi...

41
00:05:20,312 --> 00:05:24,913
Tớ biết... cậu sẽ nói không mà...

42
00:05:25,112 --> 00:05:26,113
Sao lại không được chứ?

43
00:05:28,512 --> 00:05:30,313
Sáng nay khi thức dậy, tớ nghĩ về chuyện hôm qua...
Ngay sau đó, tớ đi tìm cậu.

39
00:05:30,512 --> 00:05:33,313
Tớ đã tìm kiếm suốt cả ngày , nhưng không thấy cậu.
Không như mọi khi, tớ không thể gặp cậu 
ở bất cứ nơi nào tớ thường gặp cậu...

40
00:05:33,512 --> 00:05:36,313
Thành thật mà nói,
dường như những giọt nước mắt đã chảy ra. 

41
00:05:36,912 --> 00:05:38,113
Shidou-san...

42
00:05:38,712 --> 00:05:41,113
Dù là một ký ức đau buồn...
tớ vẫn muốn ở bên cậu thật lâu.

43
00:05:42,112 --> 00:05:43,513
Shidou-san...







39
00:05:46,512 --> 00:05:48,513
Còn rất nhiều thứ cậu muốn biết.
Cậu đã nói thế đúng không?

40
00:05:48,713 --> 00:05:53,513
Thực sự, cậu vẫn nhớ điều đó.

41
00:05:53,913 --> 00:05:54,913
Hãy tiếp tục... ở bên nhau nhé... 

42
00:05:55,113 --> 00:05:57,913
Cảm ơn cậu rất nhiều.

43
00:05:58,113 --> 00:06:03,713
Tớ... tớ có thể ở lại với cậu không?

44
00:06:03,913 --> 00:06:05,513
Ừ... Không sao. Tớ cũng muốn ở bên cậu.





45
00:06:07,912 --> 00:06:11,513
Vậy thì, chứng minh đi.

46
00:06:11,712 --> 00:06:12,513
Chứng minh à?

41
00:06:12,712 --> 00:06:16,513
Tớ còn một yêu cầu nữa.

46
00:06:16,712 --> 00:06:18,913
Được, cứ nói đi. Tớ sẽ giúp cậu.

47
00:06:19,113 --> 00:06:21,113
Cậu chắc chắn chắn chứ?

48
00:06:21,112 --> 00:06:22,313
Gì... gì cơ?

49
00:06:27,112 --> 00:06:28,113
Hở? Ku... Kurumi? Sao lại tắt đèn?

52
00:06:28,312 --> 00:06:35,713
Đúng là ngốc. 
Tuy nhiên, đó lại là điểm thu hút của Shidou-san.

51
00:06:35,912 --> 00:06:37,113
Um... nghĩa là sao...?

52
00:06:38,312 --> 00:06:39,113
Cậu... cậu làm gì vậy?

53
00:06:39,312 --> 00:06:40,713
Shidou-san...





54
00:06:43,112 --> 00:06:44,313
Ku... Kurumi! Cậu... cậu đang làm gì?

55
00:06:44,512 --> 00:06:49,113
Shidou-san, Hãy chắc chắn rằng 
cậu đang cảm thấy chúng.

56
00:06:49,312 --> 00:06:56,113
Và rồi, hãy đánh dấu nó.
Đó là bằng chứng mà tớ muốn thấy...

57
00:06:56,312 --> 00:06:57,313
Kurumi... tớ...

58
00:07:01,112 --> 00:07:01,913
A...

59
00:07:02,112 --> 00:07:03,113
Shidou-san...

60
00:07:03,312 --> 00:07:05,113
Tớ xin lỗi, đợi chút... A...

61
00:07:05,312 --> 00:07:06,113
Kotori à?

62
00:07:06,312 --> 00:07:08,513
Một cái gì đó, tôi cảm thấy kỳ lạ. 
Một cảm giác bồn chồn, khó chịu...?

63
00:07:10,112 --> 00:07:21,513
Trong tương lai, cậu sẽ yêu quý Tokisaki-san.
Bất kể chuyện gì xảy ra... cậu sẽ luôn như vậy.

64
00:07:23,112 --> 00:07:24,513
Đột nhiên, những lời của Rinne xuất hiện trong tâm trí tôi. 

64
00:07:24,712 --> 00:07:26,713
Bây giờ, trước mắt tôi, 
cuối cùng Kurumi và tôi cũng đã hiểu nhau.
Đây là khoảnh khắc hạnh phúc 
không thể đánh đổi bất cứ điều gì trên đời.

65
00:07:26,912 --> 00:07:30,113
Tôi... tôi muốn giữ Kurumi lại.
Tôi muốn khiến cô ấy cảm thấy mình đang ở đây.
Chúng tôi muốn ở bên nhau không rời!

66
00:07:30,312 --> 00:07:34,113
Sẽ ổn thôi nếu em ấy gọi lại sau...
Có lẽ đó là việc quan trọng cần báo.
Tôi cảm thấy mình sẽ mất một cái gì đó quý giá
nếu tôi nhận cuộc gọi.

67
00:07:36,512 --> 00:07:37,513
Shidou-san?

68
00:07:38,112 --> 00:07:38,913
Ừ...

69
00:07:39,112 --> 00:07:40,513
Vâng, tất nhiên tôi sẽ không trốn tránh.
Tôi, cuộc gọi này...

70
00:07:41,512 --> 00:07:46,113
1) Nghe điện thoại
2) Không nghe điện thoại.

71
00:07:47,112 --> 00:07:51,113
Xin lỗi em, Kotori.
Bây giờ... Anh không thể nghe điện thoại.

72
00:07:55,112 --> 00:07:57,713
Khi tôi không biết khi nào Kurumi sẽ biến mất,
Tôi muốn trân trọng [hiện tại] này với Kurumi...

73
00:07:57,912 --> 00:07:58,913
Vậy là được rồi.

74
00:07:59,912 --> 00:08:02,513
Như vậy có sao không?

75
00:08:02,713 --> 00:08:04,113
Không sao đâu. Sẽ không có chuyện gì lớn đâu.

76
00:08:04,312 --> 00:08:05,313
Shidou-san...

77
00:08:05,712 --> 00:08:06,913
Ừ, bây giờ không còn gì cản trở nữa.

78
00:08:07,112 --> 00:08:12,513
Phải rồi... Vậy chúng ta bắt đầu nhé?

79
00:08:12,912 --> 00:08:13,913
Ừ. Tớ yêu cậu... Kurumi.

80
00:08:14,112 --> 00:08:17,513
Tôi cảm nhận thân nhiệt của Kurumi.
Sao nó ấm áp thế? Dù là kẻ thủ 
hay tất cả những gì Kurumi đã làm cho đến nay,
tôi cảm thấy tất cả bọn họ đều tốt cả.

81
00:08:17,712 --> 00:08:20,913
Giờ tôi chỉ muốn bảo vệ Kurumi... cô gái này.
Dù không có ai đứng về phía cô gái này, 
tôi vẫn sẽ ở bên cô ấy.
Tôi muốn làm cho cô ấy mỉm cười.

82
00:08:21,112 --> 00:08:22,513
Shidou-san...

83
00:08:27,712 --> 00:08:29,113
Và như thế, tôi và Kurumi đã là của nhau.

84
00:08:29,312 --> 00:08:31,913
Mối quan hệ giữa tôi và Kurumi đã thay đổi.
Như những người yêu nhau trong khoảng thời gian hữu hạn.

85
00:08:32,112 --> 00:08:35,113
Nhưng Kurumi này là một thực thể kỳ lạ.
Sớm muộn gì cô cũng sẽ biến mất vào một ngày nào đó.

86
00:08:35,312 --> 00:08:38,113
Tôi biết, nhưng như thế mới đúng là
một con người bình thường.
Tình yêu cũng vậy, luôn mãi trong tâm trí 
mỗi người qua thời gian.

87
00:08:38,312 --> 00:08:40,913
Trong một thời gian hữu hạn, 
nếu chúng ta ở bên nhau, mọi thứ đều được chia sẻ.

88
00:08:41,112 --> 00:08:43,113
Chiến đấu với thời gian... Đó là cuộc chiến không hồi kết.
Nhưng với Kurumi, tôi muốn làm một điều gì đó.

89
00:08:43,312 --> 00:08:44,513
Bây giờ, đừng bỏ lỡ hạnh phúc hiện tại của bạn.





368
00:11:01,112 --> 00:11:03,513
Tốt nghiệp, mình không tin được 
thời gian trôi nhanh thế.

368
00:11:03,912 --> 00:11:06,313
Trong năm đầu trung học 
tôi không có quan hệ với con gái,
Ngay cả Tonomachi cũng nghi ngờ tôi
và đó là một thảm họa.

368
00:11:06,712 --> 00:11:09,113
Đến năm hai, tôi gặp Kurumi. Lúc đầu tôi bị cô ấy đe dọa,
nhưng bây giờ cô ấy là người quan trọng nhất trong đời tôi.

368
00:11:09,313 --> 00:11:12,113
Những chuyện xảy ra với Kurumi thật đáng kinh ngạc 
và tôi nhận ra mình đã học hết năm ba rồi tốt nghiệp.

368
00:11:12,312 --> 00:11:15,113
Anh thích nơi này nhỉ, Shidou-san.

368
00:11:15,312 --> 00:11:16,313
Kurumi.

368
00:11:16,912 --> 00:11:24,713
Em đoán anh ở đây vì nơi này 
lưu rất nhiều kỷ niệm của chúng ta.

368
00:11:24,912 --> 00:11:25,913
Đúng vậy.

368
00:11:26,912 --> 00:11:29,313
Không, sau đó Kurumi không biến mất.
Cô ấy ở bên cạnh tôi đến bây giờ.

368
00:11:29,512 --> 00:11:32,313
Vào ngày đó, Kurumi nói với tôi rằng 
cô sẽ biến mất sau đêm đó.
Nhưng rồi cô ấy vẫn ở bên tôi.

368
00:11:32,512 --> 00:11:35,113
Kurumi không biến mất,
Không ai có thể giải thích, 
cũng như tôi không biết tại sao.

368
00:11:35,312 --> 00:11:38,113
Tôi bỏ lại sự nghi ngờ đó.
Đó không phải chuyện quan trọng.

368
00:11:38,312 --> 00:11:40,513
Tôi không quan tâm dù đó là một phép lạ.
Bây giờ, Kurumi đang ở bên tôi.
Chỉ vậy thôi là đủ rồi.

368
00:11:43,312 --> 00:11:45,313
Anh đang nghĩ gì vậy?

368
00:11:45,512 --> 00:11:46,713
Anh đang nghĩ về em đấy, Kurumi.

368
00:11:47,112 --> 00:11:52,913
Ý anh là, Shidou-san,
giống như mọi khi, đúng không?

368
00:11:53,112 --> 00:11:55,113
Kurumi, em không tin anh chỉ nghĩ về em sao?

368
00:11:55,512 --> 00:11:58,113
Ara, em có sai không?

368
00:11:58,512 --> 00:12:00,513
Phải, em không sai về điều đó.

368
00:12:00,712 --> 00:12:02,113
Thật là...

368
00:12:02,312 --> 00:12:03,713
Hahaha...

368
00:12:06,912 --> 00:12:16,113
Shidou-san, em rất hạnh phúc.
Em cảm thấy sợ nếu phải kết thúc.

368
00:12:16,312 --> 00:12:17,513
Khoan đã, em nói gì vậy, Kurumi?

368
00:12:20,112 --> 00:12:28,513
Hôm nay anh sẽ nói lời tạm biệt với bộ đồng phục này phải không?
Vậy nhân dịp này, hãy chia sẻ lời tạm biệt đó với em.

368
00:12:28,912 --> 00:12:29,913
Th... thật sao?

368
00:12:30,113 --> 00:12:34,113
Phải. Anh không muốn cởi bộ đồng phục này sao?

368
00:12:34,312 --> 00:12:35,113
Không... cái này...

368
00:12:35,312 --> 00:12:40,113
Anh sẽ nhận được niềm hạnh phúc của em tốt hơn.

368
00:12:41,112 --> 00:12:42,113
À... được rồi.

368
00:12:42,512 --> 00:12:45,113
Thành thật mà nói, tôi đã cảm nhận được
niềm vui của cô ấy rồi. Nhưng điều này cũng không tệ...

368
00:12:45,312 --> 00:12:48,113
Tôi sẽ sống và tận ưởng từng khoảnh khắc hạnh phúc 
vì một ngày nào đó, nó có thể biến mất.

368
00:12:48,312 --> 00:12:49,913
Một ngày nào đó, hạnh phúc sẽ biến mất.
Và tôi sẽ không hối hận hay bỏ lỡ bất cứ điều gì.







91
00:13:03,112 --> 00:13:04,113
1) Nghe điện thoại
2) Không nghe điện thoại.

92
00:13:05,112 --> 00:13:09,513
Kurumi. Xin lỗi, tớ phải nghe điện thoại...

93
00:13:10,112 --> 00:13:12,113
Điện thoại sao?

94
00:13:12,512 --> 00:13:15,513
Ừ... Tớ sẽ xong ngay.

95
00:13:17,112 --> 00:13:20,113
Đợi đã, anh đang làm gì thế?

96
00:13:21,112 --> 00:13:23,113
À, ờ... anh xin lỗi.

97
00:13:24,112 --> 00:13:28,113
Đây là tình huống khẩn cấp.
Bọn em đã tìm ra chỗ của kẻ thù rồi.

98
00:13:29,112 --> 00:13:31,113
Kẻ thù sao?

99
00:13:31,512 --> 00:13:41,113
Em nghĩ tháp Tengu Mới chính là 
nguồn sức mạnh của kết giới.

100
00:13:39,112 --> 00:13:44,513
Tháp Tengu Mới sao? 
Khoan đã! Em đang nói gì vậy?

101
00:13:45,112 --> 00:13:52,113
Anh vẫn chưa già phải không?
Anh hỏi ngớ ngẩn gì vậy?

102
00:13:52,512 --> 00:13:54,113
Hở?

103
00:14:03,112 --> 00:14:05,113
Này, anh ổn chứ?

104
00:14:05,512 --> 00:14:11,513
À... anh xin lỗi. 
Thực sự chuyện gì đang xảy ra?
Những gì em nói là thật sao?

105
00:14:12,112 --> 00:14:18,113
Vâng, đó là sự thật.
Bây giờ Kurumi có ở đó không?

106
00:14:19,112 --> 00:14:20,113
Có đây...

107
00:14:21,112 --> 00:14:24,113
Anh cho em nói chuyện với Kurumi một chút.

108
00:14:24,512 --> 00:14:26,513
Hở?

109
00:14:27,112 --> 00:14:39,513
Để chiến đấu với kẻ thù này, anh cần sức mạnh Tinh Linh.
Vì Kurumi không bị ảnh hưởng bởi phong ấn,
để cô ấy chiến đấu sẽ tốt hơn.

110
00:14:40,112 --> 00:14:41,513
Nhưng... nhưng mà...

111
00:14:42,112 --> 00:14:52,513
Phải, không có gì đảm bảo cô ấy sẽ chiến đấu.
Đó là quyết định của riêng cô ấy.
Vậy nên em sẽ nói chuyện. Đưa cô ấy điện thoại đi.

108
00:14:53,112 --> 00:14:57,513
Kurumi, Kotori có chuyện muốn nói với cậu.

109
00:14:59,112 --> 00:15:00,513
Được rồi.

110
00:15:02,112 --> 00:15:09,113
Alo. Đúng... được... ừ...

111
00:15:10,112 --> 00:15:22,113
Hiểu rồi. được... ừ... 
Nhưng đây có gọi là một "cuộc hẹn hò" không?

112
00:15:23,112 --> 00:15:31,113
Hết cách rồi nhỉ. Vậy chào nhé.

108
00:15:33,512 --> 00:15:35,113
Kurumi?

109
00:15:36,112 --> 00:15:46,113
Shidou-san... Tớ sẽ đi với cậu.
Việc này rất quan trọng.
Tớ không thể để cậu đi một mình.

110
00:15:47,112 --> 00:15:49,113
Thế có ổn không?

111
00:15:50,112 --> 00:15:58,113
Vậy cậu định đi một mình sao?
Hơn nữa, đây là dịp tớ chuộc lỗi.

112
00:15:59,112 --> 00:16:00,113
Được rồi...

112
00:16:01,112 --> 00:16:05,513
Ừ, chuẩn bị tinh thần đi.

113
00:16:06,112 --> 00:16:08,113
Được!






114
00:16:14,112 --> 00:16:18,513
Tháp Tenguu... tớ không nghĩ đây là
nơi ẩn náu của kẻ thù.

1
00:16:19,112 --> 00:16:25,113
Đây là trung tâm của thành phố...
Rất hợp làm nơi ẩn náu.

1
00:16:26,112 --> 00:16:29,513
Phải... chúng ta có nên vào đó không, Kurumi?

1
00:16:30,112 --> 00:16:32,513
Có chứ, Shidou-san.

1
00:16:45,112 --> 00:16:47,513
Cô là... người trong giấc mơ... của tôi?

1
00:16:48,112 --> 00:16:50,113
Tinh Linh à?

1
00:16:51,112 --> 00:16:55,113
Không ai chịu "kết thúc" cả...

1
00:16:56,112 --> 00:16:58,113
Đó là Tinh Linh sao?

368
00:16:59,112 --> 00:17:06,513
Tôi muốn các cậu ở trong 
"giấc mơ hạnh phúc" mà tôi tạo ra.

368
00:17:07,112 --> 00:17:10,113
Cô nói gì...?

368
00:17:12,112 --> 00:17:19,113
Shidou-san, lùi lại!
Sức mạnh này, chính là thứ đã truyền cho tớ.

368
00:17:20,112 --> 00:17:22,113
Là cô ta sao?

368
00:17:24,112 --> 00:17:36,113
Tôi là người cai trị thế giới này. 
Người cai quản Eden.
Cậu có thể gọi tôi là Ruler.

368
00:17:37,112 --> 00:17:44,113
Nhưng... cậu biết cũng chẳng để làm gì...
Cậu sẽ quên thôi.

368
00:17:48,112 --> 00:17:51,113
Shidou-san, lùi lại!

368
00:17:56,112 --> 00:18:05,113
Tôi sẽ vứt bỏ "sự sống được cứu rỗi" này.
Nó không đáng được giúp đỡ.

368
00:18:20,112 --> 00:18:22,113
Kurumiiiiii!!!!!!

368
00:18:23,112 --> 00:18:26,113
Shidou-san... Tớ không thể...

368
00:18:29,512 --> 00:18:31,513
Tạm biệt.

68
00:18:42,512 --> 00:18:48,113
Kurumi? KURUMIIIIIII!!!!!!
KHỐN KIẾP... VẬY LÀ SAO?

368
00:18:49,112 --> 00:19:01,113
Chúc ngủ ngon, Itsuka Shidou. 
Lần sau, chắc chắn cậu sẽ có "giấc mơ hạnh phúc".

368
00:19:10,512 --> 00:19:17,513
Cậu đã hứa với tôi... cậu sẽ yêu quý Kurumi...


