﻿368
00:00:02,712 --> 00:00:04,313
Ngôi đền này có vẻ ổn.

368
00:00:04,513 --> 00:00:06,713
Kể cả bây giờ, mình vẫn nên cẩn thận sao?
Kotori cũng nói thế...

368
00:00:06,912 --> 00:00:09,513
Nhưng bằng cách nào đó, mình tự quyết định
đến đây một mình... Sao thế nhỉ? Sao không có ai ở đây?

368
00:00:09,712 --> 00:00:10,913
Kurumi có vẻ chưa tới...

368
00:00:11,112 --> 00:00:13,113
Shidou-san?

368
00:00:14,912 --> 00:00:18,113
Vậy là, cuối cùng cậu cũng tới.

368
00:00:18,312 --> 00:00:19,913
Kurumi...

368
00:00:20,112 --> 00:00:26,713
Tiếc thật đấy... tớ đã mong 
cậu vui mừng khi gặp tớ.

368
00:00:26,913 --> 00:00:28,113
Hở? Ý cậu là sao?

368
00:00:28,713 --> 00:00:32,513
À, không... không có gì đâu.

368
00:00:33,112 --> 00:00:37,713
Quan trọng hơn... Hôm nay là buổi hẹn hò
của chúng ta phải không?

368
00:00:37,912 --> 00:00:38,913
Ừ, đúng vậy.

368
00:00:39,112 --> 00:00:43,713
Nếu vậy... cậu có muốn 
chúng ta hẹn hò ở ngôi đền này không?

368
00:00:43,913 --> 00:00:45,113
Ở đây luôn sao?

368
00:00:45,512 --> 00:00:49,913
Phải... chúng ta có thể thư giãn ở đây.

368
00:00:50,112 --> 00:00:52,113
Đúng vậy. Nhưng chúng ta 
không thể ăn uống hay đi dạo ở đây.

368
00:00:52,512 --> 00:00:57,713
Nhưng đây là nơi rất yên bình cho đôi ta.

368
00:00:57,912 --> 00:00:58,713
Hở?

368
00:00:59,112 --> 00:01:02,712
Thôi quên nó đi.

368
00:01:02,912 --> 00:01:05,112
Kurumi, cậu muốn ở đây
có phải vì cậu muốn nói gì đó phải không?

368
00:01:05,512 --> 00:01:07,912
Đó là bí mật.

368
00:01:08,112 --> 00:01:08,912
Bí mật sao?

368
00:01:09,112 --> 00:01:13,713
Một cô gái luôn có bí mật của riêng mình.

368
00:01:13,913 --> 00:01:16,913
Cậu có thích điều đó không, Shidou-san?

368
00:01:17,112 --> 00:01:18,513
Vậy... vậy sao?

368
00:01:18,713 --> 00:01:19,713
Phải.

368
00:01:19,913 --> 00:01:21,713
Ừ, dù sao đó cũng là tính hài hước bí ẩn của cậu.

368
00:01:21,912 --> 00:01:25,113
Đó có phải là điểm xấu không?

368
00:01:25,312 --> 00:01:27,613
Ngược lại, nó làm tớ thấy vui.

368
00:01:27,712 --> 00:01:29,913
Làm cậu vui sao?

368
00:01:30,112 --> 00:01:33,713
Phải, tớ nghĩ điều này thật thú vị...
Thật vui khi được ở bên cậu và trò chuyện với cậu.

368
00:01:33,913 --> 00:01:35,513
Ra là vậy.

368
00:01:35,712 --> 00:01:36,912
Còn cậu thì sao, Kurumi?

368
00:01:38,112 --> 00:01:41,113
Đó là bí mật.

368
00:01:41,312 --> 00:01:42,713
Vâng, "bí mật của con gái" là một câu khá tiện lợi...

368
00:01:42,913 --> 00:01:46,513
Phải, tớ cũng nghĩ vậy.

368
00:01:47,312 --> 00:01:52,313
Quan trọng nhất là Shidou-san 
vẫn sẽ hướng dẫn tớ chứ?

368
00:01:52,512 --> 00:01:53,913
Hướng dẫn sao?

368
00:01:54,112 --> 00:02:01,913
Phải. Gia đình, trường học và người thân của cậu...

368
00:02:02,112 --> 00:02:04,513
Tớ không phiền gì đâu, nhưng...

368
00:02:04,712 --> 00:02:11,513
Không, không sao đâu. 
Rất nhiều thứ tớ muốn được cậu hướng dẫn.

368
00:02:11,712 --> 00:02:13,513
Không sao, tớ hiểu. Tớ sẽ hướng dẫn cậu.

368
00:02:13,712 --> 00:02:15,313
Cảm ơn cậu rất nhiều.

368
00:02:15,512 --> 00:02:17,913
Eto... bây giờ, nếu cậu muốn, 
chúng ta sẽ nói về trường học.

368
00:02:18,112 --> 00:02:20,513
Ừ, tớ rất mong chờ đấy.



368
00:02:24,712 --> 00:02:25,913
Ồ? Đã đến giờ về rồi sao?

51
00:02:26,112 --> 00:02:30,113
Phải, mặt trời sắp lặn và đã muộn rồi.

52
00:02:30,312 --> 00:02:31,913
Sẽ có chuyện nếu tớ không về sớm đấy.

53
00:02:32,112 --> 00:02:34,913
Cậu cần về nhà ngay bây giờ à?

54
00:02:35,112 --> 00:02:36,913
Ừ... tớ phải về chuẩn bị bữa tối.

55
00:02:37,112 --> 00:02:42,113
Nếu Shidou-san làm đồ ăn, tớ rất muốn thử.

56
00:02:42,312 --> 00:02:43,913
Vậy sao cậu không đến nhà tớ ăn tối đi?

57
00:02:44,112 --> 00:02:48,713
Lời mời đột ngột thế... liệu có được không?

58
00:02:48,912 --> 00:02:50,513
Ừ... cậu có việc gì không?

59
00:02:51,112 --> 00:02:57,713
Thật tiếc là có!
Nếu vậy, ngày mai cậu có thể làm bento cho tớ không?

60
00:02:57,912 --> 00:02:59,713
Tớ hiểu rồi. Được đấy.
Mai tớ sẽ làm.

41
00:03:00,112 --> 00:03:02,513
Thật vậy sao?

42
00:03:02,712 --> 00:03:05,513
Tất nhiên rồi. Khi nào tớ có thể gặp cậu?

43
00:03:05,912 --> 00:03:10,113
Vậy ngày mai, tớ sẽ đợi cậu.

44
00:03:11,112 --> 00:03:13,713
À, Shidou-san, chân cậu...

45
00:03:14,112 --> 00:03:15,513
Hở?

46
00:03:16,712 --> 00:03:18,113
A, tệ thật...

47
00:03:18,312 --> 00:03:22,113
Không, tớ sẽ không cười vì cậu bị ngã đâu.

48
00:03:24,712 --> 00:03:25,913
Ku... Kurumi?

49
00:03:26,112 --> 00:03:29,113
A... Xin lỗi...

50
00:03:30,512 --> 00:03:31,713
À, không sao...

51
00:03:32,112 --> 00:03:35,513
Xin lỗi vì điều đó...

52
00:03:35,912 --> 00:03:37,513
Vậy tớ đi nhé...

53
00:03:37,712 --> 00:03:39,713
A... ano!

54
00:03:39,912 --> 00:03:44,713
Cậu có thể ở bên cạnh tớ nhiều hơn chút nữa không?

55
00:03:45,112 --> 00:03:45,913
Hở?

56
00:03:46,912 --> 00:03:52,113
Khi ở cạnh Shidou-san, tớ cảm thấy rất yên bình...

57
00:03:52,312 --> 00:03:53,113
Hở?

58
00:03:53,312 --> 00:03:58,113
Trong hoàng hôn này,
ở ngôi đền này, cậu có muốn ở bên tớ không?

59
00:03:58,312 --> 00:03:59,513
Ku... ru... mi...?

60
00:04:00,112 --> 00:04:02,313
Cậu không thể sao?

61
00:04:02,512 --> 00:04:03,713
Không sao đâu...

62
00:04:03,912 --> 00:04:09,313
Sau đó, tớ sẽ cho cậu biết mục đích của tớ.

63
00:04:09,512 --> 00:04:10,713
Hở?

64
00:04:12,312 --> 00:04:14,313
À... Tớ nhớ ra rồi. Là... là chuyện đó.

65
00:04:14,912 --> 00:04:19,513
Shidou-san thật là.
Không cần tỏ ra xấu hổ vậy đâu.

66
00:04:19,712 --> 00:04:20,912
Ý cậu là gì chứ? Tớ không thể hiểu nổi!

67
00:04:21,112 --> 00:04:22,513
Tớ xin lỗi. Nhưng còn chuyện mục đích của cậu...

68
00:04:23,112 --> 00:04:26,113
Vâng, đó là...

69
00:04:26,712 --> 00:04:35,113
Thật là... đây là vấn đề lớn đấy.
Tôi đã nói sẽ không tha thứ cho việc này rồi mà.

70
00:04:35,312 --> 00:04:36,513
Hở?

71
00:04:39,912 --> 00:04:46,513
Lần cuối cùng tôi gặp cô, 
Shidou-san đã rất tốt với cô phải không?

72
00:04:46,712 --> 00:04:47,913
Có 2... Kurumi?

73
00:04:48,512 --> 00:05:02,113
Vẻ mặt đó là sao, Shidou-san?
Cậu không quên bọn tớ không đi một mình đấy chứ?

74
00:05:02,312 --> 00:05:05,513
Phải, tớ biết... Kurumi có nhiều tính cách đối lập...
Vậy thì... Kurumi thật chính là...?

75
00:05:08,712 --> 00:05:22,113
Không được đâu nhé, Shidou-san!
Cậu không quên cô ta, nhưng lại quên tớ.
Vậy có công bằng không?

76
00:05:23,512 --> 00:05:32,113
Nếu tớ cho cậu thấy một cơn ác mộng kinh khủng
sẽ khắc sâu vào tâm trí cậu thì sao nhỉ?

77
00:05:33,112 --> 00:05:34,513
Ý cậu là gì?

78
00:05:37,112 --> 00:05:43,113
Chúng ta sẽ giải trí một lúc nhé!

79
00:05:46,112 --> 00:05:47,513
Ánh sáng gì vậy?



80
00:05:50,112 --> 00:05:51,513
Chuyện gì đang xảy ra ở đây vậy?

81
00:05:51,712 --> 00:05:54,113
Nơi này, giống như...

82
00:05:55,712 --> 00:05:57,513
Đúng rồi... nơi này... giống như ở Tháp Tenguu.

83
00:05:59,112 --> 00:06:00,513
Shidou-san!

84
00:06:00,912 --> 00:06:02,513
Ku... Kurumi!

85
00:06:05,512 --> 00:06:14,513
Giỏi thật! Nếu cô tỏ ra muốn bảo vệ Shidou-san,
cô sẽ chết theo cách rất đau đớn đấy.

86
00:06:17,112 --> 00:06:19,113
Không... tôi sẽ không cho phép!

87
00:06:21,312 --> 00:06:22,513
Ku... Kurumi!

88
00:06:23,112 --> 00:06:27,113
Lùi lại đi! Tớ sẽ bảo vệ cậu dù có chuyện gì.

89
00:06:27,312 --> 00:06:28,313
Cậu sẽ làm gì, Kurumi?

90
00:06:28,513 --> 00:06:37,113
Thật dễ dàng để tuyệt vọng.
Không có tương lai cho tôi vì tôi đã không vâng lời.

91
00:06:38,112 --> 00:06:42,513
Nhưng... tôi sẽ không cho phép cô
làm tổn thương Shidou-san!

92
00:06:44,512 --> 00:06:46,513
Đừng làm thế... nếu cậu làm thế, cậu sẽ biến mất!

93
00:06:48,112 --> 00:06:56,113
Diễn văn của cô xong rồi phải không?
Vậy thì, đã đến lúc cô biến mất!

94
00:06:59,912 --> 00:07:00,712
Kurumi!

95
00:07:00,912 --> 00:07:08,113
Shidou-san, tớ xin lỗi.
Tớ không muốn chịu đựng nữa...

96
00:07:09,112 --> 00:07:13,513
Ara ara, đừng nói với tôi cô sẽ kể hết mọi chuyện.

97
00:07:14,112 --> 00:07:30,113
Cô không thấy trái tim của Shidou đang tan vỡ sao?
Xem cách cô phá hủy chính mình.
Cô nên biến mất đi.

98
00:07:32,112 --> 00:07:34,113
Này Kurumi, cậu thật sự đã bảo vệ tớ đúng không?

99
00:07:34,512 --> 00:07:38,513
Dù cậu không nói, tớ vẫn sẽ bảo vệ cậu.

100
00:07:38,912 --> 00:07:40,713
Vậy thì, tớ muốn cậu tiếp tục làm thế.
Tớ không thể sống mà không có cậu.

101
00:07:40,913 --> 00:07:42,513
Shidou-san...

102
00:07:43,112 --> 00:07:45,513
Vì vậy, tớ sẽ không cho phép cậu biến mất nữa!
Tớ... tớ muốn bảo vệ cậu!

103
00:07:46,112 --> 00:08:02,713
Thật sự chẳng còn phép màu nào đâu!
Shidou-san... cậu muốn chết sớm?
Nếu vậy, tớ sẽ là người làm điều đó.

104
00:08:03,112 --> 00:08:14,113
Giờ Shidou-san chỉ là một cái vỏ rỗng,
không đáng để ăn nữa. Vậy nên hãy biến mất tại đây...

105
00:08:15,112 --> 00:08:16,713
Tôi sẽ không bao giờ để cô làm điều đó!

106
00:08:17,112 --> 00:08:20,113
Đừng tham gia vào việc không liên quan đến cô!

107
00:08:23,112 --> 00:08:29,113
Đánh đổi nhiệm vụ của mình lấy cảm xúc...
Nó chẳng có giá trị gì.

108
00:08:32,912 --> 00:08:37,513
Cô thực sự không biết chuyện gì đang xảy ra sao?

109
00:08:40,512 --> 00:08:48,113
Sớm thôi. Thời gian sống của chúng ta
đang trên bờ vực cạn kiệt.

110
00:08:48,312 --> 00:08:59,113
Ngoài ra, tôi không bổ sung thời gian từ bên ngoài.
Cô đang chết dần trong sự do dự của cô về cuộc sống đấy.

111
00:09:00,112 --> 00:09:01,913
Nghĩa là Kurumi từ đó đến giờ chưa từng tấn công ai? 
(lời hứa trên sân thượng)

108
00:09:02,112 --> 00:09:14,913
Phải, đúng đấy Shidou-san.
Cậu cũng biết điều gì sẽ xảy ra với Tinh Linh chứ?
Đó là lý do tại sao cô ta phải chết!

109
00:09:15,112 --> 00:09:16,313
Nhưng đó là đường cùng rồi...

110
00:09:16,512 --> 00:09:26,713
Nói thật, giống như mèo đi tìm thức ăn vậy.
Tớ vẫn là một kẻ giết người, và đó là bản chất của tớ.

111
00:09:26,912 --> 00:09:27,913
Kurumi... đừng nghĩ vậy...

112
00:09:28,112 --> 00:09:30,113
Kurumi... cô ấy đã suy nghĩ và tự mình trả giá.
Nhưng điều đó sẽ khiến cô ấy biến mất.

108
00:09:30,312 --> 00:09:32,513
Tôi nên làm gì đây? Nếu tôi không làm gì đó,
Kurumi sẽ làm! Tôi yêu cô ấy 
và tôi muốn cô ấy ở bên tôi!

109
00:09:32,712 --> 00:09:34,913
Mình có thể làm gì?
Mình không có sức mạnh, mình không thể giúp cô ấy...

110
00:09:35,112 --> 00:09:36,313
Mình không muốn ... Kurumi sẽ biến mất như thế này!

111
00:09:36,512 --> 00:09:38,713
Sức mạnh... sức mạnh... Mình muốn sức mạnh!
Mình muốn sức mạnh... để có thể bảo vệ Kurumi!

112
00:09:42,112 --> 00:09:50,113
Cái gì vừa vây quanh tớ vậy?
Tớ có thể cảm thấy nó từ tận sâu cơ thể mình.

112
00:09:50,312 --> 00:09:51,513
Đây là...?

113
00:09:51,912 --> 00:09:53,113
2 cậu, chuyện gì vậy?

114
00:09:53,512 --> 00:09:58,513
Đây là... Sức mạnh này là...!

1
00:10:00,112 --> 00:10:06,513
Thật tuyệt, thật tuyệt vời!

1
00:10:06,912 --> 00:10:12,513
Đúng vậy... thứ này! 
Cảm giác này thật tuyệt vời!

1
00:10:12,912 --> 00:10:21,113
Tớ hiểu rồi, đó là vì mong muốn đó.
Sức mạnh này, có lẽ đến từ Shidou-san.

1
00:10:21,112 --> 00:10:27,513
Được rồi. Tớ đã có thêm thời gian.

1
00:10:28,112 --> 00:10:34,113
Nhưng lần sau... tớ muốn thấy sức mạnh đó nữa.






1
00:10:49,112 --> 00:10:51,113
Kurumi! Cậu không sao chứ?

1
00:10:52,512 --> 00:10:54,513
Tớ không sao... phần nào đó.

368
00:10:55,112 --> 00:10:56,713
Tớ cứ ngỡ cậu biến mất rồi.

368
00:10:57,112 --> 00:11:03,113
Tớ cần nghỉ ngơi một chút.
Toàn thân tớ đã bị thương rồi...

368
00:11:07,112 --> 00:11:15,513
Shidou-san có cảm thấy sức mạnh to lớn đó không?
Nó gần bằng với tháp Tenguu mới...

368
00:11:15,913 --> 00:11:31,513
Phải, sức mạnh đó lớn hơn tớ nữa.
Tớ không biết tại sao, nhưng so với 
giới hạn của tớ, nó thật khủng khiếp...

368
00:11:32,112 --> 00:11:33,513
Phải, thật đáng sợ...

368
00:11:33,912 --> 00:11:43,113
Sức mạnh đó không đến chỉ vì mong muốn có nó.
Phải có lý do tại sao.

368
00:11:43,512 --> 00:11:53,113
Shidou-san, cậu nên cẩn thận với sức mạnh đó.
Nếu chuyện xấu nhất xảy ra,
nó sẽ phá hủy mọi thứ nó có.

368
00:11:53,512 --> 00:11:56,513
Liệu đó là gì?
Chỉ nghĩ đến một người có sức mạnh 
lớn như thế đã cảm thấy ớn lạnh rồi.

368
00:11:56,712 --> 00:12:08,513
Phải. Một sự tồn tại
siêu việt với sức mạnh to lớn.
Một người như thế đã mở ra vụ việc này.

368
00:12:09,112 --> 00:12:19,113
Tớ là một Tinh Linh chưa bị phong ấn... 
Cậu giải cứu tớ khỏi "Nightmare" 
có thể là chìa khóa kích hoạt
một sức mạnh chưa từng thấy.

368
00:12:19,712 --> 00:12:28,113
Tình huống vừa rồi tương tự như lần trước 
chúng ta tự cứu mình. Điều gì đó kỳ lạ
đang xảy ra trong thế giới này.

68
00:12:28,512 --> 00:12:40,113
Không phải vì "Nightmare" đã có được sức mạnh,
mà kỳ lạ là cậu đã cứu tớ với sức mạnh đó.

368
00:12:42,112 --> 00:12:48,113
Và rồi... nếu tớ ở một mình, 
tớ tin tớ có thể gây ảnh hưởng nhiều hơn.

368
00:12:48,512 --> 00:12:49,713
Kurumi...

368
00:12:50,112 --> 00:12:55,113
Codename "Nightmare" không chỉ để trang trí.

368
00:12:55,512 --> 00:12:58,713
Tớ hiểu... nhưng cậu có ổn không?
Dù cậu không chiến đấu, nó vẫn trong cơ thể...

68
00:12:59,112 --> 00:13:09,113
Tớ không nói dối cậu, Shidou-san.
Nếu có Shidou-san với sức mạnh của cậu, tớ có thể sống sót.

368
00:13:10,712 --> 00:13:21,913
Nhưng... giờ thì khác.
Từ khi cậu bước vào cuộc đời tớ, cậu đã rất quan trọng với tớ.

368
00:13:22,112 --> 00:13:24,913
Kurumi... Nhưng... cậu quên cơ thể cậu 
có thể tự chữa lành được sao?

368
00:13:25,112 --> 00:13:26,513
Không...

368
00:13:26,912 --> 00:13:28,113
Hở? Nhưng cậu có thể dùng...

368
00:13:28,312 --> 00:13:42,113
Tớ không thể làm thế.
Cơ thể của tớ không giúp được nhiều. 
Cơ thể của tớ với Kurumi Evil rất yếu 
và biến mất là vấn đề thời gian.

368
00:13:42,513 --> 00:13:43,513
Chúng ta không thể làm gì được sao?

368
00:13:44,112 --> 00:13:45,113
Rất tiếc là không...

368
00:13:45,512 --> 00:13:46,513
Không thể thế được!



368
00:13:47,112 --> 00:13:51,113
Shidou-san, cảm ơn cậu rất nhiều.

368
00:13:51,312 --> 00:13:52,113
Kurumi, đợi đã! Cậu định đi đâu?

368
00:13:53,512 --> 00:13:57,513
Tớ rất vui và tớ yêu cậu rất nhiều...

368
00:13:59,512 --> 00:14:01,113
Kurumiiiiii!

368
00:14:07,112 --> 00:14:13,113
Ngày 2 tháng 7
