﻿0
00:00:04,312-->00:00:10,513
Shidou... dậy đi...
Anh còn ngủ tới bao giờ nữa?

1
00:00:10,912-->00:00:12,113
Rinne... Xin lỗi, tớ...

2
00:00:15,112-->00:00:15,913
Uida!

3
00:00:16,112-->00:00:18,113
Ai là Rinne hả?

4
00:00:18,512-->00:00:20,113
Hở? Ko... Kotori... Gì vậy?

5
00:00:21,112-->00:00:28,113
"Gì vậy" cái gì.
Từ hôm nay, anh phải chuẩn bị 
bữa sáng đúng không?

6
00:00:28,312-->00:00:29,913
Ồ! Anh tưởng Rinne làm...

7
00:00:30,312-->00:00:33,913
Anh đang trốn việc đấy à?

7
00:00:34,112-->00:00:42,513
Chán thật... Anh nhầm em với Rinne,
chứng tỏ anh càng lúc càng ỷ lại
rồi phải không?

8
00:00:43,112-->00:00:47,513
Không, cậu ấy là bạn thuở nhỏ
nên đó là chuyện bình thường thôi.
Hôm qua, cậu ấy vẫn đến làm bữa tối mà.

9
00:00:49,712-->00:00:53,113
Rinne là người luôn nhận việc khó 
giúp người khác. Anh sẽ không 
lợi dụng lòng tốt của cậu ấy.

10
00:00:53,512-->00:00:55,113
Giống như "ai đó"

11
00:00:55,912-->00:00:57,113
Hở? Em nói gì?

12
00:00:58,112-->00:01:08,113
Không có gì đâu...
Vậy giờ anh có thay quần áo không?

13
00:01:09,112-->00:01:18,113
Hay là... anh cần em gái dễ thương
của anh giúp đỡ? Shidou, anh là một
kẻ biến thái không trả lời thành thật.

14
00:01:18,712-->00:01:20,113
Biến thái? Anh chỉ đang nói chuyện với em về Rinne...

15
00:01:21,112-->00:01:31,113
Em biết. Em cũng biết anh từ nhỏ đấy...
Anh không có việc cần làm ngay sao?

16
00:01:31,512-->00:01:33,113
Ừ, xin lỗi... anh sẽ thay đồ nhanh.

17
00:01:33,712-->00:01:37,713
Và rồi... này!

18
00:01:38,112-->00:01:39,113
Sao vậy Kotori?

19
00:01:39,712-->00:01:44,113
Anh không biết thế nào là ý tứ sao?

20
00:01:44,312-->00:01:45,513
Hở? Anh đang cố làm nhanh mà?

21
00:01:46,112-->00:01:49,113
Thay đồ nhanh rồi xuống đi!

22
00:01:53,512-->00:01:55,513
Dù có đeo nơ đen huyền thoại,
đây vẫn là Kotori...

23
00:02:00,112-->00:02:02,113
Được rồi, đã lâu rồi
mình không làm việc này.

24
00:02:02,313-->00:02:04,113
Nguyên liệu... có đầy đủ cả rồi.

25
00:02:05,112-->00:02:06,113
Được, bắt đầu thôi.

25
00:02:06,313-->00:02:08,113
Kotori, những người khác đâu?
Họ chưa dậy à?

25
00:02:08,512-->00:02:13,113
Vâng, chỉ có mình em thôi.

25
00:02:13,312-->00:02:15,113
Vậy à? Này...
Đừng có mút kẹo trước khi ăn nữa!

25
00:02:15,912-->00:02:19,113
Không quan trọng. Cứ ăn sáng đi đã.

25
00:02:20,112-->00:02:21,113
Chán thật...

25
00:02:21,312-->00:02:27,913
Anh biết không. Nếu anh muốn não hoạt động,
anh cần thêm đường. Em chỉ đang
nạp thêm lượng đường thích hợp thôi.

25
00:02:28,112-->00:02:33,113
Không như anh, Shidou.
Em có rất nhiều điều cần nghĩ.

25
00:02:33,312-->00:02:34,513
Anh chẳng hiểu em đang nói gì.

25
00:02:35,312-->00:02:38,113
Anh sao đủ trình độ để hiểu chứ.

25
00:02:38,312-->00:02:39,513
Có lẽ... Sao em không nói thế ngay từ đầu đi?

25
00:02:40,112-->00:02:43,113
Con trai nhiều chuyện 
đáng ghét lắm đấy biết không?

25
00:02:43,312-->00:02:48,113
Ồ. Tháp Tengu được mở cửa rồi kìa.

25
00:02:48,312-->00:02:50,513
Tháp Tengu à? Sau khi tháp được xây dựng,
báo chí ít nói gì về nguồn gốc của nó.

25
00:02:51,112-->00:03:01,113
Mà em cũng đoán trước rồi.
Nó không có gì giống như
một điểm nhấn du lịch cả.

25
00:03:01,512-->00:03:04,113
Phải... Anh nghĩ anh đã từng đến đó.
Nhưng anh không nhớ gì cả.

25
00:03:05,112-->00:03:09,113
Anh đã ở tượng đài Tháp Tengu sao?

25
00:03:09,312-->00:03:10,513
Gì cơ? Đó là gì?

25
00:03:11,112-->00:03:20,913
Nó đã từng rất nổi tiếng.
Những cặp đôi thề hứa với nhau trước tượng đài.
Họ sẽ sống bên nhau mãi mãi.

25
00:03:21,112-->00:03:21,913
Thế à...

25
00:03:22,112-->00:03:24,513
Tôi có cảm giác khó chịu nào đó... 
tượng đài đó. Tôi nên đến xem thử...

25
00:03:24,912-->00:03:26,113
Ồ, nước sôi rồi. Anh làm bữa sáng đây.

25
00:03:28,112-->00:03:33,113
Chào buổi sáng, Shidou-san...!

25
00:03:34,112-->00:03:37,513
Shidou-kun, chào buổi sáng!

25
00:03:37,913-->00:03:40,113
Chào buổi sáng, Yoshino, Yoshinon. 
Anh làm xong bữa sáng ngay đây.
2 em ngồi chờ đi.

25
00:03:41,112-->00:03:46,913
Để... để em giúp anh...!

25
00:03:47,112-->00:03:48,113
Hở? Yoshino giúp à?

25
00:03:48,312-->00:03:51,113
Từ hôm nay, Rinne-chan 
sẽ không đến nữa phải không?

25
00:02:51,312-->00:02:51,913
À phải...

26
00:03:52,112-->00:04:02,113
Yoshino và em đã lo không biết
anh có làm kịp bữa sáng không đấy.
Anh chỉ mới khỏe lại thôi mà.

26
00:04:05,112-->00:04:07,113
Được rồi. Vậy nhờ em giúp nhé.

26
00:04:08,112-->00:04:13,513
V... Vâng! Em... em sẽ cố gắng!

26
00:04:14,112-->00:04:21,113
Tốt lắm Yoshino!
Rõ ràng cậu đang muốn Shidou-kun thấy
một cô bé dễ thương nấu ăn phải không?

26
00:04:21,312-->00:04:24,313
Này này Yoshinon! 
Em nói vậy khiến Yoshino khó xử đấy.

26
00:04:24,712-->00:04:29,113
Vậy à? Em không nghĩ thế.

26
00:04:32,112-->00:04:35,113
Um, um... Chúng ta bắt đầu nhé.
Vậy Yoshino, em có thể mang cho anh
một ít nước tương không?

26
00:04:35,912-->00:04:37,113
V... vâng!

26
00:04:41,912-->00:04:43,313
Có sao không, Yoshino? 
Nó bắn lên áo em à?

26
00:04:44,112-->00:04:47,113
Em... em xin lỗi...

26
00:04:47,312-->00:04:49,513
Không sao. Đồ của em bị bẩn rồi à?
Nước chấm khó rửa lắm đấy.

26
00:04:49,913-->00:04:52,113
Ổn thôi. Dù sao đây là lần đầu 
em làm bếp. Giờ chắc là ổn rồi.

26
00:04:52,312-->00:04:55,113
Yoshino! Đừng bận tâm! Đừng bận tâm!
(Don't mind! Don't mind!)

27
00:04:56,112-->00:04:59,513
Bây giờ... Em sẽ lau sạch chỗ này!

28
00:05:00,112-->00:05:03,113
Được rồi, anh sẽ lau chỗ này.
Yoshino, em lấy giúp anh 
mấy cái đĩa được không?

29
00:05:03,512-->00:05:05,113
V... Vâng!

30
00:05:07,712-->00:05:09,313
Yo... Yoshino? Không sao chứ?
Em có bị thương không?

31
00:05:15,112-->00:05:20,113
Shidou-kun, tại anh đấy.

32
00:05:20,312-->00:05:21,113
Hở? Tại anh à?

33
00:05:21,312-->00:05:23,513
Không, phải rồi.
Yoshino không thể dùng tay trái được.
Tôi cần giao việc khác...

34
00:05:24,512-->00:05:26,713
Ồ, cái nồi đã sôi rồi.

35
00:05:27,112-->00:05:28,513
Được rồi! Yoshino, em giúp anh nhé?

36
00:05:30,912-->00:05:33,113
Cái nồi này có vẻ chín rồi.
Em có thể mở nắp kiểm tra bên trong không?

37
00:05:34,112-->00:05:39,113
Ý anh là mở nắp ra à?

38
00:05:39,912-->00:05:42,113
Phải! Lửa tắt rồi, 
nhưng vẫn nóng lắm. Cẩn thận đấy!

39
00:05:43,112-->00:05:44,313
V... Vâng!

40
00:05:45,113-->00:05:48,913
Nồi này có tay cầm bằng kim loại.
Vấn đề là em ấy không thể
mở nắp mà không cầm chắc được.
Nếu hướng dẫn của tôi chính xác...

41
00:05:49,113-->00:05:53,113
Nóng quá! Yoshino! Nhanh lên!

42
00:05:54,113-->00:05:55,513
Đúng rồi! Yoshino đã dùng Yoshinon
như đồ lót tay!

43
00:05:58,912-->00:06:01,113
Tốt lắm Yoshino! 
Để nắp sang bên kia đi!

39
00:06:01,912-->00:06:09,113
V... Vâng! Em đặt nó ở đây nhé?

40
00:06:09,313-->00:06:10,913
Ừ! Để đó đi!

41
00:06:11,113-->00:06:16,113
Vâng... em nghĩ là... nó chín rồi đấy!

42
00:06:16,913-->00:06:18,113
Ừ! Làm tốt lắm, Yoshino!

43
00:06:19,113-->00:06:23,713
Nhờ Yoshinon đấy...!

39
00:06:23,913-->00:06:25,113
Không, đó là sự kết hợp tuyệt vời 
của Yoshino và Yoshinon.

40
00:06:25,313-->00:06:32,113
Yoshino đã làm được rồi đấy. 
Tớ mệt quá...

41
00:06:33,113-->00:06:43,513
Tớ xin lỗi, Yoshinon...!
Shidou-san, cảm ơn anh...!

42
00:06:44,113-->00:06:51,113
Yoshino và Yoshinon làm tốt lắm 
đúng không? Giỏi không? Giỏi không?

43
00:06:51,712-->00:06:54,113
Phải. Thật may là em không bị bỏng.
Làm tốt lắm, Yoshinon!

44
00:06:54,312-->00:07:02,113
Anh biết cách tâng bốc nhỉ.
Anh đang kích thích em rất nhiều đấy.
Anh cần em làm gì nữa không?

45
00:07:02,912-->00:07:05,513
Nếu vậy, em có thể cho đồ ăn vào
hộp bento được không Yoshino?

46
00:07:06,112-->00:07:11,113
V... Vâng! Em sẽ cố gắng!

47
00:07:12,112-->00:07:20,113
Hết cách rồi. Vâng, hôm nay Yoshino và Yoshinon
sẽ giúp anh hết mình như Rinne-chan.

48
00:07:20,312-->00:07:22,113
Được rồi! Cảm ơn hai em nhiều!

49
00:07:22,312-->00:07:24,113
V... Vâng! 

54
00:07:32,512-->00:07:34,113
Anh đi đây.

55
00:07:34,912-->00:07:36,913
Đợi đã.

56
00:07:37,112-->00:07:38,313
Sao vậy?

57
00:07:38,712-->00:07:41,113
Cổ áo đồng phục đang dựng lên kìa.

58
00:07:41,312-->00:07:42,513
Ở đâu?

59
00:07:43,112-->00:07:48,513
Lại đây. Ở đây này...

60
00:07:49,312-->00:07:52,113
Um... Mặt em ấy ở gần qua...
Từ trước đến giờ tôi đã không để ý.
Em đã trưởng thành, giống như một người vợ.

41
00:07:53,112-->00:07:55,113
Anh đang nghĩ gì vậy?

42
00:07:55,312-->00:07:56,313
Không... không có gì...

43
00:07:57,112-->00:08:00,113
Mặt anh hiện rõ vẻ ưu tư đấy.

44
00:08:02,112-->00:08:04,113
Xong rồi đấy.

45
00:08:04,912-->00:08:06,113
Cảm ơn em....

46
00:08:06,712-->00:08:12,913
Anh không cần cảm ơn.
Nếu anh ăn mặc như thế, em xấu hổ đấy.

47
00:08:13,113-->00:08:15,313
Gì vậy? Phản ứng của Kotori là gì?
Một cái gì đó, một chút... 
ngại ngùng sao...?

48
00:08:15,512-->00:08:17,513
Hẹn hò và cuộc sống thường ngày,
em ấy đang dần thay đổi sao?

49
00:08:18,112-->00:08:21,113
Anh đi cẩn thận.

50
00:08:21,312-->00:08:22,713
Ừ... cảm ơn em. Anh đi đây.

51
00:08:29,912-->00:08:31,913
Cuối cùng cũng hết giờ. 
Cảm giác như chờ cả ngày vậy.

52
00:08:32,112-->00:08:34,113
Đến giờ ăn trưa rồi...
Bữa trưa này là do tôi làm...

53
00:08:34,312-->00:08:35,513
Um... hôm nay em ấy ăn ở đâu vậy?

54
00:08:40,112-->00:08:42,713
Xin lỗi... Reine-san có ở đây không?
Kotori không có ở đây.
Và bữa trưa cũng thế.

55
00:08:43,912-->00:08:45,113
Điện thoại? 
Đúng lúc mình cần nói chuyện.

56
00:08:45,312-->00:08:46,513
Alo, Kotori à? Có chuyện gì vậy?

57
00:08:47,512-->00:08:55,113
Alo, Shidou à? Hôm nay có môn 
thể dục phải không?
Anh đã mang theo đồ thể dục chưa?

58
00:08:55,312-->00:08:57,113
Hở? Ồ, có rồi. Anh vẫn nhớ mà.

59
00:08:57,312-->00:09:01,513
Ồ, à phải. Vậy thì tốt rồi.

60
00:09:01,913-->00:09:02,713
Em không mang à?

60
00:09:02,913-->00:09:16,913
Cái gì? Phản ứng cái gì?
Em quên bộ đồ thể dục rồi.
Nếu anh quên đồ của Tohka,
tinh thần của cô ấy sẽ đạt Burst đấy.

61
00:09:17,112-->00:09:18,513
Anh sẽ không làm điều đó!

62
00:09:18,912-->00:09:20,313
Sao biết được.

63
00:09:20,512-->00:09:21,713
Ý em là sao...?

64
00:09:21,913-->00:09:24,513
Em ước mình không quên đồ. Vậy chào nhé!

65
00:09:24,712-->00:09:25,713
Khoan đã, Kotori...

66
00:09:25,912-->00:09:27,313
Gì vậy?

67
00:09:27,512-->00:09:30,113
Cảm ơn sự quan tâm của em.
Nghe giọng em, nó mạnh mẽ hơn bình thường.

68
00:09:30,312-->00:09:32,513
Không có đâu, đồ ngốc!

69
00:09:33,112-->00:09:38,113
Em không có thời gian để nói chuyện!
Em đi đây!

70
00:09:38,312-->00:09:39,513
Này! Còn bữa trưa...

71
00:09:41,312-->00:09:42,713
Cúp máy rồi...

72
00:09:42,912-->00:09:45,913
Nhưng Kotori... tại sao em lại gọi?
Em ấy muốn nghe giọng mình à?

73
00:09:46,112-->00:09:46,913
Giờ mình gọi lại cũng không nghe đâu...

74
00:09:47,112-->00:09:48,313
Mình nên trở lại lớp học và ăn trưa.

75
00:09:54,112-->00:09:56,513
Cuối cùng đã hết buổi học.
Văn học cổ điển là gì?
Chẳng có gì trong đầu tôi...

76
00:09:56,712-->00:09:59,113
Văn học cổ điển thật sai lầm.
Đó là một loại ngôn ngữ ngoài hành tinh chăng?
Cả một vườn ngôn từ mới.

77
00:09:59,312-->00:10:02,713
Aaa... tiếng chuông chùa,
dòng chảy của những con sông,
chiều tà mùa xuân và đêm mùa hạ...
Văn học cổ điển thật vô nghĩa.

79
00:10:03,112-->00:10:05,113
Mình không muốn đụng độ Tonomachi nữa.
Hôm nay mình nên đi đâu đây?

80
00:10:20,112-->00:10:21,513
Đi đến Phòng gần cầu thang?
1) Đồng ý
2) Không đồng ý

81
00:10:26,112-->00:10:27,513
Được rồi, hôm nay mình sẽ
hẹn hò với Kotori.

82
00:10:30,512-->00:10:31,713
Alo?

83
00:10:32,112-->00:10:32,913
Alo, anh đây...

84
00:10:33,112-->00:10:35,113
Shidou à? Có chuyện gì vậy?

85
00:10:35,512-->00:10:37,513
Um... Anh muốn hẹn hò với em.

86
00:10:38,112-->00:10:39,513
Hẹn hò à?

87
00:10:40,112-->00:10:41,313
Em bận rồi à?

1
00:10:41,512-->00:10:45,113
Xem nào... 
Được thôi, chúng ta hẹn hò đi.

1
00:10:45,512-->00:10:46,513
Cái gì? Đồng ý ngay luôn...  
Thật kỳ lạ.

1
00:10:46,912-->00:10:48,113
Sao vậy?

1
00:10:48,312-->00:10:50,113
À không... Vậy chúng ta gặp nhau ở đâu?

1
00:10:50,312-->00:10:53,113
Hôm nay hẹn hò ở nhà được không?

1
00:10:53,312-->00:10:53,913
Gì cơ?

1
00:10:54,112-->00:11:00,113
Ồ, hôm nay trời mưa từ buổi tối, 
dự báo thời tiết nói thế...

368
00:11:00,312-->00:11:03,113
Phải không? Bây giờ mặt trời 
vẫn sáng. Em đang nhầm sang 
dự báo thời tiết buổi sáng à?

368
00:11:03,313-->00:11:10,113
Không sao đâu, thế thì càng tốt!
Tóm lại, hôm nay hẹn hò ở nhà đi!

368
00:11:10,312-->00:11:11,313
Ừ... ừ... Được rồi.

368
00:11:11,512-->00:11:13,113
Vậy chào anh nhé!

368
00:11:15,312-->00:11:17,513
Con bé bị sao thế nhỉ?

368
00:11:22,312-->00:11:23,113
Anh về rồi.

368
00:11:23,312-->00:11:24,513
Trễ quá!

368
00:11:24,912-->00:11:26,513
Phải không? Anh đi thẳng về mà...

368
00:11:27,112-->00:11:35,113
Sao anh không đi cách khác?
Thực sự, mỗi hành động của anh
là một con gấu xấu xí.

368
00:11:35,312-->00:11:36,113
Kotori hàng ngày à?

368
00:11:36,312-->00:11:39,113
Từ sáng đến giờ, 
Kotori luôn lo lắng cho mình.
Đó có phải là một giấc mơ không?

68
00:11:40,112-->00:11:41,113
Shidou...

368
00:11:41,312-->00:11:42,113
Hở? Ồ... gì vậy?

368
00:11:42,312-->00:11:45,113
Làm ơn đừng đứng đó.

368
00:11:45,712-->00:11:47,113
Hả... Cái gì? Sao lại đổi giọng rồi...

368
00:11:48,112-->00:11:55,113
Anh biết đấy. Khi em về,
không có ai ở đây cả.

68
00:11:55,312-->00:11:56,513
Chắc mọi người đều ở nhà mình rồi...

368
00:11:57,112-->00:12:01,113
Hẹn hò với anh, anh lại nghĩ đến 
cô gái khác à?

368
00:12:01,913-->00:12:03,513
Không, không phải như thế.

368
00:12:04,112-->00:12:05,713
Biết đâu được...

368
00:12:05,912-->00:12:12,113
Vâng, anh ngồi đi. Em sẽ dọn bữa tối.

368
00:12:12,312-->00:12:13,313
Ồ, anh làm...

368
00:12:13,912-->00:12:16,113
Em đã nói để em làm!

368
00:12:16,312-->00:12:18,113
Được rồi. Cảm ơn em, Kotori.

368
00:12:21,913-->00:12:23,913
Kotori... Trông con bé loay hoay quá...

368
00:12:24,113-->00:12:25,913
Um... mình có nên để
Kotori như thế này không?

368
00:12:26,112-->00:12:28,113
Ngồi chờ thế này cũng chán.
Mình muốn giúp đỡ...

368
00:12:32,112-->00:12:34,313
Kotori! Em có sao không?
Có bị thương không?

368
00:12:37,112-->00:12:38,113
May quá.

368
00:12:38,913-->00:12:42,113
Nhưng quần áo của em ướt rồi...

368
00:12:42,712-->00:12:44,113
Vậy em phải thay đồ đi tắm thôi.

368
00:12:45,112-->00:12:50,113
Không cần đâu...
Cứ để đó rồi nó tự khô thôi.

368
00:12:50,712-->00:12:52,113
Nếu để lâu sẽ bị cảm lạnh đấy.

368
00:12:55,112-->00:12:56,713
Nào, anh giặt đồ cho.

368
00:12:57,112-->00:12:59,113
Nhưng mà...

368
00:12:59,712-->00:13:01,113
Em còn ngại gì nữa?

368
00:13:01,713-->00:13:05,113
Chỉ là... chỉ có em và anh ở nhà...

368
00:13:05,312-->00:13:06,113
Hở?

368
00:13:06,313-->00:13:11,113
Đây là buổi hẹn hò,
nhưng em không có thời gian để...

368
00:13:11,312-->00:13:12,513
Em ấy đang nói gì vậy?

368
00:13:13,112-->00:13:17,513
Tôi không muốn mất nhiều thời gian,
nhưng có vẻ em ấy đang do dự 
khi bước vào phòng tắm...
Không, chẳng lẽ em ấy muốn thế?

368
00:13:17,712-->00:13:21,113
Bình tĩnh nào? Tốt nhất là 
để Kotori vào phòng tắm.
Được, tôi sẽ làm thế.
Hít sau và bình tĩnh nào!

368
00:13:21,312-->00:13:22,513
Thôi nào! Hãy đi tắm đi! Được không?

368
00:13:23,112-->00:13:25,513
Nhưng mà...

368
00:13:26,112-->00:13:27,513
Anh sẽ vào với em nhé?

368
00:13:33,512-->00:13:36,113
Tại sao mình lại nói thế?

368
00:13:36,512-->00:13:38,313
Thông thường tôi không nghĩ 
mình sẽ chấp nhận chuyện này!

368
00:13:38,512-->00:13:42,513
Mình đang nói gì vậy?
Lẽ ra mình phải để Kotori 
vào phòng tắm một mình chứ?

368
00:13:43,712-->00:13:49,113
Shidou... Em vào đây. Anh quay lại đi.

368
00:13:49,312-->00:13:50,113
Ừ... ừ...

368
00:13:58,112-->00:14:01,313
Chỉ tắm một chút thôi, 
đừng có nhìn đấy.

368
00:14:01,512-->00:14:02,713
Đừng vô lý thế. Anh biết giới hạn mà.

368
00:14:03,112-->00:14:05,113
Vậy không còn cách nào khác.

368
00:14:05,312-->00:14:06,713
Ừ... Um...

368
00:14:07,112-->00:14:12,313
Gì thế? Anh muốn nói gì thì nói đi.
Không có gì em không thể nói.

368
00:14:12,712-->00:14:14,313
À, không, anh không nghĩ
rốt cuộc lại thành ra thế này...

368
00:14:14,712-->00:14:23,313
Em có thể đi đến những nơi khác. 
Nhưng nếu không có anh, 
chẳng có ý nghĩa gì cả.

368
00:14:23,513-->00:14:25,113
Hở? Gì cơ? Xin lỗi, anh không nghe rõ.

368
00:14:25,312-->00:14:26,313
Em đang nói về anh đấy.

368
00:14:26,512-->00:14:28,113
À, anh đang nghĩ chuyện cũ.

368
00:14:29,112-->00:14:30,313
Chúng ta đã từng tắm chung thế này.

368
00:14:30,912-->00:14:34,113
Em không nhớ gì cả.

368
00:14:35,112-->00:14:37,113
Không đơn giản như vậy.
Bởi vì đó là ký ức với 
em gái quý giá của anh...

368
00:14:37,312-->00:14:38,313
Shidou...

368
00:14:38,912-->00:14:40,113
Nhân tiện, cơ thể em vẫn chưa phát triển...

368
00:14:41,112-->00:14:42,113
Đau quá! Em làm gì vậy?

368
00:14:42,712-->00:14:45,113
Anh đang để ý cơ thể em.

368
00:14:45,912-->00:14:47,913
Sau đó anh nghĩ: 
"Em có nên lớn lên nhanh không?"

368
00:14:48,112-->00:14:52,313
Ra là vậy, suy nghĩ thật sâu sắc.

368
00:14:52,912-->00:14:54,113
Vậy em thấy sao?

368
00:14:54,512-->00:14:58,113
Đừng nói gì cả, cứ ngồi im đi!

368
00:14:58,312-->00:14:59,113
Được rồi.

368
00:14:59,913-->00:15:05,513
Phải, đúng vậy... 
Đừng nói gì cả, cứ ngồi đây thôi!

368
00:15:06,912-->00:15:07,913
Hở? Điện thoại à?

368
00:15:08,112-->00:15:09,113
Em à?

368
00:15:10,512-->00:15:16,513
Reine? Ừ... Cảm ơn chị.

368
00:15:16,912-->00:15:23,113
Tohka và Yoshino cũng qua đây sao?
Thế là thế nào?

368
00:15:23,912-->00:15:29,113
Vài phút nữa... Chờ đã, Reine?

368
00:15:29,712-->00:15:34,113
Mà chắc vẫn kịp.

368
00:15:34,512-->00:15:35,513
Tại sao, tại sao?

368
00:15:35,712-->00:15:40,113
Sẽ rất nguy hiểm nếu họ thấy
cảnh này. Shidou!

368
00:15:40,312-->00:15:41,513
Gì vậy? Tự nhiên...

368
00:15:41,912-->00:15:45,513
Anh còn định ở đây bao lâu nữa?
Ra ngoài nhanh lên!

368
00:15:48,112-->00:15:50,113
Khoan, khoan đã, Kotori...
Đừng có đột nhiên đứng dậy...

368
00:15:50,512-->00:15:54,113
Anh đang nhìn chỗ nào vậy?

368
00:15:55,112-->00:15:56,113
Uida!

368
00:16:00,312-->00:16:03,113
Shidou! Em về rồi!

368
00:16:11,112-->00:16:17,113
Vừa kịp lúc.

368
00:16:17,912-->00:16:20,113
Ha... Ha... 
Ừ, chúng ta vừa thay đồ xong.

368
00:16:21,112-->00:16:28,113
Xin lỗi nhé. Đúng lúc thật.
Tôi ước mình có thêm chút thời gian...

368
00:16:28,512-->00:16:33,113
Thế mà nói là... đúng lúc à...? 

368
00:16:34,112-->00:16:35,113
Không đúng sao?

368
00:16:37,112-->00:16:40,113
Tôi không biết!

368
00:16:59,112-->00:17:02,113
Đi đến Đối diện nhà ga?
1) Đồng ý
2) Không đồng ý

368
00:17:07,112-->00:17:09,113
Um... Rốt cuộc chẳng có buổi hẹn hò nào.

368
00:17:09,312-->00:17:11,113
Giờ mình đi mua bữa tối thôi.

368
00:17:14,912-->00:17:17,113
Trời có vẻ sắp tối rồi, phải không?

368
00:17:19,112-->00:17:19,913
Kotori?

368
00:17:20,112-->00:17:22,313
Ara, Shidou?

368
00:17:22,712-->00:17:24,113
Em đang làm gì ở đây?

368
00:17:24,312-->00:17:27,113
Em đang... nhìn bầu trời.

368
00:17:27,312-->00:17:28,113
Bầu trời?

368
00:17:28,312-->00:17:37,113
Phải... nếu nhìn vào đám mây đang tới...
Em cảm thấy chúng đột ngột
biến mất bằng cách nào đó...

368
00:17:37,712-->00:17:40,313
Đúng vậy. Có gì đó đang xảy ra,
chỉ huy Fraxinus.

368
00:17:41,112-->00:17:46,113
Vâng... nhưng vì em không thể 
thoát khỏi nó...

368
00:17:46,312-->00:17:47,113
Kotori...

368
00:17:49,712-->00:17:52,113
Shin, Kotori có ở đó không?

368
00:17:52,312-->00:17:54,113
Reine-san? Vâng, em đang ở cạnh con bé.

368
00:17:54,312-->00:18:00,113
May quá. Hệ thống hơi không ổn định.
Vì vậy tôi không xác định được vị trí.

368
00:18:01,112-->00:18:02,913
Vậy... có vấn đề gì sao?

368
00:18:03,112-->00:18:08,113
Hmm. Tôi cảm thấy có một chút 
tinh thần không ổn định.

368
00:18:08,312-->00:18:10,113
Dù có dải băng màu đen, 
Kotori trông khá yếu đuối.

368
00:18:10,312-->00:18:13,113
Shin, tôi mong cậu hãy khuyến khích Kotori.

368
00:18:13,312-->00:18:14,113
Hở? Em à?

368
00:18:14,312-->00:18:21,113
Bây giờ, gánh nặng của Kotori là rất lớn. 
Và cậu phải khuyến khích con bé 

368
00:18:21,312-->00:18:32,713
Về phía tôi, tôi sẽ quan tâm 
nhiều nhất có thể. Tuy nhiên, 
chạm vào trái tim Kotori chỉ có cậu.

368
00:18:32,912-->00:18:34,313
Dù chị nói thế... 
Em không biết phải làm gì...

368
00:18:34,512-->00:18:40,113
Không sao, em không phải suy nghĩ nhiều.
Chỉ cần làm như thường ngày thôi.

368
00:18:40,512-->00:18:41,513
Được rồi. 
Nếu em có thể làm được, em sẽ thử.

368
00:18:42,112-->00:18:43,913
Ừ, thế thì tốt.

368
00:18:45,112-->00:18:46,513
Vậy thì...

368
00:18:47,112-->00:19:03,113
1) Động viên tích cực "Làm tốt lắm!"
2) Động viên thoải mái. "Nghỉ ngơi đi!"

368
00:19:04,112-->00:19:05,513
Um... Trông em...
không bình thường, Kotori.

368
00:19:08,112-->00:19:12,113
Em đang rất mệt mỏi phải không?
Nếu là em thường ngày, em sẽ nói
"Không có gì!" đúng không?

368
00:19:13,112-->00:19:15,513
Đó là...

368
00:19:15,712-->00:19:19,513
Không sao, Kotori. Em đang làm tốt.
Nếu có vấn đề, em chắc chắn sẽ 
thoát khỏi nó. Khi xong việc, 
em có thể nghỉ ngơi.

368
00:19:19,912-->00:19:24,113
Anh vẫn chỉ có một kiểu động viên đó nhỉ.

368
00:19:24,312-->00:19:25,513
Không, đó là ...

368
00:19:26,112-->00:19:35,113
Dù anh không nói, em cũng biết!
Anh mới là người không quan tâm 
đến bản thân nên hãy lo cho mình đi!

368
00:19:35,512-->00:19:37,113
Hahahaha! Đúng là Kotori.

368
00:19:38,112-->00:19:40,113
Em không nghĩ thế.

368
00:19:42,112-->00:19:45,113
Giờ em trở lại Fraxinus đây!

368
00:19:48,112-->00:19:52,113
Đúng là Shin. Tôi thấy Kotori
đã có chút hạnh phúc đấy.

368
00:19:53,112-->00:19:54,513
Em ước mình có thể làm nhiều hơn...
Hy vọng con bé không quá sức.

368
00:19:55,912-->00:19:57,313
Kotori... chờ đã.

368
00:20:02,513-->00:20:05,113
Nghe những gì Kotori nói,
tôi cứ lo lắng về tòa tháp đó.

368
00:20:05,313-->00:20:06,713
Được! Tôi nghĩ tôi không thể
bỏ qua nó được!

368
00:20:06,912-->00:20:08,113
Tôi muốn ghé qua đó kiểm tra...

368
00:20:09,512-->00:20:28,113
1) Đi với Tohka
2) Đi với Kotori
3) Đi một mình

368
00:20:33,112-->00:20:34,313
Kotori, em cũng đi à?

368
00:20:35,912-->00:20:37,113
Gì cơ?

368
00:20:37,312-->00:20:40,113
Không, chỉ là anh muốn đến tháp Tengu.

368
00:20:40,512-->00:20:47,513
Anh vẫn nghĩ đến chuyện đó à?
Anh đang lo cái gì vậy?

368
00:20:47,712-->00:20:50,113
Anh không biết... Không rõ nữa.
Vậy nên anh muốn xác nhận...

368
00:20:50,312-->00:20:51,513
Nếu có thể, em có muốn đi không?

368
00:20:52,112-->00:20:55,513
Em à? Đêm nay sao?

368
00:20:55,712-->00:20:59,113
Không... Nhìn xem. Nếu không có gì,
chúng ta có thể lên tháp
ngắm thành phố buổi đêm...

370
00:21:00,112-->00:21:06,113
Ý anh là, anh đang định...
rủ em ra ngoài hẹn hò ban đêm nữa sao?

368
00:21:06,912-->00:21:09,313
Như thế... có được không? 
Nếu em không thích thì...

368
00:21:10,112-->00:21:18,313
Nếu là hẹn hò thì cứ nói luôn đi...
Cứ vòng vo...

368
00:21:18,513-->00:21:20,513
Đúng vậy... Anh nghĩ mình nên
có chút không khí buổi đêm.

368
00:21:21,112-->00:21:26,113
Thôi được rồi... Chờ em một chút.

368
00:21:26,512-->00:21:27,513
Ừ

368
00:21:33,112-->00:21:34,913
Có nhiều người quá. 
Dường như tòa tháp cũng đang nổi điên.

368
00:21:36,912-->00:21:39,513
Tượng đài cũng không xa lạ với anh...
Cảm giác giống như đã đến đây...

368
00:21:40,112-->00:21:48,113
Này, em không thấy gì lạ cả.
Anh lúc nào cũng bận, 
sao anh có thể đến đây được chứ?

368
00:21:48,512-->00:21:51,313
Không, không được...
Anh cần phải xem kỹ hơn...

368
00:21:52,512-->00:21:54,513
Shidou, cúi xuống!

368
00:21:59,912-->00:22:01,113
Chuyện... chuyện gì vậy?

368
00:22:01,512-->00:22:03,113
Chuyện gì đang xảy ra vậy?

368
00:22:04,712-->00:22:06,513
Thiên... Thiên Sứ! 
Có phải Tinh Linh không?

368
00:22:06,712-->00:22:09,113
Nó khác với "Thiên Sứ" được dùng 
bởi Tinh Linh. Ý tôi là, nó giống như 
"Guardian" trong các game...

368
00:22:12,312-->00:22:13,513
Uida!

368
00:22:14,112-->00:22:16,113
Shidou!

368
00:22:17,112-->00:22:18,713
Anh không sao! May mà suýt trúng!

368
00:22:20,112-->00:22:21,113
Cái quái gì thế này?

368
00:22:24,112-->00:22:26,113
Aaa! Suýt trúng nữa!

368
00:22:26,312-->00:22:31,913
Nó là một lời cảnh báo? 
Đừng để nó đến gần chúng ta.

368
00:22:32,112-->00:22:33,313
Vậy chắc chắn ở đây có gì đó!

368
00:22:36,112-->00:22:37,113
Hả? Nó sắp tấn công nữa?

368
00:22:38,112-->00:22:39,113
Shidou!

368
00:22:45,112-->00:22:46,313
Kotori! Em...

368
00:22:46,912-->00:22:49,713
Đừng di chuyển, Shidou! Camael!

368
00:22:52,112-->00:22:52,913
Ca... Camael!

368
00:22:53,112-->00:22:54,113
Megido!

368
00:22:55,313-->00:22:56,513
Kotori!

368
00:22:59,712-->00:23:01,513
Nó là cái gì vậy?

368
00:23:07,312-->00:23:10,513
Nó đang biến mất... Giống như 
kết giới của Tinh Linh đang biến mất...

368
00:23:17,112-->00:23:18,113
Kotori! Em có sao không?

368
00:23:19,113-->00:23:23,113
Em không sao. 
Shidou... Em nghĩ giờ an toàn rồi.

368
00:23:23,312-->00:23:25,113
Cảm ơn em. Em đã cứu anh.

368
00:23:25,912-->00:23:31,913
Chỉ là... Trong tình huống đó, 
bắt buộc phải chiến đấu thôi!

368
00:23:32,112-->00:23:34,513
Khi nó xuất hiện, đã có gì đó xảy ra. 
Em có thấy không?

368
00:23:35,112-->00:23:40,113
Em cảm thấy rất nhiều sức mạnh... 
Chuyện đó không bình thường.

368
00:23:40,312-->00:23:44,113
Đúng vậy. Đây không phải là một 
di tích bình thường.
Nó có thể có liên quan đến 
kết giới xung quanh thành phố Tengu.

368
00:23:44,512-->00:23:49,113
Vâng... Em không biết 
anh có thể nghĩ đến thế đấy.

368
00:23:49,912-->00:23:51,313
Anh trai em là vậy sao?

368
00:23:51,912-->00:24:03,113
Thật ngốc để nói...
Nhưng đây không phải việc dễ dàng. 
Em sẽ liên lạc với Fraxinus để điều tra.

368
00:24:03,312-->00:24:05,713
Dù sao, anh nghĩ hôm nay đến đây thôi.
Anh e rằng không thể tới đài quan sát đâu.

368
00:24:06,112-->00:24:11,713
Ừ... Chúng ta về nhà thôi.
Lần khác chúng sẽ quay lại.

368
00:24:11,913-->00:24:13,113
Ừ... chắc chắn là vậy.

368
00:24:14,112-->00:24:16,113
Chắc chắn

368
00:24:21,912-->00:24:22,913
Aaaa... Mệt quá.

368
00:24:23,112-->00:24:25,513
Thứ xuất hiện ở tháp Tengu là gì vậy?
Đó cũng là Tinh Linh sao?

368
00:24:25,712-->00:24:27,313
Tôi không biết... 
Tôi nghĩ mình nên để cho Kotori...

368
00:24:27,712-->00:24:29,913
Tuy nhiên, có một điều tôi phát hiện ra.
Có thể tôi đoán sai...

368
00:24:30,112-->00:24:33,513
Một cái gì đó rất lạ... 
Một cái gì đó không đúng.
Tôi đang đổ lỗi cho tình trạng
cơ thể mình. Nhưng có lẽ không phải.

368
00:24:33,912-->00:24:36,913
Tôi cảm thấy không thoải mái, 
nhưng với người khác thì bình thường.
Có gì đó kỳ lạ mà chỉ mình tôi nhận thấy được.

368
00:24:37,113-->00:24:39,913
Phải chăng chỉ có mình tôi
nhận ra sự thay đổi này,
còn những người khác thì không?

368
00:24:40,112-->00:24:41,113
Có khi nào một Tinh Linh có sức mạnh như vậy...?

368
00:24:41,312-->00:24:43,113
Nếu tôi nói, Kotori sẽ xem tôi
như một thằng ngốc.

368
00:24:43,312-->00:24:46,113
Bây giờ, những gì tôi phải làm là...
Đừng để Yoshino hay Tohka biết.
Tôi sẽ tiếp tục hẹn hò 
và trấn tĩnh bản thân mình.

368
00:24:46,312-->00:24:48,113
Giờ đây, tôi chỉ tập trung 
vào những gì tôi có thể làm.

368
00:24:55,112-->00:25:02,113
Ngày 30 tháng 6

368
00:25:02,112-->00:25:03,113

