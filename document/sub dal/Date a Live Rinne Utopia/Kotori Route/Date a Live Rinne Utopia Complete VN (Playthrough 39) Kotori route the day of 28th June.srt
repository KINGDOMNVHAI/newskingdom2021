﻿0
00:00:06,112-->00:00:07,513
Cô... cô là...

0
00:00:08,512-->00:00:11,113
Shidou? Không sao chứ?

1
00:00:11,512-->00:00:12,513
Cô là ai?

2
00:00:17,112-->00:00:18,112
Gì vậy?

3
00:00:18,312-->00:00:22,513
Shi... Shidou?

4
00:00:24,312-->00:00:25,313
Hở?

5
00:00:30,112-->00:00:31,513
Wa! Ri... Ri... Rinne, tớ xin lỗi!

6
00:00:31,912-->00:00:37,513
Không có gì đâu. 
Tớ chỉ hơi bất ngờ thôi...

7
00:00:38,112-->00:00:40,513
Không, tớ xin lỗi!
Hôm qua cũng vậy. Tại sao chuyện này xảy ra?
Có phải vì giấc mơ kỳ lạ đó?

8
00:00:41,312-->00:00:42,813
Giấc mơ kỳ lạ à?

9
00:00:43,512-->00:00:45,113
Không, không có gì đâu. 
Dù sao thì, tớ xin lỗi!

10
00:00:46,112-->00:00:51,113
Giấc mơ chỉ là giấc mơ thôi.
Cậu không cần lo lắng quá.

11
00:00:51,312-->00:00:52,513
Tớ cũng mong là vậy...

12
00:00:53,112-->00:00:56,113
Shidou...  Cậu mệt à?

13
00:00:56,512-->00:00:58,113
Không, ổn thôi. Tại hôm qua mệt quá.
Tớ sẽ xuống ngay.

14
00:00:59,112-->00:01:04,113
Tớ cũng hy vọng vậy. 
Vậy tớ xuống trước nhé.

15
00:01:04,312-->00:01:05,513
Được rồi.

16
00:01:10,912-->00:01:12,113
Vậy mọi người đâu?

17
00:01:12,512-->00:01:23,113
À, phải rồi. Yoshino-chan cảm thấy 
không khỏe. Kotori-chan đưa em ấy 
đến bệnh viện vào sáng sớm. Tớ cũng muốn 
đi theo nhưng họ bảo tớ nên ở nhà với cậu.

18
00:01:23,912-->00:01:25,113
Yoshino đi bệnh viện sao? Tình hình sao rồi?

19
00:01:25,512-->00:01:30,113
Kotori-chan nói không có gì nghiêm trọng, nhưng...

20
00:01:30,312-->00:01:31,513
Nhưng vẫn còn đáng lo ngại, phải không?

20
00:01:31,912-->00:01:33,513
Phải...

21
00:01:33,812-->00:01:36,113
Nhiều khả năng là Kotori đưa cô bé
lên Fraxinus. Mọi thứ sẽ ổn thôi. Nhưng mà...

22
00:01:36,312-->00:01:37,513
Vậy còn Tohka? Cô ấy vẫn đang ngủ à?

23
00:01:38,112-->00:01:44,113
À không. Hôm nay Tohka 
đã đến trường trước rồi.

24
00:01:44,312-->00:01:46,113
Cái gì? Thật sao? Sự kiện lớn đấy!

25
00:01:46,912-->00:01:51,513
Tớ cũng ngạc nhiên khi thấy
cậu và Tohka-chan không đi cùng nhau đấy.

25
00:01:51,912-->00:01:53,113
À, không, ý tớ không phải thế.

25
00:01:53,312-->00:01:56,113
Có khi nào Yoshino và Tohka... 
Không, nếu vậy Kotori đã đưa Tohka đi rồi.

25
00:01:56,512-->00:01:59,513
À, cậu ngồi đi. Tớ sẽ dọn bữa sáng.

25
00:02:00,112-->00:02:01,113
À, được rồi. Cảm ơn cậu.

25
00:02:01,712-->00:02:04,113
Hôm nay không có ai cả.
Chỉ có chúng tôi ăn sáng cùng nhau.

25
00:02:05,112-->00:02:11,313
Để cậu đợi lâu. Tớ đã hâm nóng súp miso. 
Cơm nấu chín rồi đấy. Cậu thấy sao?

25
00:02:11,812-->00:02:13,113
Cảm ơn cậu. Itadakimasu.

25
00:02:14,112-->00:02:19,513
Xin lỗi nhé. Sáng nay khá bận rộn.
Tớ chỉ có thể chuẩn bị món salad 
và thịt xông khói thôi.

25
00:02:20,112-->00:02:22,513
Không, thế này là đủ rồi. Hơn nữa, nếu Yoshino
bị bệnh, vậy chắc đã có nhiều việc phải làm...

25
00:02:23,112-->00:02:24,813
Tôi cảm thấy rằng tôi là người 
phải xin lỗi, vì tôi chỉ ngủ.

25
00:02:25,112-->00:02:33,113
À, nếu được thì cậu mang
bento cho Tohka-chan nhé. 
Tohka-chan quên mang bento rồi.

25
00:02:33,912-->00:02:35,313
À được rồi. Đống bento kia nhiều thật.

25
00:02:36,112-->00:02:38,113
Nhờ cậu nhé.

25
00:02:39,512-->00:02:40,513
Rinne...

25
00:02:41,112-->00:02:41,913
Gì vậy?

25
00:02:42,112-->00:02:43,113
Không, chỉ là...

25
00:02:43,312-->00:02:46,113
Um... Tôi muốn nói chuyện, nhưng...
Một lần nữa, tôi không biết phải nói gì 
trong tình huống này!

25
00:02:47,112-->00:02:53,113
Sao vậy Shidou?
Mặt cậu trông hơi đỏ, cậu lại bệnh à?

25
00:02:53,312-->00:02:55,513
Không, không phải! Tớ không sao.

25
00:02:56,112-->00:03:02,513
Vậy à? Giống như cậu đang 
cố gắng làm gì đó.

25
00:03:02,912-->00:03:04,113
À... không, tớ xin lỗi.

25
00:03:04,712-->00:03:09,113
Cậu không cần xin lỗi.
Cậu lúc nào cũng thế mà.

25
00:03:09,912-->00:03:11,313
Xấu hổ thật. Tôi phải làm gì đó...
Mở TV! Đúng rồi!

25
00:03:11,912-->00:03:14,113
Sau đây, tin tức tiếp theo...

25
00:03:14,312-->00:03:16,513
Có tin tức trong không gian 
nơi tôi và Rinne đang ở một mình.
Không khí trở nên thoải mái hẳn.

26
00:03:17,112-->00:03:18,313
Ồ, vậy cậu không tham gia CLB à?

26
00:03:18,912-->00:03:22,313
Hôm qua cậu ấy đã thi đấu rồi.
Nó có thể bị trì hoãn, trước đây cũng thế.

26
00:03:22,912-->00:03:28,113
Hôm nay tớ được nghỉ
Hội thao đã kết thúc
và đây cũng là khoảng thời gian nghỉ.

26
00:03:28,312-->00:03:29,513
Vậy thay vào đó là...

26
00:03:30,112-->00:03:33,113
Cậu quên buổi thực hành à?

26
00:03:33,312-->00:03:35,513
Không, không phải thế... Hở?
Vậy là hôm nay cậu nghỉ sao?

26
00:03:36,112-->00:03:45,113
Thực ra tớ có chút việc và ra ngoài 
sớm hơn một chút. Xin lỗi nhé.

26
00:03:45,512-->00:03:48,113
Không, không sao cả. Tớ chỉ nghĩ 
nếu không có buổi luyện tập, 
chúng ta có thể đi học cùng nhau.

26
00:03:49,112-->00:03:53,113
Thực ra, tớ sẽ đi với cậu...

27
00:03:53,312-->00:03:54,313
Không cần lắm đâu...

28
00:03:55,212-->00:04:03,513
Tin khẩn cấp. Một người đàn ông
đang giữ con tin ở bưu điện.
Đó là một nhân viên...

28
00:04:03,913-->00:04:05,313
Cái gì? Đùa sao? 
Chuyện này xảy ra ngay bây giờ à?

28
00:04:05,512-->00:04:08,113
Tôi nhìn màn hình TV và nghe tin tức.

28
00:04:08,312-->00:04:09,913
Trên màn hình là một bưu điện 
trông như bị che khuất.

28
00:04:10,312-->00:04:14,513
Đây có phải là bưu điện trước nhà ga không?

28
00:04:14,912-->00:04:19,113
Cảnh sát tiếp tục thuyết phục,
Không có tiếp xúc nào được nêu ra.

28
00:04:19,912-->00:04:21,113
Tớ lo lắng về những người bên trong...

28
00:04:21,312-->00:04:24,113
Nhưng đó có phải những người
cậu quen biết không?

28
00:04:24,312-->00:04:27,113
Quen biết hay không không quan trọng.
Tớ không muốn nhìn thấy 
người khác bị tổn thương.

28
00:04:27,912-->00:04:30,113
Đúng vậy... nhỉ...

28
00:04:30,312-->00:04:38,113
Chúng tôi đã kết nối với hiện trường.
Miyasaka tại hiện trường phải không? 
Đây là đài truyền hình, tình hình sao rồi?

28
00:04:38,912-->00:04:50,113
Vâng! Mọi người có thấy rõ không?
Ngay bây giờ! Một nam giới chưa xác định 
đã đột nhập vào bưu điện.
Họ đang tiến hành giải cứu con tin.

28
00:04:50,312-->00:04:52,113
Cố lên! Thế nào rồi?

28
00:04:52,312-->00:04:54,113
Trên màn hình, một người đàn ông 
bị còng tay được cảnh sát đưa ra ngoài.

28
00:04:54,312-->00:04:57,513
Cảm giác như đây là chương trình đùa vậy.
Mọi thứ diễn ra đột ngột đến mức
không thể tin là thật....

28
00:04:58,112-->00:05:02,113
Thật may là hắn đã bị bắt 
trước khi ai đó bị thương.

28
00:05:02,312-->00:05:04,113
Hở? Ồ, phải rồi. May thật...

28
00:05:04,312-->00:05:05,913
Cậu sao vậy?

28
00:05:06,312-->00:05:08,513
Không, nói thế nào nhỉ...
Tớ rất ngạc nhiên vì mọi chuyện quá dễ.

28
00:05:09,112-->00:05:17,913
Đúng là vậy... nhưng cũng tốt mà.
Chúng ta thường đi qua khu đối diện nhà ga.
Vậy nên việc này đúng ý tớ.

28
00:05:18,112-->00:05:19,513
Rinne... cậu có vẻ rất vui vì may mắn này...

28
00:05:20,112-->00:05:26,113
Ngay cả khi tớ nói như vậy,
cậu cũng không thích sao?

28
00:05:26,312-->00:05:28,113
Ừ phải... Cậu nói đúng.

28
00:05:28,512-->00:05:31,913
Đúng vậy. Cậu hiểu đơn giản đi.

28
00:05:32,112-->00:05:33,513
Phải, có lẽ tớ hơi ngu ngốc...

28
00:05:34,112-->00:05:36,513
Không sao đâu.

60
00:05:37,112-->00:05:43,113
Tớ xin lỗi, tớ phải đi rồi. 
Cậu dọn giúp tớ được không?

45
00:05:43,312-->00:05:45,113
Ừ, được rồi. Tớ sẽ dọn dẹp cho.

46
00:05:45,512-->00:05:49,113
Vậy tớ để lại cho cậu nhé. Hẹn gặp lại.

48
00:05:52,912-->00:05:55,113
Cái gì... cảm giác kỳ lạ gì vậy?
Cái gì đó không đúng. 
Tôi không biết đây là gì.

49
00:05:55,512-->00:05:57,513
Tôi có một cảm giác khó chịu kỳ lạ, 
ở đâu đó trong đầu tôi.
Giờ dọn dẹp đã...

50
00:06:02,112-->00:06:05,113
Hmm... Bây giờ còn quá sớm để đến trường.
Mình nên làm gì nhỉ?

51
00:06:05,512-->00:06:19,113
1) Hỏi Fraxinus về Yoshino
2) Đến trường

52
00:06:25,112-->00:06:26,113
Này, Tohka...

53
00:06:26,312-->00:06:27,513
Đúng là Tohka đã đến trường trước.
Thật khác mọi khi.

54
00:06:33,812-->00:06:36,513
Một cái ngáp rõ to. Kinh thật!
Phải, chỉ có thể là Tohka.

55
00:06:36,713-->00:06:38,113
Này, Tohka. Em thức khuya à?

56
00:06:38,812-->00:06:40,113
Shi... Shidou!

57
00:06:40,312-->00:06:42,113
Ừ, anh đây. Sao em ngạc nhiên thế?

58
00:06:42,512-->00:06:45,113
À, vâng! Em xin lỗi!

59
00:06:45,512-->00:06:47,113
Trông em có vẻ căng thẳng...
Có chuyện gì à?

60
00:06:47,312-->00:06:49,113
Không có gì cả!

61
00:06:49,512-->00:06:51,313
Vậy thì... Hở?
Ngón tay em bị sao thế?

62
00:06:52,912-->00:06:54,513
Đây này, em băng ngón tay nhiều thế này...

63
00:06:55,112-->00:07:00,113
Không có gì đâu! 
Chỉ thực hành chút thôi...

64
00:07:00,312-->00:07:01,513
Thực hành à? Thực hành gì?

65
00:07:02,112-->00:07:07,513
Không! Không có gì cả!
Tóm lại, anh không cần biết đâu!

66
00:07:07,912-->00:07:08,913
À, ừ...

67
00:07:09,112-->00:07:10,513
Có lẽ... tối qua cô ấy đã làm gì đó.

68
00:07:10,912-->00:07:12,513
Hãy cẩn thận đấy.
Anh không muốn thấy em bị thương.

69
00:07:13,812-->00:07:17,113
Em biết rồi... Em sẽ cố gắng...

70
00:07:17,312-->00:07:20,513
Cô ấy có vẻ đang rất cố gắng.
Nếu cô ấy muốn giấu, 
cứ để cô ấy giữ bí mật một thời gian.

71
00:07:27,112-->00:07:29,313
Tôi đã trải qua lớp học nhàm chán, 
ăn bữa trưa do Rinne làm 
và sẵn sàng tham gia lớp buổi chiều.

72
00:07:29,712-->00:07:31,513
Mặt trời vào đầu mùa hè, 
cái nóng lan tràn khắp nơi.

73
00:07:31,912-->00:07:33,713
Ngay cả hơi thở cũng khó mà kìm nén.
Cảm giác buồn ngủ như được cất đi.

74
00:07:33,913-->00:07:35,513
Tuy nhiên, lát nữa là giờ thể dục.
Dưới cái nóng của mùa hè.

75
00:07:35,912-->00:07:38,113
Thậm chí những con chữ trên bảng phấn
không lọt vào đầu tôi.

76
00:07:38,312-->00:07:41,113
Dù trong lớp, tôi vẫn nghĩ đến
buổi hẹn hò sau khi tan trường.

77
00:07:41,312-->00:07:45,113
Thực tế là... đó là suy nghĩ khiến tôi 
không giống phần còn lại của lớp.
Từng người một gục xuống.

78
00:07:45,312-->00:07:47,113
Và cuối cùng, tôi lẩm bẩm
như cầu nguyện.

79
00:07:47,312-->00:07:50,113
"Tôi đang học cho kỳ thi",
tôi có thể thoát khỏi suy nghĩ đó 
trong một vài phút không?

80
00:07:58,112-->00:07:59,513
Được rồi, đến giờ sinh hoạt lớp rồi.

81
00:08:00,112-->00:08:02,113
Đoạn này bản tiếng Hàn không dịch.
Sau này biết tiếng Nhật,
KINGDOM NVHAI sẽ dịch sau.

368
00:11:03,112-->00:11:08,113
Đi đến Cổng trường?
1) Đồng ý
2) Không đồng ý

68
00:11:13,512-->00:11:15,313
Xem nào, hôm nay là một cuộc hẹn khác...
Có lẽ nên chọn Kotori nhỉ?

368
00:11:15,512-->00:11:16,513
Được rồi, để gọi con bé.

368
00:11:18,512-->00:11:19,713
Có cuộc gọi? Từ Kotori à?

368
00:11:19,912-->00:11:21,113
Tuyệt! Vừa đúng lúc.

368
00:11:21,512-->00:11:22,313
Alo...

368
00:11:22,512-->00:11:26,113
Shidou đấy à? 
Em có chuyện muốn nói với anh.

368
00:11:26,812-->00:11:28,113
Kotori. Em gọi đúng lúc lắm.

368
00:11:28,512-->00:11:30,113
Anh đang nói gì cơ?

368
00:11:30,512-->00:11:32,913
Anh đang định gọi cho em.
Anh muốn rủ em đi hẹn hò.

368
00:11:33,112-->00:11:37,113
Vậy à? Vậy thì đến nhanh đi.
Em đang ở công viên.

368
00:11:37,712-->00:11:38,113
Công viên à? Anh sẽ đến.

368
00:11:38,512-->00:11:39,513
Vậy nhé.

368
00:11:40,112-->00:11:42,113
À, chờ đã Kotori. 
Em gọi anh có phải em có gì cho anh không?

368
00:11:42,513-->00:11:46,113
À, đúng là vậy nhưng...
em đã xong rồi.

368
00:11:47,513-->00:11:54,313
Nghĩa là em cũng nghĩ giống anh đấy.
Vậy hẹn gặp lại.

368
00:11:54,712-->00:11:56,113
Nghĩ giống mình...
Con bé nói về buổi hẹn hò à?

368
00:11:56,312-->00:11:58,513
Hôm qua, con bé nói không hứng thú
với việc đó... Giờ con bé nói
ngược lại khiến tôi thắc mắc.

368
00:12:22,112-->00:12:22,913
Kotori đâu nhỉ...

368
00:12:03,812-->00:12:06,113
Shidou, ở đây này.

368
00:12:07,912-->00:12:09,113
Trễ quá đấy

368
00:12:09,312-->00:12:11,513
Từ lúc cúp máy, anh đã chạy đến đây
ngay lập tức đấy.

368
00:12:12,112-->00:12:13,513
Đừng xạo với em.

368
00:12:13,912-->00:12:15,113
À, vâng... xin lỗi

368
00:12:15,512-->00:12:19,513
Vậy chúng ta ngồi ở ghế kia đi.

368
00:12:20,112-->00:12:22,513
Hở? Chúng ta không đi đâu à?
Đây là hẹn hò mà.

368
00:12:22,912-->00:12:26,113
Hẹn hò ở công viên không được sao?

368
00:12:26,313-->00:12:35,513
Với lại, em không có nhiều tiền.
Mỗi lần đi hẹn hò là tốn tiền của anh.
Anh không thấy phiền à?

368
00:12:35,912-->00:12:37,113
Phải, đó là thực tế xảy ra
với ví của anh hiện giờ...

368
00:12:37,312-->00:12:40,513
Nhưng... sau một ngày dài chờ đợi,
chỉ ngồi ở băng ghế công viên cùng nhau,
cảm giác hơi kỳ cục với con gái...

368
00:12:41,112-->00:12:47,113
Em đã nói với em thì không sao. 
Ngồi xuống đi!

368
00:12:47,312-->00:12:48,313
Ừ, được rồi...

368
00:12:48,812-->00:12:53,113
Vậy... anh xác nhận đi.

368
00:12:53,312-->00:12:54,513
Gì cơ?

368
00:12:55,112-->00:12:58,513
Những gì anh nói hôm qua 
không phải nói dối đấy chứ?

368
00:12:58,912-->00:12:59,913
Nói gì cơ?

368
00:13:00,512-->00:13:10,513
"Nếu có bất cứ điều gì anh có thể làm, 
hãy nói anh biết. Anh sẽ làm mọi thứ vì em."
Chắc chắn anh đã nói thế.

368
00:13:10,912-->00:13:11,913
À phải...

368
00:13:12,112-->00:13:16,313
Còn nữa, em muốn nhờ anh.

368
00:13:16,812-->00:13:17,913
Ừ, gì vậy?

368
00:13:18,112-->00:13:25,513
Nhưng em hỏi chuyện này...
có lẽ anh thấy hơi kỳ quặc.

368
00:13:25,912-->00:13:28,513
Không đâu. Cứ nói đi.
Anh sẽ cho em bất kỳ thứ gì em yêu cầu.
Anh là Onii-chan của em mà.

368
00:13:29,112-->00:13:35,113
Thật không? Anh vừa yêu cầu em
nói phải không?

368
00:13:35,312-->00:13:36,913
Hở? Đúng rồi.

368
00:13:37,112-->00:13:43,113
Chỉ xác nhận lại thôi.
Có đúng là anh muốn em nói không?

368
00:13:43,312-->00:13:45,913
Kotori, đôi mắt trông rất nghiêm túc...
Em ấy muốn gì vậy?

368
00:13:46,112-->00:13:47,713
Nhưng vì lý do nào đó,
tôi nghĩ chắc không có chuyện gì lớn...

368
00:13:47,912-->00:13:49,113
Được... bất cứ điều gì.

368
00:13:29,112-->00:13:30,113
Được rồi. Vậy...

368
00:13:48,112-->00:13:49,513
Ko... Kotori?

368
00:13:15,512-->00:13:19,113
Uida! Đừng cử động!

368
00:13:29,112-->00:13:30,113
À, xin lỗi... Khoan đã, không phải thế này!

368
00:13:48,112-->00:13:49,513
Đây là gì?

368
00:13:15,512-->00:13:19,113
Anh không biết "Hizamakura" (gối đùi) à?

368
00:13:29,112-->00:13:30,113
Ý anh không phải thế!

368
00:13:48,112-->00:13:49,513
Đây là ngoài trời,
nhưng không có ai phải không?

368
00:13:29,112-->00:13:30,113
Hở? À... đúng vậy.

368
00:13:49,112-->00:13:40,113
Anh nói anh sẽ làm bất cứ điều gì.
Gối đùi quá khó với anh à?

368
00:13:49,112-->00:13:40,113
Hay đó là một lời nói dối?

368
00:13:49,112-->00:13:40,113
Không, không phải...

368
00:13:49,112-->00:13:40,113
Vậy thì im lặng như một cái gối đùi đi!

368
00:13:49,112-->00:13:40,113
Được rồi. Nhưng nếu có ai thấy
thì chúng ta dừng lại nhé?

368
00:13:49,112-->00:13:40,113
Em biết rồi. Em biết khi nào
chúng ta nên dừng lại.

368
00:13:49,112-->00:13:40,113
...Này, Kotori.

368
00:13:49,112-->00:13:40,113
Yên lặng đi! Em muốn nghỉ ngơi một chút...

368
00:13:49,112-->00:13:40,113
Ừ... anh xin lỗi.

368
00:13:49,112-->00:13:40,113


368
00:13:49,112-->00:13:40,113


368
00:13:49,112-->00:13:40,113


368
00:13:49,112-->00:13:40,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113




368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113


368
00:14:19,212-->00:14:21,113




368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113


368
00:15:19,212-->00:15:21,113
Con bé ngủ rồi à?

368
00:15:19,212-->00:15:21,113
Cũng được một lúc rồi...
Mình không nên di chuyển...

368
00:15:35,112-->00:15:37,913
Kotori... con bé chắc mệt lắm.
Trông mặt con bé, chắc nó phải làm việc 
vất vả đến tận đêm.

368
00:15:38,112-->00:15:40,113
Có vẻ con bé đang ngủ rất say.
Màu nơ của nó không khiến con bé 
đổi tính cách như mọi ngày.

368
00:15:40,312-->00:15:41,313
Hở? Waaa...!

368
00:15:41,512-->00:15:42,513
Mình có thể nhìn bên trong váy.

368
00:15:42,812-->00:15:45,113
Vậy mình nên gọi con bé dậy
hay cứ để nó ngủ?

368
00:15:45,312-->00:15:46,513
Mình nên làm gì?

368
00:15:47,112-->00:16:04,113
1) Kotori cũng là con gái. Gọi con bé dậy
và nói với nó váy nó có thể nhìn thấy.
2) Con bé vẫn là trẻ con. Mình chỉ cần nhìn thôi.

368
00:16:04,512-->00:16:07,513
Con bé vẫn là trẻ con. Mình chỉ cần nhìn thôi.

368
00:16:08,112-->00:16:10,113
Cứ để Kotori ngủ. 
Mình sẽ lưu giữ khoảnh khắc này thật kỹ.

368
00:16:18,512-->00:16:22,113
Shi... dou?

368
00:16:22,312-->00:16:23,813
Aaa... Em dậy rồi à...

368
00:16:24,112-->00:16:29,113
Anh nhìn gì em...?

368
00:16:37,512-->00:16:42,113
Khoan đã! Thế này là sao?

368
00:16:42,312-->00:16:43,513
Ơ... Um...

368
00:16:43,912-->00:16:48,513
Anh... nãy giờ anh nhìn em đấy à?

368
00:16:48,912-->00:16:52,113
Không, không phải... Anh không muốn xem.
Chỉ là váy em tự lật lên thôi... 

368
00:16:52,512-->00:16:57,113
Đồ... đồ biến thái! (Ero-nii)

368
00:16:57,512-->00:16:58,513
Hự!

368
00:16:59,112-->00:17:01,113
Em không muốn nhìn thấy anh nữa!

368
00:17:02,112-->00:17:03,813
Aaa... sao mình xui thế...?

368
00:17:21,112-->00:17:23,113
Đi đến Phòng Gần Cầu Thang?
1) Đồng ý
2) Không đồng ý

368
00:17:38,512-->00:17:42,613
Đi đến Đối Diện Nhà Ga?
1) Đồng ý
2) Không đồng ý

368
00:17:47,112-->00:17:49,113
Mình đã mua những gì cần thiết.
Mình nghĩ vẫn có thể hẹn hò vào lúc này.

368
00:17:49,312-->00:17:51,513
Kotori về nhà chưa?
Chúng ta sẽ về nhà chung...

368
00:17:52,112-->00:17:53,513
Alo, Fraxinus đấy à?

368
00:17:54,112-->00:17:56,713
Chào Shidou-kun, cậu rảnh không?

368
00:17:56,913-->00:17:59,113
Kannazuki-san đấy à? Có chuyện gì vậy?

368
00:17:59,312-->00:18:04,113
Thực ra là... Tôi có việc muốn nhờ cậu...

368
00:18:04,312-->00:18:05,513
Là gì vậy?

368
00:18:05,712-->00:18:09,513
Thực ra... Tôi muốn cậu giúp tôi thử nghiệm.

368
00:18:09,912-->00:18:11,113
Đột nhiên, em có linh cảm xấu...

368
00:18:11,512-->00:18:17,513
Đừng nói thế. Đây là thử nghiệm quan trọng,
liên quan đến tương lai của Fraxinus đấy.

368
00:18:17,712-->00:18:18,713
Ý em là... thử nghiệm gì vậy?

368
00:18:18,913-->00:18:24,713
Thực ra, cá nhân tôi 
có một hệ thống AI mới.
Tôi đang cố gắng phát triển nó.

368
00:18:24,912-->00:18:25,913
Cá nhân à?

368
00:18:26,112-->00:18:33,313
AI của Fraxinus rất tuyệt vời, 
nhưng nếu có chuyện gì xảy ra
mà không có bản sao thì rất nguy hiểm.

368
00:18:33,712-->00:18:38,513
Trong trường hợp đó, chúng tôi cần 
một hệ thống hỗ trợ khác.

368
00:18:38,912-->00:18:40,313
Có vẻ hệ thống không đảm bảo lắm nhỉ...

368
00:18:40,512-->00:18:47,513
Nào, đừng nói thế!
Tôi đảm bảo có sự hợp tác đầy đủ
bởi các kỹ thuật viên Fraxinus.

368
00:18:47,712-->00:18:49,913
Sao em vẫn thấy lo nhỉ?
Vậy hệ thống như thế nào?

368
00:18:50,112-->00:18:57,313
Đây là một kiệt tác nổi tiểng,
một tạo vật của Chúa.

368
00:18:57,513-->00:19:04,513
Kiệt tác Tsundere có tên là
"Em không quan tâm đến Onii-chan chút nào!"

368
00:19:04,912-->00:19:06,713
Ai đặt cái tên đó thế?

368
00:19:06,912-->00:19:12,113
Nào, đừng nói thế.
Chỉ huy cũng đã đồng ý rồi.

368
00:19:12,312-->00:19:13,313
Hở? Kotori biết à?

368
00:19:13,512-->00:19:20,113
Phải. Để chuẩn bị cho tình huống xấu,
chỉ huy đã cho phép phát triển hệ thống này.

368
00:19:20,312-->00:19:23,513
Kotori? Có lẽ là thật...
Nếu anh ta nói dối, hình phạt nào
anh ta sẽ phải chịu đây?

368
00:19:23,912-->00:19:26,113
Vậy... mô phỏng này...
cần gặp ai và làm gì?

368
00:19:26,312-->00:19:32,713
Tất nhiên! Ngay bây giờ!
Đối thủ của cậu là chỉ huy 
mà chúng ta tôn sùng!

368
00:19:33,112-->00:19:34,9513
Chuyện này trở nên điên rồ
một cách nghiêm túc đấy!

368
00:19:35,112-->00:19:39,113
Không sao đâu! Chỉ huy về nhà rồi đấy!

368
00:19:39,312-->00:19:40,313
Vậy à...?

368
00:19:40,512-->00:19:44,113
Tôi cũng nhận được sự cho phép 
của chỉ huy rồi. Giờ trông chờ vào cậu đấy.

368
00:19:44,312-->00:19:46,113
Được rồi. Giờ em sẽ gặp Kotori phải không?

368
00:19:46,312-->00:19:47,513
Mong cậu giúp đỡ!

368
00:19:47,912-->00:19:49,513
Thật sự, tôi cảm thấy rất lo lắng...

368
00:19:54,112-->00:19:55,313
Kotori ơi!

368
00:19:55,712-->00:19:57,113
Gì vậy?

368
00:19:57,912-->00:19:59,513
Wow... ánh mắt nhìn đáng sợ thật.

368
00:19:59,713-->00:20:00,913
Kannazuki-san, em đã gặp Kotori rồi.

368
00:20:01,112-->00:20:09,113
Được rồi. Vị trí: nhà Itsuka.
Thời gian: ăn tối.
Tìm kiếm! Cậu đợi một chút nhé!

368
00:20:10,113-->00:20:13,113
Sao vậy? Anh muốn nói gì?

368
00:20:13,513-->00:20:14,913
À... chờ chút đã.

368
00:20:15,113-->00:20:19,113
Có kết quả rồi! Có 2 lựa chọn...

368
00:20:19,312-->00:20:23,513
1) "Trái tim nóng bỏng này 
không thể dừng lại." 
Sau đó đột nhiên quỳ xuống!

368
00:20:24,112-->00:20:28,113
2) "Hãy nhìn anh đây!" 
và bắt đầu cởi quần áo ra!

368
00:20:28,312-->00:20:30,113
Nào, hãy chọn đi Shidou-kun!

368
00:20:30,312-->00:20:31,513
Đợi đã... Cái gì thế này?

368
00:20:32,112-->00:20:39,513
"Đợi đã" gì cơ? Chẳng phải anh gọi em sao?
Anh muốn nói gì?

368
00:20:39,812-->00:20:42,913
Shidou-kun, nếu cậu không nhanh lên,
chỉ huy sẽ tức giận đấy!

368
00:20:45,112-->00:20:46,513
Em... em không biết!

368
00:20:47,112-->00:21:00,113
1) "Trái tim nóng bỏng này 
không thể dừng lại." 
Sau đó đột nhiên quỳ xuống!

368
00:21:01,112-->00:21:14,113
2) "Hãy nhìn anh đây!" 
và bắt đầu cởi quần áo ra!

368
00:21:14,312-->00:21:15,813
Ko... Kotori... Xin lỗi!

368
00:21:18,312-->00:21:20,113
Ko... Kotori! Trái tim nóng bỏng này...

368
00:21:20,912-->00:21:25,513
Anh đang nói gì vậy? Đồ biến thái!

368
00:21:26,112-->00:21:26,913
Uida!

368
00:21:27,112-->00:21:30,313
Rốt cuộc anh đang làm cái quái gì vậy?

368
00:21:30,512-->00:21:32,513
Tại vì... Kannazuki-san nói muốn thử 
hệ thống AI mới...

368
00:21:33,112-->00:21:44,513
Thử nghiệm? Hệ thống AI mới? Kannazuki?
Anh... anh đang bịa chuyện gì đấy?

368
00:21:45,112-->00:21:46,513
Anh ấy nói em đã cho phép...

368
00:21:46,912-->00:21:55,513
Nếu em phát triển hệ thống mới,
em sẽ tự nguyền rủa bản thân
tại sao em không nhớ.

368
00:21:56,112-->00:21:57,113
Xin... xin lỗi...

368
00:21:58,112-->00:21:59,113
Nói một lần thôi nhé, Shidou.

368
00:21:59,912-->00:22:01,313
Vâng, em gái.

368
00:22:02,112-->00:22:06,113
Không được lặp lại nữa, nhớ nhé?

368
00:22:07,112-->00:22:08,113
A... A... A... Ano...

368
00:22:09,112-->00:22:15,113
Cái nhìn lạnh lùng 
của chỉ huy, thật tuyệt vời! 
Hệ thống này quả là tuyệt vời!

368
00:22:15,312-->00:22:17,313
Cái gì? Đợi đã... Mục đích của hệ thống là...

368
00:22:17,512-->00:22:22,513
Tất nhiên, đây là mô phỏng 
cách tiếp cận mạnh mẽ với một cô gái
để tìm hiểu suy nghĩ của họ.

368
00:22:24,912-->00:22:26,113
Mình phải cho hắn ta một trận!



368
00:22:33,112-->00:22:36,513
Mình tưởng hôm nay Rinne sẽ đến.
Có lẽ cô ấy đến trễ.

368
00:22:38,112-->00:22:43,113
Shidou, tớ xin lỗi vì đến trễ.
Cậu nấu cơm chưa?

368
00:22:43,912-->00:22:47,113
À, tớ thấy trễ quá nên làm rồi.
Cũng lâu rồi tớ mới nấu cơm lại.
Vậy cậu bận gì vậy?

368
00:22:47,912-->00:22:53,113
À, ừ. Tớ không sao?
Tớ chỉ có chút việc thôi...

368
00:22:53,812-->00:22:55,113
Được rồi. Công việc có tốt không?

368
00:22:55,512-->00:23:00,113
À vâng, cũng tốt. Tớ xin lỗi, 
tớ không thể liên lạc được với cậu.

368
00:23:00,912-->00:23:02,713
Được rồi, không có gì đâu.
Đó là việc nên làm mà.

368
00:23:03,313-->00:23:04,513
Nhưng mà...

368
00:23:04,912-->00:23:08,113
Rinne, cậu không thể suốt ngày
chạy sang nhà tớ, đúng không?
Cậu đã giúp quá nhiều rồi.

368
00:23:09,112-->00:23:13,113
Không phải vậy đâu! 
Tớ cũng muốn được qua đây...

368
00:23:13,312-->00:23:16,113
Không, dù là vậy, giờ tớ mới chỉ
chuẩn bị, chưa cần thiết để cậu qua.

368
00:23:18,513-->00:23:21,113
Ý tớ là... tớ nghĩ mình vẫn 
đủ sức để chuẩn bị bữa ăn.

368
00:23:21,912-->00:23:28,513
À... phải rồi... 
tớ có lẽ... đã quấy rầy cậu...

368
00:23:29,112-->00:23:30,513
Không, tớ nợ cậu! 
Cậu thực sự đã giúp tớ rất nhiều!

368
00:23:33,112-->00:23:36,113
Nhưng cậu cũng nên dành thời gian
cho bản thân. Chưa kể nếu không làm,
kỹ năng của tớ cũng sẽ mai một.

368
00:23:37,112-->00:23:43,513
Thế à...? Không còn cách nào khác.
Cậu đúng là một người thẳng thắn đấy.

368
00:23:44,112-->00:23:45,113
Rinne cũng thế. 
Tớ thích sự nhanh nhẹn của cậu.

368
00:23:45,512-->00:23:49,913
Nhưng... cậu có thể dựa vào tớ mà.

368
00:23:50,112-->00:23:51,113
Hở? Cậu vừa nói gì?

368
00:23:52,112-->00:23:58,513
Không, Không có gì.
Tóm lại, giờ tớ sẽ bắt đầu nấu.
Cậu giúp tớ nấu cơm nhé?

368
00:23:58,913-->00:24:00,513
Được rồi, 2 người vẫn tốt hơn 1 người mà.

368
00:24:01,112-->00:24:03,113
Cứ để cho tớ.






368
00:24:08,112-->00:24:08,913
Huaaaaaaa...

368
00:24:09,312-->00:24:13,113
Gì mà ngáp to thế.

368
00:24:13,312-->00:24:15,113
Anh thấy mệt quá.

368
00:24:15,512-->00:24:20,113
Vậy anh ngủ đi. Mai em có việc làm nữa.

368
00:24:20,912-->00:24:21,913
Được rồi...

368
00:24:24,112-->00:24:26,513
Bây giờ, trong phòng chỉ có tôi và Kotori...
Tôi không biết liệu tôi có thể...

368
00:24:27,812-->00:24:28,913
Gì vậy?

368
00:24:29,112-->00:24:31,513
À, không... 
Em hôm nay chắc đã ngủ nhiều rồi.

368
00:24:32,112-->00:24:33,113
Cũng không hẳn...

368
00:24:33,312-->00:24:34,113
À, ừ...

368
00:24:37,112-->00:24:39,313
Sáng nay, trời có vẻ hơi lạnh...

368
00:24:39,712-->00:24:41,113
Không đâu.

368
00:24:42,712-->00:24:50,113
Thôi nào, bây giờ anh không biết
mai sẽ làm gì phải không?

368
00:24:50,712-->00:24:51,913
Em cũng vậy sao?

368
00:24:52,512-->00:24:58,113
Không phải, ý em là... 
Điều quan trọng là phải làm tốt đúng không?

368
00:24:58,912-->00:25:00,113
Đúng vậy.

368
00:25:00,712-->00:25:07,513
Vì chúng ta đang cùng ở trong 
một vị trí có trách nhiệm,
em không thể lúc nào cũng căng thẳng.

368
00:25:08,512-->00:25:17,113
Vì vậy... Trong thời gian hẹn hò,
hãy chắc chắn mình biết phải làm gì.
Đó là quan trọng nhất.

368
00:25:17,912-->00:25:18,913
Được rồi.

368
00:25:19,512-->00:25:22,113
Nói cách khác...

368
00:25:24,112-->00:25:27,513
Lần hẹn hò tiếp theo đã được xác định

368
00:25:27,912-->00:25:29,113
Ừ... ừ...

368
00:25:34,112-->00:25:37,113
Cuối cùng tôi cũng đã nghĩ về việc
học bài cho kỳ thi. Nhưng vô dụng,
tôi chẳng thể tập trung được.

368
00:25:37,312-->00:25:39,913
Có lẽ tôi vẫn chưa hồi phục hẳn.
Dù có ép bản thân, tôi vẫn rất vất vả...

368
00:25:40,112-->00:25:42,113
Có lẽ thay vì tự ép mình, tôi nên 
nghỉ ngơi một chút. Vậy sẽ tốt hơn.

368
00:25:42,512-->00:25:44,113
Trước tiên, cứ đi tắm cái đã.

368
00:25:47,112-->00:25:48,113
Huaaaaaaa...

368
00:25:53,112-->00:25:55,113
Hở? Ko... Kotori?

368
00:25:57,512-->00:26:00,113
Khoan, đợi đã! 
Thế này, mình nên nói gì...? 
Sao em không khóa cửa...?

368
00:26:00,512-->00:26:05,513
Đồ biến thái!!!!!!!!!!!

368
00:26:06,512-->00:26:07,513
Uida!

368
00:26:13,112-->00:26:15,113
Uida... Anh không cố ý.
Em biết mà, đúng không?

368
00:26:15,512-->00:26:21,113
Em biết. Nếu anh cố ý, 
anh biết hậu quả như thế nào không?

368
00:26:21,512-->00:26:22,513
Thế nào?

368
00:26:23,112-->00:26:32,113
Khi anh bị bắt, anh sẽ là tâm điểm
trong một chương trình vào ban đêm.
Bạn của anh sẽ rất quan tâm đấy.

368
00:26:32,912-->00:26:33,913
Bạn của anh à?

368
00:26:34,112-->00:26:48,113
"Đây không phải trò đùa, tôi biết 
cậu ta sẽ làm thế này một ngày nào đó.
Đó là lý do tôi gọi hắn là Sexual Beast Itsuka.
Nhưng làm thế với em gái..." như thế đấy.

368
00:26:48,912-->00:26:50,113
Nghe chân thực quá nhỉ?

368
00:26:51,112-->00:26:59,113
Em không muốn như thế.
Một con vi trùng không biết suy nghĩ.

368
00:26:59,312-->00:27:01,313
Con vi trùng... 
Dù sao, lần sau anh sẽ cẩn thận. Nhưng...

368
00:27:01,912-->00:27:03,113
Gì nữa?

368
00:27:03,312-->00:27:05,113
Em ổn chứ?

368
00:27:06,112-->00:27:08,113
Chỉ là... Anh cảm thấy em
khá mệt mỏi trong phòng tắm.

368
00:27:09,112-->00:27:14,113
Shidou, em suy nghĩ về việc sắp tới, 
và sau đó em phải đi.

368
00:27:14,512-->00:27:15,913
Vậy là em thực sự có vấn đề?

368
00:27:16,312-->00:27:20,513
Cơ thể của em vẫn ổn,
vấn đề là ở chỗ khác.

368
00:27:21,112-->00:27:23,113
Cuộc điều tra có tiến triển gì không?

368
00:27:24,112-->00:27:36,113
Hôm nay, em đã thử cho Fraxinus
có thể đánh xuyên qua kết giới được không.
Kết quả là thất bại.

368
00:27:36,912-->00:27:41,113
Nhưng sự thật đúng là 
em không thể xuyên qua nó được.

368
00:27:41,512-->00:27:43,113
Vậy là em không còn cách nào
phá hủy được kết giới sao?

368
00:27:43,512-->00:27:55,113
Phải. Ngay cả nguồn năng lượng to lớn từ Fraxinus
và điều khiển Realizer cũng không đủ.

368
00:27:56,112-->00:28:04,113
Kể cả pháo Mystletainn (pháo chính của Fraxinus)  
cũng không ăn thua. Có lẽ cưỡng chế nó là không thể.

368
00:28:04,312-->00:28:05,513
Vậy ra đó là lý do trông em mệt mỏi.

368
00:28:06,112-->00:28:13,113
Không... không phải vậy.
Không giống như anh, con gái không có 
cái đầu đơn giản vậy đâu.

368
00:28:13,312-->00:28:14,513
Được rồi, anh xin lỗi vì nghĩ đơn giản.

368
00:28:15,112-->00:28:22,313
Không sao. Nếu anh ở vị trí của em,
anh cũng sẽ mệt mỏi thôi.

368
00:28:22,712-->00:28:24,113
Anh sẽ xem đó là một lời khen ngợi.

368
00:28:25,112-->00:28:30,113
Tóm lại, giờ em hoàn toàn
không có giải pháp gì cả. Đường cụt rồi.

368
00:28:30,512-->00:28:32,513
Em nói vậy chắc là nghiêm trọng lắm.
Nhưng nếu là em, chắc sẽ tìm ra
cách nào đó thôi. 

368
00:28:32,712-->00:28:34,513
Đứa em gái dễ thương không thể 
kết thúc theo cách này, phải không?

368
00:28:35,112-->00:28:43,113
Gì vậy? Anh đang khuyến khích em đấy à?
Anh có thể nói chi tiết hơn không?

368
00:28:43,912-->00:28:45,513
Anh xin lỗi. Anh hết ý để nói rồi.

368
00:28:46,112-->00:28:54,113
Đó là vì anh luôn cố gắng làm điều gì đó 
ngoài khả năng của mình. 
Nhưng dù sao cũng cảm ơn anh. 
Em đánh giá cao cử chỉ đó.

368
00:28:54,512-->00:28:56,113
Ừ, được rồi... Không có gì.

368
00:28:56,712-->00:29:01,513
Đợi đã... Nếu em thử dùng như 
cách với thiên thần và thử lại thì sao?

368
00:29:02,112-->00:29:03,313
Thiên thần à?

368
00:29:04,112-->00:29:08,113
Cũng giống như anh, Shidou. 
Có lẽ đánh thẳng vào là không hiệu quả.

368
00:29:10,112-->00:29:16,113
Khi phong ấn, có thể đây là giải pháp.
Nhưng lần này không như vậy.

368
00:29:16,312-->00:29:18,113
Phải. Kế hoạch mọi khi của em
cũng không có tác dụng.

368
00:29:18,512-->00:29:29,513
Điều kỳ lạ là không thể tự nhiên
có một kết giới rộng và mạnh mẽ như vậy.
Nó không bị phát hiện bởi bất kỳ người dân nào.

368
00:29:30,112-->00:29:32,513
Nhưng em không thể phát hiện nó
bằng Fraxinus hay AST, phải không?

368
00:29:33,112-->00:29:38,713
Vậy nên nó mới kỳ lạ. 
Cứ như một trò lừa vậy.

368
00:29:39,112-->00:29:40,113
Trò lừa à? Trò lừa...

368
00:29:40,312-->00:29:41,513
Tôi bối rối gãi đầu,
nhưng tôi không thể nghĩ ra điều gì cả.

368
00:29:42,112-->00:29:51,113
Theo tính toán của em, phải có gì đó
dưới mặt đất định hình kết giới.

368
00:29:51,312-->00:29:53,113
Anh hiểu rồi. Đó là một cách có thể nghĩ tới.

368
00:29:54,112-->00:30:03,113
Nhưng nếu chúng ta không thể tìm ra
thứ gì ở dưới đất thì cũng vô ích.
Nhưng em khá chắc với khả năng đó.

368
00:30:03,512-->00:30:05,513
Được rồi. Vậy anh có thể làm gì?

368
00:30:05,912-->00:30:12,113
Vâng... nếu anh thấy có điều gì 
đáng ngờ, hãy nói em biết

368
00:30:12,312-->00:30:26,113
Nhưng ít nhất là cho đến bây giờ,
nhiệm vụ của anh là giữ mọi thứ
không chạy ra khỏi tầm kiểm soát.

368
00:30:27,112-->00:30:34,113
Thay vào đó, anh cứ để cho bọn em.
Cố gắng tập trung vào việc đảm bảo 
trạng thái tinh thần của Tinh Linh đi.

368
00:30:34,312-->00:30:35,513
Được rồi... Anh sẽ làm thế.

368
00:30:36,112-->00:30:39,113
Cuối cùng anh cũng học cách phối hợp nhỉ.

368
00:30:39,512-->00:30:43,113
Em nói cứ để em lo. Nếu anh không tin em
thì anh là loại anh trai nào chứ?

368
00:30:44,512-->00:30:45,513
Sao thế?

368
00:30:46,512-->00:30:56,113
Không có gì cả! Tóm lại, nếu có gì
dưới mặt đất, kết giới hẳn phải 
được tạo bởi thứ gì đó mạnh tương tự.

368
00:30:56,712-->00:30:58,513
Tương tự à? Giống như tắc kè đổi màu sao?

368
00:30:59,112-->00:31:08,113
Đó là một phần của nó. 
Phải mất một thời gian để điều tra 
với Fraxinus một cách kỹ lưỡng.

368
00:31:09,112-->00:31:15,113
Vì vậy, em nhận ra mình không thể 
tiếp xúc với bên ngoài cũng như
không biết những gì bên trong.

368
00:31:15,512-->00:31:26,513
Em đã hoàn toàn mất phương hướng
trong thế giới này. Thay vào đó, 
em phải tìm ra thứ gì trong đây.

368
00:31:27,112-->00:31:28,513
Vậy làm sao chúng ta có thể tìm ra nó?

368
00:31:29,112-->00:31:35,113
Sao em biết được?
Anh lúc nào cũng hỏi những câu ngớ ngẩn.

368
00:31:35,512-->00:31:37,513
Xin lỗi... Này, sao anh có thể 
theo kịp hết những gì em nói chứ?

368
00:31:38,112-->00:31:48,113
Điều em thắc mắc lớn nhất là 
ai có thể điều khiển được kết giới này.
Ý nghĩa của việc này là gì?

368
00:31:48,312-->00:31:49,513
Hm... Để bẫy ai đó chăng?

00:31:50,112-->00:31:56,113
Ý anh là.. Tinh Linh? 
Đó cũng là một khả năng.

368
00:31:56,712-->00:31:59,113
Được rồi. Anh sẽ báo nếu anh tìm thấy
gì đó. Đừng cố gắng quá đấy, Kotori.

368
00:32:00,112-->00:32:02,113
Vâng, em biết.

368
00:32:03,512-->00:32:06,113
Vậy hôm nay thế là đủ rồi.

368
00:32:06,312-->00:32:07,513
Ừ, chúc ngủ ngon

368
00:32:08,112-->00:32:10,513
Chúc ngủ ngon, Onii-chan

368
00:32:12,912-->00:32:16,113
Kotori, con bé có vẻ mệt...
Nếu giờ không ngủ thì không chịu được.
Đi thôi.

368
00:32:28,112-->00:32:33,113
Ngày 29 tháng 6