﻿0
00:00:04,712 --> 00:00:05,713
Shidou...

1
00:00:09,512 --> 00:00:12,113
Shidou, dậy đi.

2
00:00:12,312 --> 00:00:14,513
Kotori đấy à? Gì vậy?

3
00:00:14,712 --> 00:00:18,113
Em muốn báo cáo. Nói chuyện với em
một chút trước khi ăn sáng đã.

4
00:00:19,112 --> 00:00:20,513
Được rồi. Chờ anh chút.

5
00:00:24,712 --> 00:00:27,513
Vậy em muốn báo cáo chuyện gì? 
Có phát hiện mới hay là... 
vụ tấn công tối qua?

6
00:00:28,112 --> 00:00:31,113
Là cả hai.

7
00:00:31,312 --> 00:00:32,313
Cả hai à?

8
00:00:32,712 --> 00:00:39,113
Khi đến gần di tích, một thiên thần 
xuất hiện giống như muốn can thiệp.

9
00:00:40,112 --> 00:00:40,913
Đúng vậy.

10
00:00:41,313 --> 00:00:45,113
Sau đó, nó biến mất không dấu vết.

11
00:00:45,512 --> 00:00:48,313
Chắc chắn rồi... Vậy nó là thứ gì?
Nó là... Tinh Linh sao?

12
00:00:48,512 --> 00:00:58,113
Vẫn chưa có gì đảm bảo.
Nhưng theo những gì chứng kiến,
nó rất giống với Tinh Linh.

13
00:00:58,312 --> 00:00:59,313
Ý em là sao?

14
00:00:59,512 --> 00:01:10,113
Về sức mạnh, nó gần giống với Tinh Linh.
Dù vậy, nó chỉ là thực thể được tạo ra
từ Tinh Linh có ý chí tự do như Tohka.

15
00:01:10,512 --> 00:01:11,313
Hở?

16
00:01:11,512 --> 00:01:21,313
Giống như bộ não của anh như côn trùng,
anh vẫn có thể hành động tự do 
như một con người.

17
00:01:21,512 --> 00:01:22,713
So sánh kiểu gì vậy?

18
00:01:23,112 --> 00:01:30,113
Mà thôi. Tóm lại, nó chắc chắn
đã được tạo ra bởi sức mạnh Tinh Linh
dựa vào những gì anh mô tả.

19
00:01:31,112 --> 00:01:37,113
Nó giống như một "Golem" 
hoặc "Người bảo vệ" trong các trò chơi.

20
00:01:37,312 --> 00:01:39,113
Thật vậy... 
Nhưng không rõ mục đích là gì sao?

25
00:01:39,312 --> 00:01:47,713
Phải, em không tìm ra
nhưng chắc chắn nó có việc phải làm.

25
00:01:47,913 --> 00:01:49,113
Có bao nhiêu thứ đó được tạo ra?

25
00:01:49,312 --> 00:01:57,113
Các dạng sóng đang xuất hiện yếu ớt 
trong tháp Tenguu. Nó chồng lấn với
kết giới bao phủ thành phố Tengu.

25
00:01:59,112 --> 00:02:07,113
Nó có kích thước khác nhau,
nhưng em đã kiểm tra.
Kết quả là nó giống hệt nhau.

25
00:02:07,312 --> 00:02:08,713
2 sóng đó... giống nhau sao?

25
00:02:09,113 --> 00:02:17,913
Sự hiện diện của cả kết giới 
và người bảo vệ có liên quan đến câu hỏi đó.
Em chắc chắn chúng liên kết đến nhau.

25
00:02:18,113 --> 00:02:26,113
Thật dễ dàng để nói có khả năng 
cả di tích và người bảo vệ đều được tạo ra.

25
00:02:26,512 --> 00:02:34,113
Câu trả lời cho điều này là... 
các di tích phải là nền tảng để giữ kết giới.

25
00:02:34,512 --> 00:02:37,113
Trước đây em đã đề cập về di tích?
Vậy nếu anh có thể...

25
00:02:37,312 --> 00:02:45,113
Có lẽ vậy. Nhưng em e rằng nó không đơn giản.

25
00:02:45,512 --> 00:02:47,113
Ý của em là... không chỉ có một...?

25
00:02:47,712 --> 00:02:58,113
Đúng vậy. Ở nơi nào đó khắp Tengu
cũng có những thứ tương tự như thế.

25
00:02:58,512 --> 00:03:01,513
Lần sau, em phải tìm nó...
Đây mới chỉ là bắt đầu.

25
00:03:02,113 --> 00:03:12,113
Phải. Vẫn còn một chặng đường dài để đi.
Nhưng điều này cũng tùy thuộc vào anh đấy.
Anh hãy tiếp tục hẹn hò.

25
00:03:12,512 --> 00:03:13,513
Ừ, anh hiểu...

25
00:03:14,112 --> 00:03:23,113
Tất nhiên, nếu anh biết điều gì,
hãy báo cáo ngay.
Nhưng đừng làm thế nữa, được không?

25
00:03:23,312 --> 00:03:24,113
Ừ, anh biết...

25
00:03:25,112 --> 00:03:30,113
Giờ chỉ còn ý định của kẻ thù.
Em ước gì mình biết được...

25
00:03:30,512 --> 00:03:31,713
Kẻ thù à? Là người bảo vệ sao?

25
00:03:32,112 --> 00:03:47,113
Em đang nói về kẻ chủ mưu.
Lý do khiến chúng ta bị nhốt trong kết giới.
Không, chuyện này chưa kết thúc! 
Anh phải làm những gì cần làm!

25
00:03:47,312 --> 00:03:50,513
Uh... Anh xin lỗi. Anh sẽ hợp tác 
càng nhiều càng tốt. Anh muốn làm sớm. 
Mọi người đang vật lộn với chuyện này...

25
00:03:51,112 --> 00:03:57,713
Một lần nữa, anh giờ không còn 
khả năng hồi phục. 
Tuyệt đối không được làm thế nữa.

25
00:03:58,112 --> 00:03:59,513
Ừ, anh hứa.

25
00:04:00,112 --> 00:04:04,513
Thực sự... Em lo lắng cho anh
đến không ngủ được đấy...

25
00:04:05,112 --> 00:04:06,113
Hở?

26
00:04:06,312 --> 00:04:09,113
Không có gì đâu!

26
00:04:09,312 --> 00:04:10,113
Ừ... ừ...

26
00:04:15,512 --> 00:04:17,113
Bữa sáng xong chưa anh?

26
00:04:17,512 --> 00:04:19,513
Xin lỗi đã để em đợi. Anh xong rồi đây.

26
00:04:20,512 --> 00:04:27,513
Shidou...san. Chào buổi sáng.

26
00:04:29,312 --> 00:04:33,113
Ồ, Yoshino và Yoshinon, chào buổi sáng.
Vừa đúng lúc bữa sáng xong rồi đấy.

26
00:04:33,712 --> 00:04:38,513
Vâng... Itadakimasu...

26
00:04:39,112 --> 00:04:42,113
Tohka đâu rồi?

26
00:04:43,112 --> 00:04:45,513
Tohka à? Tohka đi học rồi. 
Cô ấy không cần ăn sáng...

26
00:04:46,112 --> 00:04:50,513
Tohka không cần ăn sáng sao?

26
00:04:51,112 --> 00:04:53,513
Trông cô ấy cũng có vẻ khó chịu...
nhưng cũng không phải việc lớn
chỉ vì cô ấy không ăn cơm đâu.

26
00:04:56,112 --> 00:04:57,313
Gì vậy?

27
00:04:58,112 --> 00:05:02,513
Không có gì đâu. Anh sẽ biết sớm thôi.

28
00:05:03,112 --> 00:05:04,113
Vậy sao?

46
00:05:08,512 --> 00:05:10,113
Tôi đã đi học như thường ngày, 
nhưng hôm nay tâm trí tôi trống rỗng...

47
00:05:10,312 --> 00:05:13,113
Nếu tôi cứ tiếp tục tâm trạng này,
tôi không thể hẹn hò với ai cả. Tôi phải thay đổi.

48
00:05:15,312 --> 00:05:16,113
Uida!

49
00:05:19,312 --> 00:05:21,113
Oh... Tớ xin lỗi, cậu không sao chứ?

50
00:05:21,312 --> 00:05:22,113
Tớ không sao.

51
00:05:22,312 --> 00:05:24,113
Không...! Có sao đấy!
Hoàn toàn không ổn chút nào!

52
00:05:25,112 --> 00:05:26,113
Thôi nào! Đứng dậy nhanh nào!

53
00:05:29,112 --> 00:05:30,113
Cảm ơn cậu.

54
00:05:30,312 --> 00:05:32,113
Tớ xin lỗi. Tớ đang suy nghĩ một chút...

55
00:05:32,912 --> 00:05:34,113
Cậu nghĩ gì?

56
00:05:34,312 --> 00:05:36,113
Đừng lo, không có gì quan trọng đâu!

57
00:05:37,112 --> 00:05:39,513
Vậy à? Được rồi...

58
00:05:39,912 --> 00:05:41,513
Ừ. Còn mấy phút nữa là đến giờ nhỉ?

59
00:05:41,712 --> 00:05:43,513
5 phút 17 giây nữa...

60
00:05:43,712 --> 00:05:46,113
À đúng vậy. Vậy chúng ta vào lớp đi.
Cậu có vào lớp không?

41
00:05:48,112 --> 00:05:51,113
Phù... Mọi thứ bất ngờ khiến tôi lơ đãng
trong thoáng chốc. Tôi ngừng suy nghĩ 
và đi thẳng vào lớp.



29
00:05:56,112 --> 00:05:57,513
Vậy bao giờ Kotori học xong?

30
00:05:57,912 --> 00:05:59,313
Mình muốn hỏi con bé sẽ
làm gì vào buổi chiều.

31
00:06:00,112 --> 00:06:01,113
OK. Việc này sẽ ổn thôi.

32
00:06:07,113 --> 00:06:08,513
Tôi vẫn không thể gọi cho Kotori...

33
00:06:08,712 --> 00:06:10,513
Con bé đang ở đâu? 
Em đã bỏ điện thoại ở đâu vậy?

34
00:06:11,112 --> 00:06:12,513
Thôi, kiểm tra sau vậy.

35
00:06:21,112 --> 00:06:23,113
Đã đến giờ ăn trưa. Sao mãi mới tới nhỉ?
Tôi lấy đồ ăn trưa của mình ra...

35
00:06:23,512 --> 00:06:28,113
Itsuka-kun, bạn trai của tôi! 
Đi ăn chung đi!

35
00:06:28,512 --> 00:06:31,113
Đừng gọi tên tôi to như vậy!
Thay vào đó, ai là bạn trai của ông hả?

35
00:06:32,112 --> 00:06:39,113
Itsuka, ông xấu hổ à?
Nhưng gọi như thế cũng không tệ chứ?

35
00:06:39,512 --> 00:06:41,713
Không tệ cái gì? Này... nếu ông nói nữa, 
cả lớp sẽ nghĩ xấu đấy!

35
00:06:42,112 --> 00:06:51,313
Đừng lo, Itsuka. 
Tôi sẽ phá vỡ sự nghi ngờ đó!
Ông không thể thú vị hơn sao?

35
00:06:51,712 --> 00:06:54,113
Làm ơn...! Đừng làm gì cả!
Ông không nghĩ gì về hành động của mình sao?

35
00:06:55,112 --> 00:07:01,713
Itsuka... Đối với người như ông...
tôi biết chúng ta đã quen nhau từ lâu...

35
00:07:02,112 --> 00:07:04,113
Tôi biết ông sắp kể một câu chuyện
kỳ lạ nào đó đúng không?

35
00:07:05,113 --> 00:07:10,713
Không, ông không thể làm thế.
Câu chuyện của tôi luôn luôn quan trọng...

35
00:07:11,112 --> 00:07:12,913
Ồ, hình như có đứa con gái đang gọi ông ở đó.

35
00:07:13,512 --> 00:07:14,713
Thật chứ?

35
00:07:15,112 --> 00:07:17,113
Thật. Này, Tonomachi ở đây này!
Ông muốn cô ấy bỏ đi sao?

35
00:07:17,712 --> 00:07:22,113
Cái gì? Chờ đã, người yêu tương lai ơi!

35
00:07:23,712 --> 00:07:25,713
Thoát rồi. Vậy mà hắn cũng tưởng thật.

35
00:07:25,913 --> 00:07:28,113
Mệt mỏi thật... giờ là lúc để 
nói chuyện và ăn trưa với Tohka... 
Đâu rồi?

35
00:07:28,312 --> 00:07:30,313
Tohka đi đâu rồi? Cô ấy đi trong lúc 
tôi nói chuyện với Tonomachi...

35
00:07:30,512 --> 00:07:32,513
Phù... mình sẽ ăn trưa một mình...




36
00:07:33,112 --> 00:07:36,513
Phù... Mình cố ăn cho xong để
không bị vướng vào Tonomachi,
Mình đã ăn quá nhanh... nên hơi tức bụng.

37
00:07:36,912 --> 00:07:38,113
Vậy... còn bao nhiêu giờ nghỉ trưa nữa?

38
00:07:38,313 --> 00:07:39,913
Môn tiếp theo là môn vật lý.

39
00:07:40,113 --> 00:07:42,713
Reine-san có lẽ sẽ đến trễ.
Cô ấy cũng rất bận...

40
00:07:42,913 --> 00:07:45,113
Tôi nghĩ nên đi tìm cô ấy,
sẵn tiện giúp đỡ nếu cần thiết.

41
00:07:46,913 --> 00:07:47,713
Xin lỗi

40
00:07:51,113 --> 00:08:02,113
Đúng vậy. Chắc chắn đã có phản ứng 
với phạm vi này... từ đây đến đây...

41
00:08:03,113 --> 00:08:05,113
Shidou?

42
00:08:05,513 --> 00:08:07,713
Chuyện gì vậy, Shin?

43
00:08:07,913 --> 00:08:11,113
À, không... Lớp tiếp theo là của cô đấy. 
Không biết cô có thể về lớp...

44
00:08:11,313 --> 00:08:19,313
À phải rồi. Tiếp theo là lớp của tôi.
Xin lỗi. Tôi hoàn toàn quên mất.

45
00:08:19,713 --> 00:08:23,113
Cậu cũng chuẩn bị đi. Tôi sẽ đợi.

45
00:08:26,513 --> 00:08:28,113
Em vừa nói gì với Reine vậy?

46
00:08:29,113 --> 00:08:34,113
À không... liên quan đến Fraxinus thôi.

47
00:08:34,513 --> 00:08:38,113
Nếu em làm nhiều hơn một chút, 
có thể sẽ tìm thấy một đầu mối lớn...

48
00:08:39,113 --> 00:08:42,713
Em nghĩ không dễ dàng đến thế.

49
00:08:43,113 --> 00:08:44,513
Em ổn chứ? Anh thấy em có vẻ kiệt sức rồi.

50
00:08:45,113 --> 00:08:50,113
Vâng, nếu em nói em không mệt,
chắc chắn là nói dối.

51
00:08:51,113 --> 00:08:55,313
Dù em không khỏe, nó cũng không lây được đâu.

52
00:08:55,512 --> 00:08:57,113
Này...

53
00:08:57,512 --> 00:08:59,113
Anh có lo lắng không?

54
00:08:59,513 --> 00:09:00,713
Tất nhiên.

55
00:09:01,112 --> 00:09:03,713
Vậy à? Cảm ơn anh

56
00:09:04,112 --> 00:09:04,712
À, ừ...

57
00:09:04,912 --> 00:09:06,913
À... Hồi trưa anh có gọi điện cho em đấy...

58
00:09:07,112 --> 00:09:12,913
Vậy à? Em xin lỗi... 
Em mải làm việc nên không để ý.

59
00:09:13,112 --> 00:09:15,513
Vâng... À, anh biết hơi đột xuất.
Anh muốn làm gì đó vào buổi chiều...

60
00:09:16,112 --> 00:09:22,713
Vâng... từ chiều trở đi
em sẽ tiếp tục làm việc ở đây với Fraxinus...

41
00:09:23,112 --> 00:09:24,113
Ừ, sẽ khó khăn hơn nhiều.

42
00:09:24,312 --> 00:09:25,113
Vâng...

43
00:09:27,112 --> 00:09:31,313
Tôi đang đợi đấy, Shin. 
Chúng ta đến lớp thôi.

44
00:09:31,512 --> 00:09:32,513
Vâng. Chào nhé, Kotori

45
00:09:32,712 --> 00:09:39,113
Vâng, hẹn gặp lại anh...
Em cũng phải về lớp đây.

46
00:09:39,512 --> 00:09:41,513
Ừ, gặp lại sau.

47
00:09:50,113 --> 00:09:52,513
Cuối cùng cũng hết giờ. Không hiểu sao
tôi thấy lâu hơn bình thường.

48
00:09:52,712 --> 00:09:55,713
Mặt khác, tôi tự hỏi tại sao lại mệt mỏi vậy?
Tôi lại bệnh sao? Không, không phải thế...

49
00:09:55,912 --> 00:09:57,313
Không thể để thế này được. Tôi phải hẹn hò.

50
00:10:13,512 --> 00:10:15,513
Đi đến Hành lang?
1) Đồng ý
2) Không đồng ý

51
00:10:20,112 --> 00:10:22,513
Kotori... có vẻ bận lắm.

52
00:10:22,712 --> 00:10:25,513
Hôm nay, mình muốn đi hẹn hò 
với Kotori nếu có thể.
Con bé quá bận nên cần được nghỉ ngơi...

53
00:10:26,112 --> 00:10:28,113
Mình sẽ tìm Kotori. 
Kotori nói buổi chiều con bé vẫn ở phòng Reine.

54
00:10:28,312 --> 00:10:29,513
Giờ đi kiểm tra thôi.

55
00:10:32,112 --> 00:10:34,513
Kotori, em đâu rồi?
Không thấy ai trả lời... Ở đây có ai không?

56
00:10:39,112 --> 00:10:42,113
Sao vậy Kotori?
Em đang lo lắng gì à? 
Anh vào có đúng lúc không?

57
00:10:43,112 --> 00:10:45,113
Shidou...

58
00:10:46,112 --> 00:10:46,913
Chào em...

59
00:10:47,112 --> 00:10:49,113
Gì vậy? Anh muốn gặp Reine à?

60
00:10:49,712 --> 00:10:51,113
Không, lần này anh muốn gặp em...

61
00:10:51,712 --> 00:10:53,513
Gì vậy? 

62
00:10:54,112 --> 00:10:56,513
Có vẻ đến giờ em vẫn bận nhỉ.
Em vẫn không có thời gian à?

63
00:10:57,112 --> 00:11:01,113
Vâng. Em xin lỗi, nhưng...

64
00:11:01,312 --> 00:11:02,313
Không, đừng bận tâm.

65
00:11:02,512 --> 00:11:04,113
Vậy chắc hôm nay anh sẽ đi với người khác.

66
00:11:06,312 --> 00:11:07,513
Vậy nhé...

67
00:11:08,112 --> 00:11:09,113
Khoan đã.

68
00:11:09,312 --> 00:11:10,113
Hở?

69
00:11:11,112 --> 00:11:13,713
Em sẽ đi hẹn hò.

70
00:11:13,912 --> 00:11:15,113
Hở? Anh tưởng em bận...

71
00:11:16,112 --> 00:11:18,313
Em muốn đi!

71
00:11:19,112 --> 00:11:25,113
Em làm việc suốt cả ngày hôm nay...
Em cũng muốn được nghỉ ngơi.

72
00:11:25,312 --> 00:11:26,513
Được rồi... vậy chúng ta đi.

73
00:11:27,112 --> 00:11:29,113
Ừ... đi thôi.

75
00:11:34,112 --> 00:11:36,313
Được rồi, giờ chúng ta đi đâu đây?
Kotori, em muốn đi đâu?

76
00:11:38,112 --> 00:11:39,113
Kotori?

77
00:11:39,312 --> 00:11:43,113
Xin lỗi. Gì vậy?

78
00:11:43,312 --> 00:11:44,513
Anh hỏi em có muốn đi đâu không?

79
00:11:45,112 --> 00:11:49,713
Anh dẫn đi đâu cũng được.

80
00:11:50,112 --> 00:11:51,313
Em muốn mua đồ ăn không?

81
00:11:52,112 --> 00:11:55,113
Vâng, thế cũng được...

82
00:11:55,312 --> 00:11:56,113
Vậy cũng được.

83
00:11:57,112 --> 00:11:59,113
Có phải đó là... bánh mì vỉa hè không?

84
00:11:59,313 --> 00:12:00,513
Để anh đi mua. Đợi ở đây nhé.

85
00:12:00,913 --> 00:12:02,113
Vâng.

86
00:12:07,112 --> 00:12:09,113
Anh về rồi... Hở, này, này Kotori?

87
00:12:09,512 --> 00:12:10,513
Có chuyện gì vậy? Em không khỏe à?

90
00:12:11,112 --> 00:12:16,513
Đừng hỏi nữa. Đầu em đau lắm...

91
00:12:16,912 --> 00:12:18,113
Anh xin lỗi... nhưng...

92
00:12:19,112 --> 00:12:25,113
Thật sự, em mệt quá. Em muốn nghỉ ngơi...

93
00:12:25,312 --> 00:12:30,113
Chỉ một chút thôi... 
Chỉ một chút thôi, làm ơn...

94
00:12:30,312 --> 00:12:31,113
Kotori...

95
00:12:31,312 --> 00:12:35,113
Không sao đâu. Em không sao cả...

96
00:12:37,512 --> 00:12:39,113
Khuôn mặt của Kotori... đỏ hết rồi.
Có nên để con bé lại đây?

97
00:12:40,112 --> 00:12:41,513
Sẽ tốt nếu con bé không sao... 
nhưng... nếu nó có chuyện gì...

98
00:12:41,712 --> 00:12:42,513
Mình...

99
00:12:43,112 --> 00:12:48,513
1) Con bé không nên di chuyển,
tôi sẽ để thế này thêm lúc nữa.
2) Không thể để nó một mình. 
Đưa con bé về thôi.

100
00:12:49,112 --> 00:12:50,513
Kotori... thực sự không sao chứ?

101
00:12:52,512 --> 00:12:55,113
Có thể, như Kotori nói, 
con bé sẽ sớm khỏe lại.
Nhưng nếu không thì sao?

102
00:12:55,513 --> 00:12:57,113
Kotori, xin lỗi chút nhé.

103
00:13:03,512 --> 00:13:07,913
Anh làm gì vậy? Bỏ em xuống đi!

108
00:13:08,112 --> 00:13:09,113
Xin lỗi. Anh không thể làm thế.

109
00:13:09,912 --> 00:13:12,113
Em đang bị bệnh, đó là điều quan trọng.

110
00:13:12,312 --> 00:13:14,513
Anh chỉ muốn giúp em...
Anh sẽ đưa em về.

111
00:13:14,712 --> 00:13:18,713
Nhưng mà... em xấu hổ lắm...

108
00:13:19,112 --> 00:13:21,513
Thay vì xấu hổ, em nên ưu tiên cho sức khỏe.
Nếu xấu hổ, hãy úp mặt vào lưng anh.

109
00:13:21,712 --> 00:13:24,113
Nếu không giúp em,
anh sẽ không phải là anh trai em nữa.

110
00:13:25,112 --> 00:13:27,513
Shidou...

111
00:13:28,112 --> 00:13:32,113
Chỉ lần này thôi đấy.

112
00:13:32,312 --> 00:13:33,113
Anh biết.

108
00:13:33,312 --> 00:13:35,513
Đúng là một tên Siscon.

109
00:13:35,712 --> 00:13:36,513
Tệ lắm à?

110
00:13:37,112 --> 00:13:39,713
Không thực sự, không tệ đâu.

111
00:13:39,913 --> 00:13:41,313
Vậy đi nhé.

113
00:13:41,913 --> 00:13:42,913
Ừ...

114
00:13:43,512 --> 00:13:45,313
Em biết không, Kotori. 
Nếu em không nhớ, em không cần trả lời...

1
00:13:45,512 --> 00:13:47,113
Trước đây đã từng có chuyện thế này.
Khi em vẫn còn nhỏ...

1
00:13:47,512 --> 00:13:48,913
Có một thời gian khi em té trong công viên, 
đầu gối em bị trầy

1
00:13:49,713 --> 00:13:51,113
Anh đã lo lắng đến nỗi 
anh làm tổn thương em nhiều hơn.

1
00:13:51,312 --> 00:13:53,513
"Em nên gặp bác sĩ sớm! 
Nếu em cần phẫu thuật thì sao?"
Như thế đấy.

1
00:13:53,713 --> 00:13:55,513
Anh không thể không băn khoăn... hahaha.
Anh tự hỏi giờ đã thay đổi bao nhiêu rồi.

368
00:13:56,712 --> 00:14:01,113
Từ đó, anh chẳng lớn lên chút nào.

368
00:14:01,312 --> 00:14:03,513
Mặt khác, em quá trưởng thành.
Nhưng anh nghĩ anh cũng đã 
trưởng thành một chút rồi.

368
00:14:03,712 --> 00:14:07,113
Hãy thử đi, anh cần nó đấy.

368
00:14:07,513 --> 00:14:09,113
Vậy à? Anh cũng cảm thấy thế.

368
00:14:09,313 --> 00:14:13,113
Đừng thay đổi, em muốn anh cứ thế này thôi.

368
00:14:13,312 --> 00:14:14,313
Kotori...

368
00:14:14,512 --> 00:14:19,713
Và em cũng vậy, đã không thay đổi nhiều.

368
00:14:19,912 --> 00:14:22,113
Không, em đã thay đổi. 
Em lạnh lùng hơn với anh.

368
00:14:24,112 --> 00:14:25,313
Tôi đã lầm...con bé đang trong 
tâm trạng tốt và thoải mái.

368
00:14:25,512 --> 00:14:27,513
Trông con bé lúc này...
thật sự dễ thương hơn hẳn bình thường...

68
00:14:29,112 --> 00:14:32,113
Anh vừa nói gì...

368
00:14:32,312 --> 00:14:33,913
À, không có gì!

368
00:14:34,112 --> 00:14:36,513
Nói lại đi!

68
00:14:36,712 --> 00:14:37,713
Không đâu!

368
00:14:38,112 --> 00:14:40,113
Thực sự... hèn nhát.

368
00:14:40,312 --> 00:14:41,313
Này!

368
00:14:41,513 --> 00:14:50,513
Em không thấy gì khi nghe điều đó,
Em cũng không muốn thay đổi gì nhiều.

368
00:14:50,912 --> 00:14:58,113
Bởi vì... em luôn có anh, Shidou...

368
00:14:58,312 --> 00:14:59,113
Kotori?

368
00:15:06,112 --> 00:15:07,513
Đến rồi, Kotori

368
00:15:07,712 --> 00:15:10,513
Giờ bỏ em xuống đi. Không sao đâu.

368
00:15:11,112 --> 00:15:12,113
Thật chứ?

368
00:15:17,112 --> 00:15:26,713
Vậy Shidou... Mặc dù em không thích nó 
như trước đây, em buộc phải chịu cảnh
mọi người đang xem chúng ta...

368
00:15:27,113 --> 00:15:28,713
Không... Lúc đó anh chỉ muốn giúp em...

368
00:15:29,113 --> 00:15:32,113
Em biết chuyện đó

368
00:15:32,312 --> 00:15:42,113
Cảm... cảm ơn anh, Shidou.
Em luôn cố gắng làm việc,
nhưng em cảm thấy nhẹ nhõm 
khi có gì đó an toàn.

368
00:15:42,312 --> 00:15:43,113
À, ừ...

368
00:15:43,312 --> 00:15:50,113
Và... anh đã khác với ngày xưa

368
00:15:50,512 --> 00:15:57,113
Anh trưởng thành hơn, cố gắng hơn...
Em nghĩ thế...

368
00:15:57,512 --> 00:15:59,513
Ừ phải... em đang lớn lên đấy.

368
00:16:02,112 --> 00:16:03,113
Thôi vào nhà đi.

368
00:16:03,312 --> 00:16:04,113
Vâng...

368
00:16:13,112 --> 00:16:17,113
Tôi muốn hít thở không khí bên ngoài...
Cũng không tệ khi đi bộ một mình như thế này.
Tôi nghe nói đi như vậy suy nghĩ sẽ tốt hơn.

368
00:16:17,313 --> 00:16:19,913
Những ngày này, thật khó hiểu.
Một cái gì đó không giải quyết được 
bằng nhiều cách...

368
00:16:20,112 --> 00:16:21,113
Vậy mình nên đi bộ ở đâu nhỉ?

368
00:16:21,312 --> 00:16:23,113
Tôi nói điều đó và hối thúc chân tôi.
Phải, chỉ có thể đến công viên thôi.

368
00:16:28,112 --> 00:16:31,113
Là chỉ huy của Fraxinus,
con bé làm việc không được thở.
So với tuổi của nó, con bé đã làm việc quá sức.

368
00:16:31,312 --> 00:16:33,113
Kotori... Anh không biết 
phải làm gì cho em nữa... Hở?

368
00:16:34,512 --> 00:16:35,513
Kotori?

368
00:16:36,512 --> 00:16:37,513
Shidou?

368
00:16:38,112 --> 00:16:41,113
Anh chưa muốn về nhà.
Em vẫn chưa về nghỉ ngơi à?

368
00:16:42,112 --> 00:16:50,513
Vâng, em chỉ đến làm sạch tâm trí thôi.
Không sao đâu... Em thật sự khỏe rồi.

368
00:16:51,112 --> 00:16:52,513
Dù em nói thế, em...

368
00:16:53,112 --> 00:16:58,513
Được rồi! Em... em lo lắng 
về việc phải cố gắng...

368
00:16:58,912 --> 00:16:59,712
Kotori...

368
00:17:01,513 --> 00:17:02,513
Um... Kotori...

368
00:17:03,112 --> 00:17:21,513
1) Có điều gì anh có thể giúp em không?
2) Nó sẽ rất khó khăn, nhưng hãy tiếp tục.
3) Em có vẻ bận lắm.

368
00:17:22,512 --> 00:17:24,513
Có điều gì anh có thể giúp em không?
Nếu có thể làm em thấy nhẹ nhõm hơn, anh...

368
00:17:25,112 --> 00:17:27,513
Không. Anh không giúp được đâu

368
00:17:27,712 --> 00:17:29,713
Được chứ... Dù chỉ một chút...

368
00:17:29,912 --> 00:17:30,713
Không

368
00:17:30,913 --> 00:17:32,113
À, ừ...

368
00:17:32,512 --> 00:17:36,513
Những gì em đang làm là việc em không thể làm.

368
00:17:37,112 --> 00:17:41,113
Dù anh cố gắng thế nào, 
em cũng không thể yêu cầu anh làm điều đó.

368
00:17:42,112 --> 00:17:49,113
Em hiểu anh đang làm hết sức.
Đó cũng là một nỗ lực lớn rồi.

368
00:17:50,312 --> 00:17:51,512
Phải, anh biết.

368
00:17:52,112 --> 00:17:55,113
Em muốn nói là thay vào đó...

368
00:17:55,312 --> 00:17:56,113
Hở?

368
00:17:56,512 --> 00:18:01,513
Em có thể nhận sức mạnh từ
sự nỗ lực của anh không?

368
00:18:02,112 --> 00:18:04,113
Được, anh sẽ giúp em một tay
nếu em đồng ý.

368
00:18:05,112 --> 00:18:10,113
Em biết anh sẽ nói thế.

368
00:18:10,112 --> 00:18:16,513
Ngay bây giờ... 
hãy truyền sức mạnh đó... cho em đi...

368
00:18:16,913 --> 00:18:18,113
Kotori... Anh sẽ ở lại đây với em...

368
00:18:24,512 --> 00:18:27,713
Kotori, mình đã nghĩ sai về nó.
Bây giờ tình hình sẽ khá căng thẳng.

368
00:18:28,112 --> 00:18:29,113
Nếu mình không giúp con bé nhiều hơn...

368
00:18:29,312 --> 00:18:31,713
Tất nhiên, hẹn hò. Chỉ có hẹn hò.
Ít nhất mình phải giúp Kotori thư giãn...

368
00:18:31,912 --> 00:18:34,313
Không thể để tình cảm nguội lạnh.
Mình muốn hỗ trợ cho trái tim Kotori.

368
00:18:34,512 --> 00:18:36,513
Vậy đi. Mai mình sẽ hẹn hò với Kotori!

368
00:18:37,112 --> 00:18:37,913
Ai vậy? Kotori đấy à?

368
00:18:41,112 --> 00:18:43,713
Xin lỗi nhé, không phải Kotori-chan đâu.

368
00:18:43,912 --> 00:18:45,513
Rinne? Sao cậu... chuyện gì đã xảy ra?

368
00:18:45,712 --> 00:18:48,713
Shidou này, tớ muốn nghe một chuyện.

368
00:18:48,913 --> 00:18:49,913
Cậu muốn nghe chuyện gì?

368
00:18:50,112 --> 00:18:57,113
Vâng, tớ muốn nghe nó.
Shidou... là chuyện cậu đang đối mặt.

368
00:18:57,312 --> 00:18:58,513
Ý cậu là sao?

368
00:18:58,912 --> 00:19:02,313
Cậu sẽ là người... hỗ trợ cho Kotori-chan.

368
00:19:02,513 --> 00:19:05,513
Phải. Tớ không muốn nhìn thấy ai buồn.
Tớ muốn làm mọi người hạnh phúc.

368
00:19:06,112 --> 00:19:10,113
Vậy à? Vậy đó là câu trả lời của cậu.

368
00:19:10,312 --> 00:19:11,313
Phải. Tớ sẽ làm thế.

368
00:19:13,712 --> 00:19:18,113
Này Shidou, cậu có thể hứa không?

368
00:19:18,312 --> 00:19:19,513
Hứa sao? Hứa gì cơ?

368
00:19:20,112 --> 00:19:30,113
Trong tương lai, cậu sẽ yêu quý Kotori-chan.
Bất kể chuyện gì xảy ra... cậu sẽ luôn như vậy.

368
00:19:30,312 --> 00:19:32,513
Sao tự nhiên cậu nói vậy?
Dù cậu không nói thì...

368
00:19:33,112 --> 00:19:40,113
Xin cậu đấy, tớ muốn cậu hứa với tớ.
Hãy nghĩ về Kotori-chan...

368
00:19:40,312 --> 00:19:46,513
Nếu không, một ngày nào đó cậu sẽ rời xa em ấy.

368
00:19:47,112 --> 00:19:48,113
Rinne...

368
00:19:48,512 --> 00:19:49,113
Làm ơn...

368
00:19:49,912 --> 00:19:52,513
Được, tớ hứa. Là một người đàn ông, 
đã nói là phải giữ lời.
Tớ sẽ luôn quan tâm đến Kotori.

368
00:19:53,112 --> 00:19:57,313
Tớ tin cậu sẽ làm được nếu cậu cố gắng.

368
00:19:57,512 --> 00:19:59,113
Haha, tớ cũng nghĩ vậy.

368
00:19:59,512 --> 00:20:10,113
Tớ tin cậu... Shidou.
Đừng quên... Đừng bao giờ quên đấy.

368
00:20:10,312 --> 00:20:11,513
Ừ, tất nhiên rồi.

368
00:20:12,112 --> 00:20:16,713
Nếu cậu thất hứa, cậu sẽ phải làm lại đấy nhé.

368
00:20:17,112 --> 00:20:19,113
Ừ, tớ biết rồi!

368
00:20:19,912 --> 00:20:21,513
Thế à? Cuối cùng nó đã được thực hiện rồi.

368
00:20:22,112 --> 00:20:23,513
Cái gì? Thực hiện gì cơ?

368
00:20:24,112 --> 00:20:26,113
Tớ xin lỗi.

368
00:20:26,312 --> 00:20:27,313
Nhưng mà...

368
00:20:29,112 --> 00:20:35,113
Sẽ ổn thôi. Nếu cậu cố gắng...
mọi thứ sẽ ổn thôi.

368
00:20:41,112 --> 00:20:42,513
Cái... cái gì vậy?

368
00:20:43,112 --> 00:20:44,113
Này Rinne... Cậu vừa làm gì...

368
00:20:44,312 --> 00:20:46,113
Này... cậu đâu rồi? Rinne về rồi à?

368
00:20:46,312 --> 00:20:47,713
Đã quá muộn rồi. Đi ngủ thôi.

368
00:21:37,112 --> 00:21:17,113

