﻿0
00:00:04,112 --> 00:00:06,113
Đi đến Phòng gần cầu thang
1) Đồng ý
2) Không đồng ý

0
00:00:10,112 --> 00:00:12,513
Hẹn hò à? Dù sao Kotori cũng đang
gặp vấn đề tương tự như những người khác.

1
00:00:13,112 --> 00:00:15,213
Con bé cũng là Tinh Linh. 
Cũng phải đề phòng có chuyện xấu xảy ra.

2
00:00:15,512 --> 00:00:18,913
Được rồi. Đi hẹn hò với Kotori thôi.
Gọi là hẹn hò, nhưng mình chỉ muốn
đảm bảo là con bé vẫn ổn.

3
00:00:19,112 --> 00:00:22,113
Nếu mình nói mình muốn đi hẹn hò với con bé,
mình đoán nó sẽ... rất xấu hổ. 
Nhưng mình sẽ nói chuyện với nó trước.

4
00:00:23,112 --> 00:00:25,513
Chán thật. Mình không gọi được cho con bé.
Hay là nó ở Ratatoskr? Hoặc là...

5
00:00:26,112 --> 00:00:27,513
Trường học sắp đóng cửa rồi. 
Mình có nên gọi về nhà không?

6
00:00:28,113 --> 00:00:30,113
Mình vẫn không thấy con bé. 
Có lẽ Reine-san biết Kotori ở đâu.

7
00:00:34,512 --> 00:00:35,513
Em xin lỗi.

8
00:00:36,112 --> 00:00:38,513
À Shin, cậu đến rồi.

9
00:00:39,112 --> 00:00:40,513
Reine-san, em biết cô đang ở đây.

10
00:00:41,113 --> 00:00:46,513
Ara Shidou? Chuyện gì vậy?
Anh nhớ ngực của Reine à?

11
00:00:47,112 --> 00:00:48,513
Ồ, Kotori cũng ở đây à?

12
00:00:49,112 --> 00:00:51,113
Anh muốn hỏi gì à?

12
00:00:51,512 --> 00:00:56,113
Em làm phiền anh chăng?
Nếu vậy sao anh không đi đi?

13
00:00:56,513 --> 00:00:58,113
Không phải. Ngược lại là đằng khác.

14
00:00:59,112 --> 00:01:01,113
Ý anh là sao?

15
00:01:02,112 --> 00:01:04,113
Anh đang tìm em đấy Kotori.
Anh đã gọi cho em nhưng không thấy bắt máy.

16
00:01:04,512 --> 00:01:11,513
Điện thoại à? Giờ em mới nhớ.
Em để nó trong phòng rồi.

17
00:01:12,112 --> 00:01:16,113
Hiếm khi Kotori quên gì đó nhỉ?

18
00:01:17,112 --> 00:01:20,113
Tôi cũng có thể quên điều gì đó mà.

19
00:01:21,112 --> 00:01:27,113
Vậy anh định nói gì? 
Anh định xin tiền em gái anh à?

20
00:01:27,512 --> 00:01:28,513
Anh không làm thế với tư cách anh trai.

21
00:01:29,112 --> 00:01:32,513
Vậy anh ở đây làm gì?

22
00:01:33,112 --> 00:01:36,113
Có nhiều lý do lắm...
Kotori, hẹn hò với anh đi.

23
00:01:38,512 --> 00:01:43,113
Sao tự nhiên anh nói thế?

24
00:01:43,512 --> 00:01:45,513
Thì em nói rồi đấy. Hẹn hò với Tinh Linh.

25
00:01:46,112 --> 00:01:52,113
Đúng là Kotori cũng là Tinh Linh.
Vậy nên chuyện đó không có gì sai cả.

26
00:01:53,112 --> 00:01:58,113
Ý em không phải thế. 

26
00:01:59,112 --> 00:02:02,113
Chỉ là... em là Tinh Linh không cần hẹn hò.

26
00:02:03,112 --> 00:02:06,513
Nhưng em cũng không ổn định phải không?
Em phải làm gì đó chứ.

26
00:02:07,112 --> 00:02:10,113
Hơn nữa, em còn là em gái anh. 
Vậy nên anh quan tâm đến em gái mình 
là chuyện bình thường thôi.

26
00:02:13,512 --> 00:02:24,113
Buổi hẹn gần nhất có tác dụng tốt phải không?
Đây cũng là dịp để ngăn cản 
sức mạnh Tinh Linh mất kiếm soát. 
Cô cũng cần tận hưởng một buổi hẹn hò đi.

26
00:02:25,112 --> 00:02:27,513
Không cần đâu!

26
00:02:28,112 --> 00:02:29,113
Tại sao chứ?

26
00:02:30,112 --> 00:02:32,113
Cô nên nghe lời đi.

26
00:02:33,112 --> 00:02:40,113
Đừng lo cho em! Nếu anh quan tâm đến
các Tinh Linh thì sao anh không hẹn hò
với những người khác đi?

26
00:02:41,112 --> 00:02:47,113
Hơn nữa, Reine và em vẫn đang tìm 
biện pháp đối phó tình hình hiện nay.

26
00:02:48,112 --> 00:02:52,113
Vậy nên em không có thời gian hẹn hò với anh đâu!

26
00:02:53,112 --> 00:02:55,513
Tôi không phiền nếu tôi tự làm đâu.

26
00:02:56,513 --> 00:03:05,113
Được rồi! Shidou, anh cần phải hẹn hò
với các Tinh Linh khác! Đi ra khỏi đây ngay!

27
00:03:06,112 --> 00:03:07,513
Này, này!

28
00:03:10,512 --> 00:03:13,113
Phù... Con bé không sợ chuyện gì đấy chứ?
Nhưng dù con bé có nói thế thì mình vẫn lo.

29
00:03:13,512 --> 00:03:15,513
Mình không thật sự mời nhưng mình cũng 
đã gợi ý rồi. Giờ mình nên làm gì đây?

30
00:03:16,112 --> 00:03:34,113
1) Làm theo lời Kotori
2) Mình không thể bỏ mặc con bé.

31
00:03:34,512 --> 00:03:37,113
Thật lòng mình rất lo cho Kotori...
Mình chẳng có tâm trạng làm việc gì khác cả.

33
00:03:37,512 --> 00:03:39,113
Hôm nay mình sẽ đợi Kotori đến khi con bé rảnh.

34
00:03:52,112 --> 00:03:53,513
Đi đến Phòng gần cầu thang
1) Đồng ý
2) Không đồng ý

36
00:03:58,112 --> 00:04:00,113
Kotori không ra sớm. Coi bộ con bé bận thật.

37
00:04:00,312 --> 00:04:02,513
Chờ ở đây thật vô dụng. 
Mình nên về nhà và đợi con bé.

38
00:04:07,112 --> 00:04:09,113
7 giờ rồi à? 
Con bé phải ở nhà rồi chứ. Chẳng lẽ...

39
00:04:09,712 --> 00:04:10,713
A, về rồi.

40
00:04:15,113 --> 00:04:16,113
Mừng em về nhà, Kotori.

41
00:04:16,313 --> 00:04:19,513
Ừ, em về rồi. Những người khác đâu?

42
00:04:20,113 --> 00:04:21,513
Họ đã ăn tối và về phòng rồi.

43
00:04:22,112 --> 00:04:26,113
Nhanh quá nhỉ?

44
00:04:27,112 --> 00:04:28,113
Hở? Em sao vậy?

45
00:04:28,512 --> 00:04:35,513
Em đang hỏi anh có nói với mọi người chưa?
Về chuyện hẹn hò ấy.

46
00:04:36,112 --> 00:04:37,513
À, xin lỗi. Anh không nói được.

47
00:04:38,112 --> 00:04:41,513
Vậy chứ anh đã làm cái quái gì thế?

48
00:04:42,112 --> 00:04:43,513
Um... anh chỉ làm bữa tối thôi.

49
00:04:44,512 --> 00:04:48,513
Em không thể tin anh lại đần đến thế đấy.

50
00:04:49,112 --> 00:04:50,113
Em thẳng thắn quá nhỉ?

51
00:04:51,112 --> 00:04:57,113
Em phải nói sao với ông anh
không biết mình phải làm gì đây?

52
00:04:58,112 --> 00:05:01,513
Việc anh cần phải làm ngay bây giờ
là hẹn hò với các Tinh Linh.

53
00:05:02,112 --> 00:05:05,113
Anh biết. Nhưng anh quan tâm đến em, Kotori.
Chẳng lẽ như thế không được sao?

54
00:05:05,512 --> 00:05:09,113
Hôm nay, anh đã làm món em thích
để ăn cùng nhau. Đó cũng là cách hẹn hò đấy.

55
00:05:10,112 --> 00:05:11,113
Cái...

56
00:05:13,112 --> 00:05:16,113
Anh thật sự là một tên siscon à, Shidou?

57
00:05:16,512 --> 00:05:17,513
Em...

58
00:05:18,112 --> 00:05:24,513
Sao cũng được. Giờ cũng trễ rồi.
Không còn ai để anh hẹn hò nữa cả.

59
00:05:25,512 --> 00:05:27,513
Đúng vậy. Vậy là tốt rồi phải không?
Ngồi xuống đi. Anh sẽ dọn đồ ăn cho em ngay.

60
00:05:34,112 --> 00:05:37,513
Này, sao anh cứ nhìn em thế?

60
00:05:38,112 --> 00:05:40,113
Không, anh chỉ nghĩ không biết
đồ ăn có ngon không...

60
00:05:38,112 --> 00:05:40,113
Ai quan tâm đồ ăn ngon hay không chứ!
Em phải ăn để có sức làm việc tiếp.

60
00:05:38,112 --> 00:05:40,113
Vậy mà hồi nhỏ, em lúc nào cũng nói:
"Em thích đồ ăn Onii-chan nấu lắm!"

60
00:05:38,112 --> 00:05:40,113
Im ngay! Em không nói kiểu đó!
Em chỉ... ăn đồ ăn anh nấu. Thế thôi.

60
00:05:38,112 --> 00:05:40,113
Phải. Cũng chẳng có gì đặc biệt cả.

60
00:05:38,112 --> 00:05:40,113
Nhưng mà, anh nấu ăn cho em... Em thích lắm.

60
00:05:38,112 --> 00:05:40,113
Ừ, nếu em chỉ khen lịch sự thôi cũng được.

60
00:05:38,112 --> 00:05:40,113
À không, không phải thế...

60
00:05:38,112 --> 00:05:40,113
Haha, đừng lo chuyện đó.
Còn nhiều đồ ăn lắm. Cứ ăn cho no đi!

60
00:05:38,112 --> 00:05:40,113
Gì vậy? Anh muốn em ăn nhiều để mập lên à?

60
00:05:38,112 --> 00:05:40,113
Em không thấy em cần ăn 
để phù hợp với độ tuổi đó sao?

41
00:06:00,112 --> 00:06:02,113
Này, anh lại nhìn em nữa đấy à.
Em không bình tĩnh được đâu đấy!

42
00:06:03,112 --> 00:06:08,113
À phải. Xin lỗi.
Chỉ là cũng lâu rồi chúng ta 
không nói chuyện riêng với nhau.

43
00:06:09,112 --> 00:06:11,113
Đúng vậy. Đã có quá nhiều việc gần đây.

44
00:06:12,112 --> 00:06:13,313
Vậy là do những vụ việc đó à?

45
00:06:13,812 --> 00:06:16,113
Vâng, vẫn chưa có tiến triển gì cả.

46
00:06:17,112 --> 00:06:18,513
Vậy à?

47
00:06:19,113 --> 00:06:20,113
Có quá nhiều thứ kỳ lạ về vụ việc này.

48
00:06:21,112 --> 00:06:23,113
Anh hiểu. Không còn cách nào
ngoài việc quan sát trong im lặng thôi.

49
00:07:24,112 --> 00:07:26,113
Phải. Trong lúc đó, công việc của anh
là ngăn các Tinh Linh có cảm xúc bất ổn.
Đó là một trách nhiệm nặng nề đấy, hiểu chứ?

50
00:07:33,112 --> 00:07:34,113
Được rồi, anh sẽ để ý.

51
00:07:35,112 --> 00:07:11,113
Nhân tiện, anh nói những người khác
đã ăn tối rồi à?

52
00:07:11,513 --> 00:07:12,513
Hở? Đúng rồi.

53
00:07:13,112 --> 00:07:21,513
Đừng nói là... 
Anh ấy chỉ muốn ăn tối riêng với mình...

54
00:07:22,512 --> 00:07:25,113
Tối nay có trò thử thách lòng dũng cảm.
Anh cho họ ăn cơm sớm để họ chuẩn bị đi.

55
00:07:26,112 --> 00:07:29,513
Thử thách lòng dũng cảm à?

56
00:07:30,112 --> 00:07:31,513
Phải. Em có muốn đi không?

57
00:07:32,112 --> 00:07:34,113
Em không đi đâu!

58
00:07:34,512 --> 00:07:35,513
Dứt khoát quá nhỉ...

59
00:07:36,113 --> 00:06:37,513
Phải rồi. 
Em sợ mấy thứ ma quỷ, siêu nhiên mà.

60
00:07:38,112 --> 00:07:41,113
Cái gì? Không phải thế!

61
00:07:42,112 --> 00:07:48,513
Em chỉ không tin mấy thứ đó thôi!
Không phải em sợ mấy thứ đó!

62
00:07:49,112 --> 00:07:51,113
Vậy em có đi với bọn anh không?

63
00:07:55,112 --> 00:07:56,513
Có em đi cùng sẽ thú vị hơn đấy. 
Được không?

64
00:07:57,112 --> 00:07:59,113
Thôi được rồi!

65
00:08:00,112 --> 00:08:08,113
Đổi lại, nếu có chuyện gì xảy ra với mọi người,
anh phải chịu trách nhiệm và bảo vệ em đấy!

66
00:08:09,112 --> 00:08:10,113
Được, anh biết rồi.

67
00:08:11,112 --> 00:08:15,113
Shidou, bọn em chuẩn bị xong rồi! Đi thôi!

68
00:08:16,112 --> 00:08:20,113
Em có thể... đi bất kỳ lúc nào...

69
00:08:20,512 --> 00:08:23,113
Được rồi, mọi người chuẩn bị xong rồi.
Kotori, em cũng chuẩn bị đi!

70
00:08:24,112 --> 00:08:26,113
Em biết rồi!




82
00:21:03,112 --> 00:21:03,113
Hắn ta cũng không có ở đây sao?

83
00:21:03,112 --> 00:21:03,113
Này Tonomachi!

84
00:21:03,112 --> 00:21:03,113
Ông ở đâu, Tonomachi?

82
00:21:03,112 --> 00:21:03,113
Vô vọng thật. Mình không tìm thấy hắn.
Hắn chắc đi mất rồi. Đúng là Tonomachi...

83
00:21:03,112 --> 00:21:03,113
Có thể vậy chăng? Hắn thật sự...
Khoan, không thể được.
Hắn là người bày ra trò này mà.

84
00:21:03,112 --> 00:21:03,113
Ở đây không có ma thật.
Nếu có thì mình cũng chẳng sợ.

82
00:21:03,112 --> 00:21:03,113
Shi... Shidou!

83
00:21:03,112 --> 00:21:03,113
Waaa!

84
00:21:03,112 --> 00:21:03,113
Ko... Kotori? Nơ của em... cái lúc nãy đâu rồi?

82
00:21:03,112 --> 00:21:03,113
Chuyện đó không quan trọng!

83
00:21:03,112 --> 00:21:03,113
Anh đã nói anh sẽ bảo vệ em.
Vậy mà sao anh còn ra đây?

84
00:21:03,112 --> 00:21:03,113
À, xin lỗi. Nhưng nếu em đi cùng mọi người
chẳng phải sẽ đỡ sợ hơn sao?

82
00:21:03,112 --> 00:21:03,113
Không có anh... mới là điều quan trọng...

83
00:21:03,112 --> 00:21:03,113
À... vậy à...

84
00:21:03,112 --> 00:21:03,113
Anh đã hứa! Anh sẽ nhận trách nhiệm
và ở bên cạnh em!

82
00:21:03,112 --> 00:21:03,113
Đúng là thế...

83
00:21:03,112 --> 00:21:03,113
Kotori đầy sợ hãi. Từ trước đến giờ,
con bé chưa bao giờ hết sợ những thứ này.
Nếu cứ tiếp tục, con bé sẽ càng căng thẳng hơn.

84
00:22:03,112 --> 00:22:03,113
Giờ tôi phải làm gì?
Trước tiên, tôi phải an ủi để con bé bớt sợ.

84
00:22:03,112 --> 00:22:03,113
Được rồi, anh sẽ không rời xa em nữa.

84
00:22:03,112 --> 00:22:03,113
Thật không?

84
00:22:03,112 --> 00:22:03,113
Ừ, anh sẽ luôn ở bên em. Nào, đi thôi.

84
00:22:03,112 --> 00:22:03,113
Ko... Kotori...

84
00:22:03,112 --> 00:22:03,113
Gì... gì vậy?

84
00:22:03,112 --> 00:22:03,113
Này, đừng kéo áo anh. Nó sẽ rách đấy!

84
00:22:03,112 --> 00:22:03,113
Em không kéo áo anh!

84
00:22:03,112 --> 00:22:03,113
Nhưng mà...

84
00:22:03,112 --> 00:22:03,113
Em không kéo!

84
00:22:03,112 --> 00:22:03,113
Được rồi.

84
00:22:03,112 --> 00:22:03,113
Vừa... vừa... vừa rồi... là... cái gì vậy?

84
00:22:03,112 --> 00:22:03,113
Anh không biết. Chắc là gió thổi qua ngọn cỏ thôi.

84
00:22:03,112 --> 00:22:03,113
Th... thật không?

84
00:22:03,112 --> 00:22:03,113
Anh nghĩ thế... Bỏ anh ra nào...

84
00:22:03,112 --> 00:22:03,113
Gì thế?

84
00:22:03,112 --> 00:22:03,113
Kotori... nếu em cứ bám thế này 
thì không đi tiếp được đâu.

84
00:22:03,112 --> 00:22:03,113
Em... em không bám anh...

84
00:22:03,112 --> 00:22:03,113
Nhưng mà...

84
00:22:03,112 --> 00:22:03,113
Kệ đi! Cứ giữ thế này, đừng di chuyển gì cả!

84
00:22:03,112 --> 00:22:03,113
Kotori...

84
00:23:03,112 --> 00:23:03,113
Gì nữa?

84
00:23:03,112 --> 00:23:03,113
Nhìn em thế này...

84
00:23:03,112 --> 00:23:03,113
Khi em còn nhỏ, những lúc em sợ hãi,
em luôn bám vào anh thế này...

84
00:23:03,112 --> 00:23:03,113
Lúc đó, anh luôn xoa đầu em và
em bình tĩnh lại ngay sau đó...

84
00:23:03,112 --> 00:23:03,113
Em là chỉ huy Ratatoskr với rất nhiều
công việc và trách nhiệm. Những thứ đó
khiến em làm việc quá nhiều.

84
00:23:03,112 --> 00:23:03,113
Và những lúc chúng ta nói chuyện,
em không thay đổi so với ngày xưa.
Em vẫn là đứa em bé nhỏ rất quan trọng của anh.

84
00:23:03,112 --> 00:23:03,113
Shi... Shidou...

84
00:23:03,112 --> 00:23:03,113
Ổn thôi. Anh đây. Em thấy đỡ hơn chưa?

84
00:23:03,112 --> 00:23:03,113
Vâng...

84
00:23:03,112 --> 00:23:03,113
Này!

84
00:23:03,112 --> 00:23:03,113
A, mọi người đến rồi à?

84
00:23:03,112 --> 00:23:03,113
Shidou! Anh đi lâu quá nên bọn em đi tìm đấy!

84
00:23:03,112 --> 00:23:03,113
Ừ... anh xin lỗi...

84
00:23:03,112 --> 00:23:03,113
Kotori cũng ở đây à?
Chúng tôi không thấy cô nên lo lắm đấy.

84
00:23:03,112 --> 00:23:03,113
Em rất mừng... vì tìm được anh chị đấy...

84
00:23:03,112 --> 00:23:03,113
Xin lỗi nhé... anh bị lạc và 
Kotori đã tìm thấy anh.

84
00:23:03,112 --> 00:23:03,113
Vậy à? Thật may là tìm thấy hai người đấy.
Chúng ta đi thôi!

84
00:23:03,112 --> 00:23:03,113
Ừ...

84
00:23:03,112 --> 00:23:03,113
Rốt cuộc Tonomachi đang ở chỗ quái nào thế?

84
00:23:03,112 --> 00:23:03,113