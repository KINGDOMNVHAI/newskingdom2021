﻿368
00:16:00,512-->00:16:04,113
Đến lúc rồi Itsuka! Chúng ta hãy
cùng đến thiên đường trần thế nào!

368
00:16:04,512-->00:16:07,513
Không, trước tiên chúng ta phải đi xem
bóng đá. Vì chúng ta không tham gia 
nên phải đi cổ vũ họ.

368
00:16:08,112-->00:16:13,513
Để sau, để sau đi! Phải ưu tiên 
con gái trước chứ, phải không?
Nhanh lên, chúng ta phải đi nhìn trộm!

368
00:16:14,112-->00:16:15,913
Ông định đi theo dõi họ à? 
Chúng ta đi cổ vũ họ mà.

368
00:16:16,112-->00:16:20,513
Dẹp mấy thứ đó đi Itsuka-chan!
Vậy bây giờ chúng ta đi đâu đây?

368
00:16:20,913-->00:16:23,113
Thằng cha này không bao giờ thay đổi...
Vậy mình nên đi cổ vũ ai đây?

368
00:16:24,112-->00:16:36,113
1) Cổ vũ Tohka.
2) Cỗ vũ Origami.
3) Cổ vũ Rinne.

368
00:16:42,112-->00:16:51,113
Ồ! Bắt đầu rồi! 
Toàn những cô gái dễ thương!
Bộ váy thể dục đó dễ thương phải không Itsuka?
Đó là báu vật đấy!

368
00:16:51,512-->00:16:54,113
Ông không còn nghĩ ra gì khác à?
Cả ngày ông chỉ toàn nói về chủ đề đó.

368
00:16:54,512-->00:17:01,113
Thôi nào, nhìn kìa! 
Nếu không phải lúc này thì 
còn lúc nào để ngắm nữ sinh mặc đồng phục chứ?

368
00:17:01,312-->00:17:02,313
Chẳng biết nói gì nữa...

368
00:17:02,512-->00:17:04,313
Này cậu! Đánh đi nào!

368
00:17:07,112-->00:17:10,513
Cô ấy đánh tốt thật!

368
00:17:11,112-->00:17:12,513
Phải, tôi cũng thấy vậy...

368
00:17:12,713-->00:17:14,113
Gì thế nhỉ? Trông Rinne như đang tỏa sáng...

368
00:17:14,312-->00:17:16,513
Khi Rinne chơi, cô ấy mang một biểu hiện vui vẻ.
Một cái gì đó rất tươi vui.

368
00:17:19,312-->00:17:22,113
Không thể nào! Cách di chuyển đó!

368
00:17:22,312-->00:17:23,113
Hở?

368
00:17:24,512-->00:17:27,113
Ra ngoài rồi!

368
00:17:27,512-->00:17:28,613
Ai thắng vậy?

368
00:17:29,112-->00:17:34,113
Giờ tỉ số là 11:11
Chúng ta bị bắt kịp rồi!

368
00:17:34,312-->00:17:35,513
Còn bao nhiêu thời gian?

368
00:17:36,112-->00:17:42,113
Giờ đã là hiệp hai.
Thời gian còn lại là... 5 phút.

368
00:17:42,312-->00:17:43,513
Họ có thể giành chiến thắng không?

368
00:17:43,913-->00:17:45,113
Tôi không biết nữa...

368
00:17:45,512-->00:17:46,513
Phải, tôi cũng nghĩ vậy...

368
00:17:47,112-->00:17:56,113
Đừng nản chí. Các cô gái dễ thương 
sẽ chiến thắng. Tôi tin là vậy.
Tôi biết vì đó là quy luật đấy.

368
00:17:56,312-->00:17:58,513
Thật à? Đôi khi nó không giúp ích gì cả!

368
00:17:58,913-->00:18:07,113
Cuộc thi này không áp dụng các quy tắc chính thức.
Ở đây có quy tắc đơn giản và an toàn hơn.

368
00:18:07,412-->00:18:08,513
Quy tắc đơn giản à?

368
00:18:08,912-->00:18:20,913
Nó được gọi là cú đánh mềm.
Nhìn đi, có 6 người trong một đội, phải không?
Tổng cộng là 12 người. 
(Search Lacrosse để xem bóng vợt)

368
00:18:21,112-->00:18:22,513
Đúng vậy!

368
00:18:22,912-->00:18:33,713
12 người bao gồm cả 2 đội
sẽ đánh những cú đánh mềm.
Quy tắc rất đơn giản, 
chỉ cần đánh quả bóng vào gôn ở mỗi bên

368
00:18:33,913-->00:18:35,313
Nghe giống như bóng đá vậy.

368
00:18:35,712-->00:18:42,913
Nhưng trong trò này, ông sử dụng 
vợt như hockey trên băng. Vì vậy...

368
00:18:43,112-->00:18:44,113
Vì vậy...?

368
00:18:44,312-->00:18:53,113
Đây là sự khác biệt với bóng đá,
Mỗi người sẽ phải cúi xuống dùng vợt 
để đưa bóng vào mục tiêu.

368
00:18:53,312-->00:18:55,513
Và cả chuyền bóng à?

368
00:18:55,912-->00:19:05,113
Đúng vậy. Vì thế Rinne không thể 
lấy bóng của đối phương một mình.
Cô ấy và các cô gái phải làm cùng nhau.

368
00:19:05,312-->00:19:06,113
À...

368
00:19:06,312-->00:19:10,513
Đương nhiên không chỉ vậy. 
Tôi không phải là thằng ngốc.

368
00:19:11,512-->00:19:22,113
Hãy nghĩ đi, Rinne-chan sẽ lộ hàng.
Nếu ông để ý, Rinne-chan không thể
không phạm sai lầm.

368
00:19:25,812-->00:19:27,313
Rinne? Cậu không sao chứ?

368
00:19:28,412-->00:19:35,513
Tôi không nhìn thấy nó vì bụi cát,
Việc nhìn trộm Rinne ngoài dự kiến rồi!

368
00:19:36,112-->00:19:45,313
Lần này, tôi nhất định phải thấy nó.
Rinne đã chỉ còn một ít thời gian thôi, Itsuka!

368
00:19:45,912-->00:19:47,713
Rinne! Cố lên!

368
00:19:48,512-->00:19:56,113
Bên đó là...
Tonomachi-kun, em hết ho rồi à?

368
00:19:57,112-->00:20:03,313
Tama-chan, không phải, Okamine-sensei!
Có chuyện gì vậy ạ?

368
00:20:04,112-->00:20:13,313
Cô đến xem bóng vợt.
Bây giờ không phải lúc rồi. 
Tonomachi-kun, qua đây chút nhé.

368
00:20:14,112-->00:20:15,513
Dạ?

368
00:20:15,912-->00:20:20,113
Trước tiên, em phải đến phòng y tế đã.

368
00:20:21,112-->00:20:25,513
Không, em vẫn còn khỏe mà...

368
00:20:26,512-->00:20:34,113
Không được! Em đang bị bệnh mà! Đi thôi!

368
00:20:35,112-->00:20:39,513
Này Itsuka! Nói gì đi!
Chúng ta là bạn mà, phải không?

368
00:20:39,812-->00:20:43,513
Sensei... Cậu ta bị bệnh dễ lây đấy.
Nếu cậu ở đây sẽ lây cho người khác.
Cô đưa cậu ấy ra ngoài đi.

368
00:20:43,912-->00:20:47,113
Itsuka, chẳng lẽ...

368
00:20:48,112-->00:20:59,513
Đây là chuyện lớn đấy!
Tonomachi-kun! Em muốn cổ vũ cho lớp.
Điều đó rất tốt, nhưng em phải quan tâm đến sức khỏe!

368
00:21:00,512-->00:21:05,113
Không, không! Tama-chan... chuyện này.

368
00:21:05,512-->00:21:16,113
Giờ không phải lúc!
Cô sẽ đưa em đến phòng y tế!
Em cần có giáo viên đi cùng!
Ở đây giao cho em nhé!

368
00:21:16,312-->00:21:17,313
Ờ! Cô chăm sóc Tonomachi tốt nhé!

368
00:21:17,913-->00:21:23,113
Tuổi trẻ... tuổi thanh xuân của tôi!!!

368
00:21:23,512-->00:21:25,113
Cuối cùng mình cũng có thể tập trung vào trận đấu.

368
00:21:27,112-->00:21:28,913
Ồ, phải rồi! Nhưng thời gian còn lại...

368
00:21:29,112-->00:21:30,113
Có lẽ đây là cơ hội cuối cùng của cậu!

368
00:21:30,312-->00:21:31,713
Ồ! Rinne không đỡ được đâu!

368
00:21:31,913-->00:21:33,313
Dừng lại! Cậu không thể chuyền 
cho Rinne bây giờ!

368
00:21:33,512-->00:21:36,513
Tôi biết tôi muốn Rinne không bị chú ý.
Nhưng, đó là những gì đối thủ nghĩ ...
Hở? Rinne vượt qua hàng thủ?

368
00:21:36,913-->00:21:38,113
Thật không?
Đồng đội giữ bóng đang chạy bên kia.
Họ sắp chuyền cho cô ấy.

368
00:21:38,313-->00:21:40,113
Bên này!

368
00:21:40,313-->00:21:41,313
Ngay lúc này!

368
00:21:41,912-->00:21:42,913
Nào! Dứt điểm đi!

368
00:21:43,112-->00:21:45,113
Dứt điểm!

368
00:21:48,912-->00:21:55,113
Kết thúc! 12/11
Học sinh lớp 2-4 giành chiến thắng!

368
00:21:56,112-->00:21:59,513
Thành công rồi! Nhờ mọi người đấy!

368
00:22:00,112-->00:22:03,113
Chiến thắng trong sự cố gắng của mọi người.
Sau đó là một nụ cười rạng rỡ của Rinne.
Tôi chỉ muốn ngắm nó mãi.

368
00:22:07,912-->00:22:09,513
Trưa rồi à? Nhanh thật đấy.

368
00:22:09,912-->00:22:11,113
Không biết hôm nay mình sẽ ăn gì đây?

368
00:22:11,512-->00:22:14,913
Anh đến trễ đấy, Shidou!

368
00:22:15,112-->00:22:16,113
Ừ, anh xin lỗi.





368
00:25:05,912-->00:25:10,313
Các em, tất cả đều thi đấu 
rất tốt trong hội thao đấy.

368
00:25:11,112-->00:25:21,113
Nhờ những nỗ lực của tất cả mọi người, 
chúng ta đã có kết quả tốt... nhưng tiếc là 
chúng ta vẫn không có giải nhất.

368
00:25:22,912-->00:25:30,113
Thật không may là chúng ta thiếu vài vị trí. 
Vì vậy các bạn nam đã thua phải không?

368
00:25:31,912-->00:25:38,113
Nếu các em ra ngoài và hít một hơi thật sâu,
các em sẽ nghĩ đây là kết quả tốt nhất
mình đạt được mà phải không?

368
00:25:43,912-->00:25:46,313
Những gì cô giáo nói là đúng. Nhưng đối với
nhiều người, dù họ có làm hết sức, họ vẫn
cảm thấy bực bội vì đã thua.

368
00:25:46,513-->00:25:49,113
Đúng, đây chỉ là những gì tôi nghĩ.
Tôi là một khán giả... Nhưng cũng bất ngờ
trước những cố gắng nghiêm túc của mọi người.

368
00:25:50,112-->00:25:59,113
Shidou, đây là lỗi của em à?
Nếu em chịu khó luyện tập nhiều hơn...

368
00:25:59,312-->00:26:00,313
Không, đó không phải là lỗi của em, Tohka. 
Không cần quan trọng chuyện đó đâu.

368
00:26:00,512-->00:26:03,513
Vậy à...

368
00:26:04,112-->00:26:07,113
Nói ra cảm thấy hơi chán nản. 
Cả Tohka và Origami đều đã thua. 
Chỉ có Rinne là chiến thắng.

368
00:26:09,912-->00:26:12,513
Ngay cả Origami, dù khuôn mặt không cảm xúc,
tôi đoán cô ấy cũng buồn... 
Tôi tự hỏi phải chăng cô ấy đã làm hết sức?

368
00:26:13,112-->00:26:15,513
Khi ở trong lớp này, không khí luôn vui vẻ.
Nếu lớp đầy những bộ mặt u ám thế này thì
không khí sẽ nặng nề lắm.

368
00:26:16,112-->00:26:17,113
Sao vậy Rinne?

368
00:26:17,312-->00:26:20,313
Tiếc thật đấy... Tớ xin lỗi.

368
00:26:20,912-->00:26:23,113
Hở? Nhưng đây đâu phải là lỗi của cậu.

368
00:26:24,112-->00:26:32,513
Hở? À, ý tớ là nếu mọi người luyện tập 
nhiều hơn thì có thể có kết quả tốt hơn.

368
00:26:32,912-->00:26:34,513
Cậu đã làm hết sức rồi Rinne.
Tớ chắc chắn vậy đấy.

368
00:26:35,112-->00:26:39,113
Cảm ơn nhé, Shidou. Chỉ cần vậy thôi,
tớ đã cảm thấy hạnh phúc rồi.

368
00:26:39,312-->00:26:40,113
Ờ... ờ...

368
00:26:42,112-->00:26:53,113
Được rồi. Sau hội thao là đến kỳ thi 
rồi đấy! Chuẩn bị tinh thần ôn thi đi nhé!

368
00:26:55,112-->00:27:03,513
Nếu các em phàn nàn, các bài kiểm tra 
sẽ nhiều hơn. Vậy nên cố học cho tốt nhé.

368
00:27:05,812-->00:27:07,513
Sensei! Tonomachi đâu rồi ạ?

368
00:27:07,912-->00:27:11,513
Không sao đâu, em ấy đến phòng y tế rồi.

368
00:27:12,112-->00:27:13,113
Vậy sao?

368
00:27:13,312-->00:27:19,113
Em ấy đã được kiểm tra tổng quát rồi.
Em không phải lo đâu.

368
00:27:19,312-->00:27:21,113
Vâng, em hiểu rồi. Cảm ơn cô...

368
00:27:21,312-->00:27:26,113
Sức khoẻ của học sinh
là trách nhiệm của cô mà.

368
00:27:26,312-->00:27:28,513
Đây có phải là thảm hoạ không?
Nó sẽ là bài học tốt cho Tonomachi.

368
00:27:28,912-->00:27:30,113
Vậy thì... hôm nay tôi sẽ hẹn hò với ai?

368
00:27:54,112-->00:27:57,113
Đi đến Phòng vật lý
1) Đồng ý
2) Không đồng ý

368
00:28:02,112-->00:28:04,513
Chán thật!
Reine-san, Kotori đều không ở đây...

368
00:28:04,912-->00:28:06,513
Mình sẽ gọi xem Kotori đang làm gì.

368
00:28:09,512-->00:28:12,113
Chuyện gì vậy, onii-chan?

368
00:28:12,312-->00:28:13,113
Em đang ở đâu thế?

368
00:28:13,312-->00:28:17,113
Vâng, đó là vấn đề đấy! 
Em đang ở đâu nhỉ?

368
00:28:17,312-->00:28:19,113
Em chỉ có thể ở
trường học hoặc ở nhà... trường học!

368
00:28:19,312-->00:28:23,113
Đúng rồi đấy! Bây giờ nó vẫn là trường học.

368
00:28:23,312-->00:28:24,513
Làm sao anh biết?

368
00:28:24,912-->00:28:26,913
Anh nhìn vào màn hình.

368
00:28:27,112-->00:28:31,113
Đúng là onii-chan của em!
Vậy chào nhé!

368
00:28:31,912-->00:28:33,113
A... Đợi đã! Đừng cúp máy!

368
00:28:33,312-->00:28:35,113
Gì vậy?

368
00:28:35,312-->00:28:36,513
Em có rảnh không?

368
00:28:36,812-->00:28:40,713
Em hơi bận, nhưng sao?

368
00:28:40,912-->00:28:42,113
Nếu em rảnh, anh muốn đi mua sắm với em.

368
00:28:42,312-->00:28:44,113
Đó có phải là hẹn hò không?

368
00:28:44,312-->00:28:46,113
Ừ... cũng giống vậy.

368
00:28:46,312-->00:28:47,113
Ồ, nhưng anh nghĩ nếu em bận...

368
00:28:47,312-->00:28:50,513
Không sao. Hôm nay em hoàn toàn rảnh mà!

368
00:28:50,912-->00:28:52,113
Thế à? Vậy chúng ta gặp nhau
ở trước nhà ga nhé.

368
00:28:52,512-->00:28:57,113
Em hiểu rồi!
Anh mà đến trễ hơn em là sẽ có phạt đấy!

368
00:28:57,312-->00:28:58,513
Gì cơ? Trò chơi có phạt à...?

368
00:29:00,512-->00:29:03,113
Kotori. Hẹn hò bằng trò chơi có phạt...
Nếu em không đến sớm, em sẽ biết tay.

368
00:29:07,912-->00:29:10,113
Ha, ha... cuối cùng cũng đến...

368
00:29:10,312-->00:29:15,113
Nhìn từ xa, anh thở như một con thú vậy.

368
00:29:15,312-->00:29:17,113
Hở? Em thấy anh à...

368
00:29:17,312-->00:29:28,313
Phản ứng lần đầu bị bắt gặp sao? 
Để không làm anh thất vọng,
em đã bị cảnh sát bắt gặp một lần.

368
00:29:28,512-->00:29:30,113
Vậy... Ủa, không phải em đeo nơ trắng à?

368
00:29:30,912-->00:29:33,513
Thì sao?

368
00:29:34,112-->00:29:37,113
Không... Anh chỉ tưởng em đeo nơ trắng.
Nó không phải là xấu.
Dù thế nào, em vẫn là Kottori.

368
00:29:37,512-->00:29:40,113
Vâng. Không sao đâu.

368
00:29:40,312-->00:29:43,113
Còn nữa... Sao em thay quần áo?
Anh tưởng em đi thẳng từ trường...

368
00:29:44,112-->00:29:48,113
Em thay đồ vì đây là hẹn hò mà.

368
00:29:48,312-->00:29:49,513
Hở? Gì cơ?

368
00:29:50,112-->00:29:52,113
Không có gì.

368
00:29:53,112-->00:29:54,113
Vậy giờ sao?

368
00:29:54,312-->00:29:55,113
Sao cái gì?

368
00:29:55,312-->00:30:00,913
Anh định đi đâu?
Đây là hẹn hò phải không?

368
00:30:01,113-->00:30:03,113
À, đúng rồi. Đây...

368
00:30:03,512-->00:30:06,113
Gì thế, đi mua quần áo à?

368
00:30:06,312-->00:30:08,313
Em nghĩ gì vậy?
Kế hoạch ngày hôm nay là...

368
00:30:08,513-->00:30:10,313
...đi ăn nhà hàng gia đình nhé?

368
00:30:11,112-->00:30:14,513
Em không mong đợi điều đó. 
Em thất vọng đấy.

368
00:30:14,912-->00:30:15,913
À...

368
00:30:16,512-->00:30:18,113
Vâng, chúng ta đi thôi.

368
00:30:18,312-->00:30:19,513
Um... em có muốn đi ăn 
nhà hàng gia đình thật không?

368
00:30:20,112-->00:30:25,513
Anh không thể đi ăn một mình,
vậy nên em sẽ đi với anh.

368
00:30:25,912-->00:30:27,113
À, được rồi...

368
00:30:30,812-->00:30:35,513
Chào Shin. Kotori có đi cùng cậu không?

368
00:30:35,912-->00:30:36,913
Ồ, vâng. Em ấy ở đây...

368
00:30:37,112-->00:30:44,113
À không... Kotori tắt máy 
nên tôi thắc mắc thôi.

368
00:30:44,312-->00:30:45,313
Có chuyện gì sao?

368
00:30:45,512-->00:30:54,513
Không, tôi muốn liên lạc với Kotori,
Nó không hoạt động.
Tôi nghĩ cô ấy không muốn bị quấy rầy.

368
00:30:54,912-->00:30:55,913
À, vâng...

368
00:30:56,112-->00:31:02,913
Em nhắn với Kotori được không?
Kannazuki hiện đang ở Fraxinus
Anh ấy muốn cô ấy đến đó.

368
00:31:03,112-->00:31:10,713
Cá nhân tôi không muốn can thiệp vào hai người.
Dù sao, tôi có việc để báo cáo ngay.

368
00:31:10,912-->00:31:12,113
Vâng, em hiểu rồi. 
Em sẽ nói với em ấy...

368
00:31:14,912-->00:31:16,113
Gì vậy?

368
00:31:16,312-->00:31:19,513
À, là Reine-san.
Kannazuki-san cần báo cáo khẩn cấp.
Họ muốn em đến Fraxinus ngay lập tức ...

368
00:31:20,512-->00:31:25,113
Vậy à? Vậy ngưng buổi hẹn hò lại đi.

368
00:31:25,312-->00:31:26,113
Được rồi. Tiếc thật đấy...

368
00:31:27,112-->00:31:31,513
Vâng, em sẽ quay lại ngay. Em xin lỗi.

368
00:31:32,112-->00:31:33,113
Ừ...

368
00:31:34,912-->00:31:36,513
Mình sẽ hẹn hò một mình.
Em ấy thực sự rất bận.

368
00:31:47,112-->00:31:50,113
Đi đến Đồi núi?
1) Đồng ý
2) Không đồng ý

368
00:31:54,112-->00:31:57,113
Phù... Mình toàn nghĩ về Kotori,
vậy nên mình chẳng đi chơi đâu cả,
chỉ quanh quẩn trong công viên.

368
00:31:57,312-->00:31:59,113
Hôm nay đã kết thúc...
Mình sẽ ở nhà đợi Kottori

368
00:32:10,112-->00:32:11,513
Em về rồi...

368
00:32:12,312-->00:32:14,113
Ừ, cuối cùng em cũng về.

368
00:32:14,512-->00:32:19,113
Shidou, từ chiều đến giờ anh làm gì?

368
00:32:19,512-->00:32:21,113
À không... anh không làm gì cả.

368
00:32:21,912-->00:32:25,113
Em mệt quá... 
Anh có làm nước tắm chưa?

368
00:32:25,312-->00:32:26,313
Rồi. Giờ em chỉ cần vào thôi.

368
00:32:26,512-->00:32:32,113
Vậy em sẽ tắm.
Có thể mất một lúc đấy.

368
00:32:32,312-->00:32:33,513
À... chờ một chút!

368
00:32:34,112-->00:32:40,113
Gì thế? Anh muốn tắm chung với em à?

368
00:32:40,312-->00:32:41,513
Không phải thế!

368
00:32:42,112-->00:32:44,113
Vậy thì là gì?

368
00:32:44,312-->00:32:46,113
À... Em có muốn tiếp tục 
buổi hẹn hò hồi sáng không?

368
00:32:47,112-->00:32:51,513
Bây giờ à? Giờ thì còn đi đâu nữa?

368
00:32:51,912-->00:32:55,113
Buổi hẹn hôm nay bị gián đoạn phải không?
Ít nhất chúng ta cũng nên 
đi dạo vào ban đêm... được chứ?

368
00:32:55,312-->00:32:58,113
Và... em cũng làm việc nhiều quá rồi.
Em không muốn đi dạo cho thoải mái sao?

368
00:32:59,112-->00:33:03,113
Điều đó đúng... nhưng ...

368
00:33:03,512-->00:33:05,513
Nhìn em gái mình mệt mỏi, anh...

368
00:33:08,812-->00:33:11,513
À, nếu em mệt, 
không nhất thiết là hôm nay đâu.
Có thể ngày mai hoặc...

368
00:33:12,112-->00:33:15,113
Em hiểu rồi. Đi thôi.

368
00:33:15,312-->00:33:17,313
Hở, em không muốn đi tắm sao?

368
00:33:17,912-->00:33:21,113
Em đã nói đi ngay bây giờ mà.

368
00:33:21,912-->00:33:23,113
Ừ, em ổn chứ?

368
00:33:23,512-->00:33:29,113
Em bảo em muốn đi. Hay là sao?
Anh không muốn à?

368
00:33:30,112-->00:33:31,113
À không, anh sẽ đi! Anh muốn đi!

368
00:33:32,112-->00:33:34,913
Vâng, chuẩn bị đi.

368
00:33:35,112-->00:33:36,113
Được rồi...

368
00:33:39,912-->00:33:41,113
Đã lâu rồi chúng ta mới đi thế này.

368
00:33:41,312-->00:33:43,113
Vâng...

368
00:33:43,812-->00:33:46,513
Đã có một thời, em cũng thế này, 
giống như một đứa trẻ đi sau anh.

368
00:33:46,912-->00:33:48,113
Giờ em đã thành một người bận rộn.

368
00:33:48,312-->00:33:54,113
Không phải thế. 
Em chỉ giải quyết hậu quả 
của Tinh Linh thôi.

368
00:33:54,312-->00:33:56,113
Phải... và em đã thành người lớn 
khi em làm điều đó.

368
00:33:56,312-->00:33:57,513
Người lớn sao?

368
00:33:58,112-->00:34:00,113
Ừ. Đêm nay, anh cảm thấy
một cái gì đó như dòng chảy thời gian.

368
00:34:00,512-->00:34:04,113
Hẹn hò... chỉ đi dạo thế này thôi sao?

368
00:34:04,313-->00:34:06,513
Thế à? Vậy chúng ta nắm tay nhau
cho giống hẹn hò nhé.

368
00:34:07,112-->00:34:11,113
Anh nói gì vậy! Không có chuyện đó đâu!

368
00:34:11,312-->00:34:12,913
Um... Em không thích à?

368
00:34:13,112-->00:34:16,113
Không phải... em không muốn...

368
00:34:16,512-->00:34:18,113
Vậy, bây giờ... nắm tay nhé?

368
00:34:21,912-->00:34:23,513
Em thấy sao? Giống hẹn hò không?

368
00:34:23,812-->00:34:24,913
Thật ngớ ngẩn.

368
00:34:25,112-->00:34:27,113
Hẹn hò kiểu này không tệ phải không?

368
00:34:32,112-->00:34:33,113
Em biết đấy, Kotori...

368
00:34:33,312-->00:34:34,513
Gì cơ?

368
00:34:34,912-->00:34:37,113
Anh hiểu một chút về công việc của em,
không phải nó quá sức lắm sao?

368
00:34:37,312-->00:34:38,913
Em hiểu rồi.

368
00:34:39,112-->00:34:40,713
Nếu có bất cứ điều gì anh có thể làm, 
hãy nói anh biết.

368
00:34:40,912-->00:34:42,113
Anh sẽ làm mọi thứ vì em.

368
00:34:45,912-->00:34:47,113
Kotori... em có nghe không?

368
00:34:47,512-->00:34:54,913
"Làm mọi thứ" à?
Chắc đêm nay em ngủ ngon rồi.

368
00:34:55,112-->00:34:57,113
Hở? À không... Không phải mọi thứ đâu! 
Chỉ những thứ anh có thể giúp thôi!

368
00:34:57,312-->00:35:03,113
Thôi muộn rồi. Em về đây.
Em tin ở anh đấy, onii-chan.

368
00:35:03,312-->00:35:04,313
Ừ...

368
00:35:09,512-->00:35:11,513
Dù đang mùa hè, nhưng ban đêm vẫn lạnh.
Mình thấy lạnh quá.

368
00:35:11,912-->00:35:13,113
Mình nên đi tắm thôi.

368
00:35:16,112-->00:35:17,113
Hở? Đèn nhà tắm còn sáng à?

368
00:35:22,112-->00:35:23,113
Ơ?

368
00:35:23,512-->00:35:26,113
Shi... dou?

368
00:35:26,312-->00:35:27,513
Ko, Kotori...? Sao em lại...?

368
00:35:29,112-->00:35:32,313
Sao... sao anh vẫn còn 
nhìn chằm chằm thế?

368
00:35:33,112-->00:35:36,113
À... không phải...
anh tưởng em quên tắt đèn...

368
00:35:37,112-->00:35:41,113
Anh... anh... anh là đồ ngốc!

368
00:35:41,312-->00:35:43,113
Xin... xin lỗi! Anh không có ý đó!

368
00:35:43,312-->00:35:44,513
Hy vọng anh đã sẵn sàng rồi!

368
00:35:44,912-->00:35:46,113
Khoan... khoan đã. Cho anh nói...

368
00:35:50,912-->00:35:54,513
Thực sự em chịu thua anh rồi đấy.

368
00:35:54,912-->00:35:57,113
Anh xin lỗi... khi chúng ta còn nhỏ,
chúng ta đã tắm chung, vậy nên...

368
00:35:57,312-->00:36:00,113
Anh-vừa-nói-gì?

368
00:36:00,512-->00:36:02,113
Không có gì!

368
00:36:02,512-->00:36:04,113
Chán anh thật đấy...

368
00:36:04,312-->00:36:05,313
Anh xin lỗi...

368
00:36:05,512-->00:36:16,113
Nhưng tình hình không tốt
nên đừng lơ là đấy!
Mặt anh đờ đẫn như Reine ấy!

368
00:36:16,312-->00:36:17,513
Reine-san... cô ấy ổn chứ?

368
00:36:18,112-->00:36:27,913
À, không lạ với vẻ mặt của Reine nhỉ?
Đây không phải lần đầu cô ấy không ngủ được...

368
00:36:28,112-->00:36:34,113
Em chắc chắn 
có những thứ cần theo dõi.

368
00:36:34,312-->00:36:35,513
Vâng... Reine-san, anh đang lo lắng.

368
00:36:36,112-->00:36:39,113
Anh đang nghĩ chuyện bậy bạ phải không?

368
00:36:39,512-->00:36:41,113
Không. Không phải thế... 
Anh chỉ lo cho Reine-san.

368
00:36:41,312-->00:36:43,513
Chuyện gì xảy ra... 
Kể từ hôm đó, chuyện gì đã xảy ra? 
Em đã tìm thấy gì rồi?

368
00:36:44,112-->00:36:53,513
Không có gì cả. Có một sức mạnh 
Tinh Linh trong toàn bộ thành phố Tengu.
Em đã cố tìm hiểu, nhưng...

368
00:36:53,912-->00:36:55,113
Em không biết thêm gì nữa sao?

368
00:36:55,312-->00:37:06,313
Nếu có thể thì em đã biết rồi.
Nhưng em không thể liên lạc 
với trụ sở chính, và Fraxinus cũng vậy.

368
00:37:06,912-->00:37:21,513
Hiện tại, chúng ta đang ở trong
một cái gì đó khổng lồ... Có lẽ điều này 
đang xảy ra xung quanh thành phố Tengu.
Đó là tất cả những gì em biết hiện giờ.

368
00:37:21,912-->00:37:22,913
Hiểu rồi...

368
00:37:23,112-->00:37:25,113
Anh chưa hiểu đâu.

368
00:37:25,312-->00:37:26,113
Nhưng...

368
00:37:26,312-->00:37:37,113
Nhiệm vụ của anh là ổn định tinh thần 
bằng cách hẹn hò với Tinh Linh.
Nếu đến giờ mà anh còn chưa làm, anh đúng là kẻ ngốc.

368
00:37:37,312-->00:37:38,813
Anh xin lỗi... em đang lo lắng à?

368
00:37:39,112-->00:37:49,913
Phải. Một chút lo lắng.
Anh phải càng bình tĩnh càng tốt.
Nếu không, Tinh Linh sẽ không cải thiện tinh thần đâu.

368
00:37:50,112-->00:37:51,113
Được rồi...

368
00:37:56,112-->00:37:57,313
Haa... Hôm nay nhiều việc quá...

368
00:37:57,912-->00:37:59,113
Toàn thân mình mệt rã rời rồi...

368
00:38:01,112-->00:38:02,113
Phù... Chúc ngủ ngon...

368
00:38:07,112-->00:38:09,113
Tiếng gì vậy? Ai vào nhà tắm à?

368
00:38:09,312-->00:38:10,113
Hmm... ngủ thôi...

368
00:38:18,912-->00:38:20,113
Đây... là một giấc mơ...?

368
00:38:20,312-->00:38:23,513
Tôi đang tiếp tục theo dõi...

368
00:38:24,112-->00:38:25,113
Cô đang theo dõi sao...?

368
00:38:25,912-->00:38:27,113
Tại sao?

368
00:38:27,312-->00:38:30,113
Để tránh nỗi buồn tái diễn...

368
00:38:30,512-->00:38:32,113
Nỗi buồn... Gì cơ...?

368
00:38:32,312-->00:38:34,113
Tôi không cần phải hiểu...

368
00:38:34,512-->00:38:35,313
Hở?

368
00:38:35,513-->00:38:39,113
Mọi thứ đều có thể ở thiên đường...

368
00:38:39,312-->00:38:40,113
Này, cô muốn nói gì...?

368
00:38:40,512-->00:38:41,713
Cái gì... ý thức của mình...

368
00:38:41,912-->00:38:45,913
Cậu... hãy tin tôi.

368
00:38:46,112-->00:38:48,113
Trước khi cô nói tôi, cô... Um...

368
00:38:53,112-->00:38:58,113
Ngày 28 tháng 6

368
00:39:40,112-->00:39:41,113


368
00:39:40,112-->00:39:41,113

