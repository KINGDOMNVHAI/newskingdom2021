﻿26
00:00:02,712 --> 00:00:07,513
Hôm nay là thứ 7. Được rồi.
Mình sẽ cố rủ Yoshino đi hẹn hò.

26
00:00:12,112 --> 00:00:15,113
Chào buổi sáng, Yoshino.

26
00:00:16,112 --> 00:00:19,513
Chào... buổi sáng...

26
00:00:20,112 --> 00:00:27,113
Yoshino vẫn buồn. Nhưng tôi biết
em ấy sẽ không để những chuyện xảy ra 
hôm qua kéo sang hôm nay.

26
00:00:28,112 --> 00:00:34,513
Nhưng anh hứa anh sẽ giúp em tươi cười lần nữa.
Anh chắc rằng Yoshinon cũng muốn như vậy.

26
00:00:35,512 --> 00:00:40,113
Này Yoshino, hôm nay em có muốn đi đâu không?

26
00:00:43,912 --> 00:00:46,113
Hãy hẹn hò đi.

26
00:00:47,112 --> 00:00:50,113
Hẹn hò...

26
00:00:51,112 --> 00:00:56,113
Tất nhiên rồi. Nếu em không muốn đi 
thì anh cũng không ép đâu.

26
00:01:00,112 --> 00:01:04,113
Có vẻ em không có tâm trạng nhỉ.

26
00:01:05,112 --> 00:01:10,113
Không... em muốn đi...

26
00:01:13,112 --> 00:01:24,113
Nhờ anh... dẫn em đi...
Em xin lỗi... vì không thể... vui được...

27
00:01:25,112 --> 00:01:31,113
Đừng lo chuyện đó. Được rồi, đi thôi nào!
Em thay đồ đi.

28
00:01:32,112 --> 00:01:35,113
V... vâng!

29
00:01:38,112 --> 00:01:44,113
"Yoshino, cố gắng lên". Tôi tự nhủ
khi Yoshino bước lên cầu thang.




30
00:01:51,112 --> 00:01:53,113
Yoshino.

31
00:01:53,512 --> 00:01:55,513
Vâng?

32
00:01:56,112 --> 00:01:59,113
Nắm tay anh nào.

33
00:02:02,912 --> 00:02:05,113
Em không muốn sao?

34
00:02:06,112 --> 00:02:11,113
Em... không phiền đâu.

35
00:02:11,912 --> 00:02:13,513
Vậy thì... đây.

38
00:02:18,112 --> 00:02:27,113
Bàn tay Yoshino... lạnh hơn thường ngày.
Cũng hiểu được vì Yoshinon không ở đây.
Em ấy có chút không thoải mái.

39
00:02:30,912 --> 00:02:34,113
Giờ thì em muốn đi đâu nào?

40
00:02:35,112 --> 00:02:41,113
Đi đâu cũng được ạ.

41
00:02:42,112 --> 00:02:48,113
Anh hiểu rồi. Anh cũng không có kế hoạch gì.
Hay là giờ chúng ta đến cửa hàng tạp hóa nhé. 

42
00:02:49,112 --> 00:02:50,513
Vâng.

43
00:03:00,112 --> 00:03:08,113
Tôi đoán đưa em ấy ra ngoài là chưa đủ.
Nhưng tôi không thể bỏ cuộc.
Tôi cần cổ vũ Yoshino nhiều hơn.

39
00:03:08,512 --> 00:03:14,113
Yoshino nghĩ về Yoshinon một cách thân thương.
Tôi muốn em ấy giữ lấy những cảm xúc đó

40
00:03:15,213 --> 00:03:24,113
Nhưng tôi cũng có chút thắc mắc.
Nếu cảm xúc của em ấy quá lớn,
nó có làm tổn thương Yoshino không?
Em ấy có nên học tính tự lập không?

41
00:03:25,113 --> 00:03:31,113
Sẽ tốt hơn nếu em ấy quên đi Yoshinon
và tìm thấy hạnh phúc.

42
00:03:34,113 --> 00:03:36,113
Shidou-san?

44
00:03:38,112 --> 00:03:41,113
À, anh xin lỗi. Không có gì đâu.

45
00:03:42,112 --> 00:03:49,113
Tôi không nên nghĩ vậy. Yoshinon cũng là bạn tôi.
Tôi không muốn loại bỏ điều đó
ra khỏi tâm trí mình.

46
00:03:52,512 --> 00:03:55,113
Gì vậy Yoshino?

47
00:03:59,112 --> 00:04:02,113
Em đi đâu vậy, Yoshino?

48
00:04:09,112 --> 00:04:13,113
Yoshino, em không nên chạy một mình như vậy!

49
00:04:14,112 --> 00:04:23,113
Em xin lỗi... Nhưng Yoshinon...!
Yoshinon!

50
00:04:29,912 --> 00:04:32,113
Yoshino, đó là...

51
00:04:33,112 --> 00:04:35,113
Yoshinon!

52
00:04:40,112 --> 00:04:43,113
Ý em đó là Yoshinon sao?

53
00:04:44,112 --> 00:04:48,113
Một con thỏ nhồi bông đang ngồi trong tủ kính.

54
00:04:49,112 --> 00:05:54,113
Yoshinon! Sao cậu... lại ở đây?

55
00:05:06,112 --> 00:05:08,113
Yoshi... non...

56
00:05:10,512 --> 00:05:14,113
Tôi không thể bỏ mặc Yoshino một mình thế này được.

57
00:05:17,512 --> 00:05:21,113
Này Yoshino, Yoshinon là bạn em phải không?

41
00:05:25,112 --> 00:05:31,113
Yoshinon đôi khi nói nhiều,
nhưng cậu ấy luôn mạnh mẽ, tốt bụng và thân thiện.

42
00:05:32,112 --> 00:05:40,113
Vâng, Yoshinon... là bạn của em...

43
00:05:41,112 --> 00:05:45,113
Vậy sao chúng ta không để Yoshinon thanh thản?

44
00:05:49,112 --> 00:05:55,313
Chắc anh đã từng nói rồi,
nếu Yoshinon thấy em bây giờ,
cậu ấy sẽ buồn lắm.

45
00:05:59,112 --> 00:06:06,113
Thật lòng, anh không biết tại sao Yoshinon bất động,
hay làm sao để hồi sinh cậu ấy...
Anh không biết gì cả.

46
00:06:07,112 --> 00:06:12,113
Nhưng khi Yoshinon trở về,
anh biết cách để cậu ấy hạnh phúc.

47
00:06:13,113 --> 00:06:19,113
Đó là gì vậy? Làm thế nào ạ?

48
00:06:20,112 --> 00:06:24,113
Đó là cho cậu ấy thấy em đã trưởng thành thế nào.

49
00:06:25,112 --> 00:06:32,113
Cho Yoshinon thấy, dù không còn cậu ấy bên cạnh,
em vẫn sống tốt. Đó là cách tốt nhất.

50
00:06:33,112 --> 00:06:42,113
Nhưng mà... Yoshinon thật sự 
rất quan trọng với em. Em cần cậu ấy.

51
00:06:43,112 --> 00:06:48,113
Anh biết, nhưng em không thể yêu cầu
bạn bè em ở cạnh em mãi mãi được.

52
00:06:48,712 --> 00:06:56,113
Đứng vững bằng cả 2 chân trên mặt đất,
và tìm điểm mạnh của em.
Bạn em sẽ luôn bên cạnh em.

53
00:06:57,112 --> 00:07:06,113
Tình bạn là một con đường 2 chiều.
Thật tốt khi có người làm chỗ dựa,
nhưng nó phải đến từ 2 phía. 
Em phải hỗ trợ họ nữa.

54
00:07:07,112 --> 00:07:11,113
Đó mới là bạn thật sự đấy, Yoshino.

55
00:07:12,112 --> 00:07:16,113
Bạn... thật sự sao?

56
00:07:17,112 --> 00:07:25,113
Phải. Vậy nên anh muốn em phải trưởng thành
và mạnh mẽ hơn, Yoshino. Một ngày nào đó,
Yoshinon trở lại, em có thể hỗ trợ cậu ấy.

57
00:07:26,112 --> 00:07:31,113
Em... hỗ trợ...

58
00:07:32,112 --> 00:07:34,113
Yoshino...

59
00:07:35,112 --> 00:07:50,113
Em hiểu rồi. Em vẫn buồn, nhưng...
Khi Yoshinon trở lại, em sẽ... cố gắng hết sức!

60
00:07:51,112 --> 00:07:53,513
Đúng rồi. Cố lên, Yoshino!

61
00:07:54,512 --> 00:07:57,113
Vâng!

62
00:07:57,712 --> 00:08:05,113
Nhưng... em phải làm gì?

63
00:08:07,112 --> 00:08:18,113
Em cũng phải nghĩ... về bản thân mình nữa.

64
00:08:19,112 --> 00:08:28,113
Em sẽ... nghĩ về bản thân.
Những gì em có thể làm cho Yoshinon.

65
00:08:29,112 --> 00:08:38,113
Em đã hiểu rồi đấy. Cứ từ từ và làm theo
những gì trái tim mách bảo.
Nếu em gặp chuyện gì, có anh ở đây.

66
00:08:39,112 --> 00:08:44,113
Vâng, cảm ơn anh!




67
00:08:51,112 --> 00:08:56,113
Khuôn mặt của Yoshino buồn bã,
và đang học cách tiến lên.

68
00:08:57,112 --> 00:09:02,113
Này Yoshinon. Bạn em thật tốt bụng
và đang làm việc chăm chỉ.

69
00:09:03,112 --> 00:09:11,113
Lý do em rời bỏ Yoshino là vì em biết
chuyện này sẽ xảy ra phải không?
Em biết em ấy có thể đứng vững mà không có em.

74
00:09:12,112 --> 00:09:17,113
Anh biết mọi thứ em làm đều là vì
lợi ích của Yoshino.

75
00:09:18,112 --> 00:09:27,113
Nhưng em biết không, Yoshinon.
Không có em, trái tim Yoshino tan vỡ.
Không ai biết làm sao để đối diện
với sự mất mát bạn bè.

76
00:09:28,112 --> 00:09:33,513
Vì vậy, dù mất bao lâu đi nữa,
xin em hãy trở về, Yoshinon.

77
00:09:34,512 --> 00:09:39,513
Yoshino cần Yoshinon. Em ấy tươi cười
rất nhiều khi ở bên cạnh Yoshinon.

78
00:09:40,512 --> 00:09:48,513
Yoshinon, cho đến khi em quay lại,
anh sẽ bảo vệ Yoshino. Một ngày nào đó,
Yoshino sẽ lại cười tươi như hoa hướng dương.

79
00:09:54,112 --> 00:09:58,113
Ngày 2 tháng 7

80
00:07:58,112 --> 00:07:59,513
