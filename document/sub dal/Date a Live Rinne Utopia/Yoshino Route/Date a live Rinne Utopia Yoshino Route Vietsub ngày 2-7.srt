﻿0
00:00:07,112 --> 00:00:09,113
Shidou-san!

1
00:00:10,512 --> 00:00:15,113
Hả? Chuyện gì vậy? Có kẻ nào tấn công sao?

2
00:00:16,112 --> 00:00:21,113
Là em à, Yoshino. Sao vậy? Trời
Trời vừa sáng thôi mà.

3
00:00:22,112 --> 00:00:32,113
Em đã nghĩ ra rồi! 
Việc em... có thể làm... cho Yoshinon... 

4
00:00:32,712 --> 00:00:34,513
Thật sao?

5
00:00:35,112 --> 00:00:36,513
Vâng!

6
00:00:37,512 --> 00:00:43,113
Yoshino, em đã suy nghĩ chuyện đó...
suốt từ hôm qua sao?

7
00:00:43,512 --> 00:00:45,513
Vâng.

8
00:00:46,112 --> 00:00:49,513
Em đã ngủ ít phải không?

9
00:00:50,512 --> 00:00:57,113
Em không sao... Đó là vì Yoshinon mà...

10
00:00:58,112 --> 00:01:02,113
Yoshino, em thật tuyệt. Anh bất ngờ đấy.

11
00:01:03,112 --> 00:01:09,113
Không... phải vậy đâu.

12
00:01:09,912 --> 00:01:16,113
Không, em thật sự rất tuyệt.
Em đã rất cố gắng vì ai đó quan trọng với em.

13
00:01:21,112 --> 00:01:26,113
Vậy anh có thể hỏi việc em định làm
cho Yoshinon là gì không?

14
00:01:27,112 --> 00:01:39,113
Vâng... Um... Đó là vẽ... 
Vẽ một bức tranh ạ.

15
00:01:40,112 --> 00:01:42,513
Một bức tranh à?

18
00:01:43,512 --> 00:01:48,113
Vâng, một bức tranh... vẽ Yoshinon.

19
00:01:52,112 --> 00:01:57,113
Yoshino muốn vẽ một bức tranh cho Yoshinon...
vì lợi ích của Yoshinon sao?

20
00:01:58,112 --> 00:02:04,113
Thật lòng, tôi thấy không thiết thực lắm...
Nhưng đó là thứ mà Yoshino có thể
cố gắng hết sức để hoàn thành.

25
00:02:05,112 --> 00:02:13,113
Yoshino, một đứa trẻ gặp vấn đề trong giao tiếp,
đang cố gắng làm điều gì đó vì người bạn
Yoshinon của em ấy. Tôi không thể làm giảm 
tinh thần đó đi được.

25
00:02:14,112 --> 00:02:20,113
Anh thấy được đấy. Em có thể
truyền tải cảm xúc của mình vào bức tranh đó.

25
00:02:21,112 --> 00:02:35,113
Um... ở khu mua sắm, em có thấy...
một bức tranh... về một người nào đó 
rất quan trọng với mình.

25
00:02:36,112 --> 00:02:40,113
Ừ, anh hiểu rồi. Anh chắc chắn
Yoshinon sẽ thích nó đấy.

25
00:02:41,112 --> 00:02:44,513
Th... thật vậy sao?

25
00:02:45,712 --> 00:02:48,113
Nhất định là thế.

25
00:02:49,112 --> 00:02:51,113
Vâng!

25
00:02:52,112 --> 00:02:57,513
Vậy chúng ta phải chuẩn bị dụng cụ để vẽ.
Hiện giờ chúng ta có gì?

25
00:02:58,512 --> 00:03:04,113
Nếu tôi nhớ không nhầm, 
Kotori có một bộ chì màu khi còn...

26
00:03:05,112 --> 00:03:09,113
Chờ một chút nhé, Yoshino. Anh đi tìm đã.

26
00:03:10,112 --> 00:03:12,113
Vâng!

26
00:03:24,912 --> 00:03:27,113
Đẹp đấy chứ.

26
00:03:28,112 --> 00:03:36,113
Thật không ạ? Em sẽ cố gắng hơn nữa.

26
00:03:36,912 --> 00:03:44,513
Như mọi khi, Yoshino vẫn mang theo Yoshinon
ở tay trái. Vậy nên có hơi khó khăn khi vẽ.

26
00:03:45,112 --> 00:03:50,113
Nhưng dù vậy, em ấy không muốn lấy Yoshinon ra.

26
00:03:51,112 --> 00:03:53,113
Em có cần giúp gì không?

26
00:03:54,112 --> 00:03:58,113
Em... em làm được mà!

26
00:03:59,112 --> 00:04:01,513
Vậy thì được.

38
00:04:02,512 --> 00:04:11,113
Vâng! Đây là... việc mà em...
phải tự hoàn thành...

39
00:04:12,112 --> 00:04:15,113
Phải, cố gắng lên nhé.

40
00:04:16,113 --> 00:04:21,513
Đây có lẽ là lần đầu tôi thấy Yoshino
nghiêm túc thế này một mình.

41
00:04:22,113 --> 00:04:28,113
Em ấy trông nhiệt tình, thẳng thắn và nghiêm túc.
Rất chăm chú và quyết tâm.

42
00:04:29,113 --> 00:04:37,113
Từ ngoài nhìn vào, có thể không giống lắm,
nhưng đây là trận chiến của Yoshino
giống như khi hẹn hò với tôi.

43
00:04:38,112 --> 00:04:44,113
Vậy anh đi ra ngoài nhé.
Anh rất mong chờ bức vẽ hoàn thành đấy.

42
00:04:45,113 --> 00:04:46,513
Vâng!

39
00:04:57,912 --> 00:05:00,113
Này, Yoshi...

44
00:05:03,112 --> 00:05:10,113
Yoshino thật sự đã khổ luyện rồi.
Bức vẽ hơi khác so với cái em ấy mới vẽ.

45
00:05:11,112 --> 00:05:21,113
Em ấy đã vẽ đi vẽ lại đến khi ưng ý.
Vậy nên tôi đã để em ấy làm việc 
đến khi hoàn thành.

46
00:05:25,512 --> 00:05:30,113
Thế nào rồi, Yoshino? Em đã vẽ xong chưa?

53
00:05:31,112 --> 00:05:37,113
Không có tiếng trả lời.
Nhưng bức tranh có vẻ đã hoàn thành.

54
00:05:38,112 --> 00:05:42,113
Ồ, bức tranh rất đẹp. Còn Yoshino thì...

56
00:05:49,112 --> 00:05:55,513
Em ấy ngủ rồi. Em ấy đã làm việc
chăm chỉ nhất từ trước đến giờ.

58
00:05:56,512 --> 00:06:02,113
Em ấy trông rất mệt. Giống như mọi sinh lực
đã truyền vào bức tranh vậy.

42
00:06:05,112 --> 00:06:14,113
Đối với tôi, dù bức tranh không có gì 
quá ấn tượng nhưng nó là bức tranh tuyệt vời nhất.

43
00:06:14,512 --> 00:06:24,113
Những cảm xúc thật lòng của Yoshino đã truyền
vào bức tranh này. Cả trái tim lẫn tâm hồn.
Tôi sẽ khen ngợi khi em ấy thức dậy.

48
00:06:25,112 --> 00:06:31,113
Này Yoshino. Em mà không xem 
tác phẩm của mình thì rất đáng tiếc đấy.

50
00:06:32,112 --> 00:06:39,113
Em nên dậy nhanh đi.
Hy vọng bức tranh sẽ không mờ đi sớm.

53
00:06:40,112 --> 00:06:43,513
Cậu đã làm rất tốt đấy.

54
00:06:44,512 --> 00:06:50,113
Đúng không? Yoshino đã rất cố gắng...
Hở... Khoan đã, Yoshinon?

55
00:06:50,512 --> 00:06:56,513
Sao vậy, Shidou-kun? 
Trông anh cứ như vừa gặp zombie vậy.

56
00:06:57,512 --> 00:07:02,113
Này Yoshino! Dậy đi! Dậy nhanh lên!

60
00:07:03,112 --> 00:07:10,113
Gì vậy ạ...?

61
00:07:15,112 --> 00:07:19,513
Yoshino! Cậu khỏe chưa?

67
00:07:24,112 --> 00:07:27,513
Cậu vẫn còn buồn ngủ à?

68
00:07:28,912 --> 00:07:37,113
Shi... Shidou-san...
Đây là... mơ phải không?

69
00:07:38,112 --> 00:07:44,113
Không! Không phải mơ đâu. Đây là sự thật đấy!
Yoshino... em mau dậy đi.

75
00:07:45,112 --> 00:07:50,113
Yoshi... non!

76
00:07:52,112 --> 00:08:01,113
Tớ ở đây có làm gì có lỗi với cậu sao?

82
00:08:01,712 --> 00:08:05,113
Không phải vậy đâu!

83
00:08:06,112 --> 00:08:12,113
Tất nhiên rồi! Yoshino là thần tượng
của mọi người mà!

84
00:08:13,112 --> 00:08:22,513
Nếu đã vậy, tớ thật sự không thể 
ra đi mãi mãi được. Và tớ cũng đã có
một bức tranh tuyệt vời nữa.

85
00:08:23,112 --> 00:08:30,513
Lựa chọn của Yoshino rất chính xác.
Bức tranh đã thể hiện Yoshino quan tâm
đến Yoshinon nhiều đến thế nào.

86
00:08:31,112 --> 00:08:39,513
Phải rồi. Yoshino và Yoshinon là một.
Họ có gì đó tạo nên mối liên kết 
mạnh mẽ giữa hai người.

88
00:08:40,112 --> 00:08:47,113
Yoshinon, xin cậu... đừng đi nữa.

89
00:08:48,112 --> 00:08:52,513
Từ bây giờ, chúng ta sẽ luôn ở bên nhau nhé!

90
00:08:53,112 --> 00:08:59,113
Yoshino và Yoshinon... và Shidou-kun nữa,
cả ba chúng ta sẽ luôn bên nhau!

98
00:09:00,112 --> 00:09:02,113
Đúng vậy!

99
00:09:03,112 --> 00:09:06,113
Phải, đúng vậy! Hở... Cả anh nữa à?

100
00:09:07,113 --> 00:09:16,513
Shidou-kun, đừng nói là anh không muốn đấy nhé.
Yoshino và Yoshinon sẽ là 2 cô gái xinh đẹp
trong 2 tay anh, đúng không?

101
00:09:17,112 --> 00:09:23,113
T... tất nhiên rồi!
Anh sẽ chăm sóc cho 2 em thật tốt.

102
00:09:27,112 --> 00:09:31,113
Um... Shidou-san!

103
00:09:32,112 --> 00:09:34,113
Gì vậy?

104
00:09:35,112 --> 00:09:41,113
Bức tranh này... em có thể treo nó
trên tường được không?

105
00:09:42,112 --> 00:09:48,113
Được chứ. Đó là tác phẩm của em mà.
Hãy treo nó ở chỗ mà mọi người 
đều có thể thưởng thức nó.

106
00:09:49,112 --> 00:09:53,113
Em thấy chỗ đó được đấy!

107
00:09:54,112 --> 00:09:56,513
Ở đó à?

108
00:09:57,512 --> 00:10:05,113
Nhưng... em nghĩ... ở đây... có thể xem tốt hơn.

113
00:10:06,112 --> 00:10:12,113
Cái gì? Nhưng bức tường đó 
nhiều ánh sáng và trang trọng mà!

114
00:10:12,912 --> 00:10:15,113
Nhưng...

1
00:10:16,112 --> 00:10:22,113
Chỉ cần đảm bảo là treo chỗ nào mà
cả hai em đều ưng ý là được.

1
00:10:23,112 --> 00:10:28,113
Tất nhiên, tất nhiên rồi!
Em nghĩ chỗ đó rất tốt đấy!

1
00:10:29,112 --> 00:10:32,113
Không, chỗ này cơ!

1
00:10:33,112 --> 00:10:41,113
Nhìn em ấy giữ chính kiến của mình 
với Yoshinon như vậy... 
Yoshino đã trưởng thành rồi.



368
00:10:48,112 --> 00:10:53,113
Shidou-kun? Có chuyện gì vậy?

368
00:10:54,112 --> 00:10:56,113
Hở? Không có gì quan trọng đâu...

368
00:10:57,112 --> 00:11:04,113
Anh tệ thật đấy, Shidou-kun.
Đặt chế độ im lặng là phép lịch sự 
cơ bản đấy. Anh biết không?

68
00:11:05,112 --> 00:11:08,113
Anh xin lỗi. Um... Kotori à?

368
00:11:08,512 --> 00:11:16,113
Một cái gì đó, tôi cảm thấy kỳ lạ. 
Một cảm giác bồn chồn, khó chịu...?

368
00:11:19,112 --> 00:11:32,113
Trong tương lai, cậu sẽ yêu quý Yoshino-chan.
Bất kể chuyện gì xảy ra... cậu sẽ luôn như vậy.

368
00:11:33,912 --> 00:11:36,513
Đột nhiên, những lời của Rinne xuất hiện trong tâm trí tôi. 

368
00:11:36,912 --> 00:11:43,113
Bây giờ, trước mắt tôi, 
cuối cùng Yoshino và Yoshinon cũng đã tái hợp.
Đây là khoảnh khắc hạnh phúc 
không thể đánh đổi bất cứ điều gì trên đời.

368
00:11:44,112 --> 00:11:53,113
Sẽ ổn thôi nếu em ấy gọi lại sau...
Có lẽ đó là việc quan trọng cần báo.
Tôi cảm thấy mình sẽ mất một cái gì đó quý giá
nếu tôi nhận cuộc gọi.

368
00:11:54,112 --> 00:11:58,113
Thật khó chịu. Tại sao tôi lại lo lắng đến vậy?

368
00:12:01,113 --> 00:12:04,113
Shidou-san?

368
00:12:05,112 --> 00:12:07,113
Ừ...

368
00:12:08,112 --> 00:12:14,113
Vâng, tất nhiên tôi sẽ không trốn tránh.
Tôi, cuộc gọi này...

368
00:12:15,112 --> 00:12:17,113
1) Nghe điện thoại
2) Không nghe điện thoại.

368
00:12:18,112 --> 00:12:20,113
Xin lỗi em, Kotori.
Bây giờ... Anh không thể nghe điện thoại.

368
00:12:21,112 --> 00:12:29,113
Bây giờ, tôi muốn trân trọng khoảnh khắc
quý giá này với Yoshino và Yoshinon.
Tôi chắc chắn dù chỉ vài ngày xa cách
cũng là rất khó khăn.

368
00:12:30,112 --> 00:12:35,113
Nào Shidou-kun... Anh đã sẵn sàng chưa?

368
00:12:36,112 --> 00:12:39,113
Được rồi. Xung quanh chỗ này được không?

368
00:12:40,112 --> 00:12:43,113
Sang phải một tí nhé.

368
00:12:44,112 --> 00:12:49,113
Em nghĩ... sang trái một chút!

368
00:12:50,112 --> 00:12:54,113
Chỗ đó sẽ không lệch chứ?

368
00:12:55,112 --> 00:12:59,313
Không... Không có đâu!

368
00:13:00,112 --> 00:13:05,113
Thật là... 2 em thật bướng bỉnh.
2 em không nhường nhau được à?

368
00:13:06,112 --> 00:13:10,113
Không, chuyện này không nhường nhau được.

368
00:13:11,112 --> 00:13:16,113
Vâng! Em là người vẽ ra bức tranh đó mà!

368
00:13:17,112 --> 00:13:21,113
Hai em giống nhau như hai hạt đậu vậy.
(thành ngữ like two peas in a pod)

368
00:13:23,112 --> 00:13:27,113
Như vậy, Yoshino và tôi đã tìm lại được
người bạn của mình.

368
00:13:28,112 --> 00:13:34,113
Tôi tin sau này, Yoshino, Yoshinon và tôi
đã bước vào một loại quan hệ thân thiết.

368
00:13:34,912 --> 00:13:39,513
So với trước đây, 
Yoshino phụ thuộc vào Yoshinon đã ít hơn nhiều.

368
00:13:40,112 --> 00:13:50,113
Mặc dù là bạn bè, Yoshino thường dựa dẫm vào 
Yoshinon quá nhiều. Không tự làm được gì một mình.
Không nói chuyện được với người khác.
Những chuyện đó không còn nữa.

368
00:13:50,712 --> 00:13:59,513
Theo cách riêng của em ấy, 
Yoshino đã tự lập hơn.
Yoshinon đã hỗ trợ tốt nên 
Yoshinon không lặp lại thói quen cũ.

368
00:14:00,512 --> 00:14:06,513
Từ bây giờ, tôi mong rằng mình sẽ được 
dõi theo từng bước chân của Yoshino
và luôn bên cạnh em ấy.

368
00:14:07,112 --> 00:14:14,113
Mãi mãi như vậy. Dù thời gian có trôi bao lâu,
tình bạn của chúng tôi sẽ còn mãi.






368
00:16:28,112 --> 00:16:30,913
Em xong chưa?

368
00:16:31,112 --> 00:16:36,113
Chờ... chờ em một chút...

368
00:16:36,912 --> 00:16:39,113
Tuân lệnh.

368
00:16:44,512 --> 00:16:47,513
Um... vẫn chưa xong à?

368
00:16:48,312 --> 00:16:58,113
Chưa... chưa xong đâu...
Nhưng Shidou-san... giúp em với...

368
00:16:58,912 --> 00:17:01,113
Chuyện gì vậy?

368
00:17:05,112 --> 00:17:08,113
Yoshino, anh vào nhé?

368
00:17:13,712 --> 00:17:18,113
Em không sao chứ? Khoan... này!
Thế này là sao?

368
00:17:18,512 --> 00:17:26,113
Em xin lỗi... em không thể...
tự mặc nó được.

368
00:17:27,112 --> 00:17:34,513
Shidou-kun, may mà có anh ở đây!
Thật là... lúc nào cũng phải có anh canh chừng.

368
00:17:35,112 --> 00:17:37,113
Nào nào...

368
00:17:38,112 --> 00:17:42,113
Yoshinon đang muốn nhờ làm gì vậy?

368
00:17:43,112 --> 00:17:49,113
Anh có thể... giúp em mặc cái đầm này không?

368
00:17:50,112 --> 00:17:53,113
Hở... nhưng...

368
00:17:53,512 --> 00:17:56,513
Không được sao...?

368
00:17:57,512 --> 00:18:01,113
Aaa... được rồi...

368
00:18:01,512 --> 00:18:05,513
Đây là trường hợp khẩn cấp.
Không còn cách nào khác đâu.

368
00:18:06,112 --> 00:18:10,113
Anh làm được! Nào, đưa tay đây.

368
00:18:13,112 --> 00:18:18,513
Yoshino có vẻ khá khó khăn khi 
tự mặc trang phục này.

368
00:18:19,512 --> 00:18:28,513
Nhưng nếu hai em muốn tôi thấy họ dễ thương thế nào,
tôi không thể từ chối được. 
Nó giống như tôi không muốn gặp họ vậy.

368
00:18:31,912 --> 00:18:37,513
Em thấy Yoshino sẽ hình thành ngay bây giờ.

368
00:18:38,112 --> 00:18:46,113
Nhưng... em đã ngừng... 
dựa dẫm vào Yoshinon... phải không?

368
00:18:46,912 --> 00:18:55,513
Cậu nói đúng, nhưng... với bộ đồ này,
cậu đã trở nên khá quyến rũ với Shidou-kun

368
00:18:56,512 --> 00:19:01,113
Nếu là Shidou-san... thì không sao!

368
00:19:02,112 --> 00:19:04,113
Thật vậy sao?

368
00:19:04,512 --> 00:19:06,113
Vâng!

368
00:19:07,112 --> 00:19:13,113
Cô bé ngoan, cậu chắc chắn chỉ muốn ngất đi 
khi nói đến Shidou-kun, Yoshino

368
00:19:18,112 --> 00:19:23,113
Dù sao thì việc đó cũng không tệ lắm.

368
00:19:24,112 --> 00:19:28,113
Em chỉ thích gì nói đấy thôi.

368
00:19:28,712 --> 00:19:31,513
Đây, xong hết rồi đấy.

368
00:19:34,112 --> 00:19:39,113
Anh thấy sao ạ?

368
00:19:39,712 --> 00:19:43,113
Hở? À phải rồi! Nó rất hợp với em đấy, Yoshino.

368
00:19:47,112 --> 00:19:56,913
Shidou-kun. Cứ xấu hổ đi!
Dễ thương lắm. Cả hai dễ thương lắm!

368
00:19:57,112 --> 00:20:00,113
Yoshinon, đừng nói tào lao nữa!

368
00:20:01,113 --> 00:20:03,113
Shidou-san.

368
00:20:06,913 --> 00:20:11,513
Em... hạnh phúc lắm...

368
00:20:12,512 --> 00:20:14,513
Ừ, phải rồi...

368
00:20:15,112 --> 00:20:25,113
Yoshino ở đây, và cả Yoshinon cũng vậy.
Giờ chúng tôi có thể luôn vui đùa mỗi khi
ở bên nhau. Thời gian sẽ trôi, 
nhưng tôi sẽ không đánh mất họ.

368
00:20:25,512 --> 00:20:29,113
Anh cũng vậy.

368
00:20:29,512 --> 00:20:31,113
Vâng!

368
00:20:42,912 --> 00:20:45,113
1) Nghe điện thoại
2) Không nghe điện thoại.

368
00:20:46,112 --> 00:20:49,113
Yoshino, Yoshinon. 
Anh xin lỗi, đợi anh một chút.

368
00:20:49,512 --> 00:20:51,113
V... vâng...

368
00:20:52,112 --> 00:20:54,113
Anh nhanh lên đấy nhé.

368
00:20:54,512 --> 00:20:56,113
Được rồi...

368
00:20:56,912 --> 00:20:59,513
Này, anh đang làm gì vậy?

368
00:21:00,112 --> 00:21:01,113
Um...

368
00:21:01,512 --> 00:21:05,113
Đây là tình huống khẩn cấp.
Bọn em đã tìm ra chỗ của kẻ thù rồi.

368
00:21:05,312 --> 00:21:06,913
Kẻ thù sao?

368
00:21:07,112 --> 00:21:12,113
Em nghĩ tháp Tengu Mới chính là 
nguồn sức mạnh của kết giới.

368
00:21:12,312 --> 00:21:15,513
Tháp Tengu Mới sao? 
Khoan đã! Em đang nói gì vậy?

368
00:21:15,912 --> 00:21:21,513
Anh vẫn chưa già phải không?
Anh hỏi ngớ ngẩn gì vậy?

368
00:21:21,912 --> 00:21:22,913
Hở?

368
00:21:29,512 --> 00:21:31,513
Này, anh ổn chứ?

368
00:21:31,712 --> 00:21:35,913
À... anh xin lỗi. 
Thực sự chuyện gì đang xảy ra?
Những gì em nói là thật sao?

368
00:21:36,112 --> 00:21:42,113
Vâng, đó là sự thật.
Bây giờ Yoshino có ở đó không?

368
00:21:42,302 --> 00:21:43,513
Có đây...

368
00:21:43,712 --> 00:21:48,113
Anh phải đến tháp Tengu Mới với em ấy.

368
00:21:48,512 --> 00:21:49,513
Cái gì cơ?

368
00:21:49,712 --> 00:21:52,513
Yoshino cuối cùng cũng gặp lại Yoshinon.
Yêu cầu em ấy làm việc này đột ngột như vậy...

368
00:21:52,912 --> 00:22:02,513
Em hiểu anh đang nghĩ gì. Nhưng chỉ có
một Tinh Linh sức mạnh đang ổn định là Yoshino.
Anh hiểu không?

368
00:22:04,712 --> 00:22:06,113
Anh hiểu, nhưng...

368
00:22:06,312 --> 00:22:08,513
Shidou-san...

368
00:22:08,712 --> 00:22:09,713
Yoshino...

368
00:22:09,912 --> 00:22:15,913
Có chuyện gì... 
mà chỉ có em làm được... phải không ạ?

368
00:22:16,112 --> 00:22:17,913
Phải, nhưng...

368
00:22:18,112 --> 00:22:20,513
Vậy em sẽ làm!

368
00:22:20,912 --> 00:22:23,113
Nhưng em chỉ mới gặp lại Yoshinon mà!

368
00:22:23,312 --> 00:22:27,513
Shidou-san, anh đã dạy em...

368
00:22:27,912 --> 00:22:33,113
Khi có ai đó gặp rắc rồi, em cần hỗ trợ họ.

368
00:22:33,312 --> 00:22:34,713
Anh hiểu rồi. Em nói đúng.

368
00:22:34,912 --> 00:22:39,113
Được rồi, cũng đã lâu 
chúng ta không chiến đấu rồi. 



368
00:22:45,112 --> 00:22:47,513
Tháp Tengu... đây là nơi ẩn náu của kẻ thù.

368
00:22:47,912 --> 00:22:49,113
Yoshino, em có chắc việc này ổn không?

368
00:22:49,512 --> 00:22:55,113
Em ổn mà... Em ở bên cạnh anh mà, Shidou-san...

368
00:22:55,512 --> 00:22:57,713
Cảm ơn em. Đi nào, Yoshino!

368
00:22:58,113 --> 00:23:00,513
Vâng... Shidou-san!

368
00:23:10,512 --> 00:23:12,113
Cô là... người trong giấc mơ... của tôi?

368
00:23:12,312 --> 00:23:15,113
Đây là... ai?

368
00:23:16,112 --> 00:23:19,113
Không ai chịu "kết thúc" cả...

368
00:23:19,913 --> 00:23:20,513
Không ai... "kết thúc" sao?

368
00:23:20,913 --> 00:23:28,113
Tôi muốn các cậu ở trong 
"giấc mơ hạnh phúc" mà tôi tạo ra.

368
00:23:29,112 --> 00:23:30,513
Cô nói gì...?

368
00:23:31,112 --> 00:23:36,113
Người này là... Tinh Linh sao?

368
00:23:37,112 --> 00:23:39,513
Anh không biết, nhưng đây không phải 
lần đầu anh gặp cô ấy.

368
00:23:40,112 --> 00:23:52,113
Tôi là người cai trị thế giới này. 
Người cai quản Eden.
Cậu có thể gọi tôi là Ruler.

368
00:23:52,712 --> 00:23:59,113
Nhưng... cậu biết cũng chẳng để làm gì...
Cậu sẽ quên thôi.

368
00:24:02,112 --> 00:24:05,113
Shidou-san...! Nguy hiểm đấy!

368
00:24:13,712 --> 00:24:23,513
Tôi sẽ dạy cho cô biết cô ngu ngốc
và mong manh dễ vỡ đến thế nào.

368
00:24:33,112 --> 00:24:40,513
Sao lại...? Di chuyển đi! 
Làm ơn di chuyển đi! Zadkiel!

368
00:24:41,112 --> 00:24:45,113
Không được rồi! Tớ không cử động được!

368
00:24:47,112 --> 00:24:48,513
Yoshino... Yoshinon!

368
00:24:48,912 --> 00:24:51,113
Shidou-san!

368
00:24:53,112 --> 00:24:55,113
Tạm biệt.

368
00:25:05,912 --> 00:25:07,513
YOSHINOOOOO!
KHỐN KIẾP... VẬY LÀ SAO?

368
00:25:07,912 --> 00:25:16,113
Chúc ngủ ngon, Itsuka Shidou. 
Lần sau, chắc chắn cậu sẽ có "giấc mơ hạnh phúc".

368
00:25:25,112 --> 00:25:32,113
Cậu đã hứa với tôi... cậu sẽ yêu quý Yoshino...

368
00:25:41,712 --> 00:25:42,513
Đây là đâu?

368
00:25:42,712 --> 00:25:44,113
Phòng của mình à?

368
00:25:44,312 --> 00:25:45,713
Tất cả chỉ là... một giấc mơ, đúng không?

368
00:25:45,912 --> 00:25:47,113
Một giấc mơ như thật...

368
00:25:47,312 --> 00:25:49,113
Ơ? Giấc mơ đó là gì?

368
00:25:49,312 --> 00:25:52,513
Đó chỉ là một giấc mơ...
Mình nghĩ chẳng có gì đâu.
Hãy thôi suy nghĩ đi...

368
00:25:52,912 --> 00:25:55,113
Vẫn còn sớm để thức dậy. 
Mình muốn ngủ thêm chút nữa...

368
00:26:40,112 --> 00:26:41,113


368
00:26:40,112 --> 00:26:41,113
