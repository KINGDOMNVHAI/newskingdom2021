﻿0
00:00:03,512 --> 00:00:05,513
Shidou...

1
00:00:09,512 --> 00:00:13,113
Shidou, dậy đi.

2
00:00:16,112 --> 00:00:20,113
Kotori? Có chuyện gì vậy? Vẫn còn sớm mà...

3
00:00:21,112 --> 00:00:26,513
Em muốn báo cáo. Nói chuyện với em
một chút trước khi ăn sáng đã.

4
00:00:27,112 --> 00:00:30,113
Được rồi. Chờ anh chút.





5
00:00:36,112 --> 00:00:42,113
Vậy em muốn báo cáo chuyện gì? 
Có phát hiện mới hay là... 
vụ tấn công tối qua?

6
00:00:43,112 --> 00:00:45,113
Là cả hai.

7
00:00:46,112 --> 00:00:48,113
Cả hai à?

8
00:00:49,112 --> 00:00:59,113
Khi đến gần di tích, một thiên sứ
xuất hiện giống như muốn can thiệp. Phải vậy không?

9
00:01:00,112 --> 00:01:02,113
Đúng vậy.

10
00:01:03,112 --> 00:01:07,513
Sau đó, nó biến mất không dấu vết.

11
00:01:08,512 --> 00:01:13,513
Chắc chắn rồi... Vậy nó là thứ gì?
Nó là... Tinh Linh sao?

12
00:01:14,512 --> 00:01:24,113
Vẫn chưa có gì đảm bảo.
Nhưng theo những gì chứng kiến,
nó rất giống với Tinh Linh.

13
00:01:25,112 --> 00:01:28,113
Ý em là sao?

14
00:01:29,112 --> 00:01:44,113
Về sức mạnh, nó gần giống với Tinh Linh.
Dù vậy, nó chỉ là thực thể được tạo ra
từ Tinh Linh có ý chí tự do như Tohka.

15
00:01:45,112 --> 00:01:47,113
Hở?

16
00:01:48,112 --> 00:01:59,113
Giống như bộ não của anh như côn trùng,
anh vẫn có thể hành động tự do 
như một con người.

25
00:02:00,112 --> 00:02:04,113
So sánh kiểu gì vậy?

25
00:02:05,112 --> 00:02:16,113
Mà thôi. Tóm lại, nó chắc chắn
đã được tạo ra bởi sức mạnh Tinh Linh
dựa vào những gì anh mô tả.

25
00:02:17,112 --> 00:02:24,113
Nó giống như một "Golem" 
hoặc "Người bảo vệ" trong các trò chơi.

25
00:02:25,112 --> 00:02:28,113
Nó giống như một thực thể vậy.

25
00:02:29,112 --> 00:02:35,113
Shidou, anh có biết nó là gì
sau khi gặp tình huống như vậy không?

25
00:02:36,112 --> 00:02:38,113
Xin lỗi...

25
00:02:39,112 --> 00:02:47,113
Nếu anh nói đúng, có lẽ... nó là... 
một thứ có nhiệm vụ phòng thủ.

25
00:02:48,112 --> 00:02:50,113
Phòng thủ sao?

25
00:02:51,112 --> 00:02:57,113
Đó là tượng đài, thứ phát sáng vào thời điểm đó.

25
00:02:58,112 --> 00:03:03,113
Vậy à... Nhưng thứ đó tạo ra cái gì?

25
00:03:04,112 --> 00:03:13,113
Phải, em chỉ biết đến thế.
Nhưng ít nhất em chắc chắn 
nó có liên quan đến kết giới.

26
00:03:14,112 --> 00:03:22,113
Em đã kiểm tra sóng Tinh Linh
quanh tượng đài vào thời điểm anh bị tấn công.

26
00:03:23,112 --> 00:03:26,113
Vậy em đã phát hiện ra điều gì chưa?

26
00:03:27,112 --> 00:03:36,113
Dạng sóng đang yếu đi ở tháp Tengu...
Nó trùng với kết giới bao quanh thành phố Tengu.

26
00:03:39,112 --> 00:03:48,513
Nó có kích thước khác nhau,
nhưng em đã kiểm tra.
Kết quả là nó giống hệt nhau.

26
00:03:49,512 --> 00:03:52,113
2 sóng đó... giống nhau sao?

26
00:03:53,112 --> 00:04:00,113
Sự hiện diện của cả kết giới 
và người bảo vệ có liên quan đến câu hỏi đó.
Em chắc chắn chúng liên kết đến nhau.

26
00:04:00,512 --> 00:04:10,113
Thật dễ dàng để nói có khả năng 
cả di tích và người bảo vệ đều được tạo ra.

26
00:04:11,112 --> 00:04:20,113
Câu trả lời cho việc này là... 
các di tích phải là nền tảng để giữ kết giới.

26
00:04:21,112 --> 00:04:27,513
Trước đây em đã đề cập về di tích?
Vậy nếu anh có thể...

40
00:04:28,513 --> 00:04:37,113
Có lẽ vậy. Nhưng em e rằng nó không đơn giản.

41
00:04:38,113 --> 00:04:42,113
Ý của em là... không chỉ có một...?

42
00:04:43,113 --> 00:04:54,113
Đúng vậy. Ở nơi nào đó khắp Tengu
cũng có những thứ tương tự như thế.

43
00:04:55,112 --> 00:05:01,113
Vậy là giờ chúng ta phải tìm những cái khác.
Đây mới chỉ là mở đầu.

39
00:05:02,112 --> 00:05:13,113
Phải. Vẫn còn một chặng đường dài để đi.
Nhưng điều này cũng tùy thuộc vào anh đấy.
Anh hãy tiếp tục hẹn hò.

44
00:05:14,112 --> 00:05:17,113
Ừ, anh hiểu...

45
00:05:18,112 --> 00:05:28,113
Tất nhiên, nếu anh biết điều gì,
hãy báo cáo ngay.
Nhưng đừng làm thế nữa, được không?

46
00:05:28,712 --> 00:05:30,513
Ừ, anh biết...

47
00:05:31,512 --> 00:05:38,113
Giờ chỉ còn ý định của kẻ thù.
Em ước gì mình biết được...

48
00:05:39,112 --> 00:05:42,113
Kẻ thù à? Là người bảo vệ sao?

49
00:05:43,112 --> 00:05:59,113
Em đang nói về kẻ chủ mưu.
Lý do khiến chúng ta bị nhốt trong kết giới.
Không, chuyện này chưa kết thúc! 
Anh phải làm những gì cần làm!

41
00:06:00,112 --> 00:06:06,113
Uh... Anh xin lỗi. Anh sẽ hợp tác 
càng nhiều càng tốt. Anh muốn làm sớm. 
Mọi người đang vật lộn với chuyện này...

42
00:06:07,112 --> 00:06:15,113
Một lần nữa, anh giờ không còn 
khả năng hồi phục. 
Tuyệt đối không được làm thế nữa.

43
00:06:16,112 --> 00:06:18,113
Ừ, anh hứa.

44
00:06:19,112 --> 00:06:26,313
Thực sự... Em lo lắng cho anh
đến không ngủ được đấy...

45
00:06:26,712 --> 00:06:28,513
Hở?

46
00:06:29,112 --> 00:06:32,513
Không có gì đâu!

47
00:06:33,713 --> 00:06:35,513
Ừ... ừ...





48
00:06:41,112 --> 00:06:44,113
Bữa sáng xong chưa anh?

49
00:06:45,112 --> 00:06:48,113
Xin lỗi đã để em đợi. Anh xong rồi đây.

50
00:06:49,112 --> 00:06:56,113
Shidou...san. Chào buổi sáng.

51
00:07:01,112 --> 00:07:08,113
Ồ, Yoshino và Yoshinon, chào buổi sáng.
Vừa đúng lúc bữa sáng xong rồi đấy.

52
00:07:09,112 --> 00:07:14,113
Vâng... Itadakimasu...

64
00:07:15,112 --> 00:07:18,113
Tohka đâu rồi?

65
00:07:20,112 --> 00:07:27,113
Tohka à? Tohka đi học rồi. 
Cô ấy không cần ăn sáng...

66
00:07:27,912 --> 00:07:33,113
Tohka không cần ăn sáng sao?

67
00:07:34,112 --> 00:07:41,113
Trông cô ấy cũng có vẻ khó chịu...
nhưng cũng không phải việc lớn
chỉ vì cô ấy không ăn cơm đâu.

69
00:07:45,112 --> 00:07:47,113
Gì vậy?

70
00:07:48,112 --> 00:07:54,113
Không có gì đâu. Anh sẽ biết sớm thôi.

71
00:07:54,912 --> 00:07:57,113
Vậy sao?




72
00:08:03,112 --> 00:08:08,113
Hôm nay là lần đầu mình đi học một mình.

83
00:08:09,112 --> 00:08:15,113
Mình tự hỏi tại sao. Dù trông bình thường,
mình lại có cảm giác cô đơn.

84
00:08:16,112 --> 00:08:24,113
Cảm giác như mình thiếu đi điều gì đó.
Đó là gì vậy? Mình không chắc lắm,
nhưng giống như là một lỗ hổng trong tim mình.

85
00:08:25,112 --> 00:08:30,113
Có khi nào là do Tohka không có ở đây?
Không, không thể như vậy...

88
00:08:35,912 --> 00:08:41,113
Tôi đã đi học như thường ngày, 
nhưng hôm nay tâm trí tôi trống rỗng...

89
00:08:42,112 --> 00:08:48,513
Nếu tôi cứ tiếp tục tâm trạng này,
tôi không thể hẹn hò với ai cả. Tôi phải thay đổi.

91
00:08:59,112 --> 00:09:02,113
Oh... Tớ xin lỗi, cậu không sao chứ?

92
00:09:03,112 --> 00:09:05,113
Tớ không sao.

93
00:09:06,112 --> 00:09:10,113
Không...! Có sao đấy!
Hoàn toàn không ổn chút nào!

101
00:09:14,112 --> 00:09:17,113
Thôi nào! Đứng dậy nhanh nào!

102
00:09:20,112 --> 00:09:22,113
Cảm ơn cậu.

104
00:09:23,112 --> 00:09:27,113
Tớ xin lỗi. Tớ đang suy nghĩ một chút...

105
00:09:28,112 --> 00:09:31,113
Cậu nghĩ gì?

106
00:09:32,112 --> 00:09:35,113
Đừng lo, không có gì quan trọng đâu!

107
00:09:36,112 --> 00:09:40,113
Vậy à? Được rồi...

108
00:09:41,112 --> 00:09:44,113
Ừ. Còn mấy phút nữa là đến giờ nhỉ?

109
00:09:45,112 --> 00:09:48,113
5 phút 17 giây nữa...

114
00:09:49,112 --> 00:09:54,113
À đúng vậy. Vậy chúng ta vào lớp đi.
Cậu có vào lớp không?

1
00:10:00,112 --> 00:10:08,113
Phù... Mọi thứ bất ngờ khiến tôi lơ đãng
trong thoáng chốc. Tôi ngừng suy nghĩ 
và đi thẳng vào lớp.

1
00:10:14,112 --> 00:10:20,113
Dù tiết này ngay trước bữa trưa
tôi vẫn thấy buồn ngủ.

1
00:10:22,112 --> 00:10:26,113
Hở? Có điện thoại sao? Ai gọi sớm thế nhỉ?

1
00:10:27,112 --> 00:10:32,113
Ở nhà sao? Chỉ một mình Yoshino 
ở nhà vào lúc này thôi.

1
00:10:33,112 --> 00:10:38,113
Không biết là chuyện gì, 
nhưng mình không thể nghe điện thoại trong lớp được.

1
00:10:39,112 --> 00:10:42,513
Họ cúp máy rồi... Tôi tự hỏi đây là ai?

1
00:10:43,512 --> 00:10:52,513
Nếu là Ratatoskr, họ đã gọi qua tai nghe rồi.
Có thể là Yoshino gọi bằng điện thoại
của em ấy chăng?

368
00:10:53,512 --> 00:10:58,113
Mình có thể gọi em ấy vào bữa trưa...

368
00:11:06,912 --> 00:11:14,113
Giờ có vẻ hơi trễ, nhưng chẳng phải
bài kiểm tra này quá khó sao?
Đầu tôi muốn nổ tung rồi.

68
00:11:14,512 --> 00:11:20,113
Này! Itsuka-kun, bạn trai của tôi!
Đi ăn trưa đi!

368
00:11:21,112 --> 00:11:28,113
Này, hạ cái giọng xuống! Ai là bạn trai của ông?
Đây không phải là tuần trăng mật đâu!

368
00:11:29,112 --> 00:11:37,513
Sao vậy Itsuka? Ông ngại à?
Đó cũng là điểm tốt của ông đấy.

368
00:11:38,112 --> 00:11:43,513
Này, nếu ông tiếp tục nói những thứ 
kỳ lạ như vậy, tôi đập ông đấy!

368
00:11:44,512 --> 00:11:54,113
Đừng lo, Itsuka. 
Tôi không thể ngừng xấu hổ được.

68
00:11:55,112 --> 00:12:02,513
Làm ơn! Tôi xin ông đấy! 
Ông có thể ý thức hành động của mình
hơn một chút được không?

368
00:12:03,113 --> 00:12:10,513
Itsuka, đây là lúc đàn ông hành động,
dù anh ấy có nghĩ khác đi nữa.

368
00:12:12,112 --> 00:12:17,113
Tôi có thể cảm thấy cuộc hội thoại này
ngu ngốc thế nào, nhưng...

368
00:12:18,112 --> 00:12:24,513
Không, không thể vậy được.
Câu chuyện của tôi luôn luôn quan trọng...

368
00:12:25,512 --> 00:12:31,113
Nhìn kìa, có cô gái nhìn về phía này.
Có vẻ cô ấy nhìn ông đấy.

368
00:12:32,112 --> 00:12:34,513
Hả? Thật sao?

368
00:12:35,512 --> 00:12:42,513
Thật đấy. Đi ngay đi, Tonomachi!
Nhanh lên kẻo cô ấy đi mất đấy!
Đến lúc ông thể hiện rồi!

368
00:12:43,312 --> 00:12:49,513
Vậy sao? Chờ anh với, vợ tương lai ơi!

368
00:12:50,512 --> 00:12:55,113
Đi đi. 
Phù... Đuổi tên ngốc đó đi dễ thật.

368
00:12:56,112 --> 00:13:03,113
Mệt mỏi thật... giờ là lúc để 
nói chuyện và ăn trưa với Tohka... 
Đâu rồi?

35
00:13:04,112 --> 00:13:09,113
Tohka đi đâu rồi? Cô ấy đi trong lúc 
tôi nói chuyện với Tonomachi...

35
00:13:10,112 --> 00:13:13,113
Phù... bữa trưa nay mình làm gì đây?

35
00:13:13,512 --> 00:13:19,113
À phải rồi... Mình sẽ gọi điện trước khi ăn.
Nếu Yoshino vừa gọi tới, chắc là có chuyện gì đó.










368
00:13:24,912 --> 00:13:26,113
Reine-san?

368
00:13:27,112 --> 00:13:32,113
Tôi xin lỗi vì gọi lúc cậu đang bận.
Đây là việc hệ trọng.

368
00:13:33,112 --> 00:13:38,113
Vâng, khoan đã. Em sẽ ra ngoài hành lang.

368
00:13:42,712 --> 00:13:46,113
Alo? Có chuyện gì thế ạ?

368
00:13:46,512 --> 00:13:54,113
Tôi vừa nhận tin từ Fraxinus.
Yoshino đã biến mất ở nhà rồi.

368
00:13:55,112 --> 00:13:59,513
Biến mất sao? Có lẽ em ấy đã đi đâu đó?

368
00:14:00,112 --> 00:14:04,513
Không may là tình hình rất bất thường.

368
00:14:08,112 --> 00:14:15,113
Năng lượng Tinh Linh của Yoshino
trở nên khá bất ổn.

368
00:14:16,112 --> 00:14:18,113
Sao cơ? Tại sao chứ?

368
00:14:19,112 --> 00:14:30,113
Tôi không rõ lý do. Nhưng chúng ta không thể
cứ để thế này. Shin, tôi xin lỗi nhưng
cậu có thể tìm cô bé ngay không?

368
00:14:30,512 --> 00:14:36,113
Em hiểu rồi! Em sẽ cố gắng!
Em chắc chắn sẽ tìm thấy Yoshino!

368
00:14:36,512 --> 00:14:39,113
Tôi trông cậy vào cậu.

47
00:14:48,713 --> 00:14:55,513
Cuối cùng cũng hết giờ. Không hiểu sao
tôi thấy lâu hơn bình thường.

48
00:14:56,112 --> 00:15:03,113
Mặt khác, tôi tự hỏi tại sao lại mệt mỏi vậy?
Tôi lại bệnh sao? Không, không phải thế...

49
00:15:04,113 --> 00:15:09,513
Không thể để thế này được. Tôi phải hẹn hò.




368
00:15:16,112 --> 00:15:18,113
Đi đến Tháp Tengu?
1) Đồng ý
2) Không đông ý

368
00:15:21,112 --> 00:15:23,513
Ồ... Itsuka-kun.

368
00:15:24,512 --> 00:15:27,113
Ồ, sensei, chào cô.

368
00:15:28,112 --> 00:15:31,113
Em đang đi về nhà đấy à?

368
00:15:32,112 --> 00:15:35,513
Vâng, em có vài việc cần làm...

368
00:15:36,512 --> 00:15:40,113
Em đang ghé qua nơi nào đó à?

368
00:15:41,112 --> 00:15:44,113
À... vâng... có lẽ vậy.

368
00:15:45,112 --> 00:15:51,113
Nhân tiện, em đã đến Tháp Tengu mới bao giờ chưa?

368
00:15:52,112 --> 00:15:56,513
Vâng, em chỉ đứng ngoài ngắm nhìn nó thôi.

368
00:15:57,512 --> 00:16:03,113
Phải. Đó là biểu tượng mới của thành phố đấy.

368
00:16:04,112 --> 00:16:06,113
Đúng vậy.

368
00:16:10,112 --> 00:16:14,113
Có chuyện gì ạ?

368
00:16:15,112 --> 00:16:21,113
Cô cảm giác cô đã nói chuyện này 
với em trước đây rồi.

368
00:16:21,712 --> 00:16:27,113
Về tòa tháp sao ạ? Chúng ta đã nói 
về nó rất nhiều, nhưng...

368
00:16:28,112 --> 00:16:36,513
Ý cô không phải vậy. Cô cảm giác
cuộc hội thoại y như thế này với em,
ngay tại lúc này, tại đây đã từng xảy ra rồi.

368
00:16:37,512 --> 00:16:40,113
Ý cô thế là sao?

368
00:16:41,112 --> 00:16:46,113
Um... Déjà... 

368
00:16:47,112 --> 00:16:49,113
Déjà vu? (Cảm giác quen quen)

368
00:16:50,112 --> 00:16:58,113
Đúng rồi, đúng rồi... Kiểu như vậy đấy.
Cô cảm giác việc này cứ lặp đi lặp lại...

368
00:16:59,512 --> 00:17:02,113
Lặp đi lặp lại sao?

368
00:17:03,112 --> 00:17:09,513
Ồ... Đừng lo về chuyện đó.
Có lẽ cô tưởng tượng thôi.

368
00:17:10,512 --> 00:17:16,113
Vậy nhé Itsuka-kun, về nhà cẩn thận nhé.

368
00:17:16,512 --> 00:17:18,513
V... vâng ạ.



368
00:17:26,112 --> 00:17:27,113
Đi đến Cổng Trường?
1) Đồng ý
2) Không đông ý

368
00:17:31,112 --> 00:17:35,113
Mình phải nhanh chóng tìm Yoshino.

368
00:17:36,112 --> 00:17:40,113
Mưa sao? Vài giây trước không có mưa mà.

368
00:17:40,512 --> 00:17:44,113
Khoan đã. Mưa ngay tại đây... không lẽ...?

368
00:17:46,512 --> 00:17:49,113
Yoshino!

368
00:17:52,712 --> 00:17:55,513
Yoshino! Này, có chuyện gì vậy?

368
00:17:56,512 --> 00:17:59,113
Shidou-san?

368
00:18:01,112 --> 00:18:05,113
Đúng rồi. Anh đây.
Em không sao chứ? Đã có chuyện gì vậy?

368
00:18:09,112 --> 00:18:14,113
Yoshino! Này, Yoshino!
Bình tĩnh lại đi, Yoshino! Có chuyện gì vậy?

368
00:18:19,112 --> 00:18:25,113
Yoshinon...

368
00:18:30,112 --> 00:18:38,113
Cậu ấy... không còn... cử động nữa...

368
00:18:39,112 --> 00:18:42,113
Sao cơ? Thật vậy sao?

368
00:18:47,512 --> 00:18:51,513
Yoshinon không cử động nữa sao?
Vậy là sao?

368
00:18:52,112 --> 00:19:03,113
Kể cả khi... em nói chuyện với cậu ấy...
cậu ấy vẫn không... trả lời...
Và cậu ấy... không nói chuyện với em nữa...

368
00:19:04,112 --> 00:19:10,113
Đây không phải trò đùa phải không?
Có thể cậu ấy đột nhiên cử động 
và nói "Got ya" như mọi khi...

368
00:19:11,112 --> 00:19:20,113
Em không biết...
Em không biết... phải làm gì...

368
00:19:21,112 --> 00:19:25,113
Vậy là em đã gọi từ trưa vì việc này sao?

368
00:19:26,112 --> 00:19:38,113
Vâng... nhưng... không gọi được!
Em đã sợ... mình sẽ... phải một mình.

368
00:19:40,112 --> 00:19:42,113
Lạnh quá...!

368
00:19:45,512 --> 00:19:52,513
Yoshino, bình tĩnh! Hãy bình tĩnh lại!
Việc này... em không thể làm gì được đâu!

368
00:19:53,512 --> 00:19:59,113
Nhưng... Yoshinon là...!

368
00:20:00,113 --> 00:20:07,113
Em không muốn... Yoshinon... ra đi đâu!

368
00:20:08,113 --> 00:20:14,113
Chẳng có tác dụng gì cả...
Yoshinon không cử động, Yoshino mất hết hy vọng.
Nhưng, tôi...

368
00:20:15,113 --> 00:20:18,113
Anh... anh sẽ không để em một mình đâu, Yoshino!

368
00:20:24,112 --> 00:20:31,113
Anh sẽ ở bên em! Anh sẽ giúp em và Yoshinon nữa!
Vậy nên... Yoshino, dừng lại! Làm ơn đừng khóc nữa!

368
00:20:32,112 --> 00:20:38,113
Shidou... san...

368
00:20:38,512 --> 00:20:42,113
Làm ơn hãy để anh bên cạnh em!

368
00:20:45,112 --> 00:20:52,113
Giờ chúng ta hãy về nhà.
Sau đó sẽ nghĩ cách về Yoshinon, được chứ?

368
00:20:57,112 --> 00:21:03,113
Không có phản hồi từ Yoshino.
Nhưng bù lại, em ấy nắm chặt tay tôi




368
00:21:09,112 --> 00:21:12,113
Em bình tĩnh lại chưa?

368
00:21:13,112 --> 00:21:16,113
V... vâng...

368
00:21:17,112 --> 00:21:21,113
Giờ thì... anh thắc mắc chuyện gì 
đã xảy ra... với Yoshinon.

368
00:21:25,112 --> 00:21:34,113
Em ấy thực sự không nói một chút gì cả.
Dù tính em ấy nói rất nhiều.

368
00:21:35,112 --> 00:21:41,113
Có lẽ... đó là lỗi của em...

368
00:21:41,512 --> 00:21:45,113
Không, anh không nghĩ vậy.

368
00:21:46,112 --> 00:21:48,513
Nhưng...

368
00:21:49,512 --> 00:21:54,113
Yoshino... Hãy bình tĩnh 
và suy nghĩ chính chắn hơn đi.

368
00:21:58,112 --> 00:22:07,113
Yoshinon luôn bên cạnh em, phải không?
Anh chắc chắn phải có lý do
Yoshinon trở nên như vậy.

368
00:22:08,112 --> 00:22:11,113
Lý do... sao?

368
00:22:12,112 --> 00:22:17,513
Phải, anh chắc rằng phải có lý do nào đó
đằng sau nó... Vậy nên, Yoshino...

368
00:22:18,512 --> 00:22:26,113
Chúng ta phải chắc rằng chúng ta luôn chào đón
Yoshinon quay lại. Như thế Yoshinon mới có thể
cử động trở lại bất kỳ lúc nào.

368
00:22:26,512 --> 00:22:31,113
Nếu Yoshinon thấy em trông như hiện giờ,
cậu ấy sẽ buồn lắm đấy.

368
00:22:32,112 --> 00:22:37,113
Vâng. Em xin lỗi.

368
00:22:38,112 --> 00:22:42,513
Hãy nghỉ ngơi chút đi. 
Có thể chúng ta sẽ nghĩ ra điều gì đó
nếu chúng ta ngủ một giấc.

368
00:22:43,512 --> 00:22:53,113
Vâng... Shidou-san... Chúc ngủ ngon.

368
00:22:53,712 --> 00:22:56,113
Chúc em ngủ ngon.

368
00:23:01,112 --> 00:23:07,513
Cảm ơn cậu, Shin.
Có vẻ Yoshino đã bình tĩnh lại rồi.

368
00:23:08,512 --> 00:23:15,113
Phải, em cũng mừng. 
Nhưng nếu Yoshino cứ như vậy...
Yoshino sẽ mãi mãi bị giam cầm.

368
00:23:15,712 --> 00:23:26,113
Phải, cậu nói đúng.
Tuy nhiên, chúng ta đã làm hết sức.
Chúng ta không biết lý do đằng sau
tình trạng của Yoshinon.

368
00:23:27,112 --> 00:23:32,113
Em đã cổ tìm hiểu rồi.
Em không hiểu tại sao Yoshinon lại ngừng cử động.

368
00:23:32,513 --> 00:23:39,113
Giờ nghĩ lại... Hôm qua, Yoshinon đã nói
gì đó với Yoshino. Tôi nghĩ đó là lý do.

368
00:23:40,112 --> 00:23:46,113
"Khi mà cậu cứ dựa vào bố mẹ, 
thì cậu vẫn là một đứa trẻ..."

368
00:23:47,112 --> 00:23:52,513
Sao vậy Shin? Cậu vừa nghĩ ra điều gì à?

368
00:23:54,112 --> 00:24:01,113
À, không có gì đâu. Em sẽ cố gắng 
nghĩ cách để giúp Yoshinon. Gặp chị sau nhé.

368
00:24:01,512 --> 00:24:04,113
Được rồi, nhờ cậu đấy.

368
00:24:07,112 --> 00:24:14,113
Tôi nghĩ lại về lời Yoshinon nói hôm qua.
Tại sao Yoshinon lại ngừng cử động?

368
00:24:15,112 --> 00:24:24,113
Yoshino có nhiều việc em ấy không làm được.
Yoshinon luôn hỗ trợ em ấy.
2 đứa trông như cộng sự tốt, và...

368
00:24:25,112 --> 00:24:28,113
Tôi vừa nhận ra có gì đó hợp lý.

368
00:24:29,112 --> 00:24:35,113
Yoshinon. Lý do em ngừng cử động...
Có lẽ nào... là vì lợi ích của Yoshino?

368
00:24:36,112 --> 00:24:40,113
Tôi tin đó là câu trả lời.

368
00:24:41,112 --> 00:24:47,513
Chờ một chút, Yoshinon. Yoshino có thể
không phải đứa trẻ như em nghĩ đâu.

368
00:24:48,512 --> 00:24:56,113
Vừa nói trong phòng không có ai,
tôi cảm thấy như nghe thấy một giọng nói "Hmm"
phát ra từ đâu đó.

368
00:25:06,112 --> 00:25:15,113
Tôi muốn hít thở không khí bên ngoài...
Cũng không tệ khi đi bộ một mình như thế này.
Tôi nghe nói đi như vậy suy nghĩ sẽ tốt hơn.

368
00:25:16,112 --> 00:25:23,513
Những ngày này, thật khó hiểu.
Rất nhiều chuyện xảy ra như thể
không thể giải quyết được.

368
00:25:24,512 --> 00:25:27,513
Vậy mình nên đi bộ ở đâu nhỉ?

368
00:25:28,512 --> 00:25:35,113
Tôi nói điều đó và hối thúc chân tôi.
Phải, chỉ có thể đến công viên thôi.





368
00:25:40,112 --> 00:25:47,513
Yoshino đang dần trở thành người lớn.
Dù đó có là sự thật, mình có thể làm gì
vì lợi ích của Yoshino đây?

368
00:25:48,112 --> 00:25:55,113
Tôi biết câu trả lời sẽ không đến
dễ như vậy. Nhưng tôi không thể giúp
nếu không nghĩ về nó.

368
00:25:56,112 --> 00:25:59,113
Hở? Có ai đó đang đến...

368
00:26:01,712 --> 00:26:04,113
Yoshino đấy à?

368
00:26:05,112 --> 00:26:11,113
Yoshinon... hãy nói chuyện đi...

368
00:26:15,112 --> 00:26:24,113
Tại sao cậu... vẫn im lặng chứ?
Làm ơn... hãy trả lời tớ... trả lời tớ đi!

368
00:26:29,112 --> 00:26:31,513
Yoshino, hãy dừng lại đi.

368
00:26:33,112 --> 00:26:36,113
Shidou... san?

368
00:26:37,112 --> 00:26:44,113
Anh hiểu em đang lo lắng cho Yoshinon.
Nhưng em làm vậy không giúp Yoshinon trở về đâu.

368
00:26:45,112 --> 00:26:52,113
Em... em biết... nhưng...

368
00:26:53,112 --> 00:27:01,513
Điều này cũng rất đau với anh.
Nó còn đau đớn hơn nữa với em, Yoshino.
Anh hiểu điều đó. Em ấy là người bạn 
rất quan trọng với em.

368
00:27:02,512 --> 00:27:04,513
Vâng...

368
00:27:05,512 --> 00:27:08,113
Nhưng Yoshino... Nghe anh nói đây.

368
00:27:09,512 --> 00:27:17,113
1) Anh ở đây vì em, Yoshino.
2) Chúng ta hãy cùng đợi Yoshinon
3) Hãy cứ giả vờ như em ấy 
từ đầu đã không có ở đây.

368
00:27:18,112 --> 00:27:23,113
Yoshino... Em không cần phải buồn nữa.
Em hãy nghĩ thế này...
Yoshinon ngay từ đầu chưa hề ở đây.

368
00:27:25,912 --> 00:27:30,513
Nếu em tiếp tục ám ảnh về Yoshinon,
nó sẽ làm trái tim em tan vỡ...
Vì vậy, tốt nhất là...

368
00:27:31,112 --> 00:27:36,113
Tại sao...? Sao em... Sao em có thể...?

368
00:27:38,112 --> 00:27:44,113
Em không thể giả vờ rằng... 
Yoshinon chưa từng ở đây!

368
00:27:50,112 --> 00:27:52,113
Đột nhiên... mọi thứ lạnh cóng... Tệ rồi!

368
00:27:53,112 --> 00:27:55,113
Cứ... quên nó đi...

368
00:27:55,712 --> 00:27:57,113
Yo... Yoshino!

368
00:27:57,512 --> 00:27:58,713
Chờ đã, Yoshino!

368
00:28:03,112 --> 00:28:07,513
Tôi đã sai sao? Một đợt tuyết rơi 
trái mùa tạo ra cơn bão tuyết
bao phủ khắp thành phố.

368
00:28:08,112 --> 00:28:13,113
Nỗi buồn Yoshino không dừng lại,
và cơn bão tuyết ngày càng dữ dội hơn.

368
00:28:13,512 --> 00:28:15,513
Cuối cùng, mọi thứ đóng băng...

368
00:28:24,112 --> 00:28:35,113
Không ai hạnh phúc với kết thúc này...
Itsuka Shidou... Tiếp theo sẽ là... 
một giấc mơ hạnh phúc...

368
00:28:47,912 --> 00:28:53,113
1) Anh ở đây vì em, Yoshino.
2) Chúng ta hãy cùng đợi Yoshinon
3) Hãy cứ giả vờ như em ấy 
từ đầu đã không có ở đây.

368
00:28:54,512 --> 00:28:59,113
Có thể sẽ phải mất một thời gian.
Nhưng anh tin Yoshinon sẽ trở lại bình thường.
Dù sao thì Yoshinon cũng là bạn thân 
của em mà, phải không?

https://www.youtube.com/watch?v=IULbU-n37yg

368
00:29:03,112 --> 00:29:06,113
Anh chắc chắn cậu ấy sẽ về.
Cho đến lúc đó, anh sẽ chờ đợi cùng em. Anh hứa.

368
00:29:08,112 --> 00:29:14,113
Vâng! Cảm ơn anh.

368
00:29:15,112 --> 00:29:21,113
Yoshino nở một nụ cười tốt nhất có thể.
Cô bé vẫn còn buồn và lo lắng,
nhưng vẫn cười nghĩa là một tín hiệu tốt.

368
00:29:22,112 --> 00:29:27,113
Đó là lý do tôi quyết định...
tôi sẽ tiếp tục bên cạnh Yoshino.

368
00:29:30,112 --> 00:29:31,513
1) Anh ở đây vì em, Yoshino.
2) Chúng ta hãy cùng đợi Yoshinon
3) Hãy cứ giả vờ như em ấy 
từ đầu đã không có ở đây.

368
00:29:33,112 --> 00:29:37,113
Cho đến khi Yoshino trở lại, 
em sẽ không cô đơn đâu, Yoshino

368
00:29:37,512 --> 00:30:41,113
Anh đã nói hôm qua rồi.
Anh ở đây vì em.

368
00:29:46,112 --> 00:29:54,113
Dù Yoshinon có đi thật, xin em đừng nghĩ
em sẽ sống một mình. Em có rất nhiều bạn bè
xung quanh luôn quan tâm đến em mà.

368
00:29:55,112 --> 00:29:57,113
Bạn bè sao?

368
00:29:58,912 --> 00:30:07,113
Đúng vậy. Tohka, Kotori, Rinne...
Kể cả Reine-san và Kannazuki-san...
Mọi người đều nghĩ em rất quan trọng.

368
00:30:07,512 --> 00:30:15,113
Bọn anh không thể thay thế vị trí của Yoshinon...
Nhưng bọn anh luôn lo cho em.
Yoshino... Em không đơn độc đâu.

368
00:30:21,112 --> 00:30:29,113
Một chút thời gian. Chỉ cần một chút thời gian thôi.
Và em sẽ có thể chào đón Yoshinon trở về
với một nụ cười. Hãy cũng cố gắng nhé.

368
00:30:30,112 --> 00:30:32,113
Được chứ, Yoshino?

368
00:30:33,112 --> 00:30:43,113
Vâng... Cảm ơn anh, em... thấy tốt hơn rồi.

368
00:30:44,112 --> 00:30:52,513
Yoshino nở một nụ cười tốt nhất có thể.
Nụ cười hạnh phúc nhưng ẩn chứa sự cô đơn.

368
00:30:58,112 --> 00:31:08,113
Nỗi buồn của Yoshino quá lớn.
Mình càng nói, em ấy càng đau buồn.
Bây giờ, em ấy giống tôi khi tôi bị bỏ rơi.

368
00:31:09,112 --> 00:31:16,113
Trường hợp này, dù tôi có cố thế nào,
nó chỉ có tác dụng tạm thời.
Tôi có thể làm gì nữa không?

368
00:31:17,112 --> 00:31:23,513
Sẽ không có câu trả lời chính xác nào cả.
Không có cách nào giúp được.

368
00:31:25,112 --> 00:31:27,113
Hãy... hẹn hò.

368
00:31:28,112 --> 00:31:35,113
Cuối cùng, tôi nhận ra đó là
tất cả những gì tôi có thể làm.
Dù nó có thể giúp Yoshino chỉ một chút...

368
00:31:36,112 --> 00:31:43,513
Đây không phải vì không ổn định 
hay sợ em ấy mất kiểm soát.
Mình muốn giúp em ấy thật lòng.

368
00:31:44,512 --> 00:31:49,113
Ngày mai, mình sẽ hẹn hò với Yoshino.

368
00:31:51,112 --> 00:31:53,113
Ai vậy? Yoshino đấy à?

368
00:31:58,112 --> 00:32:01,113
Xin lỗi nhé, không phải Yoshino-chan đâu.

368
00:32:02,112 --> 00:32:06,113
Rinne? Sao cậu... chuyện gì đã xảy ra?

368
00:32:07,112 --> 00:32:11,113
Shidou này, tớ muốn nghe một chuyện.

368
00:32:12,112 --> 00:32:14,513
Cậu muốn nghe chuyện gì?

368
00:32:16,112 --> 00:32:23,113
Vâng, tớ muốn nghe nó.
Shidou... là chuyện cậu đang đối mặt.

368
00:32:24,112 --> 00:32:27,113
Ý cậu là sao?

368
00:32:28,112 --> 00:32:33,113
Cậu sẽ là người... hỗ trợ cho Yoshino-chan.

368
00:32:34,112 --> 00:32:41,113
Phải. Tớ không muốn nhìn thấy ai buồn.
Tớ muốn làm mọi người hạnh phúc.

368
00:32:42,112 --> 00:32:47,113
Vậy à? Vậy đó là câu trả lời của cậu.

368
00:32:48,112 --> 00:32:51,113
Phải. Tớ sẽ làm thế.

368
00:32:58,112 --> 00:33:03,513
Này Shidou, cậu có thể hứa không?

00:33:04,512 --> 00:33:07,113
Hứa sao? Hứa gì cơ?

368
00:33:08,112 --> 00:33:18,113
Trong tương lai, cậu sẽ yêu quý Yoshino-chan.
Bất kể chuyện gì xảy ra... cậu sẽ luôn như vậy.

368
00:33:19,112 --> 00:33:25,113
Sao tự nhiên cậu nói vậy?
Dù cậu không nói thì...

368
00:33:26,112 --> 00:33:32,113
Xin cậu đấy, tớ muốn cậu hứa với tớ.
Hãy nghĩ về Yoshino-chan...

368
00:33:33,112 --> 00:33:40,113
Nếu không, một ngày nào đó cậu sẽ rời xa em ấy.

368
00:33:40,512 --> 00:33:42,513
Rinne...

368
00:33:43,512 --> 00:33:45,513
Làm ơn...

368
00:33:46,512 --> 00:33:54,113
Được, tớ hứa. Là một người đàn ông, 
đã nói là phải giữ lời.
Tớ sẽ luôn quan tâm đến Yoshino.

368
00:33:55,112 --> 00:34:00,113
Tớ tin cậu sẽ làm được nếu cậu cố gắng.

368
00:34:01,112 --> 00:34:04,113
Haha, tớ cũng nghĩ vậy.

368
00:34:05,112 --> 00:34:16,113
Tớ tin cậu... Shidou.
Đừng quên... Đừng bao giờ quên đấy.

368
00:34:17,112 --> 00:34:20,113
Ừ, tất nhiên rồi.

368
00:34:21,112 --> 00:34:27,113
Nếu cậu thất hứa, cậu sẽ phải làm lại đấy nhé.

368
00:34:28,112 --> 00:34:30,113
Ừ, tớ biết rồi!

368
00:34:31,112 --> 00:34:34,113
Thế à? Cuối cùng nó đã được thực hiện rồi.

368
00:34:35,112 --> 00:34:39,113
Cái gì? Thực hiện gì cơ?

368
00:34:40,112 --> 00:34:43,113
Tớ xin lỗi.

368
00:34:44,912 --> 00:34:46,113
Nhưng mà...

368
00:34:50,112 --> 00:34:56,113
Sẽ ổn thôi. Nếu cậu cố gắng...
mọi thứ sẽ ổn thôi.

368
00:35:06,112 --> 00:35:09,113
Cái... cái gì vậy?

368
00:35:10,112 --> 00:35:13,113
Này Rinne... Cậu vừa làm gì...

368
00:35:14,112 --> 00:35:17,113
Này... cậu đâu rồi? Rinne về rồi à?

368
00:35:18,112 --> 00:35:22,513
Đã quá muộn rồi. Đi ngủ thôi.

368
00:35:33,112 --> 00:35:38,113
Ngày 1 tháng 7
