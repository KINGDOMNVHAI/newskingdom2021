﻿0
00:00:05,112-->00:00:12,113
Câu hỏi. Sư phụ Origami,
điều gì là quan trọng nhất trong tình yêu vậy?

1
00:00:12,512-->00:00:17,113
Biết tất cả mọi thứ về người đó.

2
00:00:17,313-->00:00:22,113
Ngưỡng mộ. Chỉ một câu nói ngắn gọn
mà lại có sức thuyết phục tuyệt vời.

3
00:00:22,312-->00:00:24,513
Đúng là sư phụ Origami.

4
00:00:25,112-->00:00:29,513
Nếu cô thích ai đó, việc tìm hiểu
về người đó là chuyện hiển nhiên.

5
00:00:30,112-->00:00:34,513
Cô nên sử dụng mọi cách mình có.

6
00:00:35,112-->00:00:38,913
Thuyết phục. Nói cách khác, thiên hạ có câu:

7
00:00:39,112-->00:00:44,113
"Biết mình biết ta, trăm trận trăm thắng"

8
00:00:44,312-->00:00:47,113
Đúng vậy. Tình yêu là một cuộc chiến.

9
00:00:47,512-->00:00:49,513
Cô không được phép lơ là dù chỉ một khắc.

10
00:00:50,112-->00:00:53,913
Ngưỡng mộ. Không hổ danh là sư phụ Origami.

11
00:00:54,112-->00:00:56,113
Lời nói của sư phụ thật sâu sắc.

12
00:00:56,312-->00:00:58,113
Điều đó không có gì xấu cả.

13
00:00:58,313-->00:01:03,913
Câu hỏi. Vậy để tìm hiểu 
về Shidou như sư phụ đã làm.

14
00:01:04,112-->00:01:06,113
Đệ tử nên làm gì?

15
00:01:06,313-->00:01:12,113
Những gì đệ tử tìm hiểu hoặc nghe được
từ Kotori và Reine vẫn chưa đủ.

16
00:01:12,312-->00:01:14,513
Chuyện đó là tất nhiên.

17
00:01:15,112-->00:01:19,513
Không thể tìm hiểu Shidou chỉ bằng
dữ liệu hoặc bên thứ 3 nào cả.

18
00:01:20,112-->00:01:25,113
Điều quan trọng nhất là phải 
quan sát và điều tra Shidou.

19
00:01:25,512-->00:01:31,113
Thắc mắc. Tuy nhiên, dù đệ tử 
có quan sát Shidou

20
00:01:31,512-->00:01:36,113
Nhưng đệ tử dường như không thể biết cậu ấy 
sâu sắc như sư phụ Origami.

21
00:01:36,312-->00:01:40,113
Một người vợ biết rõ mọi thứ
về chồng mình là chuyện hiển nhiên.

22
00:01:40,512-->00:01:45,113
Nếu đó là một người xa lạ,
tôi cũng chỉ có những thông tin
giống như người lạ thôi.

23
00:01:46,112-->00:01:49,513
Thuyết phục. Không hổ danh là sư phụ Origami.

24
00:01:50,113-->00:01:56,113
Đệ tử có thể tìm hiểu về Shidou
giống như sư phụ không?

25
00:01:56,512-->00:01:57,513
Được chứ.

26
00:01:58,112-->00:02:02,113
Tôi sẽ dạy cô vài chiêu. Đi theo tôi.

27
00:02:02,512-->00:02:07,113
Biết ơn. Cảm ơn rất nhiều, sư phụ Origami!

28
00:02:07,512-->00:02:11,113
Từ bây giờ, mọi thứ sẽ được tiến hành 
một cách bí mật. Hạ thấp giọng xuống.

29
00:02:12,112-->00:02:17,113
Xác nhận. Đệ tử hiểu rồi, thưa sư phụ.

30
00:02:19,112-->00:02:24,513
Xác nhận. Đây là lớp học 
của sư phụ và Shidou, phải không?

31
00:02:25,112-->00:02:28,113
Đúng vậy. Bọn tôi đã bắt đầu gặp nhau ở đây.

32
00:02:28,513-->00:02:30,513
Ở đây rất nhiều thứ.

33
00:02:31,112-->00:02:34,513
Để không bỏ lỡ bất cứ điều gì,
chúng ta sẽ phải kiểm tra kỹ càng.

34
00:02:35,112-->00:02:41,113
Lo lắng. Hiện giờ trong lớp chỉ có 2 chúng ta.

35
00:02:41,512-->00:02:44,113
Chúng ta sẽ kiểm tra gì vậy?

25
00:02:44,512-->00:02:47,113
Cô thiếu khả năng quan sát.

25
00:02:47,513-->00:02:52,113
Nơi này là một mỏ thông tin về Shidou.

25
00:02:52,512-->00:02:58,112
Hoang mang. Thông tin về Shidou sao?
Ở đâu mà có vậy?

25
00:02:58,512-->00:03:01,513
Nếu cô không biết, cô cần phải tìm kiếm.

25
00:03:02,112-->00:03:07,113
Một người vợ của Shidou như tôi, 
đó là chuyện phải làm.

26
00:03:07,512-->00:03:11,113
Ngưỡng mộ. Đệ tử sẽ tìm hiểu.

26
00:03:11,512-->00:03:15,513
Sư phụ Origami, sư phụ đang làm gì 
trên bàn Shidou vậy?

26
00:03:16,112-->00:03:22,113
Hôm nay, trong giờ nghỉ trưa, 
Shidou đã ngủ thiếp đi trên bàn học.

26
00:03:23,112-->00:03:26,513
Vì vậy, nước dãi của Shidou 
vẫn còn lưu lại trên đó.

26
00:03:27,512-->00:03:28,913
Ở đây.

26
00:03:29,112-->00:03:32,513
Ngạc nhiên. Một khả năng quan sát tuyệt hảo!

26
00:03:33,112-->00:03:36,513
Không hổ danh là sư phụ Origami.
Thật không thể xem thường.

26
00:03:37,112-->00:03:42,113
Vậy người định làm gì với nước dãi đó?

26
00:03:43,112-->00:03:46,113
Để bắt đầu, hãy kiểm tra các tính chất của nước bọt.

26
00:03:46,512-->00:03:52,513
Từ đó chúng ta có thể suy đoán về
bữa ăn sáng, tập thể dục, vv... của Shidou.

26
00:03:53,112-->00:03:57,513
Sau đó cảm nhận hương vị và mang nó về nhà.

26
00:03:58,112-->00:04:04,113
Sửng sốt. Thật là phức tạp,
thật sự đệ tử chẳng hiểu gì cả.

27
00:04:05,112-->00:04:08,513
Bây giờ, chúng ta sẽ tập trung vào phần da.

38
00:04:09,112-->00:04:12,113
Chúng ta sẽ lấy bút mà Shidou đã sử dụng.

39
00:04:12,512-->00:04:16,513
Đây là thứ mà Shidou đã cầm cả ngày.

40
00:04:17,113-->00:04:19,513
Mồ hôi của cậu ta được ngâm tẩm trong đó.

41
00:04:20,213-->00:04:24,513
Và điều cuối cùng nhưng không kém 
phần quan trọng, hôm nay cậu ta đã 
ngậm bút 7 lần khi suy nghĩ.

42
00:04:25,113-->00:04:29,113
Nói cách khác, tôi có thể thực hiện
một nụ hôn gián tiếp.

43
00:04:30,112-->00:04:32,513
Tôi đã có thêm một vật chứng nữa.

39
00:04:33,112-->00:04:40,513
Câu hỏi. Nếu bút của Shidou biến mất,
cậu ta không nghi ngờ sao?

40
00:04:41,113-->00:04:48,513
Không vấn đề gì. Mỗi ngày tôi đều
chuẩn bị một cây bút giống y như vậy.

41
00:04:49,113-->00:04:52,513
Nếu tôi đánh tráo, Shidou sẽ không nghi ngờ.

42
00:04:53,113-->00:04:57,113
Đến giờ, Shidou vẫn không phát hiện ra
dù chỉ một lần.

43
00:04:57,512-->00:05:01,513
Thuyết phục. Giống như bài tập về nhà vậy!

44
00:05:02,112-->00:05:04,513
Thật không hổ danh là sư phụ Origami.

45
00:05:05,112-->00:05:09,513
Điểm cần lưu ý khi đánh tráo là 
chọn một cây bút đã được sử dụng giống như thế.

46
00:05:10,112-->00:05:13,513
Tất nhiên, nếu nó được ngậm vào miệng
thì sẽ tốt hơn nhiều.

47
00:05:14,112-->00:05:18,513
Ớn lạnh. Bài tập về nhà nhiều thế?

48
00:05:19,112-->00:05:22,513
Đệ tử cảm nhận được tinh thần 
của một chiến binh đã sẵn sàng cho trận chiến.

49
00:05:23,112-->00:05:27,113
Tôi đã nói rồi. Tình yêu là một cuộc chiến.

50
00:05:27,512-->00:05:32,113
Với những thứ khác cũng vậy, 
chúng đều có thể bị đánh tráo.

51
00:05:32,312-->00:05:35,113
Quan trọng là cô phải chuẩn bị đúng món đồ.

52
00:05:35,512-->00:05:42,113
Bằng cách phân tích cửa hàng 
Shidou đã mua, cô có thể biết chính xác hơn.

53
00:05:42,512-->00:05:48,113
Thắc mắc. Nếu muốn biết những thông tin đó,
sư phụ phải đi thu thập chúng sao?

54
00:05:48,512-->00:05:53,513
Nó không chỉ là phân tích, 
cô phải theo dõi và thu thập chúng
thành một cơ sở dữ liệu.

55
00:05:54,112-->00:05:57,513
Điều đó giúp cô phân tích chính xác hơn.

56
00:05:58,112-->00:06:00,513
Ngạc nhiên. Đệ tử đã bị thuyết phục.

60
00:06:01,112-->00:06:04,513
Kiến thức của sư phụ Origami
thực sự rất sâu rộng.

41
00:06:05,113-->00:06:09,513
Đệ tử không bao giờ nghĩ rằng 
sư phụ có thể thu thập rất nhiều thông tin
chỉ từ cái bàn của Shidou.

42
00:06:10,113-->00:06:16,513
Chưa xong đâu. Ngày hôm nay khác 
ngày thường. Có một thứ rất có giá trị.

43
00:06:17,112-->00:06:19,513
Tò mò. Một thứ rất có giá trị sao?

44
00:06:20,112-->00:06:23,513
Bộ đồ thể dục ướt đẫm mồ hôi của Shidou.

45
00:06:24,112-->00:06:26,513
Đó là thứ có giá trị cực kỳ cao.

46
00:06:27,112-->00:06:30,513
Nếu như thường lệ, Shidou sẽ mang về nhà giặt.

47
00:06:31,113-->00:06:36,513
Nhưng hôm nay cậu ta đã không tìm thấy
túi đồ thể dục sau khi học xong.

48
00:06:37,112-->00:06:46,113
Ghi nhớ. Shidou nói cậu ta 
phải làm gì đó. Đệ tử nghĩ cậu ta 
đã không tìm thấy thứ cậu ta cần.

51
00:06:47,112-->00:06:50,513
Đúng vậy. Cậu ta không thể tìm thấy 
chiếc túi đồ thể dục của mình.

55
00:06:51,112-->00:06:56,113
Nhưng vì cậu ta không có thời gian
nên đã quay lại lớp học.

56
00:06:56,512-->00:06:58,113
Đúng như kế hoạch.

57
00:06:58,313-->00:07:00,513
Ý tôi là, đó là một sự may mắn.

58
00:07:01,113-->00:07:02,513
Cái túi đó ở đây.

59
00:07:03,113-->00:07:07,513
Thắc mắc. Tại sao cái túi đó
lại ở bàn của sư phụ?

60
00:07:08,112-->00:07:10,113
Tôi nghĩ có gì đó nhầm lẫn ở đây.

61
00:07:10,512-->00:07:15,513
Vì chúng tôi ngồi sát nhau nên đồ dùng
ở gần nhau cũng thường xảy ra.
Giờ tôi sẽ mang nó về.

62
00:07:16,112-->00:07:21,513
Dừng lại. Nếu mang nó về thì chẳng phải
là quá dễ bị phát hiện sao?

63
00:07:22,112-->00:07:23,113
Không vấn đề gì.

64
00:07:23,312-->00:07:27,513
Tôi đã lường trước việc này
và đã chuẩn bị đồ thể dục có tên của Shidou.

65
00:07:28,112-->00:07:31,513
Ớn lạnh. Điều này vượt xa 
bất kỳ kế hoạch nào có thể nghĩ ra...

66
00:07:32,112-->00:07:35,513
Sư phụ Origami thật đáng sợ.

67
00:07:36,112-->00:07:39,513
Nếu cô sợ, cô có thể rút lui ngay bây giờ.

68
00:07:40,112-->00:07:43,513
Những gì trong lớp chỉ là khởi động thôi.

69
00:07:44,112-->00:07:49,113
Ngạc nhiên. Vẫn còn nữa à?
Còn gì đáng sợ hơn nữa sao?

70
00:07:50,112-->00:07:53,513
Không. Đệ sẽ đi theo sư phụ Origami.

71
00:07:54,112-->00:07:59,113
Đạt đến trình độ như sư phụ Origami
là rất khó khăn, phải không?

72
00:08:00,112-->00:08:04,513
Nếu cô có ý chí để làm điều đó, 
cô có thể đi theo tôi.

73
00:08:05,112-->00:08:08,112
Tôi đồng ý. Cho phép tôi đi cùng cô.

74
00:08:12,112-->00:08:16,113
Thắc mắc. Tại sao chúng ta lại
đến nhà của Shidou vậy?

82
00:08:17,112-->00:08:19,513
Có vẻ hiện giờ Shidou và Kotori không có nhà...

83
00:08:20,112-->00:08:22,113
Chúng ta sẽ đợi họ về sao?

84
00:08:22,512-->00:08:27,113
Không. Chúng ta sẽ xong việc trước khi họ về.

85
00:08:28,512-->00:08:32,513
Thắc mắc. Tại sao sư phụ lại có 
chìa khóa cửa chính vậy?

86
00:08:33,112-->00:08:34,513
Shidou đã đưa cho tôi.

87
00:08:35,112-->00:08:37,513
Nghi ngờ. Thật vậy sao?

88
00:08:38,112-->00:08:40,113
Tốt hơn là cô đừng nên thắc mắc về nó.

89
00:08:40,512-->00:08:42,513
Quan trọng nhất, 
chúng ta không có nhiều thời gian đâu.

90
00:08:42,913-->00:08:43,913
Giờ chúng ta sẽ lên phòng.

91
00:08:44,112-->00:08:47,113
Thắc mắc. Phòng của Shidou phải không?

92
00:08:48,112-->00:08:53,113
Chúng tôi sẽ thu thập dữ liệu như
đã làm trước đây, phải không sư phụ?

93
00:08:53,512-->00:08:59,113
Đúng vậy. Đây là kế hoạch chỉ thực hiện được
khi Shidou và Itsuka Kotori đi khỏi nhà.

94
00:09:00,112-->00:09:05,113
Khi có nguy hiểm, phải thực hiện
nhanh chóng và cẩn thận.

95
00:09:05,513-->00:09:10,113
Hiểu biết. Đệ sẽ phải cẩn thận 
để không trở thành trở ngại cho sư phụ.

99
00:09:13,112-->00:09:17,113
Từ những gì chúng ta đã làm,
Yamai Yuzuru cũng đã học được một cái gì đó.

100
00:09:18,112-->00:09:22,513
Cô nghĩ mình cần làm gì khi 
đột nhập vào phòng của Shidou?

101
00:09:23,112-->00:09:27,113
Hiểu biết. Căn cứ vào những gì 
đã làm trong lớp học...

102
00:09:30,112-->00:09:33,513
Chúng ta bắt đầu 
với quần áo của Shidou phải không?

103
00:09:34,112-->00:09:35,913
Ví dụ như trong tủ quần áo này...

104
00:09:36,112-->00:09:37,113
Cô sẽ thất bại.

105
00:09:38,112-->00:09:40,113
Kinh ngạc. Tại sao chứ?

106
00:09:40,512-->00:09:44,513
Quần áo trong tủ đều đã được giặt và sấy khô.

107
00:09:45,512-->00:09:49,513
Khi Shidou chưa sử dụng,
nó chẳng có giá trị gì cả.

108
00:09:50,112-->00:09:56,113
Bác bỏ. Tuy nhiên, sau khi quan sát,
trong phòng không có quần áo nào đã sử dụng cả.

109
00:09:56,512-->00:10:01,113
Bên cạnh quần áo, có cái gì đó nắm giữ
"Bản chất Shidou" không?

108
00:10:01,512-->00:10:03,513
Suy nghĩ của cô quá hời hợt.

109
00:10:04,112-->00:10:05,513
Cô phải bắt đầu từ...

110
00:10:05,712-->00:10:06,913
Cái gối.

113
00:10:07,112-->00:10:09,513
Hoang mang. Cái gối sao?

114
00:10:10,112-->00:10:14,513
Đúng vậy. Thứ có đầy mùi Shidou.

1
00:10:15,112-->00:10:18,113
Nó là một đối tượng chứa 
"bản chất Shidou" cực kỳ cao.

1
00:10:19,112-->00:10:25,513
Nhưng nó là thứ sử dụng hàng ngày,
nếu đánh tráo sẽ rất dễ nhận ra.

1
00:10:26,112-->00:10:29,513
Nói cách khác, nó là một con dao
hai lưỡi rất nguy hiểm.

1
00:10:30,112-->00:10:32,113
Người mới không nên làm việc này.

1
00:10:32,512-->00:10:35,113
Tôn trọng. Không hổ danh là sư phụ Origami.

1
00:10:36,112-->00:10:38,113
Điều đó có nghĩa là người đang 
không có tân binh, phải không?

1
00:10:38,512-->00:10:43,113
Nhưng nếu đó là thứ có nguy cơ 
bị phát hiện, làm sao để đánh tráo được?

368
00:10:44,112-->00:10:47,113
Tôi đã chuẩn bị một vỏ gối giống nhau.

368
00:10:48,112-->00:10:52,113
Và đối với các mùi, nó sẽ bị xóa khi
món đồ được sử dụng bằng tay.

368
00:10:53,112-->00:10:55,513
Như vậy, khả năng nhận ra rằng nó là thấp.

368
00:10:56,512-->00:11:00,113
Thắc mắc. Sư phụ lấy vỏ gối ở đâu vậy?

368
00:11:01,113-->00:11:02,113
Bí mật quân sự.

368
00:11:03,112-->00:11:05,513
Bây giờ thì tìm trong thùng rác.

68
00:11:06,112-->00:11:10,113
Lẫn lộn. Trong thùng rác chỉ có rác thôi mà?

368
00:11:11,112-->00:11:16,113
Đây là một nơi dễ bị bỏ qua
nhưng thường tìm được
những thứ có giá trị đặc biệt.

368
00:11:16,512-->00:11:20,113
Ngạc nhiên. Vậy cũng được sao?

368
00:11:21,112-->00:11:27,113
Đệ tử bắt đầu nghi ngờ mọi thứ 
từ lúc theo sư phụ Origami đấy.

368
00:11:28,112-->00:11:30,113
Cuối cùng là xem ở đây.

68
00:11:31,112-->00:11:36,113
Thắc mắc. Đó là tủ quần áo 
đệ tử đã muốn xem mà?

368
00:11:36,512-->00:11:40,113
Quần áo đúng là không cần phải xem.

368
00:11:40,512-->00:11:49,113
Tuy nhiên, khi mở ngăn kéo 
và tháo nó ra, trong đó là...

368
00:11:50,112-->00:11:55,113
Câu hỏi. Những cuốn sách đó là gì vậy?

368
00:11:56,112-->00:12:00,113
Đây là những gì Shidou giấu cẩn thận
để Itsuka Kotori không tìm thấy nó.

368
00:12:00,513-->00:12:02,113
"Sách Ufufu"

368
00:12:03,113-->00:12:05,113
Nói cách khác, đây là những kho báu thực sự.

368
00:12:06,113-->00:12:10,113
Ngạc nhiên. Chúng được giấu trong đó sao?

368
00:12:10,513-->00:12:13,113
Nhưng sư phụ định làm gì 
với mấy cuốn sách đó vậy?

368
00:12:14,112-->00:12:17,513
Xác nhận nội dung. Tôi sẽ quét
và lưu nó trong cơ sở dữ liệu.

368
00:12:18,112-->00:12:25,113
Qua đó cô có thể biết những gì Shidou
tìm hiểu trong quan hệ tình dục và những thứ khác.

368
00:12:26,112-->00:12:27,513
Đây là thông tin rất quan trọng.

368
00:12:28,512-->00:12:32,113
Thấu hiểu. Nếu biết về sở thích
tình dục của Shidou.

368
00:12:32,312-->00:12:36,513
Chúng ta có thể có cách 
tiếp cận phù hợp phải không?

368
00:12:37,112-->00:12:38,513
Không hổ danh là sư phụ Origami.

368
00:12:39,113-->00:12:42,113
Vì kho tàng sưu tập thường xuyên được cập nhật

368
00:12:42,312-->00:12:45,113
Việc xác nhận và theo dõi rất quan trọng.

368
00:12:47,112-->00:12:50,113
Trong dữ liệu, những bộ ngực lớn
xuất hiện rất nhiều.

368
00:12:51,112-->00:12:54,113
Nó có thể khiến mọi thứ phá sản.

368
00:12:55,112-->00:12:59,113
Vui mừng. Có nghĩa là đệ có một lợi thế hơn Kaguya.

368
00:13:02,112-->00:13:04,513
Đây là một tin tốt...

368
00:13:05,112-->00:13:06,513
"Luật loli"

368
00:13:07,112-->00:13:10,513
Thắc mắc. "Luật loli" là cái gì?

368
00:13:11,112-->00:13:15,113
Nó là tên gọi những người có vóc dáng trẻ em, 
mặc dù về mặt pháp lý thì không vấn đề gì.

368
00:13:16,112-->00:13:21,513
Báo cáo. Nó mâu thuẫn với thông tin
về những bộ ngực lớn.

368
00:13:22,112-->00:13:25,513
Đàn ông đôi khi có những thời điểm
mâu thuẫn với khẩu vị mình thích.

368
00:13:26,112-->00:13:28,513
Đó gọi là "Chán cơm thèm phở".

368
00:13:29,112-->00:13:33,113
Khó khăn. Trong trường hợp đó, 
sư phụ sẽ khó xác định được cách tiếp cận.

368
00:13:35,112-->00:13:40,513
Có một khả năng nó không phải của Shidou mua
mà là mượn của bạn bè.

368
00:13:41,112-->00:13:43,113
hoặc đó cũng là hình mẫu mà Shidou thích.

368
00:13:44,112-->00:13:46,513
Phân tích chính xác rất khó.

368
00:13:47,112-->00:13:49,513
Tôi không tìm ra câu trả lời.

368
00:13:50,112-->00:13:55,113
Ngạc nhiên. Ngay cả sư phụ Origami cũng
không biết câu trả lời chính xác.

368
00:13:55,512-->00:13:56,513
Thật phức tạp

368
00:13:57,113-->00:13:59,113
Do đó, phân tích rất quan trọng,

368
00:13:59,312-->00:14:05,113
Ví dụ như những trang được lật nhiều lần,
những cuốn sách được đọc nhiều nhất.

368
00:14:06,113-->00:14:07,513
Nhiệm vụ này xong rồi.

368
00:14:08,112-->00:14:11,513
Cảnh báo. Đệ tử nghe thấy tiếng cửa trước mở.

368
00:14:12,112-->00:14:14,113
Shidou hoặc Kotori đã về rồi sao?

368
00:14:14,512-->00:14:15,513
Chiến dịch hoàn thành.

368
00:14:16,112-->00:14:17,913
Giờ là lúc rút lui.

368
00:14:18,112-->00:14:23,113
Xác nhận. Tuy nhiên, sư phụ Origami,
đôi giày để ở cửa chính...

368
00:14:24,112-->00:14:25,113
Cái gì cơ?

368
00:14:25,312-->00:14:29,113
Đột nhập thì phải mang giày theo chứ.

368
00:14:29,512-->00:14:30,513
Đó là điều cơ bản nhất của những điều cơ bản đấy.

368
00:14:31,113-->00:14:35,513
Ngạc nhiên. Đây là lần đầu 
đệ tử biết đấy. Đệ phải làm gì đây?

368
00:14:37,112-->00:14:40,113
Tôi đã chuẩn bị một lối thoát 
cho tình huống khẩn cấp.

368
00:14:41,112-->00:14:44,513
Cửa sổ phòng khách phải đi xuống phía dưới.

368
00:14:45,112-->00:14:46,513
Chọn đường đó là không ổn.

368
00:14:47,112-->00:14:48,513
Tôi sẽ nhảy ra cửa sổ phòng này.

368
00:14:49,112-->00:14:53,113
Với các dấu vết trong phòng Shidou
thì không sao.

368
00:14:54,112-->00:14:55,513
Thu dọn đống đồ vừa lấy ra đi.

368
00:14:56,112-->00:15:01,113
Choáng váng. Sư phụ Origami,
chúng ta phải nhanh lên!

368
00:15:02,112-->00:15:03,113
Tôi sẽ giao phần còn lại cho cô.

368
00:15:03,513-->00:15:08,113
Hoang mang. Sư phụ Origami, 
không có thời gian...

368
00:15:09,112-->00:15:10,113
Good luck!

368
00:15:11,112-->00:15:14,113
Hoảng sợ. Nếu người để lại cho đệ tử
thì đúng là thảm họa.

368
00:15:14,512-->00:15:17,113
Mình dọn hết vào tủ quần áo rồi.

368
00:15:18,112-->00:15:21,113
Nguy hiểm. Mình cần phải trốn ngay.

368
00:15:21,512-->00:15:25,113
Mình không nghĩ ra được gì cả. 
Mình cần có sự trợ giúp.

368
00:15:27,112-->00:15:29,113
Ka... Kaguya...!

368
00:15:50,112-->00:15:51,113


368
00:15:46,112-->00:15:49,113


368
00:15:50,112-->00:15:51,113
