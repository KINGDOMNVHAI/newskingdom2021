﻿1
00:00:25,512-->00:00:30,313
Now then, I should probably go back now.
Looks like Kotori is coming back earlier too.

2
00:00:31,012-->00:00:32,213
Shidou-san.

3
00:00:32,512-->00:00:33,213
Ah...Kurumi!

4
00:00:34,112-->00:00:39,113
Ara ara. You shouldn't be that surprised.

5
00:00:40,112-->00:00:41,513
W... Why are you here?

6
00:00:42,112-->00:00:44,513
Shidou-san, you're alone today right?

7
00:00:45,112-->00:00:48,113
Hey, listen to me! Why are you here?

8
00:00:49,112-->00:00:54,113
Isn't that obvious? I was waiting you, Shidou-san.

9
00:00:55,512-->00:00:58,113
So, you really are alone right?

10
00:00:58,712-->00:01:00,113
Well... yes.

11
00:01:01,112-->00:01:03,113
Ah... Thank god!

12
00:01:04,112-->00:01:10,113
I wanted to have a private talk with Shidou-san.
So I've waiting you all this time.

13
00:01:11,112-->00:01:12,513
Talk... with me?

14
00:01:13,112-->00:01:18,113
I want to know more about you.

15
00:01:19,112-->00:01:21,513
Every little aspect... everything.

16
00:01:22,112-->00:01:24,113
W... What are you talking about?

17
00:01:25,112-->00:01:28,113
Like, you secret.

18
00:01:28,513-->00:01:30,113
My secret?

19
00:01:30,512-->00:01:33,113
Yes. I'm very interested.

20
00:01:34,512-->00:01:39,113
S... sorry but, I won't share something that
might be a weakness with my enemy.

21
00:01:41,112-->00:01:44,113
Weakness? Aren't we being a little too rude?

22
00:01:44,812-->00:01:49,113
What I want to know is your secret.

23
00:01:50,112-->00:01:52,513
T... There's no way I'm telling you that. Bye!

24
00:01:55,112-->00:01:55,913
Kurumi.

25
00:01:56,552-->00:01:58,113
Yes? What is it?

26
00:01:59,112-->00:02:00,713
Why are you following me?

27
00:02:02,112-->00:02:04,113
Why I wonder...

28
00:02:04,512-->00:02:06,113
Don't answer my question with another question.

29
00:02:09,112-->00:02:10,113
Do whatever you want.

30
00:02:10,513-->00:02:13,113
Yes. I'll do that.

31
00:02:14,113-->00:02:18,113
Well. As long as the enemy is following me,
I should probably give up on returning home.

32
00:02:20,113-->00:02:22,113
Shit! I got lost?

33
00:02:23,112-->00:02:25,113
Where is this place? Am I an idiot?








34
00:02:26,312-->00:02:30,113
Are you perhaps... lost?

35
00:02:30,512-->00:02:32,113
Well, looks like it.

36
00:02:34,112-->00:02:35,513
You're in trouble.

37
00:02:36,112-->00:02:40,113
If you really thought so,
you wouldn't be making that smile.

38
00:02:41,112-->00:02:45,113
Shidou-san, I don't mind showing you the way back.

39
00:02:45,512-->00:02:46,113
Really?

40
00:02:46,512-->00:02:51,113
Yes, of course. But it won't be for free.

41
00:02:51,512-->00:02:54,513
Wait... forget it. I'll sort something out.

42
00:02:56,512-->00:03:01,113
You are acting like a child Shidou-san.

43
00:03:01,572-->00:03:03,113
I'm not!

44
00:03:04,112-->00:03:05,513
I just find my way back.

45
00:03:06,112-->00:03:08,113
Why don't you try to calm down a little bit first?

46
00:03:09,112-->00:03:13,113
Well, but I do think that's also so much like Shidou-san.

47
00:03:15,112-->00:03:20,113
Ah! I'm very sorry! What was I thinking?

48
00:03:21,112-->00:03:28,513
Taking me to this dark and empty place...
You're really something Shidou-san.

49
00:03:30,112-->00:03:32,113
Y... you're wrong! That was not my intention!

50
00:03:32,512-->00:03:35,113
You don't have to be ashamed, Shidou-san.

51
00:03:36,112-->00:03:38,113
I really just got lost!














52
00:03:38,512-->00:03:42,113
If you really wished for it... I...

53
00:03:43,112-->00:03:45,113
I... uh... eh... huh!?

54
00:03:46,112-->00:03:48,513
H... hey Kurumi, where are you touching?

55
00:03:49,112-->00:03:53,113
Yes? I haven't done anything yet.

56
00:03:53,512-->00:03:55,113
Eh? But...

57
00:03:58,112-->00:04:01,113
So there is a cat.

58
00:04:02,112-->00:04:04,513
That's probably what you felt.

59
00:04:06,112-->00:04:07,113
Th... that surpised me.

60
00:04:09,112-->00:04:14,513
Anyways, this cat... seems very attached
to humans... he's not trying to escape.

61
00:04:17,112-->00:04:20,113
Kurumi, why don't you pat him too? It feels good.

62
00:04:20,532-->00:04:21,513
Yes.

63
00:04:22,112-->00:04:24,513
Here. Looks like he wants you to join too.

64
00:04:26,112-->00:04:28,113
He really isn't afraid huh...

65
00:04:29,512-->00:04:32,113
He's so small and soft.

66
00:04:33,112-->00:04:34,113
You look relaxed.

67
00:04:35,112-->00:04:39,513
But... is he alone? where did his family go?

68
00:04:41,512-->00:04:47,113
Perhaps you're lost too? If so, you're
in the same situation as Shidou-san.

69
00:04:48,512-->00:04:50,113
Look, look, Shidou-san!

70
00:04:51,112-->00:04:55,113
He's making a cute sound!






71
00:04:55,512-->00:04:56,513
That's right.







72
00:04:57,112-->00:05:06,113
Shidou-san, did you know? if you rub in places
where his hand can't reach, he'll be very happy... like this.

73
00:05:08,112-->00:05:09,113
Looks like he's enjoying it huh?

74
00:05:10,112-->00:05:15,113
It tickles... licking my finger won't taste good.

75
00:05:16,112-->00:05:18,113
Haha, he must be hungry








76
00:05:19,112-->00:05:23,513
Oh, you're right! Isn't there something he can eat?











77
00:05:24,112-->00:05:25,513
Looks like you're having fun, Kurumi.

78
00:05:27,112-->00:05:30,513
No... just... how should I put it?

Không... chỉ là... biết nói sao nhỉ?

79
00:05:32,113-->00:05:34,113
You're making this gentle smile.

Cậu cười trông rất hiền lành và tốt bụng.

80
00:05:35,112-->00:05:37,113
So you can also make that face, huh? Kurumi

81
00:05:39,112-->00:05:40,513
W... what are you saying, Shidou-san?

82
00:05:41,512-->00:05:42,513
Do you like cat?

83
00:05:43,512-->00:05:45,113
W... well... this is...

84
00:05:46,512-->00:05:49,113
H... he happens to be here so I'm playing with him.

85
00:05:49,512-->00:05:50,513
Why are you reacting like that?

86
00:05:51,113-->00:05:53,113
I'm perfectly normal!

87
00:05:54,112-->00:05:57,513
H... hey, if you raise your voice that much
you'll scare the cat.

88
00:05:59,112-->00:06:00,113
What's going on, Kurumi?

89
00:06:01,112-->00:06:03,113
The cat got inside my clothes...

90
00:06:04,112-->00:06:06,113
Calm down! I can't see any things...

91
00:06:07,112-->00:06:14,113
W...wait... It tickles! Um... please catch him!

92
00:06:14,512-->00:06:15,513
No no no no!

93
00:06:16,512-->00:06:19,113
Catch the cat? That would require...

94
00:06:19,512-->00:06:24,113
Aaa! Hurry up! Or I'll just have to take my clothes off!

95
00:06:24,512-->00:06:26,113
Don't do that!

96
00:06:27,112-->00:06:29,113
S... Shidou-san... I'm already...

97
00:06:30,112-->00:06:34,113
Aaa... Don't take your clothes off! 
What if someone sees you?

98
00:06:34,812-->00:06:36,513
B... But... Aaaa!

99
00:06:37,112-->00:06:40,113
Alright! I'll catch it so please 
don't take your clothes off!

100
00:06:41,112-->00:06:43,513
O... okay, please do it...

101
00:06:45,112-->00:06:48,513
Um... your hand tickles...

102
00:06:49,512-->00:06:54,113
H... Hold it up! It'll be over soon! 
Or else I'll be a goner too!

103
00:06:55,512-->00:06:56,513
Alright, got it!

104
00:06:57,512-->00:07:01,113
Aaaa.... Shidou-san, where are you touching?

105
00:07:02,112-->00:07:06,113
T... that's not it Kurumi! 
That was just an accident! Sorry!

106
00:07:06,812-->00:07:10,113
if you're going to touch it, be gentle...

107
00:07:11,112-->00:07:13,113
W... what are you saying?

108
00:07:14,112-->00:07:15,113
The next one is...

109
00:07:16,112-->00:07:19,113
Aaaa... Not that place!

110
00:07:20,112-->00:07:23,513
N... No... I was trying to catch the cat...

111
00:07:26,112-->00:07:32,113
What are you saying, Shidou-san?
The cat's been over there for a while...

112
00:07:36,112-->00:07:37,113
S... Since when?

113
00:07:38,112-->00:07:42,113
Since you got your hand into my clothes.

114
00:07:43,112-->00:07:45,513
Why was I even trying...

115
00:07:46,112-->00:07:51,513
Ara, so you knew all that and still did this to me?

116
00:07:53,112-->00:07:56,113
T... That's not it! I wanted to save you!

117
00:07:58,112-->00:08:02,113
Do as you please until you're done.


118
00:08:03,512-->00:08:05,113
I... I wouldn't do it in this place!

119
00:08:05,512-->00:08:08,113
You'd do it if we were in a different place?

120
00:08:09,112-->00:08:10,113
T... That's not what I meant!

121
00:08:12,112-->00:08:15,113
I don't mind if you keep doing it right now...

122
00:08:16,112-->00:08:22,513
Because if I'm alone with Shidou-san,
I can do whatever I want.

123
00:08:24,112-->00:08:28,113
B... But you see... if I'm too late, they'll get worried.

124
00:08:29,112-->00:08:36,113
I want to talk more with you. 
I still want to know more about you

125
00:08:37,112-->00:08:40,113
What are you saying? I haven't said 
anything about me yet...

126
00:08:41,112-->00:08:42,113
That's not true!

127
00:08:44,112-->00:08:52,113
I still don't know your secret, but I got to
contemplate that embarrassed side of yours.

128
00:08:56,112-->00:09:00,113
Well... I'm also satisfied that I got to see you today.

129
00:09:02,512-->00:09:06,513
Um... C... Cute things are cute, so it is ok!

130
00:09:07,512-->00:09:08,513
You're right.

131
00:09:12,112-->00:09:15,113
Well, what are we going to do with this guy?

132
00:09:16,112-->00:09:17,113
What do you think, Kurumi?

133
00:09:18,112-->00:09:20,113
Eh, me?

134
00:09:20,512-->00:09:22,113
Want to search for his parents?

135
00:09:25,112-->00:09:29,513
Well, if you really want to do it that badly,
I'll accompany you.

136
00:09:30,112-->00:09:33,513
That's right. I want to find them. 
I really want to find them.

136
00:09:36,112-->00:09:39,113
If you're going that far then, I guess I have no choice.

137
00:09:40,112-->00:09:46,113
So it's decided. You carry him, Kurumi.
He seems more attached to you anyway.

138
00:09:46,512-->00:09:48,113
Alright.

139
00:09:50,112-->00:09:56,113
Here, here. It's okay... we'll find them soon.

140
00:09:58,112-->00:09:59,513
Ok, let's go!

141
00:10:04,112-->00:10:07,113
Haa... We're finally back in a familiar place.

142
00:10:07,512-->00:10:12,113
Yes... I'm glad we found that cat's parents.

143
00:10:13,112-->00:10:15,113
You're so gentle, Kurumi.

144
00:10:16,112-->00:10:20,113
Y... you said you wanted my help and that's why I did it.

145
00:10:20,512-->00:10:24,513
And there's also that teary face you made
when we found his parents...

146
00:10:25,112-->00:10:26,113
That's not true!

147
00:10:27,112-->00:10:29,113
If you say so.

148
00:10:31,112-->00:10:34,513
I can't figure out what is she thinking about
but she was also this gentle huh.

149
00:10:36,112-->00:10:37,113
Did you say something?

150
00:10:38,112-->00:10:41,113
No, I didn't. I was just kind of thirsty.

151
00:10:43,112-->00:10:44,113
That's right.

152
00:10:45,112-->00:10:48,113
Want to go bye something? I'll let you
pick whatever you want.

153
00:10:48,512-->00:10:51,113
Then, let's do that.

154
00:10:53,112-->00:10:56,113
There's also a bench over there.
So want to take a break here?

155
00:11:03,112-->00:11:04,113
Haa... refreshing

156
00:11:06,112-->00:11:07,113
What's wrong?

157
00:11:08,112-->00:11:14,113
You were avoiding me all this time, but you're
totally defenseless right now.

158
00:11:16,112-->00:11:21,113
Don't worry, I have no intention to do anything right now.

159
00:11:22,112-->00:11:23,113
Alright.

161
00:11:25,112-->00:11:27,513
You really went straight to believe my words huh?

162
00:11:28,112-->00:11:32,113
If you really planned to do something, you would've
done it when we were alone right?

163
00:11:33,112-->00:11:36,513
Yeah... well... it's not like nothing happened either...

164
00:11:37,512-->00:11:44,113
That's right! When I finally get 
the perfect chace... what have I done...

165
00:11:45,112-->00:11:46,113
Hey, hey.

166
00:11:46,512-->00:11:49,513
I'm joking, don't worry about it.

167
00:11:50,112-->00:11:52,113
Is that really a joke?

169
00:11:53,112-->00:11:55,113
(But it does look like she's regretting something...)

170
00:11:57,112-->00:11:58,513
Thanks.

171
00:12:00,112-->00:12:02,113
You drank it that fast, were you really that thirsty?

172
00:12:03,112-->00:12:05,113
We were running a bit too much today.

173
00:12:06,512-->00:12:09,113
Well, you were always playing with the cat while
we were looking for her parents anyways.

174
00:12:11,112-->00:12:13,513
That's... he was following me 
so I didn't have much of a choice.

175
00:12:14,112-->00:12:15,113
Okay. Okay.

176
00:12:16,112-->00:12:18,113
Here, you can drink mine if you want.

177
00:12:19,112-->00:12:22,113
Don't really think I should take it, 
but I'll accept your offer.

178
00:12:26,112-->00:12:28,113
What is it? You don't like it?

179
00:12:30,112-->00:12:32,113
What's so funny, Kurumi?

180
00:12:33,512-->00:12:36,113
This makes it... a indirect kiss right?

181
00:12:37,112-->00:12:38,113
I... Indirect kiss?

182
00:12:40,112-->00:12:41,513
I didn't offer you my drink with that intention!

183
00:12:42,113-->00:12:47,113
Ara, is that so? I really thought you were well aware.

184
00:12:47,512-->00:12:52,113
Of course not! That's right, give me that back! 
I'll buy you a new one!

185
00:12:54,112-->00:12:57,113
Ara... what an honor.

186
00:13:04,112-->00:13:06,113
You're like a kid after all...

187
00:13:07,112-->00:13:10,113
I didn't know you were this much of an amusing person.

188
00:13:11,112-->00:13:12,513
Sorry then. For being a kid.

189
00:13:13,112-->00:13:17,113
Don't take it like that, I'm praising you.

190
00:13:17,112-->00:13:19,113
It doesn't feel like it.

191
00:13:20,112-->00:13:24,113
And like this we have both successfully
exchanged an indirect kiss right?

192
00:13:27,112-->00:13:29,113
A... Kurumi... that's...!

193
00:13:30,112-->00:13:34,113
Was this one on purpose or premeditated?


194
00:13:34,512-->00:13:36,113
It wasn't on purpose nor premeditated!

195
00:13:37,112-->00:13:39,113
It was a mistake!

196
00:13:40,112-->00:13:44,113
If it were a mistake you'd be making fun of me right?

197
00:13:46,112-->00:13:47,513
I can't say anything to that.

198
00:13:48,512-->00:13:50,113
So, how was it?

199
00:13:51,112-->00:13:52,113
How was it...?

200
00:13:53,112-->00:13:57,113
Of course... the taste of my kiss.

201
00:13:57,512-->00:14:01,113
Well... even if you ask me... I don't know.

202
00:14:02,112-->00:14:09,113
Ara! What a waste! In that case, the next one will
be a direct one. Want to taste it?

203
00:14:09,512-->00:14:14,113
Huh? It's not that... but...

204
00:14:16,112-->00:14:18,513
(If I go ahead and kiss her right now will I be
able to seal her powers?)

205
00:14:20,112-->00:14:22,113
(It might work...)












206
00:14:23,112-->00:14:24,113
(No! Wait...)

207
00:14:26,112-->00:14:30,113
What a surprise, you're not that bad after all.

208
00:14:31,112-->00:14:34,113
No! There's a complicated reason for this.

209
00:14:35,112-->00:14:38,113
What? My phone?

210
00:14:39,112-->00:14:45,113
Ara, too bad. Someone decided 
to interfere at a good part.

211
00:14:46,112-->00:14:48,113
That's right...

212
00:14:50,112-->00:14:51,513
A mail from Kotori huh?

213
00:14:54,112-->00:14:59,113
Aaa! Crap! I totally forgot about the dinner!

214
00:15:00,112-->00:15:02,113
She'll be so angry...

215
00:15:03,112-->00:15:09,113
Ara ara, that's a problem right?
Should I go and apologize too?

216
00:15:10,112-->00:15:12,113
If you do that, it'll be even worse!

217
00:15:12,512-->00:15:14,113
Ara, is that so?

218
00:15:15,512-->00:15:19,113
I thought it'd be a perfect opportunity
to prove my innocence.

219
00:15:20,112-->00:15:22,113
That's... maybe some other time.

220
00:15:23,112-->00:15:25,113
You just promised it, Shidou-san.

221
00:15:27,112-->00:15:28,513
I'll be anxiously waiting.

222
00:15:29,112-->00:15:30,113
Y... Yes...

223
00:15:31,112-->00:15:37,113
In that case, it is also about time for me to leave.

224
00:15:38,112-->00:15:42,113
Ara? Do you want to spend more time with me?

225
00:15:43,112-->00:15:43,813
That's not it!

226
00:15:44,512-->00:15:46,113
I know.

227
00:15:47,112-->00:15:51,113
It wasn't something that important, but it was really fun.

228
00:15:52,112-->00:15:54,113
So I'll retreat for today.

229
00:15:55,112-->00:15:56,113
I see.

230
00:15:57,112-->00:16:02,113
I really hope we meet again so we can walk
in the city alone like this one more time.

231
00:16:03,112-->00:16:05,113
I feel that good when you're being this honest.

232
00:16:06,112-->00:16:08,513
Let's meet again, Shidou-san.

233
00:16:09,112-->00:16:11,113
Y... Yes, see you later.

234
00:16:14,112-->00:16:20,113
A lot of things happened but I got to understand
Kurumi a little bit more. Isn't that enough?

235
00:16:21,112-->00:16:24,113
Shidou-san! I forgot to tell you!

236
00:16:25,112-->00:16:27,113
Oh? What? What happended?

237
00:16:27,512-->00:16:32,113
Tomorrow, I'll definitely be listening 
to that secret of yours

238
00:16:33,112-->00:16:34,113
I told you I had no intention to...

239
00:16:36,112-->00:16:39,513
W... Wait Kurumi! What do you mean with "tomorrow"?

240
00:16:42,112-->00:16:45,113
See you tomorrow then, Shidou-san!
