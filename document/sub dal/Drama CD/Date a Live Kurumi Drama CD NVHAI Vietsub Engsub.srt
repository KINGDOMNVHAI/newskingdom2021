﻿1
00:00:25,512-->00:00:30,313
Now then, I should probably go back now.
Looks like Kotori is coming back earlier too.



















Bây giờ, chắc mình nên về thôi. 
Kotori chắc cũng về sớm.

2
00:00:31,012-->00:00:32,213
Shidou-san.

3
00:00:32,512-->00:00:33,213
Ah...Kurumi!

4
00:00:34,112-->00:00:39,113
Ara ara. You shouldn't be that surprised.























Ara ara. Cậu không nên hoảng hốt như thế.

5
00:00:40,112-->00:00:41,513
W... Why are you here?























S...sao cậu lại ở đây?

6
00:00:42,112-->00:00:44,513
Shidou-san, you're alone today right?























Shidou-san, hôm nay cậu rảnh chứ?

7
00:00:45,112-->00:00:48,113
Hey, listen to me! Why are you here?























Nè, nghe tớ hỏi đã! Sao cậu lại ở đây?

8
00:00:49,112-->00:00:54,113
Isn't that obvious? I was waiting you, Shidou-san.























Cậu không thấy sao? Tớ đang đợi cậu, Shidou-san.

9
00:00:55,512-->00:00:58,113
So, you really are alone right?























Vậy, cậu có rảnh không?

10
00:00:58,712-->00:01:00,113
Well... yes.























Ơ...rảnh...

11
00:01:01,112-->00:01:03,113
Ah... Thank god!























Ah...vậy thì tốt quá!

12
00:01:04,112-->00:01:10,113
I wanted to have a private talk with Shidou-san.
So I've waiting you all this time.




















Tớ muốn nói chuyện riêng với Shidou-san.
Vậy nên tớ đợi cậu nãy giờ.

13
00:01:11,112-->00:01:12,513
Talk... with me?
























Nói chuyện...với tớ?

14
00:01:13,112-->00:01:18,113
I want to know more about you.























Tớ muốn biết nhiều hơn về Shidou-san.

15
00:01:19,112-->00:01:21,513
Every little aspect... everything.























Tất cả mọi thứ... từng chi tiết nhỏ.

16
00:01:22,112-->00:01:24,113
W... What are you talking about?























C...cậu đang nói về cái gì vậy?

17
00:01:25,112-->00:01:28,113
Like, you secret.























Chẳng hạn như... bí mật của cậu.

18
00:01:28,513-->00:01:30,113
My secret?
























Bí mật của tớ?

19
00:01:30,512-->00:01:33,113
Yes. I'm very interested.























Đúng. Tớ rất muốn được biết đấy.

20
00:01:34,512-->00:01:39,113
S... sorry but, I won't share something that
might be a weakness with my enemy.




















Xin lỗi, nhưng... tớ không thể nói về điểm yếu
của tớ cho kẻ thù biết được.

21
00:01:41,112-->00:01:44,113
Weakness? Aren't we being a little too rude?























Điểm yếu sao? Nghe có vẻ nghiêm trọng quá.

22
00:01:44,812-->00:01:49,113
What I want to know is your secret.























Thứ tớ muốn biết là... bí mật của cậu.

23
00:01:50,112-->00:01:52,513
T... There's no way I'm telling you that. Bye!























Kh...không bao giờ tớ nói đâu. Tạm biệt!

24
00:01:55,112-->00:01:55,913
Kurumi.

25
00:01:56,552-->00:01:58,113
Yes? What is it?























Vâng? Chuyện gì vậy?

26
00:01:59,112-->00:02:00,713
Why are you following me?























Sao cậu đi theo tớ vậy?

27
00:02:02,112-->00:02:04,113
Why I wonder...























Tớ cũng tự hỏi vậy đấy.

28
00:02:04,512-->00:02:06,113
Don't answer my question with another question.























Đừng trả lời bằng cách thêm câu hỏi khác như thế.

29
00:02:09,112-->00:02:10,113
Do whatever you want.























Tùy cậu vậy.

30
00:02:10,513-->00:02:13,113
Yes. I'll do that.























Vâng, cứ đi đi.

31
00:02:14,113-->00:02:18,113
Well. As long as the enemy is following me,
I should probably give up on returning home.




















(Nếu có kẻ thù đi theo mình như vậy, 
mình không nên về nhà.)

32
00:02:20,113-->00:02:22,113
Shit! I got lost?























Chết tiệt! Mình bị lạc rồi sao?

33
00:02:23,112-->00:02:25,113
Where is this place? Am I an idiot?























Đây là đâu vậy? Mình ngốc thật!

34
00:02:26,312-->00:02:30,113
Are you perhaps... lost?























Cậu thực sự...bị lạc à?

35
00:02:30,512-->00:02:32,113
Well, looks like it.























Ừ, có vẻ vậy.

36
00:02:34,112-->00:02:35,513
You're in trouble.























Vậy là cậu gặp rắc rối rồi.

37
00:02:36,112-->00:02:40,113
If you really thought so, you wouldn't be
making that smile.






















Nếu cậu nghĩ vậy, cậu đã không cười tớ như thế.

38
00:02:41,112-->00:02:45,113
Shidou-san, I don't mind showing you the way back.
























Shidou-san, tớ có thể chỉ cậu lối ra.

39
00:02:45,512-->00:02:46,113
Really?























Thật không?

40
00:02:46,512-->00:02:51,113
Yes, of course. But it won't be for free.























Dĩ nhiên. Nhưng phải có điều kiện đấy.

41
00:02:51,512-->00:02:54,513
Wait... forget it. I'll sort something out.
























Vậy quên đi. Tớ sẽ tự tìm lối ra.

42
00:02:56,512-->00:03:01,113
You are acting like a child Shidou-san.























Shidou-san cứ như trẻ con ấy nhỉ.

43
00:03:01,572-->00:03:03,113
I'm not!
























Không có đâu!

44
00:03:04,112-->00:03:05,513
I should only find my way back.
























Tớ chỉ đi tìm đường ra thôi.

45
00:03:06,112-->00:03:08,113
Why don't you try to calm down a little bit first?























Sao cậu không bình tĩnh lại trước đã?

46
00:03:09,112-->00:03:13,113
Well, but I do think that's also so much like Shidou-san.























Nhưng mà, tớ nghĩ như thế mới là Shidou-san.

47
00:03:15,112-->00:03:20,113
Ah! I'm very sorry! What was I thinking?























Ah...Xin lỗi! Tớ đang nghĩ gì thế nhỉ?

48
00:03:21,112-->00:03:28,513
Taking me to this dark and empty place...
You're really something Shidou-san.




















Đưa tớ vào chỗ vắng vẻ. 
Cậu muốn làm gì với tớ ở đây à?

49
00:03:30,112-->00:03:32,113
Y... you're wrong! That was not my intention!























C...cậu đùa à? Ý tớ không phải thế!

50
00:03:32,512-->00:03:35,113
You don't have to be ashamed, Shidou-san.























Cậu không cần phải xấu hổ, Shidou-san.

51
00:03:36,112-->00:03:38,113
I really just got lost!























Tớ bị lạc thật mà!

52
00:03:38,512-->00:03:42,113
If you really wished for it... I...
























Nếu cậu thực sự muốn điều đó thì... tớ có thể...

53
00:03:43,112-->00:03:45,113
I... uh... eh... huh!?























Ơ...tớ...hả...!?

54
00:03:46,112-->00:03:48,513
H... hey Kurumi, where are you touching?























N...này Kurumi, cậu đụng vào tớ à?

55
00:03:49,112-->00:03:53,113
Yes? I haven't done anything yet.























Hở? Tớ có làm gì đâu?

56
00:03:53,512-->00:03:55,113
Eh? But...























Ơ... nhưng mà...

57
00:03:58,112-->00:04:01,113
So there is a cat.























Ara, vậy là do con mèo đó.

58
00:04:02,112-->00:04:04,513
That's probably what you felt.























Nó vừa đụng vào cậu đấy.

59
00:04:06,112-->00:04:07,113
Th... that surpised me.























Làm tớ giật cả mình.

60
00:04:09,112-->00:04:14,513
Anyways, this cat... seems very attached
to humans... he's not trying to escape.




















Con mèo này...có vẻ rất thân thiện với người.
Nó không có ý chạy trốn gì cả.

61
00:04:17,112-->00:04:20,113
Kurumi, why don't you pat him too? It feels good.























Kurumi sờ vào nó thử đi. Dễ chịu lắm.

62
00:04:20,532-->00:04:21,513
Yes.























Vâng.

63
00:04:22,112-->00:04:24,513
Here. Looks like he wants you to join too.























Nè. Có vẻ như nó cũng muốn cậu đó.

64
00:04:26,112-->00:04:28,113
He really isn't afraid huh...























Nó không hề sợ hãi nhỉ.

65
00:04:29,512-->00:04:32,113
He's so small and soft.























Nó nhỏ nhắn và mềm mại quá.

66
00:04:33,112-->00:04:34,113
You look relaxed.























Cậu có vẻ thích thú nhỉ.

67
00:04:35,112-->00:04:39,513
But... is he alone? where did his family go?























Nhưng mà...nó ở đây một mình à? Gia đình nó đâu?

68
00:04:41,512-->00:04:47,113
Perhaps you're lost too? If so, you're
in the same situation as Shidou-san.




















Chắc nó cũng lạc giống cậu. Nếu vậy,
cậu có bạn đồng cảnh rồi, Shidou-san.

69
00:04:48,512-->00:04:50,113
Look, look, Shidou-san!























Nhìn nè, nhìn nè, Shidou-san!

70
00:04:51,112-->00:04:55,113
He's making a cute sound!























Nó kêu nghe dễ thương quá!

71
00:04:55,512-->00:04:56,513
That's right.























Ừ, phải rồi.

72
00:04:57,112-->00:05:06,113
Shidou-san, did you know? if you rub in places
where his hand can't reach, he'll be very happy... like this.


















Shidou-san, cậu biết không? Nếu cậu
để tay vào chỗ tay nó không thể với tới.
Nó sẽ rất thích đấy... Như vậy nè.

73
00:05:08,112-->00:05:09,113
Looks like he's enjoying it huh?























Trông nó rất thích thú đấy.

74
00:05:10,112-->00:05:15,113
It tickles... licking my finger won't taste good.























Nhột quá...Nó liếm tay tớ nữa nè.

75
00:05:16,112-->00:05:18,113
Haha, he must be hungry























Haha, chắc nó đang đói lắm.

76
00:05:19,112-->00:05:23,513
Oh, you're right! Isn't there something he can eat?























Ồ, cậu nói đúng! Có gì cho nó ăn không?

77
00:05:24,112-->00:05:25,513
Looks like you're having fun, Kurumi.























Trông cậu có vẻ thích thú lắm, Kurumi.

78
00:05:27,112-->00:05:30,513
No... just... how should I put it?























Không...chỉ là... tớ đưa nó cho cậu nhé?

79
00:05:32,113-->00:05:34,113
You're making this gentle smile.























Cậu vừa cười trông dễ thương lắm.

80
00:05:35,112-->00:05:37,113
So you can also make that face, huh? Kurumi























Nhìn kỹ trông cậu cũng dễ thương đấy chứ.

81
00:05:39,112-->00:05:40,513
W... what are you saying, Shidou-san?























C...Cậu đang nói gì vậy, Shidou-san?

82
00:05:41,512-->00:05:42,513
Do you like cat?























Cậu thích mèo à?

83
00:05:43,512-->00:05:45,113
W... well... this is...























Um...chỉ là...

84
00:05:46,512-->00:05:49,113
H... he happens to be here so I'm playing with him.























Nó xuất hiện ở đây nên tớ chơi với nó thôi.

85
00:05:49,512-->00:05:50,513
Why are you reacting like that?























Sao cậu lại tức giận như vậy?

86
00:05:51,113-->00:05:53,113
I'm perfectly normal!























Tớ bình thường mà!

87
00:05:54,112-->00:05:57,513
H... hey, if you raise your voice that much
you'll scare the cat.





















Này, nếu cậu nói lớn như vậy, con mèo sẽ sợ đấy.

88
00:05:59,112-->00:06:00,113
What's going on, Kurumi?























Gì vậy, Kurumi?

89
00:06:01,112-->00:06:03,113
The cat got inside my clothes...
























Con... con mèo nhảy vào trong áo tớ...

90
00:06:04,112-->00:06:06,113
Calm down! I can see many things...
























Bình tĩnh! Tớ chẳng thấy gì cả...

91
00:06:07,112-->00:06:14,113
W...wait... It tickles! Um... please catch him!
























Khoan... nhột quá...! Bắt nó ra giúp tớ với...!

92
00:06:14,512-->00:06:15,513
No no no no!
























Không không không không!

93
00:06:16,512-->00:06:19,113
Catch the cat? That would require...
























Bắt con mèo à? Chẳng lẽ tớ phải...

94
00:06:19,512-->00:06:24,113
Aaa! Hurry up! Or I'll just have to take my clothes off!
























Aaa... nhanh lên! Hay là tớ cởi áo ra vậy!

95
00:06:24,512-->00:06:26,113
Don't do that!























Đừng làm thế!

96
00:06:27,112-->00:06:29,113
S... Shidou-san... I'm already...























Shidou-san...tớ phải...cởi ra...

97
00:06:30,112-->00:06:34,113
Aaa... Don't take your clothes off! 
What if someone sees you?






















Aaa... Đừng cởi áo ra! Lỡ có ai thấy cậu thì sao?

98
00:06:34,812-->00:06:36,513
B... But... Aaaa!























Nhưng...mà...Aaaa!

99
00:06:37,112-->00:06:40,113
Alright! I'll catch it so please 
don't take your clothes off!






















Được rồi! Tớ sẽ bắt nó. Làm ơn đừng cởi áo ra!

100
00:06:41,112-->00:06:43,513
O... okay, please do it...























Được rồi! Làm đi!

101
00:06:45,112-->00:06:48,513
Um... your hand tickles...























Um... tay cậu... nhột quá...

102
00:06:49,512-->00:06:54,113
H... Hold it up! It'll be over soon! 
Or else I'll be a goner too!






















Từ từ! Xong ngay thôi! Tớ sẽ bắt nó ra!

103
00:06:55,512-->00:06:56,513
Alright, got it!
























Bắt được rồi!

104
00:06:57,512-->00:07:01,113
Aaaa.... Shidou-san, where are you touching?
























Aaaa.... Shidou-san, cậu sờ vào đâu vậy?

105
00:07:02,112-->00:07:06,113
T... that's not it Kurumi! 
That was just an accident! Sorry!






















X...xin lỗi Kurumi! Tớ vô ý thôi. Xin lỗi!

106
00:07:06,812-->00:07:10,113
if you're going to touch it, be gentle...
























Nếu cậu đụng nó, làm ơn đụng nhẹ thôi...

107
00:07:11,112-->00:07:13,113
W... what are you saying?























C... cậu nói gì vậy?

108
00:07:14,112-->00:07:15,113
The next one is...
























Tiếp theo là...

109
00:07:16,112-->00:07:19,113
Aaaa... Not that place!
























Aaaa... không phải chỗ đó.

110
00:07:20,112-->00:07:23,513
N... No... I was trying to catch the cat...
























K...không... Tớ đang cố bắt con mèo...

111
00:07:26,112-->00:07:32,113
What are you saying, Shidou-san?
The cat's been over there for a while...






















Cậu nói gì vậy, Shidou-san? Con mèo đang ở đây mà.

112
00:07:36,112-->00:07:37,113
S... Since when?
























T...từ khi nào vậy?

113
00:07:38,112-->00:07:42,113
Since you got your hand into my clothes.
























Từ khi cậu cho tay vào người tớ.

114
00:07:43,112-->00:07:45,513
Why was I even trying...
























Sao tớ vẫn cứ cố bắt nó...

115
00:07:46,112-->00:07:51,513
Ara, so you knew all that and still did this to me?
























Ara, vậy là cậu biết nhưng vẫn cố ý cho tay vào người tớ?

116
00:07:53,112-->00:07:56,113
T... That's not it! I wanted to save you!
























Không phải thế! Tớ muốn giúp cậu thôi!

117
00:07:58,112-->00:08:02,113
Do as you please until you're done.
























Cậu làm chẳng giống như cậu nói gì cả.

118
00:08:03,512-->00:08:05,113
I... I wouldn't do it in this place!
























Tớ không muốn làm thế ở đây đâu!

119
00:08:05,512-->00:08:08,113
You'd do it if we were in a different place?
























Vậy cậu sẽ làm thế khi ở chỗ khác à?

120
00:08:09,112-->00:08:10,113
T... That's not what I meant!
























Không phải thế!

121
00:08:12,112-->00:08:15,113
I don't mind if you keep doing it right now...
























Tớ không phiền nếu cậu tiếp tục làm thế ngay bây giờ đâu.

122
00:08:16,112-->00:08:22,513
Because if I'm alone with Shidou-san,
I can do whatever I want.




















Vì nếu tớ ở một mình với Shidou-san, 
tớ có thể làm bất kì điều gì tớ muốn.

123
00:08:24,112-->00:08:28,113
B... But you see... if I'm too late, they'll get worried.













N...nhưng... giờ trễ rồi. Mọi người sẽ lo lắng đấy.

124
00:08:29,112-->00:08:36,113
I want to talk more with you. 
I still want to know more about you












Nhưng tớ muốn nói chuyện nhiều hơn.
Tớ vẫn chưa biết nhiều về cậu.

125
00:08:37,112-->00:08:40,113
What are you saying? I haven't said 
anything about me yet...

















Cậu nói gì vậy? Tớ đã nói gì về tớ đâu...

126
00:08:41,112-->00:08:42,113
That's not true!













Không phải vậy!

127
00:08:44,112-->00:08:52,113
I still don't know your secret, but I got to
contemplate that embarrassed side of yours.



















Tớ vẫn không biết về bí mật của cậu, nhưng tớ 
đã được chiêm ngưỡng vẻ mặt xấu hổ của cậu.

128
00:08:56,112-->00:09:00,113
Well... I'm also satisfied that I got to see you today.













Mà... tớ cũng đã được chiêm ngưỡng 
vẻ mặt dễ thương của cậu đấy.

129
00:09:02,512-->00:09:06,513
Um... C... Cute things are cute, so it is ok!













Um...dễ thương gì chứ! Không có đâu!

130
00:09:07,512-->00:09:08,513
You're right.













Thật vậy sao?

131
00:09:12,112-->00:09:15,113
Well, what are we going to do with this guy?

















Vậy...giờ chúng ta làm gì với con mèo đây?

132
00:09:16,112-->00:09:17,113
What do you think, Kurumi?

















Cậu thấy sao, Kurumi?

133
00:09:18,112-->00:09:20,113
Eh, me?

















Eh, tớ à?

134
00:09:20,512-->00:09:22,113
Want to search for his parents?

















Muốn đi tìm gia đình con mèo này không?

135
00:09:25,112-->00:09:29,513
Well, if you really want to do it that badly,
I'll accompany you.

















Um... Nếu cậu muốn vậy thì tớ sẽ giúp cậu.

136
00:09:30,112-->00:09:33,513
That's right. I want to find them. 
I really want to find them.

















Đúng vậy. Tớ muốn tìm gia đình nó. 
Tớ rất muốn tìm gia đình nó.

136
00:09:36,112-->00:09:39,113
If you're going that far then, I guess I have no choice.

Nếu cậu muốn thì tớ sẽ giúp thôi.

137
00:09:40,112-->00:09:46,113
So it's decided. You carry him, Kurumi.
He seems more attached to you anyway.

Quyết định vậy đi. Cậu bế nó đi, Kurumi.
Nó có vẻ thích cậu hơn đấy.

138
00:09:46,512-->00:09:48,113
Alright.

Được rồi.

139
00:09:50,112-->00:09:56,113
Here, here. It's okay... we'll find them soon.

Được rồi. Được rồi. Sẽ ổn thôi.
Chúng ta sẽ tìm được chúng sớm thôi.

140
00:09:58,112-->00:09:59,513
Ok, let's go!

Nào, đi thôi!

141
00:10:04,112-->00:10:07,113
Haa... We're finally back in a familiar place.

Cuối cùng chúng ta cũng tìm được chỗ 
của gia đình con mèo đó.

142
00:10:07,512-->00:10:12,113
Yes... I'm glad we found that cat's parents.



Ừ... thật may là chúng ta đã tìm thấy 
bố mẹ con mèo đó. 

143
00:10:13,112-->00:10:15,113
You're so gentle, Kurumi.


Cậu tốt bụng thật, Kurumi.

144
00:10:16,112-->00:10:20,113
Y... you said you wanted my help and that's why I did it.

C... cậu nói cậu muốn giúp thì tớ giúp thôi.

145
00:10:20,512-->00:10:24,513
And there's also that teary face you made
when we found his parents...


Khi chúng ta tìm thấy gia đình con mèo đó, 
cậu cũng có vẻ mặt dễ thương như vậy.

146
00:10:25,112-->00:10:26,113
That's not true!

Không phải vậy!

147
00:10:27,112-->00:10:29,113
If you say so.

Vậy thôi vậy.

148
00:10:31,112-->00:10:34,513
I can't figure out what is she thinking about
but she was also this gentle huh.

Mình không biết cô ấy đang nghĩ gì, nhưng mà
cô ấy cũng dễ thương đấy chứ.

149
00:10:36,112-->00:10:37,113
Did you say something?

Cậu nói gì vậy?

150
00:10:38,112-->00:10:41,113
No, I didn't. I was just kind of thirsty.



Không. Có gì đâu. Tớ chỉ hơi khát thôi.

151
00:10:43,112-->00:10:44,113
That's right.

Ừ... cũng đúng.

152
00:10:45,112-->00:10:48,113
Want to go bye something? I'll let you
pick whatever you want.

Muốn mua gì không? Tớ sẽ mua bất kì thứ gì cậu muốn.

153
00:10:48,512-->00:10:51,113
Then, let's do that.

Vậy mua nước đi.

154
00:10:53,112-->00:10:56,113
There's also a bench over there.
So want to take a break here?

Có một cái băng ghế ở đó. Ra đó ngồi đi.

155
00:11:41,112-->00:11:42,113
Haa... refreshing

Haa... Thoải mái quá.

156
00:11:44,112-->00:11:45,113
What's wrong?

Gì vậy?

157
00:11:46,112-->00:11:52,113
You were avoiding me all this time, but you're
totally defenseless right now.

Cậu lúc nào cũng muốn tránh khỏi tớ,
nhưng hiện giờ cậu lại không hề đề phòng tí nào.

158
00:11:53,912-->00:11:58,813
Don't worry, I have no intention to do anything right now.

Đừng lo, tớ không làm gì bây giờ đâu.

159
00:11:00,112-->00:11:01,113
Alright.

Được rồi.

161
00:11:03,112-->00:11:05,213
You really went straight to believe my words huh?

Cậu thực sự tin những gì tớ nói sao?

162
00:11:06,112-->00:11:10,113
If you really planned to do something, you would've
done it when we were alone right?

Nếu cậu muốn làm gì thì cậu đã làm 
lúc chúng ta ở một mình rồi.

163
00:11:11,112-->00:11:15,113
Yeah... well... it's not like nothing happened either...

Ừ...mà...nó không giống những gì đã xảy ra...

164
00:11:16,612-->00:11:22,113
That's right! When I finally get 
the perfect chace... what have I done...

Phải rồi! Tớ đã có một cơ hội tốt. Tớ phải làm gì nhỉ?

165
00:11:22,312-->00:11:23,513
Hey, hey.

Này, này.

166
00:11:24,112-->00:11:27,613
I'm joking, don't worry about it.

Đùa thôi, tớ không làm gì đâu.

167
00:11:28,112-->00:11:29,613
Is that really a joke?

Cậu đùa thật không đó?

169
00:11:31,112-->00:11:33,113
(But it does look like she's regretting something...)

(Nhưng có vẻ cô ấy đang tiếc gì đó...)

170
00:11:35,112-->00:11:36,613
Thanks.

Cảm ơn nhé.

171
00:11:37,812-->00:11:40,113
You drank it that fast, were you really that thirsty?

Cậu uống nhanh nhỉ. Cậu có vẻ khát lắm à?

172
00:11:41,112-->00:11:43,213
We were running a bit too much today.

Chúng ta đi đường xa mà.

173
00:11:44,512-->00:11:47,213
Well, you were always playing with the cat while
we were looking for her parents anyways.

Cậu luôn chơi với con mèo 
khi chúng ta tìm gia đình nó mà.

174
00:11:48,112-->00:11:51,413
That's... he was following me 
so I didn't have much of a choice.

Đó là... tại nó đi theo tớ nên tớ chơi với nó thôi.

175
00:11:51,812-->00:11:53,113
Okay. Okay.

Được rồi. Được rồi.

176
00:12:54,112-->00:12:56,113
Here, you can drink mine if you want.

Nè, nếu cậu muốn thì uống của tớ cũng được.

177
00:12:57,112-->00:12:01,113
Don't really think I should take it, 
but I'll accept your offer.

Đừng nghĩ tớ muốn uống, 
nhưng tớ chấp nhận lời mời của cậu.

178
00:12:04,112-->00:12:06,113
What is it? You don't like it?

Sao vậy? Cậu không thích à?

179
00:12:08,112-->00:12:10,113
What's so funny, Kurumi?

Cậu cười gì vậy, Kurumi?

180
00:12:12,112-->00:12:14,513
This makes it... a indirect kiss right?

Đây là... hôn gián tiếp à?

181
00:12:15,812-->00:12:17,113
I... Indirect?

Hôn...gián tiếp?

182
00:12:17,712-->00:12:19,113
I didn't offer you my drink with that intention!

Tớ không mời cậu vì ý đó!

183
00:12:20,112-->00:12:25,113
Ara, is that so? I really thought you were well aware.

Ara, vậy sao? Tớ nghĩ cậu biết trước chuyện này rồi.

184
00:12:25,312-->00:12:30,113
Of course not! That's right, give me that back! 
I'll buy you a new one!

Không phải! Được rồi, trả đây! Tớ sẽ mua cái mới!

185
00:12:33,112-->00:12:35,113
Ara... what an honor.

Ara... Sĩ diện thế.

186
00:12:41,712-->00:12:44,113
You're like a kid after all...

Cậu trông cứ như đứa trẻ ấy nhỉ.

187
00:12:45,112-->00:12:48,113
I didn't know you were this much of an amusing person.

Tớ không biết là cậu vui tính đến vậy đấy.

188
00:12:49,112-->00:12:50,813
Sorry then. For being a kid.

Xin lỗi... về việc tớ như trẻ con.

189
00:12:51,212-->00:12:54,813
Don't take it like that, I'm praising you.

Đừng nghĩ vậy, tớ khen cậu mà.

190
00:12:55,112-->00:12:57,113
It doesn't feel like it.

Nghe chả giống vậy tí nào.

191
00:12:58,112-->00:13:02,113
And like this we have both successfully
exchanged an indirect kiss right?






Và chúng ta đều đã có nụ hôn 
gián tiếp rồi phải không?

192
00:13:04,512-->00:13:07,513
A... Kurumi... that's...!


A... Kurumi... đó là...!

193
00:13:08,112-->00:13:11,313
Was this one on purpose or premeditated?


Đây là mục đích hay là 
đã tính toán từ trước của cậu vậy?

194
00:13:11,712-->00:13:14,113
It wasn't on purpose nor premeditated!

Không phải mục đích hay đã tính toán gì cả!

195
00:13:15,112-->00:13:16,913
It was an accident!

Chỉ là tai nạn thôi!

196
00:13:18,112-->00:13:22,113
If it were an accident you'd be making fun of me right?

Nếu đó chỉ là tai nạn, vậy cậu 
đang làm tớ vui phải không?

197
00:13:23,512-->00:13:25,813
I can't say anything to that.

Tớ không nói thế.

198
00:13:26,412-->00:13:28,513
So, how was it?

Vậy...nó thế nào?

199
00:13:29,112-->00:13:30,113
How was it...?

Gì cơ?

200
00:13:31,112-->00:13:35,113
Of course... the taste of my kiss.

Dĩ nhiên là... vị của nụ hôn của tớ ấy.

201
00:13:35,212-->00:13:38,813
Well... even if you ask me... I don't know.

Um... cậu hỏi vậy... tớ không biết.

202
00:13:40,112-->00:13:47,113
Ara! What a waste! In that case, the next one will
be a direct one. Want to taste it?

Ara! Lãng phí quá! Vậy tiếp theo sẽ là
một nụ hôn trực tiếp nhé! Muốn thử không?

203
00:13:47,512-->00:13:52,113
Huh? It's not that... but...

Hả? Không được... nhưng mà...

204
00:13:53,112-->00:13:56,113
(If I go ahead and kiss her right now will I be
able to seal her powers?)

(Nếu mình hôn cô ấy bây giờ, biết đâu mình
sẽ phong ấn sức mạnh của cô ấy?)

205
00:13:58,112-->00:14:00,113
(Có thể lắm chứ.)

206
00:14:23,112-->00:14:24,113
(Không! Khoan đã...)

207
00:14:26,112-->00:14:30,113
Ngạc nhiên làm sao, cậu không muốn hôn tớ à?

208
00:14:31,112-->00:14:34,113
Không... thực ra tớ muốn...

209
00:14:35,112-->00:14:38,113
Gì vậy? Điện thoại của tớ?

210
00:14:39,112-->00:14:45,113
Ara, tiếc thật đấy. Ai đó đã phá mất 
khoảnh khắc đẹp đẽ này rồi.

211
00:14:46,112-->00:14:48,113
Đúng là tiếc thật.

212
00:14:50,112-->00:14:51,513
Tin nhắn của Kotori à?

213
00:14:54,112-->00:14:59,113
Aaa! Tệ thật! Tớ phải về làm bữa tối rồi.

214
00:15:00,112-->00:15:02,113
Con bé chắc đang bực mình lắm.

215
00:15:03,112-->00:15:09,113
Ara ara, có tệ lắm không? Tớ có nên đến xin lỗi không?

216
00:15:10,112-->00:15:12,113
Cậu làm thế còn tệ hơn.

217
00:15:12,512-->00:15:14,113
Ara, vậy à?

218
00:15:15,512-->00:15:19,113
Tớ muốn chứng minh cho Shidou-san thấy 
sự trong sạch của tớ.

219
00:15:20,112-->00:15:22,113


Thôi...chắc lần khác đi.

220
00:15:23,112-->00:15:25,113
Cậu vừa hứa đấy nhé, Shidou-san.

221
00:15:27,112-->00:15:28,513
Tớ sẽ đợi đó.

222
00:15:29,112-->00:15:30,113
Y... Yes...























Ừ... ừ...

223
00:15:31,112-->00:15:37,113
In that case, it is also about time for me to leave.























Cũng đến lúc tớ phải đi rồi.

224
00:15:38,112-->00:15:42,113
Ara? Do you want to spend more time with me?























Ara? Cậu muốn ở bên tớ thêm nữa à?

225
00:15:43,112-->00:15:43,813
That's not it!























Không phải vậy!

226
00:15:44,512-->00:15:46,113
I know.























Tớ biết.

227
00:15:47,112-->00:15:51,113
It wasn't something that important, but it was really fun.























Mặc dù chẳng có gì quan trọng nhưng hôm nay thật vui. 

228
00:15:52,112-->00:15:54,113
So I'll retreat for today.























Vậy nên tớ mong sẽ có dịp gặp lại.

229
00:15:55,112-->00:15:56,113
I see.























Được rồi.

230
00:15:57,112-->00:16:02,113
I really hope we meet again so we can walk
in the city alone like this one more time.




















Tớ hy vọng chúng ta sẽ gặp lại
và đi dạo cùng nhau thêm lần nữa.

231
00:16:03,112-->00:16:05,113
I can't feel that good when you're being this honest.























Tớ cũng hy vọng vậy.

232
00:16:06,112-->00:16:08,513
Let's meet again, Shidou-san.























Gặp sau nhé, Shidou-san

233
00:16:09,112-->00:16:11,113
Y... Yes, see you later.























Ừ... gặp lại sau.

234
00:16:14,112-->00:16:20,113
A lot of things happened but I got to understand
Kurumi a little bit more. Isn't that enough?




















Nhiều chuyện đã xảy ra nhưng mình cũng đã 
hiểu hơn về Kurumi một chút. Như vậy đã đủ chưa nhỉ?

235
00:16:21,112-->00:16:24,113
Shidou-san! I forgot to tell you!























Shidou-san! Tớ quên nói với cậu.

236
00:16:25,112-->00:16:27,113
Oh? What? What happended?























Sao? Chuyện gì vậy?

237
00:16:27,512-->00:16:32,113
Tomorrow, I'll definitely be listening 
to that secret of yours






















Ngày mai, tớ chắc chắn sẽ nghe bí mật của cậu.

238
00:16:33,112-->00:16:34,113
I told you I had no intention to...























Tớ đã nói tớ không có ý định...

239
00:16:36,112-->00:16:39,513
W... Wait Kurumi! What do you mean with "tomorrow"?























Khoan đã, Kurumi! Cậu nói "ngày mai" là sao?

240
00:16:42,112-->00:16:45,113
See you tomorrow then, Shidou-san!
























Gặp cậu ngày mai nhé, Shidou-san!



