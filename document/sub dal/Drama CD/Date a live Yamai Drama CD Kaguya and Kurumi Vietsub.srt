﻿0
00:00:05,112-->00:00:08,113
Yuzuru chết tiệt. 
Cô lại chế giễu tôi nữa.

1
00:00:09,112-->00:00:13,513
Tôi không hiểu có gì khác giữa
"Con Mắt Nguyền Rủa" và "Ngọn Thương Bão Tố" nữa.

2
00:00:14,112-->00:00:16,113
Tôi chẳng hiểu gì cả.

3
00:00:16,512-->00:00:19,513
Nó nghe không hay sao?

4
00:00:20,112-->00:00:24,113
Không, không! Không thể thế được! 
Không thể được!

5
00:00:25,112-->00:00:29,113
"Hãy triệu hồi cuồng phong, 
hỡi Ngọn Thương Bão Tố"

6
00:00:31,112-->00:00:35,113
Quả thật mình không nghĩ ra 
thứ gì hay hơn cả.

7
00:00:36,112-->00:00:39,113
Nhưng giờ nghĩ về nó,

8
00:00:39,512-->00:00:43,513
Có lẽ vấn đề là do mình không có
kỹ thuật phù hợp với tên của mình.

9
00:00:44,112-->00:00:50,113
Mình gọi "Con Mắt Nguyền Rủa", 
nhưng tên đó có chính xác không?

10
00:00:50,512-->00:00:54,513
Nhưng mình muốn trả thù 
Yuzuru bằng cách nào đó.

11
00:00:55,112-->00:01:00,513
Từ khi Yuzuru bái Origami làm sư phụ, 
mình cảm thấy mình cũng cần ai đó.

12
00:01:01,112-->00:01:07,113
Quả nhiên có người hướng dẫn
thì sẽ dễ dàng hơn nhiều.

13
00:01:07,512-->00:01:12,113
Nhưng ai có thể chỉ mình nhỉ?

14
00:01:13,112-->00:01:17,513
Hở? Đó là...

15
00:01:19,512-->00:01:22,113
Đúng thật là, những chuyện này...

16
00:01:22,512-->00:01:25,113
Mình thấy người cần tìm rồi!

17
00:01:25,512-->00:01:28,513
Cái... cái gì vậy? Cô... là Yamai à...

18
00:01:29,112-->00:01:31,113
Kaguya! Sư phụ của đệ!

19
00:01:31,512-->00:01:36,113
Sư... Sư phụ? Chính xác là
đang xảy ra chuyện gì vậy?

20
00:01:36,312-->00:01:37,513
Con Mắt Nguyền Rủa!

21
00:01:38,112-->00:01:42,513
Những gì đệ tử thiếu chính là
sức mạnh của sư phụ đấy!

22
00:01:43,112-->00:01:49,113
Kurumi, không, Khoảng Không Bóng Tối, 
Người Giữ Con Mắt Ác Quỷ, Time Ruler!

23
00:01:50,112-->00:01:57,513
Hở? Tôi không hiểu cô đang nói gì, 
nhưng gây chú ý thế này không phải ý hay đâu.

24
00:01:58,112-->00:02:00,113
Giờ tôi phải đi rồi.

25
00:02:00,512-->00:02:01,913
Vâng, sư phụ của đệ!

25
00:02:02,112-->00:02:06,113
Tại sao cô cứ gọi tôi là "sư phụ" vậy?

25
00:02:08,112-->00:02:12,113
Theo đệ nghĩ, sư phụ có phong cách
mà đệ còn thiếu, thưa sư phụ.

25
00:02:12,512-->00:02:16,513
Sư phụ sẽ hoàn thiện đệ giống như 
gắn lại mảnh gương vỡ vậy.

25
00:02:17,112-->00:02:19,513
Eto... Kaguya-san?

25
00:02:20,112-->00:02:25,913
"Ngươi cần một vài gợi ý về kỹ năng hành động. 
Ngươi chưa muốn dừng lại phải không?"

25
00:02:26,112-->00:02:28,113
Đúng thế đấy!

25
00:02:28,512-->00:02:30,113
Đó chính là cái đệ muốn!

25
00:02:30,512-->00:02:31,913
Đệ cũng đang muốn nói như thế!

25
00:02:32,112-->00:02:37,513
Nếu tôi không nhầm thì 
cô đang chế giễu tôi à?

25
00:02:37,912-->00:02:40,113
Sư phụ nhầm rồi, thưa sư phụ.

25
00:02:40,312-->00:02:44,913
Những gì đệ còn thiếu là sức hút trong 
Con Mắt Nguyền Rủa của đệ.

25
00:02:45,112-->00:02:47,913
Sư phụ có tất cả những điều đó.

25
00:02:48,112-->00:02:54,513
Vì vậy, đệ muốn biết 
mình phải làm gì, thưa sư phụ.

25
00:02:55,112-->00:03:00,113
Những gì cô còn thiếu?
Cô cần phải làm gì à?

25
00:03:00,512-->00:03:06,113
Đúng vậy. Đệ không thể hoàn thiện 
nếu không có sư phụ.
Sư phụ chính là phần thiếu sót mà đệ cần

26
00:03:06,512-->00:03:12,913
Đó là những gì mà đệ đang tìm kiếm. 
Đệ chắc chắn rằng cuộc gặp 
của chúng ta là do số phận sắp đặt.

26
00:03:13,512-->00:03:15,513
Vậy cô muốn tôi làm gì?

26
00:03:16,112-->00:03:20,113
Không cần. Sư phụ không cần làm gì đâu.

26
00:03:20,512-->00:03:25,113
Đệ sẽ theo sư phụ như một cái bóng
và tìm ra thứ đệ cần thôi.

26
00:03:26,112-->00:03:30,513
Tôi hiểu rồi. Tôi sẽ làm như cô nói.

26
00:03:31,113-->00:03:32,913
Nhưng chỉ ngày hôm nay thôi đấy.

26
00:03:33,313-->00:03:36,513
Vâng. Rất cảm ơn người, thưa sư phụ.

26
00:03:37,112-->00:03:40,113
Chuyện này có vẻ phức tạp đấy.

26
00:03:40,512-->00:03:47,113
Mà mình cũng chỉ cần đợi cô ta
không chú ý là bỏ trốn thôi.

26
00:03:47,512-->00:03:54,113
À mà sư phụ, người có hay đi 
qua đây thường xuyên không?

26
00:03:54,512-->00:03:57,513
Thường xuyên à? Nếu tôi tiết lộ
chuyện đó ra thì thật ngu ngốc đấy, Kaguya-san.

26
00:03:58,112-->00:04:01,113
Bời vì... Tôi luôn có mặt ở mọi nơi mà.

27
00:04:04,512-->00:04:07,113
Th... Thật ngầu...

28
00:04:08,113-->00:04:10,513
Mình cũng muốn có mặt ở khắp mọi nơi.

29
00:04:11,112-->00:04:12,913
Cô là đứa con gái kỳ lạ nhỉ?

30
00:04:13,112-->00:04:18,113
Không, không được.
Đệ không bao giờ tìm được người cả, sư phụ.

31
00:04:19,112-->00:04:22,113
Cô hiểu nhầm rồi.

32
00:04:23,112-->00:04:27,113
Tôi... "Chúng tôi" đang ở mọi nơi.

33
00:04:27,312-->00:04:33,113
Sâu trong bóng tối... trong vương quốc 
của nơi mà ánh sáng không thể chạm vào...
"Chúng tôi" liên kết với mọi thứ.

34
00:04:36,112-->00:04:38,513
Nó thật là ngầu!! Quá ngầu luôn!!!

35
00:04:39,112-->00:04:43,913
Cô ăn phải thứ gì à?
Cô đang hành động lạ lắm đó.

36
00:04:44,112-->00:04:46,913
Đệ tử ổn! Không có gì đâu.

37
00:04:47,113-->00:04:50,513
Nhưng mà, điều đó rất hữu ích đấy.

38
00:04:51,112-->00:04:53,113
Đệ nên ghi chép nó lại. 

39
00:04:53,512-->00:05:01,513
"Ta ở khắp mọi nơi, ở tất cả
những nơi mà gió có thể tới".

40
00:05:02,113-->00:05:04,513
Cô đúng là đứa con gái kỳ lạ nhỉ?

41
00:05:06,913-->00:05:12,113
Hình ảnh của đệ đang được cải thiện 
từng chút từng chút một. 
Đúng như đệ kỳ vọng với sư phụ.

42
00:05:12,313-->00:05:16,913
Gần 1 tiếng trôi qua rồi mà  
tôi không hiểu sao cô không thấy mệt nhỉ.

43
00:05:17,112-->00:05:21,113
Sao đệ tử lại mệt mỏi khi đang được học 
rất nhiều thứ từ cuộc nói chuyện của chúng ta chứ.

39
00:05:21,512-->00:05:24,513
Đệ sẽ không như đệ của ngày hôm qua đâu

40
00:05:25,113-->00:05:27,513
Cô nên dùng thời gian của cô cẩn thận hơn đi.

41
00:05:28,113-->00:05:31,913
Một người bình thường không thể lấy lại 
thời gian đã mất đâu.

42
00:05:32,113-->00:05:39,113
Trừ khi cô có cách để đánh cắp thời gian như tôi.
cô nên làm tốt việc sử dụng thời gian cô có đi.

43
00:05:41,112-->00:05:43,513
Câu đó cũng thật tuyệt!

39
00:05:44,912-->00:05:49,113
Nhưng nếu đệ không phải là Kẻ Cắp Thời Gian 
thì đệ không thể nói như vậy.

40
00:05:49,513-->00:05:52,913
Có cách nói nào phù hợp với mình không nhỉ?

41
00:05:53,113-->00:05:55,513
Cô có vẻ không biết thất vọng là gì nhỉ?

42
00:05:56,113-->00:06:01,113
Lời khen ngợi của sư phụ
chính là thứ đệ tử mong muốn đấy.

43
00:06:01,512-->00:06:05,113
Mình thật sự muốn kết thúc
buổi nói chuyện này ngay bây giờ...

44
00:06:06,112-->00:06:13,113
Sư phụ, tại sao người lại cột bím tóc
một bên dài một bên ngắn vậy?

45
00:06:13,312-->00:06:16,513
Chắc là vì nó tượng trưng cho 
kim giờ và kim phút của đồng hồ chăng?

46
00:06:18,112-->00:06:22,113
Tuyệt vời! Nó thật tuyệt vời!

47
00:06:22,512-->00:06:25,113
Sao tôi chẳng cảm thấy mình đang được khen nhỉ?

48
00:06:25,512-->00:06:29,113
Không, những chi tiết nhỏ nhưng vẫn rất quan trọng.

49
00:06:29,312-->00:06:35,113
Nó giống như một người viết tiểu thuyết 
đang ở trên đỉnh cao của anh ấy! 
Thật đáng kinh ngạc!

50
00:06:35,512-->00:06:40,113
V... Vậy à? Nếu cô nói vậy thì 
tôi nghĩ nó không tệ lắm...

51
00:06:40,312-->00:06:45,913
À mà sư phụ, đệ nghe nói người sử dụng
sức mạnh bằng cách tự bắn mình.

52
00:06:46,112-->00:06:49,913
Từ khi Shidou và những người khác thấy nó, 
tôi đã không sử dụng nó nữa.

53
00:06:50,112-->00:06:54,113
Nhưng tôi sẽ không bình luận gì 
về sức mạnh của mình đâu.

54
00:06:54,312-->00:06:56,513
Quá ngầu luôn!!

55
00:06:57,112-->00:06:59,513
Kích hoạt sức mạnh với một cây súng!

56
00:07:00,113-->00:07:02,913
Người ta gọi đó là "Sự lãng mạn tinh khiết"!

57
00:07:03,112-->00:07:06,113
Đệ muốn sử dụng nhiều kỹ năng khác quá!

58
00:07:06,512-->00:07:11,113
Dường như cô đang sẵn sàng có một kỹ năng 
sử dụng cho nhiều tình huống nhỉ.

59
00:07:12,112-->00:07:17,113
Bao bọc trong gió, 
đệ có khả năng bay lên bầu trời và tấn công!

60
00:07:17,512-->00:07:18,912
Hoặc đại loại thế...

41
00:07:19,112-->00:07:23,913
Nó còn thể sử dụng như một cách phòng thủ
và có thể vận chuyển thứ khác.

42
00:07:24,112-->00:07:26,113
Đúng là vậy nhưng...

43
00:07:27,112-->00:07:29,913
Phải rồi!

44
00:07:30,112-->00:07:32,513
Mình đặt cược tất cả vào cái tên này!

45
00:07:32,713-->00:07:36,913
Không... Tôi nghĩ cái tên không phải vấn đề...

46
00:07:37,112-->00:07:39,113
Không, nó phải như vậy.

47
00:07:39,513-->00:07:42,113
Cái tên mang sức mạnh và linh hồn của đệ tử.

48
00:07:42,512-->00:07:44,113
V... Vậy à?

49
00:07:45,112-->00:07:49,513
Đệ đã bắt đầu hiểu ra rồi.
Sư phụ, đệ tử rất biết ơn sư phụ đấy.

50
00:07:50,112-->00:07:53,113
Không có gì.

51
00:07:54,113-->00:07:56,113
"Lá Chắn Gió"

52
00:07:56,513-->00:07:59,113
Không, nghe đơn giản quá.

53
00:08:00,112-->00:08:02,513
"Người bảo vệ vô hình" nghe hay hơn.

54
00:08:03,112-->00:08:12,513
Và khi mình đang di chuyển thì 
"Khu vườn trên không" nghe có vẻ hay hơn.

55
00:08:13,112-->00:08:17,113
Nhưng mình nghĩ tốt hơn là 
gọi theo phong cách phép thuật...

56
00:08:18,112-->00:08:22,913
Mình cần đặt tên cẩn thận lại.

57
00:08:23,112-->00:08:25,513
Đ... Đúng vậy... Cố gắng lên nhé...

58
00:08:26,112-->00:08:27,913
Vậy giờ tôi đi đây...

59
00:08:28,112-->00:08:29,913
Đợi đã, Sư phụ!

60
00:08:30,113-->00:08:32,113
Cho đệ tử thêm chút thời gian nữa đi.

61
00:08:32,312-->00:08:36,113
Đệ tử cần mượn sự hiểu biết của người 
cho những việc rất quan trọng.

62
00:08:36,312-->00:08:39,113
Những việc quan trọng? Nó là gì vậy?

63
00:08:39,313-->00:08:42,513
Như đệ đã nói. Đệ muốn học những
kỹ năng hành động!

64
00:08:43,112-->00:08:44,113
Gì cơ?

65
00:08:44,512-->00:08:49,113
Sử dụng sức mạnh của mình 
với mục đính rõ ràng.

66
00:08:49,312-->00:08:53,113
Đó là cách đệ muốn bộc lộ 
sức mạnh đẳng cấp của đệ.

67
00:08:53,513-->00:08:55,513
Đó là thứ đệ luôn ao ước.

68
00:08:56,113-->00:08:58,113
Việc đó đâu cần đến tôi...

69
00:08:58,513-->00:09:01,113
Không được! Không có sư phụ là không được!

70
00:09:01,512-->00:09:04,513
Sự hiểu biết rộng, vẻ đẹp và cách thể hiện.

71
00:09:05,112-->00:09:08,913
Người là ánh sáng chân lý của đệ, thưa sư phụ!

72
00:09:09,112-->00:09:10,913
Chân lý?

73
00:09:11,113-->00:09:14,513
Nếu cô đã năn nỉ thì tôi cũng
không còn lựa chọn nào khác.

74
00:09:15,112-->00:09:19,513
Mì... Mình đang làm gì thế này? 
Cô ta không cho mình từ chối.

75
00:09:20,112-->00:09:23,113
Có lẽ nào, mình đang bắt đầu 
cảm thấy thích thú chuyện này...

76
00:09:23,312-->00:09:25,513
Không, không thể có chuyện đó được!

77
00:09:26,112-->00:09:33,113
Quan sát tính chất sức mạnh mới của đệ
và khám phá chuyện gì sẽ xảy ra 
khi đệ được đào tạo đi, sư phụ! 

78
00:09:34,112-->00:09:35,913
Thổi lên đi! Cơn giận của gió!

79
00:09:36,113-->00:09:37,913
Thức tỉnh đi! Bão tố!

80
00:09:38,113-->00:09:40,112
Ngọn Thương Bão Tố!

81
00:09:41,512-->00:09:44,113
Có vẻ như cô đang thiếu gì đó.

82
00:09:45,512-->00:09:47,113
Đệ thiếu thứ gì cơ?

83
00:09:47,312-->00:09:54,113
Để xem... Cô thử làm cụ thể 
những gì cô tưởng tượng xem.

84
00:09:54,512-->00:09:59,113
Đầu tiên, cô hãy điều khiển cơn gió 
xung quanh đối thủ mà đối thủ không biết.

85
00:09:59,312-->00:10:02,113
Tiếp theo, bí mật hạn chế 
di chuyển của đối thủ.

86
00:10:02,513-->00:10:03,913
Đệ hiểu rồi!

87
00:10:04,113-->00:10:08,113
Sau đó, cho đối thủ một ít dấu hiệu 
để đối thủ chú ý...

88
00:10:09,113-->00:10:11,513
"Giờ ngươi mới nhận ra ư?"

89
00:10:12,113-->00:10:15,913
"Ngươi đã lọt vào trận địa gió của ta rồi!"

90
00:10:16,113-->00:10:17,913
"Với Mê Cung Bão Tố!"

91
00:10:18,113-->00:10:21,113
Tuyệt thật! Đệ muốn thực hiện nó ngay!

92
00:10:21,313-->00:10:25,113
Luyện thuần thục và thực hiện nó 
trước khi đối thủ nhận ra.

93
00:10:25,312-->00:10:27,513
Đó là điểm quan trọng.

94
00:10:28,113-->00:10:31,113
Hiện giờ, chúng ta đã thành thạo 
nó rồi phải không?

95
00:10:31,313-->00:10:33,913
Vâng! Vâng! Đó là những gì đệ đang thiếu sót!

96
00:10:34,113-->00:10:36,113
Bây giờ thì... Con Mắt Nguyền Rủa.

97
00:10:37,112-->00:10:39,912
Hmm... Nếu cô có thể cảm thấy 
sự di chuyển của gió,

98
00:10:40,112-->00:10:43,913
hãy thay đổi hướng gió để gây trờ ngại 
với mục đích hạ gục đối thủ.

99
00:10:44,112-->00:10:45,913
Và nắm bắt di chuyển của đối thủ.

100
00:10:46,112-->00:10:47,113
Sau đó...

101
00:10:47,513-->00:10:50,113
"Không cần phải né tránh đâu."

102
00:10:50,313-->00:10:55,113
"Hãy cảm ơn ta đã cho người thấy
Con Mắt Nguyền Rủa đi."

103
00:10:55,312-->00:10:56,113
Cô thấy thế nào?

104
00:10:56,312-->00:11:02,113
Thật phi thường đấy, sư phụ!
Đệ biết đệ có thể tin tưởng vào người mà!

106
00:11:02,313-->00:11:05,113
Khi cơn gió mà cô điều khiển trở nên vô hình,

107
00:11:05,313-->00:11:07,113
Cô có thể khai thác tối đa
khả năng của nó mà phải không?

108
00:11:07,312-->00:11:09,513
Ghi chép... Phải ghi chép lại mới được!

109
00:11:10,112-->00:11:12,913
Quả đúng như kì vọng sự phụ của mình.

110
00:11:13,112-->00:11:17,113
Những từ này quý như vàng vậy.

111
00:11:17,513-->00:11:21,113
Vậy người có thể trả lời 
câu hỏi cuối cùng không?

108
00:11:21,313-->00:11:23,113
Được, được. Gì vậy?

109
00:11:23,513-->00:11:27,113
Là về sự di chuyển của đệ, Ngọn Thương Bão Tố.

110
00:11:28,113-->00:11:33,113
Yuzuru ngốc lúc nào chê bai
khi xem cách đệ di chuyển.

111
00:11:33,513-->00:11:36,113
Vậy cô có thể cho tôi xem lại không?

112
00:11:36,513-->00:11:42,513
Ừ! Được rồi! 
Đây là... Ngọn Thương Bão Tố của ta!

108
00:11:43,113-->00:11:50,113
Tôi hiểu rồi. Cô cần phải 
đưa tay phải lên và hét lớn từ "Bão Tố"....

109
00:11:50,313-->00:11:52,113
"Bão Tố"! Vậy à?

111
00:11:52,313-->00:11:54,913
Phải, như vậy đó.

112
00:11:55,112-->00:11:59,113
Và còn chi tiết quan trọng là cô nên 
rung chuyển mái tóc và quần áo trong cơn gió.

112
00:11:59,312-->00:12:01,113
Cuối cùng cô hô to... 

112
00:12:01,512-->00:12:03,113
"Ngọn Thương Bão Tố"!

113
00:12:04,113-->00:12:07,113
Ra vậy. Ra là vậy.

1
00:12:08,112-->00:12:10,113
"Lên đi! Ngọn Thương Bão Tố!"

1
00:12:10,313-->00:12:15,113
Đúng. Đúng. Đó là cách cô cần làm. 
Cô cũng cần cải thiện cách nói đi.

1
00:12:15,312-->00:12:18,113
"Cho chúng thấy đi, Ngọn Thương Bão Tố!" 
Như vậy đấy.

1
00:12:18,312-->00:12:20,113
Đệ cũng muốn thử sử dụng từ "bóng đêm".

1
00:12:20,313-->00:12:24,113
Sự phụ. Đệ chắc rằng
sư phụ cũng có thể cho đệ vài từ như vậy.

1
00:12:24,312-->00:12:25,513
"Bóng đêm" à?

368
00:12:26,112-->00:12:29,112
Để xem, hay là 
"Bóng đêm tiêu diệt vạn vật"?

368
00:12:29,312-->00:12:31,513
Đệ sẽ thử ngay...

368
00:12:32,112-->00:12:35,113
"Hãy nhận lấy."

368
00:12:35,512-->00:12:40,113
"Bóng đêm tiêu diệt vạn vật. 
Hãy cho chúng thấy! Ngọn Thương Bão Tố!"

368
00:12:41,112-->00:12:42,113
Sư phụ thấy sao?

368
00:12:42,512-->00:12:46,513
Di chuyển của cô còn rất vụng về. 
Cô cần phải trở nên nhẹ hơn một tí.

368
00:12:47,112-->00:12:48,913
Hãy cùng thử nào.

368
00:12:49,112-->00:12:51,913
Cô phải đưa cánh tay cao hơn, và....

368
00:12:52,512-->00:12:55,113
"Ngọn Thương Bão Tố!"

368
00:12:57,112-->00:13:01,113
Đây quả là một ngày tuyệt vời nhất!

368
00:13:01,513-->00:13:03,113
Sư phu, cảm ơn rất nhiều!

68
00:13:03,513-->00:13:05,113
Những bước di chuyển của chúng ta 
đã đồng bộ hóa một cách hoàn hảo!

368
00:13:05,513-->00:13:07,113
Không cần phải cảm...

368
00:13:09,512-->00:13:11,113
Shidou-san!

368
00:13:12,112-->00:13:15,512
Ồ, Shidou! Vậy cậu đang theo dõi bọn tôi à?

68
00:13:16,112-->00:13:19,313
Cậu thấy tư thế mới này thế nào?

368
00:13:19,512-->00:13:23,913
Hình dạng thật sự của Ngọn Thương Bão Tố
mà sư phụ và mình đã tạo ra đấy.

368
00:13:24,112-->00:13:28,913
T... tôi xin lỗi nhưng tôi 
có việc gấp phải đi rồi. Xin lỗi!

368
00:13:29,113-->00:13:31,113
Được rồi! Sư phụ đã giúp đệ rất nhiều!

368
00:13:31,512-->00:13:34,113
Nhớ quay lại và truyền cho đệ thêm kiến thức nhé!

368
00:13:34,513-->00:13:39,513
Mì... Mình không thể ngờ
Shidou-san đã thấy tất cả!

368
00:11:53,812-->00:11:56,113


368
00:11:57,112-->00:11:58,113


368
00:11:48,112-->00:11:50,513


368
00:11:52,112-->00:11:53,113


368
00:11:53,812-->00:11:56,113


368
00:11:57,112-->00:11:58,113
