$(function() {
    //show modal notification detail
    $('.btnShowNotificationDetail').on(
            "click",
            function() {
                $.ajax({
                    url : '/qna/notice/detail/' + $(this).attr('notifyId'),
                    type : "GET",
                    success : function(data) {
                        $('#noticeDetail').html(data);
                        $('#noticeDetail').modal('show');
                    },
                    error : function(data) {
                    }
                });
            }
        );
});