
function openContactPopup() {
	if ($('#username').length) {
		if ($('#writeQna').html().trim() != '') {
			$('#inq_category :nth-child(1)').prop('selected', true);
			$('#inq_title').val('');
			$('#inq_content').val('');
			$('#writeQna').modal('show');
		} else {
			$.ajax({
	            url : '/qna/contact/init_inquiry',
			    type : "GET",
			    success : function(data) {
			        $('#writeQna').html(data);
			        $('#writeQna').modal('show');
			    },
			    error : function(data) {
			    }
			})
		}
	} else {
		window.location.href = '/logout';
	}
}

function saveInquiry() {
	if (validateInquiry() == false) {
		return;
	}
	
	var obj = new Object();
	obj.inquiryCategory = $('#inq_category').val();
	obj.inquiryTitle = $('#inq_title').val();
	obj.inquiryContent = $('#inq_content').val();
	$.ajax({
		url: '/qna/contact/save_inquiry',
		type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(obj),
        async: false,
        cache: false,
        processData:false,
        success: function(data) {
        	if (data == 'OK') {
	        	swal({
	                title: '',
	                text: '',
	                type: 'success',
	        	})
        	} else {
        		swal({
	                title: '',
	                text: 'ERROR',
	                type: 'error',
	        	})
        	}
        },
        error: function(err) {
        	swal({
                title: '',
                text: 'ERROR',
                type: 'error',
        	})
        }
	});
}

function validateInquiry() {
	$('#inq_title').removeClass('required');
	$('#inq_content').removeClass('required');
	var flg = true;
	
	if ($('#inq_title').val().trim() == '') {
		$('#inq_title').addClass('required');
		flg =  false;
	}
	
	if ($('#inq_content').val().trim() == '') {
		$('#inq_content').addClass('required');
		flg =  false;
	}
	
	return flg;
}