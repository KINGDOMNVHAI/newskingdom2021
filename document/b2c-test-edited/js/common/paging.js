function goToPage(page, currentPage, totalPage) {
	
    if(parseInt(page) < 1 || parseInt(page) == parseInt(currentPage) || parseInt(page) > parseInt(totalPage)){
        return;
    }
    var href = $(location).attr('href');
    href = removeURLParameter(href, "page");
    if (regularUrl.test(href)) {
        href = href.concat('&');
    } else {
        href = href.concat('?');
    }
    href = href.concat('page=' + page);
    window.location.href = href;

}

function removeURLParameter(url, parameter) {
    // prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        // reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            // idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
}