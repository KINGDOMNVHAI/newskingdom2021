const LIST_CART_COOKIE = 'lstCart';
const TIMEOUT_COOKIE_DAYS = 30;

$(document).ready(function(){

    $(".search .form-control").focusin(
            function() {
                loadMasterSearchData("headerSearchData");
    });
    
    $(document).mouseup(
            function(e) {
                var container = $(".category_wrap");
                var search = $("#keyword");
                if (!container.is(e.target)
                        && container.has(e.target).length === 0
                        && !search.is(e.target)
                        && search.has(e.target).length === 0) {
                    container.hide();
                }
    });

    $("#keyword").on('keydown', function(ev) {
        if (ev.keyCode === 13) {
            var urlArr = document.URL;
            var key = $(this).val();
            /*if($.trim(key) != "" && $.trim(key) != null)*/{
                doSearchData();
            }
        }
    });
    $(".btn_search").mouseup(function(e){
        doSearchData();
    });

    $(".animate-menu-button").click(function() {
        $(this).parents(".menu-icon-container").siblings(".nav").toggleClass("open");  
        if($(".nav").hasClass("open")) {
            $(".search_area").hide();
            return false;
        } else {
            $(".search_area").show();
            return false;
        }
    });

    $('.main-backdrop').click(function(event) {
        if($(this).hasClass('main-backdrop-showed')){
            var mainMenu = $("#mainMenu");
            if(mainMenu.hasClass('open')){
                mainMenu.removeClass('open');
                $('.left-menu-toggle').removeClass('active');
                $('.main-backdrop').removeClass('main-backdrop-showed');
                $(".search_area").show();
            }
        }
    });

    $(".btn_confirm").click(function(){
        $(".booking").find(".booking_detail").css( "display", "block" );
    });

    $('.backToTop').click(function () {
       $("html, body").animate({scrollTop: 0}, 1000);
    });

    $('.lazy').lazy({
        effect : "fadeIn",
        effectTime : 500,
        threshold: 50
    });

    initButtonCart();
    if (!$('#username').length) {
        countCookieCart(null);
    }
    
    // Prevent close cart popup when click inside
    $(document).click(function() {
        var wrapTogglePrice = document.getElementById('cart_content');
        if(wrapTogglePrice){
            wrapTogglePrice.style.display = 'none';
        }
    });
    $('#btnUser').click(function() {
        var wrapTogglePrice = document.getElementById('cart_content');
        if(wrapTogglePrice){
            wrapTogglePrice.style.display = 'none';
        }
    });
    
    $('#btn_cart_popup').on("click", function(e) {
        var wrapTogglePrice = document.getElementById('cart_content');
        $('.dropdown').removeClass('open');
        if (wrapTogglePrice.style.display === 'none' || wrapTogglePrice.style.display === '') {
            wrapTogglePrice.style.display = 'block';
        } else {
            wrapTogglePrice.style.display = 'none';
        }
        e.stopPropagation();
    });
    
    $('#cart_content').on('click', function(e) {
        e.stopPropagation();
    });

    $('.left-menu-toggle').on('click', function(){
        $(this).toggleClass('active');
        $('nav.left-menu').toggleClass('left-menu-showed');
        $('.main-backdrop').toggleClass('main-backdrop-showed')
    });
    
    $('[data-toggle="tooltip"]').tooltip(); 
});

function getUrlParam(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function removeUrlParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function replaceUrlParam(url, paramName, paramValue){
    var pattern = new RegExp('(\\?|\\&)('+paramName+'=).*?(&|$)');
    var newUrl=url;
    if(url.search(pattern)>=0){
        newUrl = url.replace(pattern,'$1$2' + paramValue + '$3');
    } else {
        newUrl = newUrl + (newUrl.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
    }
    return newUrl;
}

function doSearchData(){
    var keyword = $("#keyword").val(); 
    var redirect = "";
    if($.trim(keyword) != "" && $.trim(keyword) != null){
        redirect = window.location.origin+"/list/?keyword="+escapeSpecialChar(keyword);
    }else{
        redirect = window.location.origin+"/list";
    }
    if(getUrlParam("cat") != null){
        redirect = replaceUrlParam(redirect, "cat", getUrlParam("cat"));
    }
    if(getUrlParam("city") != null){
        redirect = replaceUrlParam(redirect, "city", getUrlParam("city"));
    }
    window.location.href = redirect;
}

function escapeSpecialChar(tst){
    var result = "";
    if(tst!=null && tst!= undefined){
        tst=tst.toString();
        var char="!@#$%^&*()_+-:;<>?|\'\"\/";
        for(var i=0;i<tst.length;i++){
            if(char.indexOf(tst[i])!=-1){
                if(escape(tst[i])==tst[i]){
                    result += encodeURIComponent(tst[i]);
                }else{
                    result += escape(tst[i]);
                }
            }else{
                result += encodeURI(tst[i]);
            }
        }
    }
    return result;
}

function ajaxSessionTimeout() {
    // Handle Ajax session timeout, redirect to logout
    window.location.href = "/login";
}

!function($) {
    $.ajaxSetup({
        statusCode : {
            // Ajax has token not exist
            401 : ajaxSessionTimeout,
            // Ajax has session timeout
            403 : ajaxSessionTimeout
        }
    });
}(window.jQuery);


function loadMasterSearchData(idSearch){
    if($("#"+idSearch).attr('isloaded') != true && $("#"+idSearch).attr('isloaded') != 'true'){
        $(".btn_search").children("i").removeClass("fa fa-search").addClass("fa fa-clock-o");
        $.ajax({
            url : '/common/get_master_search_data',
            type : "GET",
            success : function(data) {
                $("#"+idSearch).html(data);
                $(".search .form-control").focusin(function() {
                    $(".search").find(".category_wrap").css("display", "block");
                });
                $(".search .form-control").focusin(
                        function() {
                            $(".search").find(".category_wrap").css(
                                    "display", "block");
                });
                $("#"+idSearch).attr('isloaded',true);
                $(".search").find(".category_wrap").css(
                        "display", "block");
                $(".btn_search").children("i").removeClass("fa fa-clock-o").addClass("fa fa-search");
                $("#keyword").focus();
                $("#keyword").on('keydown', function(ev) {
                    if (ev.keyCode === 13) {
                        var urlArr = document.URL;
                        var key = $(this).val();
                        if($.trim(key) != "" && $.trim(key) != null){
                            doSearchData();
                        }
                    }
                });
                $(".btn_search").mouseup(function(e){
                    var key = $("#keyword").val();
                    if($.trim(key) != "" && $.trim(key) != null){
                        doSearchData();
                    }
                });
            },
            error : function(data) {}
        });
    }else{
        $(".search").find(".category_wrap").css(
                "display", "block");
    }
}
function setCookieCart(name, value, days) {
    var d = new Date();
    d.setTime(d.getTime() + (days*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = name + "=" + encodeURIComponent(value) + ";" + expires + ";path=/";
}

function getCookieCart(name) {
    var cName = name + '=';
    var allCookies = decodeURIComponent(document.cookie);
    var cookieArray = allCookies.split(';');
    for(var i = 0; i < cookieArray.length; i++) {
        var c = cookieArray[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cName) == 0) {
            return c.substring(cName.length, c.length);
        }
    }
    return "";
}

function initButtonCart() {
    $('.btn_cart').on('click', function() {
        var liTag = $(this).closest('li');
        var pTag = liTag.find('p.price_sale');
        var price = pTag.find('strong').text();
        price = price.substr(1, price.length - 1);
        if (price == '0') {
            swal({
                title: msg_wrn_title,
                text: msg_wrn_price_incomplete,
                type: 'warning'
            });
            return;
        } else {
            $(this).addClass('add');

            // Set animation
            var cart = $('.cart');
            var img = $(this).parent().parent().find('img')[0];
            flyToElement(img, cart);

            // Add to cart
            var productId = $(this).parent('p').attr('productId');
            addCart(productId);
        }
    })
}

function loadCartPopup() {
    NProgress.configure({ parent: '#cart_loader' });
    NProgress.start();
    
    $('#cart_loader').html('');
    $('#cart_loader').attr('style', 'width: 300px; height: 104px');
    
    if ($('#username').length) {
         $.ajax({
            url: '/cart/load_popup',
            type: 'GET',
            async: false,
            cache: false,
            processData:false,
            success: function(data) {
                setTimeout(function() {
                    $('#cart_loader').html(data);
                    $('[data-toggle="tooltip"]').tooltip(); 
                    countCartClient();
                    if ($('#cart_loader').find('li').length > 0) {
                        $('#cart_loader').removeAttr('style');
                    }
                    NProgress.done();
                    initButtonBook();
                }, 500);
            },
            error: function(err) { }
        })
    } else {
        var lstCart = getCookieCart(LIST_CART_COOKIE);
        if (lstCart == '' || lstCart == '[]') {
            lstCart = '[]';
        }

        $.ajax({
            url: '/guest/cart/load_popup',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: lstCart,
            async: false,
            cache: false,
            processData:false,
            success: function(data) {
                setTimeout(function() {
                    countCookieCart(lstCart);
                    $('#cart_loader').html(data);
                    if ($('#cart_loader').find('li').length > 0) {
                        $('#cart_loader').removeAttr('style');
                    } 
                    NProgress.done();
                    initButtonBook();
                }, 500);
            },
            error: function(err) {}
        })
    }
}

function countCookieCart(lstCart) {
    if (lstCart == null) {
        lstCart = getCookieCart(LIST_CART_COOKIE);
    }
    var cnt = 0;
    if (lstCart != '' && lstCart != '[]') {
        lstCart = JSON.parse(lstCart);
        for (var i = 0; i < lstCart.length; i++) {
            for (var j = 0; j < lstCart[i].lstPlanPrice.length; j ++) {
                var plan = lstCart[i].lstPlanPrice[j];
                var planCount = parseInt(plan.planCount) || 0;
                cnt += planCount;
            }
        }
    }
    $('#cart_count').text(cnt);
}

function initButtonBook() {
    if ($('#cart_count').text() == '0') {
        $('#btn_booking').prop("disabled", true);
    } else {
        $('#btn_booking').prop("disabled", false);
    }
}

function flyToElement(flyer, flyingTo) {
    var $func = $(this);
    var divider = 5;
    var flyerClone0 = $(flyer).clone();
    var src = flyerClone0.attr('src');
    src = src.replace('/small/','/thumb/');
    flyerClone0.attr('src',src);
    var flyerClone = flyerClone0.clone();
    
    $(flyerClone).css({
        position: 'absolute', 
        top: $(flyer).offset().top + "px", 
        left: $(flyer).offset().left + "px", 
        opacity: 1, 'z-index': 1000
    });
    $('body').append($(flyerClone));
    var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
    var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

    $(flyerClone).animate({
        opacity: 0.4,
        left: gotoX,
        top: gotoY,
        width: $(flyer).width()/divider,
        height: $(flyer).height()/divider
    }, 700,
    function () {
        $(flyingTo).fadeOut('fast', function () {
            $(flyingTo).fadeIn('fast', function () {
                $(flyerClone).fadeOut('fast', function () {
                    $(flyerClone).remove();
                    $(flyerClone0).remove();
                });
            });
        });
    });
}

function addCart(productId) {
    if ($('#username').length) {
        $.ajax({
            url: '/product/cart/add',
            type: 'POST', 
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(productId),
            async: false,
            cache: false,
            processData:false,
            success: function(data) {
                if(data != ''){
                    $('#cart_count').text(parseInt(data));
                    setTimeout(function() {
                        $('#btn_cart_popup').click();
                    }, 1000);
                }
            },
            error: function(err) {}
        })
    } else {
        $.ajax({
            url: '/product/cart/get_plan_is_adult?productId=' + productId,
            type: 'GET', 
            success: function(data) {
                if (data != '0') {
                    var lstCart = getCookieCart(LIST_CART_COOKIE);
                    var sequence = 0;
                    if (lstCart == '' || lstCart == '[]') {
                        lstCart = [];
                    } else {
                        lstCart = JSON.parse(lstCart);
                        var lastObj = lstCart[lstCart.length - 1];
                        sequence = parseInt(lastObj.sequence) || 0;
                    }

                    var obj = new Object();
                    obj.sequence = sequence + 1;
                    obj.productId = productId;
                    obj.strUseDate = '';

                    var arrPlan = [];
                    var plan = new Object();
                    plan.planId = data;
                    plan.isAdult = 1;
                    plan.planCount = 1;
                    arrPlan.push(plan);

                    obj.lstPlanPrice = arrPlan;

                    lstCart.push(obj);
                    setCookieCart(LIST_CART_COOKIE, JSON.stringify(lstCart), TIMEOUT_COOKIE_DAYS);
                    setTimeout(function() {
                        $('#btn_cart_popup').click();
                    }, 1000);
                }
            },
            error: function(err) {}
        })
    }
}

function delCartPopup(a) {
    var li = a.closest('li');
    var sequence = li.find('[name=sequence]').val();
    var productId = li.find('[name=productId]').val();
    if ($('#username').length) {
        var arrCart = [];
        var cart = new Object();
        cart.productId = productId;
        cart.sequence = sequence;
        arrCart.push(cart);
        $.ajax({
            url: '/cart/del_cart',
            type: 'POST', 
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(arrCart),
            async: false,
            cache: false,
            processData:false,
            success: function(data) {
                if (data != '') {
                    setTimeout(function() {
                        loadCartPopup();
                        deactivateButtonCart(productId);
                    }, 1000);
                }
            },
            error: function(err) {}
        })
    } else {
        var lstCart = getCookieCart(LIST_CART_COOKIE);
        if (lstCart != '') {
            lstCart = JSON.parse(lstCart);
            for (var i = 0; i < lstCart.length; i++) {
                var obj = lstCart[i];
                if (obj.sequence == sequence) {
                    lstCart.splice(i, 1);
                    i--;
                }
            }
            if (lstCart.length == 0) {
                setCookieCart(LIST_CART_COOKIE, '', 0);
            } else {
                setCookieCart(LIST_CART_COOKIE, JSON.stringify(lstCart), TIMEOUT_COOKIE_DAYS);
            }
        }
        setTimeout(function() {
            $('#btn_cart_popup').click();
            deactivateButtonCart(productId);
        }, 1000);
    }
}

function deactivateButtonCart(productId) {
    var products = $('#cart_loader ul li');
    var isExistProduct = 0;
    for (var i = 0; i < products.length; i++) {
        if (productId == products.eq(i).find('[name=productId]').val()) {
            isExistProduct = 1;
            break;
        }
    }
    if (isExistProduct == 0) {
        $('#'+productId).removeClass('add');
    }
}

function book() {
    window.location.pathname = '/my_page/checkout';
}

function countCartClient() {
    var count = $.map($('#cart_content').find('[name=planCount]'), function(elem, i) {
        return parseInt(elem.innerText) || 0;
    }).reduce(function (a, b) {
        return a + b;
    }, 0);
    $('#cart_count').text(count);
}

function bookNow(a) {
    var liTag = a.closest('li');
    var pTag = liTag.find('p.price_sale');
    var price = pTag.find('strong').text();
    price = price.substr(1, price.length - 1);
    if (price == '0') {
        swal({
            title: msg_wrn_title,
            text: msg_wrn_price_incomplete,
            type: 'warning'
        });
        return;
    }

    if ($('#username').length) {
        var btnAddCart = a.prev();
        if (!btnAddCart.hasClass('add')) {
            var productId = btnAddCart.attr('id');
            $.ajax({
                url: '/product/cart/add',
                type: 'POST', 
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(productId),
                async: false,
                cache: false,
                processData:false,
                success: function(data) {},
                error: function(err) {}
            })
        }
        window.location.pathname = '/cart/checkout';
    } else {
        window.location.pathname = '/login';
    }
}

function urlifyHTTP(text) {
    var urlRegex = /(http?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    });
}
function urlifyHTTPS(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    });
}

function get_browser() {
	var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
 }

function getUserAgent() {
    var usAG = '';
    var browser = get_browser();
    var device = WURFL.form_factor;
    if (browser != null) {
        usAG += browser + ',';
    }
    if (device != null) {
        usAG += device + ',';
    }
    if (usAG != '') {
        usAG = usAG.substr(0, usAG.length - 1);
    }
    return usAG;
 }
function checkExistUrlParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                return true;
            }
        }
    }
    return false;
}

function checkPriceOnPackage(productId, useDate) {
    var rs = 1;
    $.ajax({
        url: '/free/cart/check_valid_package?productId='+productId+'&useDate='+useDate,
        type: 'GET',
        async: false,
        cache: false,
        success: function(data) {
            if (data != null && data == 'NOTOK') {
            	swal({
                    title: msgWrnTitle,
                    text: msgUseDateNotAllow,
                    type: 'warning'
                });
            	rs = 0;
            }
        },
        error: function(err) {
        }
    });
    return rs;
}