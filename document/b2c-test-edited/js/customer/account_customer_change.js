var CONST_PARTNER_CONFIRM_FORM_ID = "#accountCustomerForm";
var IS_INSERT = 1; // register acc (create new acc)
var MAX_SIZE = 1024000;
var MAX_WIDTH = 700;
var MAX_HEIGHT = 700;
var isImageFile = true;
var frm = $(CONST_PARTNER_CONFIRM_FORM_ID);
frm.submit(function (e) {
	hideErrMsg();
    e.preventDefault();
    var employeeForm = $(CONST_PARTNER_CONFIRM_FORM_ID)[0];
    var formData = new FormData(employeeForm);
    $('#saveBtn').prop('disabled', true);
    if ($(".icon_ck").hasClass("on")){
    	isNewsLetter = 1;
    }else{
    	isNewsLetter = 0;
    }
    formData.append("isNewsLetter", isNewsLetter);
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        enctype: "multipart/form-data",
        data: formData,
        async : false,
        cache : false,
        processData : false,
        contentType: false,
        success: function (data) {
        	if (data != null) {
        		var data = JSON.parse(data);
            	if (data.errMsg != null && data.errMsg != undefined && data.errMsg != '') {
                    showErrMsg(data.errMsg);
                    grecaptcha.reset();
                    $('#saveBtn').prop('disabled', true);
                } else {
                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    form.setAttribute("action", "/registration/done");
                    $.each(data, function(key, value) {
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", key);
                        hiddenField.setAttribute("value", value);

                        form.appendChild(hiddenField);
                    });
                    $(document.body).append(form);
                    form.submit();
                }
        	}
        },
        error: function (data) {
            swal("","","warning");
        },
    });
});
$(function(){
    $(".icon_ck").click(function(e) {
        if($(this).hasClass("on")) {
            $(this).removeClass("on");
            return false;
        } else {
            $(this).addClass("on");
            return false;
        }      
    });
});

function confirmAccountCustomer(isInsert) {
    var frm = $(CONST_PARTNER_CONFIRM_FORM_ID);
    if (isValidForm(isInsert)) {
        frm.submit();
    }
}

/**
 * Calculate center of window
 * @returns {{w: number, h: number, left: number, top: number}}
 */
function calculatePopupDimensions() {
    var w = 650;
    var h = 650;
    var wLeft = window.screenLeft ? window.screenLeft : window.screenX;
    var wTop = window.screenTop ? window.screenTop : window.screenY;

    var left = wLeft + (window.innerWidth / 2) - (w / 2);
    var top = wTop + (window.innerHeight / 2) - (h / 2);
    return {w: w, h: h, left: left, top: top}; 
}

function loginSocial(type) {
    var provider = "";
    if (type == 1) {
    	provider = "facebook";
    } else if (type == 2) {
    	provider = "google";
    } else if (type == 3) {
    	provider = "twitter";
    }
    var __ret = calculatePopupDimensions();
    window.open( '/signin/' + provider + '/popup', "_blank", 'scrollbars=yes, width=' + __ret.w + ', height=' + __ret.h + ', top=' + __ret.top + ', left=' + __ret.left );
}

function isValidForm(isInsert) {
	$(CONST_PARTNER_CONFIRM_FORM_ID).find('*').removeClass('required');
	 // validate email
    var email = $("#email");
    var password = $("#password");
    var passwordConfirm = $("#password_confirm");
    var phone = $("#phone");
    var isValid = true;
    if (!isImageFile) {
    	return false;
    }
    hideErrMsg();
    var nameFocus = '';
    var errMgs = '';
    var isChangePass = true;
    if (isInsert != undefined && isInsert != null && isInsert == IS_INSERT) {
    	var staffName = $("#staffName"); 
    	if (staffName.val() == "" || staffName.val() == undefined) {
        	nameFocus = staffName;
            isValid = false;
        }
    	if (nameFocus != '') {
        	scrollToElement(nameFocus, true);
        	if (errMgs != '') {
        		showErrMsg(errMgs);
        	}
        	return false;
        }
    } else {
    	var passwordOld = $("#oldPassword");
    	if ((passwordOld.val() == "" || passwordOld.val() == undefined) 
    			&&  (password.val() == "" || password.val() == undefined) 
    			&&  (passwordConfirm.val() == "" || passwordConfirm.val() == undefined)) {
    		isChangePass = false;
    	}
    	if (isChangePass && (passwordOld.val() == "" || passwordOld.val() == undefined)) {
    		nameFocus = passwordOld;
            isValid = false;
    	} else if (isChangePass && (passwordOld.val() == password.val())) {
    		nameFocus = passwordOld;
    		errMgs = msg_err_new_pass_is_not_valid;
            isValid = false;
    	}
    }
    if (email.val() == "" || email.val() == undefined || !/^([^@]+?)@(([a-z0-9]-*)*[a-z0-9]+\.)+([a-z0-9]+)$/i.test(email.val())) {
    	if (email.val() != "") {
    		errMgs = msg_err_email_is_not_valid;
    	}
    	nameFocus = email;
        isValid = false;
    } else if (isChangePass && (password.val() == "" || password.val() == undefined || password.val().length < 6)) {
    	if (password.val().length < 6) {
    		errMgs = msg_err_partner_password_min_length;
    	}
    	nameFocus = password;
        isValid = false;
    } else if (isChangePass && (password.val() != passwordConfirm.val())) {
    	nameFocus = passwordConfirm;
    	errMgs = msg_err_pass_is_not_valid;
        isValid = false;
    } else if (phone.val() != "" && phone.val() != undefined && !/^[0-9-+]+$/i.test(phone.val())) {
    	nameFocus = phone;
        isValid = false;
    } 
    // check recaptcha 
    else if (grecaptcha.getResponse().length <= 0) {
        $('#saveBtn').attr('disabled', 'disabled');
        errMgs = customer_account_require_captcha;
        isValid = false;
    }
    
    if (nameFocus != '') {
    	scrollToElement(nameFocus, true);
    }

    if (errMgs != '') {
        showErrMsg(errMgs);
    }

    return isValid;
}

function file_change(fuData) {
	hideErrMsg();
	var errMss = '';
	var FileUploadPath = fuData.value;
	if (FileUploadPath != '') {
	    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
	    if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
	            if (fuData.files && fuData.files[0]) {
	                var size = fuData.files[0].size;
	                if (size > MAX_SIZE) {
	                	errMss = validationMessages.file_image_error_fileSize;
	                }
	                var reader = new FileReader();
                	reader.onload = function (e) {
                		var img = document.getElementById("avatarView");
                		img.src = e.target.result;
                		img.style.display = "inline";
                		img.onload = function () {
                            var height = this.naturalHeight || this.height;
                            var width = this.naturalWidth || this.width;
                            if (height > MAX_HEIGHT) {
                            	errMss = validationMessages.file_image_error_maxHeight;
                            } else if (width > MAX_WIDTH) {
                            	errMss = validationMessages.file_image_error_maxWidth;
                            }
                            if (errMss != '') {
        	            		isImageFile = false;
        	            		showErrMsg(errMss);
        	            	} else {
        	            		isImageFile = true;
        	            	}
                        };
                	};
                	reader.readAsDataURL(fuData.files[0]);
	            }

	   } else {
		   errMss = validationMessages.file_image_error_imageFormat;
	   }
	}
	if (errMss != '') {
		isImageFile = false;
		showErrMsg(errMss);
	} else {
		isImageFile = true;
	}
}

function showErrMsg(errMss) {
    $('#errMsgSpan').html(errMss);
    $('#errMsg').show();
}

function hideErrMsg(errMss) {
    $('#errMsgSpan').html('');
    $('#errMsg').hide();
}

function clickFileUpload() {
    $('#avatar').trigger('click');
}

function scrollToElement(element, isFocus) {
    element.addClass('required');
    if (isFocus == true || isFocus == 'true') {
        element.focus();
    }
}

function recaptchaCallback() {
    $("#saveBtn").removeAttr("disabled");
    hideErrMsg();
};