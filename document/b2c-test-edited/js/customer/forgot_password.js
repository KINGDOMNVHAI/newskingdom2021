function resetPass() {
    hideErrMsg();
    $('#forgetPassDiv').find('*').removeClass('required');
    var email = $('#email');
    if (email.val() == ""
            || email.val() == undefined
            || !/^([^@]+?)@(([a-z0-9]-*)*[a-z0-9]+\.)+([a-z0-9]+)$/i.test(email
                    .val())) {
    	showErrMsg(msg_err_email_is_not_valid);
        scrollToElement(email, true);
        return;
    }
    $('#saveBtn').prop('disabled', true);
    $.ajax({
        url : '/forgot_password/reset_password?email=' + email.val().trim(),
        async : false,
        cache : false,
        processData : false,
        success : function(data) {
            if (data == "OK") {
                swal({ 
                    title: request_email_reset_password_success,
                     text: "",
                      type: "success"
                    },
                    function(){
                      window.location.href = '/login';
                  });
            } else {
                showErrMsg(data);
            }
            $('#saveBtn').prop('disabled', false);
        },
        error : function(err) {
            $('#saveBtn').prop('disabled', false);
        }
    });
}

$(function() {
    $('#email').on('keypress', function(e) {
        // key press enter
        if (e.which === 13) {
            resetPass();
        }
    });
});

function showErrMsg(errMss) {
    $('#errMsgSpan').html(errMss);
    $('#errMsg').show();
}

function hideErrMsg(errMss) {
    $('#errMsgSpan').html('');
    $('#errMsg').hide();
}

function scrollToElement(element, isFocus) {
    element.addClass('required');
    if (isFocus == true || isFocus == 'true') {
        element.focus();
    }
}