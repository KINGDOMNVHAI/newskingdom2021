$(document).ready(function(){
   var firstCatId = $("#firstCatLoad").attr('val');
   $.simpleTicker($("#notice_area"),({'effectType':'fade', interval: 3000, delay: 10000}));
});

function loadProductByCategory(id){
    var isLoaded = $('#cate'+id).children().attr('isLoaded');
    if(isLoaded != 'true'){
        $.ajax({
            url : '/get_product_by_category?catId=' + id,
            type : "GET",
            success : function(data) {
                $('#cate'+id).children().html(data);
                $('#cate'+id).children().attr('isLoaded',true);
                $('.lazy').lazy({
                    effect : "fadeIn",
                    effectTime : 500,
                    threshold: 50
                });
                initButtonCart();
            },
            error : function(data) {
            }
        });
    }
}

function closePopup(btn) {
    var popup = btn.closest('div.popup');
    popup.remove();
    removeFade();
    //$('#system_popup').hide();
}

function notDisplayToday() {
	$('#system_popup').remove();
	var flag = getCookieCart('fnd_noti_flg');
	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth() + 1;
	if (month.toString().length < 2) {
		month = '0' + month;
	}
	var date = d.getDate();
	if (date.toString().length < 2) {
		date = '0' + date;
	}
	var dateISO = year + '-' + month + '-' + date;
	setCookieCart('fnd_noti_flg', dateISO, 1);
	removeFade();
}

function notDisplayToday3Dsecure() {
    $('.popup-secure').remove();
    var flag = getCookieCart('fnd_3d_noti_flg');
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    if (month.toString().length < 2) {
        month = '0' + month;
    }
    var date = d.getDate();
    if (date.toString().length < 2) {
        date = '0' + date;
    }
    var dateISO = year + '-' + month + '-' + date;
    setCookieCart('fnd_3d_noti_flg', dateISO, 1);
    removeFade();
}

function removeFade() {
    if ($('.popup-all').find('div.popup').length == 0) {
        $('.modal-backdrop.fade.in').remove();
    }
}