const SPINNER_CSS = {'position':'relative','left':'auto','top':'auto','margin':'20px auto 0'};

var isLoadMoreReview = false;
var oldUrl = null;
$(function() {
    $(document).on('scroll', function () {
        var distanceFromBottom = $(document).height() - ($(document).scrollTop() + $(window).height());
        if(distanceFromBottom <= 600 && isLoadMoreReview){
            loadMoreReview();
        }
    });

    isLoadMoreReview = parseInt($("#totalReview").text().replace(/\(|\)/g,"")) > parseInt($('#offsetReview').val());
});

function loadMoreReview() {
    // check condition to load more
    var totalReview = $("#totalReview").text().replace(/\(|\)/g,"");
    var offset = $('#offsetReview').val();
    if (parseInt(totalReview) > 0 && parseInt(totalReview) > parseInt(offset)) {
        var productId = $('#productId').val();
        var limit = $('#limitReview').val();
        var currentUrl = '/product/load_more_product_review?productId=' + productId + '&limit=' + limit + '&offset=' + offset;
        if (currentUrl == oldUrl) {
            return;
        }
        oldUrl = currentUrl;
        NProgress.configure({ parent: '#nprogressReview' });
        NProgress.start();
        $('#nprogressReview').find('div.bar').hide();
        $('#nprogressReview').find('.spinner').css(SPINNER_CSS);
        $("#nprogressReview").show();
        $.ajax({
            url : currentUrl,
            type : "GET",
            success : function(data) {
                $('#wrap_product_review').append(data);
                
                $('.lazy').lazy({
                    effect : "fadeIn",
                    effectTime : 500,
                    threshold: 50
                });
                $('#offsetReview').val(parseInt(offset) + parseInt(limit));
                NProgress.done();
                $("#nprogressReview").hide();
            },
            error : function(data) {
                swal("", "", "warning");
            }
        });
    } else {
        isLoadMoreReview = false;
    }
}