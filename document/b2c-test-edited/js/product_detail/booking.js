$(function() {
    initializeDatePrice();
    loadPricePlan();
});

function loadPricePlan(numTicketMap) {
    var planPrice = document.getElementById('planPrice');
    var productId = $('#productId').val();
    var datePrice = $('#datePrice').val();
    if(datePrice == ""){
    	var curDate = new Date();
    	datePrice = curDate.getFullYear() + "/" + (curDate.getMonth() + 1) + "/" + curDate.getDate();
    }
    var date = new Date(datePrice);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    if (month < 10) {
        month = '0' + month;
    }
    if (day < 10) {
        day = '0' + day;
    }

    var dateFormatted = year + "/" + month + "/" + day;

    $.ajax({
        url : '/product/change_product_price?datePrice=' + dateFormatted + '&productId=' + productId,
        type : "GET",
        success : function(data) {
            $('#planPrice').html(data);

            if (numTicketMap != undefined) {
                // update number of ticket for each plan
                $("#formCalculatorPrice li").each(
                    function() {
                        var planId = $(this).find("[name=frmPlanId]").val();
                        if (isNaN(numTicketMap[planId])) {
                        	$(this).find("input[name=num_ticket]").val("0");
                        } else {
                            $(this).find("input[name=num_ticket]").val(numTicketMap[planId]);
                        }
                    }
                );
            }

            updateTotalPrice();
            lastDatePrice = datePrice;
        },
        error : function(data) {
            swal("","","warning");
        }
    });
}

function changeDate() {
    // backup selected ticket amount for each plan that user selected before
    var numTicketMap = {};
    $("#formCalculatorPrice li").each(
        function () {
            var planId = $(this).find("[name=frmPlanId]").val();
            var numTicket = $(this).find("input[name=num_ticket]").val();
            numTicketMap[planId] = numTicket;
        }
    );

    // load price by date
    loadPricePlan(numTicketMap);
}

function updateTotalPrice() {
    var symbol = $("[name=frmSymbol]").val();
    var totalPrice = 0;
    $("#formCalculatorPrice input[name=num_ticket]").each(
        function() {
            var amount = $(this).val();
            if (amount == undefined || amount == "") {
                amount = 0;
            }
            var price = $(this).closest("li").find("[name=frmPriceCalculate]").val();
            price = amount * price;
            if (price > 0 && !isNaN(price)) {
                totalPrice = totalPrice + price;
            }
        }
    );

    if($("#productType").val() != 1){
        if (!isNaN(totalPrice) && totalPrice > 0) {
            $("#productDetailTotalPrice").text(symbol + formatNumberWithCommas(totalPrice));
            $("#btnAddAccountCart").removeAttr("disabled");
            $("#btnGoToBooking").removeAttr("disabled");
        } else {
            $("#productDetailTotalPrice").text("0");
            $("#btnAddAccountCart").attr("disabled", "disabled");
            $("#btnGoToBooking").attr("disabled", "disabled");
        }
    }
}

function formatNumberWithCommas(x) {
    x = Number((x).toFixed(2));
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function initializeDatePrice() {
    var curDate = new Date();
    var datePrice = curDate.getFullYear() + "/" + (curDate.getMonth() + 1) + "/" + curDate.getDate();
//    $("#datePrice").val(datePrice);not set date default
}

$(function() {

    $('#toggleCalendar').on(
        "click",
        function(event) {
            var productId = $('#productId').val();
            var wrapToggleCalendar = document.getElementById('wrapToggleCalendar');
            if (wrapToggleCalendar.style.display === 'none' && $(this).attr('hasData') == 0) {
                var planId = $("#planId").val();
                getStockCalendar(productId, planId, 0, 0);
                $(this).attr('hasData', 1);
                wrapToggleCalendar.style.display = 'block';
                $(this).closest('div.dropdown').addClass('open');
            } else {
                if (wrapToggleCalendar.style.display === 'none') {
                    wrapToggleCalendar.style.display = 'block';
                    $(this).closest('div.dropdown').addClass('open');
                } else {
                    wrapToggleCalendar.style.display = 'none';
                    $(this).closest('div.dropdown').removeClass('open');
                }
                activeCurrentDateToCalendar();
            }

            event.stopPropagation();
        }
    );

    $('.datepicker-only-init').datetimepicker({
        widgetPositioning: {
            horizontal: 'left'
        },
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'YYYY/MM/DD'
    });

    $('.datepicker-only-init').on('dp.show', function() {
        $(this).datetimepicker('minDate', new Date(strCutOffDate).withoutTime());
    })


    $('#datePrice').on('blur', function() {
        if ($(this).val() == '' || $("#datePrice").val() == lastDatePrice) {
            return false;
        }
        activeCurrentDateToCalendar();
        changeDate();
        checkMemoDate();
    });
});

function checkMemoDate() {
    var datePrice = $("#datePrice").val();
    var productId = $('#productId').val();
    if (selectedDateList.indexOf(datePrice) < 0) {
        getAndShowMemoByDate(productId, datePrice);
        selectedDateList.push(datePrice);
    }
    $('#togglePrice').attr('hasData', 0);
}

function activeCurrentDateToCalendar() {
    $("#wrapToggleCalendar").find(".check").each(
        function() {
            $(this).removeClass("check");
        }
    );
    var selectedDateCalendar = $("#datePrice").val();
    if (selectedDateCalendar == undefined || selectedDateCalendar == "") {
        var curDate = new Date();
        selectedDateCalendar = curDate.getFullYear + "/" + curDate.getMonth + 1 + "/" + curDate.getDate();
        if(strCutOffDate != null && strCutOffDate !== undefined){
            selectedDateCalendar = strCutOffDate.split("-")[0] + "/" + strCutOffDate.split("-")[1] + "/" + strCutOffDate.split("-")[2];
        }
    }
    var selectedYear = selectedDateCalendar.replace(/(.*)\/.*\/.*/, "$1");
    var selectedMonth = parseInt(selectedDateCalendar.replace(/.*\/(.*)\/.*/, "$1"));
    var selectedDay = parseInt(selectedDateCalendar.replace(/.*\/.*\/(.*)/, "$1"));
    
    var year = $("#year").val();
    var month = $("#month").val();
    if (Number(selectedYear) == Number(year) && Number(selectedMonth) == Number(month)) {
        $("#wrapToggleCalendar table tbody td a[day='" + selectedDay + "']").parent().parent().addClass("check");
    }
}

function getStockCalendar(productId, planId, year, month) {
    // NProgress.configure({ parent: '#wrapToggleCalendar' });
    // NProgress.configure({ minimum: 0.5 });
    // NProgress.configure({ easing: 'ease', speed: 500 });
    // NProgress.set(0.3);
    // NProgress.start();
    $.ajax({
        url : '/product/append_product_stock_calendar_data?productId=' + productId + '&planId=' + planId + '&year=' + year + '&month=' + month+ '&cutOff='+strCutOffDate,
        type : "GET",
        success : function(data) {
            $("#wrapToggleCalendar").html(data);
            $("#wrapToggleCalendar").attr("hasData", "1");
            $('#wrapToggleCalendar').css("height",""); 
            // NProgress.done();
            activeCurrentDateToCalendar();
        },
        error : function(data) {
        	// NProgress.done();
        }
    });
}