checkPricePackage = true;
$(function() {
    $('.btnRemoveBooking').on(
        "click",
        function() {
            var total = parseInt($('#total').val());
            total = total - parseInt($(this).attr('price'));
            $('#strTotal').text(total);
            $("#strTotal").digits();
            $('#total').val(total);
            $(this).closest('li').remove();
        }
    );

    $('#btnAddAccountCart').on(
        "click",
        function() {
        	var datePrice = $('#datePrice').val();
        	$('#datePrice').removeClass('required');
        	if( datePrice != ""){
        		if($("#productType").val() != '1' && $('#formCalculatorPrice li').size() == 0){
                    showPopupPreviewNotCart();
                } else {
                    if ($('#username').length) {
                        addAccountCartPreview('/product/add_account_cart_preview');
                    } else {
                        addCartDetail();
                    }
                    if (checkPricePackage) {
                    	$('#btnGoToBooking').attr('hasCheck', 1);
                    }
                }
        	}else{
        		var result = validateData();
                if (result == false) {
                    return false;
                }
        	}
        }
    );

    $('#btnGoToBooking').on(
        "click",
        function() {
        	var datePrice = $('#datePrice').val();
        	$('#datePrice').removeClass('required');
        	if( datePrice != ""){
	            if($("#productType").val() != '1' && $('#formCalculatorPrice li').size() == 0){
	                showPopupPreviewNotCart();
	            }else{
	                if($(this).attr('hasCheck') == 1){
	                    window.location.pathname = '/my_page/checkout';
	                }else{
	                    if ($('#username').length) {
	                        addAccountCartPreview('/product/go_to_account_cart_preview');
	                    } else {
	                        addCartDetail();
	                        if (checkPricePackage) {
	                        	window.location.pathname = '/my_page/checkout';
	                        }
	                    }
	                }
	            }
        	} else {
        		var result = validateData();
                if (result == false) {
                    return false;
                }
        	}
        }
    );
});
function validateData() {
    $('#datePrice').removeClass('required');
    var result = true;
        if ($('#datePrice').val() == '') {
        	$('#datePrice').addClass('required');
            swal({
                title : msgWrnTitle,
                text : msgWrnText,
                type : 'warning',
            });
            result = false;
    }
    return result;
}
function showPopupPreviewNotCart() {
    swal({
        title: product_preview_not_cart,
        text: "",
        type: "warning",
        timer: 3000,
        showConfirmButton: false
    });
}

function addAccountCartPreview(url) {
	checkPricePackage = true;
    var productId = $("#productId").val();
    var strUseDate = $("#datePrice").val();
    if (checkPriceOnPackage(productId, strUseDate) == 0) {
    	checkPricePackage = false;
		return;
	}
    var useTime = '';
    if ($('#use_time').length > 0) {
        useTime = $('#use_time').val();
    }
    var frmPlanIds = [];
    var frmNums = [];

    $("#formCalculatorPrice li").each(
        function() {
            var planId = $(this).find("input[name=frmPlanId]").val();
            var numTicket = $(this).find("input[name=num_ticket]").val();
            frmPlanIds.push(planId);
            frmNums.push(numTicket);
        }
    );

    var data = new FormData();
    data.append("productId", productId);
    data.append("strUseDate", strUseDate);
    data.append("frmPlanIds", frmPlanIds);
    data.append("frmNums", frmNums);
    data.append("useTime", useTime);

    $.ajax({
        url : url,
        type : "POST",
        enctype: 'multipart/form-data',
        contentType : false,
        data : data,
        async : false,
        cache : false,
        processData : false,
        success : function(data) {
            if(data == "OK"){
                swal({
                    text : product_preview_add_cart_success,
                    title: "", 
                    type: "success"
                },
                function(){
                    $('#cart_add_flag').val('true');
                    setTimeout(function(){
                        $("#btn_cart_popup").click();
                    }, 500);
                });
            }else if(data == "GOTO"){
                window.location.pathname = '/my_page/checkout';
            }
        },
        error : function(data) {}
    });
}

function addCartDetail() {
	checkPricePackage = true;
	if ($('#productType').val() != undefined && $('#productType').val() != '' && $('#productType').val() == '1') {
		if (checkPriceOnPackage($("#productId").val(), $('#datePrice').val()) == 0) {
			checkPricePackage = false;
			return;
		}
		var lstCart = getCookieCart('lstCart');
        var sequence = 0;
        if (lstCart == '' || lstCart == '[]') {
            lstCart = [];
        } else {
            lstCart = JSON.parse(lstCart);
            var lastObj = lstCart[lstCart.length - 1];
            sequence = parseInt(lastObj.sequence) || 0;
        }
		$('.detail input[name=frmProduct]').each(function() {
			var obj = new Object();
            obj.sequence = sequence + 1;
            obj.productId = $(this).attr('productId');
            obj.strUseDate = $('#datePrice').val();
            obj.packageId = $("#productId").val();
            obj.packageShortName = $(this).attr('packageShortName');
            
            var arrPlan = [];
            var plan = new Object();
            plan.planId = $(this).attr('planId');
            plan.isAdult = 1;
            plan.planCount = 1;
            arrPlan.push(plan);

            obj.lstPlanPrice = arrPlan;

            lstCart.push(obj);
        });
		setCookieCart(LIST_CART_COOKIE, JSON.stringify(lstCart), TIMEOUT_COOKIE_DAYS);
        setTimeout(function() {
            $('#btn_cart_popup').click();
        }, 1000);
	} else {
		var lstCart = getCookieCart('lstCart');
	    var sequence = 0;
	    if (lstCart == '' || lstCart == '[]') {
	        lstCart = [];
	    } else {
	        lstCart = JSON.parse(lstCart);
	        var lastObj = lstCart[lstCart.length - 1];
	        sequence = parseInt(lastObj.sequence) || 0;
	    }

	    var obj = new Object();
	    obj.sequence = sequence + 1;
	    obj.productId = $('#productId').val();
	    obj.strUseDate = $('#datePrice').val();

	    var arrPlan = [];
	    var plans = $('#formCalculatorPrice').find('li');
	    for (var i = 0; i < plans.length; i++) {
	        var plan = new Object();
	        plan.planId = plans.eq(i).find('[name=frmPlanId]').val();
	        if (i == 0) {
	            plan.isAdult = 1;
	        } else {
	            plan.isAdult = 0;
	        }
	        plan.planCount = plans.eq(i).find('[name=num_ticket]').val();
	        
	        arrPlan.push(plan);
	    }

	    obj.lstPlanPrice = arrPlan;
	    lstCart.push(obj);
	    setCookieCart('lstCart', JSON.stringify(lstCart), 30);
	    setTimeout(function() {
	        $('#btn_cart_popup').click();
	    }, 1000);
	}
}