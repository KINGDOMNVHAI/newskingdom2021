var selectedDateList = [];
var lastDatePrice = null;

Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
}

$(document).ready(function(){
    $('.datepicker-only-init').datetimepicker({
        widgetPositioning: {
            horizontal: 'left'
        },
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'YYYY/MM/DD'
    });
    $('.datepicker-only-init').on('dp.show', function() {
        $(this).datetimepicker('minDate', new Date(strCutOffDate).withoutTime());
    })   
    $('#datePrice').val("");
});

$(document).click(function() {
    var wrapToggle = document.getElementById("wrapToggleCalendar");
    if(wrapToggle){
        wrapToggle.style.display = 'none';
    }
});

$(function(){
    $(".side_book .btn_toggle").click(function(){
        $(this).siblings(".date_picker, .quantity, dl.total").fadeToggle();
        $(this).children("i").toggleClass("icmn-arrow-down15")
    });
});

$("#wrapToggleCalendar").click(function(event) {
    event.stopPropagation();
});

$(function() {
    Grid.init();
    hiddenButtonLoadMore();
    $('#wrapMapUrl').html($('#mapUrl').val());
    var textDescription = $('#textDescription').text();
    $('#textDescription').html(textDescription);
    //$('#textDescription').html(urlifyHTTP(textDescription));
    //$('#textDescription').html(urlifyHTTPS(textDescription));
    // RADAR CHART
    if ($("#chart-radar").length) {
        var radarCtx = document.getElementById('chart-radar').getContext('2d');
        var rate1 = $("#rate1").val();
        var rate2 = $("#rate2").val();
        var rate3 = $("#rate3").val();
        var rate4 = $("#rate4").val();
        var rate5 = $("#rate5").val();
        var dataRadar = {
            labels: [product_review_rate_1, product_review_rate_2, product_review_rate_3, product_review_rate_4, product_review_rate_5],
            datasets: [
                {
                    label: product_review_title_chart,
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,0)",
                    pointBackgroundColor: "rgba(255,99,132,1)",
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(255,99,132,1)",
                    data: [rate1, rate2, rate3, rate4, rate5]
                }
            ]
        };

        new Chart(radarCtx, {
            type: 'radar',
            data: dataRadar,
            options: {
                scale: {
                    reverse: false,
                    ticks: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
});

function getAndShowMemoByDate(productId, memoDate) {
    $.ajax({
        url: "/product/get_product_calendar_description?productId=" + productId + "&memoDate=" + memoDate,
        type: "GET",
        success: function(data) {
            data = JSON.parse(data);
            if (data != null && data.id > 0 ) {
                var title = data.memoContent;
                var imageUrl = data.memoImage != null ? data.memoImage : "";
                swal({
                    title: title,
                    text: "",
                    imageUrl: imageUrl,
                    imageSize: '200x120'
                });
            }
        },
        error: function(data) {
            swal("", "", "warning");
        }
    });
}

function hiddenButtonLoadMore() {
    if($('#wrap_product_review li').size() >= $('em#totalReview').text().replace(/\(|\)/g,"")){
        $('#btnLoadMoreProductReview').remove();
    }
}


$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}

function scrollToReview() {
    $('html, body').animate({
        scrollTop: $("#userReview").offset().top - 100
    }, 1000);
}

//show product image second detail
function openModal() {
    document.getElementById('myModal').style.display = "block";
}

function closeModal() {
    document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
if($('#myModal').length > 0 && $("#og-grid li").length > 0){
    showSlides(slideIndex);
}

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slide = $('#myModal').find('.mySlides');
    var dots = $('#og-grid').find('.thum');
    $('#caption').text('');
    
    if (n > dots.length) {slideIndex = 1}
    if (n < 1) {slideIndex = dots.length}

    var url = dots.eq(slideIndex-1).attr('src');
    url = url.replace('medium', 'large');
    
    for (i = 0; i < dots.length; i++) {
    	dots.eq(i).removeClass('active');
        dots[i].className = dots[i].className.replace(" active", "");
    }
    dots.eq(i).addClass('active');
    $('#caption').text(dots.eq(slideIndex-1).attr('data-description'));
    slide.find('img').attr('src', url);
    slide.show();
}