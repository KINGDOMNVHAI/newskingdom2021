$(document).ready(function(){
    var lstMenuSearch = $(".city_product_list").clone();
    $("#list_menu_search_fixed").hide();
    $("#list_menu_search_fixed").html(lstMenuSearch);
    $("#list_menu_search_fixed ul").removeClass('city_product_list');
    $("#list_menu_search_fixed ul").addClass('city_product_list_fixed');
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        var x = screen.height;
        if (scroll >= 150 && scroll>x/3) {
            $(".gnb, .side_nav, .side_wrap").addClass("fixed");
            $(".link-category").addClass("link-category-fixed");
            $(".city_product_list").addClass("city_product_list_fixed");
            $(".main .search_area").removeClass("center");
            $(".floating_totop").css( "display", "block" );
            $("#list_menu_search_fixed").show();
        }
        
        var url = window.location.href;
        if(checkExistUrlParam('city',url) == true){
            if (scroll < 280) {
                $(".gnb, .side_nav, .side_wrap").removeClass("fixed");
                $(".link-category").removeClass("link-category-fixed");
                $(".city_product_list").removeClass("city_product_list_fixed");
                $(".main .search_area").addClass("center");
                $(".floating_totop").css( "display", "none" );
                $("#list_menu_search_fixed").hide();
            }
        }else{
            if (scroll < 50) {
                $(".gnb, .side_nav, .side_wrap").removeClass("fixed");
                $(".link-category").removeClass("link-category-fixed");
                $(".city_product_list").removeClass("city_product_list_fixed");
                $(".main .search_area").addClass("center");
                $(".floating_totop").css( "display", "none" );
                $("#list_menu_search_fixed").hide();
            }
        }
        
    });

    $("#headLanguage li a").click(function(){
        $("#headLanguageVal:first-child").text($(this).text());
        $("#headLanguageVal:first-child").val($(this).text());

        var langCode = $(this).attr("code");
        var urlArr = document.URL;
        urlArr = replaceUrlParam(urlArr,"lang",langCode);
        window.location.href = urlArr;
    });

    $("#headCurrency li a").click(function(){
        $("#headCurrencyVal:first-child").text($(this).text());
        $("#headCurrencyVal:first-child").val($(this).text());

        var currCode = $(this).attr("code");
        var urlArr = document.URL;
        urlArr = replaceUrlParam(urlArr,"cur",currCode);
        window.location.href = urlArr;
    });

    if ($('#headCurrencyVal').length) {
        var nowId = $("#headNowCurrencyId").val();
        var selectItem = $("#headCurrency li a[value="+nowId+"]")[0];
        $("#headCurrencyVal:first-child").text(selectItem.text);
        $("#headCurrencyVal:first-child").val(selectItem.getAttribute('value'));
    }

    if ($('#headLanguageVal').length) {
        var nowId = $("#headNowLanguageId").val();
        var selectItem = $("#headLanguage li a[value="+nowId+"]")[0];
        $("#headLanguageVal:first-child").text(selectItem.text);
        $("#headLanguageVal:first-child").val(selectItem.getAttribute('value'));
    }

    $('.lazy').lazy({
        effect : "fadeIn",
        effectTime : 500,
        threshold: 50
    });
});