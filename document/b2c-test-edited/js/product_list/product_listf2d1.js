const SPINNER_CSS = {'position':'relative','left':'auto','top':'auto','margin':'20px auto 0'};
var isLoadingMore = false;
$(document).ready(function(){
    // Scroll custom page list product
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        var x = screen.height;
        if (scroll >= 150 && scroll>x/3) {
            $(".gnb, .side_nav, .side_wrap").addClass("fixed-custom");
        }
        
        var url = window.location.href;
        if(checkExistUrlParam('city',url) == true){
            if (scroll < 280) {
                $(".gnb, .side_nav, .side_wrap").removeClass("fixed-custom");
            }
        }else{
            if (scroll < 50) {
                $(".gnb, .side_nav, .side_wrap").removeClass("fixed-custom");
            }
        }
    });
    // End scroll custom page list product
	$('.btn_list').click(function(){
        $(this).addClass('on');
        $('body').find('.list_wrap').removeClass('type_card');
        $('body').find('.list_wrap').addClass('type_list');
        $('.btn_card').removeClass('on');
    });
    $('.btn_card').click(function(){
        $(this).addClass('on');
        $('body').find('.list_wrap').removeClass('type_list');
        $('body').find('.list_wrap').addClass('type_card');
        $('.btn_list').removeClass('on');
    });
    $(".city").click(function(){
        $(this).toggleClass("open");
    });
    
    $("#headerSearchData").attr('isloaded',true);
    var urlSort = getUrlParam("sort");
    if (urlSort != undefined) {
        $("#pdSearchFilter").val(parseInt(urlSort));
    } else {
        if (getUrlParam("keyword")) {
            $("#pdSearchFilter").val(0);
        } else {
            $("#pdSearchFilter").val(5);
        }
    }

    var viewType = getUrlParam("vt");
    if (viewType == 2) {
        $('.btn_card').addClass('on');
        $('body').find('.list_wrap').removeClass('type_list');
        $('body').find('.list_wrap').addClass('type_card');
        $('.btn_list').removeClass('on');
    } else if (viewType == 1) {
        $('.btn_list').addClass('on');
        $('body').find('.list_wrap').removeClass('type_card');
        $('body').find('.list_wrap').addClass('type_list');
        $('.btn_card').removeClass('on');
    } else {
        $('.btn_card').addClass('on');
        $('body').find('.list_wrap').removeClass('type_list');
        $('body').find('.list_wrap').addClass('type_card');
        $('.btn_list').removeClass('on');
    }

    $(window).scroll(function() {
        var foot = $(".footer").height();
        if(($(window).scrollTop() + $(window).height() >= $(document).height() - foot) && isLoadingMore == false) {
            loadingMorePaging();
        }
     });

    
    $( ".lazy" ).each(function( index ) {
        if($(this).attr('src') == undefined || $(this).attr('src') == null){
            $(this).attr('src',window.location.origin+"/img/no-image.gif");
        }
    });
    
    var totalResult = parseInt($("#hideTotalResult").val());
    var totalShowing = parseInt($("#lstDataContent").children().size());
    if(totalResult == totalShowing){
        $("#nprogressList").hide();
    }
    
});

function filterSearch() {
    var filterType = $("#pdSearchFilter").val();
    var url = window.location.href;
    url = replaceUrlParam(url, "sort", filterType);
    window.location.href = url;
}
function viewListType(control, type) {
    var url = window.location.href;
    if (type == 2) {
        url = replaceUrlParam(url, "vt", 2);
    } else {
        url = replaceUrlParam(url, "vt", 1);
    }
    window.location.href = url;
}
function searchById(type, id) {
    var url = window.location.href;
    if (type == 'city') {
        //url = removeUrlParam('cat', url);
        //url = removeUrlParam('keyword', url);
        //url = removeUrlParam('page', url);
        if(Number(getUrlParam("city")) === Number(id)){
            url = removeUrlParam('city', url);
        }else{
            url = replaceUrlParam(url, "city", id);
        }
    } else if (type == 'category') {
        //url = removeUrlParam('city', url);
        //url = removeUrlParam('keyword', url);
        //url = removeUrlParam('page', url);
        if(getUrlParam("cat") === id){
            url = removeUrlParam('cat', url);
        }else{
            url = replaceUrlParam(url, "cat", id);
        }
    }
    window.location.href = url;
}
function loadingMorePaging(){
    var param = null;
    if(getUrlParam("city") != undefined && getUrlParam("city") != null){
        param = "city";
    }else if(getUrlParam("cat") != undefined && getUrlParam("cat") != null){
        param = "cat";
    }else if(getUrlParam("keyword") != undefined && getUrlParam("keyword") != null){
        param = "keyword";
    }
    var totalResult = parseInt($("#hideTotalResult").val());
    var totalShowing = parseInt($("#lstDataContent").children().size());
    
    if(totalResult != totalShowing){
        isLoadingMore = true;
        NProgress.configure({ parent: '#nprogressList' });
        NProgress.start();
        $('#nprogressList').find('div.bar').hide();
        $('#nprogressList').find('.spinner').css(SPINNER_CSS);
        $("#nprogressList").show();
        var page = parseInt($("#hideCurrentPage").val());
        page += 1;
        $("#hideCurrentPage").val(page);

        var url = '/list_load_more?'+param+"="+getUrlParam(param)+"&page="+page;
        if (getUrlParam("cat") != undefined && getUrlParam("cat") != null && getUrlParam("cat") != 0) {
            url += '&cat=' + getUrlParam("cat");
        }
        if (getUrlParam("city") != undefined && getUrlParam("city") != null && getUrlParam("city") != 0) {
            url += '&city=' + getUrlParam("city");
        }
        if (getUrlParam("keyword") != undefined && getUrlParam("keyword") != null) {
            url += '&keyword=' + getUrlParam("keyword");
        }
        if (getUrlParam("sort") != undefined && getUrlParam("sort") != null && getUrlParam("sort") != 0) {
            url += '&sort=' + getUrlParam("sort");
        }
        
        $.ajax({
            url : url,
            type:"GET", 
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            processData:false,
            success : function(data) {
                $("#lstDataContent").append(data);
                $('.lazy').lazy({
                    effect : "fadeIn",
                    effectTime : 500,
                    threshold: 50
                });
                $( ".lazy" ).each(function( index ) {
                    if($(this).attr('src') == undefined || $(this).attr('src') == null){
                        $(this).attr('src',window.location.origin+"/img/no-image.gif");
                    }
                });
                $('[data-toggle="tooltip"]').tooltip(); 
                if(totalResult != totalShowing){
                    isLoadingMore = false;
                }else{
                    isLoadingMore = true;
                }
                NProgress.done();
            },
            error : function(data) {
            	NProgress.done();
            	$("#nprogressList").hide();
        	}
        });            

    }else{
        $("#nprogressList").hide();
        isLoadingMore = true;
    }

}