<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    // variable in message :second

    'login' => 'Login',
    'username' => 'Username',
    'password' => 'Password',
    'dont_have_account' => "Don't have account?",
    'forgot_password' => 'Forgot Password',

    // Register
    'register.success' =>  'Successfully Register',

    'failed.username_is_not_exist' => 'These username do not match our records.',
    'failed.wrong_username' => 'The provided username is incorrect.',
    'failed.wrong_password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in few seconds.',

];
