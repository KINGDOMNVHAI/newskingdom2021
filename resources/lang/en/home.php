<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | Multi Languages for homepage
    |
    */

    // Menu
    'about' => 'About',
    'categories' => 'Categories',
    'over_16' => 'Over 16',
    'website_social_network' => 'Website - Social Network',
    'game' => 'Game',
    'anime_manga' => 'Anime - Manga',
    'anime_manga_game' => 'Anime - Manga - Game',
    'it_tips' => 'IT Tips',
    'game_and_girl_16' => 'Game and Girl (16+)',

    // Widget
    'search_posts' => 'Search',
    'all_categories' => 'All Categories',
    'related_posts' => 'Related Posts',
];
