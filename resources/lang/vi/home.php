<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | Multi Languages for homepage
    |
    */

    // Menu
    'about' => 'Giới thiệu',
    'categories' => 'Chuyên mục',
    'over_16' => 'Trên 16 tuổi',
    'website_social_network' => 'Website - Mạng Xã Hội',
    'game' => 'Game',
    'anime_manga' => 'Anime - Manga',
    'anime_manga_game' => 'Anime - Manga - Game',
    'it_tips' => 'Thủ thuật IT',
    'game_and_girl_16' => 'Game và Gái (16+)',

    // Widget
    'search_posts' => 'Tìm bài viết',
    'all_categories' => 'Tất cả chuyên mục',
    'related_posts' => 'Đề xuất',
];
