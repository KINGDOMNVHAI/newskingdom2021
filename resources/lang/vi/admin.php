<?php

return [
    // Menu
    'dashboard' => 'Tổng quan',
    'category' => 'Chuyên mục',
    'post' => 'Bài viết',
    'list_post' => 'Danh sách bài viết',

    'username' => 'Tên đăng nhập',
    'password' => 'Mật khẩu',
];
