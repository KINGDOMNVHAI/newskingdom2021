<!-- header -->
<header class="header-default">
    <nav class="navbar navbar-expand-lg">
        <div class="container-xl">
            <!-- site logo -->
            <a class="navbar-brand" href="{{asset('/')}}"><img src="{{asset('news/images/Mascot-LG-KINGDOMNVHAI-small-blue.png')}}" width="150px" alt="KINGDOM NVHAI" /></a>

            <div class="collapse navbar-collapse">
                <!-- menus -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('about-kingdom-nvhai')}}">Giới thiệu</a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#">Chuyên mục</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('category-page', 'website-mang-xa-hoi' ) }}">{{ __('home.website_social_network') }}</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'game' ) }}">Game</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'anime-manga' ) }}">Anime - Manga</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'thu-thuat-it' ) }}">{{ __('home.it_tips') }}</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#">{{ __('home.over_16') }}</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('category-page', 'game-girl' ) }}">{{ __('home.game_and_girl_16') }}</a></li>
                        </ul>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="{{ KDPLAYBACK_COM_URL }}" target="_blank" title="KDPLAYBACK">VIDEO</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="{{ route('subscribed-youtube-channels-ranking') }}">SUBCRIBE YOUTUBE RANKING</a></li>
                </ul>
            </div>

            <!-- header right section -->
            <div class="header-right">
                <!-- social icons -->
                <ul class="social-icons list-unstyled list-inline mb-0">
                    <li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                    <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li>
                </ul>
                <!-- header buttons -->
                <div class="header-buttons">
                    <!-- <button class="search icon-button"><i class="icon-magnifier"></i></button> -->
                    <button class="burger-menu icon-button"><span class="burger-icon"></span></button>
                </div>
            </div>
        </div>
    </nav>
</header>

<!-- page header -->
<section class="page-header-padding-100" style="background-image:url({{asset('news/images/posts/single-cover.jpg')}})">
    <div class="container-xl">
        <div class="text-center"><h1 class="mt-0 mb-2">Contact</h1></div>
    </div>
</section>
