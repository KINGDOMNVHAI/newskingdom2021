<link rel="shortcut icon" type="image/x-icon" href="{{asset('news/images/favicon.ico')}}">
<!-- =========== SEO =========== -->
<!-- Meta -->
<meta charset=UTF-8 />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noodp,index,follow" />
<meta name="keywords" content="hololive virtual youtuber, pewdiepie kizuna ai, yandere anime, overwatch porn hentai" />
<!-- <link rel=pingback href="xmlrpc.php" /> -->

<meta http-equiv="content-language" content="vi" />
<meta name="dc.title" content="{{$title}}" />
@if(isset($content->present_post))
<meta name="description" content="{{$content->present_post}}" />
@elseif(isset($videoContent->thumbnail_video))
<meta name="description" content="{{$videoContent->thumbnail_video}}" />
@else
<meta name="description" content="welcome to NEWS KINGDOM NVHAI, where you can find news about anime manga game and more" />
@endif
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel=canonical href="https://kingdomnvhai.info/" />
<meta property="og:locale" content=en_US />
<meta property="og:type" content="article">
<meta property="og:title" content="{{$title}}" />
@if(isset($content->thumbnail_post))
<meta property="og:image" content="{{ "http://" . $_SERVER['SERVER_NAME'] . "/upload/images/thumbnail/" . $content->thumbnail_post }}" />
<meta property="og:description" content="{{$content->present_post}}" />
@elseif(isset($videoContent->thumbnail_video))
<meta property="og:image" content="{{ "http://" . $_SERVER['SERVER_NAME'] . "/upload/images/video/" . $videoContent->thumbnail_video }}" />
<meta property="og:description" content="{{$videoContent->description_video}}" />
@else
<meta property="og:description" content="Welcome to NEWS KINGDOM NVHAI. You can find news about Anime Manga Game and more" />
@endif
<meta property="og:url" content="{{ "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] }}" />
<meta property="og:site_name" content="KINGDOM NVHAI" />

<!-- Xem thẻ rel=alternate -->

<!-- Google Analytics
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-65258802-1', 'auto');
ga('send', 'pageview');
</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-H3FQ5E3S6W"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-H3FQ5E3S6W');
</script>

<!-- Fanpage -->
<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!-- Google Webmaster Tool -->

<!-- Token Laravel -->
<meta name="csrf-token" content="{{ csrf_token() }}">
