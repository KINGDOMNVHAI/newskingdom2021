<!-- sidebar -->
<div class="sidebar">
    <!-- widget about -->
    <div class="widget rounded">
        <div class="widget-about data-bg-image text-center" data-bg-image="{{asset('news/images/map-bg.png')}}">
            <img src="{{asset('upload/images/channel/600x400/' . $channelInfo->thumbnail_channel)}}" alt="{{$channelInfo->name_channel}}" class="mb-4" />
            <h3 class="mb-4">{{$channelInfo->name_channel}}</h3>
            <ul class="social-icons list-unstyled list-inline mb-0">
                @if ($channelInfo->facebook_channel != null)
                    <li class="list-inline-item"><a href="{{$channelInfo->facebook_channel}}" target="_blank" title="{{$channelInfo->name_channel}}"><i class="fab fa-facebook-f"></i></a></li>
                @elseif ($channelInfo->twitter_channel != null)
                    <li class="list-inline-item"><a href="{{$channelInfo->twitter_channel}}" target="_blank" title="{{$channelInfo->name_channel}}"><i class="fab fa-twitter"></i></a></li>
                @elseif ($channelInfo->instagram_channel != null)
                    <li class="list-inline-item"><a href="{{$channelInfo->instagram_channel}}" target="_blank" title="{{$channelInfo->name_channel}}"><i class="fab fa-instagram"></i></a></li>
                @elseif ($channelInfo->patreon_channel != null)
                    <li class="list-inline-item"><a href="{{$channelInfo->patreon_channel}}" target="_blank" title="{{$channelInfo->name_channel}}"><i class="fab fa-patreon"></i></a></li>
                @endif
            </ul>
        </div>
    </div>

    <!-- widget categories -->
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">Social Network</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            <ul class="list">
                <li><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI">FACEBOOK</a><span>({{$facebookAPI}})</span></li>
                <li><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI">TWITTER</a><span>({{$twitterAPI}})</span></li>
                <li><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI">YOUTUBE</a><span>({{$youtubeAPI}})</span></li>
                <!-- <li><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI">VIMEO</a><span>(10)</span></li> -->
            </ul>
        </div>
    </div>

    <!-- widget newsletter
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">Searching Video</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            <form action="{{ route('list-search-post') }}" method="GET">
                <div class="mb-2"><input type="search" name="keyword" class="form-control w-100 text-center" placeholder="Keyword"></div>
                <div class="mb-2">
                <select name="category" class="form-control w-100 text-center">
                    <option value="all">Tất cả chuyên mục</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->id_cat_post }}">{{$category->name_cat_post}}</option>
                    @endforeach
                </select>
                </div>
                <div class="mb-2">
                <select name="date" class="form-control w-100 text-center">
                    <option value="desc">Mới nhất</option>
                    <option value="asc">Cũ nhất</option>
                </select>
                </div>
                <button type="submit" class="btn btn-default btn-full">Search</button>
            </form>
        </div>
    </div> -->

    <!-- widget tags -->
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">Tag Clouds</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            @for($i = 0; $i < count($viewTags); $i++)
            <a href="{{ route('list-search-post', 'keyword=' . $viewTags[$i] . '&category=all&date=desc&search=Search' ) }}" class="tag">{{$viewTags[$i]}}</a>
            @endfor
        </div>
    </div>
</div>
