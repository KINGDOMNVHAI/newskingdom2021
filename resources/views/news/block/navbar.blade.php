<!-- header -->
<header class="header-default">
    <nav class="navbar navbar-expand-lg">
        <div class="container-xl">
            <!-- site logo -->
            <a class="navbar-brand" href="{{asset('/')}}"><img src="{{asset('news/images/Mascot-LG-KINGDOMNVHAI-small-blue.png')}}" width="150px" alt="KINGDOM NVHAI" /></a>

            <div class="collapse navbar-collapse">
                <!-- menus -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="{{route('about-kingdom-nvhai')}}">{{ __('home.about') }}</a></li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#">{{ __('home.categories') }}</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('category-page', 'website-mang-xa-hoi' ) }}">{{ __('home.website_social_network') }}</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'game' ) }}">Game</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'anime-manga' ) }}">Anime - Manga</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'thu-thuat-it' ) }}">{{ __('home.it_tips') }}</a></li>
                            <li><a class="dropdown-item" href="{{ route('category-page', 'game-girl' ) }}">{{ __('home.game_and_girl_16') }}</a></li>
                        </ul>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="{{ KDPLAYBACK_COM_URL }}" target="_blank" title="KDPLAYBACK">VIDEO</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="{{ route('subscribed-youtube-channels-ranking') }}">SUBCRIBE YOUTUBE RANKING</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('login')}}">LOGIN</a></li>
                </ul>
            </div>

            <!-- header right section -->
            <div class="header-right">
                <!-- social icons -->
                <select id="selectLang" onchange="changeLang(value);">
                    @if ($language == 'en')
                        <option value="vi">Vietnam</option>
                        <option value="en" selected>English</option>
                    @else
                        <option value="vi" selected>Vietnam</option>
                        <option value="en">English</option>
                    @endif
                </select>
                <!-- header buttons -->
                <div class="header-buttons">
                    <!-- <button class="search icon-button"><i class="icon-magnifier"></i></button> -->
                    <button class="burger-menu icon-button"><span class="burger-icon"></span></button>
                </div>
            </div>
        </div>
    </nav>
</header>
