<!-- STYLES -->
<link rel="stylesheet" href="{{asset('news/css/bootstrap.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{asset('news/css/all.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{asset('news/css/slick.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{asset('news/css/simple-line-icons.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{asset('news/css/style.css')}}" type="text/css" media="all">
