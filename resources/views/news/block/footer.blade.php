    <!-- footer -->
    <footer>
        <div class="container-xl">
            <div class="footer-inner">
                <div class="row d-flex align-items-center gy-4">
                    <!-- copyright text -->
                    <div class="col-md-4">
                        <span class="copyright">© KINGDOM NVHAI 2021. Website developed by KINGDOM NVHAI. Theme Katen of ThemeGer.</span>
                    </div>
                    <!-- social icons -->
                    <div class="col-md-4 text-center">
                        <ul class="social-icons list-unstyled list-inline mb-0">
							<li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
							<li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
							<li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                    		<li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li>
                        </ul>
                    </div>
                    <!-- go to top button -->
                    <div class="col-md-4">
                        <a href="#" id="return-to-top" class="float-md-end"><i class="icon-arrow-up"></i>Back to Top</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div><!-- end site wrapper -->

<!-- search popup area -->
<div class="search-popup">
	<!-- close button -->
	<button type="button" class="btn-close" aria-label="Close"></button>
	<!-- content -->
	<div class="search-content">
		<div class="text-center">
			<h3 class="mb-4 mt-0">Press ESC to close</h3>
		</div>
		<!-- form -->
		<form class="d-flex search-form">
			<input type="text" class="form-control me-2" placeholder="Search and press enter ..." aria-label="Search">
			<input type="search" class="form-control me-2" placeholder="Search and press enter ..." aria-label="Search">
			<button class="btn btn-default btn-lg" type="submit"><i class="icon-magnifier"></i></button>
		</form>
	</div>
</div>

<!-- canvas menu -->
<div class="canvas-menu d-flex align-items-end flex-column">
	<!-- close button -->
	<button type="button" class="btn-close" aria-label="Close"></button>
	<!-- logo -->
	<div class="logo"><img src="{{asset('news/images/Mascot-LG-KINGDOMNVHAI-small-blue.png')}}" alt="KINGDOM NVHAI" /></div>

	<!-- menu mobile -->
	<nav>
		<ul class="vertical-menu">
			<li><a href="{{route('about-kingdom-nvhai')}}">Giới thiệu</a></li>
			<li>
				<a href="#">Chuyên mục</a>
				<ul class="submenu">
					<li><a href="{{ route('category-page', 'website-mang-xa-hoi' ) }}">{{ __('home.website_social_network') }}</a></li>
					<li><a href="{{ route('category-page', 'game' ) }}">Game</a></li>
					<li><a href="{{ route('category-page', 'anime-manga' ) }}">Anime - Manga</a></li>
					<li><a href="{{ route('category-page', 'thu-thuat-it' ) }}">{{ __('home.it_tips') }}</a></li>
				</ul>
			</li>
			<li><a href="{{ route('subscribed-youtube-channels-ranking') }}">SUBCRIBE YOUTUBE RANKING</a></li>
			<li><a href="{{ route('video-page') }}">VIDEO</a></li>
		</ul>
	</nav>

	<!-- social icons -->
	<ul class="social-icons list-unstyled list-inline mb-0 mt-auto w-100">
		<li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
		<li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
		<li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
		<li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li>
	</ul>
</div>

<!-- JAVA SCRIPTS -->
<script src="{{asset('news/js/jquery.min.js')}}"></script>
<script src="{{asset('news/js/popper.min.js')}}"></script>
<script src="{{asset('news/js/bootstrap.min.js')}}"></script>
<script src="{{asset('news/js/slick.min.js')}}"></script>
<script src="{{asset('news/js/jquery.sticky-sidebar.min.js')}}"></script>
<script src="{{asset('news/js/custom.js')}}"></script>

<script type="text/javascript">
    function changeLang(value) {
		// let url = 'https://kingdomnvhai.info/change-locale/' + value;
		let url = 'http://localhost/newskingdom2021/public/change-locale/' + value;
        window.location.href = url;
    }
</script>
