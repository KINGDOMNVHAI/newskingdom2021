
<div class="container-xl header-top">
    <div class="row align-items-center">
        <div class="col-4 d-none d-md-block d-lg-block">
            <!-- social icons -->
            <ul class="social-icons list-unstyled list-inline mb-0">
                <li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
                <li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
                <li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li>
            </ul>
        </div>

        <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="margin:5px 0px;">
            <a class="navbar-brand" href="{{asset('/')}}"><img src="{{asset('news/images/Mascot-LG-KINGDOMNVHAI-square-blue.png')}}" alt="logo" width="100px" /></a>
            <!-- <span class="slogan d-block">THAY HÌNH NÀY BẰNG LOGO NVHAI KHÔNG CHỮ</span> -->
        </div>

        <div class="col-md-4 col-sm-12 col-xs-12">
            <!-- header buttons -->
            <div class="header-buttons float-md-end mt-4 mt-md-0">
                <!-- <button class="search icon-button"><i class="icon-magnifier"></i></button> -->
                <button class="burger-menu icon-button ms-2 float-end float-md-none"><span class="burger-icon"></span></button>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg">
    <div class="container-xl">
        <div class="collapse navbar-collapse justify-content-center centered-nav">
            <!-- menus -->
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link" href="{{route('about-kingdom-nvhai')}}">Giới thiệu</a></li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#">Chuyên mục</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('category-page', 'website-mang-xa-hoi' ) }}">{{ __('home.website_social_network') }}</a></li>
                        <li><a class="dropdown-item" href="{{ route('category-page', 'game' ) }}">Game</a></li>
                        <li><a class="dropdown-item" href="{{ route('category-page', 'anime-manga' ) }}">Anime - Manga</a></li>
                        <li><a class="dropdown-item" href="{{ route('category-page', 'thu-thuat-it' ) }}">{{ __('home.it_tips') }}</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#">{{ __('home.over_16') }}</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('category-page', 'game-girl' ) }}">{{ __('home.game_and_girl_16') }}</a></li>
                    </ul>
                </li>
                <!-- <li class="nav-item"><a class="nav-link" href="{{ KDPLAYBACK_COM_URL }}" target="_blank" title="KDPLAYBACK">VIDEO</a></li> -->
                <li class="nav-item"><a class="nav-link" href="{{ route('subscribed-youtube-channels-ranking') }}">SUBCRIBE YOUTUBE RANKING</a></li>
            </ul>
        </div>
    </div>
</nav>
</header>

<!-- cover header -->
<section class="single-cover data-bg-image" data-bg-image="{{asset('news/images/bg-cover.jpg')}}">
    <div class="container-xl">
        <div class="cover-content post text-center">
            <h1 class="title mt-0 mb-3">SUBCRIBE YOUTUBE RANKING</h1>
        </div>
    </div>
</section>
