<!-- sidebar -->
<div class="sidebar">
    <!-- widget about -->
    <div class="widget rounded">
        <div class="widget-about data-bg-image text-center" data-bg-image="{{asset('news/images/map-bg.png')}}">
            <img src="{{asset('news/images/Mascot-LG-KINGDOMNVHAI-small-blue.png')}}" alt="KINGDOM NVHAI" class="mb-4" />
            <p class="mb-4">Greeting, traveler! KINGDOM NVHAI is content writer who is fascinated by content website, social network, anime, manga, game and IT tips. We helps clients bring the right content to the right people.</p>
            <ul class="social-icons list-unstyled list-inline mb-0">
                <li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
                <li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
                <li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                <!-- <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li> -->
                <!-- <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fas fa-chess-knight"></i></a></li> -->
                <!-- <li class="list-inline-item"><a href="{{TWITCH_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitch"></i></a></li> -->
            </ul>
        </div>
    </div>

    <!-- widget categories -->
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">Social Network</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            <a href="https://www.alexa.com/siteinfo/kingdomnvhai.info"><script type="text/javascript" src="http://xslt.alexa.com/site_stats/js/t/a?url=wpism.com"></script></a>
            <ul class="list">
                <li><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI">FACEBOOK</a><span>({{$facebookAPI}})</span></li>
                <li><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI">TWITTER</a><span>({{$twitterAPI}})</span></li>
                <li><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI">YOUTUBE</a><span>({{$youtubeAPI}})</span></li>
                <!-- <li><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI">VIMEO</a><span>(10)</span></li> -->
                <!-- <li><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI">F2C</a><span>()</span></li> -->
                <!-- <li><a href="{{TWITCH_URL}}" target="_blank" title="KINGDOM NVHAI">TWITCH</a><span>()</span></li> -->
            </ul>
        </div>
    </div>

    <!-- widget newsletter -->
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">{{__('home.search_posts')}}</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            <!-- <span class="newsletter-headline text-center mb-3">Join 70,000 subscribers!</span> -->
            <form action="{{ route('list-search-post') }}" method="GET">
                <div class="mb-2"><input type="search" name="keyword" class="form-control w-100 text-center" placeholder="Keyword"></div>
                <div class="mb-2">
                <select name="category" class="form-control w-100 text-center">
                    <option value="all">{{__('home.all_categories')}}</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->id_cat_post }}">{{$category->name_cat_post}}</option>
                    @endforeach
                </select>
                </div>
                <div class="mb-2">
                <select name="date" class="form-control w-100 text-center">
                    <option value="desc">Mới nhất</option>
                    <option value="asc">Cũ nhất</option>
                </select>
                </div>
                <button type="submit" class="btn btn-default btn-full">Search</button>
            </form>
            <!-- <span class="newsletter-privacy text-center mt-3"><a href="#">Privacy Policy</a></span> -->
        </div>
    </div>

    <a href="{{AD_CHAN_NUOI_92}}" target="_blank" title="{{AD_CHAN_NUOI_92_ALT}}">
    <img src="{{asset('news/images/ads/ads-chan-nuoi-92-360.jpg')}}" alt="{{AD_CHAN_NUOI_92_ALT}}" width="300px"></a>

    <!-- widget post carousel -->
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">{{__('home.related_posts')}}</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            @foreach($randoms as $random)
            <div class="post">
                <div class="thumb rounded">
                    <span class="post-format"><i class="icon-picture"></i></span>
                    <a href="{{ route('post-content', $random->url_post ) }}">
                        <div class="inner">
                            @if($random->thumbnail_post && file_exists('upload/images/thumbnail/' . $random->thumbnail_post))
                            <img src="{{ asset('upload/images/thumbnail/' . $random->thumbnail_post) }}" alt="{{$random->name_post}}" title="{{$random->name_post}}" />
                            @else
                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$random->name_post}}" title="{{$random->name_post}}" />
                            @endif
                        </div>
                    </a>
                </div>
                <ul class="meta list-inline mt-4 mb-0">
                    <li class="list-inline-item">{{$random->date_post}}</li>
                </ul>
                <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $random->url_post ) }}">{{$random->name_post}}</a></h5>
            </div>
            @endforeach
        </div>
    </div>

    <!-- widget tags -->
    <div class="widget rounded">
        <div class="widget-header text-center">
            <h3 class="widget-title">Tag Clouds</h3>
            <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
        </div>
        <div class="widget-content">
            @for($i = 0; $i < count($viewTags); $i++)
            <a href="{{ route('list-search-post', 'keyword=' . $viewTags[$i] . '&category=all&date=desc&search=Search' ) }}" class="tag">{{$viewTags[$i]}}</a>
            @endfor
        </div>
    </div>
</div>
