
<!-- hero section -->
<section id="hero">
    <div class="container-xl">
        <div class="row gy-4">
            <div class="padding-30 rounded bordered">
                <div class="row gy-5">
                    <div class="col-sm-4">
                        <div class="post">
                            <div class="thumb rounded">
                                <a href="category.html" class="category-badge position-absolute">Lifestyle</a>
                                <span class="post-format">
                                    <i class="icon-picture"></i>
                                </span>
                                <a href="{{ route('post-content', $newest[0]['url_detailpost'] ) }}" title="{{ $newestPost[0]['name_detailpost'] }}">
                                    <div class="inner">
                                        @if($newestPost[0]['img_detailpost'] && file_exists('upload/images/thumbnail/' . $newestPost[0]['img_detailpost']))
                                        <img src="upload/images/thumbnail/{{ $newestPost[0]['img_detailpost'] }}" alt="{{ $newestPost[0]['name_detailpost'] }}" />
                                        @else
                                        <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                                        @endif
                                    </div>
                                </a>
                            </div>
                            <ul class="meta list-inline mt-4 mb-0">
                                <li class="list-inline-item">{{ $newestPost[0]['date_detailpost'] }}</li>
                            </ul>
                            <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $newest[0]['url_detailpost'] ) }}">{{ $newestPost[0]['name_detailpost'] }}</a></h5>
                            <p class="excerpt mb-0">{{ $newestPost[0]['present_detailpost'] }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        @for($i = 0; $i < count($listNewestPost1); $i++)
                        <div class="post post-list-sm square">
                            <div class="thumb rounded">
                                <a href="{{ route('post-content', $listNewestPost1[$i]['url_detailpost'] ) }}">
                                    <div class="inner">
                                        @if($listNewestPost1[$i]['img_detailpost'] && file_exists('upload/images/thumbnail/' . $listNewestPost1[$i]['img_detailpost']))
                                        <img src="upload/images/thumbnail/{{ $listNewestPost1[$i]['img_detailpost'] }}" alt="{{ $listNewestPost1[$i]['name_detailpost'] }}" />
                                        @else
                                        <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                                        @endif
                                    </div>
                                </a>
                            </div>
                            <div class="details clearfix">
                                <h6 class="post-title my-0"><a href="{{ route('post-content', $listNewestPost1[$i]['url_detailpost'] ) }}">{{ $listNewestPost1[$i]['name_detailpost'] }}</a></h6>
                                <ul class="meta list-inline mt-1 mb-0">
                                    <li class="list-inline-item">{{ $listNewestPost1[$i]['date_detailpost'] }}</li>
                                </ul>
                            </div>
                        </div>
                        @endfor
                    </div>
                    <div class="col-sm-4">
                        @for($i = 0; $i < count($listNewestPost2); $i++)
                        <div class="post post-list-sm square">
                            <div class="thumb rounded">
                                <a href="{{ route('post-content', $listNewestPost2[$i]['url_detailpost'] ) }}">
                                    <div class="inner">
                                        @if($listNewestPost2[$i]['img_detailpost'] && file_exists('upload/images/thumbnail/' . $listNewestPost2[$i]['img_detailpost']))
                                        <img src="upload/images/thumbnail/{{ $listNewestPost2[$i]['img_detailpost'] }}" alt="{{ $listNewestPost2[$i]['name_detailpost'] }}" />
                                        @else
                                        <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                                        @endif
                                    </div>
                                </a>
                            </div>
                            <div class="details clearfix">
                                <h6 class="post-title my-0"><a href="{{ route('post-content', $listNewestPost2[$i]['url_detailpost'] ) }}">{{ $listNewestPost2[$i]['name_detailpost'] }}</a></h6>
                                <ul class="meta list-inline mt-1 mb-0">
                                    <li class="list-inline-item">{{ $listNewestPost2[$i]['date_detailpost'] }}</li>
                                </ul>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
