@extends('news.master')
@section('content')
@include('news.block.navbar')

<!-- section main content -->
<section class="main-content mt-3">
    <div class="container-xl">
        <div class="row gy-4">
            <img src="{{asset('news/images/no-thumb/404-error.jpg')}}" width="100%" />
        </div>
    </div>
</section>

@endsection

@section('js')
@endsection
