@extends('news.master')
@section('content')
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4172631569878022" crossorigin="anonymous"></script>
@include('news.block.navbar')

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <!-- section header -->
        <div class="section-header">
            <center>
                <h3 class="section-title">NEWEST VIDEOS</h3>
                <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
            </center>
        </div>
        <div class="row gy-4">
            @foreach($videoNewest as $video)
            <div class="col-sm-3">
                <div class="post post-grid rounded bordered" style="min-height:350px;">
                    <div class="thumb top-rounded">
                        <!-- <a href="category.html" class="category-badge position-absolute">29 March 2021</a>
                        <p class="category-badge position-absolute" style="top:80%;">20213</p> -->
                        <a href="{{route('video-watch', $video['url_video'])}}">
                            <div class="inner">
                                @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                @else
                                <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="details" style="padding:20px;">
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="{{route('channel', $video['id_channel'])}}">
                                    <img src="{{asset('upload/images/channel/100x100/' . $video['thumbnail_channel'])}}" class="author" alt="{{$video['name_channel']}}" width="25px" />{{$video['name_channel']}}
                                </a>
                            </li>
                        </ul>
                        <h5 class="post-title mb-3 mt-3" style="font-size:14px;">{{$video['name_video']}}</h5>
                        <p>{{$video['date_video']}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="spacer" data-height="50"></div>

        <!-- section header -->
        <div class="section-header">
            <center>
                <h3 class="section-title"><a href="{{route('channel', CHANNEL_HOLOLIVE)}}">HOLOLIVE</a></h3>
                <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
            </center>
        </div>
        <div class="row gy-4">
            @foreach($videoHololive as $video)
            <div class="col-sm-3">
                <div class="post post-grid rounded bordered" style="min-height:350px;">
                    <div class="thumb top-rounded">
                        <!-- <a href="category.html" class="category-badge position-absolute">29 March 2021</a>
                        <p class="category-badge position-absolute" style="top:80%;">20213</p> -->
                        <a href="{{route('video-watch', $video['url_video'])}}">
                            <div class="inner">
                                @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                @else
                                <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="details" style="padding:20px;">
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="{{route('channel', $video['id_channel'])}}">
                                    <img src="{{asset('upload/images/channel/100x100/' . $video['thumbnail_channel'])}}" class="author" alt="{{$video['name_channel']}}" width="25px" />{{$video['name_channel']}}
                                </a>
                            </li>
                        </ul>
                        <h5 class="post-title mb-3 mt-3" style="font-size:14px;">{{$video['name_video']}}</h5>
                        <p>{{$video['date_video']}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="spacer" data-height="50"></div>

        <!-- section header -->
        <div class="section-header">
            <center>
                <h3 class="section-title"><a href="{{route('channel', CHANNEL_KINGDOM_NVHAI)}}">KINGDOM NVHAI</a></h3>
                <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
            </center>
        </div>
        <div class="row gy-4">
            @foreach($videoKingdomnvhai as $video)
            <div class="col-sm-3">
                <div class="post post-grid rounded bordered" style="min-height:350px;">
                    <div class="thumb top-rounded">
                        <!-- <a href="category.html" class="category-badge position-absolute">29 March 2021</a>
                        <p class="category-badge position-absolute" style="top:80%;">20213</p> -->
                        <a href="{{route('video-watch', $video['url_video'])}}">
                            <div class="inner">
                                @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                @else
                                <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="details" style="padding:20px;">
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="{{route('channel', $video['id_channel'])}}">
                                    <img src="{{asset('upload/images/channel/100x100/' . $video['thumbnail_channel'])}}" class="author" alt="{{$video['name_channel']}}" width="25px" />{{$video['name_channel']}}
                                </a>
                            </li>
                        </ul>
                        <h5 class="post-title mb-3 mt-3" style="font-size:14px;">{{$video['name_video']}}</h5>
                        <p>{{$video['date_video']}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="spacer" data-height="50"></div>

        <!-- section header -->
        <div class="section-header">
            <center>
                <h3 class="section-title"><a href="{{route('channel', CHANNEL_KIZUNA_AI)}}">Kizuna AI</a></h3>
                <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
            </center>
        </div>
        <div class="row gy-4">
            @foreach($videoKizunaAI as $video)
            <div class="col-sm-3">
                <div class="post post-grid rounded bordered" style="min-height:350px;">
                    <div class="thumb top-rounded">
                        <!-- <a href="category.html" class="category-badge position-absolute">29 March 2021</a>
                        <p class="category-badge position-absolute" style="top:80%;">20213</p> -->
                        <a href="{{route('video-watch', $video['url_video'])}}">
                            <div class="inner">
                                @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                @else
                                <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="details" style="padding:20px;">
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="{{route('channel', $video['id_channel'])}}">
                                    <img src="{{asset('upload/images/channel/100x100/' . $video['thumbnail_channel'])}}" class="author" alt="{{$video['name_channel']}}" width="25px" />{{$video['name_channel']}}
                                </a>
                            </li>
                        </ul>
                        <h5 class="post-title mb-3 mt-3" style="font-size:14px;">{{$video['name_video']}}</h5>
                        <p>{{$video['date_video']}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="spacer" data-height="50"></div>

        <!-- section header -->
        <div class="section-header">
            <center>
                <h3 class="section-title"><a href="{{route('channel', CHANNEL_PEWDIEPIE)}}">Pewdiepie</a></h3>
                <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
            </center>
        </div>
        <div class="row gy-4">
            @foreach($videoPewdiepie as $video)
            <div class="col-sm-3">
                <div class="post post-grid rounded bordered" style="min-height:350px;">
                    <div class="thumb top-rounded">
                        <!-- <a href="category.html" class="category-badge position-absolute">29 March 2021</a>
                        <p class="category-badge position-absolute" style="top:80%;">20213</p> -->
                        <a href="{{route('video-watch', $video['url_video'])}}">
                            <div class="inner">
                                @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                @else
                                <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="details" style="padding:20px;">
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="{{route('channel', $video['id_channel'])}}">
                                    <img src="{{asset('upload/images/channel/100x100/' . $video['thumbnail_channel'])}}" class="author" alt="{{$video['name_channel']}}" width="25px" />{{$video['name_channel']}}
                                </a>
                            </li>
                        </ul>
                        <h5 class="post-title mb-3 mt-3" style="font-size:14px;">{{$video['name_video']}}</h5>
                        <p>{{$video['date_video']}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
