@extends('news.master')
@section('content')

@include('news.block.navbar')

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <div class="row gy-4">
            <h2>Từ khóa: {{ $keyword }}</h2>
            <div class="col-lg-8">
                <div class="row gy-4">
                    @foreach($listpost as $post)
                    <div class="col-sm-6">
                        <!-- post -->
                        <div class="post post-grid rounded bordered">
                            <div class="thumb top-rounded">
                                <span class="post-format"><i class="icon-picture"></i></span>
                                <a href="{{ route('post-content', $post->url_post ) }}">
                                    <div class="inner">
                                        <a href="{{ route('post-content', $post->url_post ) }}">
                                        @if($post->thumbnail_post && file_exists('upload/images/thumbnail/' . $post->thumbnail_post))
                                        <img src="../upload/images/thumbnail/{{ $post->thumbnail_post }}" alt="{{ $post->name_post }}" title="{{ $post->name_post }}"/>
                                        @else
                                        <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                                        @endif
                                        </a>
                                    </div>
                                </a>
                            </div>
                            <div class="details">
                                <ul class="meta list-inline mb-0"><li class="list-inline-item">{{ $post->date_post }}</li></ul>
                                <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $post->url_post ) }}">{{ $post->name_post }}</a></h5>
                                <p class="excerpt mb-0">{{ $post->present_post }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- <nav>
                    <ul class="pagination justify-content-center">
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">1</span>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                    </ul>
                </nav> -->

            </div>
            <div class="col-lg-4">
                @include('news.block.widget')
            </div>
        </div>
    </div>
</section>

@endsection
