@extends('news.master')
@section('content')

@include('news.block.navbar')

<!-- section main content -->
<section class="main-content mt-3">
    <div class="container-xl">
        <div class="row gy-4">
            <div class="col-lg-8">
                <!-- post single -->
                <div class="post post-single">
                    <!-- post header -->
                    <div class="post-header">
                        <h1 class="title mt-0 mb-3">{{$content->name_detailpost}}</h1>
                        <h1 style="display:none">{{$content->name_hidden_post}}</h1>
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item"><a href="{{ route('category-page', $content->url_cat_post) }}">{{$content->name_cat_post}}</a></li>
                            <li class="list-inline-item">{{$content->date_post}}</li>
                        </ul>
                    </div>
                    <!-- post content -->
                    <div class="post-content clearfix">
                        <p style="16px;"><b>
                            <div id="getRequestDataPresent">
                            {!! $content->present_detailpost !!}
                            </div>
                        </b></p>

                        <p style="display:none">{{$content->present_hidden_post}}</p>

                        @if ($contentExist == true)
                            <button onclick="contentAjax('vi')" style="margin-right:5px;">Vietnam</button>
                            <button onclick="contentAjax('en')">English</button><br>
                        @endif

                        <div id="getRequestDataContent">
                            {!! $content->content_detailpost !!}
                        </div>
                    </div>
                    <!-- post bottom section -->
                    <div class="post-bottom">
                        <div class="row d-flex align-items-center">
                            <div class="col-md-6 col-12 text-center text-md-start">
                                <!-- tags
                                <a href="#" class="tag">#Trending</a>
                                <a href="#" class="tag">#Video</a>
                                <a href="#" class="tag">#Featured</a> -->
                            </div>
                            <div class="col-md-6 col-12">
                                <!-- social icons -->
                                <ul class="social-icons list-unstyled list-inline mb-0 float-md-end">
                                    <li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                                    <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="spacer" data-height="50"></div>
                <center><br><br><a href="{{AD_NHATHUOC}}" target="_blank" title="{{AD_NHATHUOC_ALT}}">
                <img src="../news/images/ads/ads-medical-720.jpg" alt="Nhà thuốc online 24h" width="720px"></a>
                <br><br></center>

                <div class="about-author padding-30 rounded">
                    <div class="thumb">
                        <img src="{{asset('news/kd-nvhai/avatar-kiryu-coco.jpg')}}" alt="avatar kingdom nvhai" />
                    </div>
                    <div class="details">
                        <h4 class="name"><a href="#">NVHAI</a></h4>
                        <p>Everything will be 大丈夫</p>
                        <!-- social icons -->
                        <ul class="social-icons list-unstyled list-inline mb-0">
                            <li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                            <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                @include('news.block.widget')
            </div>
        </div>
    </div>
</section>

<script src="{{asset('/news/kd-nvhai/jquery-1.11.3.js')}}"></script>
<script type="text/javascript">
    function contentAjax(lang)
    {
        var urlPost = <?php echo json_encode($content->url_post) ?>;
        console.log(urlPost);
        console.log(lang);
        //tren localhost phai them link localhost
        var url = '/content-language/' + lang + '/' + urlPost;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'POST',
            dataType: 'JSON',
            url: url,
            data:{
                lang:lang.trim(),
                urlPost:urlPost.trim()
            },
            success:function(data){
        //         $.hideLoading($('#getRequestData'));
                $('#getRequestDataPresent').html(data.htmlPresent);
                $('#getRequestDataContent').html(data.htmlContent);
        //         $('#getListSeeAll').html(data.seeAll);
                console.log(lang);
            },
            error:function(xhr, data){
                // $.hideLoading($('#getRequestData'));
                console.log(xhr);
            }
        });
    }
</script>
@endsection
