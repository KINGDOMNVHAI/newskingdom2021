@extends('news.master')
@section('content')
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4172631569878022" crossorigin="anonymous"></script>
@include('news.block.navbar')

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <div class="row gy-4">
            <div class="col-lg-9">
                <!-- featured post large -->
                <div class="post featured-post-lg">
                    @if ($videoContent->embed_link_1 != null)
                        {!! $videoContent->embed_link_1 !!}
                    @elseif ($videoContent->embed_link_2 != null)
                        {!! $videoContent->embed_link_2 !!}
                    @elseif ($videoContent->embed_link_3 != null)
                        {!! $videoContent->embed_link_3 !!}
                    @elseif ($videoContent->embed_link_4 != null)
                        {!! $videoContent->embed_link_4 !!}
                    @endif
                </div>

                <div class="about-author padding-30 rounded">
                    <div class="thumb">
                        <img src="{{asset('upload/images/channel/100x100/' . $videoContent->thumbnail_channel)}}" alt="{{$videoContent->name_channel}}" />
                    </div>
                    <div class="details">
                        <h4 class="name">{{$videoContent->name_video}}</h4>
                        <p>{!! $videoContent->description_video !!}</p>
                        <!-- social icons -->
                        <ul class="social-icons list-unstyled list-inline mb-0">
                            <li class="list-inline-item"><a href="{{$videoContent->url_channel}}" target="_blank" title="{{$videoContent->name_channel}}"><i class="fab fa-youtube"></i></a></li>
                        @if ($videoContent->facebook_channel != null)
                            <li class="list-inline-item"><a href="{{$videoContent->facebook_channel}}" target="_blank" title="{{$videoContent->name_channel}}"><i class="fab fa-facebook-f"></i></a></li>
                        @elseif ($videoContent->twitter_channel != null)
                            <li class="list-inline-item"><a href="{{$videoContent->twitter_channel}}" target="_blank" title="{{$videoContent->name_channel}}"><i class="fab fa-twitter"></i></a></li>
                        @elseif ($videoContent->instagram_channel != null)
                            <li class="list-inline-item"><a href="{{$videoContent->instagram_channel}}" target="_blank" title="{{$videoContent->name_channel}}"><i class="fab fa-instagram"></i></a></li>
                        @elseif ($videoContent->patreon_channel != null)
                            <li class="list-inline-item"><a href="{{$videoContent->patreon_channel}}" target="_blank" title="{{$videoContent->name_channel}}"><i class="fab fa-patreon"></i></a></li>
                        @endif
                        </ul>
                    </div>
                </div>

                @include('news.block.ads-rows')

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <!-- <div class="section-header">
                    <h3 class="section-title">Leave Comment</h3>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                </div>

                <div class="comment-form rounded bordered padding-30">
                    <form id="comment-form" class="comment-form" method="post">
                        <div class="messages"></div>
                        <div class="row">
                            <div class="column col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="InputName" name="InputName" placeholder="Your name" required="required">
                                </div>
                            </div>
                            <div class="column col-md-12">
                                <div class="form-group">
                                    <textarea name="InputComment" id="InputComment" class="form-control" rows="4" placeholder="Your comment here..." required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-default">Submit</button>
                    </form>
                </div> -->

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h3 class="section-title">Comments (3)</h3>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                </div>
                <!-- post comments -->
                <div class="comments bordered padding-30 rounded">
                    <ul class="comments">
                        <!-- comment item -->
                        <li class="comment rounded">
                            <div class="thumb">
                                <img src="{{asset('news/images/other/comment-1.png')}}" alt="John Doe" />
                            </div>
                            <div class="details">
                                <h4 class="name"><a href="#">John Doe</a></h4>
                                <span class="date">Jan 08, 2021 14:41 pm</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae odio ut tortor fringilla cursus sed quis odio.</p>
                                <a href="#" class="btn btn-default btn-sm">Reply</a>
                            </div>
                        </li>
                        <!-- comment item -->
                        <li class="comment child rounded">
                            <div class="thumb">
                                <img src="{{asset('news/images/other/comment-2.png')}}" alt="John Doe" />
                            </div>
                            <div class="details">
                                <h4 class="name"><a href="#">Helen Doe</a></h4>
                                <span class="date">Jan 08, 2021 14:41 pm</span>
                                <p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
                                <a href="#" class="btn btn-default btn-sm">Reply</a>
                            </div>
                        </li>
                        <!-- comment item -->
                        <li class="comment rounded">
                            <div class="thumb">
                                <img src="{{asset('news/images/other/comment-3.png')}}" alt="John Doe" />
                            </div>
                            <div class="details">
                                <h4 class="name"><a href="#">Anna Doe</a></h4>
                                <span class="date">Jan 08, 2021 14:41 pm</span>
                                <p>Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</p>
                                <a href="#" class="btn btn-default btn-sm">Reply</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <!-- sidebar -->
                <!-- <div class="sidebar">
                    <!-- widget post carousel
                    <div class="widget rounded">
                        <div class="widget-content">
                            <div class="post">
                                <div class="thumb rounded">
                                    <p class="category-badge position-absolute">14:40</p>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="images/posts/trending-lg-1.jpg" alt="post-title" />
                                        </div>
                                    </a>
                                </div>
                                <h5 class="post-title mb-3 mt-3" style="font-size:14px;"><a href="blog-single.html">Facts About Business That Will Help You Success</a></h5>
                            </div>
                            <div class="post">
                                <div class="thumb rounded">
                                    <p class="category-badge position-absolute">14:40</p>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="images/posts/trending-lg-1.jpg" alt="post-title" />
                                        </div>
                                    </a>
                                </div>
                                <h5 class="post-title mb-3 mt-3" style="font-size:14px;"><a href="blog-single.html">Facts About Business That Will Help You Success</a></h5>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>

<script>
var vid = document.getElementById("video");
vid.addEventListener('loadedmetadata', getDuration, false);

function getDuration() {
    console.log(vid.duration)
}
</script>

@endsection
