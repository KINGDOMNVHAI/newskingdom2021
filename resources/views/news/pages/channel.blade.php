@extends('news.master')
@section('content')

@include('news.block.navbar')

@include('news.block.ads-rows-kd')

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <div class="row gy-4">
            <div class="col-lg-8">
                <div class="row gy-4">
                    @foreach($videoNewest as $video)
                    <div class="col-sm-4">
                        <div class="post post-grid rounded bordered" style="min-height:380px;">
                            <div class="thumb top-rounded">
                                <!-- <a href="category.html" class="category-badge position-absolute">29 March 2021</a>
                                <p class="category-badge position-absolute" style="top:80%;">20213</p> -->
                                <a href="{{route('video-watch', $video['url_video'])}}">
                                    <div class="inner">
                                        @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                        <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                        @else
                                        <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                        @endif
                                    </div>
                                </a>
                            </div>
                            <div class="details" style="padding:20px;">
                                <ul class="meta list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="{{route('video-watch', $video['url_video'])}}">
                                            <img src="{{asset('upload/images/channel/100x100/' . $video['thumbnail_channel'])}}" class="author" alt="{{$video['name_channel']}}" width="25px" />{{$video['name_channel']}}
                                        </a>
                                    </li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3" style="font-size:14px;">{{$video['name_video']}}</h5>
                                <p>{{$video['date_video']}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <nav>{!! $videoNewest->links('pagination::bootstrap-4') !!}</nav>

                @include('news.block.ads-rows')
            </div>
            <div class="col-lg-4">
                @include('news.block.widget-channel')
            </div>
        </div>
    </div>
</section>

@endsection
