@extends('news.master')

@section('content')

@include('news.block.navbar')

<center><a href="{{AD_NHATHUOC}}" target="_blank" title="{{AD_NHATHUOC_ALT}}">
    <img src="news/images/ads/ads-medical-720.jpg" alt="Nhà thuốc online 24h" width="720px"></a>
<br><br></center>

<section class="hero-carousel">
    <div class="row post-carousel-featured post-carousel">
    @foreach ($newest as $new)
        <!-- post -->
        <div class="post featured-post-md">
            <div class="details clearfix">
                <a href="{{ route('post-content', $new['url_post'] ) }}" class="category-badge">{{$new['name_cat_post']}}</a>
                <h4 class="post-title"><a href="{{ route('post-content', $new['url_post'] ) }}">{{$new['name_post']}}</a></h4>
                <ul class="meta list-inline mb-0">
                    <li class="list-inline-item">{{$new['date_post']}}</li>
                </ul>
            </div>
            <a href="{{ route('post-content', $new['url_post'] ) }}">
                <div class="thumb rounded">
                    @if($new['thumbnail_post'] && file_exists('upload/images/thumbnail/' . $new['thumbnail_post']))
                    <div class="inner data-bg-image" data-bg-image="upload/images/thumbnail/{{ $new['thumbnail_post'] }}"></div>
                    @else
                    <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                    @endif
                </div>
            </a>
        </div>
    @endforeach
    </div>
</section>

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <div class="row gy-4">
            <div class="col-lg-8">
                <!-- section header -->
                <div class="section-header">
                    <h2 class="section-title">{{ __('home.website_social_network') }}</h2>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                </div>
                <div class="padding-30 rounded bordered">
                    <div class="row gy-5">
                        <div class="col-sm-6">
                            <!-- post -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="{{ route('post-content', $listWebsitePost[0]->url_post ) }}" title="{{$listWebsitePost[0]->name_cat_post}}" class="category-badge position-absolute">{{$listWebsitePost[0]->name_cat_post}}</a>
                                    <span class="post-format"><i class="icon-picture"></i></span>
                                    <a href="{{route('post-content', $listWebsitePost[0]->url_post)}}">
                                        <div class="inner">
                                            @if($listWebsitePost[0]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listWebsitePost[0]->thumbnail_post))
                                            <img src="{{asset('upload/images/thumbnail/' . $listWebsitePost[0]->thumbnail_post)}}" alt="{{$listWebsitePost[0]->name_post}}" title="{{$listWebsitePost[0]->name_post}}" width="100%" />
                                            @else
                                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item">{{$listWebsitePost[0]->date_post}}</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $listWebsitePost[0]->url_post ) }}">{{$listWebsitePost[0]->name_post}}</a></h5>
                                <p class="excerpt mb-0">{{$listWebsitePost[0]->present_post}}</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            @for($i = 1; $i < count($listWebsitePost); $i++)
                            <div class="post post-list-sm square">
                                <div class="thumb rounded">
                                    <a href="{{ route('post-content', $listWebsitePost[$i]->url_post ) }}">
                                        <div class="inner">
                                            @if($listWebsitePost[$i]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listWebsitePost[$i]->thumbnail_post))
                                            <img src="{{asset('upload/images/thumbnail/' . $listWebsitePost[$i]->thumbnail_post) }}" alt="{{$listWebsitePost[$i]->name_post}}" />
                                            @else
                                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listWebsitePost[$i]->name_post}}" width="100%" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="{{ route('post-content', $listWebsitePost[$i]->url_post ) }}" title="{{$listWebsitePost[$i]->name_post}}">{{$listWebsitePost[$i]->name_post}}</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">{{$listWebsitePost[$i]->date_post}}</li>
                                    </ul>
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>

                <div class="spacer" data-height="50"></div>

                @include('news.block.ads-rows')

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h2 class="section-title">{{ __('home.anime_manga_game') }}</h2>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                </div>

                <div class="padding-30 rounded bordered">
                    <div class="row gy-5">
                        <div class="col-sm-6">
                            <!-- post large -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="{{ route('post-content', $listGamePost[0]->url_post ) }}" class="category-badge position-absolute">{{$listGamePost[0]->name_cat_post}}</a>
                                    <span class="post-format"><i class="icon-picture"></i></span>
                                    <a href="{{ route('post-content', $listGamePost[0]->url_post ) }}" title="{{$listGamePost[0]->name_cat_post}}">
                                        <div class="inner">
                                            @if($listGamePost[0]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listGamePost[0]->thumbnail_post))
                                            <img src="upload/images/thumbnail/{{ $listGamePost[0]->thumbnail_post }}" alt="{{$listGamePost[0]->name_post}}" title="{{$listGamePost[0]->name_post}}" />
                                            @else
                                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listGamePost[0]->name_post}}" title="{{$listGamePost[0]->name_post}}" width="100%" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item">{{$listGamePost[0]->date_post}}</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $listGamePost[0]->url_post ) }}">{{$listGamePost[0]->name_post}}</a></h5>
                                <p class="excerpt mb-0">{{$listGamePost[0]->present_post}}</p>
                            </div>
                            @for($i = 1; $i < count($listGamePost); $i++) <!-- post -->
                                <div class="post post-list-sm square before-seperator">
                                    <div class="thumb rounded">
                                        <a href="{{ route('post-content', $listGamePost[$i]->url_post ) }}">
                                            <div class="inner">
                                                @if($listGamePost[$i]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listGamePost[$i]->thumbnail_post))
                                                <img class=entry-thumb src="upload/images/thumbnail/{{ $listGamePost[$i]->thumbnail_post }}" alt="{{$listGamePost[$i]->name_post}}" title="{{$listGamePost[$i]->name_post}}" />
                                                @else
                                                <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listGamePost[$i]->name_post}}" title="{{$listGamePost[$i]->name_post}}" width="100%" />
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                    <div class="details clearfix">
                                        <h6 class="post-title my-0"><a href="{{ route('post-content', $listGamePost[$i]->url_post ) }}" title="{{ route('post-content', $listGamePost[$i]->name_post ) }}">{{$listGamePost[$i]->name_post}}</a></h6>
                                        <ul class="meta list-inline mt-1 mb-0">
                                            <li class="list-inline-item">{{$listGamePost[$i]->date_post}}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endfor
                        </div>
                        <div class="col-sm-6">
                            <!-- post large -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="{{ route('post-content', $listAnimePost[0]->url_post ) }}" class="category-badge position-absolute">{{$listAnimePost[0]->name_cat_post}}</a>
                                    <span class="post-format"><i class="icon-picture"></i></span>
                                    <a href="{{ route('post-content', $listAnimePost[0]->url_post ) }}" title="{{$listAnimePost[0]->name_cat_post}}">
                                        <div class="inner">
                                            @if($listAnimePost[0]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listAnimePost[0]->thumbnail_post))
                                            <img src="upload/images/thumbnail/{{ $listAnimePost[0]->thumbnail_post }}" alt="{{$listAnimePost[0]->name_post}}" title="{{$listAnimePost[0]->name_post}}" />
                                            @else
                                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listAnimePost[0]->name_post}}" title="{{$listAnimePost[0]->name_post}}" width="100%" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item">{{$listAnimePost[0]->date_post}}</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $listAnimePost[0]->url_post ) }}">{{$listAnimePost[0]->name_post}}</a></h5>
                                <p class="excerpt mb-0">{{$listAnimePost[0]->present_post}}</p>
                            </div>
                            @for($i = 1; $i < count($listAnimePost); $i++) <!-- post -->
                                <div class="post post-list-sm square before-seperator">
                                    <div class="thumb rounded">
                                        <a href="{{ route('post-content', $listAnimePost[$i]->url_post ) }}">
                                            <div class="inner">
                                                @if($listAnimePost[$i]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listAnimePost[$i]->thumbnail_post))
                                                <img class=entry-thumb src="upload/images/thumbnail/{{ $listAnimePost[$i]->thumbnail_post }}" alt="{{$listAnimePost[$i]->name_post}}" title="{{$listAnimePost[$i]->name_post}}" />
                                                @else
                                                <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listAnimePost[$i]->name_post}}" title="{{$listAnimePost[$i]->name_post}}" width="100%" />
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                    <div class="details clearfix">
                                        <h6 class="post-title my-0"><a href="{{ route('post-content', $listAnimePost[$i]->url_post ) }}" title="{{ route('post-content', $listAnimePost[$i]->name_post ) }}">{{$listAnimePost[$i]->name_post}}</a></h6>
                                        <ul class="meta list-inline mt-1 mb-0">
                                            <li class="list-inline-item">{{$listAnimePost[$i]->date_post}}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>

                @include('news.block.ads-rows')

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h2 class="section-title">THỦ THUẬT IT</h2>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                </div>
                <div class="padding-30 rounded bordered">
                    <div class="row gy-5">
                        <div class="col-sm-6">
                            <!-- post -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="{{route('post-content', $listTipITPost[0]->url_post)}}" class="category-badge position-absolute">{{$listTipITPost[0]->name_cat_post}}</a>
                                    <span class="post-format"><i class="icon-picture"></i></span>
                                    <a href="{{route('post-content', $listTipITPost[0]->url_post)}}" title="{{$listTipITPost[0]->name_cat_post}}">
                                        <div class="inner">
                                            @if($listTipITPost[0]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listTipITPost[0]->thumbnail_post))
                                            <img src="{{asset('upload/images/thumbnail/' . $listTipITPost[0]->thumbnail_post)}}" alt="{{$listTipITPost[0]->name_post}}" title="{{$listTipITPost[0]->name_post}}" width="100%" />
                                            @else
                                            <img src="{{asset('news/images/td_600x400.jpg')}}" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item">{{$listTipITPost[0]->date_post}}</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $listTipITPost[0]->url_post ) }}">{{$listTipITPost[0]->name_post}}</a></h5>
                                <p class="excerpt mb-0">{{$listTipITPost[0]->present_post}}</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            @for($i = 1; $i < count($listTipITPost); $i++)
                            <div class="post post-list-sm square">
                                <div class="thumb rounded">
                                    <a href="{{ route('post-content', $listTipITPost[$i]->url_post ) }}">
                                        <div class="inner">
                                            @if($listTipITPost[$i]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listTipITPost[$i]->thumbnail_post))
                                            <img src="{{asset('upload/images/thumbnail/' . $listTipITPost[$i]->thumbnail_post) }}" alt="{{$listTipITPost[$i]->name_post}}" />
                                            @else
                                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listTipITPost[$i]->name_post}}" width="100%" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="{{ route('post-content', $listTipITPost[$i]->url_post ) }}" title="{{$listTipITPost[$i]->name_post}}">{{$listTipITPost[$i]->name_post}}</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">{{$listTipITPost[$i]->date_post}}</li>
                                    </ul>
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>


                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h2 class="section-title">Video</h2>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                    <div class="slick-arrows-top">
                        <button type="button" data-role="none" class="carousel-topNav-prev slick-custom-buttons" aria-label="Previous"><i class="icon-arrow-left"></i></button>
                        <button type="button" data-role="none" class="carousel-topNav-next slick-custom-buttons" aria-label="Next"><i class="icon-arrow-right"></i></button>
                    </div>
                </div>

                <div class="row post-carousel-twoCol post-carousel">
                    @foreach ($videos as $video)
                    <!-- post -->
                    <div class="post post-over-content col-md-6">
                        <div class="details clearfix">
                            <h4 class="post-title"><a href="http://kdplayback.com/video/{{$video['url_video']}}" title="KDPLAYBACK {{$video['name_video']}}">{{$video['name_video']}}</a></h4>
                        </div>
                        <a href="http://kdplayback.com/video/{{$video['url_video']}}" title="KDPLAYBACK {{$video['name_video']}}">
                            <div class="thumb rounded">
                                <div class="inner">
                                    @if($video['thumbnail_video'] && file_exists('upload/images/video/' . $video['thumbnail_video']))
                                    <img src="{{asset('upload/images/video/' . $video['thumbnail_video'])}}" alt="{{$video['name_video']}}" />
                                    @else
                                    <img src="{{asset('news/images/no-thumb/td_600x300.jpg')}}" />
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>

                @include('news.block.ads-rows')

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h2 class="section-title">CẬP NHẬT THƯỜNG XUYÊN</h2>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                    <p>Những bài viết được cập nhật bất kể thời gian nào, tùy theo sự kiện thực tế</p>
                </div>

                <div class="padding-30 rounded bordered">
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            @foreach($updates as $update)
                            <div class="post post-list clearfix">
                                <div class="thumb rounded">
                                    <span class="post-format-sm"><i class="icon-picture"></i></span>
                                    <a href="{{ route('post-content', $update->url_post ) }}" title="{{$update->name_post}}">
                                        <div class="inner">
                                            @if($update->thumbnail_post && file_exists('upload/images/thumbnail/' . $update->thumbnail_post))
                                            <img src="{{asset('upload/images/thumbnail/' . $update->thumbnail_post)}}" alt="{{$update->name_post}}" title="{{$update->name_post}}" width="100%" />
                                            @else
                                            <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$update->name_post}}" title="{{$update->name_post}}" width="100%" />
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="details">
                                    <ul class="meta list-inline mb-3">
                                        <li class="list-inline-item"><a href="{{ route('post-content', $update->url_post ) }}">{{$update->name_cat_post}}</a></li>
                                        <li class="list-inline-item">{{ \Carbon\Carbon::parse($update->date_post)->format('d-m-Y')}}</li>
                                    </ul>
                                    <h5 class="post-title"><a href="{{ route('post-content', $update->url_post ) }}">{{$update->name_post}}</a></h5>
                                    <p class="excerpt mb-0">{{$update->present_detailpost}}</p>
                                    <div class="post-bottom clearfix d-flex align-items-center">
                                        <div class="social-share me-auto">
                                            <i class="fa fa-eye"></i> Views
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @include('news.block.ads-rows')
            </div>
            <div class="col-lg-4">
                @include('news.block.widget')
            </div>
        </div>
    </div>
</div>
</section>

@endsection
