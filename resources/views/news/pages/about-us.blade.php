@extends('news.master')
@section('ads')
<script data-ad-client="ca-pub-4172631569878022" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
@endsection
@section('content')

@include('news.block.navbar')

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <div class="row">
            <div class="col-md-4" style="margin-bottom:5px;">
                <!-- contact info item -->
                <div class="contact-item bordered rounded d-flex align-items-center">
                    <span class="icon icon-user"></span>
                    <div class="details">
                        <h3 class="mb-0 mt-0">Name</h3>
                        <p class="mb-0">Hai Nguyen Viet</p>
                    </div>
                </div>
                <div class="spacer d-md-none d-lg-none" data-height="30"></div>
            </div>
            <div class="col-md-4" style="margin-bottom:5px;">
                <!-- contact info item -->
                <div class="contact-item bordered rounded d-flex align-items-center">
                    <span class="icon icon-phone"></span>
                    <div class="details">
                        <h3 class="mb-0 mt-0">Phone</h3>
                        <p class="mb-0">0706 5333 08</p>
                    </div>
                </div>
                <div class="spacer d-md-none d-lg-none" data-height="30"></div>
            </div>
            <div class="col-md-4" style="margin-bottom:5px;">
                <!-- contact info item -->
                <div class="contact-item bordered rounded d-flex align-items-center">
                    <span class="icon icon-envelope-open"></span>
                    <div class="details">
                        <h3 class="mb-0 mt-0">E-Mail</h3>
                        <p class="mb-0">kingdomnvhai@gmail.com</p>
                    </div>
                </div>
                <div class="spacer d-md-none d-lg-none" data-height="30"></div>
            </div>
            <div class="col-md-4" style="margin-bottom:5px;">
                <!-- contact info item -->
                <div class="contact-item bordered rounded d-flex align-items-center">
                    <span class="icon icon-map"></span>
                    <div class="details">
                        <h3 class="mb-0 mt-0">Location</h3>
                        <p class="mb-0">Ho Chi Minh city, Vietnam</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom:5px;">
                <!-- contact info item -->
                <div class="contact-item bordered rounded d-flex align-items-center">
                    <span class="icon icon-folder-alt"></span>
                    <div class="details">
                        <h3 class="mb-0 mt-0">Job</h3>
                        <p class="mb-0">Developer</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom:5px;">
                <!-- contact info item -->
                <div class="contact-item bordered rounded d-flex align-items-center">
                    <span class="icon icon-diamond"></span>
                    <div class="details">
                        <h3 class="mb-0 mt-0">Banking</h3>
                        <p class="mb-0">0184 1733 201, TPBank</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row gy-4">
            <div class="col-lg-8">
                <div class="page-content bordered rounded padding-30">
                    <p>KINGDOM NVHAI là hệ thống website, kênh Youtube về Game, Anime - Manga, VTuber, thủ thuật IT và chuyên dịch Visual Novel, video nước ngoài. Được thành lập vào ngày 1/7/2015, KINGDOM NVHAI đã có một số lượng thành viên ủng hộ nhất định.</p>

                    <p>Ngoài ra, KINGDOM NVHAI còn có dịch vụ làm website, được gọi là WEBSITE SỐ 7. Với mong muốn tìm kiếm các khách hàng, các doanh nghiệp muốn định hình thương hiệu của mình trên Internet.</p>

                    <ul>
                        <li>Kỷ niệm 7 năm sinh nhật KINGDOM NVHAI (2015-2022)
                        <li>Sinh nhật ngày 1/7
                        <li>Khách hàng có thể nhận demo trong 7 ngày
                        <li>Website giá 7 triệu VND với 7 tính năng chuyên nghiệp
                        <li>Bảo hành, hỗ trợ trong 7 tháng
                    </ul>

                    <p>7 tính năng của website KINGDOM NVHAI</p>

                    <ul>
                        <li>Danh sách bài viết/sản phẩm
                        <li>Tìm kiếm bài viết/sản phẩm
                        <li>Đăng nhập, đăng ký tài khoản
                        <li>Thống kê số bài viết, sản phẩm, tài khoản
                        <li>Thêm/sửa/xóa bài viết/sản phẩm
                        <li>Thêm/sửa/xóa chuyên mục của bài viết/sản phẩm
                        <li>Gửi email quên mật khẩu
                    </ul>

                    <p>Giá 7 triệu đã bao gồm cả domain, hosting được mua từ PA Việt Nam.</p>

                    <p>Một số sản phẩm của KINGDOM NVHAI</p>

                    <div class="col-lg-6">
                        https://nhathuockhanhan.com/
                        Chèn thêm hình
                    </div>

                    <p>Xin chân thành cảm ơn mọi người.</p>

                    <hr class="my-4" />
                    <ul class="social-icons list-unstyled list-inline mb-0">
                        <li class="list-inline-item"><a href="{{FACEBOOK_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="{{TWITTER_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="{{YOUTUBE_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-youtube"></i></a></li>
                        <!-- <li class="list-inline-item"><a href="{{VIMEO_URL}}" target="_blank" title="KINGDOM NVHAI"><i class="fab fa-vimeo-v"></i></a></li> -->
                    </ul>
                </div>
                @include('news.block.ads-rows')
            </div>
            <div class="col-lg-4">
                @include('news.block.widget')
            </div>
        </div>
    </div>
</section>

@endsection
