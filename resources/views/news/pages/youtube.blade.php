@extends('news.master')
@section('ads')
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4172631569878022" crossorigin="anonymous"></script>
@endsection
@section('content')

@include('news.block.navbar-3')
<!-- section main content -->
<section class="main-content">
    <div class="container-xl">
        <div class="row gy-4">
        @foreach($youtuberankFavorite as $channelFavorite)
            <div class="col-sm-3">
                <!-- post -->
                <div class="post post-grid rounded bordered">
                    <div class="thumb top-rounded">
                        <a href="{{ $channelFavorite->url_channel }}" class="category-badge position-absolute"
                            alt="{{ $channelFavorite->name_channel }}" title="{{ $channelFavorite->name_channel }}">{{ $channelFavorite->subscribe }}</a>
                        <p class="category-badge position-absolute" style="top:80%;">{{ $channelFavorite->name_channel }}</p>
                        <a href="{{ $channelFavorite->url_video }}">
                            <div class="inner">
                                <a href="{{ $channelFavorite->url_channel }}" target="_blank" alt="{{ $channelFavorite->name_channel }}" title="{{ $channelFavorite->name_channel }}">
                                @if($channelFavorite->thumbnail_channel && file_exists('upload/images/channel/600x400/' . $channelFavorite->thumbnail_channel))
                                <img src="{{asset('upload/images/channel/600x400/'. $channelFavorite->thumbnail_channel)}}" alt="{{ $channelFavorite->name_channel }}" title="{{ $channelFavorite->name_channel }}" />
                                @else
                                <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" />
                                @endif
                                </a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>

    <!-- instagram feed -->
    <div class="instagram">
        <div class="container-xl">
            <!-- <a href="#" class="btn btn-default btn-instagram">@Katen on Instagram</a> -->
            <!-- images -->
            <div class="instagram-feed d-flex flex-wrap">
                <div class="insta-item col-sm-2 col-6 col-md-2">
                    <img src="{{asset('news/kd-nvhai/anonymous-avatar-hololive.jpg')}}" alt="insta-title" onclick="listAjax('hololive')" />
                </div>
                <div class="insta-item col-sm-2 col-6 col-md-2">
                    <img src="{{asset('news/kd-nvhai/anonymous-avatar-vtuber.jpg')}}" alt="insta-title" onclick="listAjax('vtuber')" />
                </div>
                <div class="insta-item col-sm-2 col-6 col-md-2">
                    <img src="{{asset('news/kd-nvhai/anonymous-avatar-pewdiepie.jpg')}}" alt="insta-title" onclick="listAjax('other')" />
                </div>
                <!-- <div class="insta-item col-sm-2 col-6 col-md-2">
                    <img src="{{asset('news/kd-nvhai/anonymous-avatar-dva.jpg')}}" alt="insta-title" onclick="listAjax('other')" />
                </div>
                <div class="insta-item col-sm-2 col-6 col-md-2">
                    <img src="{{asset('news/kd-nvhai/anonymous-avatar-sky-kingdom.jpg')}}" alt="insta-title" onclick="listAjax('other')" />
                </div>
                <div class="insta-item col-sm-2 col-6 col-md-2">
                    <img src="{{asset('news/kd-nvhai/avatar-kiryu-coco.jpg')}}" alt="insta-title" onclick="listAjax('other')" />
                </div> -->
            </div>
        </div>
    </div>

    <div id="loading" style="display:none;position:absolute;top:200;left:50%;">
        <img src="{{ asset('/upload/images/other/loading.gif') }}" width="100px;">
    </div>

    <div class="container-xl" id="getRequestData">
        <div class="row gy-4" style="margin-top:5px;">
            <div class="widget rounded">
                <div class="widget-header text-center">
                    <h3 class="widget-title">50 Most Popular Channels</h3>
                    <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
                </div>
                @foreach($youtuberankAll as $channelAll)
                <div class="row" style="margin-top:5px;">
                    <div class="col-sm-2">
                        <a href="{{$channelAll->url_video_present}}" target="_blank">
                        <img src="{{asset('/upload/images/channel/600x400/'. $channelAll->thumbnail_channel)}}" alt="{{ $channelAll->name_channel }}" title="{{ $channelAll->name_channel }}" />
                        </a>
                    </div>
                    <div class="col-sm-10">
                        <div class="details clearfix">
                            <h6 class="post-title my-0"><a href="{{$channelAll->url_channel}}">{{$channelAll->name_channel}}</a></h6>
                            <p><b>Subscribe: {{ $channelAll->subscribe }}</b><br>
                            {!! $channelAll->description_channel !!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @include('news.block.ads-rows')
</section>

<script src="{{asset('/news/kd-nvhai/jquery-1.11.3.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).ajaxStart(function () {
            $("#loading").show();
        }).ajaxStop(function () {
            $("#loading").hide();
        });
    });

    function listAjax(list)
    {
        // var url = 'http://localhost/newskingdom2021/public/youtube-channels-ranking-ajax/' + list;
        var url = 'https://kingdomnvhai.info/youtube-channels-ranking-ajax/' + list;
        console.log(list);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // $.showLoading($('#getRequestData'));
        $.ajax({
            type:'POST',
            dataType: 'JSON',
            url: url,
            data:{
                list:list,
            },
            success:function(data){
                console.log(data);
                console.log(data.htmlTable);
        //         $.hideLoading($('#getRequestData'));
                $('#getRequestData').html(data.htmlTable);
        //         $('#getListSeeAll').html(data.seeAll);
                console.log(list);
            },
            error:function(xhr, data){
                // $.hideLoading($('#getRequestData'));
                console.log(xhr);
            }
        });
    }
</script>
@endsection
