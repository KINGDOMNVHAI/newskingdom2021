<!-- Load JS here for greater good =============================-->
<script src="{{asset('landing/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('landing/js/bootstrap.min.js')}}"></script>
<script src="{{asset('landing/js/jquery.scrollTo-min.js')}}"></script>
<script src="{{asset('landing/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('landing/js/jquery.nav.js')}}"></script>
<script src="{{asset('landing/js/wow.js')}}"></script>
<script src="{{asset('landing/js/plugins.js')}}"></script>
<script src="{{asset('landing/js/custom.js')}}"></script>
