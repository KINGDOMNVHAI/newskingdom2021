<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="">
<meta name="description" content="">

<!-- Loading Bootstrap -->
<link href="{{asset('landing/css/bootstrap.min.css')}}" rel="stylesheet">

<!-- Loading Template CSS -->
<link href="{{asset('landing/css/style.css')}}" rel="stylesheet">
<link href="{{asset('landing/css/animate.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('landing/css/pe-icon-7-stroke.css')}}">
<link href="{{asset('landing/css/style-magnific-popup.css')}}" rel="stylesheet">

<!-- Awsome Fonts -->
<link rel="stylesheet" href="{{asset('landing/css/all.min.css')}}">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">

<!-- Font Favicon -->
<link rel="shortcut icon" href="{{asset('landing/images/favicon.ico')}}">
