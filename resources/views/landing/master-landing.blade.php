
<!DOCTYPE html>
<html lang="zxx">
<head>
    @include('landing.block.head')
    <title>LeadPage - Landing Page Template</title>
</head>
<body>
    <!--begin header -->
    <header class="header">
        <!--begin navbar-fixed-top -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <!--begin container -->
            <div class="container">
                <!--begin navbar -->
                <nav class="navbar navbar-expand-lg">
                    <!--begin logo -->
                    <a class="navbar-brand" href="#">LeadPage</a>
                    <!--end logo -->

                    <!--begin navbar-toggler -->
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                    </button>
                    <!--end navbar-toggler -->

                    <!--begin navbar-collapse -->
                    <div class="navbar-collapse collapse" id="navbarCollapse">
                        <!--begin navbar-nav -->
                        <ul class="navbar-nav ml-auto">
                            <li><a href="#home">Home</a></li>
                            <li><a href="#services">Services</a></li>
                            <li><a href="#team">Team</a></li>
                            <li><a href="#faq">FAQ</a></li>
                            <li><a href="#pricing">Pricing</a></li>
                            <li class="discover-link"><a href="#home" class="discover-btn">Get Started</a></li>
                        </ul>
                        <!--end navbar-nav -->
                    </div>
                    <!--end navbar-collapse -->
                </nav>
                <!--end navbar -->
            </div>
    		<!--end container -->
        </nav>
    	<!--end navbar-fixed-top -->
    </header>
    <!--end header -->

    <!--begin home section -->
    <section class="home-section" id="home">
        <div class="home-section-overlay"></div>
		<!--begin container -->
		<div class="container">
	        <!--begin row -->
	        <div class="row">
	            <!--begin col-md-5-->
	            <div class="col-md-6 padding-top-120">
	          		<h1>Welcome To LeadPage</h1>
	          		<p class="hero-text">Trang web mẫu https://demo.epic-webdesign.com/tf-leadpage/v10/</p>
                    <!--begin home-benefits -->
                    <ul class="home-benefits">
                        <li><i class="fas fa-check-circle"></i>Đây là trang báo giá quảng cáo trên KINGDOM NVHAI</li>
                        <li><i class="fas fa-check-circle"></i>Atimus etims urnatis quisle ratione netis.</li>
                        <li><i class="fas fa-check-circle"></i>Ratione lorem nets et sequi tempor.</li>
                    </ul>
                    <!--end home-benefits -->
	            </div>
	            <!--end col-md-5-->

                <!--begin col-md-2-->
                <div class="col-md-1"></div>
                <!--end col-md-2-->

				<!--begin col-md-5-->
	            <div class="col-md-5 margin-top-20">
                    <!--begin register-form-wrapper-->
                    <div class="register-form-wrapper wow bounceIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;">
                        <h3>Get Started Today</h3>
                        <p>Velis demo enim quia tempor magnet.</p>
                        <!--begin form-->
                        <div>
                            <!--begin success message -->
                            <p class="register_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
                            <!--end success message -->

                            <!--begin register form -->
                            <form id="register-form" class="register-form register" action="php/register.php" method="post">
                                <input class="register-input white-input" required="" name="register_names" placeholder="Your Name*" type="text">
                                <input class="register-input white-input" required="" name="register_email" placeholder="Email Adress*" type="email">
                                <input class="register-input white-input" required="" name="register_phone" placeholder="Phone Number*" type="text">
                                <input value="Get Started Today!" class="register-submit" type="submit">
                            </form>
                            <!--end register form -->

                            <p class="register-form-terms">No Credit Card &#8226; No Installation Required</p>
                        </div>

                        <!--end form-->



                    </div>

                    <!--end register-form-wrapper-->



	            </div>

	            <!--end col-md-5-->



	        </div>

	        <!--end row -->



		</div>

		<!--end container -->



    </section>

    <!--end home section -->



    <!--begin section-white -->

    <section class="section-white" id="services">



        <!--begin container -->

        <div class="container">



            <!--begin row -->

            <div class="row">



                <!--begin col-md-12 -->

                <div class="col-md-12 text-center">



                    <h2 class="section-title">Discover Our Services</h2>



                    <p class="section-subtitle">Tendas tempor ante acu ipsum lorem quam etsum nets.</p>



                </div>

                <!--end col-md-12 -->



            </div>

            <!--end row -->



        </div>

        <!--end container -->



        <!--begin container -->

        <div class="container">



            <!--begin row -->

            <div class="row">



                <!--begin col-md-4 -->

                <div class="col-md-4">



                    <div class="our-services">



                        <img src="{{asset('landing/images/gym1.jpg')}}" class="width-100" alt="pic">



                        <h3><a href="#">Cardio Exercises</a></h3>



                        <p>Curabitur quam etsum lacus netum netsum nulatis iaculis etsimun vitaemis etsum nisle varius netsum.</p>



                    </div>



                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4 -->
                <div class="col-md-4">
                    <div class="our-services">
                        <img src="{{asset('landing/images/gym2.jpg')}}" class="width-100" alt="pic">
                        <h3><a href="#">Weight Lifting</a></h3>
                        <p>Curabitur quam etsum lacus netum netsumised nulatis nets iaculis etsimun vitaemis etsum nisle varius.</p>
                    </div>
                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4 -->
                <div class="col-md-4">
                    <div class="our-services">
                        <img src="{{asset('landing/images/gym3.jpg')}}" class="width-100" alt="pic">
                        <h3><a href="#">Zumba Classes</a></h3>
                        <p>Curabitur quam etsum lacus netum netsum nulatis iaculis etsimun vitaemis etsum nisle varius netsum.</p>
                    </div>
                </div>
                <!--end col-md-4 -->
            </div>

            <!--end row -->



        </div>

        <!--end container -->



    </section>

    <!--end section-white -->



    <!--begin section-grey -->

    <section class="section-grey section-bottom-border">



        <!--begin container-->

        <div class="container">



            <!--begin row-->

            <div class="row">



                <!--begin col-md-6-->

                <div class="col-md-6 padding-top-20">
                    <h3>Get ready to discover all the benefits.</h3>
                    <p>Velis demo enim ipsam voluptatem quia voluptas sit aspernatur netsum lorem fugit, sed quia magni dolores eos qui ratione sequi nesciunt neque et poris ratione sequi enim quia tempor magni.</p>
                    <p>Velis demo enim ipsam voluptatem quia voluptas ets situmad aspernatures netsum lorem fugit, sed quia ratione.</p>
                    <a href="#contact" class="btn-red small scrool">Get Started</a>
                </div>
                <!--end col-md-6-->

                <!--begin col-md-6-->
                <div class="col-md-6">
                    <img src="{{asset('landing/images/gym4.jpg')}}" class="box-shadow top-margins-images width-100" alt="pic">
                </div>

                <!--end col-sm-6-->



            </div>

            <!--end row-->



        </div>

        <!--end container-->



    </section>

    <!--end section-grey-->



    <!--begin section-white -->

    <section class="section-white section-bottom-border">



        <!--begin container-->

        <div class="container">



            <!--begin row-->

            <div class="row">



                <!--begin col-md-6-->

                <div class="col-md-6">



                    <img src="{{asset('landing/images/gym5.jpg')}}" class="width-100 margin-right-15 box-shadow" alt="pic">



                </div>

                <!--end col-sm-6-->



                <!--begin col-md-6-->
                <div class="col-md-6 padding-top-20">
                    <h4>The most affordable gym in town.</h4>
                    <p>Velis demo enim ipsam voluptatem quia voluptas sit aspernatur netsum lorem fugit, sed quia magni dolores eos qui ratione sequi nesciunt neque et poris ratione sequi enim quia tempor magnis quia voluptas sit aspernatur netsum.</p>
                    <ul class="benefits">
                        <li><i class="fas fa-arrow-right"></i> Quia magni netsum eos qui ratione sequi.</li>
                        <li><i class="fas fa-arrow-right"></i> Venis ratione sequi enim quia tempor magni.</li>
                        <li><i class="fas fa-arrow-right"></i> Enim ipsam voluptatem quia voluptas.</li>
                        <li><i class="fas fa-arrow-right"></i> Quias voluptas sit ados netsum.</li>
                    </ul>
                </div>
                <!--end col-md-6-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>

    <!--end section-white-->



    <!--begin section-bg-2 -->

    <section class="section-bg-2">



        <div class="section-bg-overlay"></div>



        <!--begin container-->

        <div class="container">



            <!--begin row-->

            <div class="row">



                <!--begin col md 8 -->

                <div class="col-md-8 mx-auto padding-bottom-40">



                    <!--begin testimonials carousel -->

                    <div id="carouselIndicators2" class="carousel slide" data-ride="carousel">



                        <!--begin carousel-indicators -->

                        <ol class="carousel-indicators testimonials-indicators">

                            <li data-target="#carouselIndicators2" data-slide-to="0" class="active"></li>

                            <li data-target="#carouselIndicators2" data-slide-to="1"></li>

                            <li data-target="#carouselIndicators2" data-slide-to="2"></li>

                        </ol>

                        <!--end carousel-indicators -->



                        <!--begin carousel-inner -->

                        <div class="carousel-inner">



                            <!--begin carousel-item -->

                            <div class="carousel-item active">



                                <!--begin testim-inner -->

                                <div class="testim-inner">
                                    <img src="{{asset('landing/images/testimonials1.jpg')}}" alt="testimonials" class="testim-img">
                                    <p>The attention of a traveller, should be particularly turned to the various works of nature, to mark the distinctions of the climates he may explore, and to offer such useful observations on the different productions as may occur.</p>
                                    <h6>Jennifer Smith<span class="job-text"> - Web Designer</span></h6>
                                    <div class="testim-rating">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                </div>

                                <!--end testim-inner -->



                            </div>

                            <!--end carousel-item -->



                            <!--begin carousel-item -->

                            <div class="carousel-item">



                                <!--begin testim-inner -->

                                <div class="testim-inner">



                                    <img src="{{asset('landing/images/testimonials2.jpg')}}" alt="testimonials" class="testim-img">



                                    <p>The attention of a traveller, should be particularly turned to the various works of nature, to mark the distinctions of the climates he may explore, and to offer such useful observations on the different productions as may occur.</p>



                                    <h6>John Doe<span class="job-text"> -  General Manager</span></h6>



                                    <div class="testim-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                    </div>



                                </div>

                                <!--end testim-inner -->



                            </div>

                            <!--end carousel-item -->



                            <!--begin carousel-item -->

                            <div class="carousel-item">



                                <!--begin testim-inner -->

                                <div class="testim-inner">



                                    <img src="{{asset('landing/images/testimonials3.jpg')}}" alt="testimonials" class="testim-img">



                                    <p>The attention of a traveller, should be particularly turned to the various works of nature, to mark the distinctions of the climates he may explore, and to offer such useful observations on the different productions as may occur.</p>



                                    <h6>Alexandra Smith<span class="job-text"> - App Magazine Editor</span></h6>



                                    <div class="testim-rating">

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                        <i class="fa fa-star" aria-hidden="true"></i>

                                    </div>



                                </div>

                                <!--end testim-inner -->



                            </div>

                            <!--end carousel-item -->



                        </div>
                        <!--end carousel-inner -->
                    </div>
                    <!--end testimonials carousel -->
                </div>
                <!--end col md 8-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-bg-2 -->

    <!--begin team section -->
    <section class="section-grey section-bottom-border" id="team">
        <!--begin container-->
        <div class="container">
            <!--begin row-->
            <div class="row">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Meet Our Trainers</h2>
                    <p class="section-subtitle">There are many variations of passages of Lorem Ipsum available, but the majority<br>have suffered alteration, by injected humour, or new randomised words.</p>
                </div>

                <!--end col-md-12 -->



                <!--begin team-item -->

                <div class="col-sm-12 col-md-4">
                    <div class="team-item">
                        <img src="{{asset('landing/images/team1.jpg')}}" class="team-img width-100" alt="pic">
                        <h3>CHRISTINA HAWKINS</h3>
                        <div class="team-info"><p>Fitness Trainer</p></div>
                        <p>Utise wisi enim minim veniam, quis etsumisad stationes ullamcorper etsum lobotisum nisle consequat.</p>
                    </div>
                </div>

                <!--end team-item -->



                <!--begin team-item -->

                <div class="col-sm-12 col-md-4">
                    <div class="team-item">
                        <img src="{{asset('landing/images/team2.jpg')}}" class="team-img width-100" alt="pic">
                        <h3>ANDRES JOHANSON</h3>
                        <div class="team-info"><p>Bodybuilding Trainer</p></div>
                        <p>Utise wisi enim minim veniam, quis etsumisad stationes ullamcorper etsum lobotisum nisle consequat.</p>
                    </div>
                </div>
                <!--end team-item -->

                <!--begin team-item -->
                <div class="col-sm-12 col-md-4">
                    <div class="team-item">
                        <img src="{{asset('landing/images/team3.jpg')}}" class="team-img width-100" alt="pic">
                        <h3>ALEXANDRA SMITHS</h3>
                        <div class="team-info"><p>Zumba Trainer</p></div>
                        <p>Utise wisi enim minim veniam, quis etsumisad stationes ullamcorper etsum lobotisum nisle consequat.</p>
                    </div>
                </div>
                <!--end team-item -->



            </div>

            <!--end row-->



        </div>

        <!--end container-->



    </section>

    <!--end team section-->



    <!--begin faq section -->

    <section class="section-white section-bottom-border" id="faq">



        <!--begin container -->

        <div class="container">



            <!--begin row -->

            <div class="row">



                <!--begin col-md-12-->

                <div class="col-md-12 text-center padding-bottom-10">



                    <h2 class="section-title">Frequently Asked Questions</h2>



                    <p class="section-subtitle">Quis autem velis ets reprehender net etid quiste voluptate.</p>



                </div>

                <!--end col-md-12 -->



            </div>

            <!--end row -->



            <!--begin row -->

            <div class="row">



                <!--begin col-md-6-->

                <div class="col-md-6">



                    <div class="faq-box">



                        <h5>How can I log in to my account?</h5>



                        <p>Utise wisi enim minim veniam, quis et stationes ullamcorper nets suscipit ets lobotis nisle consequat nihis etim. Quis autem velis ets reprehender net etid quiste voluptate velite esse sedis.</p>



                    </div>



                    <div class="faq-box">



                        <h5>Does LeadPage have a free plan?</h5>



                        <p>Utise wisi enim minim veniam, quis et stationes ullamcorper nets suscipit ets lobotis nisle consequat nihis etim. Quis autem velis ets reprehender net etid quiste voluptate velite esse sedis.</p>



                    </div>



                    <div class="faq-box">



                        <h5>Do you have a money back guarantee?</h5>



                        <p>Utise wisi enim minim veniam, quis et stationes ullamcorper nets suscipit ets lobotis nisle consequat nihis etim. Quis autem velis ets reprehender net etid quiste voluptate velite esse sedis.</p>



                    </div>



                </div>

                <!--end col-md-6 -->



                <!--begin col-md-6-->

                <div class="col-md-6">



                    <div class="faq-box">



                        <h5>How do I install LeadPage?</h5>



                        <p>Utise wisi enim minim veniam, quis et stationes ullamcorper nets suscipit ets lobotis nisle consequat nihis etim. Quis autem velis ets reprehender net etid quiste voluptate velite esse sedis.</p>



                    </div>



                    <div class="faq-box">



                        <h5>Do I get limited features in trial period?</h5>



                        <p>Utise wisi enim minim veniam, quis et stationes ullamcorper nets suscipit ets lobotis nisle consequat nihis etim. Quis autem velis ets reprehender net etid quiste voluptate velite esse sedis.</p>



                    </div>



                    <div class="faq-box">



                        <h5>What are the modes of payment that you accept?</h5>



                        <p>Utise wisi enim minim veniam, quis et stationes ullamcorper nets suscipit ets lobotis nisle consequat nihis etim. Quis autem velis ets reprehender net etid quiste voluptate velite esse sedis.</p>



                    </div>

                </div>



                <!--end col-md-6 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->
    </section>
    <!--end faq section -->

    <!--begin gallery section -->
    <section class="section-grey section-bottom-border" id="gallery">
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">



                <!--begin col-md-12 -->

                <div class="col-md-12 text-center padding-bottom-20">



                    <h2 class="section-title">Gallery</h2>



                    <p class="section-subtitle">Quis autem velis ets reprehender quiste voluptate.</p>



                </div>

                <!--end col-md-12 -->



            </div>

            <!--end row -->



        </div>

        <!--end container -->



        <!--begin container -->

        <div class="container">



            <!--begin row-->

            <div class="row">



                <!--begin col-md-3 -->

                <div class="col-md-3 col-sm-6 col-xs-12">

                    <figure class="gallery-insta">

                        <!--begin popup-gallery-->

                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">

                            <a class="popup2" href="{{asset('landing/images/gym1.jpg')}}">

                                <img src="{{asset('landing/images/gym1.jpg')}}" class="width-100" alt="pic">

                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>

                            </a>

                        </div>

                        <!--end popup-gallery-->

                    </figure>

                </div>

                <!--end col-md-3 -->



                <!--begin col-md-3 -->

                <div class="col-md-3 col-sm-6 col-xs-12">

                    <figure class="gallery-insta">

                        <!--begin popup-gallery-->

                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">

                            <a class="popup2" href="{{asset('landing/images/gym2.jpg')}}">

                                <img src="{{asset('landing/images/gym2.jpg')}}" class="width-100" alt="pic">

                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>

                            </a>

                        </div>

                        <!--end popup-gallery-->

                    </figure>

                </div>

                <!--end col-md-3 -->



                <!--begin col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <figure class="gallery-insta">
                        <!--begin popup-gallery-->
                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                            <a class="popup2" href="{{asset('landing/images/gym3.jpg')}}">
                                <img src="{{asset('landing/images/gym3.jpg')}}" class="width-100" alt="pic">
                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>
                            </a>
                        </div>
                        <!--end popup-gallery-->
                    </figure>
                </div>
                <!--end col-md-3 -->

                <!--begin col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <figure class="gallery-insta">
                        <!--begin popup-gallery-->
                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                            <a class="popup2" href="{{asset('landing/images/gym4.jpg')}}">
                                <img src="{{asset('landing/images/gym4.jpg')}}" class="width-100" alt="pic">
                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>
                            </a>
                        </div>
                        <!--end popup-gallery-->
                    </figure>
                </div>
                <!--end col-md-3 -->

                <!--begin col-md-3 -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <figure class="gallery-insta">
                        <!--begin popup-gallery-->
                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                            <a class="popup2" href="{{asset('landing/images/gym5.jpg')}}">
                                <img src="{{asset('landing/images/gym5.jpg')}}" class="width-100" alt="pic">
                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>
                            </a>
                        </div>
                        <!--end popup-gallery-->
                    </figure>
                </div>
                <!--end col-md-3 -->



                <!--begin col-md-3 -->

                <div class="col-md-3 col-sm-6 col-xs-12">

                    <figure class="gallery-insta">

                        <!--begin popup-gallery-->

                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">

                            <a class="popup2" href="{{asset('landing/images/gym6.jpg')}}">

                                <img src="{{asset('landing/images/gym6.jpg')}}" class="width-100" alt="pic">

                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>

                            </a>

                        </div>

                        <!--end popup-gallery-->

                    </figure>

                </div>

                <!--end col-md-3 -->



                <!--begin col-md-3 -->

                <div class="col-md-3 col-sm-6 col-xs-12">

                    <figure class="gallery-insta">

                        <!--begin popup-gallery-->

                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">

                            <a class="popup2" href="{{asset('landing/images/gym7.jpg')}}">

                                <img src="{{asset('landing/images/gym7.jpg')}}" class="width-100" alt="pic">

                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>

                            </a>

                        </div>

                        <!--end popup-gallery-->

                    </figure>

                </div>

                <!--end col-md-3 -->



                <!--begin col-md-3 -->

                <div class="col-md-3 col-sm-6 col-xs-12">

                    <figure class="gallery-insta">

                        <!--begin popup-gallery-->

                        <div class="popup-gallery popup-gallery-rounded portfolio-pic">

                            <a class="popup2" href="{{asset('landing/images/gym8.jpg')}}">

                                <img src="{{asset('landing/images/gym8.jpg')}}" class="width-100" alt="pic">

                                <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon" style="font-size: 38px;"></i></span>

                            </a>

                        </div>
                        <!--end popup-gallery-->
                    </figure>
                </div>
                <!--end col-md-3 -->
            </div>
            <!--end row -->



        </div>

        <!--end container -->



        <!--begin container -->

        <div class="container">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center padding-top-30">
                    <p class="follow-instagram">Like what you see? Follow us <a href="#">@leadpage_gym</a></p>
                </div>
                <!--end col-md-12 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->
    </section>
    <!--end gallery section -->

    <!--begin section-white -->
    <section class="section-white section-bottom-border" id="about">
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">
                    <h2 class="section-title">How It Works</h2>
                    <p class="section-subtitle">Get started today in three easy steps.</p>
                </div>
                <!--end col-md-12 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->

        <!--begin process-wrapper -->
        <div class="process-wrapper">
            <!--begin container -->
            <div class="container">
                <!--begin row -->
                <div class="row">
                    <!--begin col-md-4 -->
                    <div class="col-md-4">
                        <div class="process">
                            <i class="pe-7s-note2"></i>
                            <h3>Register</h3>
                            <p>Curabitur quam etsum lacus net netsum nulat iaculis etsimun vitae etsum nisle varius sed aliquam etsim vitae netsum.</p>
                        </div>
                    </div>
                    <!--end col-md-4 -->

                    <!--begin col-md-4 -->
                    <div class="col-md-4">
                        <div class="process">
                            <i class="pe-7s-users"></i>
                            <h3>Choose Trainer</h3>
                            <p>Curabitur am etsum lacus net netsum nulat iaculis etsimun vitae etsum nisle varius sed aliquam etsim vitae netsum.</p>
                        </div>
                    </div>
                    <!--end col-md-4 -->

                    <!--begin col-md-4 -->
                    <div class="col-md-4">
                        <div class="process">
                            <i class="pe-7s-like2"></i>
                            <h3>Get Fit</h3>
                            <p>Curabitur quam etsum lacus net netsum nulat iaculis etsimun vitae etsum nisle varius sed aliquam etsim vitae netsum.</p>
                        </div>
                    </div>
                    <!--end col-md-4 -->
                </div>
                <!--end row -->
            </div>
            <!--end container -->
        </div>
        <!--end process-wrapper -->
    </section>
    <!--end section-white -->

    <!--begin pricing section -->
    <section id="pricing">
        <div class="section-bg-overlay"></div>
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center padding-bottom-40">
                    <h2 class="section-title white-text">Pricing made for everyone</h2>
                    <p class="section-subtitle white-text">All pricing packages are backed up by a 30-day money back guarantee.</p>
                </div>
                <!--end col-md-12 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->

        <!--begin pricing-wrapper -->
        <div class="pricing-wrapper">
            <!--begin container -->
            <div class="container">
                <!--begin row -->
                <div class="row">
                    <!--begin col-md-4-->
                    <div class="col-md-4">
                        <div class="price-box">
                            <ul class="pricing-list">
                                <li class="price-title">BASIC</li>
                                <li class="price-value">$15</li>
                                <li class="price-subtitle">Per Day</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Lorem Ipsum Ventis</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Netsum Etsum Nordus</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Curabitur Quamyus Etsum</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Etsimun Net Vitae</li>
                                <li class="price-tag-line"><a href="#">FREE 15-DAY TRIAL</a></li>
                            </ul>
                        </div>



                    </div>

                    <!--end col-md-4 -->



                    <!--begin col-md-4-->
                    <div class="col-md-4">
                        <div class="price-box grey-price-box">
                            <ul class="pricing-list">
                                <li class="price-title">STANDARD</li>
                                <li class="price-value">$99</li>
                                <li class="price-subtitle">Per Month</li>
                                <li class="price-text strong"><i class="fas fa-check"></i><strong>All Basic features</strong></li>
                                <li class="price-text"><i class="fas fa-check green"></i>Netsum Etsum Nordus</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Curabitur Quamyus Etsum</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Etsimun Net Vitae</li>
                                <li class="price-tag"><a href="#">FREE 15-DAY TRIAL</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--end col-md-4 -->

                    <!--begin col-md-4-->
                    <div class="col-md-4">
                        <div class="price-box">
                            <ul class="pricing-list">
                                <li class="price-title white-text">PRO</li>
                                <li class="price-value white-text">$199</li>
                                <li class="price-subtitle white-text">Per Year</li>
                                <li class="price-text white-text"><i class="fas fa-check green"></i><strong>All Standard Features</strong></li>
                                <li class="price-text"><i class="fas fa-check green"></i>Netsum Etsum Nordus</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Curabitur Quamyus Etsum</li>
                                <li class="price-text"><i class="fas fa-check green"></i>Etsimun Net Vitae</li>
                                <li class="price-tag-line"><a href="#">FREE 15-DAY TRIAL</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--end col-md-4 -->
                </div>
                <!--end row -->
            </div>
            <!--end container -->
        </div>
        <!--end pricing-wrapper -->
    </section>
    <!--end pricing section -->

    <!--begin footer -->
    <div class="footer">
        <!--begin container -->
        <div class="container footer-top">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-4 -->
                <div class="col-md-4 text-center">
                    <i class="pe-7s-map-2"></i>
                    <h5>Get In Touch</h5>
                    <p>10 Oxford Street, London, UK, E1 1EC</p>
                    <p><a href="mailto:contact@youremail.com">the-office@leadpage.co.uk</a></p>
                    <p>+44 987 654 321</p>
                </div>

                <!--end col-md-4 -->



                <!--begin col-md-4 -->

                <div class="col-md-4 text-center">
                    <i class="pe-7s-comment"></i>
                    <h5>Social Media</h5>
                    <p>See bellow where you can find us.</p>

                    <!--begin footer_social -->
                    <ul class="footer_social">
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-skype"></i></a></li>
                    </ul>
                    <!--end footer_social -->
                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4 -->
                <div class="col-md-4 text-center">
                    <i class="pe-7s-link"></i>
                    <h5>Useful Links</h5>



                    <a href="#" class="footer-links">Our Cookies Policy</a>



                    <a href="#" class="footer-links">Meet The Team Behind LeadPage</a>



                    <a href="#" class="footer-links">Terms and Conditions</a>



                </div>

                <!--end col-md-4 -->



            </div>

            <!--end row -->



        </div>

        <!--end container -->



        <!--begin container -->

        <div class="container-fluid footer-bottom px-0">



            <!--begin row -->

            <div class="row no-gutters mx-0">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">
                    <p>Copyright © 2020 <span class="template-name">LeadPage</span>. Designed by <a href="https://themeforest.net/user/epic-themes/portfolio?ref=Epic-Themes" target="_blank">Epic-Themes</a></p>
                </div>
                <!--end col-md-6 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->
    </div>
    <!--end footer -->

    @include('landing.block.footer')
</body>
</html>
