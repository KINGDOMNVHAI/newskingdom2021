@extends('news.master')
@section('NoiDung')
<div class="td-container td-post-template-default ">
    <div class="td-crumb-container">
        <div class="entry-crumbs">
            <span><a class="entry-crumb" href="{{asset('/')}}">Home</a></span>

            <i class="td-icon-right td-bread-sep td-bred-no-url-last"></i>
            <span class="td-bred-no-url-last">{{ $listpost[0]['name_category'] }}</span>
        </div>
    </div>
    <div class="td-pb-row">
        <div class="td-pb-span8 td-main-content">
            <div class="td-ss-main-content">
                <div class="clearfix"></div>
                <div class="td-block-row">
                    @foreach($listpost as $post)
                    <div class="td-block-span6" style="min-height:500px;">
                        <div class="td_module_1 td_module_wrap td-animation-stack">
                            <div class="td-module-image">
                                <div class="td-module-thumb">
                                    <a href="{{ route('post-content', $post->url_post ) }}" rel="bookmark" class="td-image-wrap" title="{{ $post->name_post }}">

                                    @if($post->img_post && file_exists('upload/images/thumbnail/' . $post->img_post))
                                        <img width="100%" class="entry-thumb td-animation-stack-type0-2" src="../upload/images/thumbnail/{{ $post->img_post }}" alt="{{ $post->name_post }}" title="{{ $post->name_post }}"></a>
                                    @else
                                        <img class="entry-thumb td-animation-stack-type0-2" src="../news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                        data-type="image_tag" data-img-url="../news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                    @endif

                                </div>
                            </div>
                            <h3 class="entry-title td-module-title"><a href="{{ route('post-content', $post->url_post ) }}" rel="bookmark" title="{{ $post->name_post }}">{{ $post->name_post }}</a></h3>
                            <div class="td-module-meta-info">
                                <span class="td-post-date" style="margin-bottom:10px;">
                                    <time class="entry-date updated td-module-date" datetime="{{ $post->date_post }}">{{ $post->date_post }}</time>
                                </span>
                                <p>{{ $post->present_post }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @if(empty($content->id_category) && !empty($_GET['search']))
                    @include('news.block.ads-rows')
                @endif
                {!! $listpost->links('pagination::bootstrap-4') !!}
                <div class="clearfix"></div>
            </div>
        </div>
        @include('news.block.widget')
    </div>
</div>

@endsection
