@extends('news.master')

@section('NoiDung')
    <div class="td-container td-post-template-default ">
        <div class="td-crumb-container">
            <div class="entry-crumbs">
            <span><a title="{{$content->name_detailpost}}" class="entry-crumb" href="{{asset('/')}}">Home</a></span>

            <i class="td-icon-right td-bread-sep"></i>
            <span><a title="{{$content->name_category}}" class="entry-crumb" href="{{ route('category-page', $content->url_category) }}">{{$content->name_category}}</a></span>

            <i class="td-icon-right td-bread-sep td-bred-no-url-last"></i>
            <span class="td-bred-no-url-last">{{$content->name_detailpost}}</span>
            </div>
        </div>
        <div class="td-pb-row">
            <div class="td-pb-span8 td-main-content" role="main">
                <div class="td-ss-main-content">
                    <div class="clearfix"></div>
                    <article id="post-149" class="post-149 post type-post status-publish format-standard has-post-thumbnail category-featured category-grid-1 category-grid-2 category-grid-3 category-grid-4 category-grid-5 category-grid-6 category-grid-7 category-grid-8 category-tagdiv-health-fitness category-module-1-layout category-module-10-layout category-module-11-layout category-module-12-layout category-module-13-layout category-module-14-layout category-module-15-layout category-module-16-layout category-module-17-layout category-module-2-layout category-module-3-layout category-module-4-layout category-module-5-layout category-module-6-layout category-module-7-layout category-module-8-layout category-module-9-layout category-no-grid category-template-style-1 category-template-style-2 category-template-style-3 category-template-style-4 category-template-style-5 category-template-style-6 category-template-style-7 category-template-style-8 tag-art tag-cool tag-design tag-tutorials tag-women tag-wordpress" itemscope="" itemtype="https://schema.org/Article">
                        <div class="td-post-header">
                            <!-- <ul class="td-category">
                                <li class="entry-category"><a style="background-color:#a444bd;color:#fff;border-color:#a444bd" href="../category/tagdiv-lifestyle/index.html">Lifestyle</a></li>
                                <li class="entry-category"><a style="background-color:#3fbcd5;color:#fff;border-color:#3fbcd5" href="../category/tagdiv-lifestyle/tagdiv-health-fitness/index.html">Health &amp; Fitness</a></li>
                            </ul> -->
                            <header class="td-post-title">
                                <h1 class="entry-title">{{$content->name_detailpost}}</h1>
                                <div class="td-module-meta-info">
                                    <div class="td-post-author-name">
                                        <div class="td-author-by">By</div> {{$content->author}}
                                    </div>
                                    <div class="td-post-views"><i class="td-icon-views"></i><span class="td-nr-views-149">{{$content->views}}</span></div>
                                    <div class="td-post-comments"><span class="td-nr-views-149">{{$content->nameCat}}</span></div>
                                </div>
                            </header>
                        </div>

                        <!-- <div class="td-post-sharing-top">
                            <div id="td_social_sharing_article_top" class="td-post-sharing td-ps-bg td-ps-padding td-post-sharing-style2 ">
                                <div class="td-post-sharing-visible"><a class="td-social-sharing-button td-social-sharing-button-js td-social-network td-social-facebook" href="" style="transition: opacity 0.2s ease 0s; opacity: 1;">
                                        <div class="td-social-but-icon"><i class="td-icon-facebook"></i></div>
                                        <div class="td-social-but-text">Facebook</div>
                                    </a><a class="td-social-sharing-button td-social-sharing-button-js td-social-network td-social-twitter" href="" style="transition: opacity 0.2s ease 0s; opacity: 1;">
                                        <div class="td-social-but-icon"><i class="td-icon-twitter"></i></div>
                                        <div class="td-social-but-text">Twitter</div>
                                    </a>
                                </div>
                                <div class="td-social-sharing-hidden" style="display: none;">
                                    <ul class="td-pulldown-filter-list"></ul><a class="td-social-sharing-button td-social-handler td-social-expand-tabs" href="#" data-block-uid="td_social_sharing_article_top">
                                        <div class="td-social-but-icon"><i class="td-icon-plus td-social-expand-tabs-icon"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div> -->

                        <div class="td-post-content">
                            <p style="16px;"><b>{!! $content->present_detailpost !!}</b></p>

                            {!! $content->content_detailpost !!}

                            <p style="color:blue;text-align:right;font-size:30px;font-weight:bold;">{{$content->signature}}</p>
                        </div>
                        <footer>
                            <div class="author-box-wrap"><a href="../author/admin/index.html"><img src="../upload/images/avatar/{{$content->avatar}}" width="150" height="150" alt="Armin Vans" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo td-animation-stack-type0-1"></a>
                                <div class="desc">
                                    <div class="td-author-name vcard author"><span class="fn">{{$content->signature}}</span></div>
                                    <div class="td-author-description">{{$content->description}}</div>
                                    <!-- Ẩn hiện tùy ý tác giả -->
                                    <!-- <div class="td-author-social">
                                        <span class="td-social-icon-wrap">
                                            <a target="_blank" href="https://www.facebook.com/TagDiv/" title="Facebook">
                                                <i class="td-icon-font td-icon-facebook"></i>
                                            </a>
                                        </span>
                                        <span class="td-social-icon-wrap">
                                            <a target="_blank" href="https://twitter.com/tagdivofficial" title="Twitter">
                                                <i class="td-icon-font td-icon-twitter"></i>
                                            </a>
                                        </span>
                                        <span class="td-social-icon-wrap">
                                            <a target="_blank" href="https://www.youtube.com/user/tagdiv" title="Youtube">
                                                <i class="td-icon-font td-icon-youtube"></i>
                                            </a>
                                        </span>
                                    </div> -->
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </footer>
                    </article>
                    <div class="td_block_wrap td_block_related_posts td_uid_17_5bea442a433db_rand td_with_ajax_pagination td-pb-border-top td_block_template_1" data-td-block-uid="td_uid_17_5bea442a433db">
                        <h4 class="td-related-title td-block-title">
                            <a id="td_uid_18_5bea442a43ca9" class="td-related-left td-cur-simple-item" data-td_filter_value="" data-td_block_id="td_uid_17_5bea442a433db" href="#">Cùng chuyên mục {{$content->name_category}}</a>
                        </h4>
                        <div id="td_uid_17_5bea442a433db" class="td_block_inner">
                            <div class="td-related-row">

                            @foreach($involves as $involve)

                                <div class="td-related-span4" style="min-height:250px;">
                                    <div class="td_module_related_posts td-animation-stack td_mod_related_posts">
                                        <div class="td-module-image">
                                            <div class="td-module-thumb"><a href="{{ route('post-content', $involve->url_detailpost ) }}" rel="bookmark" class="td-image-wrap" title="{{ $involve->name_detailpost }}">
                                            <img width="218" height="150" class="entry-thumb td-animation-stack-type0-1" src="../upload/images/thumbnail/{{ $involve->img_detailpost }}" alt="" title="{{ $involve->name_detailpost }}"></a></div>
                                        </div>
                                        <div class="item-details">
                                            <h3 class="entry-title td-module-title"><a href="{{ route('post-content', $involve->url_detailpost ) }}" rel="bookmark" title="{{ $involve->name_detailpost }}">{{ $involve->name_detailpost }}</a></h3>
                                        </div>
                                    </div>
                                </div>

                             @endforeach

                            </div>
                        </div>
                    </div>

                    <!-- <div class="comments" id="comments">
                        <div class="td-comments-title-wrap td_block_template_1">
                            <h4 class="td-comments-title block-title"><span>BÌNH LUẬN</span></h4>
                        </div>

                        <p>Bình luận bằng Facebook hoặc trên trang KINGDOM NVHAI</p>

                        <div class="row" style="min-height:120px;">
                            <div class="col-md-2" style="width:100%;height:100px;">
                                <img src="../news/facebook-icon.png" width="100%" onclick="listAjax('facebook')">
                            </div>
                            <div class="col-md-2" style="width:100%;height:100px;">
                                <img src="../news/kd-icon.png" width="100%" onclick="listAjax('kd')">
                            </div>
                        </div>

                        @foreach ($comments as $comment)
                        <div class="author-box-wrap" style="margin-bottom:10px;">
                            <img src="../news/kd-nvhai/{{ $comment->img_comment }}" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo">

                            <div class="desc">
                                <div class="td-author-name vcard author">
                                    <span class="fn">{{ $comment->name_comment }}</span>
                                </div>
                                <div class="td-author-url">{{ $comment->created_at }}</div>
                                <div class="td-author-description">{{ $comment->content_comment }}</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @endforeach

                        <div class="comments" id="comments">
                            <div id="respond" class="comment-respond">

                                <form action="{{ route('comment-insert', $content->id_detailpost) }}" method="post" enctype="multipart/form-data" id="commentform" class="comment-form">
                                    {{ csrf_field() }}
                                    <div class="clearfix"></div>

                                    @if(Auth::check())
                                    <div class="comment-form-input-wrap td-form-author">
                                        <p>Name: {!! Auth::user()->username !!}</p>
                                        <input class="" name="name_comment" type="hidden" value="{!! Auth::user()->username !!}">
                                        <input class="" name="id_user" type="hidden" value="{!! Auth::user()->id !!}">
                                    </div>
                                    @else
                                    <div class="comment-form-input-wrap td-form-author">
                                        <input class="" id="author" name="name_comment" placeholder="Name:*" type="text" value="" size="30" aria-required="true" style="border: 1px solid rgb(225, 225, 225);">
                                        <input class="" id="author" name="id_user" type="hidden" value="0">
                                        <div class="td-warning-author" style="">Please enter your name here</div>
                                    </div>
                                    @endif

                                    <div class="comment-form-input-wrap td-form-comment">
                                        <textarea placeholder="Comment:" id="comment" name="content_comment" cols="45" rows="8" aria-required="true" style="border: 1px solid rgb(225, 225, 225);"></textarea>
                                        <div class="td-warning-comment" style="">Please enter your comment!</div>
                                    </div>

                                    <div class="comment-form-input-wrap td-form-email">
                                        <p>Chọn ảnh avatar cho người dùng ẩn danh</p>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-bugatti-grand-sport-vitesse.jpg" checked />
                                            <img src="../news/kd-nvhai/anonymous-avatar-bugatti-grand-sport-vitesse.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-dva.jpg" />
                                            <img src="../news/kd-nvhai/anonymous-avatar-dva.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-lamborghini-urus.jpg" />
                                            <img src="../news/kd-nvhai/anonymous-avatar-lamborghini-urus.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-miku-swimming-suit.jpg" />
                                            <img src="../news/kd-nvhai/anonymous-avatar-miku-swimming-suit.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-nino-bunny-girl.jpg" />
                                            <img src="../news/kd-nvhai/anonymous-avatar-nino-bunny-girl.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-sky-kingdom.jpg" />
                                            <img src="../news/kd-nvhai/anonymous-avatar-sky-kingdom.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="anonymous-avatar-your-name.jpg" />
                                            <img src="../news/kd-nvhai/anonymous-avatar-your-name.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                        <label for="super-happy">
                                            <input type="radio" name="img_comment" class="super-happy" id="super-happy" value="avatar-kiryu-coco.jpg" />
                                            <img src="../news/kd-nvhai/avatar-kiryu-coco.jpg" width="96" height="96" class="avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo" onclick="">
                                        </label>

                                    </div>
                                    <!-- <p class="comment-form-cookies-consent">
                                        <input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes">
                                        <label for="wp-comment-cookies-consent">Save my name, email, and website in this browser for the next time I comment.</label>
                                    </p>
                                    <p class="form-submit">
                                        <input name="submit" type="submit" id="submit" class="submit" value="Post Comment">
                                        <!-- <input type="hidden" name="comment_post_ID" value="225" id="comment_post_ID">
                                        <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                </div>
            </div>
            @include('news.block.widget')
        </div>
    </div>
@endsection
