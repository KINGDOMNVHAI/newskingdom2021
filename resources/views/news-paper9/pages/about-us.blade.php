@extends('news.master')

@section('NoiDung')
    <div class="td-container td-post-template-default ">
        <div class="td-crumb-container">
            <div class="entry-crumbs">
            <span><a class="entry-crumb" href="{{asset('/')}}">Home</a></span>
            <i class="td-icon-right td-bread-sep td-bred-no-url-last"></i>
            <span class="td-bred-no-url-last">Giới thiệu và ủng hộ</span>
            </div>
        </div>
        <div class="td-pb-row">
            <div class="td-pb-span8 td-main-content" role="main">
                <div class="td-ss-main-content">
                    <div class="clearfix"></div>
                    <article id="post-149" class="post-149 post type-post status-publish format-standard has-post-thumbnail category-featured category-grid-1 category-grid-2 category-grid-3 category-grid-4 category-grid-5 category-grid-6 category-grid-7 category-grid-8 category-tagdiv-health-fitness category-module-1-layout category-module-10-layout category-module-11-layout category-module-12-layout category-module-13-layout category-module-14-layout category-module-15-layout category-module-16-layout category-module-17-layout category-module-2-layout category-module-3-layout category-module-4-layout category-module-5-layout category-module-6-layout category-module-7-layout category-module-8-layout category-module-9-layout category-no-grid category-template-style-1 category-template-style-2 category-template-style-3 category-template-style-4 category-template-style-5 category-template-style-6 category-template-style-7 category-template-style-8 tag-art tag-cool tag-design tag-tutorials tag-women tag-wordpress" itemscope="" itemtype="https://schema.org/Article">
                        <div class="td-post-header">
                            <header class="td-post-title">
                                <h1 class="entry-title">GIỚI THIỆU VÀ ỦNG HỘ</h1>
                            </header>
                        </div>

                        <div class="td-post-content">

                            <p>KINGDOM NVHAI là hệ thống website, kênh Youtube về Game, Anime - Manga, VTuber, thủ thuật IT và chuyên dịch Visual Novel, video nước ngoài. Được thành lập vào ngày 1/7/2015, KINGDOM NVHAI đã có một số lượng thành viên ủng hộ nhất định.</p>

                            <p>Mục tiêu ban đầu của KINGDOM NVHAI là:</p>
                            <ul>
                                <li>Đạt số lượng subcribes từ 5000 - 10000
                                <li>Kiếm một số tiền từ quảng cáo
                            </ul>

                            <img src="news/quyen-gop.jpg" width="100%">

                            <p>Tuy nhiên. Rất nhiều chuyện không may đã xảy ra. KINGDOM NVHAI đã bị chặn quảng cáo trên Youtube. Google Adsense cũng bị cấm. Số lượng subcribes không tăng nhiều.
                            Suốt 5 năm qua, KINGDOM NVHAI hoàn toàn hoạt động miễn phí, bỏ tiền túi gần 2 triệu/năm để duy trì tên miền, máy chủ cho trang web.</p>

                            <p>Vì vậy, sau 5 năm hoạt động, KINGDOM NVHAI mong muốn được nhận sự ủng hộ từ mọi người.</p>

                            <p>Chuyển khoản:</br>
                            Ngân hàng TMCP Tiên Phong: 0184 1733 201</p>

                            <p>Mọi đóng góp nhỏ của các bạn là động lực để website và kênh Youtube duy trì phát triển. Xin chân thành cảm ơn mọi người.</p>
                        </div>
                    </article>
                    <div class="clearfix"></div>
                </div>
            </div>
            @include('news.block.widget')
        </div>
    </div>
@endsection
