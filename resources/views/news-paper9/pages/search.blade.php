@extends('news.master')

@section('NoiDung')

<div class="td-container td-post-template-default ">
    <div class="td-crumb-container">
        <div class="entry-crumbs">
            <span><a class="entry-crumb" href="{{asset('/')}}">Home</a></span>

            <i class="td-icon-right td-bread-sep td-bred-no-url-last"></i>
            <span class="td-bred-no-url-last">Kết quả tìm kiếm</span>
        </div>
    </div>
    <div class="td-pb-row">
        <div class="td-pb-span8 td-main-content">
            <div class="td-ss-main-content">
                <div class="clearfix"></div>
                <div class="td-block-row">

                    <p style="font-size:16px"><b>Từ khóa: {{ $keyword }}</b></p>

                    @foreach($listpost as $post)

                    <div class="td-block-span6" style="min-height:500px;">
                        <div class="td_module_1 td_module_wrap td-animation-stack">
                            <div class="td-module-image">
                                <div class="td-module-thumb">
                                    <a href="{{ route('post-content', $post->url_detailpost ) }}" rel="bookmark" class="td-image-wrap" title="{{ $post->name_detailpost }}">
                                    <img width="100%" class="entry-thumb td-animation-stack-type0-2" src="../upload/images/thumbnail/{{ $post->img_detailpost }}" alt="{{ $post->name_detailpost }}" title="{{ $post->name_detailpost }}"></a>
                                </div>
                            </div>
                            <h3 class="entry-title td-module-title"><a href="{{ route('post-content', $post->url_detailpost ) }}" rel="bookmark" title="{{ $post->name_detailpost }}">{{ $post->name_detailpost }}</a></h3>
                            <div class="td-module-meta-info">
                                <span class="td-post-date" style="margin-bottom:10px;">
                                    <time class="entry-date updated td-module-date" datetime="{{ $post->date_detailpost }}">{{ $post->date_detailpost }}</time>
                                </span>
                                <p>{{ $post->present_detailpost }}</p>
                            </div>
                        </div>
                    </div>

                    @endforeach

                </div>
                {!! $listpost->links() !!}
                <div class="clearfix"></div>
            </div>
        </div>
        @include('news.block.widget')
    </div>
</div>

@endsection
