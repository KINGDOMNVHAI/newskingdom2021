@extends('news.master')

@section('NoiDung')
<div class=tdc-content-wrap>
    <div id=td_uid_1_5bea444cabcf6 class=tdc-row>
        <div class="vc_row td_uid_16_5bea444cabd31_rand  wpb_row td-pb-row">
            <style scoped>
                .td_uid_16_5bea444cabd31_rand {min-height:0}
            </style>
            <div class="vc_column td_uid_17_5bea444cabeda_rand  wpb_column vc_column_container tdc-column td-pb-span12">
                <div class="wpb_wrapper">
                    <div class="td_block_wrap td_block_big_grid_7 td_uid_18_5cb36650c55f4_rand td-grid-style-5 td-hover-1 td-big-grids td-pb-border-top td_block_template_1" data-td-block-uid="td_uid_18_5cb36650c55f4">
                        <style>
                            @media (max-width:767px) {
                                .td_uid_18_5cb36650c55f4_rand .td_block_inner .td-big-grid-scroll .entry-title { @mx6f_title }
                                .td_uid_18_5cb36650c55f4_rand .td-big-grid-scroll .td-post-category { @mx6f_cat }
                            }
                        </style>
                        <div id="td_uid_18_5cb36650c55f4" class="td_block_inner">
                            <div class="td-big-grid-wrapper">
                                @for($i = 0; $i < count($newest); $i++)
                                <div class="td_module_mx12 td-animation-stack td-big-grid-post-0 td-big-grid-post td-small-thumb">
                                    <div class="td-module-thumb">
                                        <a href="{{ route('post-content', $newest[$i]['url_detailpost'] ) }}" rel="bookmark" class="td-image-wrap" title="{{$newest[$i]['name_detailpost']}}">
                                        @if($newest[$i]['img_detailpost'] && file_exists('upload/images/thumbnail/' . $newest[$i]['img_detailpost']))
                                            <img class="entry-thumb td-animation-stack-type0-2" src="upload/images/thumbnail/{{ $newest[$i]['img_detailpost'] }}"
                                            alt="{{$newest[$i]['name_detailpost']}}" title="{{$newest[$i]['name_detailpost']}}"
                                            data-type="image_tag" data-img-url="upload/images/thumbnail/{{ $newest[$i]['img_detailpost'] }}" width="100%">
                                        @else
                                            <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                            data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                        @endif
                                        </a>
                                    </div>
                                    <div class="td-meta-info-container">
                                        <div class="td-meta-align">
                                            <div class="td-big-grid-meta">
                                                <a href="https://demo.tagdiv.com/newspaper/category/tagdiv-lifestyle/tagdiv-health-fitness/" class="td-post-category">{{$newest[$i]['name_category']}}</a>
                                                <h3 class="entry-title td-module-title"><a href="{{ route('post-content', $newest[$i]['url_detailpost'] ) }}" rel="bookmark" title="{{$newest[$i]['name_detailpost']}}">{{$newest[$i]['name_detailpost']}}</a></h3>
                                            </div>
                                            <div class="td-module-meta-info">
                                                <span class="td-post-date"><time class="entry-date updated td-module-date" datetime="{{$newest[$i]['date_detailpost']}}">{{$newest[$i]['date_detailpost']}}</time></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endfor
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
jQuery(window).load(function() {
    jQuery('body').find('#td_uid_1_5bea444cabcf6 .td-element-style').each(function(index, element) {
        jQuery(element).css('opacity', 1);
        return;
    });
});
</script>
<script>
jQuery(window).ready(function() {
    setTimeout(function() {
        var $content = jQuery('body').find('#tdc-live-iframe'),
            refWindow = undefined;
        if ($content.length) {
            $content = $content.contents();
            refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;
        } else {
            $content = jQuery('body');
            refWindow = window;
        }
        $content.find('#td_uid_1_5bea444cabcf6 .td-element-style').each(function(index, element) {
            jQuery(element).css('opacity', 1);
            return;
        });
    });
}, 200);
</script>
    <div id=td_uid_3_5bea444cadc92 class=tdc-row>
        <div class="vc_row td_uid_20_5bea444cadccb_rand td-ss-row wpb_row td-pb-row">
            <style scoped>
                .td_uid_20_5bea444cadccb_rand {min-height:0}
            </style>

            <!-- Left Content -->
            <div class="vc_column td_uid_21_5bea444cade47_rand  wpb_column vc_column_container tdc-column td-pb-span8">
                <div class=wpb_wrapper>
                    <!-- Website - Mạng Xã Hội -->
                    <div class="td_block_wrap td_block_1 td_uid_22_5bea444cadf56_rand td_with_ajax_pagination td-pb-border-top td_block_template_1 td-column-2" data-td-block-uid=td_uid_22_5bea444cadf56>
                        <style>
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-pulldown-filter-link:hover,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-subcat-item a:hover,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-subcat-item .td-cur-simple-item {color: #f9c100}

                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .block-title>*,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-subcat-dropdown:hover .td-subcat-more {background-color: #f9c100}

                            .td-theme-wrap .td-footer-wrapper .td_uid_22_5bea444cadf56_rand .block-title>* {padding: 6px 7px 5px;line-height: 1}

                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .block-title {border-color: #f9c100}

                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td_module_wrap:hover .entry-title a,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td_quote_on_blocks,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-opacity-cat .td-post-category:hover,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-opacity-read .td-read-more a:hover,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-opacity-author .td-post-author-name a:hover,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-instagram-user a {color: #f9c100}

                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-read-more a,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-weather-information:before,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-weather-week:before,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-exchange-header:before,
                            .td-theme-wrap .td-footer-wrapper .td_uid_22_5bea444cadf56_rand .td-post-category,
                            .td-theme-wrap .td_uid_22_5bea444cadf56_rand .td-post-category:hover {background-color: #f9c100}
                        </style>
                        <div class=td-block-title-wrap>
                            <h4 class="block-title td-block-title"><span class=td-pulldown-size>WEBSITE - MẠNG XÃ HỘI</span></h4>
                        </div>
                        <div id=td_uid_22_5bea444cadf56 class=td_block_inner>
                            <div class=td-block-row>
                                <div class=td-block-span6>
                                    <div class="td_module_4 td_module_wrap td-animation-stack">
                                        <div class=td-module-image>
                                            <div class=td-module-thumb>
                                                <a href="{{ route('post-content', $listWebsitePost[0]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listWebsitePost[0]->name_post}}">
                                                    @if($listWebsitePost[0]->img_post && file_exists('upload/images/thumbnail/' . $listWebsitePost[0]->img_post))
                                                        <img width=324 height=235 class=entry-thumb src="upload/images/thumbnail/{{ $listWebsitePost[0]->img_post }}" alt="{{$listWebsitePost[0]->name_post}}" title="{{$listWebsitePost[0]->name_post}}" />
                                                    @else
                                                        <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                        data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                                    @endif
                                                </a>
                                            </div>
                                            <a href="{{ route('post-content', $listWebsitePost[0]->url_post ) }}" class=td-post-category>{{$listWebsitePost[0]->name_category}}</a>
                                        </div>
                                        <h3 class="entry-title td-module-title">
                                            <a href="{{ route('post-content', $listWebsitePost[0]->url_post ) }}" rel=bookmark title="{{$listWebsitePost[0]->name_post}}">{{$listWebsitePost[0]->name_post}}</a>
                                        </h3>

                                        <div class=td-module-meta-info>
                                            <span class=td-post-date>
                                                <time class="entry-date updated td-module-date" datetime="{{$listWebsitePost[0]->date_post}}">{{$listWebsitePost[0]->date_post}}</time>
                                            </span>
                                        </div>

                                        <div class=td-excerpt>{{$listWebsitePost[0]->present_post}}</div>
                                    </div>
                                </div>

                                <div class=td-block-span6>
                                    @for($i = 1; $i < count($listWebsitePost); $i++)
                                    <div class="td_module_6 td_module_wrap td-animation-stack">
                                        <div class=td-module-thumb>
                                            <a href="{{ route('post-content', $listWebsitePost[$i]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listWebsitePost[$i]->name_post}}">
                                                @if($listWebsitePost[$i]->img_post && file_exists('upload/images/thumbnail/' . $listWebsitePost[$i]->img_post))
                                                    <img width=100 height=70 class=entry-thumb src="upload/images/thumbnail/{{ $listWebsitePost[$i]->img_post }}" alt="{{$listWebsitePost[$i]->name_post}}" title="{{$listWebsitePost[$i]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="80">
                                                @endif
                                            </a>
                                        </div>
                                        <div class=item-details>
                                            <h3 class="entry-title td-module-title">
                                                <a href="{{ route('post-content', $listWebsitePost[$i]->url_post ) }}" rel=bookmark title="{{$listWebsitePost[$i]->name_post}}">{{$listWebsitePost[$i]->name_post}}</a>
                                            </h3>
                                            <div class=td-module-meta-info>
                                                <span class=td-post-date><time class="entry-date updated td-module-date" datetime="{{$listWebsitePost[$i]->date_post}}">{{$listWebsitePost[$i]->date_post}}</time></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Game -->
                    <div class="vc_column td_uid_64_5bea444cbb03a_rand  wpb_column vc_column_container tdc-column td-pb-span6">
                        <div class=wpb_wrapper>
                            <div class="td_block_wrap td_block_19 td_uid_65_5bea444cbb13b_rand td-pb-border-top td_block_template_1 td-column-1" data-td-block-uid=td_uid_65_5bea444cbb13b>
                                <style>
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-pulldown-filter-link:hover,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-subcat-item a:hover,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-subcat-item .td-cur-simple-item {color: #4db2ec}

                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .block-title>*,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-subcat-dropdown:hover .td-subcat-more {background-color: #4db2ec}

                                    .td-theme-wrap .td-footer-wrapper .td_uid_65_5bea444cbb13b_rand .block-title>* {padding: 6px 7px 5px;line-height: 1}

                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .block-title {border-color: #4db2ec}

                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td_module_wrap:hover .entry-title a,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td_quote_on_blocks,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-opacity-cat .td-post-category:hover,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-opacity-read .td-read-more a:hover,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-opacity-author .td-post-author-name a:hover,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-instagram-user a {color: #4db2ec}

                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-read-more a,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-weather-information:before,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-weather-week:before,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-exchange-header:before,
                                    .td-theme-wrap .td-footer-wrapper .td_uid_65_5bea444cbb13b_rand .td-post-category,
                                    .td-theme-wrap .td_uid_65_5bea444cbb13b_rand .td-post-category:hover {background-color: #4db2ec}
                                </style>
                                <div class=td-block-title-wrap>
                                    <h4 class="block-title td-block-title"><span class=td-pulldown-size>GAME</span></h4>
                                </div>
                                <div id=td_uid_65_5bea444cbb13b class="td_block_inner td-column-1">
                                    <div class="td_module_mx1 td_module_wrap td-animation-stack">
                                        <div class="td-module-thumb" style="width:100%;">
                                            <a href="{{ route('post-content', $listGamePost[0]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listGamePost[0]->name_post}}">
                                                @if($listGamePost[0]->img_post && file_exists('upload/images/thumbnail/' . $listGamePost[0]->img_post))
                                                    <img width=356 height=220 class=entry-thumb src="upload/images/thumbnail/{{ $listGamePost[0]->img_post }}" alt="{{$listGamePost[0]->name_post}}" title="{{$listGamePost[0]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                                @endif
                                            </a>
                                        </div>
                                        <div class=td-module-meta-info>
                                            <h3 class="entry-title td-module-title">
                                                <a href="{{ route('post-content', $listGamePost[0]->url_post ) }}" rel=bookmark title="{{$listGamePost[0]->name_post}}">{{$listGamePost[0]->name_post}}</a>
                                            </h3>
                                            <div class=td-editor-date>
                                                <span class=td-author-date>
                                                    <span class=td-post-date><time class="entry-date updated td-module-date" datetime="{{$listGamePost[0]->date_post}}">{{$listGamePost[0]->date_post}}</time></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    @for($i = 1; $i < count($listGamePost); $i++)
                                    <div class="td_module_mx2 td_module_wrap td-animation-stack">
                                        <div class="td-module-thumb">
                                            <a href="{{ route('post-content', $listGamePost[$i]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listGamePost[$i]->name_post}}">

                                                @if($listGamePost[$i]->img_post && file_exists('upload/images/thumbnail/' . $listGamePost[$i]->img_post))
                                                    <img width=80 height=60 class=entry-thumb src="upload/images/thumbnail/{{ $listGamePost[$i]->img_post }}" alt="{{$listGamePost[$i]->name_post}}" title="{{$listGamePost[$i]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="80">
                                                @endif

                                            </a>
                                        </div>

                                        <div class=item-details>
                                            <h3 class="entry-title td-module-title">
                                                <a href="{{ route('post-content', $listGamePost[$i]->url_post ) }}" rel=bookmark title="{{$listGamePost[$i]->name_post}}">{{$listGamePost[$i]->name_post}}</a>
                                            </h3>
                                            <div class=td-module-meta-info>
                                                <span class=td-post-date><time class="entry-date updated td-module-date" datetime="{{$listGamePost[$i]->date_post}}">{{$listGamePost[$i]->date_post}}</time></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Anime - Manga -->
                    <div class="vc_column td_uid_66_5bea444cbced2_rand  wpb_column vc_column_container tdc-column td-pb-span6">
                        <div class=wpb_wrapper>
                            <div class="td_block_wrap td_block_19 td_uid_67_5bea444cbcf83_rand td-pb-border-top td_block_template_1 td-column-1" data-td-block-uid=td_uid_67_5bea444cbcf83>
                                <div class=td-block-title-wrap>
                                    <h4 class="block-title td-block-title"><span class=td-pulldown-size>ANIME &amp; MANGA</span></h4>
                                </div>
                                <div id=td_uid_67_5bea444cbcf83 class="td_block_inner td-column-1">
                                    <div class="td_module_mx1 td_module_wrap td-animation-stack">
                                        <div class="td-module-thumb" style="width:100%;">
                                            <a href="{{ route('post-content', $listAnimePost[0]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{ $listAnimePost[0]->name_post }}">
                                                @if($listAnimePost[0]->img_post && file_exists('upload/images/thumbnail/' . $listAnimePost[0]->img_post))
                                                    <img width=356 height=220 class=entry-thumb src="upload/images/thumbnail/{{ $listAnimePost[0]->img_post }}" alt="{{$listAnimePost[0]->name_post}}" title="{{$listAnimePost[0]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                                @endif
                                            </a>
                                        </div>

                                        <div class=td-module-meta-info>
                                            <h3 class="entry-title td-module-title">
                                                <a href="{{ route('post-content', $listAnimePost[0]->url_post ) }}" rel=bookmark title="{{ $listAnimePost[0]->name_post }}">{{ $listAnimePost[0]->name_post }}</a>
                                            </h3>
                                            <div class=td-editor-date>
                                                <span class=td-author-date>
                                                    <span class=td-post-date><time class="entry-date updated td-module-date" datetime="{{ $listAnimePost[0]->date_post }}">{{ $listAnimePost[0]->date_post }}</time></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    @for($i = 1; $i < count($listAnimePost); $i++)
                                    <div class="td_module_mx2 td_module_wrap td-animation-stack">
                                        <div class="td-module-thumb">
                                            <a href="{{ route('post-content', $listAnimePost[$i]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listAnimePost[$i]->name_post}}">
                                                @if($listAnimePost[$i]->img_post && file_exists('upload/images/thumbnail/' . $listAnimePost[$i]->img_post))
                                                    <img width="80" height="60" class=entry-thumb src="upload/images/thumbnail/{{ $listAnimePost[$i]->img_post }}" alt="{{$listAnimePost[$i]->name_post}}" title="{{$listAnimePost[$i]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="80">
                                                @endif

                                            </a>
                                        </div>
                                        <div class=item-details>
                                            <h3 class="entry-title td-module-title">
                                                <a href="{{ route('post-content', $listAnimePost[$i]->url_post ) }}" rel=bookmark title="{{$listAnimePost[$i]->name_post}}">{{$listAnimePost[$i]->name_post}}</a>
                                            </h3>
                                            <div class=td-module-meta-info>
                                                <span class=td-post-date><time class="entry-date updated td-module-date" datetime="{{$listAnimePost[$i]->date_post}}">{{$listAnimePost[$i]->date_post}}</time></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Thủ thuật IT -->
                    <div class="td_block_wrap td_block_1 td_uid_43_5bea444cb315d_rand td_with_ajax_pagination td-pb-border-top td_block_template_1 td-column-2" data-td-block-uid=td_uid_43_5bea444cb315d>
                        <style>
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-pulldown-filter-link:hover,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-subcat-item a:hover,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-subcat-item .td-cur-simple-item {color: #f44336}

                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .block-title>*,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-subcat-dropdown:hover .td-subcat-more {background-color: #f44336}

                            .td-theme-wrap .td-footer-wrapper .td_uid_43_5bea444cb315d_rand .block-title>* {padding: 6px 7px 5px;line-height: 1}

                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .block-title {border-color: #f44336}

                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td_module_wrap:hover .entry-title a,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td_quote_on_blocks,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-opacity-cat .td-post-category:hover,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-opacity-read .td-read-more a:hover,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-opacity-author .td-post-author-name a:hover,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-instagram-user a {color: #f44336}

                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-next-prev-wrap a:hover,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-load-more-wrap a:hover {background-color: #f44336;border-color: #f44336}

                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-read-more a,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-weather-information:before,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-weather-week:before,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-exchange-header:before,
                            .td-theme-wrap .td-footer-wrapper .td_uid_43_5bea444cb315d_rand .td-post-category,
                            .td-theme-wrap .td_uid_43_5bea444cb315d_rand .td-post-category:hover {background-color: #f44336}
                        </style>
                        <div class=td-block-title-wrap>
                            <h4 class="block-title td-block-title"><span class=td-pulldown-size>Thủ thuật IT</span></h4>
                        </div>
                        <div id=td_uid_43_5bea444cb315d class=td_block_inner>
                            <div class=td-block-row>
                                <div class=td-block-span6>
                                    <div class="td_module_4 td_module_wrap td-animation-stack">
                                        <div class=td-module-image>
                                            <div class=td-module-thumb>
                                                <a href="{{ route('post-content', $listTipITPost[0]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listTipITPost[0]->name_post}}">
                                                @if($listTipITPost[0]->img_post && file_exists('upload/images/thumbnail/' . $listTipITPost[0]->img_post))
                                                    <img width=324 height=235 class=entry-thumb src="upload/images/thumbnail/{{ $listTipITPost[0]->img_post }}" alt="{{$listTipITPost[0]->name_post}}" title="{{$listTipITPost[0]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                                @endif
                                                </a>
                                            </div>
                                            <a href="{{ route('post-content', $listTipITPost[0]->url_post ) }}" class=td-post-category>{{$listTipITPost[0]->name_category}}</a>
                                        </div>
                                        <h3 class="entry-title td-module-title">
                                            <a href="{{ route('post-content', $listTipITPost[0]->url_post ) }}" rel=bookmark title="{{$listTipITPost[0]->name_post}}">{{$listTipITPost[0]->name_post}}</a>
                                        </h3>
                                        <div class=td-module-meta-info>
                                            <span class=td-post-date>
                                                <time class="entry-date updated td-module-date" datetime="{{$listTipITPost[0]->date_post}}">{{$listTipITPost[0]->date_post}}</time>
                                            </span>
                                        </div>
                                        <div class=td-excerpt>{{$listTipITPost[0]->present_post}}</div>
                                    </div>
                                </div>

                                <div class=td-block-span6>
                                    @for($i = 1; $i < count($listTipITPost); $i++)
                                    <div class="td_module_6 td_module_wrap td-animation-stack">
                                        <div class=td-module-thumb>
                                            <a href="{{ route('post-content', $listTipITPost[$i]->url_post ) }}" rel=bookmark class=td-image-wrap title="{{$listTipITPost[$i]->name_post}}">
                                                @if($listTipITPost[$i]->img_post && file_exists('upload/images/thumbnail/' . $listTipITPost[$i]->img_post))
                                                    <img width=100 height=70 class=entry-thumb src="upload/images/thumbnail/{{$listTipITPost[$i]->img_post}}" alt="{{$listTipITPost[$i]->name_post}}" title="{{$listTipITPost[$i]->name_post}}" />
                                                @else
                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="80">
                                                @endif
                                            </a>
                                        </div>
                                        <div class=item-details>
                                            <h3 class="entry-title td-module-title">
                                                <a href="{{ route('post-content', $listTipITPost[$i]->url_post ) }}" rel=bookmark title="{{$listTipITPost[$i]->name_post}}">{{$listTipITPost[$i]->name_post}}</a>
                                            </h3>
                                            <div class=td-module-meta-info>
                                                <span class=td-post-date><time class="entry-date updated td-module-date" datetime="{{$listTipITPost[$i]->date_post}}">{{$listTipITPost[$i]->date_post}}</time></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor
                                </div>
                            </div>

                            <div class="vc_row td_uid_54_5bea444cb7cf3_rand  wpb_row td-pb-row">
                                <style scoped>
                                    .td_uid_54_5bea444cb7cf3_rand {min-height:0}
                                </style>
                                <div class="vc_column td_uid_55_5bea444cb7e6b_rand  wpb_column vc_column_container tdc-column td-pb-span12">
                                    <div class=wpb_wrapper>
                                        <div class="td_block_wrap td_block_11 td_uid_56_5bea444cb7f66_rand td-pb-border-top td_block_template_1 td-column-2" data-td-block-uid=td_uid_56_5bea444cb7f66>
                                            <div class=td-block-title-wrap>
                                                <h4 class="block-title td-block-title"><span class=td-pulldown-size>Cập nhật thường xuyên</span></h4>
                                            </div>
                                            <div id=td_uid_56_5bea444cb7f66 class=td_block_inner>
                                                @foreach($updates as $update)
                                                <div class=td-block-span12>
                                                    <div class="td_module_10 td_module_wrap td-animation-stack">
                                                        <div class=td-module-thumb>
                                                            <a href="{{ route('post-content', $update->url_detailpost ) }}" rel=bookmark class=td-image-wrap title="{{ $update->name_detailpost }}">
                                                                @if($update->img_detailpost && file_exists('upload/images/thumbnail/' . $update->img_detailpost))
                                                                    <img width=218 height=150 class=entry-thumb src="upload/images/thumbnail/{{ $update->img_detailpost }}" alt="{{ $update->name_detailpost }}" title="{{ $update->name_detailpost }}" />
                                                                @else
                                                                    <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                                                    data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="218">
                                                                @endif
                                                            </a>
                                                        </div>
                                                        <div class=item-details>
                                                            <h3 class="entry-title td-module-title">
                                                                <a href="{{ route('post-content', $update->url_detailpost ) }}" rel=bookmark
                                                                title="{{$update->present_detailpost}}">{{$update->name_detailpost}}</a>
                                                            </h3>
                                                            <div class=td-module-meta-info>
                                                                <span class=td-post-date><time class="entry-date updated td-module-date">{{ \Carbon\Carbon::parse($update->date_detailpost)->format('d-m-Y')}} </time></span>
                                                            </div>
                                                            <div class=td-excerpt>{{$update->present_detailpost}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class=clearfix></div>
                </div>
            </div>
            @include('news.block.widget')
        </div>
    </div>
<script>
jQuery(window).load(function() {
    jQuery('body').find('#td_uid_3_5bea444cadc92 .td-element-style').each(function(index, element) {
        jQuery(element).css('opacity', 1);
        return;
    });
});
</script>
<script>
jQuery(window).ready(function() {
    setTimeout(function() {
        var $content = jQuery('body').find('#tdc-live-iframe'),
            refWindow = undefined;
        if ($content.length) {
            $content = $content.contents();
            refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;
        } else {
            $content = jQuery('body');
            refWindow = window;
        }
        $content.find('#td_uid_3_5bea444cadc92 .td-element-style').each(function(index, element) {
            jQuery(element).css('opacity', 1);
            return;
        });
    });
}, 200);
</script>

    <div id=td_uid_6_5bea444cb7cba class=tdc-row></div>
<script>
jQuery(window).load(function() {
    jQuery('body').find('#td_uid_6_5bea444cb7cba .td-element-style').each(function(index, element) {
        jQuery(element).css('opacity', 1);
        return;
    });
});
</script>
<script>
jQuery(window).ready(function() {
    setTimeout(function() {
        var $content = jQuery('body').find('#tdc-live-iframe'),
            refWindow = undefined;
        if ($content.length) {
            $content = $content.contents();
            refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;
        } else {
            $content = jQuery('body');
            refWindow = window;
        }
        $content.find('#td_uid_6_5bea444cb7cba .td-element-style').each(function(index, element) {
            jQuery(element).css('opacity', 1);
            return;
        });
    });
}, 200);
</script>

    <div id=td_uid_9_5bea444cbaac9 class=tdc-row>
        <div class="vc_row td_uid_60_5bea444cbab02_rand  wpb_row td-pb-row">
            <style scoped>
                .td_uid_60_5bea444cbab02_rand {min-height:0}
            </style>
            <div class="vc_column td_uid_61_5bea444cbac6c_rand  wpb_column vc_column_container tdc-column td-pb-span12">
                <div class=wpb_wrapper>
                    <div class="td-a-rec td-a-rec-id-custom_ad_1  td_uid_62_5bea444cbad3c_rand td_block_template_1">

                        <!-- <span class=td-adspot-title>- Advertisement -</span>
                        <div class=td-visible-desktop>
                            <a href="#"><img class=td-retina style=max-width:728px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec728.jpg" alt="" width=728 height=90 /></a>
                        </div>

                        <div class=td-visible-tablet-landscape>
                            <a href="#"><img class=td-retina style=max-width:728px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec728.jpg" alt="" width=728 height=90 /></a>
                        </div>
                        <div class=td-visible-tablet-portrait>
                            <a href="#"><img class=td-retina style=max-width:468px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec468.jpg" alt="" width=468 height=60 /></a>
                        </div>

                        <div class=td-visible-phone>
                            <a href="#"><img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec300.jpg" alt="" width=300 height=250 /></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
jQuery(window).load(function() {
    jQuery('body').find('#td_uid_9_5bea444cbaac9 .td-element-style').each(function(index, element) {
        jQuery(element).css('opacity', 1);
        return;
    });
});
</script>
<script>
jQuery(window).ready(function() {
    setTimeout(function() {
        var $content = jQuery('body').find('#tdc-live-iframe'),
            refWindow = undefined;
        if ($content.length) {
            $content = $content.contents();
            refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;
        } else {
            $content = jQuery('body');
            refWindow = window;
        }
        $content.find('#td_uid_9_5bea444cbaac9 .td-element-style').each(function(index, element) {
            jQuery(element).css('opacity', 1);
            return;
        });
    });
}, 200);
</script>
</div>

@endsection
