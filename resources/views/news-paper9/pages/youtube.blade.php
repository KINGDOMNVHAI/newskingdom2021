@extends('news.master')

@section('NoiDung')

<div id=td_uid_1_5bea444cabcf6 class=tdc-row>
    <div class="vc_row td_uid_16_5bea444cabd31_rand  wpb_row td-pb-row">
        <style scoped>
            .td_uid_16_5bea444cabd31_rand {
                min-height: 0
            }
        </style>
        <div class="vc_column td_uid_17_5bea444cabeda_rand  wpb_column vc_column_container tdc-column td-pb-span12">
            <div class=wpb_wrapper>
                <div class="td_block_wrap td_block_big_grid_3 td_uid_19_5bea444cac99e_rand td-grid-style-1 td-hover-1 td-big-grids td-pb-border-top td_block_template_1" data-td-block-uid=td_uid_19_5bea444cac99e>
                    <div id=td_uid_19_5bea444cac99e class=td_block_inner>
                        <div class=td-big-grid-wrapper>
                            <h2>SUBSCRIBED YOUTUBE CHANNELS RANKING (MY FAVORITE CHANNELS)</h2>

                            <div class="row">
                                @foreach($youtuberankFavorite as $channelFavorite)
                                <div class="td-big-grid-scroll">
                                    <div class="td_module_mx6 td-animation-stack td-big-grid-post-2 td-big-grid-post td-small-thumb">
                                        <div class=td-module-thumb>
                                            <a href="{{ $channelFavorite->url_channel }}" target="_blank" rel=bookmark class=td-image-wrap title="niji-no-conquistador-channel-thumbnail">
                                                <img width=265 height=198 class=entry-thumb src="upload/images/channel/600x400/{{ $channelFavorite->thumbnail_channel }}" alt="{{ $channelFavorite->name_channel }}" title="{{ $channelFavorite->name_channel }}" />
                                            </a>
                                        </div>
                                        <div class=td-meta-info-container>
                                            <div class=td-meta-align>
                                                <div class=td-big-grid-meta>
                                                    <a href="{{ $channelFavorite->url_channel }}" class="td-post-category" style="font-size:16px;" target="_blank">
                                                    {{ $channelFavorite->fan_count }}
                                                    </a>
                                                    <h3 class="entry-title td-module-title">
                                                        <a href="{{ $channelFavorite->name_channel }}" rel=bookmark title="{{ $channelFavorite->name_channel }}" target="_blank">{{ $channelFavorite->name_channel }}</a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <h2>TOP 30 MOST SUBCRIBED CHANNELS</h2>

                            <p><b>Chọn danh sách | Choose the list.</b></p>

                            <div class="row" style="min-height: 200px;">
                                <div class="col-md-3" style="width:100%;height:100px;">
                                    <img src="news/youtube-ranking/youtube-ranking-button-hololive.jpg" alt="hololive virtual youtuber" width="100%" onclick="listAjax('hololive')">
                                </div>
                                <div class="col-md-3" style="width:100%;height:100px;">
                                    <img src="news/youtube-ranking/youtube-ranking-button-vr.jpg" alt="hololive virtual youtuber" width="100%" onclick="listAjax('vr')">
                                </div>
                                <div class="col-md-3" style="width:100%;height:100px;">
                                    <img src="news/youtube-ranking/youtube-ranking-button-vn.jpg" alt="hololive virtual youtuber" width="100%" onclick="listAjax('vn')">
                                </div>
                                <div class="col-md-3" style="width:100%;height:100px;">
                                    <img src="news/youtube-ranking/youtube-ranking-button-other.jpg" alt="pewdiepie kizuna ai" width="100%" onclick="listAjax('other')">
                                </div>
                            </div>

                            <div class="row">

                                <div id="loading" style="display:none;position: absolute;top:200;left:50%;">
                                    <img src="{{ asset('/upload/images/other/loading.gif') }}" width="100px;">
                                </div>

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                        <th scope="col" width="30%">Name</th>
                                        <th scope="col" width="50%">Present</th>
                                        <th scope="col">Video tiêu biểu</th>
                                        <th scope="col">Subcribe</th>
                                        </tr>
                                    </thead>
                                    <tbody id="getRequestData">
                                        @foreach($youtuberankAll as $channelAll)
                                        <tr>
                                            <th scope="row"><a href="{{ $channelAll->url_video }}" target="_blank" title="hololive virtual youtuber">{{ $channelAll->name_channel }}</a></th>
                                            <td>{!! $channelAll->description !!}</td>
                                            <td>
                                                <a href="{{ $channelAll->url_video }}" target="_blank">
                                                <img width="100" class=entry-thumb src="upload/images/channel/600x400/{{ $channelAll->thumbnail_channel }}" alt="{{ $channelAll->name_channel }}" title="{{ $channelAll->name_channel }}" />
                                                </a>
                                            </td>
                                            <td>{{ $channelAll->fan_count }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class=clearfix></div>
                    </div>
                </div>
            </div>
            @include('news.block.ads-rows')
        </div>
    </div>
</div>

<script src="{{ asset('/news/kd-nvhai/jquery-1.11.3.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).ajaxStart(function () {
            $("#loading").show();
        }).ajaxStop(function () {
            $("#loading").hide();
        });
    });

    function listAjax(list)
    {
        var url = '/youtube-channels-ranking-ajax/' + list;
        console.log(list);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // $.showLoading($('#getRequestData'));

        $.ajax({
            type:'POST',
            dataType: 'JSON',
            url: url,
            data:{
                list:list,
            },
            success:function(data){
                console.log(data);
                console.log(data.htmlTable);
        //         $.hideLoading($('#getRequestData'));
                $('#getRequestData').html(data.htmlTable);
        //         $('#getListSeeAll').html(data.seeAll);
                console.log(list);
            },
            error:function(xhr, data){
                // $.hideLoading($('#getRequestData'));
                console.log(xhr);
            }
        });
    }
</script>
@endsection
