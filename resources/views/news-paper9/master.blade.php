<!doctype html>
<html lang=en-US prefix="og: http://ogp.me/ns#">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>{{ $title }}</title>
    @include('news.block.meta')

    <link rel=apple-touch-icon-precomposed sizes=76x76 href="{{asset('news/wp-content/uploads/2016/04/ico-76.png')}}" />
    <link rel=apple-touch-icon-precomposed sizes=120x120 href="{{asset('news/wp-content/uploads/2016/04/ico-120.png')}}" />
    <link rel=apple-touch-icon-precomposed sizes=152x152 href="{{asset('news/wp-content/uploads/2016/04/ico-152.png')}}" />
    <link rel=apple-touch-icon-precomposed sizes=114x114 href="{{asset('news/wp-content/uploads/2016/04/ico-114.png')}}" />
    <link rel=apple-touch-icon-precomposed sizes=144x144 href="{{asset('news/wp-content/uploads/2016/04/ico-144.png')}}" />

    <link rel=dns-prefetch href='http://fonts.googleapis.com/' />
    <link rel=dns-prefetch href='http://s.w.org/' />

    <!-- Bootstrap core CSS -->
    <link href="{{asset('news/bootstrap.min.css')}}" rel="stylesheet" type='text/css'>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel=stylesheet id=td-plugin-multi-purpose-css href="{{asset('news/wp-content/plugins/td-composer/td-multi-purpose/A.style.css%2cqver%3dd88657a7207a66506e968bfbcddc545b.pagespeed.cf.DIkkGheK9m.css')}}" type='text/css' media=all />
    <link rel=stylesheet id=google-fonts-style-css href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400%2C400italic%2C600%2C600italic%2C700%7CRoboto%3A300%2C400%2C400italic%2C500%2C500italic%2C700%2C900&amp;ver=9.1_d74" type='text/css' media=all />
    <link rel=stylesheet id=td-theme-css href="{{asset('news/wp-content/themes/011/A.style.css%2cqver%3d9.1_d74.pagespeed.cf.qICKaql5EG.css')}}" href='' type='text/css' media=all />

    <link rel=stylesheet id=tdb_front_style-css href="{{asset('news/wp-content/plugins/td-cloud-library/assets/css/A.tdb_less_front.css%2cqver%3d11bc06c909ebeb70993dc8e20cf34e8a.pagespeed.cf.jGmtab374E.css')}}" type='text/css' media=all />

    <script type='text/javascript' src="{{asset('news/wp-includes/js/jquery/jquery.js%2cqver%3d1.12.4.pagespeed.jm.pPCPAKkkss.js')}}"></script>
    <link rel='https://api.w.org/' href="{{asset('news/wp-json/index.html')}}" />

    @include('news.block.cssjs-unknow')

    <link rel=stylesheet id=td-theme-css href="{{asset('news/kd-nvhai/style-nvhai.css')}}" href='' type='text/css' media=all />
</head>

<body class="home page-template page-template-page-pagebuilder-latest page-template-page-pagebuilder-latest-php page page-id-13 homepage global-block-template-1 td-animation-stack-type0 td-full-layout" itemscope=itemscope itemtype="https://schema.org/WebPage">

    @include('news.block.header')

    <div id="td-outer-wrap" class="td-theme-wrap">

        @include('news.block.main-menu')

        <div class="td-main-content-wrap td-main-page-wrap td-container-wrap">

            @yield('NoiDung')

        </div>

        @include('news/block/footer')
    </div>

    <script defer type='text/javascript' src="{{asset('news/wp-content/themes/011/js/tagdiv_theme.min.js%2cqver%3d9.1_d74.pagespeed.jm.7SdxKbl8F9.js')}}"></script>
    <script defer type='text/javascript' src="{{asset('news/wp-content/plugins/td-cloud-library/assets/js/js_files_for_front.min.js%2cqver%3d11bc06c909ebeb70993dc8e20cf34e8a.pagespeed.jm.0o5lo9ADmu.js')}}"></script>
    <script defer type='text/javascript' src="{{asset('news/wp-includes/js/wp-embed.minb20c.js?ver=b1a8c55f')}}"></script>
</body>

</html>
