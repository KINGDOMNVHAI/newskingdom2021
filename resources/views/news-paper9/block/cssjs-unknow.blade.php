<script type="text/javascript">
    window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/",
        "svgExt": ".svg",
        "source": {
            "concatemoji": "https:\/\/demo.tagdiv.com\/newspaper\/wp-includes\/js\/wp-emoji-release.min.js?ver=b1a8c55f"
        }
    };
    ! function(a, b, c) {
        function d(a, b) {
            var c = String.fromCharCode;
            l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
            var d = k.toDataURL();
            l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
            var e = k.toDataURL();
            return d === e
        }

        function e(a) {
            var b;
            if (!l || !l.fillText) return !1;
            switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                case "flag":
                    return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                case "emoji":
                    return b = d([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039]), !b
            }
            return !1
        }

        function f(a) {
            var c = b.createElement("script");
            c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
        }
        var g, h, i, j, k = b.createElement("canvas"),
            l = k.getContext && k.getContext("2d");
        for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
        c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function() {
            c.DOMReady = !0
        }, c.supports.everything || (h = function() {
            c.readyCallback()
        }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function() {
            "complete" === b.readyState && c.readyCallback()
        })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
    }(window, document, window._wpemojiSettings);
</script>
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -.1em !important;
        background: none !important;
        padding: 0 !important
    }
</style>
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <style type="text/css">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important
        }
    </style>
    <script>
        var td_is_safari = false;
        var td_is_ios = false;
        var td_is_windows_phone = false;
        var ua = navigator.userAgent.toLowerCase();
        var td_is_android = ua.indexOf('android') > -1;
        if (ua.indexOf('safari') != -1) {
            if (ua.indexOf('chrome') > -1) {} else {
                td_is_safari = true;
            }
        }
        if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
            td_is_ios = true;
        }
        if (navigator.userAgent.match(/Windows Phone/i)) {
            td_is_windows_phone = true;
        }
        if (td_is_ios || td_is_safari || td_is_windows_phone || td_is_android) {
            if (top.location != location) {
                top.location.replace('index.html');
            }
        }
        var tdBlocksArray = [];

        function tdBlock() {
            this.id = '';
            this.block_type = 1;
            this.atts = '';
            this.td_column_number = '';
            this.td_current_page = 1;
            this.post_count = 0;
            this.found_posts = 0;
            this.max_num_pages = 0;
            this.td_filter_value = '';
            this.is_ajax_running = false;
            this.td_user_action = '';
            this.header_color = '';
            this.ajax_pagination_infinite_stop = '';
        }(function() {
            var htmlTag = document.getElementsByTagName("html")[0];
            if (navigator.userAgent.indexOf("MSIE 10.0") > -1) {
                htmlTag.className += ' ie10';
            }
            if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                htmlTag.className += ' ie11';
            }
            if (navigator.userAgent.indexOf("Edge") > -1) {
                htmlTag.className += ' ieEdge';
            }
            if (/(iPad|iPhone|iPod)/g.test(navigator.userAgent)) {
                htmlTag.className += ' td-md-is-ios';
            }
            var user_agent = navigator.userAgent.toLowerCase();
            if (user_agent.indexOf("android") > -1) {
                htmlTag.className += ' td-md-is-android';
            }
            if (-1 !== navigator.userAgent.indexOf('Mac OS X')) {
                htmlTag.className += ' td-md-is-os-x';
            }
            if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                htmlTag.className += ' td-md-is-chrome';
            }
            if (-1 !== navigator.userAgent.indexOf('Firefox')) {
                htmlTag.className += ' td-md-is-firefox';
            }
            if (-1 !== navigator.userAgent.indexOf('Safari') && -1 === navigator.userAgent.indexOf('Chrome')) {
                htmlTag.className += ' td-md-is-safari';
            }
            if (-1 !== navigator.userAgent.indexOf('IEMobile')) {
                htmlTag.className += ' td-md-is-iemobile';
            }
        })();
        var tdLocalCache = {};
        (function() {
            "use strict";
            tdLocalCache = {
                data: {},
                remove: function(resource_id) {
                    delete tdLocalCache.data[resource_id];
                },
                exist: function(resource_id) {
                    return tdLocalCache.data.hasOwnProperty(resource_id) && tdLocalCache.data[resource_id] !== null;
                },
                get: function(resource_id) {
                    return tdLocalCache.data[resource_id];
                },
                set: function(resource_id, cachedData) {
                    tdLocalCache.remove(resource_id);
                    tdLocalCache.data[resource_id] = cachedData;
                }
            };
        })();
        var tds_login_sing_in_widget = "show";
        var td_viewport_interval_list = [{
            "limitBottom": 767,
            "sidebarWidth": 228
        }, {
            "limitBottom": 1018,
            "sidebarWidth": 300
        }, {
            "limitBottom": 1140,
            "sidebarWidth": 324
        }];
        var td_animation_stack_effect = "type0";
        var tds_animation_stack = true;
        var td_animation_stack_specific_selectors = ".entry-thumb, img";
        var td_animation_stack_general_selectors = ".td-animation-stack img, .td-animation-stack .entry-thumb, .post img";
        var td_ajax_url = "index.html\/\/demo.tagdiv.com\/newspaper\/wp-admin\/admin-ajax.php?td_theme_name=Newspaper&v=9.1_d74";
        var td_get_template_directory_uri = "index.html\/\/demo.tagdiv.com\/newspaper\/wp-content\/themes\/011";
        var tds_snap_menu = "smart_snap_always";
        var tds_logo_on_sticky = "show_header_logo";
        var tds_header_style = "";
        var td_please_wait = "Please wait...";
        var td_email_user_pass_incorrect = "User or password incorrect!";
        var td_email_user_incorrect = "Email or username incorrect!";
        var td_email_incorrect = "Email incorrect!";
        var tds_more_articles_on_post_enable = "";
        var tds_more_articles_on_post_time_to_wait = "";
        var tds_more_articles_on_post_pages_distance_from_top = 0;
        var tds_theme_color_site_wide = "#4db2ec";
        var tds_smart_sidebar = "enabled";
        var tdThemeName = "Newspaper";
        var td_magnific_popup_translation_tPrev = "Previous (Left arrow key)";
        var td_magnific_popup_translation_tNext = "Next (Right arrow key)";
        var td_magnific_popup_translation_tCounter = "%curr% of %total%";
        var td_magnific_popup_translation_ajax_tError = "The content from %url% could not be loaded.";
        var td_magnific_popup_translation_image_tError = "The image #%curr% could not be loaded.";
        var tdDateNamesI18n = {
            "month_names": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "month_names_short": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            "day_names": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "day_names_short": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        };
        var td_ad_background_click_link = "";
        var td_ad_background_click_target = "";
    </script>
    <style>
        .block-title>span,
        .block-title>span>a,
        .block-title>a,
        .block-title>label,
        .widgettitle,
        .widgettitle:after,
        .td-trending-now-title,
        .td-trending-now-wrapper:hover .td-trending-now-title,
        .wpb_tabs li.ui-tabs-active a,
        .wpb_tabs li:hover a,
        .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container .vc_tta-tab.vc_active>a,
        .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container .vc_tta-tab:hover>a,
        .td_block_template_1 .td-related-title .td-cur-simple-item,
        .woocommerce .product .products h2:not(.woocommerce-loop-product__title),
        .td-subcat-filter .td-subcat-dropdown:hover .td-subcat-more,
        .td-weather-information:before,
        .td-weather-week:before,
        .td_block_exchange .td-exchange-header:before,
        .td-theme-wrap .td_block_template_3 .td-block-title>*,
        .td-theme-wrap .td_block_template_4 .td-block-title>*,
        .td-theme-wrap .td_block_template_7 .td-block-title>*,
        .td-theme-wrap .td_block_template_9 .td-block-title:after,
        .td-theme-wrap .td_block_template_10 .td-block-title::before,
        .td-theme-wrap .td_block_template_11 .td-block-title::before,
        .td-theme-wrap .td_block_template_11 .td-block-title::after,
        .td-theme-wrap .td_block_template_14 .td-block-title,
        .td-theme-wrap .td_block_template_15 .td-block-title:before,
        .td-theme-wrap .td_block_template_17 .td-block-title:before {
            background-color: #222
        }

        .woocommerce div.product .woocommerce-tabs ul.tabs li.active {
            background-color: #222 !important
        }

        .block-title,
        .td_block_template_1 .td-related-title,
        .wpb_tabs .wpb_tabs_nav,
        .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container,
        .woocommerce div.product .woocommerce-tabs ul.tabs:before,
        .td-theme-wrap .td_block_template_5 .td-block-title>*,
        .td-theme-wrap .td_block_template_17 .td-block-title,
        .td-theme-wrap .td_block_template_17 .td-block-title::before {
            border-color: #222
        }

        .td-theme-wrap .td_block_template_4 .td-block-title>*:before,
        .td-theme-wrap .td_block_template_17 .td-block-title::after {
            border-color: #222 transparent transparent transparent
        }

        .td-theme-wrap .td_block_template_4 .td-related-title .td-cur-simple-item:before {
            border-color: #222 transparent transparent transparent !important
        }

        .td-menu-background:before,
        .td-search-background:before {
            background: rgba(0, 0, 0, .5);
            background: -moz-linear-gradient(top, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .6) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0, 0, 0, .5)), color-stop(100%, rgba(0, 0, 0, .6)));
            background: -webkit-linear-gradient(top, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .6) 100%);
            background: -o-linear-gradient(top, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .6) 100%);
            background: -ms-linear-gradient(top, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .6) 100%);
            background: linear-gradient(to bottom, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .6) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='rgba(0,0,0,0.5)', endColorstr='rgba(0,0,0,0.6)', GradientType=0)
        }

        .td-footer-wrapper,
        .td-footer-wrapper .td_block_template_7 .td-block-title>*,
        .td-footer-wrapper .td_block_template_17 .td-block-title,
        .td-footer-wrapper .td-block-title-wrap .td-wrapper-pulldown-filter {
            background-color: #111
        }

        .td-footer-wrapper::before {
            background-image: url(/news/wp-content/uploads/2017/04/footer-bg1.jpg)
        }

        .td-footer-wrapper::before {
            background-size: cover
        }

        .td-footer-wrapper::before {
            background-position: center center
        }

        .td-footer-wrapper::before {
            opacity: .1
        }

        .td-menu-background,
        .td-search-background {
            background-image: url(https://demo.tagdiv.com/newspaper/wp-content/uploads/2016/05/mobile-bg.jpg)
        }

        .white-popup-block:before {
            background-image: url(https://demo.tagdiv.com/newspaper/wp-content/uploads/2016/05/login-mod.jpg)
        }
    </style>
    <noscript><img height=1 width=1 style=display:none src="https://www.facebook.com/tr?id=853851948102692&amp;ev=PageView&amp;noscript=1" /></noscript>

    <style id=tdw-css-placeholder></style>
