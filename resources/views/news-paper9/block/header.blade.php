<div class="td-scroll-up"><i class=td-icon-menu-up></i></div>
<div class="td-menu-background"></div>
<div id="td-mobile-nav">
    <div class=td-mobile-container>
        <div class="td-menu-socials-wrap">
            <div class="td-menu-socials">
                <span class=td-social-icon-wrap>
                    <a target=_blank href="#" title=Facebook><i class="td-icon-font td-icon-facebook"></i></a>
                </span>
                <span class=td-social-icon-wrap>
                    <a target=_blank href="#" title=Twitter><i class="td-icon-font td-icon-twitter"></i></a>
                </span>
                <span class=td-social-icon-wrap>
                    <a target=_blank href="#" title=Vimeo><i class="td-icon-font td-icon-vimeo"></i></a>
                </span>
                <span class=td-social-icon-wrap>
                    <a target=_blank href="#" title=Youtube><i class="td-icon-font td-icon-youtube"></i></a>
                </span>
            </div>
            <div class=td-mobile-close>
                <a href="#"><i class=td-icon-close-mobile></i></a>
            </div>
        </div>

        <!-- Menu Mobile -->
        <div class="td-mobile-content">
            <div class=menu-main-menu-container>
                <ul id=menu-main-menu class=td-mobile-main-menu>
                    <li id=menu-item-15 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-13 current_page_item menu-item-first menu-item-15">
                        <a href="{{ route('about-kingdom-nvhai') }}">Giới thiệu</a>
                    </li>

                    <li id=menu-item-15 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-13 current_page_item menu-item-first menu-item-15">
                        <a href="{{ route('subscribed-youtube-channels-ranking') }}">Subscribe Youtube Channels Ranking</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class=td-search-background></div>

<div class=td-search-wrap-mob>
    <div class=td-drop-down-search aria-labelledby=td-header-search-button>
        <form method=get class=td-search-form action="https://demo.tagdiv.com/newspaper/">
            <div class=td-search-close>
                <a href="#"><i class=td-icon-close-mobile></i></a>
            </div>
            <div role=search class=td-search-input>
                <span>Search</span>
                <input id=td-header-search-mob type=text value="" name=s autocomplete=off />
            </div>
        </form>
        <div id=td-aj-search-mob></div>
    </div>
</div>
