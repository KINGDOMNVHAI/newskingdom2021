<div class="td-footer-wrapper td-container-wrap ">
    <div class=td-footer-bottom-full>
        <div class=td-container>
            <div class=td-pb-row>
                <div class=td-pb-span3>
                    <aside class="td_block_template_1 widget widget_tag_cloud">
                        <h4 class="block-title"><span>TAGS</span></h4>
                        <div class="tagcloud">
                            @for($i = 0; $i < count($viewTags); $i++)
                            <a href="/list-search-post?_token={{ csrf_token() }}&keyword={{$viewTags[$i]}}&category=all&date=desc&search=Search" class="tag-cloud-link tag-link-47 tag-link-position-1" style="font-size:10.470588235294pt"
                            aria-label="apple (2 items)">{{$viewTags[$i]}}</a>
                            @endfor
                        </div>
                    </aside>
                </div>
                <div class=td-pb-span5>
                    <aside class=footer-text-wrap>
                        <div class=block-title><span>ABOUT US</span></div>
                        &copy; Copyright - Newspaper by TagDiv
                        <p>Website được thành lập năm 2015, dùng giao diện của Newspaper9 và lập trình bởi KINGDOM NVHAI</p>
                        <!-- <div class=footer-email-wrap>Liên Hệ: <a href="mailto:contact@yoursite.com">contact@yoursite.com</a></div> -->
                    </aside>
                </div>
                <div class=td-pb-span4>
                    <aside class="footer-social-wrap td-social-style-2">
                        <div class=block-title><span>FOLLOW US</span></div>
                        <span class=td-social-icon-wrap>
                            <a target=_blank href="{{FACEBOOK_URL}}" title=Facebook>
                                <i class="td-icon-font td-icon-facebook"></i>
                            </a>
                        </span>
                        <span class=td-social-icon-wrap>
                            <a target=_blank href="{{TWITTER_URL}}" title=Twitter>
                                <i class="td-icon-font td-icon-twitter"></i>
                            </a>
                        </span>
                        <span class=td-social-icon-wrap>
                            <a target=_blank href="{{YOUTUBE_URL}}" title=Youtube>
                                <i class="td-icon-font td-icon-youtube"></i>
                            </a>
                        </span>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>
