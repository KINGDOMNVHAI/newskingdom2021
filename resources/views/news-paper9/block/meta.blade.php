<!-- =========== SEO =========== -->
<!-- Meta -->
<meta charset=UTF-8 />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noodp,index,follow" />
<meta name="keywords" content="hololive virtual youtuber, pewdiepie kizuna ai" />
<!-- <link rel=pingback href="xmlrpc.php" /> -->

<meta http-equiv="content-language" content="vi" />
<meta name="dc.title" content="{{$title}}" />
@if(isset($content->img_detailpost))
<meta name="description" content="{{$content->present_detailpost}}" />
@else
<meta name="description" content="welcome to NEWS KINGDOM NVHAI, where you can find news about anime manga game and more" />
@endif

<!-- <link rel=canonical href="index.html" /> -->
<meta property="og:locale" content=en_US />
<meta property="og:type" content="article">
<meta property="og:title" content="{{$title}}" />
@if(isset($content->img_detailpost))
<meta property="og:image" content="{{ "http://" . $_SERVER['SERVER_NAME'] . "/upload/images/thumbnail/" . $content->img_detailpost }}" />
<meta property="og:description" content="{{$content->present_detailpost}}" />
@else
<meta property="og:description" content="Welcome to NEWS KINGDOM NVHAI, where you can find news about anime manga game and more" />
@endif
<meta property="og:url" content="{{ "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] }}" />
<meta property="og:site_name" content="KINGDOM NVHAI" />

<!-- Xem thẻ rel=alternate -->

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-65258802-1', 'auto');
ga('send', 'pageview');
</script>

<!-- Fanpage -->
<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!-- Google Webmaster Tool -->

<!-- Token Laravel -->
<meta name="csrf-token" content="{{ csrf_token() }}">
