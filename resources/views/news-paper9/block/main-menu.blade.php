<div class="td-header-wrap td-header-style-1">
    <div class="td-header-top-menu-full td-container-wrap">
        <div class="td-container td-header-row td-header-top-menu">
            <div class=top-bar-style-1>
                <div class=td-header-sp-top-menu>
                    <!-- <div class=td-weather-top-widget id=td_top_weather_uid>
                        <i class="fas fa-cloud-moon"></i>
                        <i class="fas fa-cloud-sun"></i>
                        <i class="fas fa-cloud-rain"></i>
                        <i class="td-icons broken-clouds-n"></i>
                        <div class=td-weather-now data-block-uid=td_top_weather_uid>
                            <span class=td-big-degrees></span>
                            <span class=td-weather-unit>C</span>
                        </div>
                        <div class=td-weather-header>
                            <div class=td-weather-city></div>
                        </div>
                    </div> -->
                    <div class=td_data_time>
                        <div>
                        <?php
                            $currentDay = date('N');
                            if ($currentDay == 1) { $day = 'Monday'; }
                            else if ($currentDay == 2) { $day = 'Tuesday'; }
                            else if ($currentDay == 3) { $day = 'Wednesday'; }
                            else if ($currentDay == 4) { $day = 'Thursday'; }
                            else if ($currentDay == 5) { $day = 'Friday'; }
                            else if ($currentDay == 6) { $day = 'Saturday'; }
                            else if ($currentDay == 7) { $day = 'Sunday'; }
                        ?>
                        {{$day}} {{date('Y-m-d')}}
                        </div>
                    </div>
                    <div class=menu-top-container>
                        <ul id=menu-top-menu class=top-header-menu>
                            <li id=menu-item-10 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-first td-menu-item td-normal-menu menu-item-10">
                                <a href="{{asset('/')}}">KINGDOM NVHAI</a>
                            </li>
                            <li id=menu-item-11 class="menu-item menu-item-type-custom menu-item-object-custom td-menu-item td-normal-menu menu-item-11">
                                <a target=_blank href="{{YOUTUBE_URL}}">YOUTUBE CHANNEL</a>
                            </li>
                            <li id=menu-item-9 class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-9">
                                <a target=_blank href="{{KAWAIICODE_URL}}">KAWAIICODE</a>
                            </li>
                            <li id=menu-item-12 class="menu-item menu-item-type-custom menu-item-object-custom td-menu-item td-normal-menu menu-item-12">
                                <a target=_blank href="{{DALFC_URL}}">DATE A LIVE FAN CLUB</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="td-banner-wrap-full td-logo-wrap-full td-container-wrap ">
        <div class="td-container td-header-row td-header-header">
            <div class=td-header-sp-logo>
                <h1 class=td-logo>
                    <a class=td-main-logo href="{{asset('/')}}">
                        <img class=td-retina-data data-retina="{{asset('news/Mascot-LG-KINGDOMNVHAI-small-blue.png')}}" src="{{asset('news/Mascot-LG-KINGDOMNVHAI-small-blue.png')}}" alt="NEWS KINGDOM NVHAI" title="NEWS KINGDOM NVHAI" />
                        <span class=td-visual-hidden>KINGDOM NVHAI</span>
                    </a>
                </h1>
            </div>
            <div class=td-header-sp-recs>
                <div class=td-header-rec-wrap>
                    <!-- Quảng cáo ngang -->
                    @if(empty($content->id_category) && !empty($_GET['search']))
                        @include('news.block.ads-rows')
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="td-header-menu-wrap-full td-container-wrap ">
        <div class="td-header-menu-wrap td-header-gradient ">
            <div class="td-container td-header-row td-header-main-menu">
                <div id=td-header-menu role=navigation>
                    <div id="td-top-mobile-toggle">
                        <a href="#"><i class="td-icon-font td-icon-mobile"></i></a>
                    </div>
                    <div class="td-main-menu-logo td-logo-in-header">
                        <a class="td-mobile-logo td-sticky-header" href="{{asset('/')}}">
                            <img class=td-retina-data data-retina="{{asset('news/Mascot-LG-KINGDOMNVHAI-small-white.png')}}" src="{{asset('news/Mascot-LG-KINGDOMNVHAI-small-white.png')}}" alt="NEWS KINGDOM NVHAI" title="NEWS KINGDOM NVHAI" />
                        </a>
                        <a class="td-header-logo td-sticky-header" href="{{asset('/')}}">
                            <img class=td-retina-data data-retina="{{asset('news/Mascot-LG-KINGDOMNVHAI-small-white.png')}}" src="{{asset('news/Mascot-LG-KINGDOMNVHAI-small-white.png')}}" alt="NEWS KINGDOM NVHAI" title="NEWS KINGDOM NVHAI" />
                        </a>
                    </div>

                    <!-- Menu Website -->
                    <div class=menu-main-menu-container>
                        <ul id=menu-main-menu-1 class=sf-menu>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category tdb-menu-item-button tdb-menu-item tdb-normal-menu menu-item-261">
                                <a href="{{ route('about-kingdom-nvhai') }}">Giới thiệu và ủng hộ</a>
                            </li>

                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children td-menu-item td-normal-menu menu-item-307"><a href="#">Chuyên mục</a>
                                <ul class=sub-menu>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-1112">
                                        <a href="{{ route('category-page', 'website-mang-xa-hoi' ) }}">Website - Mạng xã hội</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-1112">
                                        <a href="{{ route('category-page', 'game' ) }}">Game</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-1112">
                                        <a href="{{ route('category-page', 'anime-manga' ) }}">Anime - Manga</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-1112">
                                        <a href="{{ route('category-page', 'thu-thuat-it' ) }}">Thủ thuật IT</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category tdb-menu-item-button tdb-menu-item tdb-normal-menu menu-item-261">
                                <a href="{{ route('subscribed-youtube-channels-ranking') }}">SUBSCRIBED YOUTUBE CHANNELS RANKING</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
