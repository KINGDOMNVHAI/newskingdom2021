<div class="vc_column td_uid_49_5bea444cb4e78_rand  wpb_column vc_column_container tdc-column td-pb-span4">
    <div class=wpb_wrapper>
        <!-- <div class="td_block_wrap td_block_social_counter td_uid_48_5cb353423d2d1_rand td-social-style6 td-social-boxed td-pb-border-top td_block_template_1">
            <div style="padding:5px;color:white;background-color:red;height:50px;">
                <p style="margin:0;">Hãy tắt adblock, click quảng cáo và để chờ 10s để ủng hộ website</p>
            </div>
        </div> -->
        <div class="td_block_wrap td_block_social_counter td_uid_48_5cb353423d2d1_rand td-social-style6 td-social-boxed td-pb-border-top td_block_template_1">
            <div class="td-block-title-wrap">
                <h4 class="block-title td-block-title"><span class="td-pulldown-size">STAY CONNECTED</span></h4>
            </div>
            <div class="td-social-list">
                <div class="td_social_type td-pb-margin-side td_social_facebook">
                    <div class="td-social-box">
                        <div class="td-sp td-sp-facebook"></div>
                        <span class="td_social_info" id="facebookAPI">{{ $facebookAPI }}</span>
                        <span class="td_social_info td_social_info_name">Fans</span>
                        <span class="td_social_button"><a href="{{FACEBOOK_URL}}" target="_blank">Like</a></span>
                    </div>
                </div>
                <div class="td_social_type td-pb-margin-side td_social_twitter">
                    <div class="td-social-box">
                        <div class="td-sp td-sp-twitter"></div><span class="td_social_info">{{$twitterAPI}}</span><span class="td_social_info td_social_info_name">Followers</span><span class="td_social_button"><a href="{{TWITTER_URL}}" target="_blank">Follow</a></span>
                    </div>
                </div>
                <div class="td_social_type td-pb-margin-side td_social_youtube">
                    <div class="td-social-box">
                        <div class="td-sp td-sp-youtube"></div>
                        <span class="td_social_info" id="youtubeAPI">{{ $youtubeAPI }}</span>
                        <span class="td_social_info td_social_info_name">Subscribers</span>
                        <span class="td_social_button"><a href="{{YOUTUBE_URL}}" target="_blank">Subscribe</a></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="td-a-rec td-a-rec-id-sidebar td_uid_51_5bea444cb5113_rand td_block_template_1">
            <!-- <span class=td-adspot-title>- Advertisement -</span>
            <div class=td-visible-desktop>
                <a href="#">
                    <img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec300.jpg" alt="" width=300 height=250 />
                </a>
            </div>

            <div class=td-visible-tablet-landscape>
                <a href="#">
                    <img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec300.jpg" alt="" width=300 height=250 />
                </a>
            </div>

            <div class=td-visible-tablet-portrait>
                <a href="#">
                    <img class=td-retina style=max-width:200px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec200.jpg" alt="" width=200 height=200 />
                </a>
            </div>

            <div class=td-visible-phone>
                <a href="#">
                    <img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec200.jpg" alt="" width=300 height=250 />
                </a>
            </div> -->
        </div>

        <style>
            .wpcf7-select{
                width: 100%;
                -webkit-appearance: textfield;
                background-color: white;
                -webkit-rtl-ordering: logical;
                cursor: text;

                font-size: 12px;
                line-height: 21px;
                color: #444;
                border: 1px solid #e1e1e1;
                max-width: 100%;
                height: 34px;
                padding: 3px 9px;
            }
        </style>

        <div class="td_block_wrap td_block_15 td_uid_52_5bea444cb5279_rand td_with_ajax_pagination td-pb-border-top td_block_template_1 td-column-1 td_block_padding">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_wrapper td_block_wrap vc_raw_html td_uid_26_5be9912cdb505_rand ">
                        <div class="td-fix-index">
                            <h4 class="block-title"><span>Search post</span></h4>
                        </div>
                    </div>

                    <form action="{{ route('list-search-post') }}" method="get" class="wpcf7-form" novalidate="novalidate">
                        <div style="display:none">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="keyword" placeholder="Keyword" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span></p>

                        <p>
                        <select name="category" class="wpcf7-select">
                            <option value="all">Tất cả chuyên mục</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id_category }}">{{$category->name_category}}</option>
                            @endforeach
                        </select>
                        </p>

                        <p>
                        <select name="date" class="wpcf7-select">
                            <option value="desc">Mới nhất</option>
                            <option value="asc">Cũ nhất</option>
                        </select>
                        </p>

                        <p><input type="submit" name="search" value="Search" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                    </form>
                </div>
            </div>
        </div>

        <div class="td_block_wrap td_block_15 td_uid_52_5bea444cb5279_rand td_with_ajax_pagination td-pb-border-top td_block_template_1 td-column-1 td_block_padding" data-td-block-uid=td_uid_52_5bea444cb5279>
            <div class=td-block-title-wrap><h4 class="block-title td-block-title"><span class=td-pulldown-size>Đề xuất</span></h4></div>
            <div id=td_uid_52_5bea444cb5279 class="td_block_inner td-column-1">
                <div class=td-cust-row>

                    @foreach($randoms as $random)
                    <div class=td-block-span12 style="margin-bottom:10px;min-height:150px;">
                        <div class="td_module_mx4 td_module_wrap td-animation-stack">
                            <div class=td-module-image>
                                <div class=td-module-thumb>
                                    <a href="{{ route('post-content', $random->url_detailpost ) }}" rel=bookmark class=td-image-wrap title="{{$random->name_detailpost}}">

                                        @if($random->img_detailpost && file_exists('upload/images/thumbnail/' . $random->img_detailpost))
                                            <img width=218 height=150 class=entry-thumb src="{{ asset('upload/images/thumbnail/' . $random->img_detailpost) }}" alt="{{$random->name_detailpost}}" title="{{$random->name_detailpost}}" />
                                        @else
                                            <img class="entry-thumb td-animation-stack-type0-2" src="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg"
                                            data-type="image_tag" data-img-url="news/wp-content/themes/011/images/no-thumb/td_600x400.jpg" width="100%">
                                        @endif

                                    </a>
                                </div>
                            </div>

                            <h3 class="entry-title td-module-title">
                                <a href="{{ route('post-content', $random->url_detailpost ) }}" rel=bookmark title="{{$random->name_detailpost}}">
                                    {{$random->name_detailpost}}
                                </a>
                            </h3>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class=clearfix></div>
        <aside class="td_block_template_1 widget widget_tag_cloud">
            <h4 class="block-title"><span>TAGS CLOUD</span></h4>
            <div class="tagcloud">
                @for($i = 0; $i < count($viewTags); $i++)
                <a href="{{ route('list-search-post', 'keyword=' . $viewTags[$i] . '&category=all&date=desc&search=Search' ) }}"
                class="tag-cloud-link tag-link-67 tag-link-position-3"
                style="font-size:16.235294117647pt" aria-label="auto (7 items)">
                    {{$viewTags[$i]}}
                </a>
                @endfor
            </div>
        </aside>
        <div class="td-a-rec td-a-rec-id-sidebar td_uid_51_5bea444cb5113_rand td_block_template_1">
            <!-- <span class=td-adspot-title>- Advertisement -</span>
            <div class=td-visible-desktop>
                <a href="#">
                    <img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec300.jpg" alt="" width=300 height=250 />
                </a>
            </div>

            <div class=td-visible-tablet-landscape>
                <a href="#">
                    <img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec300.jpg" alt="" width=300 height=250 />
                </a>
            </div>

            <div class=td-visible-tablet-portrait>
                <a href="#">
                    <img class=td-retina style=max-width:200px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec200.jpg" alt="" width=200 height=200 />
                </a>
            </div>

            <div class=td-visible-phone>
                <a href="#">
                    <img class=td-retina style=max-width:300px src="https://demo.tagdiv.com/newspaper/wp-content/uploads/2017/04/newspaper-rec200.jpg" alt="" width=300 height=250 />
                </a>
            </div> -->
        </div>
    </div>
</div>
