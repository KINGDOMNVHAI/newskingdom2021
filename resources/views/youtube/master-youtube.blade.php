<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <div class="event-schedule-area-two bg-color pad100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <div class="title-text">
                            <h2>SUBSCRIBED YOUTUBE CHANNELS RANKING</h2>
                        </div>
                        <p>
                            In ludus latine mea, eos paulo quaestio an. Meis possit ea sit. Vidisse molestie<br />
                            cum te, sea lorem instructior at.
                        </p>
                    </div>
                </div>
            </div>

            <form action="{{route('subscribed-youtube-channels-ranking')}}" method="GET" class="career-form mb-60">
                <input type="hidden" class="form-control" id="channel-category">
                <div class="row">
                    <div class="col-md-6 col-lg-3 my-3">
                        <div class="input-group position-relative">
                            <input type="text" id="keywords" name="keywords" class="form-control" placeholder="Enter Your Keywords">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 my-3">
                        <div class="select-container">
                            <select class="custom-select" name="subcribeSort">
                                @if ($searchInput['subcribeSort'] != "1" && $searchInput['subcribeSort'] != "2")
                                <option value="0" selected="">Subcribe</option>
                                @else
                                <option value="0">Subcribe</option>
                                @endif

                                @if ($searchInput['subcribeSort'] == "1")
                                <option value="1" selected="">Highest</option>
                                @else
                                <option value="1">Highest</option>
                                @endif

                                @if ($searchInput['subcribeSort'] == "2")
                                <option value="2" selected="">Lowest</option>
                                @else
                                <option value="2">Lowest</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 my-3">
                        <div class="select-container">
                            <select class="custom-select" name="dateSort">
                                <option value="0" selected="">Date</option>
                                <option value="1">Newest</option>
                                <option value="2">Oldest</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 my-3">
                        <button type="submit" class="btn btn-lg btn-block btn-primary btn-custom" id="contact-submit"> Search </button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <div class="title-text">
                            <h2>Group Channel</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 radius-img mb-5">
                    <img src="{{asset('news/kd-nvhai/avatar-vtuber-2.jpg')}}" alt="" />
                </div>
                <div class="col-lg-2 radius-img mb-5">
                    <img src="{{asset('news/kd-nvhai/avatar-hololive.jpg')}}" alt="" onclick="listAjax('hololive')" />
                </div>
                <div class="col-lg-2 radius-img mb-5">
                    <img src="{{asset('news/kd-nvhai/anonymous-avatar-pewdiepie.jpg')}}" alt="" onclick="listAjax('other')" />
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="getRequestData" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" scope="col">Date</th>
                                            <th scope="col">Thumbnail</th>
                                            <th scope="col">Information</th>
                                            <th scope="col">Subscribe</th>
                                            <th class="text-center" scope="col">Video</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($youtuberankAll as $channelAll)
                                        <tr class="inner-box">
                                            <th scope="row">
                                                <div class="event-date">
                                                    <span>{{ date("d",strtotime($channelAll->created_date_channel)) }}</span>
                                                    <p>{{ date("F",strtotime($channelAll->created_date_channel)) }}</p>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="event-img">
                                                    <img src="{{asset('/upload/images/channel/500x500/'. $channelAll->thumbnail_channel)}}"
                                                    alt="{{ $channelAll->name_channel }}" title="{{ $channelAll->name_channel }}" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="event-wrap">
                                                    <h3><a href="#">{{ $channelAll->name_channel }}</a></h3>
                                                    <div class="meta">
                                                        <div class="organizers">
                                                            <a href="#">Twitter</a>
                                                        </div>
                                                        <div class="categories">
                                                            <a href="#">Facebook</a>
                                                        </div>
                                                        <div class="time">
                                                            <span>05:35 AM - 08:00 AM 2h 25'</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="r-no">
                                                    <span>Room B3</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="primary-btn">
                                                    <a class="btn btn-primary" href="{{$channelAll->url_channel}}" target="_blank"
                                                        title="{{$channelAll->url_video_present}}">Watch Video</a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="primary-btn text-center">
                        <a href="#" class="btn btn-primary">Download Schedule</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        body {
            margin-top: 20px;
        }

        .mb-5 {
            margin-bottom: 5px;
        }

        .event-schedule-area .section-title .title-text {
            margin-bottom: 50px;
        }

        .event-schedule-area .tab-area .nav-tabs {
            border-bottom: inherit;
        }

        .event-schedule-area .tab-area .nav {
            border-bottom: inherit;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            margin-top: 80px;
        }

        .event-schedule-area .tab-area .nav-item {
            margin-bottom: 75px;
        }

        .event-schedule-area .tab-area .nav-item .nav-link {
            text-align: center;
            font-size: 22px;
            color: #333;
            font-weight: 600;
            border-radius: inherit;
            border: inherit;
            padding: 0px;
            text-transform: capitalize !important;
        }

        .event-schedule-area .tab-area .nav-item .nav-link.active {
            color: #4125dd;
            background-color: transparent;
        }

        .event-schedule-area .tab-area .tab-content .table {
            margin-bottom: 0;
            width: 80%;
        }

        .event-schedule-area .tab-area .tab-content .table thead td,
        .event-schedule-area .tab-area .tab-content .table thead th {
            border-bottom-width: 1px;
            font-size: 20px;
            font-weight: 600;
            color: #252525;
        }

        .event-schedule-area .tab-area .tab-content .table td,
        .event-schedule-area .tab-area .tab-content .table th {
            border: 1px solid #b7b7b7;
            padding-left: 30px;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th .heading,
        .event-schedule-area .tab-area .tab-content .table tbody td .heading {
            font-size: 16px;
            text-transform: capitalize;
            margin-bottom: 16px;
            font-weight: 500;
            color: #252525;
            margin-bottom: 6px;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th span,
        .event-schedule-area .tab-area .tab-content .table tbody td span {
            color: #4125dd;
            font-size: 18px;
            text-transform: uppercase;
            margin-bottom: 6px;
            display: block;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th span.date,
        .event-schedule-area .tab-area .tab-content .table tbody td span.date {
            color: #656565;
            font-size: 14px;
            font-weight: 500;
            margin-top: 15px;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th p {
            font-size: 14px;
            margin: 0;
            font-weight: normal;
        }

        .event-schedule-area-two .section-title .title-text h2 {
            margin: 0px 0 15px;
        }

        .event-schedule-area-two ul.custom-tab {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            border-bottom: 1px solid #dee2e6;
            margin-bottom: 30px;
        }

        .event-schedule-area-two ul.custom-tab li {
            margin-right: 70px;
            position: relative;
        }

        .event-schedule-area-two ul.custom-tab li a {
            color: #252525;
            font-size: 25px;
            line-height: 25px;
            font-weight: 600;
            text-transform: capitalize;
            padding: 35px 0;
            position: relative;
        }

        .event-schedule-area-two ul.custom-tab li a:hover:before {
            width: 100%;
        }

        .event-schedule-area-two ul.custom-tab li a:before {
            position: absolute;
            left: 0;
            bottom: 0;
            content: "";
            background: #4125dd;
            width: 0;
            height: 2px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two ul.custom-tab li a.active {
            color: #4125dd;
        }

        .event-schedule-area-two .primary-btn {
            margin-top: 40px;
        }

        .event-schedule-area-two .tab-content .table {
            -webkit-box-shadow: 0 1px 30px rgba(0, 0, 0, 0.1);
            box-shadow: 0 1px 30px rgba(0, 0, 0, 0.1);
            margin-bottom: 0;
        }

        .event-schedule-area-two .tab-content .table thead {
            background-color: #007bff;
            color: #fff;
            font-size: 20px;
        }

        .event-schedule-area-two .tab-content .table thead tr th {
            padding: 20px;
            border: 0;
        }

        .event-schedule-area-two .tab-content .table tbody {
            background: #fff;
        }

        .event-schedule-area-two .tab-content .table tbody tr.inner-box {
            border-bottom: 1px solid #dee2e6;
        }

        .event-schedule-area-two .tab-content .table tbody tr th {
            border: 0;
            padding: 30px 20px;
            vertical-align: middle;
        }

        .event-schedule-area-two .tab-content .table tbody tr th .event-date {
            color: #252525;
            text-align: center;
        }

        .event-schedule-area-two .tab-content .table tbody tr th .event-date span {
            font-size: 50px;
            line-height: 50px;
            font-weight: normal;
        }

        .event-schedule-area-two .tab-content .table tbody tr td {
            padding: 30px 20px;
            vertical-align: middle;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .r-no span {
            color: #252525;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap h3 a {
            font-size: 20px;
            line-height: 20px;
            color: #cf057c;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap h3 a:hover {
            color: #4125dd;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .categories {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            margin: 10px 0;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .categories a {
            color: #252525;
            font-size: 16px;
            margin-left: 10px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .categories a:before {
            content: "\f07b";
            font-family: fontawesome;
            padding-right: 5px;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .time span {
            color: #252525;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            margin: 10px 0;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers a {
            color: #4125dd;
            font-size: 16px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers a:hover {
            color: #4125dd;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers a:before {
            content: "\f007";
            font-family: fontawesome;
            padding-right: 5px;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .primary-btn {
            margin-top: 0;
            text-align: center;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-img img {
            width: 100px;
            height: 100px;
            border-radius: 8px;
        }
        .radius-img img {
            width: 100%;
            border-radius: 8px;
        }
    </style>
    <script src="{{asset('/news/kd-nvhai/jquery-1.11.3.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ajaxStart(function () {
                $("#loading").show();
            }).ajaxStop(function () {
                $("#loading").hide();
            });
        });

        function listAjax(list)
        {
            var url = 'http://localhost/newskingdom2021/public/youtube-channels-ranking-ajax/' + list;
            // var url = 'https://kingdomnvhai.info/youtube-channels-ranking-ajax/' + list;
            console.log(list);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // $.showLoading($('#getRequestData'));
            $.ajax({
                type:'POST',
                dataType: 'JSON',
                url: url,
                data:{
                    list:list,
                },
                success:function(data){
                    console.log(data);
                    console.log(data.htmlTable);
            //         $.hideLoading($('#getRequestData'));
                    $('#getRequestData').html(data.htmlTable);
            //         $('#getListSeeAll').html(data.seeAll);
                    console.log(list);
                },
                error:function(xhr, data){
                    // $.hideLoading($('#getRequestData'));
                    console.log(xhr);
                }
            });
        }
    </script>

</body>
</html>
