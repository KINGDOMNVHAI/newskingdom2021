<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{$title or ''}}</title>
    <style type="text/css">
        p{
            margin: 0;
        }
        @page {
            margin: 30px;
            /*size: A4;*/
        }
        @font-face {
            font-family: 'TimesNewRoman';
            font-style: normal;
            font-weight: normal;
            src: url({{ loadAsset('public/assets/fonts/times-news-roman/times.ttf') }}) format('truetype');
        }
        body {
            font-family: TimesNewRoman, "Times Roman", "Times New Roman",serif;
            padding: 28pt 25pt 15pt 25pt;
            font-size: 10pt;
        }
        .page-break {
            page-break-after: always;
        }
        page[size="A4"] {
            background: white;
            width: 21cm;
            height: 29.7cm;
            display: block;
            margin: 0 auto;
            box-sizing: border-box;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
            padding: 1cm 1cm 0 1cm;
        }
        .tb-header tr td p{
            padding-bottom: 24px !important;
        }
        .tb-content{
            margin-bottom: 10px;
            border-collapse: collapse !important;
        }
        .tb-header tr td{
            padding-right: 8px;
        }
        .tb-content tr.thead th{
            text-align: center;
            font-weight: normal;
            padding: 2px;
            font-weight: normal;
            background-color: #D9D9D9;
            font-size: 10pt !important;
        }
        .tb-content tbody tr td{
            padding: 2px 4px;
            font-size: 9pt !important;
            border: 1px solid black;
        }
        .text-center{
            text-align: center;
        }
        .row-bottom{
            background-color: #D9D9D9;
        }
        .row-bottom td{
            font-size: 10pt;
        }
        .form-note{
            width: 100%;
            padding-top: 2px;
            margin-top: 0;
        }
        .form-note ul{
            margin-top: 0 !important;
            padding-top: 0;
        }
        .form-note ul li:first-child{
            margin-top: 0 !important;
            padding-top: 0;
        }
    </style>
</head>
<body class="page-sizea4">
    <table class="tb-header" border=0 cellspacing=0 cellpadding=0 style="width: 100%; table-layout: fixed; word-wrap: break-word;">
        <tr>
            <td width="86" rowspan="3" style="text-align: left;">
                <img style="max-width: 74px; max-height: 60px; height: auto;" src="{{asset('news/images/Mascot-LG-KINGDOMNVHAI-square.png')}}">
            </td>
        </tr>
        <tr>
            <td style="padding-bottom: 4px;">
                <p><strong style="padding-bottom: 6px; font-size: 10pt;">KINGDOM NVHAI</strong></p>
            </td>
            <td width="190" style="vertical-align: top; font-size: 10pt; text-align: right;">
                <p>Ngày: {{$viewData->date}}<b></b></p>
            </td>
        </tr>
        <tr>
            <td style="padding-bottom: 4px;">
                <p><strong style="padding-bottom: 6px; font-size: 10pt;">Tòa nhà 6</strong></p>
            </td>
            <td width="190" style="vertical-align: top; font-size: 10pt; text-align: right;">
                <p>Số: <b></b></p>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><p style="font-size: 10pt;">123 LÊ ĐẠI HÀNH</p></td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="3" style="text-align: center; padding: 10px 8px;">
                <div style="text-transform: uppercase; font-weight: bold; font-size: 14pt;">DANH SÁCH VIDEO</div>
            </td>
        </tr>
    </table>
</body>
</html>
