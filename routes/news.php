<?php
use Illuminate\Support\Facades\Route;

Route::get('/change-locale/{locale}', 'News\HomeController@locale')->name('change-locale');

// ======================= Home Page =======================

Route::get('/', 'News\HomeController@index')->name('home');

Route::get('/about-kingdom-nvhai', 'News\HomeController@about')->name('about-kingdom-nvhai');

// ======================= Category Page =======================

Route::get('/category/{urlCat}', 'News\PostController@listPostCategory')->name('category-page');

// ======================= Post Page =======================

Route::get('/post/{urlDetailPost}', 'News\PostController@contentPost')->name('post-content');

Route::post('/content-language/{lang}/{urlPost}', 'News\PostController@contentAjax')->name('content-language');

Route::post('/comment/{idDetailpost}', 'News\PostController@commentPost')->name('comment-insert');

// ======================= Video Page =======================

// Route::get('/video', 'News\VideoController@listVideo')->name('video-page');
Route::get('/video', function () {
    return redirect(KDPLAYBACK_COM_URL);
})->name('video-page');

Route::get('/video-watch/{urlVideo}', 'News\VideoController@watchVideo')->name('video-watch');

Route::get('/channel/{idChannel}', 'News\VideoController@listVideoInChannel')->name('channel');

// ======================= Download Page =======================

Route::get('/download/{urlCat}', 'News\DownloadController@listPostDownload')->name('download-page');

// ======================= Search Page =======================

Route::get('/list-search-post', 'News\PostController@listSearch')->name('list-search-post');

// Route::post('/list-search-post', 'News\PostController@listSearch')->name('list-search-post');

// ======================= SUBSCRIBED YOUTUBE CHANNELS RANKING =======================

Route::get('/subscribed-youtube-channels-ranking', 'News\YoutubeController@index')->name('subscribed-youtube-channels-ranking');

Route::post('/youtube-channels-ranking-ajax/{list}', 'News\YoutubeController@listAjax');

Route::get('/update-youtube-ranking', 'News\YoutubeController@updateYoutubeRanking')->name('update-youtube-ranking');

// ======================= SUBSCRIBED YOUTUBE CHANNELS RANKING =======================

Route::get('/donate', 'News\DonateController@index')->name('donate');
