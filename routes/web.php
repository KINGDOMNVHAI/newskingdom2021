<?php
use Illuminate\Support\Facades\Route;

/* **************************** ADMIN PAGE **************************** */

require 'admin.php';

/* **************************** AUTH PAGE **************************** */

require 'auth.php';

/* **************************** NEWS PAGE **************************** */

require 'news.php';

/* **************************** SECRET PAGE **************************** */

require 'secret.php';

Route::get('/quang-cao', 'Landing\LandingController@index')->name('landing-index');
