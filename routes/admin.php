<?php
use Illuminate\Support\Facades\Route;

Route::get('/login', 'Auth\LoginController@redirectSoshiki()')->name('login');

// ======================= Dashboard =======================

Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');

// truyền dữ liệu vào biến $title ở tiêu đề trang <title>
// Link dẫn dùng pages.dashboard hoặc pages/dashboard
// return view('pages/dashboard', ['title' => 'Welcome to Laravel Admin - Light Bootstrap Dashboard']);

// ======================= Categories =======================

Route::get('/categories', 'Admin\CategoryController@indexList')->name('categories');

Route::get('/categories-update', 'Admin\CategoryController@indexList')->name('categories-list-update');

Route::get('/categories-delete/{idCat}', 'Admin\CategoryController@delete')->name('categories-delete');

// ======================= Posts =======================

Route::get('/posts', 'Admin\PostController@index')->name('posts-index');

Route::post('/posts-change-category-ajax', 'Admin\PostController@changeCategoryAjax')->name('posts-change-category-ajax');

Route::match(['GET', 'POST'], '/posts-insert-update/{id?}', 'Admin\PostController@postInsertUpdate')->name('posts-insert-update');

// Route::post('/posts-insert', 'Admin\PostController@insert')->name('posts-insert');

// Route::get('/posts-update/{idDetailPost}', 'Admin\PostController@indexUpdate')->name('posts-list-update');

// Route::post('/posts-updated/{idDetailPost}', 'Admin\PostController@update')->name('posts-update');

Route::get('/posts-delete', 'Admin\PostController@indexDelete')->name('posts-list-delete');

Route::post('/posts-delete', 'Admin\PostController@deleteManyPost')->name('posts-delete-many-posts');

Route::get('/posts-delete/{idDetailPost}', 'Admin\PostController@deletePost')->name('posts-delete-post');

Route::post('/posts-many-image', 'Admin\PostController@updateManyImage')->name('posts-many-image');

// ======================= Sites =======================

Route::get('/sites', 'Admin\SiteController@indexList')->name('sites');

Route::post('/sites-search', 'Admin\SiteController@search')->name('sites-search');

Route::get('/sites-insert', 'Admin\SiteController@indexInsert')->name('sites-insert');

Route::post('/sites-insert', 'Admin\SiteController@insert')->name('sites-insert');

Route::get('/sites-update/{idSite}', 'Admin\SiteController@indexUpdate')->name('sites-list-update');

Route::post('/sites-update/', 'Admin\SiteController@update')->name('sites-update');

Route::get('/sites-delete/{idSite}', 'Admin\SiteController@delete')->name('sites-delete');

// ======================= Download =======================

Route::get('/download', 'Admin\DownloadController@indexList')->name('download');

Route::post('/download-search', 'Admin\DownloadController@search')->name('download-search');

Route::get('/download-insert', 'Admin\DownloadController@indexInsert')->name('download-insert');

Route::post('/download-insert', 'Admin\DownloadController@insert')->name('download-insert');

Route::get('/download-update/{idDown}', 'Admin\DownloadController@indexUpdate')->name('download-list-update');

Route::post('/download-update/', 'Admin\DownloadController@update')->name('download-update');

Route::get('/download-delete/{idDown}', 'Admin\DownloadController@delete')->name('download-delete');

// ======================= User Profile =======================

Route::get('/user-profile', 'Admin\ProfileController@index')->name('user-profile');

Route::post('/user-profile-update', 'Admin\ProfileController@update')->name('user-profile-update');

// Route::get('/user-profile-print-pdf/{id}', 'Admin\ProfileController@printPDFProfile')->name('user-profile-print-pdf');

// Route::match(['GET', 'POST'], '/user-profile', 'Admin\ProfileController@profileUpdate')->name('user-profile');

// ======================= API Social Network =======================

Route::get('/api-social-network', 'Admin\APISocialNetworkController@index')->name('api-social-network-index');

Route::post('/api-social-network-twitter', 'Admin\APISocialNetworkController@updateTwitter')->name('api-social-network-twitter');

// ======================= Security =======================

Route::get('/security', 'Admin\SecurityController@index')->name('security');

Route::post('/security-update', 'Admin\SecurityController@update')->name('security-update');

// ======================= Administrator =======================

Route::get('/administrator', 'Admin\AdministratorController@index')->name('administrator');
