<?php

// ======================= Login, Logout, Register, Forgot =======================

Route::get('/login','Auth\LoginController@index')->name('login');

Route::post('/check-login','Auth\LoginController@login')->name('check-login');

Route::get('/register', 'Auth\RegisterController@index')->name('fe-register');

Route::post('/register-insert', 'Auth\RegisterController@create')->name('register');

Route::get('/register-google','Auth\RegisterController@registerGoogle')->name('register-google');

Route::get('/forgot-password', 'Auth\ForgotPasswordController@index')->name('forgot-password');

Route::post('/forgot-password-sendcode', 'Auth\ForgotPasswordController@sendcode')->name('forgot-password-sendcode');

Route::get('/logout', 'Admin\DashboardController@logout')->name('logout');

Route::get('/thank-you-register','Auth\RegisterController@registerThankYou')->name('thank-you-register');
