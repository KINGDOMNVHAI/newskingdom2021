<?php
namespace App\Ultis;

use Illuminate\Support\ServiceProvider;

class DateUltis extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * $date = '2009-10-22';
     * $sepparator = '-';
     *
     * @return ['2009', '10', '22']
     */
    public function getDayMonthYearOfDate($date, $sepparator) {
        return explode($sepparator, $date);
    }

}
