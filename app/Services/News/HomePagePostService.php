<?php
namespace App\Services\News;

use App\Models\posts;
use App\Models\categorypost;
use DB;
use Illuminate\Support\ServiceProvider;

class HomePagePostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * In Home Page, 4 posts of each category
     *
     * @return void
     */
    public function postsEachCategory($language)
    {
        $arrayIdCategory = [];
        $objPost = [];

        // Get array have all categories id
        $objCategory = categorypost::where('enable_cat_post', '=', 1)->get();

        $i=0;
        foreach ($objCategory as $category)
        {
            $arrayIdCategory[$i] = $category->id_cat_post;
            $i++;
        }

        switch($language)
        {
            case "en":
                // Each category have 1 posts
                for ($j=0; $j < count($arrayIdCategory); $j++)
                {
                    $objPost[$j] = categorypost::join('posts', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                        ->select(
                            'categorypost.name_en_cat_post as name_category',
                            'categorypost.url_cat_post as url_cat_post',
                            'posts.name_en_post as name_post',
                            'posts.url_post as url_post',
                            'posts.present_en_post as present_post',
                            DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                            'posts.thumbnail_post as img_post'
                        )
                        ->where('categorypost.enable_cat_post', '=', true)
                        ->where('categorypost.id_cat_post', $arrayIdCategory[$j])
                        ->where('posts.enable', '=', true)
                        ->orderBy('posts.date_post', 'DESC')
                        ->take(HOME_POSTS)
                        ->get();
                }
            break;

            default:
                // Each category have 1 posts
                for ($j=0; $j < count($arrayIdCategory); $j++)
                {
                    $objPost[$j] = categorypost::join('posts', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                        ->select(
                            'categorypost.name_vi_cat_post as name_category',
                            'categorypost.url_cat_post as url_cat_post',
                            'posts.name_vi_post as name_post',
                            'posts.url_post as url_post',
                            'posts.present_vi_post as present_post',
                            DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                            'posts.thumbnail_post as img_post'
                        )
                        ->where('categorypost.enable_cat_post', '=', true)
                        ->where('categorypost.id_cat_post', $arrayIdCategory[$j])
                        ->where('posts.enable', '=', true)
                        ->orderBy('posts.date_post', 'DESC')
                        ->take(HOME_POSTS)
                        ->get();
                }
        }

        return $objPost;

    }

    public function showAllCategories()
    {
        return categorypost::where('enable_cat_post', '=', 1)->get();
    }

    public function postsPerCategory($language)
    {
        $selectQuery = "c.id_cat_post, c.url_cat_post AS url_cat_post "
        . ", p.url_post"
        . ', DATE_FORMAT(p.date_post, "%d-%m-%Y") AS date_post '
        . ", p.thumbnail_post AS thumbnail_post ";

        if ($language == "en") {
            $selectQuery = $selectQuery . ", c.name_en_cat_post AS name_cat_post, p.present_en_post AS present_post, p.name_en_post as name_post";
        } else {
            $selectQuery = $selectQuery . ", c.name_vi_cat_post AS name_cat_post, p.present_vi_post AS present_post, p.name_vi_post as name_post";
        }

        $query1 = DB::table('posts AS p')
        ->join('categorypost AS c', 'c.id_cat_post', '=', 'p.id_cat_post')
        ->selectRaw($selectQuery)
        ->where('c.enable_cat_post', '=', true)
        ->where('c.id_cat_post', WEBSITE_POST)
        ->where('p.enable', '=', true);
        if ($language == "en") {
            $query1 = $query1->where('p.name_en_post', '!=', null)->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        } else {
            $query1 = $query1->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        }

        $query2 = DB::table('posts AS p')
        ->join('categorypost AS c', 'c.id_cat_post', '=', 'p.id_cat_post')
        ->selectRaw($selectQuery)
        ->where('c.enable_cat_post', '=', true)
        ->where('c.id_cat_post', GAME_POST)
        ->where('p.enable', '=', true);
        if ($language == "en") {
            $query2 = $query2->where('p.name_en_post', '!=', null)->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        } else {
            $query2 = $query2->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        }

        $query3 = DB::table('posts AS p')
        ->join('categorypost AS c', 'c.id_cat_post', '=', 'p.id_cat_post')
        ->selectRaw($selectQuery)
        ->where('c.enable_cat_post', '=', true)
        ->where('c.id_cat_post', ANIME_POST)
        ->where('p.enable', '=', true);
        if ($language == "en") {
            $query3 = $query3->where('p.name_en_post', '!=', null)->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        } else {
            $query3 = $query3->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        }

        $query4 = DB::table('posts AS p')
        ->join('categorypost AS c', 'c.id_cat_post', '=', 'p.id_cat_post')
        ->selectRaw($selectQuery)
        ->where('c.enable_cat_post', '=', true)
        ->where('c.id_cat_post', THU_THUAT_IT_POST)
        ->where('p.enable', '=', true);
        if ($language == "en") {
            $query4 = $query4->where('p.name_en_post', '!=', null)->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        } else {
            $query4 = $query4->orderBy('p.date_post', 'DESC')->take(HOME_POSTS);
        }


        $query5 = DB::table('posts AS p')
        ->join('categorypost AS c', 'c.id_cat_post', '=', 'p.id_cat_post')
        ->selectRaw($selectQuery)
        ->where('c.enable_cat_post', '=', true)
        ->where('c.id_cat_post', GAME_GIRL_POST)
        ->where('p.enable', '=', true);
        if ($language == "en") {
            $query5 = $query5->where('p.name_en_post', '!=', null)->orderBy('p.date_post', 'DESC')->take(HOME_POSTS)
            ->union($query2)
            ->union($query1)
            ->union($query3)
            ->union($query4)
            ->get();
        } else {
            $query5 = $query5->orderBy('p.date_post', 'DESC')->take(HOME_POSTS)
            ->union($query2)
            ->union($query1)
            ->union($query3)
            ->union($query4)
            ->get();
        }

        return $query5;
    }

    // SQL lấy 5 bài viết mỗi thể loại
    // (
    //     SELECT * FROM nvhai.posts d
    //     JOIN nvhai.category c ON c.id_cat_post = d.id_cat_posts
    //     WHERE c.id_cat_post = 2
    //     ORDER BY d.date_posts DESC
    //     LIMIT 0,5
    //     )
    //     UNION
    //     (
    //     SELECT * FROM nvhai.posts d
    //     JOIN nvhai.category c ON c.id_cat_post = d.id_cat_posts
    //     WHERE c.id_cat_post = 3
    //     ORDER BY d.date_posts DESC
    //     LIMIT 0,5
    //     )
    //     UNION
    //     (
    //     SELECT * FROM nvhai.posts d
    //     JOIN nvhai.category c ON c.id_cat_post = d.id_cat_posts
    //     WHERE c.id_cat_post = 4
    //     ORDER BY d.date_posts DESC
    //     LIMIT 0,5
    //     )
    //     UNION
    //     (
    //     SELECT * FROM nvhai.posts d
    //     JOIN nvhai.category c ON c.id_cat_post = d.id_cat_posts
    //     WHERE c.id_cat_post = 5
    //     ORDER BY d.date_posts DESC
    //     LIMIT 0,5
    //     )
    //     ;

}
