<?php
namespace App\Services\News;

use App\Model\category;
use App\Model\detailpost;
use DB;
use Illuminate\Support\ServiceProvider;

class ListSearchPostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function run($request, $language)
    {
        $response = [];

        $keyword = '%' . $request->keyword . '%';

        // Create query
        // ilike only for Postgre, not support for phpmyadmin

        switch ($language)
        {
            case "en":
                $query = posts::where('name_en_detailpost', 'like', $keyword);
            break;
            default:
                $query = DB::table('detailpost')
                ->select(
                    'name_vi_detailpost AS name_detailpost',
                    'present_vi_detailpost AS present_detailpost',
                    'url_detailpost',
                    'img_detailpost',
                    DB::raw("DATE_FORMAT(date_detailpost,'%d-%m-%Y') as date_detailpost"),
                    'id_cat_detailpost'
                    )
                ->where('name_vi_detailpost', 'like', $keyword);
            break;
        }

        if ($request->category != 'all')
        {
            $query = $query->where('id_cat_detailpost', $request->category);
        }

        if ($request->date == 'asc')
        {
            $query = $query->orderBy('date_detailpost', 'desc');
        }
        else if ($request->date == 'desc')
        {
            $query = $query->orderBy('date_detailpost', 'asc');
        }

        // Get response
        $response = $query->paginate(ITEM_PER_PAGE);
        return $response;
    }

    public function tagsList()
    {
        $arrTags = [
            'Pewdiepie', 'T-Series', 'Kizuna Ai', 'Review',
            'Virtual Youtuber', 'Hololive',
        ];

        return $arrTags;
    }
}
