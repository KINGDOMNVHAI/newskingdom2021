<?php
namespace App\Services\News;

use App\Models\apisocial;
use App\Models\channels;
use DB;
use Illuminate\Support\ServiceProvider;

class YoutuberankService extends ServiceProvider
{
    /**
     * construct khởi tạo ra mảng chứa channelID
     * Cách thêm channel mới: thêm trong YoutuberankTableSeeder và thêm channelID vào mảng này
     *
     * @return void
     */
    public function __construct()
    {
        $this->listChannel = [
            'UC-lHJZR3Gqxm24_Vd_AJ5Yw', // Pewdiepie
            'UCmo55h1NKPRnxiU21b4AHFg', // Niji
            'UC4YaOt1yT-ZeyB0OmxHgolA', // A.I Channel
            'UCX4N3DioqqrugFeilxTkSIw', // BHGaming
            'UCxUL0zS-XiU36bkUsr5dWbg', // KINGDOM NVHAI
            'UCdOWyp25T0HDtjpnV2LpIyw', // EpicSkillshot - LoL VOD Library
            'UCKv8Rrrdc9oxLJmdHItafLA', // ManlyBadassHero
            'UCUFNaNGi3lnBPq4gtdfJCAA', // INFINI HD 4K
            'UCXF4WjTCUQSmGapnNEZzbYw', // VETV7 ESPORTS
            'UC8dwPSQ5DOJGtwhmcNhNjSw', // 講談社ヤンマガch
            'UCBS_hANfRVQal3OpQPMACcw', // MAGES, The Wandering Mage
            'UCXxxdMSiRrPWBnMKu6Q9DgQ', // 최하급번역기
            'UCrYpo1jB5xh6b_MgsZ4rqjQ', // Drako Gaspar
            'UCdV9tn79v3ecSDpC1AjVKaw', // Phạm Huy Hoàng
            'UCeZje_7vr6CPK9vPQDfV3WA', // Syrex
            'UCy5lOmEQoivK5XK7QCaRKug', // Yomemi
            'UCfLsVuE0A_hUQyeCDNQelxA', // NEEKO
            'UCMYtONm441rBogWK_xPH9HA', // Mirai Akari Project
            'UC6oDys1BGgBsIC3WhG1BovQ', // Shizuka Rin Official
            'UCUZNK80DemBN3kyxusDLwrA', // The Soul of Wind
            'UC1opHUrw8rvnsadT-iGp7Cg', // Aqua Ch. 湊あくあ
            'UCh_ugKacslKhsGGdXP0cRRA', // Naomi "SexyCyborg" Wu
            'UCvqRdlKsE5Q8mf8YXbdIJLw', // LOL Esport
            'UCCvt0Jc0ghFegppbyRdMPTg', // Ririchiyo咬人猫
            'UCW0GGjRL0zDhTLg18iHstng', // Ask Me Why - Hiểu Biết Hơn Mỗi Ngày
            'UCI7ktPB6toqucpkkCiolwLg', // Pan Piano
            'UCQYADFw7xEJ9oZSM5ZbqyBw', // Kaguya Luna Offical
            'UC5FqvcatEZ2pjXvfhq4eMoQ', // Rimia Kodachi
            'UCU0Z5716wQX8IIZVna7G0zg', // lyger VTuber translations
            'UCi3RiY2dus-oeBRvxRBNJgg', // birdkun21
            'UCCZr6pq56yWtgBYihhy31-w', // JudeKey
            'UCCebk1_w5oiMUTRxdNJq0sA', // ここあMusic
            'UCwRKt_raV3N5KZgxcFyC1vw', // 燦鳥ノム - SUNTORY NOMU -
            'UC_6dbaltxheAEJiy9aZDRVw', // Miru Shion
            'UCfa4LOAJb8Yet6QIWOY0BUg', // Kamome Subs
            'UCPZio2d377EtYOqzCIqpOsQ', // Yumemi Translations
            'UCnTIjR2biTlYBNxn_H2jXfw', // HoloLive etc Cuts
            'UCcIsxujzLRO5qY5f9buahCQ', // Nobita from Japan
            'UCc4ACSF6Qk6JFt4aQtyCu1Q', // ARINKO LOG - Rural Life & DIY Japan
            'UCsUnAKkMgO3puOVu9fv9Tkw', // 温泉大好きゆーちゃんねる Yuu-chan
            'UCqvaqxGePQWvXMAi9-524uA', // hot spring girl
            'UCcerE0mibcYCgdkQPByO89w', // LunaxHolic
            'UCakYsyF6MafHuTBcXb2x0xw', // まろちゃんねる
            'UCGCZAYq5Xxojl_tSXcVJhiQ', // ANN
            'UCcaYkWDw6A4_8awJgKGWeGA', // Shania Yan

            'UCu2DMOGLeR_DSStCyeQpi5Q', // Tanigox
            'UCJFZiqLMntJufDCHc6bQixg', // Hololive

            // 0 Gen
            'UCp6993wxpyDPHUpavwDFqgg', // Tokino Sora
            'UCDqI2jOz0weumE8s7paEk6g', // Roboco
            'UC5CwaMl1eIgY8h02uZw7u8A', // Suisei
            'UC-hM6YJuNYVAmUWxeIr9FeA', // Miko
            'UC0TXe_LYZ4scaW2XMyi5_kw', // AZKi

            // 1 Gen
            'UCQ0UDLQCjY0rmuxCDE38FGg', // Matsuri
            'UCdn5BQ06XqgXoAxIhbqw5Rg', // Fubuki
            'UCFTLzh12_nrtzqBPsTCqenA', // Aki
            'UC1CfXB_kRs3C-zaeTG3oGyg', // Haato
            'UCD8HOxPs4Xvsm8H0ZxXGiBw', // Mel

            // 2 Gen
            'UCp-5t9SrOQwXMU7iIjQfARg', // Mio
            'UC1opHUrw8rvnsadT-iGp7Cg', // Aqua
            'UCXTpFs_3PqI41qX2d9tL2Rw', // Shion
            'UC1suqwovbL1kzsoaZgFZLKg', // Choco
            'UCvzGlP9oQwU--Y0r9id_jnA', // Subaru
            // Ayame

            // 3 Gen
            'UCdyqAaZDKHXg4Ahi7VENThQ', // Noel
            'UCCzUftO8KOVkV4wQG1vkUvg', // Marine
            'UCl_gCybOJRIgOXw6Qb4qJzQ', // Rushia
            'UCvInZx9h3jC2JzsIzoOebWg', // Flare
            'UC1DCedRgGHBdm81E1llLhOQ', // Pekora

            // 4 Gen
            'UCS9uQI-jC3DE0L4IpXyvr6w', // Coco
            'UCZlDXzGoo7d44bwdNObFacg', // Kanata
            // Watame
            'UC1uv2Oq6kNxgATlCiez59hw', // Towa
            'UCa9Y57gfeY0Zro_noHRVrnw', // Luna

            'UCvaTdHTWBGv3MKj3KVqJVCw', // Okayu
            'UChAnqc_AY5_I3Px5dig3X1Q', // Korone

            // 5 Gen
            'UCK9V2B22uJYu3N7eR_BT9QA', // Polka
            'UCUKD-uaobj9jiqB-VXt71mA', // Botan
            'UCFKOVgVbGmX65RxO3EtH3iw', // Lamy
            'UCAWSyEs_Io8MtpY3m-zqILA', // Nene
            'UCgZuwn-O7Szh9cAgHqJ6vjw', // Aloe

            // X Gen
            'UCENwRMx5Yh42zWpzURebzTw', // Laplus
            'UCIBY1ollUsauvVi4hW4cumw', // Chloe
            'UC_vMYWcDjmfdpH6r4TTn1MQ', // Iroha
            'UC6eWCld0KwmyHFbAqK3V-Rw', // Koyori
            'UCs9_O1tRPMQTHQ-N_L6FU2g', // Koyori

            // ID Gen 1
            'UCOyYb1c43VlX9rc_lT6NKQw', // Risu
            'UCP0BspO_AMEe3aQqqpo89Dg', // Moona
            'UCAoy6rzhSf4ydcYjJw3WoVg', // Iofifteen

            // ID Gen 2
            'UCYz_5n-uDuChHtLo7My1HnQ', // Kureiji Ollie Ch. hololive-ID
            'UC727SQYUvx5pDDGQpTICNWg', // Anya Melfissa Ch. hololive-ID
            'UChgTyjG-pdNvxxhdsXfHQ5Q', // Pavolia Reine Ch. hololive-ID

            // EN Gen 1
            'UCL_qhgtOy0dy1Agp8vkySQg', // Mori Calliope Ch. hololive-EN
            'UCHsx4Hqa-1ORjQTh9TYDhww', // Takanashi Kiara Ch. hololive-EN
            'UCoSrY_IQQVpmIRZ9Xf-y93g', // Gawr Gura Ch. hololive-EN
            'UCMwGHR0BTZuLsmjY_NT5Pwg', // Ninomae Ina'nis Ch. hololive-EN
            'UCyl1z3jo3XHR1riLFKG5UAg', // Watson Amelia Ch. hololive-EN

            'UC8rcEBzJSleTkf_-agPM20g', // IRyS Ch. hololive-EN

            // EN Gen 2
            'UCmbs8T6MWqUHP1tIQvSgKrg', // Ouro Kronii Ch. hololive-EN
            'UCsUj0dszADCGbF3gNrQEuSQ', // Tsukumo Sana Ch. hololive-EN
            'UCO_aKKYxn4tvrqPjcTzZ6EQ', // Ceres Fauna Ch. hololive-EN
            'UCgmPnx-EEeOrZSg5Tiw7ZRQ', // Hakos Baelz Ch. hololive-EN
            'UC3n5uGu18FoCy23ggWWp8tA', // Nanashi Mumei Ch. hololive-EN

            // VOM Projects
            'UCajhBT4nMrg3DLS-bLL2RCg', // Pikamee
        ];
    }

    /*
     * Hàm để lấy data từ API và chuyển thành dạng mảng.
     * final để chặn ghi đè.
     * Tất cả phương thức trong class này đều phải dùng final
    */
    final private function getAPIarray($urlAPI)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlAPI,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true); // Mảng chứa API

        return $response;
    }

    /**
     * Update newest result
     *
     * @return void
     */
    final public function updateYoutuberankNewestResult()
    {
        $listChannel = $this->listChannel;

        foreach($listChannel as $channel)
        {
            // Nối chuỗi với link   https://www.googleapis.com/youtube/v3/channels?id=UC-lHJZR3Gqxm24_Vd_AJ5Yw&key=AIzaSyBYvogrKc3YK4xsXB0NCh6g7X-fnw2JJ4I&part=snippet,contentDetails,statistics,status

            $urlAPI = 'https://www.googleapis.com/youtube/v3/channels?id=' . $channel . '&key=' . YOUTUBE_API_KEY . '&part=snippet,contentDetails,statistics,status';

            $response = $this->getAPIarray($urlAPI); // Mảng chứa API

            // Case 1: accessCode còn hạn nên lấy được subscribe
            if (isset($response["items"][0]["statistics"]["subscriberCount"]))
            {
                $subcribeCount = $response["items"][0]["statistics"]["subscriberCount"];
                $idChannel = $response["items"][0]["id"];

                $query = channels::where('id_channel', '=', $idChannel)
                    ->update([
                        'subscribe' => $subcribeCount,
                    ]);
            }
            // Case 2: accessCode hết hạn nên lấy subscribe từ data
            else
            {
                $query = channels::where('id_channel', $channel)->first();
                $subcribeCount = $query["subscribe"];
            }
        }
    }

    /**
     * List Youtube Channel All
     *
     * @return void
     */
    final public function listYoutubeChannelAll()
    {
        $arrayFanCount = channels::orderBy('subscribe', 'DESC')
            ->limit(50)->get();
        return $arrayFanCount;
    }

    /**
     * List Youtube Channel Other
     *
     * @return void
     */
    public function listYoutubeChannelOther()
    {
        $arrayInfo = channels::where('enable_channel', '=', 1)
        ->where('virtual_youtuber', '=', 0)
        ->where('visual_novel', '=', 0)
        ->where('hololive', '=', 0)
        ->orderBy('subscribe', 'DESC')
        ->limit(50)
        ->get();

        return $arrayInfo;
    }

    /**
     * List Youtube Channel Favorite
     *
     * @return void
     */
    public function listYoutubeChannelFavorite()
    {
        $arrayInfo = channels::where('favorite', '=', 1)
            ->selectRaw(DB::raw('
                channels.id_channel
                , channels.name_channel
                , channels.url_channel
                , channels.url_video_present
                , channels.description_channel
                , channels.subscribe
                , channels.enable_channel
                , channels.thumbnail_channel
                , channels.created_date_channel
                , channels.virtual_youtuber
                , channels.visual_novel
                , channels.hololive
                , channels.facebook_channel
                , channels.twitter_channel
                , channels.instagram_channel
                , channels.patreon_channel
            '))
            ->orderBy('subscribe', 'DESC')
            ->limit(50)
            ->get();

        return $arrayInfo;
    }

    /**
     * List Youtube Channel Virtual Youtuber
     *
     * @return void
     */
    public function listYoutubeChannelVR()
    {
        $arrayInfo = channels::where('virtual_youtuber', '=', 1)
            ->orWhere('hololive', '=', 1)
            ->orderBy('subscribe', 'DESC')
            ->limit(50)
            ->get();
        return $arrayInfo;
    }

    /**
     * List Youtube Channel Visual Novel
     *
     * @return void
     */
    public function listYoutubeChannelVN()
    {
        $arrayInfo = channels::where('visual_novel', '=', 1)
            ->orderBy('subscribe', 'DESC')
            ->get();
        return $arrayInfo;
    }

    /**
     * List Youtube Channel Hololive
     *
     * @return void
     */
    public function listYoutubeChannelHololive()
    {
        $arrayInfo = channels::where('hololive', '=', 1)
            ->orderBy('subscribe', 'DESC')
            ->get();
        return $arrayInfo;
    }

}
