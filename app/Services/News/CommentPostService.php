<?php
namespace App\Services\News;

use App\Models\comment;
use App\Models\detailpost;
use DB;
use Illuminate\Support\ServiceProvider;

class CommentPostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function commentOfPost($urlDetailPost)
    {
        $comment = comment::join('detailpost', 'comment.id_detailpost_comment', 'detailpost.id_detailpost')
            ->select(
                'detailpost.id_detailpost',
                'detailpost.url_detailpost',

                'comment.*'
            )
            ->where('detailpost.url_detailpost', $urlDetailPost)
            ->where('comment.enable', true)
            ->get();

        return $comment;
    }

    public function commentInsert($idDetailpost, $datas)
    {
        $comment = comment::create([
            'name_comment'          => $datas['name_comment'],
            'content_comment'       => $datas['content_comment'],
            'img_comment'           => $datas['img_comment'],
            'id_user'               => $datas['id_user'],
            'id_detailpost_comment' => $idDetailpost,
            'enable'                => true,
        ]);

        return $comment;
    }
}
