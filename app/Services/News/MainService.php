<?php
namespace App\Services\News;

use App\Models\detailpost;
use Illuminate\Support\ServiceProvider;

class MainService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function recent_post()
    {
        // RECENT POST
        $breaks = posts::where('enable', '=', '0')
            ->orderBy('date_detailpost', 'desc')
            ->take(4)
            ->get();

        return $breaks;
    }

    public function most_view()
    {
        // RECENT POST
        $view = posts::where('enable', '=', '0')
            ->orderBy('date_detailpost', 'desc')
            ->take(4)
            ->get();

        return $view;
    }
}
