<?php
namespace App\Services\News;

use Illuminate\Support\ServiceProvider;

class AdsService extends ServiceProvider
{
    public function __construct()
    {
        $this->listNumber = [1,2,3];
    }

    public function randomAds728()
    {
        $listChannel = $this->listChannel;
        $number = array_rand($listChannel);
        switch ($number) {
            case 1:
                $result = AD_728_1;
                break;
            case 2:
                $result = AD_728_2;
                break;
            case 3:
                $result = AD_728_3;
                break;
            default:
                $result = AD_728_DEFAULT;
        }
        return $result;
    }

    public function randomAds300()
    {
        $listChannel = $this->listChannel;
        $number = array_rand($listChannel);
        switch ($number) {
            case 1:
                $result = AD_300_1;
                break;
            case 2:
                $result = AD_300_2;
                break;
            case 3:
                $result = AD_300_3;
                break;
            default:
                $result = AD_300_DEFAULT;
        }
        return $result;
    }
}
