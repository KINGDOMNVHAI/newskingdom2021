<?php
namespace App\Services\Auth;

use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class DashboardService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     */
    public function analyticsPost()
    {
        $query =  posts::select(
                'categorypost.id_cat_post',
                'categorypost.name_vi_cat_post',
                'categorypost.url_cat_post' ,
                DB::raw('count(posts.id_cat_post) as count_post'),
                DB::raw('sum(posts.views) as view_post')
            )
            ->leftJoin('categorypost','posts.id_cat_post','categorypost.id_cat_post')
            ->groupBy('categorypost.id_cat_post', 'categorypost.name_vi_cat_post', 'categorypost.url_cat_post', 'posts.id_cat_post')
            ->orderBy('posts.id_cat_post', 'ASC')
            ->get();

        return $query;
    }
}
