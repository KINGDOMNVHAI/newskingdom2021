<?php
namespace App\Services\Auth;

use App\Models\users;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }



    public function registerGoogle($username, $password, $email)
    {
        $query = users::insert([
            // 'id'        => $request->name_detailpost,
            'lastname'  => $username,
            'firstname' => $username,
            'username'  => $username,
            'password'  => Hash::make($password),
            'email'     => $email,  // Lấy tên file
            'type'      => 'member',
            // 'city'      => Auth::user()->signature,
            // 'address'   => Auth::user()->username,
            // 'company'   => $enable,
            // 'facebook'  => $popular,
            // 'twitter'   => $update,
            // 'aboutme'   => 0,
            // 'signature' => 0,
            // 'avatar'    => 0,
            // 'banner'    => 0,
            // 'medal'     => 0,
        ]);

        return $query;
    }
}
