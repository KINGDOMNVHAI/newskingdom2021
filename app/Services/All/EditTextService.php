<?php
namespace App\Services\All;

use DB;
use Illuminate\Support\ServiceProvider;

class EditTextService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Comment text dùng để edit text trong comment
     *
     * @return void
     */
    public function commentText($text)
    {
        // B1: Kiểm tra text
        // Nếu text có đoạn .video .org .info, replace đoạn text đó thành <a>

        // B2: Tạo nút Nhập Text Thường hoặc HTML
        // Nếu nhập text thường, dùng rule bước 1.
        // Nếu nhập HTML, không cần rule


        return $text;
    }

    public function checkRuleText($text)
    {
        // Hàm kiểm tra những từ nhạy cảm như fuck
        // Nếu có những từ đó, replace thành dấu sao ****

        return $text;
    }

}
