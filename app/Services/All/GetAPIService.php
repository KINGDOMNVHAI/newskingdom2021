<?php
namespace App\Services\All;

use App\Models\channels;
use App\Models\channelsocial;
use DB;
use Illuminate\Support\ServiceProvider;

class GetAPIService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /*
     * Hàm để lấy data từ API và chuyển thành dạng mảng.
     * final để chặn ghi đè.
     * Tất cả phương thức trong class này đều phải dùng final
    */
    final private function getAPIarray($urlAPI)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlAPI,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true); // Mảng chứa API

        return $response;
    }

    /**
     * updateNewestResult
     *
     * Cập nhật kết quả mới nhất từ API Facebook và Youtube
     *
     * Sẽ có 2 trường hợp
     * Case 1: accessCode còn hạn, chạy curl để lấy fan_count mới nhất rồi nhập vào data.
     * Case 2: accessCode hết hạn, lấy fan_count cũ đã lưu trong data.
     * Key của Facebook chỉ sống được 1 tiếng
     */
    final public function updateFacebookNewestResult()
    {
        /* ======= Facebook ======= */
        $urlFacebook = FACEBOOK_API_URL;

        // API có thể thay đổi
        // Trang tìm key mới https://developers.facebook.com/tools/explorer/?method=GET&path=me%3Ffields%3Did%2Cname%2Cfan_count&version=v3.3

        // curl -i -X GET \
        //   "https://graph.facebook.com/{your-user-id}
        //   ?fields=feed.limit(3)
        //   &access_token={your-access-token}"

        // Xem tại trang https://developers.facebook.com/docs/graph-api/using-graph-api/

        $accessCode = 'EAAEYauSI6lUBADqh1AaeV1HiZCa5yISEyDpOXsN1ZCGhvOv1mSItvf5uBCNLoyuBu3XHls0rIgbDPCRZC0voZAieive44JpoD9JVeW9je97UsEPuiixGNcBNNicD3ZB2jmB3XeMEf7xRsGSrdO1A5P91DERWnjZCY8OCrODaCKogMkSbO4yvNV9A7Wv8PoIcRdzZCxRafI74AZDZD';
        $urlAPI = $urlFacebook . $accessCode;

        $response = $this->getAPIarray($urlAPI);

        // Case 1: accessCode còn hạn nên lấy được fan_count
        if (isset($response["fan_count"]))
        {
            $fanCount = $response["fan_count"];
            // channelsocial::where('name_channel_social', 'facebook')->first();

            $query = channelsocial::where('name_channel_social', 'facebook')
                ->update([
                    'fan_count' => $fanCount,
                ]);
        }

        // Case 2: accessCode hết hạn nên lấy fan_count từ data
        // else
        // {
        //     $query = channelsocial::where('name_channel_social', 'facebook')->first();
        //     $fanCount = $query["fan_count"];
        // }

        // return $fanCount;

        /* ======= Youtube ======= */

        // API có thể thay đổi
        // Nếu key hết hạn, vào Google API https://console.cloud.google.com/apis/api/youtube.googleapis.com/credentials?project=kingdomnvhai cập nhật mới Youtube API

        $urlAPI = 'https://www.googleapis.com/youtube/v3/channels?id=UCxUL0zS-XiU36bkUsr5dWbg&key=' . YOUTUBE_API_KEY . '&part=snippet,contentDetails,statistics,status';

        $response = $this->getAPIarray($urlAPI);

        // Case 1: accessCode còn hạn nên lấy được fan_count
        if (isset($response["items"][0]["statistics"]["subscriberCount"]))
        {
            $subcribeCount = $response["items"][0]["statistics"]["subscriberCount"];

            // channelsocial::where('name_channel_social', 'youtube')->first();

            $query = channelsocial::where('name_channel_social', 'youtube')
                ->update([
                    'fan_count' => $subcribeCount,
                ]);
        }
        // Case 2: accessCode hết hạn nên lấy fan_count từ data
        // else
        // {
        //     $query = channelsocial::where('name_channel_social', 'youtube')->first();
        //     $subcribeCount = $query["fan_count"];
        // }

        // return $subcribeCount;
    }


    /**
     * getAPIFacebook
     *
     * Lấy fancount từ data
     */
    final public function getAPIFacebook()
    {
        $query = channelsocial::where('name_channel_social', 'facebook')->first();
        $fanCount = $query["fan_count"];

        return $fanCount;
    }

    /**
     * getAPIYoutube
     *
     * Tương tự như Facebook nhưng key của Youtube thời gian sống lâu hơn
     */
    final public function getAPIYoutube()
    {
        $query = channels::where('id_channel', CHANNEL_KINGDOM_NVHAI)
            ->select('subscribe as fan_count')->first();
        return $query['fan_count'];
    }

    /**
     * getAPITwitter
     *
     * Download file json rồi lấy data từ file đó.
     * Nếu đã có file cũ, xóa file cũ rồi download file mới.
     */
    final public function getAPITwitter()
    {
        // Link download file json: https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=KNvhai

        $filename = '/upload/twitterapi/json.json';

        // Check file json.json exist
        if (file_exists($filename))
        {
            // Đưa file vào upload/js/twitterapi. Nếu đã tồn tại, chép đè hoặc xóa file cũ. Sau đó lấy nội dung file json

            $json = file_get_contents($filename); // Get Content
            $file = str_replace('"', '', $json); // Replace

            $array = explode(',',$file); // Convert string to array

            $arrayFollowerCount = explode(':', $array[5]); // Convert string to array
            $followerCount = $arrayFollowerCount[1];

            // Upload số follower count mới nhất lên data
            $query = channelsocial::where('name_channel_social', 'twitter')
                ->update([
                    'fan_count' => $followerCount,
                ]);
        }
        else
        {
            // Nếu file không tồn tại, lấy dữ liệu từ data.
            $query = channelsocial::where('name_channel_social', 'twitter')->first();
            $followerCount = $query["fan_count"];
        }

        return $followerCount;
    }
}
