<?php
namespace App\Services\All;

use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class PostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get newest post
     */
    public function getListNewestPost($limit,$idCat,$convertToArray,$language)
    {
        switch ($language)
        {
            case "en":
                $new = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.id_post',
                    'posts.id_cat_post',
                    'posts.name_en_post as name_post',
                    'posts.present_en_post as present_post',
                    'posts.url_post',
                    DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                    'posts.thumbnail_post',

                    'categorypost.name_en_cat_post'
                )
                ->where('posts.enable', ENABLE)
                ->where('present_en_post', '!=', '');
            break;

            default:
                $new = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.id_post',
                    'posts.id_cat_post',
                    'posts.name_vi_post as name_post',
                    'posts.present_vi_post as present_post',
                    'posts.url_post',
                    DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                    'posts.thumbnail_post',

                    'categorypost.name_vi_cat_post'
                )
                ->where('posts.enable', ENABLE);
        }

        if ($idCat != null)
        {
            $new = $new->where('posts.id_cat_post', '=', $idCat);
        }

        $new = $new->latest('posts.date_post')->take($limit)->get();

        // Convert object to array
        if ($convertToArray === true)
        {
            $arrNewest = [];

            $i = 0;
            foreach ($new as $viewNewest)
            {
                $arrNewest[$i]['id_post'] = $viewNewest->id_post;
                $arrNewest[$i]['id_cat_post'] = $viewNewest->id_cat_post;
                $arrNewest[$i]['name_post'] = $viewNewest->name_post;
                $arrNewest[$i]['present_post'] = $viewNewest->present_post;
                $arrNewest[$i]['url_post'] = $viewNewest->url_post;
                $arrNewest[$i]['date_post'] = $viewNewest->date_post;
                $arrNewest[$i]['thumbnail_post'] = $viewNewest->thumbnail_post;
                $arrNewest[$i]['name_cat_post'] = $viewNewest->name_cat_post;
                $i++;
            }
            return $arrNewest;
        }
        else
        {
            return $new;
        }
    }

    /**
     * Get updated post
     */
    public function getListUpdatedPost($limit,$idCat,$language)
    {
        switch ($language)
        {
            case "en":
                $query = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.name_en_post as name_post',
                    'posts.url_post',
                    'posts.present_en_post as present_post',
                    'posts.thumbnail_post',
                    'posts.date_post',
                    'posts.id_cat_post',
                    'posts.update',

                    'categorypost.name_en_cat_post'
                )
                ->where('posts.enable', ENABLE)
                ->where('present_en_post', '!=', '');
            break;

            default:
                $query = posts::join('categorypost', 'categorypost.id_cat_post', '=', 'posts.id_cat_post')
                ->select(
                    'posts.name_vi_post as name_post',
                    'posts.url_post',
                    'posts.present_vi_post as present_post',
                    'posts.thumbnail_post',
                    'posts.date_post',
                    'posts.id_cat_post',
                    'posts.update',

                    'categorypost.name_vi_cat_post'
                )
                ->where('posts.enable', ENABLE);
        }

        if ($idCat != null)
        {
            $query = $query->where('posts.id_cat_post', '=', $idCat);
        }

        $query = $query->where('posts.update', '=', UPDATE_POST)
            ->orderBy('posts.updated_at', 'desc')
            ->take($limit)
            ->get();

        return $query;
    }

    /**
     * Get recent post
     */
    public function getListRandomPost($limit,$language)
    {
        switch ($language)
        {
            case "en":
                $query = posts::where('enable', ENABLE)
                ->select(
                    'name_en_post as name_post',
                    'url_post',
                    'present_en_post as present_post',
                    'thumbnail_post',
                    'date_post',
                    'id_cat_post',
                    'update'
                )->where('present_en_post', '!=', '');
            break;

            default:
                $query = posts::where('enable', ENABLE)
                ->select(
                    'name_vi_post as name_post',
                    'url_post',
                    'present_vi_post as present_post',
                    'thumbnail_post',
                    'date_post',
                    'id_cat_post',
                    'update'
                );
        }

        $query = $query->orderBy(DB::raw('RAND()'))
            ->latest('date_post')
            ->take($limit)
            ->get();

        return $query;
    }

    /**
     * Get most view post
     */
    public function getListMostViewPost($limit,$idCat)
    {
        $query = posts::where('enable', ENABLE);

        if ($idCat != null)
        {
            $query = $query->where('id_cat_post', '=', $idCat);
        }

        $query = $query->orderBy('views', 'desc')
            ->latest('date_post')
            ->take($limit)
            ->get();

        return $query;
    }

    /**
     * search post have paginate
     */
    public function searchPostHavePaginate($request, $language, $limit = 0)
    {
        $response = [];
        $keyword = '%' . $request->keyword . '%';

        // Create query
        // ilike only for Postgre, not support for phpmyadmin
        switch ($language)
        {
            case "en":
                $query = posts::where('name_en_post', 'like', $keyword);
            break;
            default:
                $query = DB::table('posts')
                ->select(
                    'name_vi_post AS name_post',
                    'present_vi_post AS present_post',
                    'url_post AS url_post',
                    'thumbnail_post AS thumbnail_post',
                    DB::raw("DATE_FORMAT(date_post,'%d-%m-%Y') as date_post"),
                    'id_cat_post'
                    )
                ->where('name_vi_post', 'like', $keyword)
                ->where('enable', ENABLE);
            break;
        }

        if ($request->category != 'all')
        {
            $query = $query->where('id_cat_post', $request->category);
        }

        if ($request->date == 'asc')
        {
            $query = $query->orderBy('date_post', 'desc');
        }
        else if ($request->date == 'desc')
        {
            $query = $query->orderBy('date_post', 'asc');
        }

        // Get response
        if ($limit > 0)
        {
            $response = $query->paginate($limit);
        }

        return $response;
    }

    public function checkContentExist($urlPost, $lang)
    {
        $post = posts::where('url_post', 'like', $urlPost)->first();
        $result = false;
        $present = $content = null;
        if ($lang == 'en') {
            $present = $post->present_en_post;
            $content = $post->content_en_post;
        } else {
            $present = $post->present_vi_post;
            $content = $post->content_vi_post;
        }
        if ($present != null && $content != null) {
            $result = true;
        }
        return $result;
    }

    /**
     * Content Post
     */
    public function content($urlDetailPost,$language)
    {
        switch ($language)
        {
            case "en":
                $post = posts::join('categorypost', 'categorypost.id_cat_post', 'posts.id_cat_post')
                    ->join('users', 'users.signature', 'posts.author')
                    ->select(
                        'categorypost.*',
                        'posts.id_post',
                        'posts.id_cat_post',
                        'posts.name_en_post as name_detailpost',
                        'posts.name_vi_post as name_hidden_post',
                        'posts.url_post',
                        'posts.present_en_post as present_post',
                        'posts.present_vi_post as present_hidden_post',
                        'posts.content_en_post as content_detailpost',
                        'posts.thumbnail_post',
                        'posts.views',
                        'posts.date_post',
                        'posts.author',
                        'posts.update',

                        'users.signature as signature',
                        'users.avatar as avatar',
                        'users.description as description'
                    );
            break;

            default:
                $post = posts::join('categorypost', 'categorypost.id_cat_post', 'posts.id_cat_post')
                    ->join('users', 'users.signature', 'posts.author')
                    ->select(
                        'categorypost.*',
                        'posts.id_post',
                        'posts.id_cat_post',
                        'posts.name_vi_post as name_detailpost',
                        'posts.name_en_post as name_hidden_post',
                        'posts.url_post',
                        'posts.present_vi_post as present_post',
                        'posts.present_en_post as present_hidden_post',
                        'posts.content_vi_post as content_detailpost',
                        'posts.thumbnail_post',
                        'posts.views',
                        'posts.date_post',
                        'posts.author',
                        'posts.update',

                        'users.signature as signature',
                        'users.avatar as avatar',
                        'users.description as description'
                    );
        }

        $post = $post->where('url_post', 'like', $urlDetailPost)->first();

        return $post;
    }

    public function involve($urlDetailPost,$language)
    {
        $idCat = $this->content($urlDetailPost,$language)->id_category;

        switch ($language)
        {
            case "en":
                $query = posts::where('id_cat_post','=',$idCat)
                    ->select(
                        'posts.id_post',
                        'posts.id_cat_post',
                        'posts.name_en_post as name_detailpost',
                        'posts.url_post',
                        'posts.thumbnail_post',
                        'posts.date_post'
                    );
            break;

            default:
                $query = posts::where('id_cat_post','=',$idCat)
                    ->select(
                        'posts.id_post',
                        'posts.id_cat_post',
                        'posts.name_vi_post as name_detailpost',
                        'posts.url_post',
                        'posts.thumbnail_post',
                        'posts.date_post'
                    );
        }

        $query = $query->where('enable', true)
                ->orderBy(DB::raw('RAND()'))
                ->take(INVOLVE_POST)
                ->get();

        return $query;
    }
}
