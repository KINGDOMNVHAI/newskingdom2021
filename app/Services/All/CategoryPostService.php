<?php
namespace App\Services\All;

use App\Models\categorypost;
use App\Models\posts;
use DB;
use Illuminate\Support\ServiceProvider;

class CategoryPostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function listCategory($lang)
    {
        switch ($lang)
        {
            case "en":
                $query = DB::table('categorypost')
                    ->select('id_cat_post'
                    , 'name_vi_cat_post AS name_cat_post'
                    , 'enable_cat_post');
            break;

            default:
                $query = DB::table('categorypost')
                ->select('id_cat_post'
                , 'name_en_cat_post AS name_cat_post'
                , 'enable_cat_post');
        }

        return $query->where('enable_cat_post', ENABLE)->get();
    }

    public function listCategoryEnable($paginate, $enable, $limit)
    {
        $query = categorypost::where('enable_cat_post', $enable);
        if ($paginate) {
            $query = $query->paginate($limit);
        } else {
            $query = $query->limit($limit)->get();
        }
        return $query;
    }

    /**
     *
     *
     * @return void
     */
    public function listPost($urlCat,$language)
    {
        $idCat = categorypost::where('url_cat_post', 'like', $urlCat)->first();

        switch ($language)
        {
            case "en":
                $post = posts::join('categorypost', 'categorypost.id_cat_post', 'posts.id_cat_post')
                ->select(
                    'categorypost.*',
                    'posts.name_en_post as name_post',
                    'posts.url_post as url_post',
                    'posts.present_en_post as present_post',
                    DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                    'posts.thumbnail_post as thumbnail_post'
                )->where('present_en_post', '!=', '');
            break;

            default:
                $post = posts::join('categorypost', 'categorypost.id_cat_post', 'posts.id_cat_post')
                ->select(
                    'categorypost.*',
                    'posts.name_vi_post as name_post',
                    'posts.url_post as url_post',
                    'posts.present_vi_post as present_post',
                    DB::raw("DATE_FORMAT(posts.date_post,'%d-%m-%Y') as date_post"),
                    'posts.thumbnail_post as thumbnail_post'
                );
        }

        $post = $post->where('posts.enable', '=', ENABLE)
            ->where('posts.id_cat_post', '=', $idCat->id_cat_post)
            ->orderBy('posts.date_post', 'DESC')
            ->paginate(POST_PER_CATEGORY);

        return $post;
    }

    public function listPostJoinCategoryPaginate($fields, $params)
    {
        // $query = DB::table('posts')->join('categorypost', 'posts.id_cat_post', '=', 'categorypost.id_cat_post');

        // // Nếu yêu cầu search cột cụ thể
        // if ($fields == null)
        // {
        //     $query = $query->select('posts.*', 'categorypost.*')
        //     ->where('posts.name_vi_post', 'LIKE', "%{$params['name_vi_post']}%");
        // }
        // else
        // {
        //     $query = $query->select($fields)
        //     ->where('posts.name_vi_post', 'LIKE', "%{$params['name_vi_post']}%");
        // }

        // // Nếu search yêu cầu id_cat_post
        // if ($params['id_cat_post'] && is_numeric($params['id_cat_post']))
        // {
        //     $query = $query->where('categorypost.id_cat_post', '=', "{$params['id_cat_post']}");
        // }

        // // Nếu search yêu cầu số năm
        // if ($params['year'] && is_numeric($params['year']))
        // {
        //     $query = $query->whereRaw('YEAR(posts.date_post) = ?', $params['year']);
        // }

        // // Nếu search yêu cầu số tháng
        // if ($params['year'] && is_numeric($params['month']))
        // {
        //     $query = $query->whereRaw('MONTH(posts.date_post) = ?', $params['month']);
        // }

        // Mặc định sắp xếp id giảm dần
        $newold = 'DESC';
        // if ($params['newold'])
        // {
        //     $newold = $params['newold'];
        // }
        // $query = $query->orderBy('posts.id_post', $newold)->paginate(PAGINATE_POST_INDEX);

        $query = DB::table('posts')->join('categorypost', 'posts.id_cat_post', '=', 'categorypost.id_cat_post')
            ->select('posts.id_post'
            ,'posts.name_vi_post'
            ,'posts.url_post'
            ,'posts.present_vi_post'
            ,'posts.thumbnail_post'
            ,'posts.date_post'
            ,'posts.update'

            , 'categorypost.id_cat_post'
            , 'categorypost.name_vi_cat_post AS name_cat_post'
            , 'categorypost.url_cat_post'
        )
        ->orderBy('posts.id_post', $newold)->paginate(PAGINATE_POST_INDEX);

        return $query;
    }
}
