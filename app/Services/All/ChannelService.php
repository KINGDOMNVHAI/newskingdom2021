<?php
namespace App\Services\All;

use App\Models\channels;
use DB;
use Illuminate\Support\ServiceProvider;

class ChannelService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show newest post
     *
     * @return void
     */
    public function getChannelInfo($idChannel)
    {
        $query = channels::where('id_channel', $idChannel)->first();

        return $query;
    }
}
