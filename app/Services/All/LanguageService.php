<?php
namespace App\Services\All;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class LanguageService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show newest post
     *
     * @return void
     */
    public function getLanguage()
    {
        $language = 'vi';
        if (session()->has('locale')) {
            App::setLocale(session()->get('locale'));
            $language = session()->get('locale');
        }
        return $language;
    }
}
