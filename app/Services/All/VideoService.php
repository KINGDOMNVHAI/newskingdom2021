<?php
namespace App\Services\All;

use App\Models\videos;
use App\Models\secretvideos;
use DB;
use Illuminate\Support\ServiceProvider;

class VideoService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get newest video
     */
    public function getListNewestVideo($limit,$idChannel,$secret,$language = 'vi')
    {
        switch ($language)
        {
            case "en":
                $query1 = videos::join('channels', 'channels.id_channel', '=', 'videos.id_channel')
                ->select(
                    'videos.id_video',
                    'videos.id_channel',
                    'videos.name_en_video as name_video',
                    'videos.description_en_video as description_video',
                    'videos.url_video',
                    DB::raw("DATE_FORMAT(videos.date_video,'%d-%m-%Y') as date_video"),
                    'videos.thumbnail_video',

                    'channels.id_channel',
                    'channels.name_channel',
                    'channels.url_channel',
                    'channels.thumbnail_channel'
                )
                ->where('videos.enable_video', ENABLE);
            break;

            default:
                $query1 = videos::join('channels', 'channels.id_channel', '=', 'videos.id_channel')
                ->select(
                    'videos.id_video',
                    'videos.id_channel',
                    'videos.name_vi_video as name_video',
                    'videos.description_vi_video as description_video',
                    'videos.url_video',
                    DB::raw("DATE_FORMAT(videos.date_video,'%d-%m-%Y') as date_video"),
                    'videos.thumbnail_video',

                    'channels.id_channel',
                    'channels.name_channel',
                    'channels.url_channel',
                    'channels.thumbnail_channel'
                )
                ->where('videos.enable_video', ENABLE);
        }

        if ($idChannel != null)
        {
            $query1 = $query1->where('channels.id_channel', '=', $idChannel);
        }
        $query1 = $query1->latest('videos.date_video')->take($limit);

        if ($secret)
        {
            switch ($language)
            {
                case "en":
                    $query2 = secretvideos::join('channels', 'channels.id_channel', '=', 'secretvideos.id_channel')
                    ->select(
                        'secretvideos.id_secretvideo',
                        'secretvideos.id_channel',
                        'secretvideos.name_en_secretvideo as name_video',
                        'secretvideos.description_en_secretvideo as description_video',
                        'secretvideos.url_video',
                        DB::raw("DATE_FORMAT(secretvideos.date_secretvideo,'%d-%m-%Y') as date_video"),
                        'secretvideos.thumbnail_secretvideo as thumbnail_video',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel'
                    )
                    ->where('secretvideos.enable_secretvideo', ENABLE);
                break;

                default:
                    $query2 = secretvideos::join('channels', 'channels.id_channel', '=', 'secretvideos.id_channel')
                    ->select(
                        'secretvideos.id_secretvideo',
                        'secretvideos.id_channel',
                        'secretvideos.name_vi_secretvideo as name_video',
                        'secretvideos.description_vi_secretvideo as description_video',
                        'secretvideos.url_video',
                        DB::raw("DATE_FORMAT(secretvideos.date_secretvideo,'%d-%m-%Y') as date_video"),
                        'secretvideos.thumbnail_secretvideo as thumbnail_video',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel'
                    )
                    ->where('secretvideos.enable_secretvideo', ENABLE);
            }

            if ($idChannel != null)
            {
                $query2 = $query2->where('channels.id_channel', '=', $idChannel);
            }

            $final = $query2->latest('secretvideos.date_secretvideo')->union($query1)->take($limit)->get();
        }
        else
        {
            $final = $query1->take($limit)->get();
        }

        return $final;
    }

    /**
     * Get newest video
     */
    public function getListNewestVideoForChannel($limit,$idChannel,$secret,$language = 'vi')
    {
        switch ($language)
        {
            case "en":
                $query1 = videos::join('channels', 'channels.id_channel', '=', 'videos.id_channel')
                ->select(
                    'videos.id_video',
                    'videos.name_en_video as name_video',
                    'videos.description_en_video as description_video',
                    'videos.url_video',
                    DB::raw("DATE_FORMAT(videos.date_video,'%d-%m-%Y') as date_video"),
                    'videos.thumbnail_video',

                    'channels.id_channel',
                    'channels.name_channel',
                    'channels.url_channel',
                    'channels.thumbnail_channel'
                )
                ->where('videos.enable_video', ENABLE);
            break;

            default:
                $query1 = videos::join('channels', 'channels.id_channel', '=', 'videos.id_channel')
                ->select(
                    'videos.id_video',
                    'videos.name_vi_video as name_video',
                    'videos.description_vi_video as description_video',
                    'videos.url_video',
                    DB::raw("DATE_FORMAT(videos.date_video,'%d-%m-%Y') as date_video"),
                    'videos.thumbnail_video',

                    'channels.id_channel',
                    'channels.name_channel',
                    'channels.url_channel',
                    'channels.thumbnail_channel'
                )
                ->where('videos.enable_video', ENABLE);
        }

        if ($idChannel != null)
        {
            $query1 = $query1->where('channels.id_channel', '=', $idChannel);
        }
        $query1 = $query1->latest('videos.date_video');

        if ($secret)
        {
            switch ($language)
            {
                case "en":
                    $query2 = secretvideos::join('channels', 'channels.id_channel', '=', 'secretvideos.id_channel')
                    ->select(
                        'secretvideos.id_secretvideo',
                        'secretvideos.name_en_secretvideo as name_video',
                        'secretvideos.description_en_secretvideo as description_video',
                        'secretvideos.url_video',
                        DB::raw("DATE_FORMAT(secretvideos.date_secretvideo,'%d-%m-%Y') as date_video"),
                        'secretvideos.thumbnail_secretvideo as thumbnail_video',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel'
                    )
                    ->where('secretvideos.enable_secretvideo', ENABLE);
                break;

                default:
                    $query2 = secretvideos::join('channels', 'channels.id_channel', '=', 'secretvideos.id_channel')
                    ->select(
                        'secretvideos.id_secretvideo',
                        'secretvideos.name_vi_secretvideo as name_video',
                        'secretvideos.description_vi_secretvideo as description_video',
                        'secretvideos.url_video',
                        DB::raw("DATE_FORMAT(secretvideos.date_secretvideo,'%d-%m-%Y') as date_video"),
                        'secretvideos.thumbnail_secretvideo as thumbnail_video',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel'
                    )
                    ->where('secretvideos.enable_secretvideo', ENABLE);
            }

            if ($idChannel != null)
            {
                $query2 = $query2->where('channels.id_channel', '=', $idChannel);
            }

            $final = $query2->latest('secretvideos.date_secretvideo')->union($query1)->paginate(PAGINATE_CHANNEL);
        }
        else
        {
            $final = $query1->paginate(PAGINATE_CHANNEL);
        }


        return $final;
    }

    /**
     * Content Video
     */
    public function getVideo($urlVideo,$secret,$language)
    {
        $video = null;
        // Kiểm tra urlVideo là tìm trên table video hay secretvideo
        if ($secret)
        {
            switch ($language)
            {
                case "en":
                    $video = secretvideos::join('channels', 'channels.id_channel', '=', 'secretvideos.id_channel')
                    ->join('iframevideo', 'iframevideo.id_video', '=', 'secretvideos.id_secretvideo')
                    ->join('iframe', 'iframe.id_iframe', '=', 'iframevideo.id_iframe')
                    ->select(
                        'secretvideos.id_secretvideo as id_video',
                        'secretvideos.id_channel',
                        'secretvideos.name_en_secretvideo as name_video',
                        'secretvideos.description_en_secretvideo as description_video',
                        'secretvideos.url_video',
                        DB::raw("DATE_FORMAT(secretvideos.date_secretvideo,'%d-%m-%Y') as date_video"),
                        'secretvideos.thumbnail_secretvideo as thumbnail_video',
                        'secretvideos.enable_en',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel',
                        'channels.facebook_channel', 'channels.twitter_channel',
                        'channels.instagram_channel', 'channels.patreon_channel',

                        'iframevideo.*'
                    )
                    ->where('secretvideos.enable_en', ENABLE);
                break;

                default:
                    $video = secretvideos::join('channels', 'channels.id_channel', '=', 'secretvideos.id_channel')
                    ->join('iframevideo', 'iframevideo.id_video', '=', 'secretvideos.id_secretvideo')
                    ->join('iframe', 'iframe.id_iframe', '=', 'iframevideo.id_iframe')
                    ->select(
                        'secretvideos.id_secretvideo as id_video',
                        'secretvideos.id_channel',
                        'secretvideos.name_vi_secretvideo as name_video',
                        'secretvideos.description_vi_secretvideo as description_video',
                        'secretvideos.url_video',
                        DB::raw("DATE_FORMAT(secretvideos.date_secretvideo,'%d-%m-%Y') as date_video"),
                        'secretvideos.thumbnail_secretvideo as thumbnail_video',
                        'secretvideos.enable_vi',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel',
                        'channels.facebook_channel', 'channels.twitter_channel',
                        'channels.instagram_channel', 'channels.patreon_channel',

                        'iframe.*',
                        'iframevideo.*'
                    )
                    ->where('secretvideos.enable_vi', ENABLE);
            }
            $video = $video->where('secretvideos.url_video', '=', $urlVideo)
                ->where('iframevideo.secret_video', '=', 1)
                ->first();
        }

        if ($video == null)
        {
            switch ($language)
            {
                case "en":
                    $video = videos::join('channels', 'channels.id_channel', '=', 'videos.id_channel')
                    ->join('iframevideo', 'iframevideo.id_video', '=', 'videos.id_video')
                    ->join('iframe', 'iframe.id_iframe', '=', 'iframevideo.id_iframe')
                    ->select(
                        'videos.id_video',
                        'videos.id_channel',
                        'videos.name_en_video as name_video',
                        'videos.description_en_video as description_video',
                        'videos.url_video',
                        DB::raw("DATE_FORMAT(videos.date_video,'%d-%m-%Y') as date_video"),
                        'videos.thumbnail_video',
                        'videos.enable_en',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel',

                        'iframevideo.*',
                        'iframe.*'
                    )
                    ->where('videos.enable_video', ENABLE)
                    ->where('videos.enable_en', ENABLE)
                    ->where('iframevideo.language', $language);
                break;

                default:
                    $video = videos::join('channels', 'channels.id_channel', '=', 'videos.id_channel')
                    ->join('iframevideo', 'iframevideo.id_video', '=', 'videos.id_video')
                    ->join('iframe', 'iframe.id_iframe', '=', 'iframevideo.id_iframe')
                    ->select(
                        'videos.id_video',
                        'videos.id_channel',
                        'videos.name_vi_video as name_video',
                        'videos.description_vi_video as description_video',
                        'videos.url_video',
                        DB::raw("DATE_FORMAT(videos.date_video,'%d-%m-%Y') as date_video"),
                        'videos.thumbnail_video',
                        'videos.enable_vi',

                        'channels.id_channel',
                        'channels.name_channel',
                        'channels.url_channel',
                        'channels.thumbnail_channel',

                        'iframevideo.*',
                        'iframe.*'
                    )
                    ->where('videos.enable_video', ENABLE)
                    ->where('videos.enable_vi', ENABLE)
                    ->where('iframevideo.language', $language);
            }
            $video = $video->where('videos.url_video', '=', $urlVideo)
                ->where('iframevideo.secret_video', '=', 0)
                ->first();
        }

        return $video;
    }
}
