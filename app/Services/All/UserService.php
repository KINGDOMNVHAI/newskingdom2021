<?php
namespace App\Services\All;

use App\Models\users;
use DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Check user by username
     */
    public function checkUserByUsername($username)
    {
        return users::where('username', $username)->first();
    }

    /**
     * Check user by id
     */
    public function checkUserById($id)
    {
        return users::where('id', $id)->first();
    }

    /**
     * Check user by email
     */
    public function checkUserByEmail($email)
    {
        return users::where('email', $email)->first();
    }

    /**
     * Check user by username
     */
    public function checkUserByUser($username)
    {
        return users::where('username', $username)->first();
    }

    /**
     * Get list account is user
     */
    public function getListAccountByRoleHavePaginate($role, $limit = 0)
    {
        $query = users::where('role', $role)->get();
        if ($role > 0) {
            $query = $query->paginate($limit);
        }
        return $query->get();
    }

    public function changePasswordById($id, $password)
    {
        return DB::table('users')
            ->where('id', '=', $id)
            ->update([
                'password' => md5($password),
                // 'password' => bcrypt($password),
            ]);
    }

    /**
     * Login for MD5
     */
    public function login($username, $password)
    {
        return users::where('username', $username)
            ->where('password',md5($password))
            ->first();
    }

    public function checkUserBySession($fields)
    {
        return users::where('id', Auth::user()->id)->select($fields)->first();
    }
}
