<?php
namespace App\Services\All;

use App\Models\detailpost;
use DB;
use Illuminate\Support\ServiceProvider;

class ListRecentPost extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function run($num,$language)
    {
        // RECENT POST
        switch ($language)
        {
            case "en":
                $view = posts::where('enable', ENABLE)
                ->select(
                    'name_en_detailpost as name_detailpost',
                    'url_detailpost',
                    'present_en_detailpost as present_detailpost',
                    'img_detailpost',
                    'date_detailpost',
                    'id_cat_detailpost',
                    'update'
                );
            break;

            default:
                $view = posts::where('enable', ENABLE)
                ->select(
                    'name_vi_detailpost as name_detailpost',
                    'url_detailpost',
                    'present_vi_detailpost as present_detailpost',
                    'img_detailpost',
                    'date_detailpost',
                    'id_cat_detailpost',
                    'update'
                );
        }

        $view = $view->orderBy(DB::raw('RAND()'))
            ->latest('date_detailpost')
            ->take($num)
            ->get();

        return $view;
    }
}
