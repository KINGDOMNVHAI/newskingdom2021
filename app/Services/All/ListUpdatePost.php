<?php
namespace App\Services\All;

use App\Models\detailpost;
use Illuminate\Support\ServiceProvider;

class ListUpdatePost extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show newest post
     *
     * @return void
     */
    public function run($num,$idCat,$language)
    {
        // UPDATE POST
        switch ($language)
        {
            case "en":
                $new = posts::where('enable', ENABLE)
                ->select(
                    'name_en_detailpost as name_detailpost',
                    'url_detailpost',
                    'present_en_detailpost as present_detailpost',
                    'img_detailpost',
                    'date_detailpost',
                    'id_cat_detailpost',
                    'update'
                );
            break;

            default:
                $new = posts::where('enable', ENABLE)
                ->select(
                    'name_vi_detailpost as name_detailpost',
                    'url_detailpost',
                    'present_vi_detailpost as present_detailpost',
                    'img_detailpost',
                    'date_detailpost',
                    'id_cat_detailpost',
                    'update'
                );
        }

        if ($idCat != null)
        {
            $new = $new->where('id_cat_detailpost', '=', $idCat);
        }

        $new = $new->where('update', '=', UPDATE_POST)
            ->orderBy('updated_at', 'desc')
            ->take($num)
            ->get();

        return $new;
    }
}
