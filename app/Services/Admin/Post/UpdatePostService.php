<?php
namespace App\Services\Admin\Post;

use App\Models\detailpost;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\ServiceProvider;

class UpdatePostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function run($datas, $request)
    {
        // File name mặc định không có tên
        $fileName = '';
        $uploadPath = 'upload/images/thumbnail';

        if ($request->hasFile('thumbnail'))
        {
            // Thư mục upload
            // $uploadPath = public_path('upload/images/thumbnail');
            $file = $request->file('thumbnail');

            // File name được gắn tên
            $fileName = $file->getClientOriginalName();

            // Đưa file vào thư mục
            $file->move($uploadPath, $fileName);
        } else {
            $fileName = $request->img;
        }

        // idUpdate là id sẽ nhập vào data
        $idUpdate = $datas['id_detailpost'];

        // Case 1: trường hợp Insert, id null
        // Case 2: trường hợp Update, id có giá trị
        if ($idUpdate == null)
        {
            // $getFinalID =  DB::table('detailpost')
            //     ->select(DB::raw('max(id_detailpost) as id_detailpost'))
            //     ->first();

            // $idUpdate = $getFinalID->id_detailpost;

            $query = posts::create([
                'name_vi_detailpost'    => $datas['name_vi_detailpost'],
                'name_en_detailpost'    => $datas['name_en_detailpost'],
                'url_detailpost'        => $datas['url_detailpost'],
                'content_vi_detailpost' => $datas['content_vi_detailpost'],
                'content_en_detailpost' => $datas['content_en_detailpost'],
                'date_detailpost'       => date('Y-m-d'),
                'present_vi_detailpost' => $datas['present_vi_detailpost'],
                'present_en_detailpost' => $datas['present_en_detailpost'],
                'img_detailpost'        => $fileName, // Lấy tên file
                'id_cat_detailpost'     => $datas['id_cat_detailpost'],
                'signature'             => Auth::user()->signature,
                'author'                => Auth::user()->username,
                'enable'                => $datas['enable'],
                'popular'               => $datas['popular'],
                'update'                => $datas['update'],
                'views'                 => random_int(10,100),
            ]);
        }
        else
        {
            $query = posts::where('id_detailpost', $idUpdate)
            ->update([
                'name_vi_detailpost'    => $datas['name_vi_detailpost'],
                'name_en_detailpost'    => $datas['name_en_detailpost'],
                'url_detailpost'        => $datas['url_detailpost'],
                'content_vi_detailpost' => $datas['content_vi_detailpost'],
                'content_en_detailpost' => $datas['content_en_detailpost'],
                'date_detailpost'       => $datas['date_detailpost'],
                'present_vi_detailpost' => $datas['present_vi_detailpost'],
                'present_en_detailpost' => $datas['present_en_detailpost'],
                'img_detailpost'        => $fileName,  // Lấy tên file
                'id_cat_detailpost'     => $datas['id_cat_detailpost'],
                'popular'               => $datas['popular'],
                'update'                => $datas['update'],
                'enable'                => $datas['enable'],
            ]);
        }

        return $query;
    }
}
