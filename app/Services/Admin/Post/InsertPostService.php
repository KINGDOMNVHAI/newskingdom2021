<?php
namespace App\Services\Admin\Post;

use App\Models\detailpost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\ServiceProvider;

class InsertPostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function run($request)
    {
        $enable  = 0;
        $popular = 0;
        $update  = 0;

        // File name mặc định không có tên
        $fileName = '';

        //Kiểm tra file
        if (Input::hasFile('thumbnail'))
        {
            // Thư mục upload
            $uploadPath = public_path('upload/images/thumbnail');
            $file = Input::file('thumbnail');

            // File name được gắn tên
            $fileName = $file->getClientOriginalName();

            // Đưa file vào thư mục
            $file->move($uploadPath, $fileName);
        }

        if ($request->enable != null)
        {
            $enable = $request->enable;
        }
        if ($request->popular != null)
        {
            $popular = $request->popular;
        }
        if ($request->update != null)
        {
            $update = $request->update;
        }

        $query = posts::insert([
            'name_detailpost'    => $request->name_detailpost,
            'url_detailpost'     => $request->url_detailpost,
            'content_detailpost' => $request->content_detailpost,
            'present_detailpost' => $request->present_detailpost,
            'date_detailpost'    => $request->date,
            'img_detailpost'     => $fileName,  // Lấy tên file
            'idCat'              => $request->id_cat_detailpost,
            'signature'          => Auth::user()->signature,
            'author'             => Auth::user()->username,
            'enable'             => $enable,
            'popular'            => $popular,
            'update'             => $update,
            'views'              => 0,
        ]);

        return $query;
    }
}
