<?php
namespace App\Services\Admin\Post;

use App\Model\detailpost;
use Illuminate\Support\ServiceProvider;
use DB;

class ListPostService extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     *
     * @return void
     */
    public function list()
    {
        $query = posts::all();

        return $query;
    }

    public function listPaginate($fields = null)
    {
        if ($fields == null)
        {
            // Nếu sử dụng model, kết quả là tất cả fields trong model.
            $query = posts::paginate(PAGINATE_POST_INDEX);
        }
        else
        {
            // Nếu sử dụng DB::table(), kết quả dựa theo select
            $query = DB::table('posts')->select($fields)->paginate(PAGINATE_POST_INDEX);
        }
        return $query;
    }
}
