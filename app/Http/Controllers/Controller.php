<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Title
    public $title = TITLE_NEWS_INDEX;

    // language
    public $language = LANGUAGE_VI;

    public function __construct()
    {
        // if (Session::has('lang'))
        // {
        //     $language = LANGUAGE_VI;
        // }
    }
}
