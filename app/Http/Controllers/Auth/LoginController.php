<?php
namespace App\Http\Controllers\Auth;

use App\Model\users;
use App\Http\Controllers\Controller;
use App\Response\Auth\LoginResponse;
use App\Services\All\UserService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        // Nếu gặp lỗi sau khi đăng nhập, gõ /login trên trình duyệt bị chuyển sang trang /home
        // xem file RouteServiceProvider
        return view('auth.login.login', [
            'title' => TITLE_ADMIN_DASHBOARD,
        ]);
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        // Cách login bằng md5()
        // $checklogin = new UserService;
        // $user = $checklogin->login($username, $password);

        // Cách login bằng bcrypt()
        // if (Auth::attempt(['username' => $username, 'password' => $password]))

        if (Auth::attempt(['username' => $username, 'password' => $password]))
        {
            return redirect('dashboard');
        }
        else
        {
            $checklogin = new UserService;
            $user = $checklogin->checkUserByUsername($username);

            // File auth.php
            if ($user == null || $user == '')
            {
                return redirect()->route('login')->with('message', __('auth.failed.wrong_username'));
            }
            else
            {
                return redirect()->route('login')->with('message', __('auth.failed.wrong_password'));
            }
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('login');
    }

    public function redirectSoshiki()
    {
        return redirect(SOSHIKI_URL);
    }
}
