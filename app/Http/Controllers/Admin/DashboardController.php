<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\All\VideoService;
use App\Services\Auth\DashboardService;
use Illuminate\Support\Facades\Auth;
use App\Services\All\PostService;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {
        if (Auth::check())
        {
            $dashboardService = new DashboardService;
            $viewDashboard    = $dashboardService->analyticsPost();

            $newestPost = new PostService;
            $viewNewest = $newestPost->getListNewestPost(DASHBOARD_HOME_POSTS, null, true, $this->language);

            return view('admin.dashboard.dashboard', [
                'title'         => TITLE_ADMIN_DASHBOARD,
                'categories'    => $viewDashboard,
                'newest'        => $viewNewest,
            ]);
        }
        else {
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
