<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\category;
use App\Model\detailpost;
use App\Services\Admin\Categories\ListCategoryService;
use App\Services\Admin\Post\InsertPostService;
use App\Services\Admin\Post\ListPostService;
use App\Services\Admin\Post\SearchPostService;
use App\Services\Admin\Post\UpdatePostService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class APISocialNetworkController extends Controller
{
    public function index()
    {
        if (Auth::check())
        {
            return view('admin.apisocial.apisocial-index', [
                'title'      => TITLE_ADMIN_POST,
                // 'posts'      => $viewPost,
                // 'categories' => $viewCategory,
            ]);
        }
        else {
            return redirect()->route('login');
        }
    }

    public function updateTwitter(Request $request)
    {
        // File name mặc định không có tên
        $fileName = '';
        $uploadPath =  'js/twitterapi';

        if ($request->hasFile('jsonfile'))
        {
            // Thư mục upload
            // $uploadPath = public_path('js/twitterapi');
            $file = $request->file('jsonfile');

            // File name được gắn tên
            $fileName = $file->getClientOriginalName();

            // Đưa file vào thư mục
            $file->move($uploadPath, $fileName);
        }

        return redirect()->route('api-social-network-index');
    }
}
