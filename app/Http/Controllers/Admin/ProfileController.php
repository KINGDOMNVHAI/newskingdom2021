<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\ProfileService;
use App\Services\All\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {
        if (Auth::check())
        {
            $fields = [
                'id', 'lastname', 'firstname', 'username', 'password',
                'email', 'role', 'city', 'address', 'company',
                'facebook', 'twitter', 'description', 'signature', 'banner', 'avatar',
            ];

            $userProfile = new UserService;
            $viewData   = $userProfile->checkUserBySession($fields);

            return view('admin.setting.profile', [
                'title' => 'User Profile',
                'user' => $viewData
            ]);
        }
        else {
            return redirect()->route('login');
        }
    }

    // Công việc:
    // 1) Tìm cách gộp 2 hàm index và update làm 1
    // 2) Sửa hàm callUserBySession bằng cách gọi hàm trong UserService, vẫn cho truyền fields vào
    public function profileUpdate($id, Request $request)
    {
        $userService = new UserService;
        $profileService = new ProfileService;
        if (Auth::check())
        {
            $fields = [
                'id', 'lastname', 'firstname', 'username', 'password',
                'email', 'role', 'city', 'address', 'company',
                'facebook', 'twitter', 'description', 'signature', 'banner', 'avatar',
            ];

            $viewData   = $userService->checkUserBySession($fields, Auth::user()->id);

            if ($request->isMethod('POST'))
            {
                $viewData = $profileService->updateProfile($request);

                return redirect()->route('user-profile');
            }

            return view('admin.setting.userprofile', [
                'title' => 'User Profile',
                'user' => $viewData
            ]);
        }
        else {
            return redirect()->route('login');
        }
    }

    public function update(Request $request)
    {
        $profileService = new ProfileService;
        $viewData = $profileService->updateProfile($request);

        return redirect('/user-profile');
    }

    // public function printPDFProfile()
    // {
    //     if (Auth::check())
    //     {

    //         $format = 'A5-L';
    //         $fontsize = 10;
    //         $fontTitle = 14;
    //         $fontThead = 10;
    //         $fontTD = 10;
    //         $fontHead = 10;

    //         try {



    //             $viewData = [
    //                 'title' => 'Thông tin cá nhân',
    //                 'date' => date("d/m/Y"),
    //                 'fontsize' => $fontsize,
    //                 'format' => $format,
    //                 'fontTitle' => $fontTitle,
    //                 'fontThead' => $fontThead,
    //                 'fontTD' => $fontTD,
    //                 'fontHead' => $fontHead
    //             ];

    //             $options = [
    //                 // 'format' => $format,
    //                 // 'tempDir' => storage_path('tempdir'),
    //                 // 'default_font_size'=> $fontsize,
    //                 'mode' => 'utf-8'
    //             ];


    //             $url = 'export/pdf/profile-'. microtime(true) .'.pdf';

    //             $pdf = PDF::loadView('pdf.profile-template',  $viewData, [], 'utf-8');
    //             $pdf->save($url);

    //             $responseData = [
    //                 'result' => true,
    //                 'data' => $url,
    //                 'message' => trans('message.success_action'),
    //                 'code' => 200,
    //             ];


    //         } catch (Exception $e) {
    //             var_dump("fail");
    //             die();
    //             // $responseData = [
    //             //     'result' => false,
    //             //     'data' => [],
    //             //     'message' => trans('message.fail_action').$e,
    //             //     'code' => 500,
    //             // ];
    //             // return response()->json($responseData);
    //             // // return view('errors.404-message');
    //         }


    //     }
    //     else {
    //         return redirect()->route('login');
    //     }
    // }

}
