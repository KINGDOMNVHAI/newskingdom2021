<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\categorypost;
use App\Models\posts;
use App\Services\Admin\Post\InsertPostService;
use App\Services\All\CategoryPostService;
use App\Services\All\LanguageService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        if (Auth::check())
        {
            $languageService = new LanguageService();
            $lang = $languageService->getLanguage();

            // Get title from parent class
            $title = "admin.list_post" . $this->title;

            // Luôn luôn trong trạng thái lấy data từ các input trong form dù có search hay không
            $request = Request::capture();
            $params = [
                'name_vi_post' => $request->query('name_vi_post'),
                'year' => $request->query('year'),
                'month' => $request->query('month'),
                'newold' => $request->query('newold'),
                'id_cat_post' => $request->query('idCat')
            ];

            $fieldsPost = [
                'posts.id_post', 'posts.name_vi_post', 'posts.present_vi_post',
                'posts.date_post', 'posts.thumbnail_post', 'posts.id_cat_post',
                'categorypost.name_cat_post',
            ];
            $postService = new CategoryPostService;
            $viewPost = $postService->listPostJoinCategoryPaginate($fieldsPost, $params);

            $categoryService = new CategoryPostService;
            $viewCategory = $categoryService->listCategory($lang);

            //Str::limit('The quick brown fox jumps over the lazy dog', 20, ' (...)');
            //https://laravel.com/docs/8.x/helpers#method-str-limit

            return view('admin.post.post-list', [
                'title'      => TITLE_ADMIN_POST,
                'posts'      => $viewPost,
                'categories' => $viewCategory,
            ]);
        }
        else {
            return redirect()->route('login');
        }
    }

    public function changeCategoryAjax(Request $request)
    {
        try
        {
            // Luôn luôn trong trạng thái lấy data từ các input trong form dù có search hay không
            $category = $request->idCat;
            $idDetailPost = $request->id_detailpost;

            $query = posts::where('id_detailpost', '=', $idDetailPost)->update(['id_cat_detailpost' => $category]);
            return response()->json(['msg'=>'Updated Successfully', 'success'=>true]);
        }
        catch(\Exception $e)
        {
            \Log::error($e); // create a log for error occurrence at storage/log/laravel.log file
            return response()->json($e->getData(), $e->getStatusCode());
        }
    }

    public function postInsertUpdate(Request $request) //$id = null,
    {
        if (Auth::check())
        {
            $model = posts::find($id);

            // Show all category, except selected category
            $listcat = categorypost::all();

            // If model is null, this is new post. Create new object
            if (!$model)
            {
                $model = new detailpost;
            }

            if ($request->isMethod('POST'))
            {
                $datas = $request->all();
                unset($datas['_token']);

                // If don't click popular and upload
                if (!isset($datas['popular']))
                {
                    $datas['popular'] = 0;
                }
                if (!isset($datas['update']))
                {
                    $datas['update'] = 0;
                }

                $rules = [
                    'name_vi_detailpost'    => 'required',
                    'url_detailpost'        => 'required',
                    'present_vi_detailpost' => 'required',
                    'id_cat_detailpost'     => 'required',
                    'content_vi_detailpost' => 'required',
                    'enable'                => 'required',
                ];

                $messages = [
                    'name_vi_detailpost.required' => 'A name_detailpost is required',
                ];

                $validator = \Validator::make($datas, $rules, $messages);

                if ($validator->fails()) {
                    // var_dump('<pre>','jha');die;
                    return \Redirect::back()->withInput()->withErrors($validator->errors());
                }

                $updatePost = new UpdatePostService;
                $viewData   = $updatePost->run($datas, $request);

                return redirect()->route('posts-index');

                // if ($validator->fails()) {
                //     var_dump('<pre>','jha');die;
                //     return \Redirect::back()->withInput()->withErrors($validator->errors());
                // }
                // foreach ($datas as $key => $value) {
                //     $model->{$key} = $value;
                // }
                // try {
                //     return redirect(route('posts-index'))->with('success', 'Tạo bài viết mới thành công.');
                // } catch (\Exception $ex) {
                //     return redirect()->back()->withInput()->with('error', $ex->getMessage());
                // }
            }

            return view('admin.post.post-insert', compact('listcat','model'));
        }
        else {
            return redirect()->route('login');
        }
    }

    public function updateManyImage(Request $request)
    {
        // Không chọn category nào.
        if ($request->id_category == 1)
        {
            return \Redirect::back()->withInput()->withErrors('Bạn phải chọn category');
        }
        else
        {
            $path =  'upload/images/';
            if ($request->id_category == 'thumbnail')
                $path = $path . 'thumbnail';
            else if ($request->id_category == WEBSITE_POST)
                $path = $path . 'website';
            else if ($request->id_category == GAME_POST)
                $path = $path . 'game';
            else if ($request->id_category == ANIME_POST)
                $path = $path . 'anime';
            else if ($request->id_category == THU_THUAT_IT_POST)
                $path = $path . 'thuthuat';

            // File name mặc định không có tên
            $fileName = '';
            if ($request->hasFile('image1'))
            {
                // Thư mục upload
                // $uploadPath = public_path($path);
                $uploadPath = $path;
                $file = $request->file('image1');

                // File name được gắn tên
                $fileName = $file->getClientOriginalName();

                // Đưa file vào thư mục
                $file->move($uploadPath, $fileName);
            }
            if ($request->hasFile('image2'))
            {
                // Thư mục upload
                // $uploadPath = public_path($path);
                $uploadPath = $path;
                $file = $request->file('image2');

                // File name được gắn tên
                $fileName = $file->getClientOriginalName();

                // Đưa file vào thư mục
                $file->move($uploadPath, $fileName);
            }
            if ($request->hasFile('image3'))
            {
                // Thư mục upload
                // $uploadPath = public_path($path);
                $uploadPath = $path;
                $file = $request->file('image3');

                // File name được gắn tên
                $fileName = $file->getClientOriginalName();

                // Đưa file vào thư mục
                $file->move($uploadPath, $fileName);
            }
            if ($request->hasFile('image4'))
            {
                // Thư mục upload
                // $uploadPath = public_path($path);
                $uploadPath = $path;
                $file = $request->file('image4');

                // File name được gắn tên
                $fileName = $file->getClientOriginalName();

                // Đưa file vào thư mục
                $file->move($uploadPath, $fileName);
            }
        }
        return redirect()->route('posts-insert-update');
    }

    public function indexInsert()
    {
        if (Auth::check())
        {
            $viewData = categorypost::all();

            return view('admin.post.postsinsert', [
                'title'      => 'Posts - list of posts in KINGDOM NVHAI',
                'categories' => $viewData
            ]);
        }
        else {
            return redirect()->route('login');
        }
    }

    public function insert(Request $request)
    {
        $insertPost = new InsertPostService;
        $viewData = $insertPost->run($request);

        // After insert, back to previous page
        return redirect()->route('posts');
    }

    public function indexUpdate($idDetailPost)
    {
        // Connect table detailpost to show information of post
        $post = posts::where('id_detailpost', '=', $idDetailPost)->first();

        // Connect table category to show name_category of post
        $postcat = categorypost::where('id_category', '=', $post->id_category)->first();

        // Show HTML of selected option
        // Use {{ $op }} or {!! $op !!} in views
        $op = '<option value="'.$postcat->id_category.'">'. $postcat->name_category .'</option>';

        // Show all category, except selected category
        $listcat = categorypost::where('id_category','!=', $post->id_category)->get();

        return view('admin.post.postsupdate', [
            'title'   => 'Site KINGDOM NVHAI',
            'post'    => $post,
            'listcat' => $listcat,
            'op'      => $op
        ]);
    }

    public function update(Request $request)
    {
        $updatePost = new UpdatePostService;
        $viewData   = $updatePost->run($request);
        return redirect()->route('posts');
    }

    public function indexDelete()
    {
        $viewData = posts::paginate(PAGINATE_POST_DELETE);
        return view('admin.post.postsdelete', [
            'title' => 'Posts - list of posts in KINGDOM NVHAI',
            'posts' => $viewData
        ]);
    }

    public function deletePost($idDetailPost)
    {
        posts::where('id_detailpost', $idDetailPost)->delete();
        return redirect()->route('posts');
    }

    public function deleteManyPost(Request $request)
    {
        // Tạo mảng chứa các id từ checkbox, đếm số lượng checkbox
        $sumarray = count($request->checkbox);
        $mang = array();
        for ($i = 0; $i<$sumarray; $i++){
            // Gán giá trị từ checkbox vào mảng
            array_push($mang, $request->checkbox[$i]);
            // Xóa mảng đó
            DB::table('detailpost')->whereIn('id_detailpost', $mang)->delete();
        }
        return redirect()->route('posts');
    }



    /**
     * Upsert tbl_license_info
     *
     * @param type $seq
     * @param Request $request
     *
     * @return type
     */
    // public function upsert($seq = null, Request $request)
    // {
    //     $title = "자격 과목 관리";
    //     $model = ClassInfo::find($seq);

    //     if (!$model)
    //     {
    //         $model = new ClassInfo;
    //     }

    //     if ($request->isMethod('POST'))
    //     {
    //         $datas = $request->all();
    //         unset($datas['_token']);

    //         $rules = [
    //             'clsid'    => 'required',
    //             'clsname'  => 'required',
    //             'clslevel' => 'required',
    //             'clskinds' => 'required',
    //         ];
    //         $messages = [];

    //         $validator = \Validator::make($datas, $rules, $messages);

    //         if ($validator->fails()) {
    //             return \Redirect::back()->withInput()->withErrors($validator->errors());
    //         }
    //         foreach ($datas as $key => $value) {
    //             $model->{$key} = $value;
    //         }
    //         if (!$model->regidate) {
    //             $model->regidate = date('Y-m-d H:i:s');
    //         }

    //         try {
    //             $model->save();
    //             return redirect(route('licencls-index'))->with('success', '수정 되었습니다.');
    //         } catch (\Exception $ex) {
    //             return redirect()->back()->withInput()->with('error', $ex->getMessage());
    //         }
    //     }

    //     return view('admin.licencls.cls_edit', compact('title', 'model'));
    // }

    /**
     *
     * @param type $seq
     * @param Request $request
     * @return type
     */
    // public function delete($seq)
    // {
    //     $model = ClassInfo::where('seq', '=', $seq)->first();
    //     if (!$model) {
    //         return redirect(route('licencls-index'))->with('error', 'Data not found!');
    //     }
    //     try {
    //         $model->delete();
    //         return redirect(route('licencls-index'))->with('success', 'Data was successfully deleted!');
    //     } catch (\Exception $ex) {
    //         return redirect(route('licencls-index'))->with('error', $ex->getMessage());
    //     }
    // }
}
