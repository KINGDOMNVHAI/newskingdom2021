<?php
namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Services\Admin\Categories\ListCategoryService;
use App\Services\All\ChannelService;
use App\Services\All\ErrorService;
use App\Services\All\GetAPIService;
use App\Services\All\PostService;
use App\Services\All\VideoService;
use App\Services\News\CategoryPostService;
use App\Services\News\ContentPostService;
use App\Services\News\CommentPostService;
use App\Services\News\HomePagePostService;
use App\Services\News\ListSearchPostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class VideoController extends Controller
{
    public function __construct()
    {
        // Menu
        $this->posts          = new HomePagePostService();
        $this->viewCategories = $this->posts->showAllCategories();

        // Tags
        $tag            = new ListSearchPostService;
        $this->viewTags = $tag->tagsList();

        // API Social: Facebook, Youtube, Twitter
        $getAPI = new GetAPIService();
        $this->facebookAPI = $getAPI->getAPIFacebook();
        $this->youtubeAPI = $getAPI->getAPIYoutube();
        $this->twitterAPI = $getAPI->getAPITwitter();
    }

    /**
     * List Video in home page
     */
    public function listVideo()
    {
        // Public Services
        $menuCategories = new ListCategoryService();
        $viewCategories = $menuCategories->listEnable();

        $secret = false;
        if (Auth::check())
        {
            $secret = true;
        }

        $videoService = new VideoService;
        $videoNewest = $videoService->getListNewestVideo(HOME_VIDEOS,null, $secret, $this->language);
        $videoKingdomnvhai = $videoService->getListNewestVideo(HOME_VIDEOS,CHANNEL_KINGDOM_NVHAI, false, $this->language);
        $videoKizunaAI = $videoService->getListNewestVideo(HOME_VIDEOS,CHANNEL_KIZUNA_AI, false, $this->language);
        $videoHololive = $videoService->getListNewestVideo(HOME_VIDEOS,CHANNEL_HOLOLIVE, false, $this->language);
        $videoPewdiepie = $videoService->getListNewestVideo(HOME_VIDEOS,CHANNEL_PEWDIEPIE, false, $this->language);
        $title = "KINGDOM VIDEO " . TITLE_KDPLAYBACK_INDEX;

        return view('news.pages.video', [
            'title'          => $title,

            // Public Services
            'categories'     => $this->viewCategories,
            'menuCategories' => $viewCategories,
            'viewTags'       => $this->viewTags,

            'facebookAPI'    => $this->facebookAPI,
            'youtubeAPI'     => $this->youtubeAPI,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'videoNewest'       => $videoNewest,
            'videoKingdomnvhai' => $videoKingdomnvhai,
            'videoKizunaAI'     => $videoKizunaAI,
            'videoHololive'     => $videoHololive,
            'videoPewdiepie'    => $videoPewdiepie,
        ]);
    }

    /**
     * Watch Video
     */
    public function watchVideo($urlVideo)
    {
        if ($urlVideo == 'kono-subarashii-sekai-ni-shukufuku-wo-drama-cd-2-vietsub') {
            return redirect(KDPLAYBACK_COM_URL . "video/kono-subarashii-sekai-ni-shukufuku-wo-drama-cd-2");
        }

        return redirect(KDPLAYBACK_COM_URL . "video/" . $urlVideo);

        // Public Services
        // $menuCategories = new ListCategoryService();
        // $viewCategories = $menuCategories->listEnable(null);

        // $secret = false;
        // if (Auth::check())
        // {
        //     $secret = true;
        // }

        // // Private Services
        // $videoService = new VideoService;
        // $videoContent = $videoService->getVideo($urlVideo,$secret,$this->language);
        // if ($videoContent == null) {
        //     return view('news.pages.404', [
        //         'title'         => '404' . TITLE_KDPLAYBACK_INDEX,
        //         'facebookAPI'   => $this->facebookAPI,
        //         'youtubeAPI'    => $this->youtubeAPI,
        //         'twitterAPI'    => $this->twitterAPI,
        //     ]);
        // }
        // $title = $videoContent->name_video . TITLE_KDPLAYBACK_INDEX;

        // return view('news.pages.video-watch', [
        //     'title'          => $title,

        //     // Public Services
        //     'categories'     => $this->viewCategories,
        //     'menuCategories' => $viewCategories,
        //     'viewTags'       => $this->viewTags,

        //     'facebookAPI'    => $this->facebookAPI,
        //     'youtubeAPI'     => $this->youtubeAPI,
        //     'twitterAPI'     => $this->twitterAPI,

        //     // Private Services
        //     'videoContent'   => $videoContent,
        // ]);
    }

    /**
     * Channel
     */
    public function listVideoInChannel($idChannel)
    {
        return redirect(KDPLAYBACK_COM_URL . "channel/" . $idChannel);
        // $channelService = new ChannelService;
        // $channelInfo = $channelService->getChannelInfo($idChannel);
        // if ($channelInfo == null) {
        //     return view('news.pages.404', [
        //         'title'         => '404' . TITLE_KDPLAYBACK_INDEX,
        //         'facebookAPI'   => $this->facebookAPI,
        //         'youtubeAPI'    => $this->youtubeAPI,
        //         'twitterAPI'    => $this->twitterAPI,
        //     ]);
        // }
        // $title = $channelInfo['name_channel'] . TITLE_KDPLAYBACK_INDEX;

        // $secret = false;
        // if (Auth::check())
        // {
        //     $secret = true;
        // }
        // $videoService = new VideoService;
        // $videoNewest = $videoService->getListNewestVideoForChannel(0,$idChannel, $secret, $this->language);

        // // Public Services
        // $menuCategories = new ListCategoryService();
        // $viewCategories = $menuCategories->listEnable();

        // $postService = new PostService;
        // $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $this->language);

        // return view('news.pages.channel', [
        //     'title'          => $title,

        //     // Public Services
        //     'categories'     => $this->viewCategories,
        //     'menuCategories' => $viewCategories,
        //     'randoms'        => $viewRandom,
        //     'viewTags'       => $this->viewTags,

        //     'facebookAPI'    => $this->facebookAPI,
        //     'youtubeAPI'     => $this->youtubeAPI,
        //     'twitterAPI'     => $this->twitterAPI,

        //     // Private Services
        //     'videoNewest'   => $videoNewest,
        //     'channelInfo'   => $channelInfo,
        // ]);
    }
}
