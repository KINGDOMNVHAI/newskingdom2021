<?php
namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Services\Admin\Categories\ListCategoryService;
use App\Services\All\GetAPIService;
use App\Services\All\ListVideo;
use App\Services\News\HomePagePostService;
use App\Services\News\ListSearchPostService;
use App\Services\News\YoutuberankService;
use Illuminate\Http\Request;
use Session;

class YoutubeController extends Controller
{
    public function __construct()
    {
        // Menu
        $this->posts          = new HomePagePostService();
        $this->viewCategories = $this->posts->showAllCategories();

        // Tags
        $tag            = new ListSearchPostService;
        $this->viewTags = $tag->tagsList();

        // API Social: Facebook, Youtube, Twitter
        $getAPI = new GetAPIService();
    }

    /**
     * Youtube Ranking
     * Trang Youtube Ranking, bảng xếp hạng các kênh Youtube dựa theo số subcribe
     */
    public function index()
    {
        $title = 'SUBSCRIBED YOUTUBE CHANNELS RANKING';

        // Always get request from input search form
        $request = Request::capture();
        $params = [
            'keywords' => $request->query('keywords'),
            'subcribeSort' => $request->query('subcribeSort'),
            'dateSort' => $request->query('dateSort'),
        ];

        // Public Services
        // $menuCategories = new ListCategoryService();
        // $viewCategories = $menuCategories->listEnable();
        $youtuberank         = new YoutuberankService;
        $youtuberankFavorite = $youtuberank->listYoutubeChannelFavorite();
        $youtuberankAll      = $youtuberank->listYoutubeChannelAll();
        $youtuberankHololive = $youtuberank->listYoutubeChannelHololive();

        return view('youtube.master-youtube', [
            'title' => $title,

            // Public Services
            'categories'     => $this->viewCategories,
            'viewTags'       => $this->viewTags,

            // Private Services
            'searchInput' => $params,
            'youtuberankFavorite' => $youtuberankFavorite,
            'youtuberankAll'      => $youtuberankAll,
            'youtuberankHololive' => $youtuberankHololive,
        ]);
    }

    public function listAjax($list)
    {
        try
        {
            if(request()->ajax())
            {
                $htmlTable = '<div class="row gy-4" style="margin-top:5px;">
                <div class="widget rounded">
                    <div class="widget-header text-center">
                        <h3 class="widget-title">50 Most Popular Channels</h3>
                        <img src="news/images/wave.svg" class="wave" alt="wave" />
                    </div>';
                $youtuberank = new YoutuberankService;

                switch ($list) {
                    case 'hololive':
                        $youtuberankVR = $youtuberank->listYoutubeChannelHololive();
                        foreach ($youtuberankVR as $data)
                        {
                            $thumbnail = '/upload/images/channel/600x400/' . $data->thumbnail_channel;
                            $htmlTable = $htmlTable . '<div class="row" style="margin-top:5px;">
                            <div class="col-sm-2">
                                <a href="' . $data->url_video_present . '" target="_blank">
                                <img src="' . $thumbnail . '" alt="' . $data->name_channel . '" title="' . $data->name_channel . '" />
                                </a>
                            </div>
                            <div class="col-sm-10">
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="' . $data->url_channel . '">' . $data->name_channel . '</a></h6>
                                    <p><b>Subscribe: ' . $data->subscribe . '</b><br>
                                    ' . $data->description_channel . '</p>
                                </div>
                            </div>
                        </div>';
                        }
                    break;
                    case 'vtuber':
                        $youtuberankVR = $youtuberank->listYoutubeChannelVR();
                        foreach ($youtuberankVR as $data)
                        {
                            $thumbnail = '/upload/images/channel/600x400/' . $data->thumbnail_channel;
                            $htmlTable = $htmlTable . '<div class="row" style="margin-top:5px;">
                            <div class="col-sm-2">
                                <a href="' . $data->url_video_present . '" target="_blank">
                                <img src="' . $thumbnail . '" alt="' . $data->name_channel . '" title="' . $data->name_channel . '" />
                                </a>
                            </div>
                            <div class="col-sm-10">
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="' . $data->url_channel . '">' . $data->name_channel . '</a></h6>
                                    <p><b>Subscribe: ' . $data->subscribe . '</b><br>
                                    ' . $data->description_channel . '</p>
                                </div>
                            </div>
                        </div>';
                        }
                    break;
                    case 'vn':
                        $youtuberankVN = $youtuberank->listYoutubeChannelVN();
                        foreach ($youtuberankVN as $data)
                        {
                            $thumbnail = '/upload/images/channel/600x400/' . $data->thumbnail_channel;
                            $htmlTable = $htmlTable . '<div class="row" style="margin-top:5px;">
                            <div class="col-sm-2">
                                <a href="' . $data->url_video_present . '" target="_blank">
                                <img src="' . $thumbnail . '" alt="' . $data->name_channel . '" title="' . $data->name_channel . '" />
                                </a>
                            </div>
                            <div class="col-sm-10">
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="' . $data->url_channel . '">' . $data->name_channel . '</a></h6>
                                    <p><b>Subscribe: ' . $data->subscribe . '</b><br>
                                    ' . $data->description_channel . '</p>
                                </div>
                            </div>
                        </div>';
                        }
                    break;
                    default:
                        $youtuberankAll = $youtuberank->listYoutubeChannelOther();
                        foreach ($youtuberankAll as $data)
                        {
                            $thumbnail = '/upload/images/channel/600x400/' . $data->thumbnail_channel;
                            $htmlTable = $htmlTable . '<div class="row" style="margin-top:5px;">
                            <div class="col-sm-2">
                                <a href="' . $data->url_video_present . '" target="_blank">
                                <img src="' . $thumbnail . '" alt="' . $data->name_channel . '" title="' . $data->name_channel . '" />
                                </a>
                            </div>
                            <div class="col-sm-10">
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="' . $data->url_channel . '">' . $data->name_channel . '</a></h6>
                                    <p><b>Subscribe: ' . $data->subscribe . '</b><br>
                                    ' . $data->description_channel . '</p>
                                </div>
                            </div>
                        </div>';
                        }
                }
                $htmlTable = $htmlTable . '</div></div>';
                return response()->json(['success' => true, 'htmlTable' => $htmlTable]);
            }
        }
        catch(\Exception $e)
        {
            \Log::error($e); // create a log for error occurrence at storage/log/laravel.log file
            return response()->json($e->getData(), $e->getStatusCode());
        }
    }

    public function updateYoutubeRanking()
    {
        $youtuberank = new YoutuberankService;
        $youtuberank->updateYoutuberankNewestResult();

        $apisocial = new GetAPIService();
        $apisocial->updateFacebookNewestResult();

        return redirect()->route('subscribed-youtube-channels-ranking');
    }

    // Download Excel YouTube Ranking
    // https://dev.to/shanisingh03/how-to-export-data-to-excel-file-in-laravel-8-58lo
    public function exportYouTubeRanking($type)
    {
        if ($type == null) {
            $type = TYPE_EXCEL_ALL;
        }


        return Excel::download(new UsersExport, 'youtube-ranking.xlsx');
    }
}
