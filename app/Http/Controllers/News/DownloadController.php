<?php
namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Services\Admin\Categories\ListCategoryService;
use App\Services\News\ListDownloadService;
use App\Services\All\GetAPIService;
use App\Services\All\ListMostViewPost;
use App\Services\All\ListNewestPost;
use App\Services\All\ListRecentPost;
use App\Services\All\ListUpdatePost;
use App\Services\All\PostService;
use App\Services\News\ListSearchPostService;
use App\Services\News\HomePagePostService;
use Illuminate\Http\Request;
use Session;

class DownloadController extends Controller
{
    public function __construct()
    {
        // Menu
        $this->posts          = new HomePagePostService();
        $this->viewCategories = $this->posts->showAllCategories();

        // Tags
        $tag            = new ListSearchPostService;
        $this->viewTags = $tag->tagsList();

        // List video on menu
        // $this->video     = new ListVideo;
        // $this->menuVideo = $this->video->run(5);

        // API Social: Facebook, Youtube, Twitter
        $getAPI = new GetAPIService();
        if (!isset($_SESSION['facebookAPI']))
        {
            Session::put('facebookAPI', $getAPI->getAPIFacebook());
        }

        // Session::flush();
        // Session::forget('youtubeAPI');
        if (!isset($_SESSION['youtubeAPI']))
        {
            Session::put('youtubeAPI', $getAPI->getAPIYoutube());
        }

        $this->twitterAPI = $getAPI->getAPITwitter();
    }

    /**
     * Download page has filter by URL
     * Trang download có danh sách được phân loại dựa theo URL
     *
     */
    public function listPostDownload($urlCat)
    {
        // Public Services
        $menuCategories = new ListDownloadService();
        $viewCategories = $menuCategories->listJoinDownloadPostPaginate();

        $postService = new PostService;
        $viewNewest = $postService->getListNewestPost(NEWEST_HOME_POSTS, null, true, $this->language);
        $viewUpdate = $postService->getListUpdatedPost(UPDATED_HOME_POSTS, null, $this->language);
        $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $this->language);
        $viewMostView = $postService->getListMostViewPost(MOST_VIEW_HOME_POSTS, null);

        // Private Services
        $postDownload = new ListDownloadService();
        $viewPost = $postDownload->listDownloadPostCategory($urlCat);

        return view('news.pages.listdownload', [
            'title'          => TITLE_NEWS_INDEX,

            // Public Services
            'menuCategories' => $viewCategories,
            'newest'         => $viewNewest,
            'mostViews'      => $viewMostView,
            'updates'        => $viewUpdate,
            'random'         => $viewRandom,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'posts'          => $viewPost,
        ]);
    }

    public function listSearch(Request $request)
    {
        // Public Services
        $menuCategories = new ListCategoryService();
        $viewCategories = $menuCategories->listEnable(null);

        // $newest     = new ListNewestPost;
        // $viewNewest = $newest->run(NEWEST_HOME_POSTS, null);

        // $mostView     = new ListMostViewPost;
        // $viewMostView = $mostView->run(MOST_VIEW_HOME_POSTS, null);

        // $update     = new ListUpdatePost;
        // $viewUpdate = $update->run(UPDATE_HOME_POSTS, null);

        // $random     = new ListRecentPost;
        // $viewRandom = $random->run(RECENT_HOME_POSTS, null);

        // Private Services
        $listPost = new PostService;
        $viewListPost = $listPost->searchPostHavePaginate($request, $this->language, ITEM_PER_PAGE);

        return view('news.pages.search', [
            'title'     => TITLE_NEWS_INDEX,

            // Public Services
            'menuCategories' => $viewCategories,
            // 'newest'         => $viewNewest,
            // 'mostViews'      => $viewMostView,
            // 'updates'        => $viewUpdate,
            // 'random'         => $viewRandom,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'posts'          => $viewListPost,
        ]);
    }

}
