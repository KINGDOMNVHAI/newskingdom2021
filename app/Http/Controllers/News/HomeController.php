<?php
namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Services\All\PostService;
use App\Services\All\LanguageService;
use App\Services\All\GetAPIService;
use App\Services\All\VideoService;
use App\Services\News\HomePagePostService;
use App\Services\News\ListSearchPostService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Session;

class HomeController extends Controller
{
    public function __construct()
    {
        // Menu
        $this->posts          = new HomePagePostService();
        $this->viewCategories = $this->posts->showAllCategories();

        // Tags
        $tag            = new ListSearchPostService;
        $this->viewTags = $tag->tagsList();

        // API Social: Facebook, Youtube, Twitter

        // Session::flush();
        // Session::forget('facebookAPI');
        // Session::forget('youtubeAPI');

        $getAPI = new GetAPIService();
        $this->facebookAPI = $getAPI->getAPIFacebook();
        $this->youtubeAPI = $getAPI->getAPIYoutube();
        $this->twitterAPI = $getAPI->getAPITwitter();
    }

    public function locale($locale)
    {
        if (! in_array($locale, ['en','vi'])) {
            abort(400);
        }
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect('/');
    }

    public function index()
    {
        // https://www.googleapis.com/youtube/v3/channels?id=UCxUL0zS-XiU36bkUsr5dWbg&key=AIzaSyA4HiPIio4DweSuCap-SVm2U8fUzHhKCnc&part=snippet,contentDetails,statistics,status

        // Get title from parent class
        $title = "Home" . $this->title;

        $languageService = new LanguageService();
        $language = $languageService->getLanguage();

        // Public Services
        $postService = new PostService;
        $viewNewest = $postService->getListNewestPost(NEWEST_HOME_POSTS, null, true, $language);
        $viewUpdate = $postService->getListUpdatedPost(UPDATED_HOME_POSTS, null, $language);
        $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $language);

        $videoService = new VideoService;
        $videoNewest = $videoService->getListNewestVideo(5,null, false, $this->language);

        // Private Services
        $viewData  = $this->posts->postsPerCategory($language);

        $listWebsitePost = $listGamePost = $listAnimePost = $listTipITPost = $listGameGirlPost = [];

        for ($i=0;$i<count($viewData);$i++){
            if ($viewData[$i]->id_cat_post == WEBSITE_POST){
                array_push($listWebsitePost,$viewData[$i]);
            } else if ($viewData[$i]->id_cat_post == GAME_POST){
                array_push($listGamePost,$viewData[$i]);
            } else if ($viewData[$i]->id_cat_post == ANIME_POST){
                array_push($listAnimePost,$viewData[$i]);
            } else if ($viewData[$i]->id_cat_post == THU_THUAT_IT_POST){
                array_push($listTipITPost,$viewData[$i]);
            } else if ($viewData[$i]->id_cat_post == GAME_GIRL_POST){
                array_push($listGameGirlPost,$viewData[$i]);
            }
        }

        return view('news.pages.home', [
            'title'         => $title,
            'language'      => $language,

            // Public Services
            'randoms'       => $viewRandom,
            'viewTags'      => $this->viewTags,

            'facebookAPI'    => $this->facebookAPI,
            'youtubeAPI'     => $this->youtubeAPI,
            'twitterAPI'     => $this->twitterAPI,

            // 'newestPost'        => $newestPost,
            // 'listNewestPost1'   => $listNewestPost1,
            // 'listNewestPost2'   => $listNewestPost2,

            // Private Services
            'videos'     => $videoNewest,
            'updates'    => $viewUpdate,
            'newest'     => $viewNewest,
            'categories' => $this->viewCategories,
            'listWebsitePost'   => $listWebsitePost,
            'listGamePost'      => $listGamePost,
            'listAnimePost'     => $listAnimePost,
            'listTipITPost'     => $listTipITPost,
        ]);
    }

    public function about()
    {
        // Get title from parent class
        $title = "About KINGDOM NVHAI" . $this->title;

        $languageService = new LanguageService();
        $language = $languageService->getLanguage();

        $postService = new PostService;
        $viewNewest = $postService->getListNewestPost(NEWEST_HOME_POSTS, null, true, $this->language);
        $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $this->language);

        // Private Services
        $viewData  = $this->posts->postsEachCategory($this->language);

        return view('news.pages.about-us', [
            'title'          => $title,
            'language'       => $language,

            // Public Services
            'randoms'        => $viewRandom,
            'viewTags'       => $this->viewTags,

            'facebookAPI'    => $this->facebookAPI,
            'youtubeAPI'     => $this->youtubeAPI,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'newest'     => $viewNewest,
            'categories' => $this->viewCategories,
            'posts'      => $viewData,
        ]);
    }

    public function adfMVShort()
    {
        return redirect('http://fumacrom.com/1v9M0');
    }

    public function kdplaybackhentai()
    {
        return redirect('http://fumacrom.com/1v9M0');
    }
}

// Tạo danh sách bài viết về JAV và dán HTML này vào home.blade

// <!-- section header -->
// <div class="section-header">
//     <h2 class="section-title">JAV</h2>
//     <img src="{{asset('news/images/wave.svg')}}" class="wave" alt="wave" />
// </div>
// <div class="padding-30 rounded bordered">
//     <div class="row gy-5">
//         <div class="col-sm-6">
//             <!-- post -->
//             <div class="post">
//                 <div class="thumb rounded">
//                     <a href="{{route('post-content', $listJAVPost[0]->url_post)}}" class="category-badge position-absolute">{{$listJAVPost[0]->name_cat_post}}</a>
//                     <span class="post-format"><i class="icon-picture"></i></span>
//                     <a href="{{route('post-content', $listJAVPost[0]->url_post)}}" title="{{$listJAVPost[0]->name_cat_post}}">
//                         <div class="inner">
//                             @if($listJAVPost[0]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listJAVPost[0]->thumbnail_post))
//                             <img src="{{asset('upload/images/thumbnail/' . $listJAVPost[0]->thumbnail_post)}}" alt="{{$listJAVPost[0]->name_post}}" title="{{$listJAVPost[0]->name_post}}" width="100%" />
//                             @else
//                             <img src="{{asset('news/images/td_600x400.jpg')}}" />
//                             @endif
//                         </div>
//                     </a>
//                 </div>
//                 <ul class="meta list-inline mt-4 mb-0">
//                     <li class="list-inline-item">{{$listJAVPost[0]->date_post}}</li>
//                 </ul>
//                 <h5 class="post-title mb-3 mt-3"><a href="{{ route('post-content', $listJAVPost[0]->url_post ) }}">{{$listJAVPost[0]->name_post}}</a></h5>
//                 <p class="excerpt mb-0">{{$listJAVPost[0]->present_post}}</p>
//             </div>
//         </div>
//         <div class="col-sm-6">
//             @for($i = 1; $i < count($listJAVPost); $i++)
//             <div class="post post-list-sm square">
//                 <div class="thumb rounded">
//                     <a href="{{ route('post-content', $listJAVPost[$i]->url_post ) }}">
//                         <div class="inner">
//                             @if($listJAVPost[$i]->thumbnail_post && file_exists('upload/images/thumbnail/' . $listJAVPost[$i]->thumbnail_post))
//                             <img src="{{asset('upload/images/thumbnail/' . $listJAVPost[$i]->thumbnail_post) }}" alt="{{$listJAVPost[$i]->name_post}}" />
//                             @else
//                             <img src="{{asset('news/images/no-thumb/td_600x400.jpg')}}" alt="{{$listJAVPost[$i]->name_post}}" width="100%" />
//                             @endif
//                         </div>
//                     </a>
//                 </div>
//                 <div class="details clearfix">
//                     <h6 class="post-title my-0"><a href="{{ route('post-content', $listJAVPost[$i]->url_post ) }}" title="{{$listJAVPost[$i]->name_post}}">{{$listJAVPost[$i]->name_post}}</a></h6>
//                     <ul class="meta list-inline mt-1 mb-0">
//                         <li class="list-inline-item">{{$listJAVPost[$i]->date_post}}</li>
//                     </ul>
//                 </div>
//             </div>
//             @endfor
//         </div>
//     </div>
// </div>
// </div>
