<?php
namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Services\Admin\Categories\ListCategoryService;
use App\Services\All\GetAPIService;
use App\Services\All\PostService;
use App\Services\All\LanguageService;
use App\Services\All\CategoryPostService;
use App\Services\News\ContentPostService;
use App\Services\News\CommentPostService;
use App\Services\News\HomePagePostService;
use App\Services\News\ListSearchPostService;
use Illuminate\Auth\Events\Failed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class PostController extends Controller
{
    public function __construct()
    {
        // Menu
        $this->posts          = new HomePagePostService();
        $this->viewCategories = $this->posts->showAllCategories();

        // Tags
        $tag            = new ListSearchPostService;
        $this->viewTags = $tag->tagsList();

        // API Social: Facebook, Youtube, Twitter
        $getAPI = new GetAPIService();
        $this->facebookAPI = $getAPI->getAPIFacebook();
        $this->youtubeAPI = $getAPI->getAPIYoutube();
        $this->twitterAPI = $getAPI->getAPITwitter();
    }

    public function listPostCategory($urlCat)
    {
        $languageService = new LanguageService();
        $language = $languageService->getLanguage();

        // Public Services
        $menuCategories = new CategoryPostService();
        $viewCategories = $menuCategories->listCategoryEnable(false, true, PAGINATE_POST_INDEX);

        $postService = new PostService;
        $viewNewest = $postService->getListNewestPost(NEWEST_HOME_POSTS, null, true, $this->language);
        $viewUpdate = $postService->getListUpdatedPost(UPDATED_HOME_POSTS, null, $this->language);
        $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $this->language);

        // Private Services
        $postCategory = new CategoryPostService();
        $viewListPost = $postCategory->listPost($urlCat,$language);

        // Get title from parent class
        $title = $viewListPost[0]['name_cat_post'] . $this->title;

        return view('news.pages.category', [
            'title'          => $title ,
            'language'       => $language,

            // Public Services
            'categories'     => $this->viewCategories,
            'menuCategories' => $viewCategories,
            'newest'         => $viewNewest,
            'updates'        => $viewUpdate,
            'randoms'        => $viewRandom,
            'viewTags'       => $this->viewTags,

            'facebookAPI'    => $this->facebookAPI,
            'youtubeAPI'     => $this->youtubeAPI,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'listpost'       => $viewListPost,
        ]);
    }

    public function contentPost($urlPost)
    {
        $languageService = new LanguageService();
        $language = $languageService->getLanguage();

        // Private Services
        $single = new PostService;
        $viewContent = $single->content($urlPost, $language);
        $checkContentExist = $single->checkContentExist($urlPost, 'en');
        if ($viewContent == null) {
            return view('news.pages.404', [
                'title'         => '404' . TITLE_KDPLAYBACK_INDEX,
                'facebookAPI'   => $this->facebookAPI,
                'youtubeAPI'    => $this->youtubeAPI,
                'twitterAPI'    => $this->twitterAPI,
            ]);
        }
        $viewInvolve = $single->involve($urlPost, $language);

        // Public Services
        $menuCategories = new CategoryPostService();
        $viewCategories = $menuCategories->listCategoryEnable(false, true, PAGINATE_POST_INDEX);

        $postService = new PostService;
        $viewMostView = $postService->getListMostViewPost(MOST_VIEW_HOME_POSTS, null);
        $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $language);

        // $comment = new CommentPostService;
        // $viewComment = $comment->commentOfPost($urlPost);

        // Get title from parent class
        $title = $this->title;
        if ($viewContent != null) {
            $title = $viewContent->name_detailpost . $this->title;

            if ($viewContent->warning) {

                // Get user id




            }
        }

        return view('news.pages.single', [
            'title'          => $title,
            'language'       => $language,

            // Public Services
            'categories'     => $this->viewCategories,
            'menuCategories' => $viewCategories,
            'mostViews'      => $viewMostView,
            'randoms'        => $viewRandom,
            'viewTags'       => $this->viewTags,

            'facebookAPI'    => $this->facebookAPI,
            'youtubeAPI'     => $this->youtubeAPI,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'content'        => $viewContent,
            'involves'       => $viewInvolve,
            'contentExist'   => $checkContentExist,
            // 'comments'       => $viewComment,
        ]);
    }

    public function commentPost(Request $request, $idDetailpost)
    {
        $datas = $request->all();
        unset($datas['_token']);

        $comment = new CommentPostService;
        $insertComment = $comment->commentInsert($idDetailpost, $datas);

        return redirect()->back();
    }

    public function listSearch(Request $request)
    {
        $languageService = new LanguageService();
        $language = $languageService->getLanguage();

        // Public Services
        $menuCategories = new CategoryPostService();
        $viewCategories = $menuCategories->listCategoryEnable(false, true, PAGINATE_POST_INDEX);

        $postService = new PostService;
        $viewNewest = $postService->getListNewestPost(NEWEST_HOME_POSTS, null, true, $this->language);
        $viewMostView = $postService->getListMostViewPost(MOST_VIEW_HOME_POSTS, null);
        $viewUpdate = $postService->getListUpdatedPost(UPDATED_HOME_POSTS, null, $this->language);
        $viewRandom = $postService->getListRandomPost(RECENT_HOME_POSTS, $language);

        // Private Services
        $viewListPost = $postService->searchPostHavePaginate($request, $this->language, PAGINATE_POST_SEARCH);

        return view('news.pages.search', [
            'title'          => TITLE_NEWS_INDEX,
            'language'       => $language,

            // Public Services
            'categories'     => $this->viewCategories,
            'menuCategories' => $viewCategories,
            'newest'         => $viewNewest,
            'mostViews'      => $viewMostView,
            'updates'        => $viewUpdate,
            'randoms'        => $viewRandom,
            'viewTags'       => $this->viewTags,

            'facebookAPI'    => $this->facebookAPI,
            'youtubeAPI'     => $this->youtubeAPI,
            'twitterAPI'     => $this->twitterAPI,

            // Private Services
            'listpost'       => $viewListPost,
            'keyword'        => $request->keyword,
        ]);
    }

    public function contentAjax($lang,$urlPost)
    {
        $single = new PostService;
        $htmlPresent = $htmlContent = '';
        try
        {
            if(request()->ajax())
            {
                switch ($lang) {
                    case 'en':
                        $content = $single->content($urlPost, 'en');
                        $htmlPresent = $htmlPresent . $content->present_detailpost;
                        $htmlContent = $htmlContent . $content->content_detailpost;
                    break;
                    default:
                        $content = $single->content($urlPost, 'vi');
                        $htmlPresent = $htmlPresent . $content->present_detailpost;
                        $htmlContent = $htmlContent . $content->content_detailpost;
                }
                return response()->json(['success' => true, 'htmlPresent' => $htmlPresent, 'htmlContent' => $htmlContent]);
            }
            return response()->json(['success' => false]);
        }
        catch(\Exception $e)
        {
            \Log::error($e); // create a log for error occurrence at storage/log/laravel.log file
            return response()->json($e->getData(), $e->getStatusCode());
        }
    }
}
