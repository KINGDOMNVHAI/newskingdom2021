<?php
namespace App\Dto;

use App\Models\channels;
use Spatie\DataTransferObject\DataTransferObject;

class ChannelsDto extends DataTransferObject
{
    public int $id_channel;
    public string $name_channel;
    public string $url_video_present;
    public ?string $description_channel;
    public int $subscribe;
    public bool $enable_channel;
    public ?string $thumbnail_channel;
    public string $created_date_channel;
    public string $created_day;
    public string $created_month;
    public string $created_year;
    public bool $virtual_youtuber;
    public bool $visual_novel;
    public bool $hololive;
    public bool $facebook_channel;
    public bool $twitter_channel;
    public bool $instagram_channel;
    public bool $patreon_channel;

    // public function fromRequest(Request $request)
    // {
    //     return new self([
    //         'email' => $request->get('email'),
    //         'is_admin' => $request->get('is_admin'),
    //         'is_moderator' => $request->get('is_moderator'),
    //         'is_worker' => $request->get('is_worker'),
    //         'is_active' => $request->get('is_active'),
    //         'name' => $request->get('name'),
    //         'phone' => $request->get('phone'),
    //         'hourly_cost' => $request->get('hourly_cost'),
    //     ]);
    // }

    public function fromModel(channels $channel)
    {
        $day = date('l', strtotime($channel->created_date_channel));
        $month = date('m', strtotime($channel->created_date_channel));
        $year = date('y', strtotime($channel->created_date_channel));
        dd($day);

        return new self([
            'id_channel' => $channel->id_channel,
            'name_channel' => $channel->name_channel,
            'url_video_present' => $channel->url_video_present,
            'description_channel' => $channel->description_channel,
            'subscribe' => $channel->subscribe,
            'enable_channel' => $channel->enable_channel,
            'thumbnail_channel' => $channel->thumbnail_channel,
            'created_date_channel' => $channel->created_date_channel,
            'virtual_youtuber' => $channel->virtual_youtuber,
            'visual_novel' => $channel->visual_novel,
            'hololive' => $channel->hololive,
            'facebook_channel' => $channel->facebook_channel,
            'twitter_channel' => $channel->twitter_channel,
            'instagram_channel' => $channel->instagram_channel,
            'patreon_channel' => $channel->patreon_channel,
        ]);
    }

    // https://laracasts.com/discuss/channels/code-review/dtos-can-be-redundant-prove-me-wrong
}
