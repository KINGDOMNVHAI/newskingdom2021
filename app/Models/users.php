<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class users extends Authenticatable
{
    //Khai báo tên table
    protected $table = 'users';

    // Khai báo primary key
    // Trong Laravel có users::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'id';

    // Bỏ updated_at
    public $timestamps = true;

    protected $fillable = [
        'id', 'lastname', 'firstname', 'username', 'password', 'email', 'role',
        'city', 'address', 'company', 'facebook', 'twitter',
        'description', 'signature', 'avatar', 'banner', 'medal',
    ];
}
