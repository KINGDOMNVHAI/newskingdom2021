<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class channelsocial extends Model
{
    //Khai báo tên table
    protected $table = 'channelsocial';

    // Khai báo primary key
    // Trong Laravel có apisocial::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'id_channel_social';

    // Bỏ updated_at
    public $timestamps = false;

    protected $fillable = [
        'id_channel_social', 'name_channel_social', 'url_channel_social', 'fan_count'
    ];
}
