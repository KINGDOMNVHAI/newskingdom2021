<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class download extends Model
{
    //Khai báo tên table
    protected $table = 'download';

    // Khai báo primary key
    // Trong Laravel có download::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'idDown';

    // Bỏ updated_at
    public $timestamps = false;

    protected $fillable = [
        'idDown', 'nameDown' , 'linkDown' , 'imgDown' , 'id_category' , 'idDetailPost'
    ];

}
