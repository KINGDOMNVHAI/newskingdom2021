<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    //Khai báo tên table
    protected $table = 'posts';

    // Khai báo primary key
    // Trong Laravel có posts::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'id_post';

    // Bỏ updated_at
    public $timestamps = true;

    protected $fillable = [
        'id_post',
        'name_vi_post', 'name_en_post',
        'url_post',
        'present_vi_post', 'present_en_post',
        'content_vi_post', 'content_en_post',
        'date_post', 'thumbnail_post', 'id_cat_post',
        'signature', 'author', 'views', 'enable', 'popular', 'update',
    ];
}
