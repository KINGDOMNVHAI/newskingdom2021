<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    //Khai báo tên table
    protected $table = 'category';

    // Khai báo primary key
    // Trong Laravel có category::find() nghĩa là tìm theo primary key
    protected $primaryKey = 'id_category';

    // Bỏ updated_at
    public $timestamps = false;

    protected $fillable = [
        'id_category', 'name_category'
    ];
}
